package de.crysxd.octoapp.connectprinter.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ConnectPrinterScope