package de.crysxd.octoapp.connectprinter.ui

import android.widget.Toast
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asFlow
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import de.crysxd.baseui.BaseViewModel
import de.crysxd.baseui.menu.power.PowerControlsMenu
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.AppSettings
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.base.usecase.AutoConnectPrinterUseCase
import de.crysxd.octoapp.base.usecase.AutoConnectPrinterUseCase.Params
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.base.usecase.GetPrinterConnectionUseCase
import de.crysxd.octoapp.base.utils.PollingLiveData
import de.crysxd.octoapp.connectprinter.R
import de.crysxd.octoapp.engine.exceptions.PrintBootingException
import de.crysxd.octoapp.engine.models.connection.Connection
import de.crysxd.octoapp.engine.models.connection.ConnectionState
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.TimeUnit


class ConnectPrinterViewModel(
    private val autoConnectPrinterUseCase: AutoConnectPrinterUseCase,
    private val getPrinterConnectionUseCase: GetPrinterConnectionUseCase,
    private val getPowerDevicesUseCase: GetPowerDevicesUseCase,
    private val octoPreferences: OctoPreferences,
    private val octoPrintRepository: OctoPrintRepository,
) : BaseViewModel() {

    companion object {
        private const val MIN_LOADING_DELAY = 2000L
    }

    val activeInstance get() = octoPrintRepository.getActiveInstanceSnapshot()
    private val connectionTimeoutNs = TimeUnit.SECONDS.toNanos(Firebase.remoteConfig.getLong("printer_connection_timeout_sec"))
    private var lastConnectionAttempt = 0L
    private var psuCyclingState = MutableLiveData<PsuCycledState>(PsuCycledState.NotCycled)

    private val availableSerialConnections = PollingLiveData {
        getPrinterConnectionUseCase.execute(GetPrinterConnectionUseCase.Params(null))
    }

    private var isPsuSupported = false
    private val defaultPsu
        get() = octoPrintRepository.getActiveInstanceSnapshot()
            ?.appSettings
            ?.defaultPowerDevices
            ?.get(AppSettings.DEFAULT_POWER_DEVICE_PSU)
    private var startedAt = System.currentTimeMillis()
    private val uiStateMediator = MediatorLiveData<UiState>()
    private val manualPsuState = MutableLiveData<Boolean?>(null)
    private var userAllowedConnectAt = 0L
    private var manualTrigger = MutableLiveData(Unit)
    val uiState = uiStateMediator.asFlow().map { it }.rateLimit(600).asLiveData()
    private val psuPollingLiveData = PollingLiveData(3000) {
        // Check if the default device (or only power device) is turned on or off
        defaultPsu?.takeIf { it != AppSettings.DEFAULT_POWER_DEVICE_VALUE_NONE }?.let {
            Timber.i("Polling PSU: default=$it")
            val state = getPowerDevicesUseCase.execute(GetPowerDevicesUseCase.Params(queryState = true, onlyGetDeviceWithUniqueId = it)).firstOrNull()
            Timber.i("PSU state: $state")
            manualPsuState.postValue(state?.second == GetPowerDevicesUseCase.PowerState.On)
        }
        Unit
    }

    init {
        uiStateMediator.addSource(octoPreferences.updatedFlow.asLiveData()) { updateUiState() }
        uiStateMediator.addSource(availableSerialConnections) { updateUiState() }
        uiStateMediator.addSource(manualPsuState) { updateUiState() }
        uiStateMediator.addSource(psuPollingLiveData) { updateUiState() }
        uiStateMediator.addSource(psuCyclingState) { updateUiState() }
        uiStateMediator.addSource(manualTrigger) { updateUiState() }
        uiStateMediator.value = UiState.Initializing

        viewModelScope.launch(coroutineExceptionHandler) {
            try {
                isPsuSupported = getPowerDevicesUseCase.execute(
                    GetPowerDevicesUseCase.Params(
                        queryState = false,
                        requiredCapabilities = PowerControlsMenu.DeviceType.PrinterPsu.requiredCapabilities
                    )
                ).isNotEmpty()
                computeUiState()
            } catch (e: Exception) {
                Timber.w(e, "Unable to check power devices")
            }
        }
    }

    private fun updateUiState() = viewModelScope.launch(coroutineExceptionHandler) {
        val s = computeUiState()
        Timber.i("Computed state: $s")
        uiStateMediator.postValue(s)
    }

    private fun computeUiState(): UiState {
        try {
            // Connection
            val connectionResponse = availableSerialConnections.value
            val connectionResult = (connectionResponse as? PollingLiveData.Result.Success)?.result

            // PSU
            val defaultPsu = defaultPsu
            val psuCyclingState = psuCyclingState.value ?: PsuCycledState.NotCycled
            val isPsuTurnedOn = manualPsuState.value ?: false.takeIf { defaultPsu != null && defaultPsu != AppSettings.DEFAULT_POWER_DEVICE_VALUE_NONE }

            // Are we allowed to automatically connect the printer?
            val isAutoConnect = octoPreferences.isAutoConnectPrinter ||
                    TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - userAllowedConnectAt) < 3

            val activeSince = System.currentTimeMillis() - startedAt
            Timber.d("-----")
            Timber.d("ConnectionResult: $connectionResult")
            Timber.d("PsuSupported: $isPsuSupported")
            Timber.d("PsuCycled: $psuCyclingState")
            Timber.d("PsuState: $isPsuTurnedOn")
            Timber.d("activeSince: ${activeSince}ms")
            Timber.d("isAutoConnect: $isAutoConnect")

            if (connectionResponse != null || activeSince > MIN_LOADING_DELAY) {
                return when {
                    isOctoPrintStarting(connectionResponse) ->
                        UiState.OctoPrintStarting

                    isOctoPrintUnavailable(connectionResponse) || connectionResult == null ->
                        UiState.OctoPrintNotAvailable

                    !isAutoConnect ->
                        UiState.WaitingForUser

                    isPsuBeingCycled(psuCyclingState) ->
                        UiState.PrinterPsuCycling

                    isNoPrinterAvailable(connectionResult) ->
                        UiState.WaitingForPrinterToComeOnline(psuConfigured = defaultPsu != null, psuIsOn = isPsuTurnedOn)

                    isPrinterOffline(connectionResult, psuCyclingState) ->
                        UiState.PrinterOffline(isPsuSupported)

                    isPrinterConnecting(connectionResult) ->
                        UiState.PrinterConnecting

                    isPrinterConnected(connectionResult) ->
                        UiState.PrinterConnected

                    else -> {
                        // Printer ready to connect
                        autoConnect(connectionResult)
                        UiState.WaitingForPrinterToComeOnline(psuConfigured = defaultPsu != null, psuIsOn = isPsuTurnedOn)
                    }

                }
            }
        } catch (e: Exception) {
            Timber.e(e)
        }

        return uiStateMediator.value ?: UiState.Initializing
    }

    private fun isPsuBeingCycled(psuCycledState: PsuCycledState) =
        psuCycledState == PsuCycledState.Cycling

    private fun isOctoPrintUnavailable(connectionResponse: PollingLiveData.Result<ConnectionState>?) =
        connectionResponse is PollingLiveData.Result.Failure

    private fun isOctoPrintStarting(connectionResponse: PollingLiveData.Result<ConnectionState>?) =
        (connectionResponse as? PollingLiveData.Result.Failure)?.exception is PrintBootingException

    private fun isNoPrinterAvailable(connectionResponse: ConnectionState) =
        connectionResponse.options.ports.isEmpty()

    private fun isPrinterOffline(
        connectionResponse: ConnectionState,
        psuState: PsuCycledState
    ) = connectionResponse.options.ports.isNotEmpty() &&
            (isInErrorState(connectionResponse) || isConnectionAttemptTimedOut(connectionResponse)) &&
            psuState != PsuCycledState.Cycled

    private fun isConnectionAttemptTimedOut(connectionResponse: ConnectionState) = isPrinterConnecting(connectionResponse) &&
            System.nanoTime() - lastConnectionAttempt > connectionTimeoutNs

    private fun isInErrorState(connectionResponse: ConnectionState) = listOf(
        Connection.State.MAYBE_UNKNOWN_ERROR,
        Connection.State.MAYBE_CONNECTION_ERROR
    ).contains(connectionResponse.current.state)

    private fun isPrinterConnecting(connectionResponse: ConnectionState) = listOf(
        Connection.State.MAYBE_CONNECTING,
        Connection.State.MAYBE_DETECTING_SERIAL_PORT,
        Connection.State.MAYBE_DETECTING_SERIAL_CONNECTION,
        Connection.State.MAYBE_DETECTING_BAUDRATE
    ).contains(connectionResponse.current.state)

    private fun isPrinterConnected(connectionResponse: ConnectionState) = connectionResponse.current.port != null ||
            connectionResponse.current.baudrate != null

    private fun autoConnect(connectionResponse: ConnectionState) = viewModelScope.launch(coroutineExceptionHandler) {
        if (connectionResponse.options.ports.isNotEmpty() && !didJustAttemptToConnect() && !isPrinterConnecting(connectionResponse)) {
            recordConnectionAttempt()
            Timber.i("Attempting auto connect")
            autoConnectPrinterUseCase.execute(
                if (connectionResponse.current.state == Connection.State.MAYBE_ERROR_FAILED_TO_AUTODETECT_SERIAL_PORT) {
                    val app = BaseInjector.get().app()
                    Toast.makeText(app, app.getString(R.string.connect_printer___auto_selection_failed), Toast.LENGTH_SHORT).show()
                    OctoAnalytics.logEvent(OctoAnalytics.Event.PrinterAutoConnectFailed)
                    Params(connectionResponse.options.ports.first())
                } else {
                    Params()
                }
            )
            psuCyclingState.postValue(PsuCycledState.NotCycled)
        }
    }

    private fun didJustAttemptToConnect() =
        (System.nanoTime() - lastConnectionAttempt) < TimeUnit.SECONDS.toNanos(10)

    private fun recordConnectionAttempt() {
        lastConnectionAttempt = System.nanoTime()
    }

    private fun resetConnectionAttempt() {
        lastConnectionAttempt = 0
    }

    fun setDeviceOn(on: Boolean) = viewModelScope.launch(coroutineExceptionHandler) {
        val wasPsuTurnedOn = manualPsuState.value
        try {
            manualPsuState.postValue(on)
        } catch (e: Exception) {
            manualPsuState.postValue(wasPsuTurnedOn)
            throw e
        }
    }

    fun cyclePsu() = viewModelScope.launch(coroutineExceptionHandler) {
        psuCyclingState.postValue(PsuCycledState.Cycled)
        manualPsuState.postValue(true)
        resetConnectionAttempt()
    }

    fun retryConnectionFromOfflineState() {
        lastConnectionAttempt = 0L
        psuCyclingState.postValue(PsuCycledState.Cycled)
    }

    fun beginConnect() {
        Timber.i("Connection initiated")
        userAllowedConnectAt = System.currentTimeMillis()
        manualTrigger.postValue(Unit)
    }

    fun powerDeviceSelected() {
        manualPsuState.postValue(null)
        manualTrigger.postValue(Unit)
    }

    private sealed class PsuCycledState {
        object NotCycled : PsuCycledState()
        object Cycled : PsuCycledState()
        object Cycling : PsuCycledState()
    }

    sealed class UiState {

        object Initializing : UiState()

        object OctoPrintStarting : UiState()
        object OctoPrintNotAvailable : UiState()

        data class WaitingForPrinterToComeOnline(val psuConfigured: Boolean, val psuIsOn: Boolean?) : UiState()
        object WaitingForUser : UiState()
        object PrinterConnecting : UiState()
        data class PrinterOffline(val psuSupported: Boolean) : UiState()
        object PrinterPsuCycling : UiState()
        object PrinterConnected : UiState()

        object Unknown : UiState()

    }
}