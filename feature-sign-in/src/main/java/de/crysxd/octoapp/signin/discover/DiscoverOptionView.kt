package de.crysxd.octoapp.signin.discover

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import de.crysxd.octoapp.base.data.models.NetworkService
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.sharedcommon.http.framework.withoutBasicAuth
import de.crysxd.octoapp.signin.R
import de.crysxd.octoapp.signin.databinding.DiscoverOptionBinding

class DiscoverOptionView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleRes: Int = 0
) : LinearLayoutCompat(
    context, attrs, defStyleRes
) {
    private val binding = DiscoverOptionBinding.inflate(LayoutInflater.from(context), this)
    var onDelete: () -> Unit = {}
        set(value) {
            field = value
            binding.buttonDelete.setOnClickListener { value() }
        }
    private var optionId: String = ""

    init {
        gravity = Gravity.CENTER_VERTICAL
    }

    override fun setOnClickListener(l: OnClickListener?) {
        binding.card.setOnClickListener(l)
    }

    fun show(option: NetworkService) {
        optionId = option.webUrl.toString()
        binding.title.text = option.label
        binding.subtitle.text = option.detailLabel
        binding.card.setCardBackgroundColor(ContextCompat.getColor(context, R.color.menu_style_printer_background))
        binding.buttonDelete.setColorFilter(ContextCompat.getColor(context, R.color.menu_style_printer_foreground))
        binding.shevron.setColorFilter(ContextCompat.getColor(context, R.color.menu_style_printer_foreground))
    }

    fun isShowing(option: NetworkService) = option.webUrl.toString() == optionId

    fun show(option: OctoPrintInstanceInformationV3, enabled: Boolean) {
        optionId = option.id
        alpha = if (enabled) 1f else 0.2f
        binding.shevron.isVisible = enabled
        binding.title.text = option.label
        binding.subtitle.text = option.webUrl.withoutBasicAuth().toString().takeIf { option.label != it }
        binding.subtitle.isVisible = binding.subtitle.text.isNotBlank()
        binding.card.setCardBackgroundColor(ContextCompat.getColor(context, R.color.menu_style_octoprint_background))
        binding.buttonDelete.setColorFilter(ContextCompat.getColor(context, R.color.menu_style_octoprint_foreground))
        binding.shevron.setColorFilter(ContextCompat.getColor(context, R.color.menu_style_octoprint_foreground))
    }

    fun isShowing(option: OctoPrintInstanceInformationV3) = option.id == optionId

    fun showManualConnect() {
        binding.title.text = context.getString(R.string.sign_in___discovery___connect_manually_title)
        binding.subtitle.text = context.getString(R.string.sign_in___discovery___connect_manually_subtitle)
        binding.card.setCardBackgroundColor(ContextCompat.getColor(context, R.color.menu_style_settings_background))
        binding.buttonDelete.setColorFilter(ContextCompat.getColor(context, R.color.menu_style_settings_foreground))
        binding.shevron.setColorFilter(ContextCompat.getColor(context, R.color.menu_style_settings_foreground))
    }

    fun showDelete() {
        binding.buttonDelete.isVisible = true
    }

    fun showQuickSwitchOption() {
        binding.title.text = context.getString(R.string.sign_in___discovery___quick_switch_disabled_title)
        binding.subtitle.text = context.getString(R.string.sign_in___discovery___quick_switch_disabled_subtitle)
        binding.card.setCardBackgroundColor(ContextCompat.getColor(context, R.color.menu_style_support_background))
        binding.buttonDelete.setColorFilter(ContextCompat.getColor(context, R.color.menu_style_support_foreground))
        binding.shevron.setColorFilter(ContextCompat.getColor(context, R.color.menu_style_support_foreground))
    }
}