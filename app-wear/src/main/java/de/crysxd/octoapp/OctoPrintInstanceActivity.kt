package de.crysxd.octoapp

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.asLiveData
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.ext.wearEventFlow

abstract class OctoPrintInstanceActivity : AppCompatActivity() {

    abstract val instanceId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setBackgroundDrawable(ColorDrawable(Color.BLACK))

        // Keep this instance's flow connected
        BaseInjector.get().octoPrintProvider()
            .wearEventFlow(tag = this::class.java.simpleName, instanceId = instanceId)
            .asLiveData()
            .observe(this) {}
    }
}