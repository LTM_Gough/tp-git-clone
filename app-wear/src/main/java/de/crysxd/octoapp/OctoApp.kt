package de.crysxd.octoapp

import android.app.Application
import androidx.wear.phone.interactions.notifications.BridgingConfig
import androidx.wear.phone.interactions.notifications.BridgingManager
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.NetworkModule
import de.crysxd.octoapp.base.di.SharedBaseComponent
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.logging.AndroidFirebaseAdapter
import de.crysxd.octoapp.base.logging.CachedAntiLog
import de.crysxd.octoapp.base.logging.FirebaseAntiLog
import de.crysxd.octoapp.base.logging.LogcatAntiLog
import de.crysxd.octoapp.base.logging.NapierTree
import de.crysxd.octoapp.base.migrations.AllMigrations
import de.crysxd.octoapp.base.network.CachedLocalDnsResolver
import de.crysxd.octoapp.base.network.NetworkServiceDiscovery
import de.crysxd.octoapp.base.network.dnssd.DnsSd
import de.crysxd.octoapp.di.WearInjector
import de.crysxd.octoapp.sharedcommon.di.PlatformModule
import de.crysxd.octoapp.sharedcommon.di.SharedCommonComponent
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import io.github.aakira.napier.Napier
import timber.log.Timber

class OctoApp : Application() {

    override fun onCreate() {
        super.onCreate()

        // Setup logging
        if (BuildConfig.DEBUG) {
            Napier.base(LogcatAntiLog)
        }
        Napier.base(FirebaseAntiLog)
        Napier.base(CachedAntiLog)
        FirebaseAntiLog.initAdapter(AndroidFirebaseAdapter())
        Timber.plant(NapierTree())
        val wrapped = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler { t, e ->
            Timber.tag("Uncaught!").wtf(e)
            wrapped?.uncaughtException(t, e)
        }

        // Do not bridge status notifications from the phone
        BridgingManager.fromContext(this).setConfig(
            BridgingConfig.Builder(this, true)
                .addExcludedTag(getString(R.string.print_status_bridge_tag))
                .build()
        )

        // Setup Koin
        SharedCommonInjector.setComponent(
            SharedCommonComponent(
                platformModule = PlatformModule(this)
            )
        )
        SharedBaseInjector.setComponent(
            SharedBaseComponent(
                sharedCommonComponent = SharedCommonInjector.get(),
                networkModule = NetworkModule(
                    networkServiceDiscovery = NetworkServiceDiscovery.Noop,
                    dns = CachedLocalDnsResolver(this, DnsSd.create(this))
                )
            )
        )

        // Setup Dagger
        BaseInjector.init(this)
        WearInjector.init(BaseInjector.get())

        // Migrate
        AllMigrations(this).migrate()

        // Setup RemoteConfig
        Firebase.remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
        Firebase.remoteConfig.setConfigSettingsAsync(remoteConfigSettings {
            minimumFetchIntervalInSeconds = if (BuildConfig.DEBUG) 10 else 3600
        })

        // Init Wear services
        WearInjector.get().wearDataLayerService()

        // Inject test env for benchmarks
        if (BuildConfig.BENCHMARK) {
            BaseInjector.get().octoPreferences().isAutoConnectPrinter = true
            BaseInjector.get().octorPrintRepository().setActive(
                trigger = "benchmark",
                instance = OctoPrintInstanceInformationV3(
                    id = "test-env",
                    webUrl = BuildConfig.BENCHMARK_TEST_ENV.toUrl(),
                    apiKey = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                )
            )
        }
    }
}