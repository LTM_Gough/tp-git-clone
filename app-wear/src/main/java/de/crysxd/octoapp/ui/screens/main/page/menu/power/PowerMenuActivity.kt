package de.crysxd.octoapp.ui.screens.main.page.menu.power

import android.os.Bundle
import android.os.Parcelable
import androidx.activity.compose.setContent
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.FlashAuto
import androidx.compose.material.icons.rounded.FlashOff
import androidx.compose.material.icons.rounded.FlashOn
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.stringResource
import androidx.core.os.bundleOf
import androidx.lifecycle.lifecycleScope
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.rememberScalingLazyListState
import de.crysxd.octoapp.OctoPrintInstanceActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.ext.showFailure
import de.crysxd.octoapp.ext.showSuccess
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.Confirmation
import de.crysxd.octoapp.ui.framework.MainPageScaffold
import de.crysxd.octoapp.ui.framework.MenuHeader
import de.crysxd.octoapp.ui.framework.MenuItemChip
import de.crysxd.octoapp.ui.framework.OctoScalingLazyColumn
import de.crysxd.octoapp.ui.framework.rememberConfirmationState
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.parcelize.Parcelize
import timber.log.Timber

class PowerMenuActivity : OctoPrintInstanceActivity() {

    private val args get() = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))
    override val instanceId get() = args.instanceId

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val deviceInfo = MutableStateFlow<PowerDevice?>(null)
        lifecycleScope.launchWhenCreated {
            try {
                GetPowerDevicesUseCase.Params(
                    instanceId = args.instanceId,
                    queryState = false,
                    onlyGetDeviceWithUniqueId = args.uniqueDeviceId
                ).let {
                    deviceInfo.value = BaseInjector.get().getPowerDevicesUseCase().execute(it).first().first
                }
            } catch (e: Exception) {
                Timber.e(e)
                finish()
                showFailure(e)
            }
        }

        setContent {
            val instance by BaseInjector.get().octorPrintRepository()
                .instanceInformationFlow(args.instanceId).collectAsState(initial = null)

            val device by deviceInfo.collectAsState()

            OctoAppTheme.ForOctoPrint(instance = instance) {
                OctoAppTheme.ForMenuStyle(OctoAppTheme.MenuStyle.Printer) {
                    MainPageScaffold {
                        val state = rememberScalingLazyListState()
                        OctoScalingLazyColumn(state = state) {
                            device?.let { content(it) }
                        }
                    }
                }
            }
        }
    }

    private fun ScalingLazyListScope.content(device: PowerDevice) {
        item {
            MenuHeader(text = device.displayName)
        }
        if (device.controlMethods.contains(PowerDevice.ControlMethod.TurnOnOff)) {
            item {
                MenuItemChip(
                    icon = Icons.Rounded.FlashOn,
                    text = stringResource(id = R.string.power_menu___turn_on),
                    onClick = {
                        device.turnOn()
                        showSuccess()
                        finish()
                    }
                )
            }
            item {
                suspend fun doPowerOff() {
                    device.turnOff()
                    showSuccess()
                    finish()
                }

                val confirmationState by rememberConfirmationState()
                Confirmation(
                    state = confirmationState,
                    text = stringResource(id = R.string.power_menu___confirm_turn_x_off, device.displayName),
                    action = ::doPowerOff,
                )

                MenuItemChip(
                    icon = Icons.Rounded.FlashOff,
                    text = stringResource(id = R.string.power_menu___turn_off),
                    onClick = {
                        val confirm = BaseInjector.get().octoPreferences().confirmPowerOffDevices.contains(device.displayName)
                        if (confirm) {
                            confirmationState.visible = true
                        } else {
                            doPowerOff()
                        }
                    }
                )
            }
        }
        if (device.controlMethods.contains(PowerDevice.ControlMethod.Toggle))
            item {
                MenuItemChip(
                    icon = Icons.Rounded.FlashAuto,
                    text = stringResource(id = R.string.power_menu___toggle),
                    onClick = {
                        device.toggle()
                        showSuccess()
                        finish()
                    }
                )
            }
        item {
            MenuItemChip(
                icon = Icons.Rounded.FlashAuto,
                text = stringResource(id = R.string.power_menu___cycle),
                onClick = {
                    device.turnOff()
                    delay(2000)
                    device.turnOn()
                    showSuccess()
                    finish()
                }
            )
        }
    }

    @Parcelize
    data class Args(
        val uniqueDeviceId: String,
        val instanceId: String,
    ) : Parcelable {
        companion object {
            fun fromBundle(bundle: Bundle) = bundle.getParcelable<Args>("args")
        }

        fun toBundle() = bundleOf(
            "args" to this
        )
    }
}