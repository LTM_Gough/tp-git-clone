package de.crysxd.octoapp.ui.framework

import androidx.compose.runtime.compositionLocalOf
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3

val LocalOctoPrint = compositionLocalOf<OctoPrintInstanceInformationV3?> { null }