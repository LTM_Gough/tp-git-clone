package de.crysxd.octoapp.ui.screens.main

import android.content.Intent
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import de.crysxd.octoapp.R
import de.crysxd.octoapp.ext.requireActivity
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.LocalOctoPrint
import de.crysxd.octoapp.ui.framework.PrimaryButtonChip
import de.crysxd.octoapp.ui.framework.TitleDetails
import de.crysxd.octoapp.ui.screens.files.FileListMenuActivity

@Composable
fun PrepareHeaderContent() {
    val instanceId = LocalOctoPrint.current?.id ?: return
    val activity = LocalContext.current.requireActivity()

    TitleDetails(detail = R.string.connect_printer___printer_connected_title)
    Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin1))
    PrimaryButtonChip(
        text = stringResource(id = R.string.start_printing),
        onClick = {
            Intent(activity, FileListMenuActivity::class.java).also {
                it.putExtras(FileListMenuActivity.Args(instanceId).toBundle())
                activity.startActivity(it)
            }
        }
    )
}