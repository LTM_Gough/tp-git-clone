package de.crysxd.octoapp.ui.screens.files

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.data.models.FileListState
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.sorted
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.ext.wearEventFlow
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import timber.log.Timber

class FileListViewModel(val instanceId: String, val folder: FileObject.Folder?) : ViewModel() {

    private val mutableState = MutableStateFlow<State>(State.Loading)
    val state = mutableState.asStateFlow()
    private val mutablePrinting = MutableStateFlow(false)
    val printing = mutablePrinting.asStateFlow()

    init {
        loadFiles()
        viewModelScope.launch {
            BaseInjector.get().octoPrintProvider().wearEventFlow("file-vm", instanceId)
                .onEach {
                    if ((it is Event.MessageReceived) && it.message is Message.Event.UpdatedFiles) {
                        Timber.i("Files changed, reloading")
                        loadFiles()
                    }
                    ((it as? Event.MessageReceived)?.message as? Message.Current)?.let {
                        mutablePrinting.value = it.state?.flags?.isPrinting() == true
                    }
                }.retry {
                    Timber.e(it)
                    delay(1000)
                    true
                }.collect()
        }

        viewModelScope.launch {
            var first = true
            BaseInjector.get().octoPreferences().updatedFlow.onEach {
                if (!first) {
                    loadFiles()
                } else {
                    first = false
                }
            }.retry {
                Timber.e(it)
                delay(1000)
                true
            }.collect()
        }
    }

    fun loadFiles() {
        viewModelScope.launch {
            try {
                Timber.i("Loading files for ${folder?.path ?: "root"}")
                mutableState.emit(State.Loading)
                val file = BaseInjector.get().loadFilesUseCase().execute(
                    LoadFilesUseCase.Params.ForFolder(
                        instanceId = instanceId,
                        folder = folder,
                        fileOrigin = FileOrigin.Local,
                    )
                ).onEach {
                    when (it) {
                        is FileListState.Error -> throw it.exception
                        is FileListState.Loaded -> mutableState.emit(State.Loading)
                        FileListState.Loading -> Unit
                    }
                }.filterIsInstance<FileListState.Loaded>().first().file
                val files = requireNotNull((file as? FileObject.Folder)?.children) { "Missing children" }
                mutableState.emit(State.Files(files.sorted()))
            } catch (e: Exception) {
                Timber.e(e)
                mutableState.emit(State.Error(e))
            }
        }
    }

    sealed class State {
        object Loading : State()
        data class Error(val exception: Exception) : State()
        data class Files(val files: List<FileObject>) : State()
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(val instanceId: String, val folder: FileObject.Folder?) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T = FileListViewModel(instanceId, folder) as T
    }
}
