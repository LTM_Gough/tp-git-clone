package de.crysxd.octoapp.ui.screens.main.page.menu.octoprint

import android.os.Parcelable
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.wear.compose.material.ScalingLazyListScope
import de.crysxd.octoapp.base.di.BaseInjector
import kotlinx.coroutines.flow.collectLatest
import kotlinx.parcelize.Parcelize

fun ScalingLazyListScope.octoPrintMenu(state: () -> OctoPrintMenuState) {
    if (state().hasSystemCommands) {
        item {
            ShowSystemCommandsMenuItem()
        }
    }

    item { OpenOctoPrintMenuItem() }
}

@Composable
fun rememberOctoPrintMenuState(instanceId: String): MutableState<OctoPrintMenuState> {
    val state = rememberSaveable { mutableStateOf(OctoPrintMenuState(hasSystemCommands = false, instanceId = instanceId)) }

    LaunchedEffect(Unit) {
        BaseInjector.get().octorPrintRepository().instanceInformationFlow(instanceId).collectLatest {
            state.value = state.value.copy(hasSystemCommands = !it?.systemCommands.isNullOrEmpty())
        }
    }

    return state
}

@Parcelize
data class OctoPrintMenuState(
    val instanceId: String,
    val hasSystemCommands: Boolean,
) : Parcelable