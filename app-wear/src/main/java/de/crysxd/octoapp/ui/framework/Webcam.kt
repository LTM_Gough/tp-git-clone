package de.crysxd.octoapp.ui.framework

import android.graphics.Bitmap
import android.os.Looper
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Refresh
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.boundsInRoot
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.content.edit
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.wear.compose.material.CircularProgressIndicator
import androidx.wear.compose.material.Icon
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.Text
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.composeErrorMessage
import de.crysxd.octoapp.base.usecase.GetWebcamSnapshotUseCase
import de.crysxd.octoapp.ui.OctoAppTheme
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch
import timber.log.Timber

fun ScalingLazyListScope.snapshotWebcam(active: () -> Boolean) {
    item("snapshotWebcam") {
        var visibleInWindow by remember { mutableStateOf(false) }

        val instanceId = LocalOctoPrint.current?.id ?: return@item
        val vm = viewModel(
            modelClass = WebcamViewModel::class.java,
            factory = WebcamViewModelFactory(instanceId),
            key = "webcam/$instanceId"
        )
        val snapshot = if (active() && visibleInWindow) {
            vm.snapshotFlow.collectAsState(WebcamViewModel.Snapshot(vm.defaultAspectRatio)).value
        } else {
            // We are not active, use loading=true to disable animation of countdown
            WebcamViewModel.Snapshot(vm.defaultAspectRatio, loading = true)
        }
        val errorMessage by remember { derivedStateOf { snapshot.errorMessage } }
        val advertiseCompanion by remember { derivedStateOf { snapshot.advertiseCompanion } }

        Message(
            background = OctoAppTheme.colors.errorBackground,
            text = errorMessage ?: "",
            showActionText = stringResource(id = R.string.wear_webcam___show_error),
            hideActionText = stringResource(id = R.string.wear_webcam___hide_error),
            messageVisible = errorMessage != null
        ) {
            Message(
                background = colorResource(id = R.color.accent),
                text = stringResource(id = R.string.wear_webcam___companion_message),
                showActionText = stringResource(id = R.string.wear_webcam___show_warning),
                hideActionText = stringResource(id = R.string.wear_webcam___hide_warning),
                messageVisible = advertiseCompanion
            ) {
                val config = LocalConfiguration.current
                val screenRect = with(LocalDensity.current) { Rect(0f, 0f, config.screenWidthDp.dp.toPx(), config.screenHeightDp.dp.toPx()) }
                Box(
                    modifier = Modifier.onGloballyPositioned {
                        val viewRect = it.boundsInRoot()
                        visibleInWindow = screenRect.intersect(viewRect).let { it.width > 0 || it.height > 0 }
                    }
                ) {
                    WebcamImage(
                        vm = vm,
                        snapshot = snapshot
                    )
                }
            }
        }
    }
}

@Composable
private fun WebcamImage(
    vm: WebcamViewModel,
    snapshot: WebcamViewModel.Snapshot,
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .aspectRatio(snapshot.aspectRatio.coerceAtLeast(5 / 4f))
            .border(width = 1.dp, shape = MaterialTheme.shapes.large, color = Color.White.copy(alpha = 0.2f))
            .clip(MaterialTheme.shapes.large)
            .background(Color.Black),
        contentAlignment = Alignment.Center,
    ) {
        Crossfade(targetState = snapshot.image) {
            if (it != null) {
                Image(bitmap = it.asImageBitmap(), contentDescription = null, modifier = Modifier.fillMaxSize())
            }
        }

        if (snapshot.errorMessage == null && snapshot.image != null) {
            CountdownClock(snapshot = snapshot)
        }

        // Overlay for content
        val showLoading = snapshot.loading && snapshot.image == null
        if (snapshot.canRetry || showLoading) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.Black.copy(alpha = 0.2f))
            )
        }

        // Content
        if (snapshot.canRetry) {
            Icon(
                imageVector = Icons.Rounded.Refresh,
                contentDescription = null,
                modifier = Modifier
                    .clip(CircleShape)
                    .clickable(role = Role.Button) { vm.retry() }
                    .padding(OctoAppTheme.dimens.margin1to2)
            )
        } else if (showLoading) {
            Text(text = stringResource(id = R.string.loading), style = OctoAppTheme.typography.sectionHeader)
        }
    }
}

@Composable
private fun Message(
    background: Color,
    text: String,
    showActionText: String,
    hideActionText: String,
    messageVisible: Boolean,
    content: @Composable () -> Unit,
) {
    var shown by remember { mutableStateOf(false) }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .clip(MaterialTheme.shapes.large)
            .background(background)
            .clickable { shown = !shown },
    ) {
        content()

        LaunchedEffect(key1 = messageVisible) { shown = false }

        AnimatedVisibility(visible = messageVisible) {
            Column {
                AnimatedVisibility(visible = shown) {
                    Text(
                        text = text,
                        style = OctoAppTheme.typography.labelSmall,
                        modifier = Modifier
                            .padding(OctoAppTheme.dimens.margin0to1)
                            .fillMaxWidth(),
                        textAlign = TextAlign.Center,
                    )
                }

                Text(
                    text = if (shown) hideActionText else showActionText,
                    style = OctoAppTheme.typography.buttonSmall,
                    modifier = Modifier
                        .padding(OctoAppTheme.dimens.margin0to1)
                        .fillMaxWidth(),
                    textAlign = TextAlign.Center,
                )
            }
        }
    }
}

@Composable
private fun CountdownClock(snapshot: WebcamViewModel.Snapshot) {
    Box(
        contentAlignment = Alignment.TopEnd,
        modifier = Modifier.fillMaxSize()
    ) {
        var lastTime by remember { mutableStateOf(0L) }
        var lastTarget by remember { mutableStateOf(1f) }
        val target = if (lastTime != snapshot.nextImageAt) {
            @Suppress("UNUSED_VALUE")
            lastTime = snapshot.nextImageAt
            lastTarget = if (lastTarget == 1f) 0f else 1f
            lastTarget
        } else {
            lastTarget
        }

        var now by remember { mutableStateOf(0L) }
        val duration = snapshot.nextImageAt - snapshot.loadedAt
        val left = snapshot.nextImageAt - now

        var progress by remember { mutableStateOf(0f) }
        val p = (left / duration.toFloat()).coerceAtLeast(0f)
        progress = if (target == 0f) p else 1 - p

        CircularProgressIndicator(
            progress = progress,
            indicatorColor = Color.White,
            trackColor = Color.White.copy(alpha = 0.2f),
            strokeWidth = 2.dp,
            modifier = Modifier
                .padding(OctoAppTheme.dimens.margin0to1)
                .clip(CircleShape)
                .background(Color.Black.copy(alpha = 0.5f))
                .padding(OctoAppTheme.dimens.margin0)
                .size(12.dp)
                .scale(scaleX = if (target == 1f) 1f else -1f, scaleY = 1f)
        )

        DisposableEffect(key1 = snapshot.loading) {
            val handler = android.os.Handler(Looper.getMainLooper())
            val runnable = object : Runnable {
                override fun run() {
                    now = System.currentTimeMillis()
                    handler.post(this)
                }
            }

            // Don't animate while loading
            if (!snapshot.loading) {
                runnable.run()
            }

            onDispose {
                handler.removeCallbacks(runnable)
            }
        }
    }
}

@Suppress("UNCHECKED_CAST")
private class WebcamViewModelFactory(val instanceId: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T = WebcamViewModel(instanceId) as T
}

@OptIn(ExperimentalCoroutinesApi::class)
private class WebcamViewModel(private val instanceId: String) : ViewModel() {

    companion object {
        private const val IMAGE_INTERVAL = 5_000L
        private const val IMAGE_SIZE = 1080 / 4
        private var instanceCounter = 0
    }

    private val keyLastAspectRatio = "webcam_aspect_$instanceId"
    private val instanceNumber: Int = instanceCounter++
    private var lastSnapshot = Snapshot(defaultAspectRatio)
    private var lastSnapshotTime = 0L
    private val flowSource = MutableSharedFlow<Unit>(replay = 1)
    val snapshotFlow = flowSource.flatMapLatest {
        flow {
            if (lastSnapshotStillValidFor() > 0L) {
                // Emit last again and wait until we need to reload
                Timber.i("[$instanceNumber] Reusing cached snapshot")
                emit(lastSnapshot)
                delay(lastSnapshotStillValidFor())
            } else {
                // Loading and then delay a bit in case we reload again
                emit(lastSnapshot.copy(loading = true, canRetry = false, errorMessage = null))
            }

            // Actual data
            val param = GetWebcamSnapshotUseCase.Params(instanceId = instanceId, interval = IMAGE_INTERVAL, maxSize = IMAGE_SIZE)
            Timber.i("[$instanceNumber] Beginning to load snapshots")
            val nested = BaseInjector.get().getWebcamSnapshotUseCase2().execute(param).map {
                Snapshot(
                    aspectRatio = it.bitmap.width / it.bitmap.height.toFloat(),
                    image = it.bitmap,
                    loading = false,
                    canRetry = false,
                    advertiseCompanion = it.advertiseCompanion,
                )
            }.onEach {
                defaultAspectRatio = it.aspectRatio
                lastSnapshot = it
                lastSnapshotTime = System.currentTimeMillis()
            }

            emitAll(nested)
        }
    }.catch {
        Timber.e(it, "[$instanceNumber] Too many failures, stopping")
        val message = it.composeErrorMessage().toString()
        emit(lastSnapshot.copy(canRetry = true, errorMessage = message, loading = false))
    }.onStart {
        Timber.i("[$instanceNumber] Starting snapshot webcam")
    }.onCompletion {
        Timber.i("[$instanceNumber] Stopping snapshot webcam")
    }.shareIn(viewModelScope, started = SharingStarted.WhileSubscribed())

    init {
        viewModelScope.launch {
            Timber.i("[$instanceNumber] Create snapshot webcam $instanceId")
            flowSource.emit(Unit)
        }
    }

    var defaultAspectRatio
        get() = BaseInjector.get().sharedPreferences().getFloat(keyLastAspectRatio, 16 / 9f)
        private set(value) = BaseInjector.get().sharedPreferences().edit { putFloat(keyLastAspectRatio, value) }

    private fun lastSnapshotStillValidFor() = lastSnapshotTime - System.currentTimeMillis() + IMAGE_INTERVAL

    fun retry() {
        viewModelScope.launch {
            flowSource.emit(Unit)
        }
    }

    data class Snapshot(
        val aspectRatio: Float,
        val image: Bitmap? = null,
        val imageByteCount: Int? = null,
        val fps: Int? = null,
        val loading: Boolean = true,
        val canRetry: Boolean = false,
        val advertiseCompanion: Boolean = false,
        val nextImageAt: Long = System.currentTimeMillis() + IMAGE_INTERVAL + 200,
        val loadedAt: Long = System.currentTimeMillis(),
        val errorMessage: String? = null,
    )
}