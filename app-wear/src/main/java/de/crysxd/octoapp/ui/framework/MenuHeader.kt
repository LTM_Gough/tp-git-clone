package de.crysxd.octoapp.ui.framework

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.Text
import de.crysxd.octoapp.R
import de.crysxd.octoapp.ui.OctoAppTheme

@Composable
fun MenuHeader(text: String) {
    Text(
        text = text,
        style = OctoAppTheme.typography.sectionHeader,
        textAlign = TextAlign.Center,
        color = MaterialTheme.colors.onBackground.copy(alpha = 0.7f),
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = OctoAppTheme.dimens.margin0to1)
    )
}

fun ScalingLazyListScope.menuHeader() {
    item(key = "menuHeader") {
        MenuHeader(text = stringResource(id = R.string.widget_quick_access))
    }
}