package de.crysxd.octoapp.ui.screens.main.page.menu.cancelobject

import android.content.Intent
import android.os.Parcelable
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ViewComfyAlt
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.wear.compose.material.ScalingLazyListScope
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.ext.requireActivity
import de.crysxd.octoapp.ext.wearCancelObjectFlow
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.MenuItemChip
import kotlinx.coroutines.flow.collectLatest
import kotlinx.parcelize.Parcelize

fun ScalingLazyListScope.cancelObjectMenu(state: () -> CancelObjectState) {
    if (state().hasObjects) {
        item("cancelObject") {
            val activity = LocalContext.current.requireActivity()

            OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Printer) {
                MenuItemChip(
                    icon = Icons.Rounded.ViewComfyAlt,
                    text = stringResource(id = R.string.widget_cancel_object),
                    showChevron = true,
                    onClick = {
                        Intent(activity, CancelObjectMenuActivity::class.java).also {
                            it.putExtras(CancelObjectMenuActivity.Args(state().instanceId).toBundle())
                            activity.startActivity(it)
                        }
                    }
                )
            }
        }
    }
}

@Composable
fun rememberCancelObjectMenuState(instanceId: String): State<CancelObjectState> {
    val state = rememberSaveable { mutableStateOf(CancelObjectState(hasObjects = false, instanceId = instanceId)) }

    LaunchedEffect(Unit) {
        BaseInjector.get().octoPrintProvider().wearCancelObjectFlow(tag = "cancel-object-menu", instanceId = instanceId).collectLatest {
            state.value = CancelObjectState(hasObjects = it.isNotEmpty(), instanceId = instanceId)
        }
    }

    return state
}

@Parcelize
data class CancelObjectState(
    val hasObjects: Boolean,
    val instanceId: String,
) : Parcelable