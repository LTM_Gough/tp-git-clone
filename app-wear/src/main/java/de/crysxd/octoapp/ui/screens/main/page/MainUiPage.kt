package de.crysxd.octoapp.ui.screens.main.page

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.Text
import de.crysxd.octoapp.R
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.MainPageScaffold
import de.crysxd.octoapp.ui.framework.OctoScalingLazyColumn
import de.crysxd.octoapp.ui.framework.headerItem
import de.crysxd.octoapp.ui.framework.menuHeader
import de.crysxd.octoapp.ui.framework.rememberTemperaturesState
import de.crysxd.octoapp.ui.framework.snapshotWebcam
import de.crysxd.octoapp.ui.framework.temperatures
import de.crysxd.octoapp.ui.screens.main.PrepareHeaderContent
import de.crysxd.octoapp.ui.screens.main.PrintHeaderContent
import de.crysxd.octoapp.ui.screens.main.connect.ConnectHeaderContent
import de.crysxd.octoapp.ui.screens.main.page.menu.cancelobject.cancelObjectMenu
import de.crysxd.octoapp.ui.screens.main.page.menu.cancelobject.rememberCancelObjectMenuState
import de.crysxd.octoapp.ui.screens.main.page.menu.gcode.gcodeMenu
import de.crysxd.octoapp.ui.screens.main.page.menu.gcode.rememberGcodeMenuState
import de.crysxd.octoapp.ui.screens.main.page.menu.octoprint.octoPrintMenu
import de.crysxd.octoapp.ui.screens.main.page.menu.octoprint.rememberOctoPrintMenuState
import de.crysxd.octoapp.ui.screens.main.page.menu.power.powerMenu
import de.crysxd.octoapp.ui.screens.main.page.menu.power.rememberPowerMenuState
import de.crysxd.octoapp.ui.screens.main.page.menu.printcontrols.printControlsMenu
import de.crysxd.octoapp.ui.screens.main.page.menu.printcontrols.rememberPrintControlsMenuState
import de.crysxd.octoapp.ui.screens.main.page.menu.temperature.rememberTemperatureMenuState
import de.crysxd.octoapp.ui.screens.main.page.menu.temperature.temperatureMenu
import timber.log.Timber

@Composable
fun MainUiPage(instanceId: String, active: Boolean) {
    val vm = viewModel(
        modelClass = MainPageViewModel::class.java,
        factory = MainPageViewModel.Factory(instanceId),
        key = "main/$instanceId",
    )
    val instance by vm.instanceInformation.collectAsState(initial = null)
    val flags by vm.flags.collectAsState(initial = null)
    val powerMenuState by rememberPowerMenuState(instanceId = instanceId)
    val printState by rememberPrintControlsMenuState(instanceId = instanceId)
    val tempState by rememberTemperaturesState(instanceId = instanceId)
    val current by vm.current.collectAsState(initial = null)
    val cancelObjectMenuState by rememberCancelObjectMenuState(instanceId = instanceId)
    val octoPrintMenuState by rememberOctoPrintMenuState(instanceId = instanceId)
    val temperatureMenuState by rememberTemperatureMenuState(instanceId = instanceId)
    val gcodeMenuState by rememberGcodeMenuState(instanceId = instanceId)

    Box {
        if (active) {
            vm.state.collectAsState(initial = null)
        }
    }

    OctoAppTheme.ForOctoPrint(instance = instance) {
        MainPageScaffold {
            OctoScalingLazyColumn(state = scalingLazyListState) {
                when {
                    flags?.isPrinting() == true -> headerItem(key = "printHeader") {
                        PrintHeaderContent { current }
                    }
                    flags?.isOperational() == true -> headerItem(key = "prepareHeader") {
                        PrepareHeaderContent()
                    }
                    else -> headerItem(key = "connectHeader") {
                        ConnectHeaderContent(active)
                    }
                }

                snapshotWebcam { active }
                divider(0)
                printControlsMenu { printState }
                cancelObjectMenu { cancelObjectMenuState }
                temperatures { tempState }
                divider(1)
                menuHeader()
                temperatureMenu { temperatureMenuState }
                gcodeMenu { gcodeMenuState }
                powerMenu { powerMenuState }
                octoPrintMenu { octoPrintMenuState }
            }
        }

        ConnectionStatus(vm)
    }
}

private fun ScalingLazyListScope.divider(index: Int) = item(key = "divider-$index") { Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin1to2)) }

@Composable
private fun ConnectionStatus(vm: MainPageViewModel) {
    val connection by vm.connection.collectAsState(initial = null)
    Timber.i("Connection: $connection")

    AnimatedVisibility(
        visible = connection == null,
        enter = slideInVertically { it } + fadeIn(),
        exit = slideOutVertically { it } + fadeOut()
    ) {
        Column(
            verticalArrangement = Arrangement.Bottom,
            modifier = Modifier
                .fillMaxSize()
                .shadow(elevation = 10.dp)
        ) {
            Box(
                modifier = Modifier
                    .background(Color.White.copy(alpha = 0.2f))
                    .height(1.dp)
                    .fillMaxSize()
            )
            Text(
                text = stringResource(id = R.string.reconnecting),
                style = OctoAppTheme.typography.label,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .background(colorResource(id = R.color.dark_grey))
                    .padding(
                        start = OctoAppTheme.dimens.margin0to1,
                        end = OctoAppTheme.dimens.margin0to1,
                        bottom = OctoAppTheme.dimens.margin1to2,
                        top = OctoAppTheme.dimens.margin0,
                    )
            )
        }
    }
}