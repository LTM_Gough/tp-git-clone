package de.crysxd.octoapp.ui.screens.main.connect

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.di.WearInjector
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.LocalOctoPrint
import de.crysxd.octoapp.ui.framework.PrimaryButtonChip
import de.crysxd.octoapp.ui.framework.SecondaryButtonChip
import de.crysxd.octoapp.ui.framework.TitleDetails
import de.crysxd.octoapp.utils.ConnectPrinterUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import timber.log.Timber

@Composable
fun ConnectHeaderContent(active: Boolean) {
    val instanceInformation = LocalOctoPrint.current ?: return Timber.e("No OctoPrint!")
    val vm = viewModel<ConnectPrinterViewModel>(factory = ConnectPrinterViewModelFactory(instanceInformation.id))
    val uiState = if (active) {
        vm.uiState.collectAsState(initial = ConnectPrinterUseCase.UiState.Initializing).value
    } else {
        ConnectPrinterUseCase.UiState.Initializing
    }

    Crossfade(targetState = uiState) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier.fillMaxSize()
        ) {
            when (it) {
                ConnectPrinterUseCase.UiState.Initializing -> TitleDetails(
                    title = R.string.connect_printer___searching_for_octoprint_title
                )

                ConnectPrinterUseCase.UiState.OctoPrintNotAvailable -> TitleDetails(
                    title = R.string.connect_printer___octoprint_not_available_title,
                    detail = R.string.connect_printer___octoprint_not_available_detail
                )

                ConnectPrinterUseCase.UiState.OctoPrintStarting -> TitleDetails(
                    title = R.string.connect_printer___octoprint_starting_title,
                )

                ConnectPrinterUseCase.UiState.PrinterConnected -> TitleDetails(
                    title = R.string.connect_printer___printer_connected_title,
                    detail = R.string.connect_printer___printer_connected_detail_1
                )

                ConnectPrinterUseCase.UiState.PrinterConnecting -> TitleDetails(
                    title = R.string.connect_printer___printer_is_connecting_title,
                )

                is ConnectPrinterUseCase.UiState.PrinterOffline -> {
                    val psuSupported = it.psuSupported

                    TitleDetails(
                        title = R.string.connect_printer___printer_offline_title,
                        detail = if (psuSupported) R.string.connect_printer___printer_offline_detail_with_psu else R.string.connect_printer___printer_offline_detail
                    )

                    Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin1))
                    if (psuSupported) {
                        PrimaryButtonChip(
                            text = stringResource(id = R.string.connect_printer___action_cycle_psu),
                            onClick = vm::cyclePsu
                        )
                    } else {
                        PrimaryButtonChip(
                            text = stringResource(id = R.string.connect_printer___action_retry),
                            onClick = vm::retryConnection
                        )
                    }
                }

                ConnectPrinterUseCase.UiState.PrinterPsuCycling -> TitleDetails(
                    title = R.string.connect_printer___psu_cycling_title,
                )

                is ConnectPrinterUseCase.UiState.WaitingForPrinterToComeOnline -> {
                    when (it.psuIsOn) {
                        true -> {
                            TitleDetails(detail = R.string.connect_printer___waiting_for_printer_title)
                            Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin1))
                            SecondaryButtonChip(
                                text = stringResource(id = R.string.connect_printer___action_turn_psu_off),
                                onClick = vm::turnPsuOff
                            )
                        }

                        false -> {
                            TitleDetails(detail = R.string.connect_printer___waiting_for_printer_title)
                            Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin1))
                            PrimaryButtonChip(
                                text = stringResource(id = R.string.connect_printer___action_turn_psu_on),
                                onClick = vm::turnPsuOn
                            )
                        }

                        null -> TitleDetails(title = R.string.connect_printer___waiting_for_printer_title)
                    }
                }

                ConnectPrinterUseCase.UiState.WaitingForUser -> {
                    PrimaryButtonChip(text = stringResource(id = R.string.connect_printer___begin_connection), onClick = vm::beginConnect)
                }
            }
        }
    }
}


@Suppress("UNCHECKED_CAST")
private class ConnectPrinterViewModelFactory(private val instanceId: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>) = ConnectPrinterViewModel(instanceId) as T
}

private class ConnectPrinterViewModel(private val instanceId: String) : ViewModel() {

    companion object {
        private var counter = 0
    }

    private val myCounter = counter++
    private val useCase by lazy { WearInjector.get().connectPrinterUseCase() }
    val uiState: Flow<ConnectPrinterUseCase.UiState> = useCase.executeBlocking(ConnectPrinterUseCase.Params(instanceId = instanceId))
        .onCompletion { Timber.i("[$myCounter] Stop connection attempts on $instanceId") }
        .onStart { Timber.i("[$myCounter] Start connecting on $instanceId") }
        .shareIn(viewModelScope, started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 100))
        .rateLimit(200)

    suspend fun beginConnect() {
        useCase.beginConnect()
    }

    suspend fun turnPsuOn() {
        useCase.turnPsuOn()
    }

    suspend fun turnPsuOff() {
        useCase.turnPsuOff()
    }

    suspend fun cyclePsu() {
        useCase.cyclePsu()
    }

    suspend fun retryConnection() {
        useCase.retryConnectionFromOfflineState()
    }
}

/*
    @Composable
    fun PreviewContainer(content: @Composable () -> Unit) {
        OctoAppTheme.WithTheme {
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .padding(OctoAppTheme.dimens.margin1)
                    .fillMaxSize()
            ) {
                content()
            }
        }
    }
/*
    @Composable
    @Preview(name = "Initial", widthDp = Constants.PREVIEW_SIZE_DP, heightDp = Constants.PREVIEW_SIZE_DP, showBackground = true, backgroundColor = 0xFF000000)
    fun PreviewInitial() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.Initializing
            )
        }
    }

    @Composable
    @Preview(
        name = "Waiting for User",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewWaitingForUser() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.WaitingForUser
            )
        }
    }

    @Composable
    @Preview(
        name = "OctoPrint not available",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewOctoPrintNotAvailable() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.OctoPrintNotAvailable
            )
        }
    }

    @Composable
    @Preview(
        name = "OctoPrint Starting",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewOctoPrintStarting() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.OctoPrintStarting
            )
        }
    }

    @Composable
    @Preview(
        name = "Printer Connected",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewPrinterConnected() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.PrinterConnected
            )
        }
    }

    @Composable
    @Preview(
        name = "PSU Cycling",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewPsuCycling() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.PrinterPsuCycling
            )
        }
    }

    @Composable
    @Preview(
        name = "Printer offline",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewPrinterOffline() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.PrinterOffline(false)
            )
        }
    }

    @Composable
    @Preview(
        name = "Printer offline (PSU)",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewPrinterOfflinePsu() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.PrinterOffline(true)
            )
        }
    }

    @Composable
    @Preview(
        name = "Waiting for printer",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewWaitingForPrinter() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.WaitingForPrinterToComeOnline(null)
            )
        }
    }

    @Composable
    @Preview(
        name = "Waiting for printer (PSU on)",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewWaitingForPrinterPsuOn() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.WaitingForPrinterToComeOnline(true)
            )
        }
    }

    @Composable
    @Preview(
        name = "Waiting for printer (PSU off)",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewWaitingForPrinterPsuOff() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.WaitingForPrinterToComeOnline(false)
            )
        }
    }
}
*/