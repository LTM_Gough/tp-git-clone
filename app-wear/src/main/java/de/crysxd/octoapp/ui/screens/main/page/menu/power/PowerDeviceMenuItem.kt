package de.crysxd.octoapp.ui.screens.main.page.menu.power

import android.content.Intent
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.PowerSettingsNew
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.ext.requireActivity
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.LocalOctoPrint
import de.crysxd.octoapp.ui.framework.MenuItemChip

@Composable
fun PowerDeviceMenuItem(powerDevice: PowerDevice) = LocalOctoPrint.current?.id?.let { instanceId ->
    val activity = LocalContext.current.requireActivity()
    OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Printer) {
        MenuItemChip(
            icon = Icons.Rounded.PowerSettingsNew,
            text = powerDevice.displayName,
            showChevron = true,
            onClick = {
                Intent(activity, PowerMenuActivity::class.java).also {
                    it.putExtras(PowerMenuActivity.Args(uniqueDeviceId = powerDevice.uniqueId, instanceId = instanceId).toBundle())
                }.also {
                    activity.startActivity(it)
                }
            }
        )
    }
}