package de.crysxd.octoapp.ui.screens.main.page.menu.gcode

import android.content.Intent
import android.os.Parcelable
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Code
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.wear.compose.material.ScalingLazyListScope
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.ext.requireActivity
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.MenuItemChip
import kotlinx.coroutines.flow.collectLatest
import kotlinx.parcelize.Parcelize

fun ScalingLazyListScope.gcodeMenu(state: () -> GcodeMenuState) {
    if (state().isVisible) {
        item("gcodeMenu") {
            val activity = LocalContext.current.requireActivity()

            OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Printer) {
                MenuItemChip(
                    icon = Icons.Rounded.Code,
                    text = stringResource(id = R.string.wear_menu___send_gcode),
                    showChevron = true,
                    onClick = {
                        Intent(activity, SendGcodeMenuActivity::class.java).also {
                            it.putExtras(SendGcodeMenuActivity.Args(state().instanceId).toBundle())
                            activity.startActivity(it)
                        }
                    }
                )
            }
        }
    }
}

@Composable
fun rememberGcodeMenuState(instanceId: String): State<GcodeMenuState> {
    val state = rememberSaveable { mutableStateOf(GcodeMenuState(hasGcodeCommands = false, instanceId = instanceId, canSend = false)) }

    LaunchedEffect(Unit) {
        BaseInjector.get().gcodeHistoryRepository().history.collectLatest {
            state.value = state.value.copy(hasGcodeCommands = it.isNotEmpty())
        }
    }

    LaunchedEffect(instanceId) {
        BaseInjector.get().octoPrintProvider().passiveCurrentMessageFlow(tag = "gcode-menu", instanceId = instanceId).collectLatest {
            state.value = state.value.copy(canSend = it.state?.flags?.printing != true && it.state?.flags?.isError() != true)
        }
    }

    return state
}

@Parcelize
data class GcodeMenuState(
    val hasGcodeCommands: Boolean,
    val instanceId: String,
    val canSend: Boolean,
) : Parcelable {
    val isVisible get() = canSend && hasGcodeCommands
}