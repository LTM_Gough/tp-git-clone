package de.crysxd.octoapp.ui.screens.main.page.menu.octoprint

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.OpenInBrowser
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.OpenOctoprintWebUseCase
import de.crysxd.octoapp.ext.openOnPhone
import de.crysxd.octoapp.ext.requireActivity
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.LocalOctoPrint
import de.crysxd.octoapp.ui.framework.MenuItemChip

@Composable
fun OpenOctoPrintMenuItem() = LocalOctoPrint.current?.id?.let { instanceId ->
    val activity = LocalContext.current.requireActivity()
    OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.OctoPrint) {
        MenuItemChip(
            icon = Icons.Rounded.OpenInBrowser,
            text = stringResource(id = R.string.main_menu___item_open_octoprint),
            onClick = {
                val uri = BaseInjector.get().openOctoPrintWebUseCase().execute(OpenOctoprintWebUseCase.Params(instanceId = instanceId))
                requireNotNull(uri)
                uri.openOnPhone(activity)
            }
        )
    }
}