package de.crysxd.octoapp.ui.screens.main.page.menu.octoprint

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Widgets
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.LocalContext
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.base.usecase.ExecuteSystemCommandUseCase
import de.crysxd.octoapp.engine.models.system.SystemCommand
import de.crysxd.octoapp.ext.showSuccess
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.Confirmation
import de.crysxd.octoapp.ui.framework.MenuItemChip
import de.crysxd.octoapp.ui.framework.rememberConfirmationState

@Composable
fun ExecuteSystemCommandMenuItem(cmd: SystemCommand, instanceId: String) {
    val context = LocalContext.current

    val confirmationState by rememberConfirmationState()
    Confirmation(
        state = confirmationState,
        text = cmd.confirmation?.toHtml()?.toString() ?: cmd.name,
        action = {
            BaseInjector.get().executeSystemCommandUseCase().execute(ExecuteSystemCommandUseCase.Params(cmd, instanceId))
            context.showSuccess(cmd.name)
        }
    )

    OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.OctoPrint) {
        MenuItemChip(
            icon = Icons.Default.Widgets,
            text = cmd.name,
            onClick = {
                confirmationState.visible = true
            }
        )
    }
}