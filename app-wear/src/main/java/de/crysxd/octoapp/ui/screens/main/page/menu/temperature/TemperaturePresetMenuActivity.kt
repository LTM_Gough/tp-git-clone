package de.crysxd.octoapp.ui.screens.main.page.menu.temperature

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.LocalFireDepartment
import androidx.compose.material.icons.rounded.MoreVert
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.core.os.bundleOf
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.items
import androidx.wear.compose.material.rememberScalingLazyListState
import de.crysxd.octoapp.OctoPrintInstanceActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.BaseChangeTemperaturesUseCase
import de.crysxd.octoapp.ext.showSuccess
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.MainPageScaffold
import de.crysxd.octoapp.ui.framework.MenuHeader
import de.crysxd.octoapp.ui.framework.OctoScalingLazyColumn
import de.crysxd.octoapp.ui.framework.SplitChip
import kotlinx.parcelize.Parcelize

class TemperaturePresetMenuActivity : OctoPrintInstanceActivity() {

    private val args get() = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))
    override val instanceId get() = args.instanceId
    private val subMenuLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))

        setContent {
            val instance by BaseInjector.get().octorPrintRepository()
                .instanceInformationFlow(args.instanceId).collectAsState(initial = null)

            OctoAppTheme.ForOctoPrint(instance = instance) {
                OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Printer) {
                    MainPageScaffold {
                        OctoScalingLazyColumn(state = rememberScalingLazyListState()) {
                            instance?.let { content(it) }
                        }
                    }
                }
            }
        }
    }

    private fun ScalingLazyListScope.content(instance: OctoPrintInstanceInformationV3) {
        item {
            MenuHeader(text = stringResource(id = R.string.main_menu___item_temperature_presets))
        }

        items(instance.settings?.temperatureProfiles ?: emptyList()) { preset ->
            val temperatures = listOf(
                BaseChangeTemperaturesUseCase.Temperature(component = "tool0", temperature = preset.extruder),
                BaseChangeTemperaturesUseCase.Temperature(component = "tool1", temperature = preset.extruder),
                BaseChangeTemperaturesUseCase.Temperature(component = "tool2", temperature = preset.extruder),
                BaseChangeTemperaturesUseCase.Temperature(component = "tool3", temperature = preset.extruder),
                BaseChangeTemperaturesUseCase.Temperature(component = "bed", temperature = preset.bed),
                BaseChangeTemperaturesUseCase.Temperature(component = "chamber", temperature = preset.chamber),
            )

            val context = LocalContext.current
            SplitChip(
                text = preset.name,
                actionIcon = Icons.Rounded.MoreVert,
                icon = Icons.Rounded.LocalFireDepartment,
                textAction = {
                    BaseInjector.get().setTargetTemperatureUseCase().execute(
                        BaseChangeTemperaturesUseCase.Params(
                            temps = temperatures,
                            instanceId = instance.id,
                        )
                    )
                    showSuccess()
                    finish()
                },
                iconAction = {
                    Intent(context, TemperaturePresetSubMenuActivity::class.java).also {
                        it.putExtras(
                            TemperaturePresetSubMenuActivity.Args(
                                presetName = preset.name,
                                temperatures = temperatures,
                                instanceId = instance.id
                            ).toBundle()
                        )
                        subMenuLauncher.launch(it)
                    }
                },
            )
        }
    }

    @Parcelize
    data class Args(
        val instanceId: String,
    ) : Parcelable {
        companion object {
            fun fromBundle(bundle: Bundle) = bundle.getParcelable<Args>("args")
        }

        fun toBundle() = bundleOf(
            "args" to this
        )
    }
}