package de.crysxd.octoapp.ext

import androidx.compose.runtime.Composable
import androidx.fragment.app.Fragment
import androidx.compose.ui.platform.ComposeView as RealComposeView

fun Fragment.ComposeView(content: @Composable () -> Unit) = RealComposeView(requireContext()).also {
    it.setContent(content)
    it.clipChildren = false
    it.clipToPadding = false
}