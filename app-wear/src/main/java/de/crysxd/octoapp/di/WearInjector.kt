package de.crysxd.octoapp.di

import de.crysxd.octoapp.base.di.BaseComponent

object WearInjector {

    private lateinit var instance: WearComponent

    fun init(baseComponent: BaseComponent) {
        instance = DaggerWearComponent.builder()
            .baseComponent(baseComponent)
            .build()
    }

    fun get(): WearComponent = instance

}