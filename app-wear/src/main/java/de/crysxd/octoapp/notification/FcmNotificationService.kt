package de.crysxd.octoapp.notification

import android.util.Base64
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.FcmPrintEvent
import de.crysxd.octoapp.base.data.models.FcmPrintEvent.Type.Printing
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.utils.AppScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import timber.log.Timber
import java.security.MessageDigest
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class FcmNotificationService : FirebaseMessagingService() {

    companion object {
        // Time in Python format with sec.micros
        private var lastUpdateServerTime = mutableMapOf<String, Double>()
        private var lastEventServerTime = mutableMapOf<String, Double>()
    }

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Timber.e(throwable)
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Timber.i("New token: $token")
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        try {
            message.data["raw"]?.let {
                handleRawDataEvent(
                    instanceId = message.data["instanceId"] ?: throw IllegalArgumentException("Not instance id"),
                    raw = it,
                )
            }
        } catch (e: Exception) {
            Timber.e(e, "Error handling FCM push notification")
        }
    }

    private fun handleRawDataEvent(instanceId: String, raw: String) = AppScope.launch(exceptionHandler) {
        Timber.i("Received message with raw data for instance: $instanceId")

        // Check if for active instance or feature enabled
        val isForActive = BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()?.id == instanceId
        if (!isForActive && !BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH)) {
            Timber.i("Dropping message for $instanceId as it's not the active instance and quick switch is disabled")
            return@launch
        }

        // Decrypt and decode data
        val key = BaseInjector.get().octorPrintRepository().get(instanceId)?.settings?.plugins?.octoAppCompanion?.encryptionKey
            ?: return@launch Timber.w("No encryption key present")
        val decrypted = AESCipher(key).decrypt(raw)
        val data = Gson().fromJson(String(decrypted), FcmPrintEvent::class.java)
        Timber.i("Data: ${data.type} => $data")

        // Ensure we don't post old data
        val serverTime = data.serverTimePrecise ?: data.serverTime ?: 0.0
        val isEvent = data.type != Printing
        fun doTimeCheck(map: MutableMap<String, Double>): Boolean {
            val previousLast = map[instanceId] ?: 0.0
            return if (previousLast > serverTime) {
                Timber.i("Skipping update, last server time was $previousLast which is after $serverTime")
                false
            } else {
                Timber.i("$serverTime after $previousLast")
                map[instanceId] = serverTime
                true
            }
        }

        val useNotification = if (isEvent) doTimeCheck(lastEventServerTime) else doTimeCheck(lastUpdateServerTime)
        if (!useNotification) return@launch

        // Handle event
        FcmPrintEventReceiver.sendToAll(instanceId = instanceId, fcmPrintEvent = data)
    }

    private class AESCipher(private val key: String) {

        private fun createDecryptCipher(ivBytes: ByteArray): Cipher {
            val c = Cipher.getInstance("AES/CBC/PKCS7Padding")
            val sk = SecretKeySpec(key.getSha256(), "AES")
            val iv = IvParameterSpec(ivBytes)
            c.init(Cipher.DECRYPT_MODE, sk, iv)
            return c
        }

        fun decrypt(data: String): ByteArray {
            val bytes = Base64.decode(data, Base64.DEFAULT)
            val ivBytes = bytes.take(16).toByteArray()
            val rawDataBytes = bytes.drop(16).toByteArray()
            val cipher = createDecryptCipher(ivBytes)
            return cipher.doFinal(rawDataBytes)
        }

        private fun String.getSha256(): ByteArray {
            val digest = MessageDigest.getInstance("SHA-256").also { it.reset() }
            return digest.digest(this.toByteArray())
        }
    }
}