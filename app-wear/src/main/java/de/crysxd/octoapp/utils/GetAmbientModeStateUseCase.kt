package de.crysxd.octoapp.utils

import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.UseCase
import de.crysxd.octoapp.engine.exceptions.PrinterNotOperationalException
import de.crysxd.octoapp.engine.models.event.HistoricTemperatureData
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.printer.PrinterState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.supervisorScope
import kotlinx.coroutines.withContext
import kotlinx.datetime.Clock
import timber.log.Timber
import javax.inject.Inject

class GetAmbientModeStateUseCase @Inject constructor(
    val octoPrintProvider: OctoPrintProvider,
) : UseCase<GetAmbientModeStateUseCase.Params, Message.Current>() {

    override suspend fun doExecute(param: Params, timber: Timber.Tree) = withContext(Dispatchers.IO) {
        supervisorScope {
            // Get status
            val octoPrint = octoPrintProvider.octoPrint(param.instanceId)
            val deferredJob = async { octoPrint.jobApi.getJob() }
            val deferredTime = async {
                try {
                    octoPrint.systemApi.getSystemInfo().generatedAt
                } catch (e: Exception) {
                    Timber.w(e, "Failed to get time from server, might be missing permissions")
                    Clock.System.now()
                }
            }
            val deferredStatus = async {
                try {
                    octoPrint.printerApi.getPrinterState()
                } catch (e: PrinterNotOperationalException) {
                    null
                }
            }
            val job = deferredJob.await()
            val status = deferredStatus.await()
            val time = deferredTime.await()

            // Create message
            Message.Current(
                logs = emptyList(),
                temps = listOf(HistoricTemperatureData(time = time.toEpochMilliseconds(), components = status?.temperature ?: emptyMap())),
                progress = job.progress,
                job = job.info,
                offsets = null,
                serverTime = time.toEpochMilliseconds(),
                state = status?.state ?: PrinterState.State(),
                isHistoryMessage = false,
                originHash = 0,
            )
        }
    }

    data class Params(
        val instanceId: String,
    )
}