package de.crysxd.octoapp

private val ktorVersion2: String
    get() {
        val ext = project.rootProject.ext
        return ext.get("ktor_version") as String
    }


object Dependencies2 {
    fun ktorDarwin() {
        implementation("io.ktor:ktor-client-darwin:$ktorVersion2")
    }

    fun ktorAndroid() {
        implementation("io.ktor:ktor-client-okhttp:$ktorVersion2")
        implementation("com.squareup.okhttp3:logging-interceptor:4.9.2")
    }


    fun ktorCommon() {
        api("io.ktor:ktor-client-core:$ktorVersion2")
        api("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion2")
        api("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.0")


        implementation("io.ktor:ktor-client-content-negotiation:$ktorVersion2")
        implementation("io.ktor:ktor-client-logging:$ktorVersion2")
        implementation("io.ktor:ktor-client-mock:$ktorVersion2")
        implementation("io.ktor:ktor-client-auth:$ktorVersion2")
        implementation("io.ktor:ktor-client-encoding:$ktorVersion2")
        implementation("io.ktor:ktor-client-websockets:$ktorVersion2")
    }
}

