-keepclassmembers class de.crysxd.octoapp.** { *; }
-keep class com.github.druk.dnssd.** { *; }

# Please add these rules to your existing keep rules in order to suppress warnings.
# This is generated automatically by the Android Gradle plugin.
-dontwarn org.bouncycastle.jsse.BCSSLParameters
-dontwarn org.bouncycastle.jsse.BCSSLSocket
-dontwarn org.bouncycastle.jsse.provider.BouncyCastleJsseProvider
-dontwarn org.conscrypt.Conscrypt$Version
-dontwarn org.conscrypt.Conscrypt
-dontwarn org.conscrypt.ConscryptHostnameVerifier
-dontwarn org.openjsse.javax.net.ssl.SSLParameters
-dontwarn org.openjsse.javax.net.ssl.SSLSocket
-dontwarn org.openjsse.net.ssl.OpenJSSE
-dontwarn sun.reflect.ReflectionFactory

# Firebase
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception

# Keep all names, we don't need to obfuscate
-keepnames class ** { *; }

# Kotlinx serialization
-keepattributes *Annotation*, InnerClasses
-dontnote kotlinx.serialization.SerializationKt
-keep,includedescriptorclasses class de.crysxd.octoapp.**$$serializer { *; }
-keepclassmembers class de.crysxd.octoapp.** {
    *** Companion;
}
-keepclasseswithmembers class de.crysxd.octoapp.** {
    kotlinx.serialization.KSerializer serializer(...);
}