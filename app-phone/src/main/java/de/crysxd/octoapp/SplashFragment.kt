package de.crysxd.octoapp

import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import de.crysxd.baseui.ext.requireOctoActivity
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay

class SplashFragment : Fragment(R.layout.splash_fragment) {

    private var job: Job? = null

    override fun onResume() {
        super.onResume()
        requireOctoActivity().octo.isVisible = false

        job?.cancel()
        job = lifecycleScope.launchWhenCreated {
            delay(3000)
            throw IllegalStateException("Splash screen active for 3s, app is stuck!!")
        }
    }

    override fun onPause() {
        super.onPause()
        job?.cancel()
    }
}