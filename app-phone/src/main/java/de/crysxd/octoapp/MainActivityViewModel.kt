package de.crysxd.octoapp

import android.net.Uri
import androidx.lifecycle.ViewModel
import de.crysxd.octoapp.engine.models.connection.ConnectionType
import de.crysxd.octoapp.engine.models.printer.PrinterState
import timber.log.Timber

class MainActivityViewModel : ViewModel() {
    var lastSuccessfulCapabilitiesUpdate = 0L
    var pendingUri: Uri? = null
    var lastNavigation: Navigation? = null
    var lastWebUrlAndApiKey: String? = "initial"
    var pendingNavigation: Navigation? = null
    var pendingFlagNavigation: Navigation? = null
    var connectionType: ConnectionType? = null
        set(value) {
            previousConnectionType = field
            Timber.i("Connection type now $value, was $field")
            field = value
        }
    var previousConnectionType: ConnectionType? = null
    var lastFlags: PrinterState.Flags? = null
    var sameFlagsCounter = 0

    sealed class Navigation(val action: Int) {
        object SignIn : Navigation(R.id.action_sign_in_required)
        object Connect : Navigation(R.id.action_connect_printer)
        object Prepare : Navigation(R.id.action_show_controls)
        object Print : Navigation(R.id.action_show_controls)
    }
}