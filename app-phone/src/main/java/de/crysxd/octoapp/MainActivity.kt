package de.crysxd.octoapp

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.view.WindowInsetsControllerCompat
import androidx.core.view.doOnNextLayout
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.map
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.transition.ChangeBounds
import androidx.transition.Explode
import androidx.transition.Fade
import androidx.transition.TransitionManager
import androidx.transition.TransitionSet
import androidx.viewpager2.widget.ViewPager2
import com.google.firebase.analytics.FirebaseAnalytics
import de.crysxd.baseui.InsetAwareScreen
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.common.OctoToolbar
import de.crysxd.baseui.common.OctoView
import de.crysxd.baseui.common.controlcenter.ControlCenterFragment
import de.crysxd.baseui.common.controlcenter.ControlCenterHostLayout
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.utils.ColorTheme
import de.crysxd.baseui.utils.colorTheme
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.billing.BillingEvent
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase.Companion.OCTOEVERYWHERE_APP_PORTAL_CALLBACK_PATH
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase.Companion.SPAGHETTI_DETECTIVE_APP_PORTAL_CALLBACK_PATH
import de.crysxd.octoapp.base.usecase.UpdateInstanceCapabilitiesUseCase
import de.crysxd.octoapp.base.utils.BenchmarkIntentHandler
import de.crysxd.octoapp.databinding.MainActivityBinding
import de.crysxd.octoapp.engine.exceptions.WebSocketUpgradeFailedException
import de.crysxd.octoapp.engine.models.connection.ConnectionQuality
import de.crysxd.octoapp.engine.models.connection.ConnectionType
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.notification.LiveNotificationManager
import de.crysxd.octoapp.pluginsupport.mmu2filamentselect.Mmu2FilamentSelectSupportFragment
import de.crysxd.octoapp.pluginsupport.ngrok.NgrokSupportFragment
import de.crysxd.octoapp.pluginsupport.octolapse.OctolapseSupportFragment
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.signin.di.SignInInjector
import de.crysxd.octoapp.widgets.updateAllWidgets
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.isActive
import timber.log.Timber
import de.crysxd.octoapp.engine.models.event.Message as SocketMessage

class MainActivity : OctoActivity() {

    companion object {
        const val EXTRA_TARGET_OCTOPRINT_ID = "octoprint_id"
        const val EXTRA_CLICK_URI = "clickUri"
    }

    private lateinit var binding: MainActivityBinding
    private val preferences = BaseInjector.get().octoPreferences()
    private val viewModel by lazy { ViewModelProvider(this)[MainActivityViewModel::class.java] }
    private val lastInsets = Rect()
    override val octoToolbar: OctoToolbar by lazy { binding.toolbar }
    override val controlCenter: ControlCenterHostLayout by lazy { binding.root }
    override val octo: OctoView by lazy { binding.toolbarOctoView }
    override val rootLayout by lazy { binding.coordinator }
    override val navController get() = findNavController(R.id.mainNavController)
    private var enforceAutoamticNavigationAllowed = false
    private var uiStoppedAt: Long? = null
    private var updateCapabilitiesJob: Job? = null
    private var bannerColorAnimator: ValueAnimator? = null
    private var bannerHeightJob: Job? = null
    private val handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        delegate.applyDayNight()

        // We need to call this before nav component grabs any link. onNewIntent
        // handles any links and removes them from the intent
        onNewIntent(intent)

        binding = MainActivityBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        binding.root.controlCenterFragment = ControlCenterFragment()
        binding.root.fragmentManager = supportFragmentManager

        // Fix fullscreen layout under system bars for frame layout
        rootLayout.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)

        // Observe events, will update when instance changes
        BaseInjector.get().octoPrintProvider().eventFlow("MainActivity@events")
            .asLiveData()
            .map { it }
            .observe(this, ::onEventReceived)

        BaseInjector.get().octoPrintProvider().passiveCurrentMessageFlow("MainActivity@currentMessage")
            .asLiveData()
            .map { it }
            .observe(this, ::onCurrentMessageReceived)

        SignInInjector.get().octoprintRepository().instanceInformationFlow()
            .distinctUntilChangedBy { it?.settings?.appearance?.color }
            .asLiveData()
            .observe(this) {
                ColorTheme.applyColorTheme(it.colorTheme)
            }

        lifecycleScope.launchWhenStarted {
            val navHost = supportFragmentManager.findFragmentById(R.id.mainNavController) as NavHostFragment

            navController.addOnDestinationChangedListener { _, destination, _ ->
                Timber.i("Navigated to ${destination.label}")

                // Sometimes it seems the UI is restored but the VM still remembers the last navigation, causing the user to be stuck on splash
                // Whenever we are on splash, we remove lastNavigation so the user is properly navigated again
                if (destination.id == R.id.splashFragment) {
                    Timber.i("Restoring navigation to ${viewModel.lastNavigation} or ${viewModel.pendingFlagNavigation}")
                    viewModel.pendingNavigation = viewModel.lastNavigation ?: viewModel.pendingFlagNavigation
                    viewModel.lastNavigation = null
                }

                OctoAnalytics.logEvent(OctoAnalytics.Event.ScreenShown, mapOf(FirebaseAnalytics.Param.SCREEN_NAME to destination.label?.toString()))
                pushBannerHeightToScreen()

                when (destination.id) {
                    R.id.discoverFragment -> OctoAnalytics.logEvent(OctoAnalytics.Event.LoginWorkspaceShown)
                    R.id.workspaceConnect -> OctoAnalytics.logEvent(OctoAnalytics.Event.ConnectWorkspaceShown)
                    R.id.workspacePrePrint -> OctoAnalytics.logEvent(OctoAnalytics.Event.PrePrintWorkspaceShown)
                    R.id.workspacePrint -> OctoAnalytics.logEvent(OctoAnalytics.Event.PrintWorkspaceShown)
                    R.id.terminalFragment -> OctoAnalytics.logEvent(OctoAnalytics.Event.TerminalWorkspaceShown)
                }

                viewModel.pendingNavigation?.let {
                    viewModel.pendingNavigation = null
                    navigate(it)
                }

                applySystemBarColors()
            }

            navHost.childFragmentManager.registerFragmentLifecycleCallbacks(
                object : FragmentManager.FragmentLifecycleCallbacks() {
                    override fun onFragmentResumed(fm: FragmentManager, f: Fragment) {
                        super.onFragmentResumed(fm, f)
                        applyInsetsToScreen(f)
                    }
                },
                false
            )

            // Listen for inset changes and store them
            window.decorView.setOnApplyWindowInsetsListener { _, insets ->
                lastInsets.top = insets.systemWindowInsetTop
                lastInsets.left = insets.systemWindowInsetLeft
                lastInsets.bottom = insets.systemWindowInsetBottom
                lastInsets.right = insets.systemWindowInsetRight
                applyInsetsToCurrentScreen()
                pushBannerHeightToScreen()
                insets.consumeSystemWindowInsets()
            }
        }

        prepareTablet()
        observeActiveInstance()
        attachSupportFragments()
        observePreferences()
    }

    private fun observePreferences() = lifecycleScope.launchWhenCreated {
        preferences.updatedFlow.collectLatest {
            setInstanceTitle(BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot())
        }
    }

    private fun prepareTablet() {
        if (!isTablet() && !preferences.allowAppRotation) {
            // Stop screen rotation on phones
            @SuppressLint("SourceLockedOrientationActivity")
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
    }

    private fun attachSupportFragments() {
        if (supportFragmentManager.findFragmentByTag("octolapse-support") != null) {
            Timber.i("Skipping support fragment creation, already attached")
        } else {
            Timber.i("Attaching support fragments")
            supportFragmentManager.beginTransaction()
                .add(OctolapseSupportFragment(), "octolapse-support")
                .add(Mmu2FilamentSelectSupportFragment(), "mmu2-select-filament-support")
                .add(NgrokSupportFragment(), "ngrok-support")
                .commit()
        }
    }

    private fun observeActiveInstance() = lifecycleScope.launchWhenResumed {
        SignInInjector.get().octoprintRepository().instanceInformationFlow().onEach {
            setInstanceTitle(it)
        }.filter {
            val webUrlAndApiKey = "${it?.webUrl}:${it?.apiKey}"
            val pass = viewModel.lastWebUrlAndApiKey != webUrlAndApiKey
            viewModel.lastWebUrlAndApiKey = webUrlAndApiKey
            Timber.i("Instance information filter $it => $pass")
            pass
        }.collect { instance ->
            when {
                instance != null && instance.apiKey.isNotBlank() -> {
                    Timber.i("Instance information received")
                    updateCapabilities("instance_change", updateM115 = true, escalateError = false)


                    // Go to connect screen if not yet connected
                    if (BaseInjector.get().octoPrintProvider().getLastCurrentMessage(instance.id) == null) {
                        Timber.i("Not connected, moving to connect state")
                        navigate(MainActivityViewModel.Navigation.Connect)
                    } else {
                        Timber.i("Already connected")
                    }

                    viewModel.pendingUri?.let {
                        viewModel.pendingUri = null
                        handleDeepLink(it)
                    }
                }

                else -> {
                    Timber.i("No instance active $this")
                    navigate(MainActivityViewModel.Navigation.SignIn)
                    LiveNotificationManager.stop(this@MainActivity)
                }
            }
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onNewIntent(intent: Intent?) {
        Timber.i("Handling new intent")
        BenchmarkIntentHandler.handle(intent)

        // Handle switch requests
        if (BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH)) {
            intent?.getStringExtra(EXTRA_TARGET_OCTOPRINT_ID)?.let { id ->
                Timber.w("Received intent to activate $id")
                val repo = BaseInjector.get().octorPrintRepository()
                repo.get(id)?.let {
                    repo.setActive(it, trigger = "Intent")
                }
            }
        }

        // Handle deep links
        val uri = intent?.data ?: intent?.getStringExtra(EXTRA_CLICK_URI)?.let { Uri.parse(it) }
        uri?.let {
            try {
                Timber.i("Handling click URI: $it")
                if (it.host == "app.octoapp.eu" || it.host == "test.octoapp.eu") {
                    // Give a second for everything to settle
                    handleDeepLink(it)
                } else {
                    Timber.w("Dropping URI, host is ${it.host}")
                }
            } catch (e: Exception) {
                Timber.e(e)
            }
        }

        // Clean data so the nav component doesn't grab the link. We need to do this manually honoring the app state
        intent?.data = null
        intent?.removeExtra(EXTRA_TARGET_OCTOPRINT_ID)
        intent?.removeExtra(EXTRA_CLICK_URI)
        super.onNewIntent(intent)
    }

    private fun handleDeepLink(uri: Uri) {
        Timber.i("Hanlding deep link2")

        lifecycleScope.launchWhenResumed {
            // Add minimal delay to ensure app is ready
            delay(timeMillis = 300)

            try {
                if (UriLibrary.isActiveInstanceRequired(uri.toString().toUrl()) && BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot() == null) {
                    Timber.i("Uri requires active instance, delaying")
                    viewModel.pendingUri = uri
                } else {
                    Timber.i("Hanlding deep link")
                    val url = uri.toString().toUrl()
                    when (url.pathSegments.firstOrNull { it.isNotBlank() }) {
                        OCTOEVERYWHERE_APP_PORTAL_CALLBACK_PATH -> BaseInjector.get().handleOctoEverywhereAppPortalSuccessUseCase().execute(url)
                        SPAGHETTI_DETECTIVE_APP_PORTAL_CALLBACK_PATH -> BaseInjector.get().handleSpaghettiDetectiveAppPortalSuccessUseCase().execute(url)
                        else -> {
                            Timber.i("Handling generic URI: $uri")
                            uri.open(this@MainActivity)
                        }
                    }
                }
            } catch (e: Exception) {
                showDialog(e)
            }
        }
    }

    private fun isTablet() = ((this.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE)

    private fun isLandscape() = resources.getBoolean(R.bool.landscape)

    private fun isNightMode() = resources.getBoolean(R.bool.night_mode)

    private fun applyInsetsToCurrentScreen() = findCurrentScreen()?.let { applyInsetsToScreen(it) }

    private fun findCurrentScreen() = supportFragmentManager.findFragmentById(R.id.mainNavController)?.childFragmentManager?.fragments?.firstOrNull()

    private fun applyInsetsToScreen(screen: Fragment, topOverwrite: Int? = null) {
        val bannerHeight = binding.bannerView.height.takeIf { binding.bannerView.isVisible && it > 0 }
        Timber.v("Applying insets: bannerView=$bannerHeight topOverwrite=$topOverwrite screen=$screen")
        binding.toolbar.updateLayoutParams<FrameLayout.LayoutParams> { topMargin = topOverwrite ?: bannerHeight ?: lastInsets.top }
        octo.updateLayoutParams<FrameLayout.LayoutParams> { topMargin = topOverwrite ?: bannerHeight ?: lastInsets.top }

        if (screen is InsetAwareScreen) {
            fun push() = screen.handleInsets(
                Rect(
                    lastInsets.left,
                    topOverwrite ?: bannerHeight ?: lastInsets.top,
                    lastInsets.right,
                    lastInsets.bottom,
                )
            )

            screen.view?.let { push() } ?: handler.postDelayed({ push() }, 100)
        } else {
            screen.view?.let { applyInsetsToView(it) }
            screen.view?.updatePadding(top = topOverwrite ?: bannerHeight ?: screen.view?.paddingTop ?: 0)
        }
    }

    override fun applyInsetsToView(view: View) =
        view.updatePadding(top = lastInsets.top, bottom = lastInsets.bottom, left = lastInsets.left, right = lastInsets.right)


    override fun onStart() {
        super.onStart()
        Timber.i("UI started")

        val stoppedAt = uiStoppedAt
        if (stoppedAt != null && (System.currentTimeMillis() - stoppedAt) > 30_000) {
            // OctoPrint might not be available, this is more like a fire and forget
            // Don't bother user with error messages
            updateCapabilities("ui_start", updateM115 = false, escalateError = false)
        } else {
            Timber.i("Ui stopped for less than 30s, skipping capabilities update")
        }

        updateConnectionBanner(alreadyShrunken = true)
    }

    override fun onStop() {
        super.onStop()
        val now = System.currentTimeMillis()
        uiStoppedAt = now
        Timber.i("UI stopped")
    }

    override fun onResume() {
        super.onResume()
        BaseInjector.get().octoPreferences().wasPrintNotificationDisabledUntilNextLaunch = false
        BillingManager.onResume()
        lifecycleScope.launchWhenResumed {
            BillingManager.billingEventFlow().collectLatest {
                it.consume { event ->
                    when (event) {
                        BillingEvent.PurchaseCompleted -> de.crysxd.baseui.purchase.PurchaseConfirmationDialog()
                            .show(supportFragmentManager, "purchase-confirmation")
                    }
                }
            }
        }

        bannerHeightJob = lifecycleScope.launchWhenResumed {
            // The banner sometimes is a bit glitchy. Push the height regularly to correct for this and then compose it soon.
            while (isActive) {
                pushBannerHeightToScreen()
                delay(2500)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        bannerHeightJob?.cancel()
        BillingManager.onPause()
    }

    override fun enforceAllowAutomaticNavigationFromCurrentDestination() {
        Timber.i("Enforcing auto navigation for the next navigation event")
        enforceAutoamticNavigationAllowed = true
    }

    private fun navigate(destination: MainActivityViewModel.Navigation?) {
        val destinationId = destination?.action
        if (destinationId != null && (destinationId != viewModel.lastNavigation?.action || enforceAutoamticNavigationAllowed)) {
            // Screens which must be/can be closed automatically when the state changes
            // Other screens will stay open and we navigate to the new state-based destination after the
            // current screen is closed
            val currentDestination = navController.currentDestination?.id
            val currentDestinationAllowsAutoNavigate = listOf(
                R.id.splashFragment,
                R.id.discoverFragment,
                R.id.requestAccessFragment,
                R.id.signInSuccessFragment,
                R.id.workspaceConnect,
                R.id.workspacePrePrint,
                R.id.workspacePrint,
                R.id.terminalFragment,
                R.id.controlsFragment,
            ).contains(currentDestination)
            val destinationName = currentDestination?.let(resources::getResourceEntryName)

            if (currentDestinationAllowsAutoNavigate || enforceAutoamticNavigationAllowed) {
                Timber.v("Navigating to $destinationName (currentDestinationAllowsAutoNavigate=$currentDestinationAllowsAutoNavigate enforceAutoamticNavigationAllowed=$enforceAutoamticNavigationAllowed)")
                enforceAutoamticNavigationAllowed = false
                viewModel.lastNavigation = destination
                navController.navigate(destinationId)
            } else {
                Timber.v("Current destination $destinationName does not allow auto navigate, storing navigation action as pending")
                viewModel.pendingNavigation = destination
            }
        } else {
            Timber.v("Skipping duplicate navigation: $destination != ${viewModel.lastNavigation}")
        }
    }

    private fun onEventReceived(e: Event) = when (e) {
        // Only show errors if we are not already in disconnected screen. We still want to show the stall warning to indicate something is wrong
        // as this might lead to the user being stuck
        is Event.Disconnected -> {
            Timber.w("Connection lost")
            when {
                e.exception is WebSocketUpgradeFailedException -> e.exception?.let(this::showDialog)
                !listOf(MainActivityViewModel.Navigation.Connect, MainActivityViewModel.Navigation.SignIn).contains(viewModel.lastNavigation) ->
                    showBanner(R.string.main___banner_connection_lost_reconnecting, 0, R.color.yellow, showSpinner = true, alreadyShrunken = false)
                else -> Unit
            }
        }

        is Event.Connected -> {
            Timber.w("Connection restored")
            updateAllWidgets()
            viewModel.connectionType = e.connectionType
            updateConnectionBanner(alreadyShrunken = false)

            (e.connectionQuality as? ConnectionQuality.Degraded)?.let {
                showDialog(it.reason)
            }

            // Start LiveNotification again, might have been stopped!
            LiveNotificationManager.restartIfWasPaused(this)
        }

        is Event.MessageReceived -> {
            if (viewModel.connectionType == ConnectionType.Default) {
                setBannerVisible(false)
            }

            onMessageReceived(e.message)
        }

        else -> Unit
    }

    private fun updateConnectionBanner(alreadyShrunken: Boolean) {
        when (viewModel.connectionType) {
            null -> setBannerVisible(false)

            ConnectionType.Default -> if (viewModel.previousConnectionType != null && viewModel.previousConnectionType != ConnectionType.Default) {
                // If we switched back from a alternative connection to primary, show banner
                showBanner(
                    R.string.main___banner_connected_via_local,
                    null,
                    R.color.green,
                    showSpinner = false,
                    alreadyShrunken = false,
                    doOnShrink = { setBannerVisible(false) }
                )
            } else {
                Timber.d("Previous connection type was ${viewModel.previousConnectionType}, not showing local banner")
                setBannerVisible(false)
            }

            ConnectionType.DefaultCloud -> showBanner(
                R.string.main___banner_connected_via_alternative,
                R.drawable.ic_round_cloud_queue_24,
                R.color.blue,
                showSpinner = false,
                alreadyShrunken = alreadyShrunken
            )

            ConnectionType.Tailscale -> showBanner(
                R.string.main___banner_connected_via_tailscale,
                R.drawable.ic_tailscale_24px,
                R.color.tailscale,
                showSpinner = false,
                alreadyShrunken = alreadyShrunken
            )

            ConnectionType.Ngrok -> showBanner(
                R.string.main___banner_connected_via_ngrok,
                R.drawable.ic_ngrok_24px,
                R.color.ngrok,
                showSpinner = false,
                alreadyShrunken = alreadyShrunken
            )

            ConnectionType.OctoEverywhere -> showBanner(
                R.string.main___banner_connected_via_octoeverywhere,
                R.drawable.ic_octoeverywhere_24px,
                R.color.octoeverywhere,
                showSpinner = false,
                alreadyShrunken = alreadyShrunken
            )

            ConnectionType.SpaghettiDetective -> showBanner(
                R.string.main___banner_connected_via_spaghetti_detective,
                R.drawable.ic_spaghetti_detective_24px,
                R.color.spaghetti_detective,
                showSpinner = false,
                alreadyShrunken = alreadyShrunken
            )
        }.hashCode()
    }

    private fun onMessageReceived(e: SocketMessage) = when (e) {
        is SocketMessage.Current -> onCurrentMessageReceived(e)
        is SocketMessage.Event -> onEventMessageReceived(e)
        is SocketMessage.Connected -> {
            // We are connected, let's update the available capabilities of the connect Octoprint
            if ((System.currentTimeMillis() - viewModel.lastSuccessfulCapabilitiesUpdate) > 10000) {
                updateCapabilities("connected_event")
            } else Unit
        }
        else -> Unit
    }

    private fun onCurrentMessageReceived(e: SocketMessage.Current) {
        Timber.tag("navigation").v(e.state?.flags.toString())
        val flags = e.state?.flags
        val lastFlags = viewModel.lastFlags
        viewModel.lastFlags = flags
        if (flags == lastFlags) {
            viewModel.sameFlagsCounter++
        } else {
            viewModel.sameFlagsCounter = 0
        }

        val navigation = when {
            // We encountered an error, try reconnecting
            flags == null || flags.isError() -> {
                LiveNotificationManager.stop(this)
                MainActivityViewModel.Navigation.Connect
            }

            // We are printing
            flags.isPrinting() -> {
                try {
                    LiveNotificationManager.start(this)
                } catch (e: IllegalStateException) {
                    // User might have closed app just in time so we can't start the service
                }
                MainActivityViewModel.Navigation.Print
            }

            // We are connected
            flags.isOperational() -> {
                LiveNotificationManager.stop(this)
                MainActivityViewModel.Navigation.Prepare
            }

            !flags.isOperational() && !flags.isPrinting() -> {
                LiveNotificationManager.stop(this)
                MainActivityViewModel.Navigation.Connect
            }

            // Fallback
            else -> viewModel.lastNavigation
        }

        // Sometimes when changing e.g. from paused to printing OctoPrint sends one wrong set of flags, so we
        // only continue if last and current are the same
        // If we have closed or error, it's always instant
        if ((viewModel.sameFlagsCounter < 3 || lastFlags == null) && flags?.closedOrError != true) {
            viewModel.pendingFlagNavigation = navigation
            return Timber.d("Skipping flag navigation, recently changed and waiting for confirmation before performing: $navigation")
        }

        if ((viewModel.sameFlagsCounter == 3 && lastFlags != null) && flags?.closedOrError != true) {
            Timber.i("Performing flag navigation to $navigation: $flags")
        }

        navigate(navigation)
    }

    private fun onEventMessageReceived(e: SocketMessage.Event) = when (e) {
        is SocketMessage.Event.PrinterConnected,
        is SocketMessage.Event.PrinterProfileModified,
        is SocketMessage.Event.SettingsUpdated -> {
            // New printer connected or settings updated, let's update capabilities
            updateCapabilities("settings_updated", updateM115 = false)
        }
        else -> Unit
    }

    private fun showBanner(
        @StringRes text: Int,
        @DrawableRes icon: Int?,
        @ColorRes background: Int,
        showSpinner: Boolean,
        alreadyShrunken: Boolean,
        doOnShrink: () -> Unit = {}
    ) {
        binding.connectionBanner.show(this, text, icon, background, showSpinner, alreadyShrunken, doOnShrink)
        setBannerVisible(true)
    }

    private fun setInstanceTitle(instance: OctoPrintInstanceInformationV3?) {
        runBannerTransition()
        val title = instance?.label?.takeIf { preferences.showActiveInstanceInStatusBar }
        val show = !title.isNullOrBlank()
        Timber.d("Title visible: $title (show=${preferences.showActiveInstanceInStatusBar})")

        binding.instanceBanner.text = title
        binding.instanceBanner.isVisible = !title.isNullOrBlank()

        val colorTo = instance?.colorTheme?.dark?.takeIf { show } ?: Color.TRANSPARENT
        val colorFrom = (binding.instanceBanner.background as? ColorDrawable)?.color ?: Color.TRANSPARENT
        bannerColorAnimator?.cancel()
        bannerColorAnimator = ValueAnimator.ofArgb(colorFrom, colorTo).apply {
            addUpdateListener {
                binding.instanceBanner.setBackgroundColor(it.animatedValue as Int)
                binding.bannerView.setBackgroundColor(it.animatedValue as Int)
            }
            start()
        }

        pushBannerHeightToScreen()
    }

    private fun setBannerVisible(visible: Boolean) {
        // Not visible and we should not be visible? Nothing to do.
        // If we are visible or should be visible, we need to update height as insets might have changed
        if (!binding.connectionBanner.isVisible && !visible) {
            return
        }

        Timber.d("Banner visible: $visible")

        // Not layed out yet? Do later
        if (rootLayout.width == 0) {
            rootLayout.doOnNextLayout { setBannerVisible(visible) }
            return
        }

        // Shrinking after delay
        runBannerTransition()
        binding.connectionBanner.onStartShrink = {
            runBannerTransition()
            binding.connectionBanner.doOnNextLayout {
                setBannerVisible(true)
            }
        }

        if (!visible) {
            binding.connectionBanner.hide()
        }

        binding.connectionBanner.isVisible = visible
        pushBannerHeightToScreen()
    }

    private fun runBannerTransition() = TransitionManager.beginDelayedTransition(rootLayout, TransitionSet().apply {
        addTransition(Explode())
        addTransition(ChangeBounds())
        addTransition(Fade())
        excludeTarget(de.crysxd.baseui.R.id.widgetContainer, true)
        excludeTarget(ViewPager2::class.java, true)
        excludeChildren(octoToolbar, true)
    })

    private fun pushBannerHeightToScreen() {
        val topInsets = if (!isTablet() && isLandscape() && !binding.connectionBanner.isVisible) 0 else lastInsets.top
        if (binding.connectionBanner.isVisible) {
            binding.connectionBanner.updatePadding(top = topInsets)
            binding.instanceBanner.updatePadding(top = resources.getDimensionPixelSize(R.dimen.margin_0_1))
        } else {
            binding.connectionBanner.updatePadding(top = 0)
            binding.instanceBanner.updatePadding(top = topInsets)
        }
        binding.bannerView.measure(
            View.MeasureSpec.makeMeasureSpec(rootLayout.width, View.MeasureSpec.EXACTLY),
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
        )
        val height = binding.bannerView.measuredHeight.takeIf { binding.connectionBanner.isVisible || binding.instanceBanner.isVisible } ?: lastInsets.top
        findCurrentScreen()?.let { applyInsetsToScreen(it, height) }

        Timber.v("Pushing status bar height=$height (connectionBanner=${binding.connectionBanner.isVisible}, instanceBanner=${binding.instanceBanner.text.isNotBlank()}")
        applySystemBarColors()
    }

    private fun applySystemBarColors() = with(WindowInsetsControllerCompat(window, binding.root)) {
        val isBannerShown = binding.connectionBanner.isVisible || binding.instanceBanner.isVisible
        val isWebcamFullscreen = try {
            navController.currentDestination?.id == R.id.webcamFragment
        } catch (e: IllegalStateException) {
            // NavController not set yet
            false
        }

        isAppearanceLightStatusBars = !isNightMode() && !isBannerShown && !isWebcamFullscreen
        isAppearanceLightNavigationBars = !isNightMode() && !isWebcamFullscreen
    }

    private fun updateCapabilities(trigger: String, updateM115: Boolean = true, escalateError: Boolean = true) {
        Timber.i("Updating capabities (trigger=$trigger)")
        updateCapabilitiesJob = lifecycleScope.launchWhenCreated {
            try {
                viewModel.lastSuccessfulCapabilitiesUpdate = System.currentTimeMillis()
                BaseInjector.get().updateInstanceCapabilitiesUseCase().execute(UpdateInstanceCapabilitiesUseCase.Params())
                updateAllWidgets()
            } catch (e: Exception) {
                viewModel.lastSuccessfulCapabilitiesUpdate = 0
                if (escalateError) {
                    Timber.e(e)
                    showSnackbar(
                        Message.SnackbarMessage(
                            text = { getString(R.string.capabilities_validation_error) },
                            type = Message.SnackbarMessage.Type.Negative
                        )
                    )
                }
            }
        }
    }

    override fun startPrintNotificationService() {
        LiveNotificationManager.start(this)
    }
}