package de.crysxd.octoapp

import android.app.Application
import androidx.annotation.VisibleForTesting
import de.crysxd.baseui.di.BaseUiInjector
import de.crysxd.octoapp.base.di.BaseComponent
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.NetworkModule
import de.crysxd.octoapp.base.di.SharedBaseComponent
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.network.AndroidNetworkServiceDiscovery
import de.crysxd.octoapp.base.network.CachedLocalDnsResolver
import de.crysxd.octoapp.base.network.dnssd.DnsSd
import de.crysxd.octoapp.connectprinter.di.ConnectPrinterInjector
import de.crysxd.octoapp.filemanager.di.FileManagerInjector
import de.crysxd.octoapp.help.di.HelpInjector
import de.crysxd.octoapp.pluginsupport.di.PluginSupportInjector
import de.crysxd.octoapp.sharedcommon.di.PlatformModule
import de.crysxd.octoapp.sharedcommon.di.SharedCommonComponent
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.signin.di.SignInInjector

fun initializeDependencyInjection(app: Application) = initializeDependencyInjection(app, null)

@VisibleForTesting
fun initializeDependencyInjection(app: Application, daggerBaseComponent: BaseComponent? = null) {
    // Setup Koin
    SharedCommonInjector.setComponent(
        SharedCommonComponent(
            platformModule = PlatformModule(app)
        )
    )

    SharedBaseInjector.setComponent(
        SharedBaseComponent(
            sharedCommonComponent = SharedCommonInjector.get(),
            networkModule = NetworkModule(
                networkServiceDiscovery = AndroidNetworkServiceDiscovery(app, DnsSd.create(app)),
                dns = CachedLocalDnsResolver(app, DnsSd.create(app))
            )
        )
    )

    // Setup Dagger
    daggerBaseComponent?.let { BaseInjector.set(it) } ?: BaseInjector.init(app)
    BaseUiInjector.init(BaseInjector.get())
    SignInInjector.init(BaseInjector.get())
    ConnectPrinterInjector.init(BaseInjector.get())
    FileManagerInjector.init(BaseInjector.get(), BaseUiInjector.get())
    HelpInjector.init(BaseInjector.get())
    PluginSupportInjector.init(BaseInjector.get())
}