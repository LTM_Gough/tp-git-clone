package de.crysxd.octoapp.notification

import android.app.Notification
import android.app.NotificationManager
import android.content.Context
import android.content.ContextWrapper
import android.os.Build
import androidx.core.content.edit
import com.google.gson.Gson
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.data.repository.NotificationIdRepository
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.di.BaseInjector
import kotlinx.coroutines.delay
import timber.log.Timber
import java.util.Date

class PrintNotificationController(
    private val notificationFactory: PrintNotificationFactory,
    private val printNotificationIdRepository: NotificationIdRepository,
    private val octoPreferences: OctoPreferences,
    private val octoPrintRepository: OctoPrintRepository,
    context: Context
) : ContextWrapper(context) {

    companion object {
        private const val KEY_LAST_PRINT_PREFIX = "last-"
        internal val instance by lazy {
            val context = BaseInjector.get().localizedContext()
            val repository = BaseInjector.get().octorPrintRepository()
            PrintNotificationController(
                context = context,
                notificationFactory = PrintNotificationFactory(context, repository, BaseInjector.get().formatEtaUseCase()),
                octoPreferences = BaseInjector.get().octoPreferences(),
                printNotificationIdRepository = BaseInjector.get().notificationIdRepository(),
                octoPrintRepository = BaseInjector.get().octorPrintRepository()
            )
        }
    }

    private val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    private val sharedPreferences = getSharedPreferences("print_notification_cache", Context.MODE_PRIVATE)
    private val gson = Gson()

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationFactory.createNotificationChannels()
        }
    }

    fun ensureNotificationChannelCreated() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationFactory.createNotificationChannels()
        }
    }

    suspend fun createServiceNotification(instance: OctoPrintInstanceInformationV3?, statusText: String, doNotify: Boolean = false): Pair<Notification, Int> {
        val notification = instance?.id?.let { getLast(it) }
            ?.let { notificationFactory.createStatusNotification(instance.id, it, getString(R.string.print_notification___connecting), doLog = true) }
            ?: notificationFactory.createServiceNotification(instance, statusText)
        val id = printNotificationIdRepository.getPrintStatusNotificationId(instance?.id)

        if (doNotify) {
            notificationManager.notify(id, notification)
        }

        return notification to id
    }

    suspend fun update(instanceId: String, printState: PrintState?, stateText: String? = null, autoCancelOtherNotification: Boolean = false) {
        val last = getLast(instanceId)
        val proceed = when {
            // Notifications disabled? Drop
            octoPreferences.wasPrintNotificationDisabledUntilNextLaunch -> {
                Timber.v("Dropping update, notifications disabled")
                false
            }

            // No last or posting last? Proceed
            last == null -> {
                Timber.v("Proceeding with update, no last found")
                true
            }

            // New live events always proceed
            printState?.source == PrintState.Source.Live -> {
                Timber.v("Proceeding with update, is live")
                true
            }

            // If the last was from remote and this is from remote (implied) and the source time progressed, then we proceed
            last.source == PrintState.Source.CachedRemote && printState != null && printState.sourceTime > last.sourceTime -> {
                Timber.v("Proceeding with update, new and last is remote and time progressed")
                true
            }

            // If the last was live but is outdated, then we proceed
            last.source == PrintState.Source.CachedLive && last.sourceTime.before(liveThreshold) -> {
                Timber.v("Proceeding with update, last is outdated live event")
                true
            }

            // Reposting? Proceed
            last == printState || printState == null -> {
                Timber.v("Proceeding with update, repost")
                true
            }

            // Else: we drop
            else -> {
                Timber.v("Dropping update, default")
                false
            }
        }

        if (proceed) (printState ?: last)?.let { ps ->
            setLast(instanceId, ps)
            val notificationId = printNotificationIdRepository.getPrintStatusNotificationId(instanceId)
            notificationFactory.createStatusNotification(instanceId, ps, stateText)?.let {
                Timber.v("Showing print notification: instanceId=$instanceId notificationId=$notificationId")
                notificationManager.notify(notificationId, it)
            } ?: Timber.e(IllegalStateException("Received update event for instance $instanceId but instance was not found"))
        }

        if (printState?.state == PrintState.State.Printing && autoCancelOtherNotification) {
            cancelNotifications(instanceId, event = true, completed = true)
        }
    }

    suspend fun notifyCompleted(instanceId: String, printState: PrintState) = notificationFactory.createPrintCompletedNotification(
        instanceId = instanceId,
        printState = printState
    )?.let {
        notifyIdle(instanceId)

        val notificationId = printNotificationIdRepository.getPrintCompletedNotificationId(instanceId)
        Timber.i("Showing completed notification: instanceId=$instanceId notificationId=$notificationId")
        notificationManager.notify(notificationId, it)
    } ?: Timber.e(IllegalStateException("Received completed event for instance $instanceId but instance was not found"))

    suspend fun notifyFilamentRequired(instanceId: String, printState: PrintState) = notificationFactory.createFilamentChangeNotification(
        instanceId = instanceId,
    )?.let {
        update(instanceId, printState, autoCancelOtherNotification = true)

        val notificationId = printNotificationIdRepository.getPrintEventNotificationId(instanceId)
        Timber.i("Showing filament runout notification: instanceId=$instanceId notificationId=$notificationId")
        notificationManager.notify(notificationId, it)
    } ?: Timber.e(IllegalStateException("Received filament event for instance $instanceId but instance was not found"))

    suspend fun notifyPausedFromGcode(instanceId: String, printState: PrintState) = notificationFactory.createPausedFromGcodeNotification(
        instanceId = instanceId,
    )?.let {
        update(instanceId, printState, autoCancelOtherNotification = true)

        val notificationId = printNotificationIdRepository.getPrintEventNotificationId(instanceId)
        Timber.i("Showing paused from Gcode: instanceId=$instanceId notificationId=$notificationId")
        notificationManager.notify(notificationId, it)
    } ?: Timber.e(IllegalStateException("Received pause from gcode for instance $instanceId but instance was not found"))

    fun notifyBeep(instanceId: String) = notificationFactory.createBeepNotification(instanceId = instanceId)?.let {
        val notificationId = printNotificationIdRepository.getBeepNotificationId(instanceId)
        if (BaseInjector.get().octoPreferences().isNotifyPrinterBeep) {
            Timber.i("Showing beep: instanceId=$instanceId notificationId=$notificationId")
            notificationManager.notify(notificationId, it)
        } else {
            Timber.i("Skipping beep notification, disabled")
        }
    } ?: Timber.e(IllegalStateException("Received beep for instance $instanceId but instance was not found"))

    suspend fun notifyFilamentSelectionRequired(instanceId: String, printState: PrintState) = notificationFactory.createFilamentSelectionNotification(
        instanceId = instanceId,
    )?.let {
        update(instanceId, printState, autoCancelOtherNotification = true)

        val notificationId = printNotificationIdRepository.getPrintEventNotificationId(instanceId)
        Timber.i("Showing filament selection notification: instanceId=$instanceId notificationId=$notificationId")
        notificationManager.notify(notificationId, it)
    } ?: Timber.e(IllegalStateException("Received filament event for instance $instanceId but instance was not found"))

    suspend fun notifyFilamentSelectionCompleted(instanceId: String, printState: PrintState) = notificationFactory.createFilamentSelectionNotification(
        instanceId = instanceId,
    )?.let {
        // Update will clear events
        Timber.i("Clearing filament selection notification: instanceId=$instanceId")
        update(instanceId, printState, autoCancelOtherNotification = true)
    } ?: Timber.e(IllegalStateException("Received filament event for instance $instanceId but instance was not found"))

    suspend fun notifyIdle(instanceId: String) {
        clearLast(instanceId)
        cancelNotifications(instanceId, event = true, status = true)
    }

    private suspend fun cancelNotifications(instanceId: String?, event: Boolean = false, completed: Boolean = false, status: Boolean = false) {
        if (status) {
            val printNotificationId = printNotificationIdRepository.getPrintStatusNotificationId(instanceId)
            Timber.v("Cancelling status notification: instanceId=$instanceId notificationId=$printNotificationId")
            notificationManager.cancel(printNotificationId)
        }

        if (event) {
            val eventNotificationId = printNotificationIdRepository.getPrintEventNotificationId(instanceId)
            Timber.v("Cancelling event notification: instanceId=$instanceId notificationId=$eventNotificationId")
            notificationManager.cancel(eventNotificationId)
        }

        if (completed) {
            val completedNotificationId = printNotificationIdRepository.getPrintCompletedNotificationId(instanceId)
            Timber.v("Cancelling completed notification: instanceId=$instanceId notificationId=$completedNotificationId")
            notificationManager.cancel(completedNotificationId)
        }

        // Give notification system a bit to settle after cancelling...some phones seem to need this (Xiaomi)
        delay(100)
    }

    fun clearLast(instanceId: String) = sharedPreferences.edit { remove("$KEY_LAST_PRINT_PREFIX$instanceId") }

    fun getLast(instanceId: String) = try {
        sharedPreferences.getString("$KEY_LAST_PRINT_PREFIX$instanceId", null)?.let { gson.fromJson(it, PrintState::class.java) }
    } catch (e: Exception) {
        Timber.e(e)
        null
    }

    private fun setLast(instanceId: String, printState: PrintState) = sharedPreferences.edit {
        putString("$KEY_LAST_PRINT_PREFIX$instanceId", gson.toJson(printState.copy(source = printState.source.asCached)))
    }

    fun cancelUpdateNotifications() {
        octoPrintRepository.getAll().forEach {
            notificationManager.cancel(printNotificationIdRepository.getPrintStatusNotificationId(it.id))
        }
    }

    private val liveThreshold get() = Date(System.currentTimeMillis() - 10_000)

}
