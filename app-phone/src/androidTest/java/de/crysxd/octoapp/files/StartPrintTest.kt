package de.crysxd.octoapp.files

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeUp
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By.res
import androidx.test.uiautomator.By.text
import androidx.test.uiautomator.UiDevice
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import de.crysxd.baseui.compose.framework.TestTags
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.framework.getString
import de.crysxd.octoapp.framework.robots.BottomToolbarRobot
import de.crysxd.octoapp.framework.robots.MenuRobot
import de.crysxd.octoapp.framework.robots.MenuRobot.checkHasMenuButton
import de.crysxd.octoapp.framework.robots.WorkspaceRobot
import de.crysxd.octoapp.framework.rules.AutoConnectPrinterRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.condition.waitFor
import de.crysxd.octoapp.tests.condition.waitForDialog
import de.crysxd.octoapp.tests.condition.waitForPass
import de.crysxd.octoapp.tests.ext.setActive
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import kotlin.time.Duration.Companion.seconds

class StartPrintTest {

    private val testEnvVanilla = TestEnvironmentLibrary.Terrier
    private val testEnvSpoolManager = TestEnvironmentLibrary.Frenchie
    private val baristaRule = BaristaRule.create(MainActivity::class.java)
    private val uiDevice by lazy { UiDevice.getInstance(InstrumentationRegistry.getInstrumentation()) }

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(IdleTestEnvironmentRule(testEnvSpoolManager, testEnvVanilla))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(AutoConnectPrinterRule())

    @Test(timeout = 300_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_a_print_is_started_THEN_the_app_shows_printing() = with(uiDevice) {
        //region Setup
        BaseInjector.get().octorPrintRepository().setActive(testEnvVanilla)
        baristaRule.launchActivity()
        //endregion
        //region Open file and start print
        "layers.gcode".let { file ->
            triggerPrint(file)
            verifyPrinting(file)
        }
        //endregion
        //region Wait for printing and Pause
        waitForPass("1% print progress", timeout = 60.seconds) {
            assert(findObject(res(TestTags.BottomBar.Status))?.text == "1%") { "Expected 1% progress to be shown" }
        }
        assert(hasObject(res(TestTags.BottomBar.Pause))) { "Expected pause to be shown" }
        assert(hasObject(res(TestTags.BottomBar.Cancel))) { "Expected cancel to be shown" }
        assert(!hasObject(res(TestTags.BottomBar.Resume))) { "Expected resume to be hidden" }
        BottomToolbarRobot.confirmButtonWithSwipe(TestTags.BottomBar.Pause)
        //endregion
        //region Wait for paused and resume
        waitForPass("Print paused", timeout = 60.seconds) {
            assert(findObject(res(TestTags.BottomBar.Status))?.text == getString(R.string.paused)) { "Expected paused to be shown" }
        }
        assert(!hasObject(res(TestTags.BottomBar.Pause))) { "Expected pause to be hidden" }
        assert(hasObject(res(TestTags.BottomBar.Cancel))) { "Expected cancel to be shown" }
        assert(hasObject(res(TestTags.BottomBar.Resume))) { "Expected resume to be shown" }
        BottomToolbarRobot.confirmButtonWithSwipe(TestTags.BottomBar.Resume)
        //endregion
        //region Wait for resume and cancel
        waitForPass("2% print progress", timeout = 60.seconds) {
            assert(findObject(res(TestTags.BottomBar.Status))?.text == "2%") { "Expected 2% progress to be shown" }
        }
        assert(hasObject(res(TestTags.BottomBar.Pause))) { "Expected pause to be shown" }
        assert(hasObject(res(TestTags.BottomBar.Cancel))) { "Expected cancel to be shown" }
        assert(!hasObject(res(TestTags.BottomBar.Resume))) { "Expected resume to be hidden" }
        BottomToolbarRobot.confirmButtonWithSwipe(TestTags.BottomBar.Cancel)
        WorkspaceRobot.waitForPrepareWorkspace()
        //endregion
    }

    @Test(timeout = 120_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_a_print_is_started_and_a_spool_is_selected_with_SpoolManager_THEN_the_app_shows_printing() =
        runMaterialTest("SM Spätzle")

    @Test(timeout = 120_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_a_print_is_started_and_a_spool_is_selected_with_Filament_Manager_THEN_the_app_shows_printing() =
        runMaterialTest("FM Fusili (ABS)")

    @Test(timeout = 120_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_a_print_is_started_and_no_spool_is_selected_THEN_the_app_shows_printing() =
        runMaterialTest(InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.material_menu___print_without_selection))

    private fun runMaterialTest(selection: String) = with(uiDevice) {
        //region Setup
        BaseInjector.get().octorPrintRepository().setActive(testEnvSpoolManager)
        baristaRule.launchActivity()
        //endregion
        //region Open file and start print
        // Use waits.gcode to NOT use material, changing the spool weights
        triggerPrint("waits.gcode")

        // Check dialog
        verifyMaterialSelection()
        onView(withText(selection)).inRoot(isDialog()).perform(click())
        //endregion
        //region Wait for print workspace to be shown
        MenuRobot.waitForMenuToBeClosed()
        verifyPrinting("waits.gcode")
        //endregion
    }

    private fun triggerPrint(file: String) = with(uiDevice) {
        WorkspaceRobot.waitForPrepareWorkspace()
        uiDevice.findObject(res(TestTags.BottomBar.Start)).click()
        onView(withId(R.id.recyclerViewFileList)).perform(swipeUp())
        waitFor(allOf(withText(file), isDisplayed()))
        onView(withText(file)).perform(click())
        waitFor(allOf(withText(R.string.start_printing), isDisplayed()))
        onView(withText(R.string.start_printing)).perform(click())
    }

    private fun verifyPrinting(file: String) = with(uiDevice) {
        // Wait for print workspace
        WorkspaceRobot.waitForPrintWorkspace()

        // Wait for print data to show up
        waitForPass(description = "Less then a minute and file name showing", timeout = 10.seconds) {
            assert(uiDevice.hasObject(text(getString(R.string.less_than_a_minute)))) { "Expected two views with text" }
            assert(uiDevice.hasObject(text(file))) { "Expected file to be mentioned" }
        }
    }

    private fun verifyMaterialSelection() {
        waitForDialog(withText(R.string.material_menu___title_select_material))
        checkHasMenuButton(text = "FM Fusili (ABS)", index = 0, rightDetail = "820g")
        checkHasMenuButton(text = "SM Spätzle", index = 1, rightDetail = "99g")
        checkHasMenuButton(text = "FM Fusili (PLA)", index = 2, rightDetail = "1000g")
        checkHasMenuButton(text = "SM Ramen (PLA, Japan)", index = 3, rightDetail = "170g")
        checkHasMenuButton(text = "SM Ramen (PLA, Japan)", index = 4, rightDetail = "1000g")
        checkHasMenuButton(text = "SM Spaghetti", index = 5, rightDetail = "90g")
        checkHasMenuButton(text = "Template", index = 0, false)
        checkHasMenuButton(text = "Empty", index = 0, false)
        checkHasMenuButton(text = "Not Active", index = 0, false)
        checkHasMenuButton(InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.material_menu___print_without_selection), 6)
    }
}
