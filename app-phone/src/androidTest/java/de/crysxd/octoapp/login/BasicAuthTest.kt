package de.crysxd.octoapp.login

import android.widget.EditText
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.framework.robots.SignInRobot
import de.crysxd.octoapp.framework.rules.MockDiscoveryRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.withBasicAuth
import de.crysxd.octoapp.tests.condition.waitFor
import de.crysxd.octoapp.tests.condition.waitForDialog
import de.crysxd.octoapp.tests.ext.setActive
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain

class BasicAuthTest {

    private val serverPort = 8000
    private val testUrl = "http://localhost:$serverPort/".toUrl()
    private val username = "gue:@st🏀"
    private val password = "gue)(*&^%$#@!/<>😍s🤣❤️‍🩹🏹t"
    private val basicAuthFormMatcher = allOf(withId(R.id.usernameInput), isDisplayed())
    private val userNameInput = onView(allOf(isDescendantOfA(withId(R.id.usernameInput)), isAssignableFrom(EditText::class.java)))
    private val passwordInput = onView(allOf(isDescendantOfA(withId(R.id.passwordInput)), isAssignableFrom(EditText::class.java)))

    private val baristaRule = BaristaRule.create(MainActivity::class.java)

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(BasicAuthServer(port = serverPort, username = username, password = password))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(MockDiscoveryRule())

    @Test(timeout = 60_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_signing_in_THEN_basic_auth_credentials_are_asked() {
        // GIVEN
        baristaRule.launchActivity()

        // Start sign in
        SignInRobot.waitForManualToBeShown()
        SignInRobot.manualInput.perform(replaceText(testUrl.toString()))
        SignInRobot.continueButton.perform(click())

        // Wait for basic auth form to be shown
        waitFor(basicAuthFormMatcher, timeout = 10000)

        // Enter wrong credentials
        val wrongUser = "secretuser$@&323"
        val wrongPassword = "secretpass$@&/?=§:;323"
        userNameInput.perform(replaceText(wrongUser))
        passwordInput.perform(replaceText(wrongPassword))
        SignInRobot.scrollDown()
        SignInRobot.continueButton.perform(click())

        // Wait for shown again and verify prefilled
        SignInRobot.waitForChecks()
        waitFor(basicAuthFormMatcher, timeout = 10000)
        userNameInput.check(matches(withText(wrongUser)))
        passwordInput.check(matches(withText(wrongPassword)))

        // Enter correct user
        userNameInput.perform(replaceText(username))
        passwordInput.perform(replaceText(password))
        SignInRobot.scrollDown()
        SignInRobot.continueButton.perform(click())
        SignInRobot.waitForChecks()

        // Check accepted (aka OctoPrint not found as we don't connect to a OctoPrint)
        waitFor(withText(R.string.sign_in___probe_finding___title_octoprint_not_found), timeout = 10000)
    }

    @Test(timeout = 60_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_credentials_become_invalid_THEN_basic_auth_credentials_are_asked() {
        // GIVEN
        val wrongUser = "secretuser$@&323"
        val wrongPassword = "secretpass$@&/?=§:;323"
        val urlWithAuth = testUrl.withBasicAuth(user = wrongUser, password = wrongPassword)
        BaseInjector.get().octorPrintRepository().setActive(
            OctoPrintInstanceInformationV3(
                id = "random",
                webUrl = urlWithAuth,
                apiKey = "random,not used"
            )
        )
        baristaRule.launchActivity()

        // Wait for error
        waitForDialog(withText(R.string.sign_in___broken_setup___basic_auth_required))
        onView(withText(R.string.sign_in___continue)).inRoot(isDialog()).perform(click())

        // Wait for shown again and verify prefilled
        SignInRobot.waitForChecks()
        waitFor(basicAuthFormMatcher, timeout = 5_000)

        // Enter correct user
        userNameInput.perform(replaceText(username))
        passwordInput.perform(replaceText(password))
        SignInRobot.scrollDown()
        SignInRobot.continueButton.perform(click())
        SignInRobot.waitForChecks()

        // Check accepted (aka OctoPrint not found as we don't connect to a OctoPrint)
        waitFor(withText(R.string.sign_in___probe_finding___title_octoprint_not_found))
    }
}