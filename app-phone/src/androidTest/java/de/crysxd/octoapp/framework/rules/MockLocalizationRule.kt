package de.crysxd.octoapp.framework.rules

import de.crysxd.octoapp.base.di.BaseComponent
import kotlinx.coroutines.runBlocking
import java.util.Locale

class MockLocalizationRule(private var targetLocale: Locale = Locale.ENGLISH) : AbstractUseCaseMockRule() {

    init {
        mockFor(targetLocale)
    }

    override fun createBaseComponent(base: BaseComponent) = MockBaseComponent(base)

    fun mockFor(locale: Locale) = runBlocking {
        targetLocale = locale
    }

    inner class MockBaseComponent(real: BaseComponent) : BaseComponent by real {
        override fun localizedContext() = context().apply {
            val config = resources.configuration
            config.setLocale(targetLocale)
            createConfigurationContext(config)
        }
    }
}