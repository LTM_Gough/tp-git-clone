package de.crysxd.octoapp.framework.robots

import androidx.test.espresso.AmbiguousViewMatcherException
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingRootException
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withParentIndex
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import de.crysxd.baseui.compose.framework.TestTags
import de.crysxd.octoapp.R
import de.crysxd.octoapp.tests.condition.waitFor
import de.crysxd.octoapp.tests.condition.waitTime
import junit.framework.AssertionFailedError
import org.hamcrest.Matchers.allOf
import kotlin.time.Duration.Companion.seconds

object MenuRobot {

    private val uiDevice get() = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

    fun openMenuWithMoreButton() = with(uiDevice) {
        val selector = By.res(TestTags.BottomBar.MainMenu)
        waitFor("Main menu button shown", timeout = 10.seconds) {
            findObject(selector) != null
        }

        // Button is not 90% visible, use custom actions to circumvent check
        findObject(selector).click()
        onView(withText(R.string.main_menu___item_show_printer)).check(matches(isDisplayed()))
        onView(withText(R.string.main_menu___item_show_settings)).check(matches(isDisplayed()))
        onView(withText(R.string.main_menu___item_show_octoprint)).check(matches(isDisplayed()))
        onView(withText(R.string.main_menu___item_show_tutorials)).check(matches(isDisplayed()))
    }

    fun checkHasMenuButton(text: String, index: Int, exists: Boolean = true, rightDetail: String? = null) {
        val menuItemMatcher = allOf(
            withId(R.id.menuItemRoot),
            if (exists) withParentIndex(index) else withId(R.id.menuItemRoot)
        )

        val buttonMatcher = allOf(
            withText(text),
            withId(R.id.text),
            isDescendantOfA(menuItemMatcher),
        )

        onView(buttonMatcher).inRoot(isDialog()).check(if (exists) matches(isDisplayed()) else doesNotExist())

        if (rightDetail != null && exists) {
            onView(
                allOf(
                    isDescendantOfA(menuItemMatcher),
                    withId(R.id.right),
                    withText(rightDetail)
                )
            ).inRoot(isDialog()).check(matches(isDisplayed()))
        }
    }

    fun clickMenuButton(label: Int) {
        onView(withText(label)).inRoot(isDialog()).perform(click())
    }

    fun clickMenuButton(label: String) {
        onView(withText(label)).inRoot(isDialog()).perform(click())
    }

    fun assertMenuTitle(title: Int) {
        onView(withText(title)).inRoot(isDialog()).check(matches(isDisplayed()))
    }

    fun waitForMenuToBeClosed() {
        fun isMenuOpen() = try {
            onView(withId(R.id.menuContainer)).check(matches(isDisplayed()))
            true
        } catch (e: AmbiguousViewMatcherException) {
            true
        } catch (e: NoMatchingViewException) {
            false
        } catch (e: NoMatchingRootException) {
            false
        } catch (e: AssertionFailedError) {
            // Is animating out
            true
        }

        while (isMenuOpen()) {
            waitTime(500)
        }
    }
}