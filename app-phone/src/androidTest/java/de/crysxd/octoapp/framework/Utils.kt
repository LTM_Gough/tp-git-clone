package de.crysxd.octoapp.framework

import androidx.annotation.StringRes
import androidx.test.platform.app.InstrumentationRegistry

fun getString(@StringRes res: Int) = InstrumentationRegistry.getInstrumentation().targetContext.getString(res)