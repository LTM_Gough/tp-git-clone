package de.crysxd.octoapp.remoteaccess

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import com.google.common.truth.Truth.assertThat
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.engine.exceptions.ngrok.NgrokTunnelNotFoundException
import de.crysxd.octoapp.framework.rules.AcceptAllAccessRequestRule
import de.crysxd.octoapp.framework.rules.AutoConnectPrinterRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.condition.waitFor
import de.crysxd.octoapp.tests.condition.waitForDialog
import de.crysxd.octoapp.tests.ext.setActive
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import io.ktor.http.Url
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain

class NgrokTest {

    private val testEnv = TestEnvironmentLibrary.Terrier
    private val testEnvRemote = TestEnvironmentLibrary.CorgiRemote
    private val baristaRule = BaristaRule.create(MainActivity::class.java)

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(IdleTestEnvironmentRule(testEnv))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(AcceptAllAccessRequestRule(testEnv))
        .around(AutoConnectPrinterRule())

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_ngrok_tunnel_was_deleted_THEN_then_we_disconnect_it() {
        // GIVEN
        BaseInjector.get().octorPrintRepository()
            .setActive(testEnvRemote.copy(alternativeWebUrl = "https://297c-31-21-114-230.ap.ngrok.io".toUrl()))

        // WHEN
        baristaRule.launchActivity()

        // THEN
        waitForDialog(withText(NgrokTunnelNotFoundException(Url("http://notused.com")).userFacingMessage))
        val info = BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()
        assertThat(info).isNotNull()
        assertThat(info!!.alternativeWebUrl).isNull()
        assertThat(info.octoEverywhereConnection).isNull()

        onView(withText(R.string.sign_in___continue)).perform(click())
        waitFor(allOf(isDisplayed(), withText(R.string.connect_printer___octoprint_not_available_title)))
    }
}