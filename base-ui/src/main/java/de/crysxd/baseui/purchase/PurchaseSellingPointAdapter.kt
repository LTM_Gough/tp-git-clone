package de.crysxd.baseui.purchase

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import de.crysxd.baseui.R
import de.crysxd.baseui.common.ViewBindingHolder
import de.crysxd.baseui.databinding.ItemPurchaseSellingPointBinding
import de.crysxd.octoapp.base.data.models.PurchaseOffers

class PurchaseSellingPointAdapter : RecyclerView.Adapter<PurchaseSellingPointAdapter.ViewHolder>() {

    var clickCallback: (Int) -> Unit = {}
    private var highlightPosition = 0

    var sellingPoints: List<PurchaseOffers.SellingPoint> = emptyList()
        set(value) {
            notifyItemRangeRemoved(0, itemCount)
            field = value
            notifyItemRangeInserted(0, itemCount)
        }

    var picasso: Picasso? = null
        set(value) {
            field = value
            notifyItemRangeInserted(0, itemCount)
        }

    fun setHighlight(layoutManager: LinearLayoutManager?, position: Int) {
        (0..itemCount).map { layoutManager?.findViewByPosition(it) to (it == position) }.forEach {
            it.first?.let { v -> setViewHighlighted(v, it.second, animated = true) }
        }
    }

    private fun setViewHighlighted(view: View, highlighted: Boolean, animated: Boolean) {
        val alpha = if (highlighted) 1f else 0.4f
        val v = ItemPurchaseSellingPointBinding.bind(view).image
        if (animated) {
            v.animate().alpha(alpha).start()
        } else {
            v.alpha = alpha
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (val url = sellingPoints[position].imageUrl) {
            "party" -> holder.binding.image.setImageResource(R.drawable.selling_point_more_to_come)

            else -> picasso?.load(url)
                ?.placeholder(R.drawable.selling_point_placeholder)
                ?.error(R.drawable.selling_point_error)
                ?.into(holder.binding.image)
                ?: holder.binding.image.setImageResource(R.drawable.selling_point_error)
        }

        setViewHighlighted(holder.itemView, highlighted = highlightPosition == position, animated = false)
        holder.itemView.setOnClickListener { clickCallback(position) }
    }

    override fun getItemCount() = sellingPoints.size

    class ViewHolder(parent: ViewGroup) : ViewBindingHolder<ItemPurchaseSellingPointBinding>(
        ItemPurchaseSellingPointBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )
}