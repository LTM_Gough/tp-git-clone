package de.crysxd.baseui.purchase

import android.graphics.Paint
import android.graphics.Rect
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import androidx.activity.OnBackPressedCallback
import androidx.core.view.doOnNextLayout
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePadding
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import androidx.transition.TransitionManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import de.crysxd.baseui.BaseFragment
import de.crysxd.baseui.InsetAwareScreen
import de.crysxd.baseui.R
import de.crysxd.baseui.common.OctoToolbar
import de.crysxd.baseui.databinding.PurchaseFragmentBinding
import de.crysxd.baseui.databinding.PurchaseFragmentInitStateBinding
import de.crysxd.baseui.databinding.PurchaseFragmentSkuStateOptionBinding
import de.crysxd.baseui.di.BaseUiInjector
import de.crysxd.baseui.di.injectViewModel
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.baseui.utils.InstantAutoTransition
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.data.models.PurchaseOffers
import de.crysxd.octoapp.base.ext.purchaseOffers
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.base.utils.LongDuration
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.isActive
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import java.text.NumberFormat
import kotlin.math.absoluteValue

class PurchaseFragment : BaseFragment(), InsetAwareScreen {

    private lateinit var binding: PurchaseFragmentBinding
    override val viewModel: PurchaseViewModel by injectViewModel()
    private var purchaseCompleted = false
    private lateinit var sellingPointAdapter: PurchaseSellingPointAdapter
    private val backPressedCallback = object : OnBackPressedCallback(false) {
        override fun handleOnBackPressed() {
            when (viewModel.viewState.value) {
                is PurchaseViewModel.ViewState.SkuSelectionState -> viewModel.moveToInitState()
                else -> Unit
            }
        }
    }
    private val config by lazy { Firebase.remoteConfig.purchaseOffers.activeConfig }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(R.transition.base_ui_screenshot_transition)
        sharedElementReturnTransition = TransitionInflater.from(context).inflateTransition(R.transition.base_ui_screenshot_transition)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        PurchaseFragmentBinding.inflate(inflater, container, false).also { binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireOctoActivity().controlCenter.disableForLifecycle(viewLifecycleOwner.lifecycle)
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, backPressedCallback)
        viewModel.viewState.observe(viewLifecycleOwner, ::moveToState)

        var eventSent = false
        binding.appBar.addOnOffsetChangedListener { _, verticalOffset ->
            val collapseProgress = verticalOffset.absoluteValue / binding.appBar.totalScrollRange.toFloat()
            val content = binding.saleBanner.takeIf { it.isVisible } ?: binding.contentContainer
            val padding = binding.statusBarScrim.height
            binding.statusBarScrim.alpha = if (collapseProgress >= 1) 1f else (1f - collapseProgress)
            binding.statusBarScrim.setBackgroundResource(
                if (collapseProgress >= 1) R.color.window_background else R.color.status_bar_scrim
            )
            content.updatePadding(top = (padding * collapseProgress).toInt())

            if (!eventSent && verticalOffset != 0) {
                eventSent = true
                OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseScreenScroll)
            }
        }

        // Picasso
        sellingPointAdapter = PurchaseSellingPointAdapter()
        sellingPointAdapter.picasso = BaseUiInjector.get().picasso()

        // Sales banner with countdown
        binding.saleBannerText.text = config.textsWithData.highlightBanner?.toHtml()
        binding.saleBanner.isVisible = binding.saleBannerText.text.isNotBlank()
        if (binding.saleBanner.isVisible) {
            viewLifecycleOwner.lifecycleScope.launchWhenCreated {
                while (isActive) {
                    delay(1000)
                    binding.saleBannerText.text = config.textsWithData.highlightBanner?.toHtml()
                }
            }
        }

        populateInitState()
    }

    private fun moveToState(state: PurchaseViewModel.ViewState) {
        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            val fullyExpanded = (binding.appBar.height - binding.appBar.bottom) == 0
            if (!fullyExpanded) {
                binding.appBar.setExpanded(true)
                delay(300)
            }

            TransitionManager.beginDelayedTransition(view as ViewGroup, InstantAutoTransition(explode = true))
            binding.initState.root.isVisible = false
            binding.skuState.root.isVisible = false
            binding.unsupportedPlatformState.root.isVisible = false
            binding.buttonSupport.isVisible = state is PurchaseViewModel.ViewState.InitState
            backPressedCallback.isEnabled = !listOf(PurchaseViewModel.ViewState.InitState, PurchaseViewModel.ViewState.Unsupported).contains(state)

            when (state) {
                PurchaseViewModel.ViewState.Unsupported -> {
                    binding.unsupportedPlatformState.root.isVisible = true
                }

                PurchaseViewModel.ViewState.InitState -> {
                    OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseIntroShown)
                    binding.skuState.skuList.removeAllViews()
                    binding.initState.root.isVisible = true
                }

                is PurchaseViewModel.ViewState.SkuSelectionState -> {
                    OctoAnalytics.logEvent(
                        OctoAnalytics.Event.PurchaseOptionsShown, mapOf(
                            "title" to binding.initState.purchaseTitle.text.take(100),
                            "text" to binding.initState.description.text.take(100),
                            "button_text" to binding.buttonSupport.text
                        )
                    )
                    binding.skuState.root.isVisible = true
                    populateSkuState(state)
                }
            }
        }
    }

    private fun populateSkuState(state: PurchaseViewModel.ViewState.SkuSelectionState) {
        binding.skuState.root.updatePadding(top = 0)
        binding.skuState.skuList.removeAllViews()
        binding.skuState.skuTitle.text = Firebase.remoteConfig.getString("sku_list_title").toHtml()

        config.offers?.mapNotNull { offer ->
            val sku = state.billingData.allSku.firstOrNull { it.sku == offer.key } ?: return@mapNotNull null
            sku to offer.value
        }?.forEach { x ->
            val (details, offer) = x
            val itemBinding = PurchaseFragmentSkuStateOptionBinding.inflate(LayoutInflater.from(requireContext()))
            val dealFor = state.billingData.allSku.firstOrNull { it.sku == offer.dealFor }

            // If we have a matching offer, we show the old price
            dealFor?.let {
                itemBinding.priceOld.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                itemBinding.priceOld.text = it.price
                itemBinding.discount.text = NumberFormat.getPercentInstance().format((1 - (details.priceAmountMicros / it.priceAmountMicros.toFloat())) * -1)
            }
            itemBinding.priceOld.isVisible = itemBinding.priceOld.text.isNotBlank()
            itemBinding.discount.isVisible = itemBinding.priceOld.isVisible

            // Normal offer
            itemBinding.price.text = details.price
            itemBinding.buttonSelect.text = offer.label ?: details.title
            itemBinding.details.text = LongDuration.parse(details.freeTrialPeriod)?.format(requireContext())?.let { getString(R.string.free_trial_x, it) }
            itemBinding.details.isVisible = !itemBinding.details.text.isNullOrBlank()
            itemBinding.buttonSelect.setOnClickListener {
                OctoAnalytics.logEvent(
                    OctoAnalytics.Event.PurchaseOptionSelected, mapOf(
                        "button_text" to itemBinding.buttonSelect.text,
                        "title" to binding.skuState.skuTitle.text,
                        "trial" to details.freeTrialPeriod,
                        "badge" to offer.badge,
                        "deal_for" to dealFor?.sku,
                        "sku" to details.sku
                    )
                )
                BillingManager.purchase(requireActivity(), details)
            }
            itemBinding.badge.setImageResource(
                when {
                    dealFor != null -> R.drawable.ic_badge_sale
                    offer.badge == PurchaseOffers.Badge.BestValue -> R.drawable.ic_badge_best_value
                    offer.badge == PurchaseOffers.Badge.Popular -> R.drawable.ic_badge_popular
                    else -> 0
                }
            )
            binding.skuState.skuList.addView(itemBinding.root)
        }

        if (state.billingData.allSku.isEmpty()) {
            OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseMissingSku)
            MaterialAlertDialogBuilder(requireContext())
                .setMessage("Thanks for your interest! There was a issue loading available offers, check back later!")
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    findNavController().popBackStack()
                }.show()
        }
    }

    private fun populateInitState() {
        val initBinding = binding.initState
        val texts = config.textsWithData
        initBinding.root.updatePadding(top = 0)

        initBinding.purchaseTitle.text = texts.purchaseScreenTitle.toHtml()
        initBinding.description.text = texts.purchaseScreenDescription.toHtml()
        binding.buttonSupport.text = texts.purchaseScreenContinueCta.toHtml()

        sellingPointAdapter.sellingPoints = config.sellingPoints
        val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        initBinding.sellingPoints.layoutManager = layoutManager
        val snapHelper = LinearSnapHelper().also { it.attachToRecyclerView(initBinding.sellingPoints) }
        val scrollListener = SellingPointScrollListener(
            snapHelper = snapHelper,
            initBinding = initBinding,
            adapter = sellingPointAdapter,
        )
        initBinding.sellingPoints.addOnScrollListener(scrollListener)
        initBinding.sellingPoints.doOnNextLayout {
            scrollListener.onScrolled(initBinding.sellingPoints, 0, 0)
        }
        sellingPointAdapter.clickCallback = {
            layoutManager.findViewByPosition(it)?.let { view ->
                val d = snapHelper.calculateDistanceToFinalSnap(layoutManager, view) ?: intArrayOf(0, 0)
                if (d[0] == 0 && d[1] == 0) {
                    config.sellingPoints[it].imageUrl?.toHttpUrlOrNull()?.toString()?.let { url ->
                        view.transitionName = "screenshot"
                        val extras = FragmentNavigatorExtras(view to "screenshot")
                        findNavController().navigate(PurchaseFragmentDirections.actionShowScreenshot(url), extras)
                    }
                } else {
                    initBinding.sellingPoints.smoothScrollBy(d[0], d[1], DecelerateInterpolator())
                }
            }
        }
        val padding = (requireOctoActivity().rootLayout.width / 2) -
                (resources.getDimensionPixelSize(R.dimen.selling_point_screenshot_width) / 2) -
                resources.getDimensionPixelSize(R.dimen.selling_point_screenshot_margin)
        initBinding.sellingPoints.updatePadding(left = padding, right = padding)
        initBinding.sellingPoints.adapter = sellingPointAdapter
        initBinding.purchaseTitle.movementMethod = LinkMovementMethod()
        initBinding.description.movementMethod = LinkMovementMethod()
        binding.buttonSupport.setOnClickListener {
            viewModel.moveToSkuListState()
        }
    }

    override fun onStart() {
        super.onStart()
        requireOctoActivity().octoToolbar.state = OctoToolbar.State.Hidden
        requireOctoActivity().octo.isVisible = false

        lifecycleScope.launchWhenStarted {
            // Billing completed, pop back stack and return
            BillingManager.billingEventFlow().collectLatest {
                if (it.isRecent()) {
                    purchaseCompleted = true
                    findNavController().popBackStack()
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (!purchaseCompleted) {
            OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseScreenClosed)
        }
    }

    override fun handleInsets(insets: Rect) {
        binding.scrollView.setPadding(insets.left, 0, insets.right, 0)
        binding.container.updatePadding(bottom = insets.bottom)
        binding.statusBarScrim.updateLayoutParams { height = insets.top }
        binding.header.imageViewStatusBackground.updateLayoutParams { height = insets.top }
    }

    class SellingPointScrollListener(
        private val initBinding: PurchaseFragmentInitStateBinding,
        private val snapHelper: SnapHelper,
        private val adapter: PurchaseSellingPointAdapter,
    ) : RecyclerView.OnScrollListener() {
        var lastSnapPosition = RecyclerView.NO_POSITION
        var interactionReported = false
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            if (dx != 0 && !interactionReported) {
                interactionReported = true
                OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseUspInteraction)
            }

            val layoutManager = recyclerView.layoutManager as LinearLayoutManager
            val snapView = snapHelper.findSnapView(layoutManager)
            val snapAdapterPosition = snapView?.let { layoutManager.getPosition(it) } ?: RecyclerView.NO_POSITION
            val lastSnapView = layoutManager.findViewByPosition(lastSnapPosition)
            lastSnapPosition = snapView?.let { layoutManager.getPosition(it) } ?: RecyclerView.NO_POSITION

            if (lastSnapView != snapView) {
                TransitionManager.beginDelayedTransition(
                    initBinding.sellingPointContainer,
                    InstantAutoTransition(fadeText = true, quickTransition = true).also {
                        it.excludeTarget(initBinding.purchaseTitle, true)
                        it.excludeTarget(initBinding.description, true)
                    }
                )

                // Focus with alpha and size
                adapter.setHighlight(layoutManager, snapAdapterPosition)

                // Content
                val sp = adapter.sellingPoints.getOrNull(snapAdapterPosition)
                initBinding.sellingPointTitle.text = sp?.title
                initBinding.sellingPointDescription.text = sp?.description
            }
        }
    }
}