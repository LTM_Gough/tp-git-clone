package de.crysxd.baseui.compose.framework

import android.content.res.Configuration
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview

@Preview(
    group = "Light",
    name = "Light",
    device = Devices.PIXEL_4,
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    backgroundColor = 0xffffffff,
    showSystemUi = true,
)
@Preview(
    group = "Dark",
    name = "Dark",
    device = Devices.PIXEL_4,
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    backgroundColor = 0xFF222222,
    showSystemUi = true,

    )
annotation class ScreenPreview

@Preview(
    group = "Light",
    name = "Light",
    device = Devices.AUTOMOTIVE_1024p,
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    backgroundColor = 0xffffffff,
    showSystemUi = true,
)
@Preview(
    group = "Dark",
    name = "Dark",
    device = Devices.PIXEL_C,
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    backgroundColor = 0xFF222222,
    showSystemUi = true,

    )
annotation class ScreenPreviewTablet
