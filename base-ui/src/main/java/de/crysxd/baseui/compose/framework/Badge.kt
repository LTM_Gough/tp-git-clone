package de.crysxd.baseui.compose.framework

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch

@Composable
fun LiveBadge(
    hideAt: Long,
    modifier: Modifier = Modifier,
    animateCountDownTo: Long? = null,
) {
    val scope = rememberCoroutineScope()

    var visible by remember { mutableStateOf(true) }
    DisposableEffect(hideAt) {
        val job = scope.launch {
            visible = hideAt > System.currentTimeMillis()
            delay(hideAt - System.currentTimeMillis())
            visible = false
        }

        onDispose {
            job.cancel()
        }
    }

    LiveBadge(
        visible = visible,
        modifier = modifier,
        animateCountDownTo = animateCountDownTo,
    )
}

@Composable
fun LiveBadge(
    visible: Boolean,
    modifier: Modifier = Modifier,
    animateCountDownTo: Long? = null,
) = Badge(
    text = stringResource(id = R.string.cd_live),
    background = OctoAppTheme.colors.red,
    visible = visible,
    showDot = true,
    modifier = run {
        val animatable = remember { Animatable(0f) }
        val color = Color.White.copy(alpha = 0.33f)

        LaunchedEffect(animateCountDownTo ?: 0L) {
            val to = animateCountDownTo ?: 0L
            val now = System.currentTimeMillis()

            // Ensure we are at target
            animatable.animateTo(animatable.targetValue)

            if (to > now) {
                val newTarget = if (animatable.targetValue == 1f) 0f else 1f
                val duration = (to - now).toInt()
                animatable.animateTo(newTarget, tween(durationMillis = duration, easing = LinearEasing))
            } else {
                animatable.animateTo(0f)
            }
        }

        modifier
            .clip(ChipShape)
            .drawWithContent {
                drawContent()
                drawRect(color, size = size.copy(width = size.width * animatable.value))
            }
    }
)

@Composable
fun Badge(
    text: String,
    modifier: Modifier = Modifier,
    background: Color = OctoAppTheme.colors.blackTranslucent2,
    visible: Boolean = true,
    showDot: Boolean = false
) {
    val animatedAlpha by animateFloatAsState(targetValue = if (visible) 1f else 0f)

    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .graphicsLayer { alpha = animatedAlpha }
            .then(modifier)
            .clip(ChipShape)
            .background(background)
            .padding(horizontal = OctoAppTheme.dimens.margin01, vertical = 1.dp)
    ) {
        if (showDot) {
            Box(
                modifier = Modifier
                    .padding(end = 3.dp)
                    .size(8.dp)
                    .clip(CircleShape)
                    .background(OctoAppTheme.colors.textColoredBackground)
            )
        }
        Text(
            text = text,
            style = OctoAppTheme.typography.label,
            color = OctoAppTheme.colors.textColoredBackground,
        )
    }
}

private val ChipShape
    @Composable get() = MaterialTheme.shapes.small

@Preview
@Composable
private fun PreviewLive() = OctoAppThemeForPreview {
    var hideAt by remember { mutableStateOf(System.currentTimeMillis() + 5000) }
    var animateCountDownTo by remember { mutableStateOf(System.currentTimeMillis() + 4000) }
    LaunchedEffect(Unit) {
        while (isActive) {
            delay(4500)
            hideAt = System.currentTimeMillis() + 5000
            animateCountDownTo = System.currentTimeMillis() + 4000
        }
    }
    LiveBadge(hideAt = hideAt, animateCountDownTo = animateCountDownTo)
}

@Preview
@Composable
private fun PreviewNormal() = OctoAppThemeForPreview {
    Badge(text = "1080p")
}