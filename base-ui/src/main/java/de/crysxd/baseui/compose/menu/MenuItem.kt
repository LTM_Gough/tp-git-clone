package de.crysxd.baseui.compose.menu

import android.content.Context
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.spring
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.graphics.ExperimentalAnimationGraphicsApi
import androidx.compose.animation.graphics.res.animatedVectorResource
import androidx.compose.animation.graphics.res.rememberAnimatedVectorPainter
import androidx.compose.animation.graphics.vector.AnimatedImageVector
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.DropdownMenu
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Switch
import androidx.compose.material.SwitchDefaults
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.TransformOrigin
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.asAnnotatedString
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.menu.base.MenuHost
import de.crysxd.baseui.menu.base.MenuItem
import de.crysxd.baseui.menu.base.RevolvingOptionsMenuItem
import de.crysxd.baseui.menu.base.ToggleMenuItem
import de.crysxd.baseui.menu.settings.AutomaticLightsSettingsMenuItem
import de.crysxd.baseui.menu.settings.HelpMenuItem
import de.crysxd.baseui.menu.settings.KeepScreenOnDuringPrintMenuItem
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.data.repository.PinnedMenuItemRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.launch
import timber.log.Timber

@Composable
fun MenuItem(
    item: MenuItem,
    host: MenuHost? = null,
    menuId: MenuId = MenuId.Other,
    featureEnabledDefault: Boolean = true
) {
    val feedback = LocalHapticFeedback.current
    val scope = rememberCoroutineScope()
    val isLoading = remember { mutableStateOf(false) }
    val loadingDoneAt = remember { mutableStateOf(0L) }
    val clickCompletedAt = remember { mutableStateOf(0L) }
    val canBePinned = item.canBePinned
    val isEnabled = if (OctoAppTheme.isPreview) {
        flowOf(featureEnabledDefault)
    } else {
        remember { item.billingManagerFeature?.let { BillingManager.isFeatureEnabledFlow(it) } ?: flowOf(true) }
    }.collectAsState(initial = featureEnabledDefault)
    val colors = MenuColors(
        foreground = item.getIconColorOverwrite(LocalContext.current)?.let { Color(it) } ?: colorResource(id = item.style.highlightColor),
        background = colorResource(id = item.style.backgroundColor)
    )

    //region Click action
    fun executeClickAction(action: suspend (MenuHost?) -> Unit) {
        scope.launch {
            try {
                // Set loading after a grace period of 200ms. Most items will be done instantly.
                val loadingJob = launch {
                    delay(200)
                    isLoading.value = true
                    feedback.performHapticFeedback(HapticFeedbackType.TextHandleMove)
                }

                // Perform action, then cancel loading (in case it didn't fire yet) and refresh values
                action(host)
                loadingJob.cancel()
                clickCompletedAt.value = System.currentTimeMillis()

                // If all conditions are met, play success animation
                val successConditions = host?.consumeSuccessAnimationForNextActionSuppressed() != true &&
                        item !is RevolvingOptionsMenuItem &&
                        item !is ToggleMenuItem &&
                        !item.showAsSubMenu

                if (successConditions) {
                    loadingDoneAt.value = System.currentTimeMillis()
                    delay(100)
                }
            } catch (e: Exception) {
                Timber.e(e)
                ExceptionReceivers.dispatchException(e)
            }
        }.invokeOnCompletion {
            feedback.performHapticFeedback(HapticFeedbackType.TextHandleMove)
            isLoading.value = false
        }
    }
    //endregion
    //region Pinned item repository
    val pinnedItems = try {
        BaseInjector.get().pinnedMenuItemsRepository()
    } catch (e: Exception) {
        null
    }
    //endregion
    CompositionLocalProvider(
        //region MenuContext
        LocalMenuContext provides object : MenuContext {
            override val item get() = item
            override val colors get() = colors
            override val isPinned get() = pinnedItems?.isPinned(item.itemId, menuId) == true
            override val isEnabled by isEnabled
            override var isLoading by isLoading
            override val loadingDoneAt by loadingDoneAt
            override val clickCompletedAt by clickCompletedAt
        }
        //endregion
    ) {
        val showPinPopUp = remember { mutableStateOf(false) }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(IntrinsicSize.Min)
        ) {
            MainButton(
                onClick = { executeClickAction(item::onClicked) },
                onLongClick = { showPinPopUp.value = true }.takeIf { canBePinned }
            )

            if (LocalMenuContext.current.isEnabled) {
                SecondaryButton(
                    onClick = { executeClickAction(item::onSecondaryClicked) }
                )
            }

            PinPopUpMenu(
                visible = showPinPopUp,
                itemId = item.itemId,
                localMenuId = menuId,
                pinnedItems = pinnedItems
            )
        }
    }
}

//region Popup menu
@Composable
private fun PinPopUpMenu(
    visible: MutableState<Boolean>,
    localMenuId: MenuId,
    itemId: String,
    pinnedItems: PinnedMenuItemRepository?
) {
    val pinned = remember(visible) {
        MenuId.values().associateWith {
            mutableStateOf(pinnedItems?.isPinned(itemId = itemId, menuId = it) == true)
        }
    }

    DropdownMenu(
        expanded = visible.value,
        onDismissRequest = {
            visible.value = false
            pinned.forEach {
                pinnedItems?.setMenuItemPinned(itemId = itemId, menuId = it.key, pinned = it.value.value)
            }
        },
    ) {
        Text(
            text = stringResource(id = R.string.menu_controls___pin_to_x, ""),
            style = OctoAppTheme.typography.sectionHeader,
            modifier = Modifier
                .padding(top = OctoAppTheme.dimens.margin12)
                .padding(horizontal = OctoAppTheme.dimens.margin12)
        )

        if (localMenuId != MenuId.Other) {

            PinMenuItem(menuId = null, isPinned = pinned[localMenuId]!!)
            Divider(color = OctoAppTheme.colors.lightText.copy(alpha = 0.3f))
        }

        MenuId.values()
            .filter { it != MenuId.Other && it != localMenuId }
            .forEach { PinMenuItem(menuId = it, isPinned = pinned[it]!!) }
    }
}

@Composable
private fun PinMenuItem(
    menuId: MenuId? = null,
    isPinned: MutableState<Boolean>,
) = Row(
    verticalAlignment = Alignment.CenterVertically,
    modifier = Modifier.padding(start = OctoAppTheme.dimens.margin12, end = OctoAppTheme.dimens.margin1),
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin2)
) {
    Text(
        text = menuId?.label?.toString() ?: stringResource(id = R.string.menu_controls___here),
        style = OctoAppTheme.typography.base,
        color = OctoAppTheme.colors.darkText,
        modifier = Modifier.weight(1f)
    )

    Switch(
        checked = isPinned.value,
        onCheckedChange = { isPinned.value = !isPinned.value },
    )
}

//endregion
//region Secondary button
@Composable
private fun SecondaryButton(
    onClick: (() -> Unit)? = null,
) = LocalMenuContext.current.item.secondaryButtonIcon?.let { icon ->
    Box(
        modifier = Modifier
            .padding(start = OctoAppTheme.dimens.margin01)
            .clip(MaterialTheme.shapes.small)
            .fillMaxHeight()
            .aspectRatio(1f)
            .menuButton(onClick = onClick)
    ) {
        androidx.compose.material.Icon(
            painter = painterResource(id = icon),
            contentDescription = null,
            tint = LocalMenuContext.current.colors.foreground,
            modifier = Modifier
                .size(24.dp)
                .align(Alignment.Center)
                .loadingSensitive()
        )
    }
}

//endregion
// region Main button
@Composable
private fun RowScope.MainButton(
    onClick: (() -> Unit)?,
    onLongClick: (() -> Unit)?,
) = Row(
    modifier = Modifier
        .weight(1f)
        .clip(MaterialTheme.shapes.small)
        .menuButton(onClick = onClick, onLongClick = onLongClick)
        .heightIn(min = 40.dp),
    verticalAlignment = Alignment.CenterVertically
) {
    val context = LocalMenuContext.current
    context.Icon()
    Title()

    if (context.isEnabled) {
        context.Switch()
        context.RightDetail()
        context.SubMenuIcon()
        Spacer(modifier = Modifier.width(OctoAppTheme.dimens.margin1))
    } else {
        context.FeatureDisabledIcon()
    }
}

//region Right elements
@Composable
private fun MenuContext.Switch() {
    if (item is ToggleMenuItem) {
        val isChecked = remember(clickCompletedAt) { (item as ToggleMenuItem).isChecked }
        Box(modifier = Modifier.height(24.dp)) {
            Switch(
                checked = isChecked,
                onCheckedChange = null,
                modifier = Modifier.loadingSensitive(),
                colors = SwitchDefaults.colors(
                    checkedThumbColor = colors.foreground,
                    checkedTrackColor = colors.foreground,
                ),
            )
        }
    }
}

@Composable
private fun MenuContext.SubMenuIcon() {
    if (item.showAsSubMenu) {
        androidx.compose.material.Icon(
            painter = painterResource(id = R.drawable.ic_round_chevron_right_24),
            contentDescription = null,
            tint = colors.foreground,
            modifier = Modifier.loadingSensitive(),
        )
    }
}

@Composable
private fun MenuContext.RightDetail() {
    val context = LocalContext.current
    val text = remember(clickCompletedAt) { item.getRightDetail(context) }
    text?.let {
        Text(
            text = it.asAnnotatedString(),
            style = OctoAppTheme.typography.sectionHeader,
            color = OctoAppTheme.colors.normalText,
            modifier = Modifier.loadingSensitive(),
        )
    }
}

@Composable
private fun MenuContext.FeatureDisabledIcon() = Box(
    contentAlignment = Alignment.Center,
    modifier = Modifier
        .fillMaxHeight()
        .aspectRatio(1f),
) {
    androidx.compose.material.Icon(
        modifier = Modifier
            .size(22.dp)
            .clip(CircleShape)
            .background(colors.background)
            .padding(4.dp),
        painter = painterResource(id = R.drawable.ic_round_favorite_18),
        contentDescription = null,
        tint = colors.foreground
    )
}

//endregion
//region Title
@Composable
private fun RowScope.Title() = Text(
    text = LocalMenuContext.current.item.getTitle(LocalContext.current).toString(),
    style = OctoAppTheme.typography.buttonSmall,
    color = OctoAppTheme.colors.normalText,
    modifier = Modifier
        .padding(horizontal = OctoAppTheme.dimens.margin1, vertical = OctoAppTheme.dimens.margin01 + 2.dp)
        .weight(1f)
        .loadingSensitive(scaleStart = true),
)


//endregion
//region Left elements
@Composable
@OptIn(ExperimentalAnimationApi::class, ExperimentalAnimationGraphicsApi::class)
private fun MenuContext.Icon() {
    var height by remember { mutableStateOf(0.dp) }
    val density = LocalDensity.current
    Box(
        modifier = Modifier
            .fillMaxHeight()
            .height(IntrinsicSize.Min)
            .aspectRatio(1f, matchHeightConstraintsFirst = false)
            .background(colors.background, CircleShape)
            .onGloballyPositioned { height = with(density) { it.size.height.toDp() } }
    ) {
        val badgeSize = 12.dp
        val scaleSpec = spring<Float>()
        val enterTransition = scaleIn(animationSpec = scaleSpec, initialScale = 0.66f) + fadeIn()
        val exitTransition = scaleOut(animationSpec = scaleSpec, targetScale = 0.66f) + fadeOut()
        val mainModifier = {
            Modifier
                .size(24.dp)
                .align(Alignment.Center)
        }

        //region Icon
        AnimatedVisibility(
            visible = !isLoading,
            enter = enterTransition,
            exit = exitTransition,
            modifier = mainModifier()
        ) {
            androidx.compose.material.Icon(
                painter = painterResource(id = item.icon),
                contentDescription = null,
                tint = colors.foreground,

                )
        }
        //endregion
        //region Laoding spinner
        AnimatedVisibility(
            visible = isLoading,
            enter = enterTransition,
            exit = exitTransition,
            modifier = mainModifier()
        ) {
            CircularProgressIndicator(
                color = colors.foreground,
                strokeWidth = 2.dp,
                modifier = Modifier
                    .size(24.dp)
                    .padding(2.dp),
            )
        }
        //endregion
        //region Confirm
        val vector = AnimatedImageVector.animatedVectorResource(R.drawable.menu_item_success)
        var atEnd by remember { mutableStateOf(true) }
        LaunchedEffect(loadingDoneAt) {
            if (loadingDoneAt > 0) atEnd = false
            delay(1000)
            atEnd = true
        }
        Image(
            painter = rememberAnimatedVectorPainter(animatedImageVector = vector, atEnd = atEnd),
            contentDescription = null,
            modifier = Modifier.size(height),
        )
        //endregion
        //region Badges
        AnimatedVisibility(
            visible = !isLoading,
            exit = fadeOut(),
            enter = fadeIn(),
            modifier = Modifier
                .height(badgeSize)
                .aspectRatio(1f)
                .offset(y = -(1.dp), x = 2.dp)
                .clip(CircleShape)
                .align(Alignment.BottomEnd),
        ) {
            when {
                item.getBadgeCount() > 0 -> Text(
                    text = item.getBadgeCount().toString(),
                    style = OctoAppTheme.typography.labelSmall.copy(fontSize = with(density) { badgeSize.toSp() }),
                    color = Color.White,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxSize()
                        .background(OctoAppTheme.colors.red)
                        .offset(y = -(2.dp))
                )

                isPinned -> androidx.compose.material.Icon(
                    painter = painterResource(id = R.drawable.ic_round_push_pin_10),
                    contentDescription = null,
                    tint = OctoAppTheme.colors.menuPinForeground,
                    modifier = Modifier
                        .fillMaxSize()
                        .background(OctoAppTheme.colors.menuPinBackground)
                        .padding(1.dp)
                )
            }
        }
        //endregion
    }
}

//endregion
//endregion
//region Modifiers
@OptIn(ExperimentalFoundationApi::class)
private fun Modifier.menuButton(onLongClick: (() -> Unit)? = null, onClick: (() -> Unit)?) = composed {
    val context = LocalMenuContext.current
    this
        .combinedClickable(
            enabled = context.isEnabled && !context.isLoading,
            interactionSource = MutableInteractionSource(),
            indication = rememberRipple(color = context.colors.background),
            onClick = onClick.takeIf { context.isEnabled && !context.isLoading } ?: {},
            onLongClick = onLongClick
        )
        .background(if (LocalMenuContext.current.item.showAsOutlined) Color.Companion.Transparent else context.colors.background)
        .border(
            shape = MaterialTheme.shapes.small,
            color = if (LocalMenuContext.current.item.showAsOutlined) context.colors.foreground else Color.Transparent,
            width = 1.dp
        )
}

private fun Modifier.loadingSensitive(scaleStart: Boolean = false) = composed {
    val isLoading = LocalMenuContext.current.isLoading
    val alpha by animateFloatAsState(if (isLoading) LoadingAlpha else 1f)
    val scale by animateFloatAsState(if (isLoading) LoadingScale else 1f)

    this.graphicsLayer {
        this.alpha = alpha
        this.scaleY = scale
        this.scaleX = scale
        this.transformOrigin = TransformOrigin(pivotFractionX = if (scaleStart) 0f else 0.5f, pivotFractionY = 0.5f)
    }
}


private val LoadingAlpha = 0.33f
private val LoadingScale = 1f

//endregion
//region Context
val LocalMenuContext = compositionLocalOf<MenuContext> { throw UninitializedPropertyAccessException() }

interface MenuContext {
    val item: MenuItem
    val colors: MenuColors
    val isPinned: Boolean
    val isEnabled: Boolean
    var isLoading: Boolean
    val loadingDoneAt: Long
    val clickCompletedAt: Long
}

data class MenuColors(
    val foreground: Color,
    val background: Color
)

//endregion
//region Preview
@Composable
@Preview(group = "Items")
private fun PreviewHelpItem() = OctoAppThemeForPreview {
    MenuItem(item = HelpMenuItem())
}

@Composable
@Preview(group = "Items")
private fun PreviewLightItem() = OctoAppThemeForPreview {
    MenuItem(item = object : MenuItem by AutomaticLightsSettingsMenuItem() {
        override fun getBadgeCount() = 1
    })
}

@Composable
@Preview(group = "Items")
private fun PreviewKeepScreenOn() = OctoAppThemeForPreview {
    var isChecked by remember { mutableStateOf(false) }
    MenuItem(
        item = object : ToggleMenuItem by KeepScreenOnDuringPrintMenuItem() {
            override val isChecked = isChecked
            override suspend fun onClicked(host: MenuHost?) {
                isChecked = !isChecked
            }
        },
    )
}

@Composable
@Preview(group = "Items")
private fun PreviewDisabled() = OctoAppThemeForPreview {
    MenuItem(item = HelpMenuItem(), featureEnabledDefault = false)
}

@Composable
@Preview(group = "Items")
private fun PreviewRightDetail() = OctoAppThemeForPreview {
    MenuItem(item = object : MenuItem by AutomaticLightsSettingsMenuItem() {
        override fun getRightDetail(context: Context) = "3 configured"
        override val showAsSubMenu = false
    })
}

@Composable
@Preview(group = "Items")
private fun PreviewSecondaryButton() = OctoAppThemeForPreview {
    MenuItem(item = object : MenuItem by AutomaticLightsSettingsMenuItem() {
        override val secondaryButtonIcon = R.drawable.ic_round_delete_24
    })
}

@Composable
@Preview(group = "Items")
private fun PreviewColorOverride() = OctoAppThemeForPreview {
    MenuItem(item = object : MenuItem by AutomaticLightsSettingsMenuItem() {
        override fun getIconColorOverwrite(context: Context) = android.graphics.Color.RED
    })
}
//endregion