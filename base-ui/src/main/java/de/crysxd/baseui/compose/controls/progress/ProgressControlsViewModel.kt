package de.crysxd.baseui.compose.controls.progress

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.job.JobInformation
import de.crysxd.octoapp.engine.octoprint.dto.message.CompanionPluginMessage
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import timber.log.Timber

@OptIn(ExperimentalCoroutinesApi::class)
class ProgressControlsViewModel(
    private val instanceId: String,
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPreferences: OctoPreferences,
) : ViewModel() {

    companion object {
        const val UPDATE_INTERVAL = 1000L
    }

    private val settings = octoPreferences.updatedFlow.map { octoPreferences.progressWidgetSettings }
    private val fullscreenSettings = octoPreferences.updatedFlow.map { octoPreferences.webcamFullscreenProgressWidgetSettings }
    private val companionMessage = flow {
        val actual = octoPrintProvider.passiveCachedMessageFlow(
            instanceId = instanceId,
            tag = "progress-controls",
            clazz = CompanionPluginMessage::class
        )

        emit(null)
        emitAll(actual)
    }
    private val resolvedFile = octoPrintProvider.passiveCurrentMessageFlow(instanceId = instanceId, tag = "progress-controls-file")
        .map { it.job?.file }
        .distinctUntilChangedBy { it?.path }
        .flatMapLatest { inFile ->
            resolveFile(inFile)
        }
    private val currentMessage = flow {
        emit(null)
        emitAll(octoPrintProvider.passiveCurrentMessageFlow(instanceId = instanceId, tag = "progress-controls"))
    }
    var stateCache: ViewState = ViewState(
        settings = octoPreferences.progressWidgetSettings,
        fullscreenSettings = octoPreferences.webcamFullscreenProgressWidgetSettings
    )
        private set
    val state = combine(settings, fullscreenSettings, currentMessage, companionMessage, resolvedFile) { parts ->
        ViewState(
            settings = parts[0] as ProgressWidgetSettings,
            fullscreenSettings = parts[1] as ProgressWidgetSettings,
            currentMessage = parts[2] as Message.Current?,
            companionMessage = parts[3] as CompanionPluginMessage?,
            resolvedFile = parts[4] as FileObject.File?,
        )
    }.onEach {
        stateCache = it
    }

    private fun resolveFile(fileObject: JobInformation.JobFile?) = flow {
        emit(null)
        fileObject ?: return@flow
        val origin = requireNotNull(fileObject.origin) { "No file origin" }
        emit(octoPrintProvider.octoPrint(instanceId = instanceId).filesApi.getFile(fileObject.origin, fileObject.path))
    }.catch {
        Timber.e("Failed to resolve file object", it)
        emit(fileObject?.asBasicFile())
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "ProgressControlsViewModel@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = ProgressControlsViewModel(
            instanceId = instanceId,
            octoPrintProvider = BaseInjector.get().octoPrintProvider(),
            octoPreferences = BaseInjector.get().octoPreferences(),
        ) as T
    }

    data class ViewState(
        val settings: ProgressWidgetSettings,
        val fullscreenSettings: ProgressWidgetSettings,
        val currentMessage: Message.Current? = null,
        val companionMessage: CompanionPluginMessage? = null,
        val resolvedFile: FileObject.File? = null,
    ) {
        fun settingsForRole(role: ProgressControlsRole) = when (role) {
            ProgressControlsRole.ForNormal -> settings
            ProgressControlsRole.ForWebcamFullscreen -> fullscreenSettings
        }
    }
}