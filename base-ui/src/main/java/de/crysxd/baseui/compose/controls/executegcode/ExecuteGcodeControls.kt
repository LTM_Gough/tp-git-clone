package de.crysxd.baseui.compose.controls.executegcode

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.controls.GenericHistoryItemsControl
import de.crysxd.baseui.compose.controls.GenericHistoryItemsControlState
import de.crysxd.baseui.compose.framework.LocalOctoPrint
import de.crysxd.baseui.compose.framework.collectAsStateWhileActive
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.menu.base.MenuBottomSheetFragment
import de.crysxd.baseui.menu.gcodeshortcuts.GcodeShortcutsMenu
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.GcodeHistoryItem

@Composable
fun ExecuteGcodeControls(
    editState: EditState,
    state: ExecuteGcodeControlsState,
) = GenericHistoryItemsControl(
    editState = editState,
    state = state,
    title = stringResource(id = R.string.send_gcode),
    otherOptionText = stringResource(id = R.string.open_terminal),
    onOtherOptionClicked = { UriLibrary.getTerminalUri().open() },
    isPinned = { it.isFavorite },
    text = { it.name }
)

@Composable
fun rememberExecuteGcodeControlsState(active: Boolean): ExecuteGcodeControlsState {
    val fragmentManager = OctoAppTheme.fragmentManager

    val vmFactory = ExecuteGcodeControlsViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = ExecuteGcodeControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory
    )


    val items = vm.items.collectAsStateWhileActive(active = active, key = "execute", initial = emptyList())
    val visible = vm.visible.collectAsStateWhileActive(active = active, key = "execute2", initial = false)

    return object : ExecuteGcodeControlsState {
        override val visible by visible
        override val items by items

        override fun onClick(item: GcodeHistoryItem) {
            vm.executeGcodeCommand(item)
        }

        override fun onLongClick(item: GcodeHistoryItem) {
            MenuBottomSheetFragment.createForMenu(GcodeShortcutsMenu(item)).show(fragmentManager())
        }
    }
}

interface ExecuteGcodeControlsState : GenericHistoryItemsControlState<GcodeHistoryItem>

//region Previews
@Preview
@Composable
private fun Preview() = OctoAppThemeForPreview {
    ExecuteGcodeControls(
        editState = EditState.ForPreview,
        state = object : ExecuteGcodeControlsState {
            override val visible = true

            override val items: List<GcodeHistoryItem>
                get() = listOf(
                    GcodeHistoryItem(command = "G90", isFavorite = true),
                    GcodeHistoryItem(command = "G28"),
                    GcodeHistoryItem(command = "G29"),
                    GcodeHistoryItem(command = "M500"),
                    GcodeHistoryItem(command = "M501"),
                    GcodeHistoryItem(command = "M502"),
                )

            override fun onLongClick(item: GcodeHistoryItem) = Unit
            override fun onClick(item: GcodeHistoryItem) = Unit
        }
    )
}
//endregion