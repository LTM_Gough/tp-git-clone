package de.crysxd.baseui.compose.controls.tune

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.fragment.app.Fragment
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.fragment.navArgs
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.ComposeContent
import de.crysxd.baseui.compose.framework.LocalOctoPrint
import de.crysxd.baseui.compose.framework.collectAsStateWhileActive
import de.crysxd.baseui.ext.requireOctoActivity

class TuneControlsFragment : Fragment() {

    private var active by mutableStateOf(false)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeContent(instanceId = navArgs<TuneControlsFragmentArgs>().value.instanceId) {
        val vmFactory = TuneControlsViewModel.Factory(LocalOctoPrint.current.id)
        val vm = viewModel(
            modelClass = TuneControlsViewModel::class.java,
            key = vmFactory.id,
            factory = vmFactory
        )

        val editState by vm.editState.collectAsState()
        val uiState by vm.uiState.collectAsStateWhileActive(
            active = active,
            initial = TuneControlsViewModel.UiState(),
            key = "tune-controls-details"
        )

        LaunchedEffect(Unit) { vm.pollSettingsNow() }

        LaunchedEffect(editState.operationCompleted) {
            if (editState.operationCompleted) {
                requireOctoActivity().showSnackbar(
                    OctoActivity.Message.SnackbarMessage(
                        text = { getString(R.string.menu___completed_command, getString(R.string.tune)) },
                        type = OctoActivity.Message.SnackbarMessage.Type.Positive,
                    )
                )
            }
        }

        TuneControlsView(
            uiState = uiState,
            editState = editState,
            onStepUp = vm::babyStepUp,
            onStepDown = vm::babyStepDown,
            onApplyValues = vm::applyChanges
        )
    }

    override fun onResume() {
        super.onResume()
        active = true
    }

    override fun onPause() {
        super.onPause()
        active = false
    }
}