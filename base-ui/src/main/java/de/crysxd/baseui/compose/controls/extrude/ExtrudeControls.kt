package de.crysxd.baseui.compose.controls.extrude

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.controls.GenericHistoryItemsControl
import de.crysxd.baseui.compose.controls.GenericHistoryItemsControlState
import de.crysxd.baseui.compose.framework.LocalOctoPrint
import de.crysxd.baseui.compose.framework.collectAsStateWhileActive
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.base.data.models.ExtrusionHistoryItem
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive

@Composable
fun ExtrudeControls(
    editState: EditState,
    state: ExtrudeControlsState,
) = GenericHistoryItemsControl(
    editState = editState,
    state = state,
    title = stringResource(id = R.string.widget_extrude),
    otherOptionText = stringResource(id = R.string.other),
    onOtherOptionClicked = { state.extrudeOther() },
    isPinned = { it.isFavorite },
    text = { getString(R.string.x_mm, it.distanceMm.toString()) }
)

@Composable
fun rememberExtrudeControlsState(active: Boolean): ExtrudeControlsState {
    val context = LocalContext.current
    val navController = OctoAppTheme.navController

    val vmFactory = ExtrudeControlsViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = ExtrudeControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory
    )

    val items = vm.items.collectAsStateWhileActive(active = active, key = "extrude", initial = emptyList())
    val visible = vm.visible.collectAsStateWhileActive(active = active, key = "extrude2", initial = false)

    return object : ExtrudeControlsState {
        override val visible by visible
        override val items by items

        override fun onClick(item: ExtrusionHistoryItem) {
            vm.extrude(distanceMm = item.distanceMm)
        }

        override fun onLongClick(item: ExtrusionHistoryItem) {
            vm.toggleFavourite(distanceMm = item.distanceMm)
        }

        override fun extrudeOther() {
            vm.extrudeOther(context = context, navContoller = navController())
        }
    }
}

interface ExtrudeControlsState : GenericHistoryItemsControlState<ExtrusionHistoryItem> {
    fun extrudeOther()
}

//region Previews
@Preview
@Composable
private fun Preview() = OctoAppThemeForPreview {
    var items by remember {
        mutableStateOf(
            listOf(
                ExtrusionHistoryItem(-10),
                ExtrusionHistoryItem(10),
                ExtrusionHistoryItem(50),
                ExtrusionHistoryItem(100),
                ExtrusionHistoryItem(120),
            )
        )
    }

    LaunchedEffect(Unit) {
        while (isActive) {
            delay(1000)
            items = listOf(
                ExtrusionHistoryItem(10),
                ExtrusionHistoryItem(-10),
                ExtrusionHistoryItem(50),
                ExtrusionHistoryItem(100),
                ExtrusionHistoryItem(120),
            )
            delay(1000)
            items = listOf(
                ExtrusionHistoryItem(-10),
                ExtrusionHistoryItem(10),
                ExtrusionHistoryItem(50),
                ExtrusionHistoryItem(100),
                ExtrusionHistoryItem(120),
            )
        }
    }


    ExtrudeControls(
        editState = EditState.ForPreview,
        state = object : ExtrudeControlsState {
            override val visible = true
            override val items = items
            override fun onClick(item: ExtrusionHistoryItem) = Unit
            override fun onLongClick(item: ExtrusionHistoryItem) = Unit
            override fun extrudeOther() = Unit
        }
    )
}
//endregion