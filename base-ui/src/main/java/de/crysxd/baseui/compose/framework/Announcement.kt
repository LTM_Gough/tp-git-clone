package de.crysxd.baseui.compose.framework

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.content.edit
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.base.di.BaseInjector

@Composable
fun AnnouncementController(
    announcementId: AnnouncementId?,
    content: @Composable AnnouncementScope.() -> Unit,
) {
    val initialVisible = checkIfVisible(announcementId)
    var visible by remember(initialVisible) { mutableStateOf(initialVisible) }

    AnimatedVisibility(
        visible = visible,
        enter = fadeIn() + expandVertically(),
        exit = fadeOut() + shrinkVertically()
    ) {
        object : AnnouncementScope {
            override fun hideAnnouncement() {
                announcementId?.let(::hideAnnouncement)
                visible = false
            }
        }.content()
    }
}

sealed class AnnouncementId(open val preferenceKey: String) {
    data class PreferencesKey(override val preferenceKey: String) : AnnouncementId(preferenceKey)
    data class Default(val announcementId: String) : AnnouncementId("announcement_${announcementId}_hidden")
}

@Composable
fun AnnouncementScope.LargeAnnouncement(
    modifier: Modifier = Modifier,
    title: String,
    text: String,
    hideButton: String = stringResource(id = R.string.hide),
    learnMoreButton: String? = null,
    onLearnMore: () -> Unit = {},
) = Column(
    modifier = modifier
        .clip(MaterialTheme.shapes.large)
        .background(OctoAppTheme.colors.inputBackground)
        .fillMaxWidth()
) {
    Text(
        text = title,
        style = OctoAppTheme.typography.subtitle,
        color = OctoAppTheme.colors.darkText,
        modifier = Modifier
            .padding(top = OctoAppTheme.dimens.margin2)
            .padding(horizontal = OctoAppTheme.dimens.margin2)
    )

    Text(
        text = text,
        style = OctoAppTheme.typography.base,
        color = OctoAppTheme.colors.lightText,
        modifier = Modifier
            .padding(top = OctoAppTheme.dimens.margin0)
            .padding(horizontal = OctoAppTheme.dimens.margin2)
    )

    LinkButtonRow(
        modifier = Modifier
            .align(Alignment.End)
            .padding(bottom = OctoAppTheme.dimens.margin0, end = OctoAppTheme.dimens.margin01),
        texts = listOfNotNull(
            learnMoreButton,
            hideButton,
        ),
        onClicks = listOfNotNull(
            onLearnMore.takeIf { learnMoreButton != null },
            { hideAnnouncement() }
        )
    )
}

@Composable
fun AnnouncementScope.SmallAnnouncement(
    modifier: Modifier = Modifier,
    text: CharSequence,
    hideButton: String = stringResource(id = R.string.hide),
    learnMoreButton: String? = null,
    onLearnMore: () -> Unit = {},
    foregroundColor: Color = OctoAppTheme.colors.primaryButtonBackground,
    backgroundColor: Color = OctoAppTheme.colors.inputBackground,
) = Row(
    verticalAlignment = Alignment.CenterVertically,
    modifier = modifier
        .clip(MaterialTheme.shapes.large)
        .background(backgroundColor)
        .fillMaxWidth()
) {
    Text(
        text = text.asAnnotatedString(),
        style = OctoAppTheme.typography.base,
        color = OctoAppTheme.colors.darkText,
        modifier = Modifier
            .weight(1f)
            .padding(vertical = OctoAppTheme.dimens.margin2)
            .padding(start = OctoAppTheme.dimens.margin2)
    )

    LinkButtonRow(
        color = foregroundColor,
        texts = listOfNotNull(
            learnMoreButton,
            hideButton,
        ),
        onClicks = listOfNotNull(
            onLearnMore.takeIf { learnMoreButton != null },
            { hideAnnouncement() }
        )
    )
}

//region Internals
private fun checkIfVisible(announcementId: AnnouncementId?): Boolean {
    announcementId ?: return false

    val prefs = try {
        BaseInjector.get().sharedPreferences()
    } catch (e: Exception) {
        // Preview
        return true
    }

    val isAnnouncementHidden = prefs.getBoolean(announcementId.preferenceKey, false)
    return !isAnnouncementHidden
}

private fun hideAnnouncement(announcementId: AnnouncementId) {
    val prefs = try {
        BaseInjector.get().sharedPreferences()
    } catch (e: Exception) {
        // Preview
        return
    }

    prefs.edit { putBoolean(announcementId.preferenceKey, true) }
}

interface AnnouncementScope {
    fun hideAnnouncement()
}
//endregion

//region Preview
@Preview
@Composable
private fun PreviewLargeWithLearnMore() = OctoAppThemeForPreview {
    AnnouncementController(announcementId = AnnouncementId.Default("test")) {
        LargeAnnouncement(
            title = stringResource(id = R.string.shortcut_tutorial_line1),
            text = stringResource(id = R.string.shortcut_tutorial_line2),
            learnMoreButton = stringResource(id = R.string.learn_more),
            onLearnMore = {}
        )
    }
}

@Preview
@Composable
private fun PreviewLarge() = OctoAppThemeForPreview {
    AnnouncementController(announcementId = AnnouncementId.Default("test")) {
        LargeAnnouncement(
            title = stringResource(id = R.string.shortcut_tutorial_line1),
            text = stringResource(id = R.string.shortcut_tutorial_line2),
        )
    }
}

@Preview
@Composable
private fun PreviewSmallWithLearnMore() = OctoAppThemeForPreview {
    AnnouncementController(announcementId = AnnouncementId.Default("test")) {
        SmallAnnouncement(
            text = stringResource(id = R.string.announcement___new_version_title),
            learnMoreButton = stringResource(id = R.string.announcement___new_version_learn_more),
            onLearnMore = {}
        )
    }
}

@Preview
@Composable
private fun PreviewSmall() = OctoAppThemeForPreview {
    AnnouncementController(announcementId = AnnouncementId.Default("test")) {
        SmallAnnouncement(
            text = stringResource(id = R.string.shortcut_tutorial_line2),
        )
    }
}
//endregion