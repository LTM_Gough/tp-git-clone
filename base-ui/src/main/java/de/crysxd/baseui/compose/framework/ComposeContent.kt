package de.crysxd.baseui.compose.framework

import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.requireOctoActivity

@Suppress("FunctionName")
fun Fragment.ComposeContent(
    instanceId: String?,
    block: @Composable () -> Unit
): View {
    val view = ComposeView(requireContext())
    view.layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
    view.setContent {
        OctoAppTheme(
            viewModelStoreOwner = requireActivity(),
            octoActivity = requireOctoActivity(),
            fragmentManager = { childFragmentManager },
        ) {
            WithOctoPrint(instanceId = instanceId) {
                block()
            }
        }
    }
    return view
}