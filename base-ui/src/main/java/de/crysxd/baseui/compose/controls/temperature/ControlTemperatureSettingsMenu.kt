package de.crysxd.baseui.compose.controls.temperature

import android.content.Context
import de.crysxd.baseui.R
import de.crysxd.baseui.menu.base.Menu
import de.crysxd.baseui.menu.base.MenuHost
import de.crysxd.baseui.menu.base.MenuItem
import de.crysxd.baseui.menu.base.MenuItemStyle
import de.crysxd.baseui.menu.base.ToggleMenuItem
import de.crysxd.octoapp.base.di.BaseInjector
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.first
import kotlinx.parcelize.Parcelize


@Parcelize
class ControlTemperatureSettingsMenu : Menu {

    override suspend fun getMenuItem(): List<MenuItem> = BaseInjector.get().temperatureDataRepository().flow()
        .combine(BaseInjector.get().octorPrintRepository().instanceInformationFlow()) { temps, instance ->
            val profile = instance?.activeProfile ?: return@combine temps
            temps.filter {
                val isChamber = it.component == "chamber" && profile.heatedChamber
                val isBed = it.component == "bed" && profile.heatedBed
                val isTool = it.component.startsWith("tool") && (!profile.extruder.sharedNozzle || it.component == "tool0")
                val isOther = it.component != "chamber" && it.component != "bed" && !it.component.startsWith("tool")
                isOther || isTool || isChamber || isBed
            }
        }
        .first()
        .mapIndexed { index, it ->
            AdditionalTemperatureMenuItem(it.component, it.canControl, index)
        }

    override suspend fun getTitle(context: Context) = context.getString(R.string.temperature_settings_menu___title)
    override fun getBottomText(context: Context) = context.getString(R.string.temperature_settings_menu___description)

    private class AdditionalTemperatureMenuItem(val componentName: String, canControl: Boolean, override val order: Int) : ToggleMenuItem {
        override val isChecked
            get() = BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()
                ?.appSettings?.hiddenTemperatureComponents?.contains(componentName) != true

        override val itemId = "additional_temperature/$componentName"
        override var groupId = if (canControl) "primary" else "additional"
        override val style = MenuItemStyle.Settings
        override val icon = R.drawable.ic_round_local_fire_department_24
        override fun getTitle(context: Context) = context.getString(R.string.temperature_settings_menu___show_x, getComponentName(context, componentName))

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            BaseInjector.get().octorPrintRepository().updateAppSettingsForActive {
                it.copy(
                    hiddenTemperatureComponents = if (enabled) {
                        it.hiddenTemperatureComponents - componentName
                    } else {
                        it.hiddenTemperatureComponents + componentName
                    }
                )
            }

        }

        private fun getComponentName(context: Context, component: String) = when (component) {
            "tool0" -> context.getString(R.string.general___hotend)
            "tool1" -> context.getString(R.string.general___hotend_2)
            "tool3" -> context.getString(R.string.general___hotend_3)
            "tool4" -> context.getString(R.string.general___hotend_4)
            "bed" -> context.getString(R.string.general___bed)
            "chamber" -> context.getString(R.string.general___chamber)
            else -> component
        }
    }
}
