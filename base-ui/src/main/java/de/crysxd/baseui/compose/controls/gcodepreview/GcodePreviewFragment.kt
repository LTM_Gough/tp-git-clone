package de.crysxd.baseui.compose.controls.gcodepreview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.fragment.navArgs
import de.crysxd.baseui.common.OctoToolbar
import de.crysxd.baseui.common.gcode.GcodeSettingsMenu
import de.crysxd.baseui.compose.framework.ComposeContent
import de.crysxd.baseui.compose.framework.LocalOctoPrint
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.baseui.menu.base.MenuBottomSheetFragment

class GcodePreviewFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeContent(navArgs<GcodePreviewFragmentArgs>().value.instanceId) {
        val vmFactory = GcodePreviewControlsViewModel.Factory(LocalOctoPrint.current.id)
        val vm = viewModel(
            modelClass = GcodePreviewControlsViewModel::class.java,
            key = vmFactory.id,
            factory = vmFactory
        )

        val scope = remember {
            object : GcodePreviewScope {
                override fun setManualProgress(layerIndex: Int, layerProgress: Float) = vm.useManual(layerIndex = layerIndex, layerProgress = layerProgress)
                override fun onRetry() = vm.retry()
                override fun onDownload() = vm.allowLargeDownloads()
                override fun syncWithLive() = vm.useLive()
                override fun showSettings() = MenuBottomSheetFragment.createForMenu(GcodeSettingsMenu()).show(childFragmentManager)
            }
        }

        val state by vm.state.collectAsState(GcodePreviewControlsViewModel.ViewState.Loading())
        scope.GcodePreviewView(state = state)
    }

    override fun onResume() {
        super.onResume()
        requireOctoActivity().run {
            controlCenter.disableForLifecycle(viewLifecycleOwner.lifecycle)
            octoToolbar.state = OctoToolbar.State.Hidden
            octo.isVisible = false
        }
    }
}