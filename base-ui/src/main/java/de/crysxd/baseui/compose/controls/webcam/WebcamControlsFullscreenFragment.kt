package de.crysxd.baseui.compose.controls.webcam

import android.content.pm.ActivityInfo
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.unit.Dp
import androidx.core.view.WindowInsetsControllerCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.crysxd.baseui.InsetAwareScreen
import de.crysxd.baseui.common.OctoToolbar
import de.crysxd.baseui.compose.controls.gcodepreview.rememberGcodeControlsState
import de.crysxd.baseui.compose.controls.progress.ProgressControlsRole
import de.crysxd.baseui.compose.controls.progress.ProgressControlsSettingsMenu
import de.crysxd.baseui.compose.controls.progress.rememberProgressControlsState
import de.crysxd.baseui.compose.framework.ComposeContent
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.baseui.menu.base.MenuBottomSheetFragment

class WebcamControlsFullscreenFragment : Fragment(), InsetAwareScreen {

    private val orientationViewModel by lazy { ViewModelProvider(this)[OrientationViewModel::class.java] }
    private var active by mutableStateOf(false)
    private var insets by mutableStateOf(PaddingValues())
    private val windowController by lazy {
        WindowInsetsControllerCompat(requireActivity().window, requireView())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        orientationViewModel.init(requireOctoActivity().requestedOrientation)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeContent(instanceId = navArgs<WebcamControlsFullscreenFragmentArgs>().value.instanceId) {
        val webcamState = rememberWebcamControlsState(active = active, isFullscreen = true, navController = remember { findNavController() })
        val progressState = rememberProgressControlsState(active = active)
        val gcodeState = rememberGcodeControlsState(
            active = active,
            navController = remember { findNavController() },
            isPrinting = progressState.current.currentMessage?.state?.flags?.isPrinting() == true
        )
        WebcamControlsFullscreenView(
            onNativeAspectRatioChanged = ::onNativeAspectRatioChanged,
            insets = insets,
            webcamState = webcamState,
            gcodeState = gcodeState,
            progressState = progressState,
            onShowProgressSettings = ::onShowProgressSettings
        )
    }

    private fun onShowProgressSettings() {
        MenuBottomSheetFragment.createForMenu(ProgressControlsSettingsMenu(role = ProgressControlsRole.ForWebcamFullscreen)).show(childFragmentManager)
    }

    private fun onNativeAspectRatioChanged(frameAspectRatio: Float) {
        val screenAspectRatio = resources.displayMetrics.run { widthPixels / heightPixels.toFloat() }

        orientationViewModel.preferredOrientation = if ((frameAspectRatio < 1 && screenAspectRatio > 1) || (frameAspectRatio > 1 && screenAspectRatio < 1)) {
            // Oh no! if we rotate the screen, the image would fit better!
            ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
        } else {
            // Aspect ratio of screen and frame match, do not change
            requireActivity().requestedOrientation
        }
        requireActivity().requestedOrientation = orientationViewModel.preferredOrientation
    }

    override fun onStart() {
        super.onStart()
        active = true

        with(requireOctoActivity()) {
            octoToolbar.state = OctoToolbar.State.Hidden
            octo.isVisible = false

            window.statusBarColor = Color.Transparent.toArgb()
            window.navigationBarColor = Color.Transparent.toArgb()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                window.navigationBarDividerColor = Color.White.copy(alpha = 0.5f).toArgb()
            }

            requestedOrientation = orientationViewModel.preferredOrientation
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            controlCenter.disableForLifecycle(this@WebcamControlsFullscreenFragment.lifecycle)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                window.isStatusBarContrastEnforced = false
                window.isNavigationBarContrastEnforced = false
            }
        }

        with(windowController) {
            systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            isAppearanceLightNavigationBars = false
            isAppearanceLightStatusBars = false
        }
    }

    override fun onStop() {
        super.onStop()
        active = false

        with(requireActivity()) {
            requestedOrientation = orientationViewModel.requestedOrientationBackup
            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }
    }

    override fun handleInsets(insets: Rect) {
        context?.let {
            fun Int.toDp() = Dp(this / it.resources.displayMetrics.density)
            this.insets = PaddingValues(
                top = insets.top.toDp(),
                bottom = insets.bottom.toDp(),
                start = insets.left.toDp(),
                end = insets.right.toDp(),
            )
        }
    }

    class OrientationViewModel : ViewModel() {
        private val default = -1000
        var preferredOrientation = default
        var requestedOrientationBackup = 0
            private set

        fun init(orientation: Int) {
            if (preferredOrientation == default) {
                preferredOrientation = orientation
                requestedOrientationBackup = orientation
            }
        }
    }
}