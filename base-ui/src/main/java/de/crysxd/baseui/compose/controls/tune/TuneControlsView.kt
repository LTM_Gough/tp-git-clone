package de.crysxd.baseui.compose.controls.tune


import androidx.compose.animation.Crossfade
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.R
import de.crysxd.baseui.common.OctoToolbar
import de.crysxd.baseui.compose.framework.AnnouncementController
import de.crysxd.baseui.compose.framework.AnnouncementId
import de.crysxd.baseui.compose.framework.HeaderSynchronizedLazyColumn
import de.crysxd.baseui.compose.framework.IconAction
import de.crysxd.baseui.compose.framework.InputField
import de.crysxd.baseui.compose.framework.LargeAnnouncement
import de.crysxd.baseui.compose.framework.PrimaryButton
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import java.text.NumberFormat

@Composable
fun TuneControlsView(
    uiState: TuneControlsViewModel.UiState,
    editState: TuneControlsViewModel.EditState,
    onStepUp: () -> Unit,
    onStepDown: () -> Unit,
    onApplyValues: (Int?, Int?, Int?) -> Unit
) {
    var feedRate by remember { mutableStateOf(TextFieldValue()) }
    var flowRate by remember { mutableStateOf(TextFieldValue()) }
    var fanSpeed by remember { mutableStateOf(TextFieldValue()) }

    val apply = {
        onApplyValues(
            feedRate.text.toIntOrNull(),
            flowRate.text.toIntOrNull(),
            fanSpeed.text.toIntOrNull()
        )
    }

    HeaderSynchronizedLazyColumn(
        octoActivity = OctoAppTheme.octoActivity,
        toolbarState = OctoToolbar.State.Print,
    ) {
        disclaimer()
        feedRate(uiState = uiState, editState = editState, value = feedRate, onValueChanged = { feedRate = it }, onApplyValue = apply)
        spacer()
        flowRate(uiState = uiState, editState = editState, value = flowRate, onValueChanged = { flowRate = it }, onApplyValue = apply)
        spacer()
        fanSpeed(uiState = uiState, editState = editState, value = fanSpeed, onValueChanged = { fanSpeed = it }, onApplyValue = apply)
        spacer()
        babyStep(uiState = uiState, onStepUp = onStepUp, onStepDown = onStepDown)
        spacer()
        button(onApplyValue = apply, editState = editState)
    }
}

//region Internals
private fun LazyListScope.button(
    onApplyValue: () -> Unit,
    editState: TuneControlsViewModel.EditState,
) = item("button") {
    val focusManager = LocalFocusManager.current
    PrimaryButton(
        text = stringResource(id = R.string.tune_print___apply),
        onClick = {
            onApplyValue()
            focusManager.clearFocus()
        },
        loading = editState.loading,
    )
}

private fun LazyListScope.spacer() = item {
    Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin12))
}

private fun LazyListScope.disclaimer() = item("disclaimer") {
    AnnouncementController(announcementId = AnnouncementId.Default("tune_data_disclaimer")) {
        LargeAnnouncement(
            title = stringResource(id = R.string.tune_print___disclaimer_title),
            text = stringResource(id = R.string.tune_print___disclaimer_detail),
            hideButton = stringResource(id = R.string.tune_print___disclaimer_action_hide),
            modifier = Modifier.padding(bottom = OctoAppTheme.dimens.margin3),
        )
    }
}

private fun LazyListScope.feedRate(
    uiState: TuneControlsViewModel.UiState,
    editState: TuneControlsViewModel.EditState,
    value: TextFieldValue,
    onValueChanged: (TextFieldValue) -> Unit,
    onApplyValue: () -> Unit,
) = item("feedRate") {
    Input(
        currentValue = uiState.feedRate,
        editState = editState,
        label = stringResource(id = R.string.tune_print___feed_rate_label),
        labelActive = stringResource(id = R.string.tune_print___feed_rate_label_active),
        onApplyValue = onApplyValue,
        onValueChanged = onValueChanged,
        value = value,
    )
}

private fun LazyListScope.flowRate(
    uiState: TuneControlsViewModel.UiState,
    editState: TuneControlsViewModel.EditState,
    value: TextFieldValue,
    onValueChanged: (TextFieldValue) -> Unit,
    onApplyValue: () -> Unit,
) = item("flowRate") {
    Input(
        currentValue = uiState.flowRate,
        editState = editState,
        label = stringResource(id = R.string.tune_print___flow_rate_label),
        labelActive = stringResource(id = R.string.tune_print___flow_rate_label_active),
        onApplyValue = onApplyValue,
        onValueChanged = onValueChanged,
        value = value,
    )
}

private fun LazyListScope.fanSpeed(
    uiState: TuneControlsViewModel.UiState,
    editState: TuneControlsViewModel.EditState,
    value: TextFieldValue,
    onValueChanged: (TextFieldValue) -> Unit,
    onApplyValue: () -> Unit,
) = item("fanSpeed") {
    Input(
        currentValue = uiState.fanSpeed,
        editState = editState,
        label = stringResource(id = R.string.tune_print___fan_speed_label),
        labelActive = stringResource(id = R.string.tune_print___fan_speed_label_active),
        onApplyValue = onApplyValue,
        onValueChanged = onValueChanged,
        value = value,
    )
}

private fun LazyListScope.babyStep(
    uiState: TuneControlsViewModel.UiState,
    onStepDown: () -> Unit,
    onStepUp: () -> Unit,
) = item("babyStep") {
    Column {
        Box {
            val feedback = LocalHapticFeedback.current

            InputField(
                value = TextFieldValue(
                    uiState.zOffsetMm?.let { stringResource(id = R.string.x_mm, NumberFormat.getInstance().format(it)) } ?: ""
                ),
                onValueChanged = { },
                placeholder = "",
                readOnly = true,
                label = stringResource(id = R.string.tune_print___z_baby_step),
                labelActive = stringResource(id = R.string.tune_print___z_baby_step),
            )

            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .align(Alignment.CenterEnd)
                    .padding(end = OctoAppTheme.dimens.margin1)
            ) {
                IconAction(
                    painter = painterResource(id = R.drawable.ic_round_keyboard_arrow_up_24),
                    contentDescription = stringResource(id = R.string.cd_move_z_up),
                    onClick = {
                        feedback.performHapticFeedback(HapticFeedbackType.LongPress)
                        onStepUp()
                    }
                )
                Box(
                    modifier = Modifier
                        .width(1.dp)
                        .height(20.dp)
                        .background(OctoAppTheme.colors.blackTranslucent2)
                )
                IconAction(
                    painter = painterResource(id = R.drawable.ic_round_keyboard_arrow_down_24),
                    contentDescription = stringResource(id = R.string.cd_move_z_down),
                    onClick = {
                        feedback.performHapticFeedback(HapticFeedbackType.LongPress)
                        onStepDown()
                    }
                )
            }
        }

        Text(
            text = stringResource(id = R.string.tune_print___baby_step_explainer),
            style = OctoAppTheme.typography.label,
            color = OctoAppTheme.colors.lightText,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(top = OctoAppTheme.dimens.margin01)
                .padding(horizontal = OctoAppTheme.dimens.margin2)
        )
    }
}

@Composable
private fun Input(
    currentValue: Int?,
    label: String,
    labelActive: String,
    editState: TuneControlsViewModel.EditState,
    onApplyValue: () -> Unit,
    value: TextFieldValue,
    onValueChanged: (TextFieldValue) -> Unit,
) {
    var isFocused by remember { mutableStateOf(false) }
    val focusManager = LocalFocusManager.current
    var synced by remember { mutableStateOf<Boolean?>(null) }
    var changeByTyping by remember { mutableStateOf(false) }

    LaunchedEffect(editState.operationCompleted) {
        changeByTyping = false
    }

    LaunchedEffect(currentValue, isFocused, editState.loading) {
        if (!isFocused && !editState.loading && !changeByTyping) {
            onValueChanged(value.copy(text = currentValue?.toString() ?: ""))
        }
    }

    LaunchedEffect(currentValue, value, editState.loading, isFocused) {
        synced = if (currentValue != null) {
            currentValue == value.text.toIntOrNull()
        } else {
            null
        }
    }

    Box(
        contentAlignment = Alignment.CenterEnd
    ) {
        InputField(
            value = value,
            onValueChanged = {
                onValueChanged(it)
                changeByTyping = true
                synced = false
            },
            placeholder = "",
            label = label,
            labelActive = labelActive,
            modifier = Modifier.onFocusChanged { isFocused = it.isFocused },
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done, keyboardType = KeyboardType.Decimal),
            keyboardActions = KeyboardActions(
                onDone = {
                    onApplyValue()
                    focusManager.clearFocus()
                }
            )
        )

        Crossfade(targetState = synced) {
            when (synced) {
                true -> painterResource(id = R.drawable.ic_round_sync_24)
                false -> painterResource(id = R.drawable.ic_round_sync_disabled_24)
                null -> null
            }?.let {
                Icon(
                    painter = it,
                    contentDescription = null,
                    tint = OctoAppTheme.colors.lightText,
                    modifier = Modifier.padding(end = OctoAppTheme.dimens.margin2)
                )
            }
        }
    }
}
//endregion

//region Preview
@Composable
@Preview(device = Devices.PIXEL_4, showSystemUi = true)
private fun Preview() = OctoAppThemeForPreview {
    val uiState = TuneControlsViewModel.UiState(
        flowRate = 100,
        feedRate = 100,
        fanSpeed = 100,
        zOffsetMm = 0.024f,
    )

    val editState = TuneControlsViewModel.EditState(
        loading = false,
        operationCompleted = false,
        initialValue = true
    )

    TuneControlsView(
        uiState = uiState,
        editState = editState,
        onStepUp = { },
        onStepDown = {},
        onApplyValues = { _, _, _ -> }
    )
}
//endregion