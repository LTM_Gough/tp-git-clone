package de.crysxd.baseui.compose.controls.quickprint

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.compose.controls.GenericLoadingControlsViewModel
import de.crysxd.baseui.utils.NavigationResultMediator
import de.crysxd.octoapp.base.data.models.FileListState
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.usecase.StartPrintJobUseCase
import de.crysxd.octoapp.base.usecase.StartPrintJobUseCase.Result
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import timber.log.Timber

@OptIn(ExperimentalCoroutinesApi::class)
class QuickPrintViewModel(
    private val instanceId: String,
    octoPrintProvider: OctoPrintProvider,
    private val startPrintJobUseCase: StartPrintJobUseCase,
    private val loadFilesUseCase: LoadFilesUseCase,
) : GenericLoadingControlsViewModel<List<QuickPrintViewModel.FileLink>>() {

    private val loadingFiles = MutableStateFlow<Set<String>>(emptySet())
    private val retryTrigger = MutableStateFlow(0)
    private val selectedFileFlow = octoPrintProvider.passiveCurrentMessageFlow("quick-print").map {
        it.job?.file?.path
    }

    private val reloadEventClasses = listOf(
        Message.Event.UpdatedFiles::class,
        Message.Event.PrintDone::class,
        Message.Event.PrintCancelled::class,
        Message.Event.PrintFailed::class,
    )

    private var skipCacheOnNext = false
    var stateCache: ViewState<List<FileLink>> = ViewState.Loading
        private set

    private val reloadEventsFlow = combine(
        retryTrigger,
        octoPrintProvider.passiveEventFlow(instanceId)
            .mapNotNull { it as? Event.MessageReceived }
            .filter { reloadEventClasses.contains(it.message::class) }
            .map { }
            .onStart { emit(Unit) }
    ) { _, _ -> }

    private val files = reloadEventsFlow.flatMapLatest {
        val params = LoadFilesUseCase.Params.All(fileOrigin = FileOrigin.Local, instanceId = instanceId, skipCache = skipCacheOnNext)
        skipCacheOnNext = false
        loadFilesUseCase.execute(params)
    }
    val state = combine(loadingFiles, files, selectedFileFlow) { loading, state, selectedPath ->
        when (state) {
            is FileListState.Error -> ViewState.Error(state.exception)
            is FileListState.Loading -> stateCache.takeIf { it is ViewState.Data } ?: ViewState.Loading
            is FileListState.Loaded -> {
                val files = (state.file as? FileObject.Folder)?.children
                    ?: throw IllegalStateException("${state.file.path} has no children, might be file instead of folder")

                fun FileObject.toFiles(): List<FileObject.File> = when (this) {
                    is FileObject.File -> listOf(this)
                    is FileObject.Folder -> children.flatMap { it.toFiles() }
                }

                val maxItems = 4
                val allFiles = files.flatMap { it.toFiles() }
                val selectedFile = listOfNotNull(allFiles.firstOrNull { it.path == selectedPath }?.let {
                    FileLink(fileObject = it, selected = true, loading = loading.contains(it.path))
                })
                val justUploaded = listOfNotNull(allFiles.filter { it.prints?.last == null }.maxByOrNull { it.date }?.let {
                    FileLink(fileObject = it, selected = it.path == selectedPath, loading = loading.contains(it.path))
                })
                val lastPrints = allFiles.filter { it.prints?.last?.date != null }
                    .sortedByDescending { it.prints?.last?.date ?: 0L }
                    .take(maxItems)
                    .map { FileLink(fileObject = it, selected = false, loading = loading.contains(it.path)) }
                val result = (justUploaded + selectedFile + lastPrints).distinctBy { it.fileObject.path }.take(maxItems)

                if (result.isEmpty()) {
                    ViewState.Hidden
                } else {
                    ViewState.Data(
                        data = result,
                        contentKey = result.joinToString { it.fileObject.path ?: "" }
                    )
                }
            }
        }
    }.onEach {
        stateCache = it
    }.onStart {
        emit(ViewState.Loading)
    }.catch {
        Timber.e(it)
        emit(ViewState.Error(it))
    }.flowOn(Dispatchers.Default).shareIn(viewModelScope, started = SharingStarted.WhileSubscribed())

    private val mutableEvents = MutableStateFlow<ViewEvent?>(null)
    val events = mutableEvents.asStateFlow()

    override fun retry() {
        skipCacheOnNext = true
        retryTrigger.update { it + 1 }
    }

    fun startPrint(
        fileObject: FileObject.File,
        materialSelectionConfirmed: Boolean = false,
        timelapseConfigConfirmed: Boolean = false
    ) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        fileObject.setLoading(true)
        try {
            val result = startPrintJobUseCase.execute(
                StartPrintJobUseCase.Params(
                    file = fileObject,
                    materialSelectionConfirmed = materialSelectionConfirmed,
                    timelapseConfigConfirmed = timelapseConfigConfirmed
                )
            )

            when (result) {
                Result.MaterialSelectionRequired -> requireMaterialSelectionAndStart(fileObject = fileObject, timelapseConfigConfirmed = timelapseConfigConfirmed)
                Result.TimelapseConfigRequired -> requireTimelapseConfigAndStart(fileObject = fileObject, materialSelectionConfirmed = materialSelectionConfirmed)
                Result.PrintStarted -> mutableEvents.value = ViewEvent.PrintStarted()
            }
        } finally {
            fileObject.setLoading(false)
        }
    }

    private suspend fun requireTimelapseConfigAndStart(fileObject: FileObject.File, materialSelectionConfirmed: Boolean) {
        val (resultId, liveData) = NavigationResultMediator.registerResultCallback<Boolean>()
        mutableEvents.value = ViewEvent.RequireTimelapseConfiguration(resultId = resultId)
        if (liveData.asFlow().first() == true) {
            startPrint(timelapseConfigConfirmed = true, materialSelectionConfirmed = materialSelectionConfirmed, fileObject = fileObject)
        } else {
            fileObject.setLoading(false)
        }
    }

    private suspend fun requireMaterialSelectionAndStart(fileObject: FileObject.File, timelapseConfigConfirmed: Boolean) {
        val (resultId, liveData) = NavigationResultMediator.registerResultCallback<Boolean>()
        mutableEvents.value = ViewEvent.RequireMaterialSelection(resultId = resultId)
        if (liveData.asFlow().first() == true) {
            startPrint(timelapseConfigConfirmed = timelapseConfigConfirmed, materialSelectionConfirmed = true, fileObject = fileObject)
        } else {
            fileObject.setLoading(false)
        }
    }

    private fun FileObject.File.setLoading(loading: Boolean) {
        val p = requireNotNull(path) { "File missing path!" }
        loadingFiles.update {
            if (loading) it + p else it - p
        }
    }

    sealed class ViewEvent {
        abstract var handled: Boolean

        data class RequireMaterialSelection(override var handled: Boolean = false, val resultId: Int) : ViewEvent()
        data class RequireTimelapseConfiguration(override var handled: Boolean = false, val resultId: Int) : ViewEvent()
        data class PrintStarted(override var handled: Boolean = false) : ViewEvent()
    }

    data class FileLink(
        val fileObject: FileObject.File,
        val selected: Boolean,
        val loading: Boolean = false,
    )

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "QuickPrintViewModel@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = QuickPrintViewModel(
            instanceId = instanceId,
            octoPrintProvider = BaseInjector.get().octoPrintProvider(),
            loadFilesUseCase = BaseInjector.get().loadFilesUseCase(),
            startPrintJobUseCase = BaseInjector.get().startPrintJobUseCase(),
        ) as T
    }
}