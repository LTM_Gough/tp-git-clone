package de.crysxd.baseui.compose.framework

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.asFlow
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.di.BaseUiInjector

@Composable
fun OctoPrintPicassoImage(
    modifier: Modifier = Modifier,
    @DrawableRes fallback: Int = R.drawable.ic_round_image_not_supported_24,
    contentScale: ContentScale = ContentScale.Inside,
    url: String?,
) {
    val picassoImage by rememberOctoPrintPicassoImage(url = url, fallback = fallback)

    //region Display
    Crossfade(
        targetState = picassoImage,
        modifier = modifier,
    ) { image ->
        Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxSize()) {
            when (image) {
                null -> CircularProgressIndicator(
                    modifier = Modifier.scale(0.6f)
                )

                else -> Image(
                    bitmap = image,
                    contentDescription = null,
                    contentScale = contentScale,
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(OctoAppTheme.dimens.margin0)
                        .clip(MaterialTheme.shapes.large)
                )
            }
        }
    }
    //endregion
}

@Composable
fun rememberOctoPrintPicassoImage(
    url: String?,
    @DrawableRes fallback: Int = R.drawable.ic_round_image_not_supported_24,
    placeholderTint: Color = OctoAppTheme.colors.accent,
): MutableState<ImageBitmap?> {
    val image = remember { mutableStateOf<ImageBitmap?>(null) }
    val context = LocalContext.current
    val isPreview = OctoAppTheme.isPreview
    val picasso = if (isPreview) null else BaseUiInjector.get().octoPrintPicasso().asFlow().collectAsState(initial = null).value
    fun Drawable.tintedImageBitmap() = apply { setTint(placeholderTint.toArgb()) }.toBitmap().asImageBitmap()

    //region Picasso
    DisposableEffect(url, picasso) {
        val target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                image.value = bitmap?.asImageBitmap()
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                image.value = errorDrawable?.tintedImageBitmap()
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                image.value = placeHolderDrawable?.tintedImageBitmap()
            }
        }

        if (url == null || isPreview || picasso == null) {
            image.value = ContextCompat.getDrawable(context, fallback)?.tintedImageBitmap()
        } else {
            picasso.load(url)?.error(fallback)?.into(target)
        }

        onDispose {
            picasso?.cancelRequest(target)
        }
    }

    return image
}
