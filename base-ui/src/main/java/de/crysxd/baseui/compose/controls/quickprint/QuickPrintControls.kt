package de.crysxd.baseui.compose.controls.quickprint

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import de.crysxd.baseui.R
import de.crysxd.baseui.common.BaseFileDetailsFragmentArgs
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.controls.GenericLoadingControls
import de.crysxd.baseui.compose.controls.GenericLoadingControlsState
import de.crysxd.baseui.compose.controls.GenericLoadingControlsViewModel.ViewState
import de.crysxd.baseui.compose.framework.LocalOctoPrint
import de.crysxd.baseui.compose.framework.OctoPrintPicassoImage
import de.crysxd.baseui.compose.framework.collectAsStateWhileActive
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.menu.base.MenuBottomSheetFragment
import de.crysxd.baseui.menu.material.MaterialPluginMenu
import de.crysxd.baseui.timelapse.TimelapseMenu
import de.crysxd.octoapp.base.ext.format
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import java.io.IOException
import java.util.Date

@Composable
fun QuickPrintControls(
    editState: EditState,
    state: QuickPrintControlsState,
) = GenericLoadingControls(
    editState = editState,
    state = state,
    title = stringResource(id = R.string.widget_quick_print),
) { files ->
    Column(verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01)) {
        files.forEach {
            Entry(
                fileLink = it,
                onSelected = { state.onFileSelected(it.fileObject) },
                onStartPrint = { state.onStartPrint(it.fileObject) },
            )
        }
    }
}

//region Internals
@Composable
private fun Entry(fileLink: QuickPrintViewModel.FileLink, onSelected: () -> Unit, onStartPrint: () -> Unit) = Row(
    modifier = Modifier
        .height(IntrinsicSize.Min)
        .fillMaxWidth()
        .clip(MaterialTheme.shapes.small)
        .background(OctoAppTheme.colors.inputBackground.copy(alpha = 0.66f))
        .clickable(enabled = !fileLink.loading) { onStartPrint() }
) {
    //region Main button
    Row(
        modifier = Modifier
            .weight(1f)
            .clip(MaterialTheme.shapes.large)
            .background(OctoAppTheme.colors.inputBackground)
            .clickable { onSelected() }
            .padding(horizontal = OctoAppTheme.dimens.margin01, vertical = OctoAppTheme.dimens.margin01),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01)
    ) {
        OctoPrintPicassoImage(
            url = fileLink.fileObject.thumbnail,
            fallback = R.drawable.ic_round_insert_drive_file_24,
            modifier = Modifier
                .size(32.dp)
                .clip(MaterialTheme.shapes.medium)
        )
        Column(modifier = Modifier.weight(1f)) {
            val lastPrint = fileLink.fileObject.prints?.last
            val lastPrintDate = lastPrint?.date?.let { Date(it).format(context = LocalContext.current) }

            Text(
                text = fileLink.fileObject.display ?: fileLink.fileObject.name ?: "???",
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = OctoAppTheme.typography.base,
                color = OctoAppTheme.colors.darkText
            )
            Text(
                text = when {
                    fileLink.selected -> stringResource(id = R.string.file_manager___file_list___currently_selected)
                    lastPrintDate != null -> when (lastPrint.success) {
                        true -> stringResource(id = R.string.file_manager___file_details___last_print_at_x_success, lastPrintDate)
                        false -> stringResource(id = R.string.file_manager___file_details___last_print_at_x_failure, lastPrintDate)
                    }
                    else -> stringResource(id = R.string.file_manager___file_list___just_uploaded)
                },
                overflow = TextOverflow.Ellipsis,
                style = OctoAppTheme.typography.labelSmall,
                color = OctoAppTheme.colors.normalText
            )
        }
        Icon(
            painter = painterResource(id = R.drawable.ic_round_chevron_right_24),
            contentDescription = null,
            tint = OctoAppTheme.colors.accent,
        )
    }
    //endregion
    //region Print button
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .fillMaxHeight()
            .aspectRatio(1f)
            .clip(MaterialTheme.shapes.small)
    ) {
        Crossfade(targetState = fileLink.loading) {
            Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxSize()) {
                if (!it) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_round_print_24),
                        contentDescription = stringResource(id = R.string.cd_print),
                        tint = OctoAppTheme.colors.accent,

                        )
                } else {
                    CircularProgressIndicator(
                        modifier = Modifier.scale(0.75f),
                    )
                }
            }
        }
    }
    //endregion
}
//endregion
//region State

@Composable
fun rememberQuickPrintState(active: Boolean, navController: NavController, childFragmentManager: FragmentManager): QuickPrintControlsState {
    val vmFactory = QuickPrintViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = QuickPrintViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory,
        viewModelStoreOwner = OctoAppTheme.octoActivity as ViewModelStoreOwner
    )
    val wasError = remember { mutableStateOf(false) }

    val event by vm.events.collectAsState()
    LaunchedEffect(event) {
        event?.handled = true
        when (val e = event) {
            null,
            is QuickPrintViewModel.ViewEvent.PrintStarted -> Unit

            is QuickPrintViewModel.ViewEvent.RequireMaterialSelection ->
                MenuBottomSheetFragment.createForMenu(MaterialPluginMenu(startPrintResultId = e.resultId)).show(childFragmentManager)

            is QuickPrintViewModel.ViewEvent.RequireTimelapseConfiguration ->
                MenuBottomSheetFragment.createForMenu(TimelapseMenu(startPrintResultId = e.resultId)).show(childFragmentManager)
        }
    }

    val state = vm.state.collectAsStateWhileActive(active = active, key = "quickprint", initial = vm.stateCache)
    wasError.value = state.value is ViewState.Error || wasError.value
    return object : QuickPrintControlsState {
        override val wasError get() = wasError.value
        override val current get() = state.value

        override fun onRetry() = vm.retry()

        override fun onStartPrint(fileObject: FileObject.File) {
            vm.startPrint(fileObject)
        }

        override fun onFileSelected(fileObject: FileObject.File) = navController.navigate(
            R.id.action_show_file_details,
            BaseFileDetailsFragmentArgs(fileObject).toBundle()
        )
    }
}

interface QuickPrintControlsState : GenericLoadingControlsState<List<QuickPrintViewModel.FileLink>> {
    fun onFileSelected(fileObject: FileObject.File)
    fun onStartPrint(fileObject: FileObject.File)
}

//endregion
//region Previews
@Preview
@Composable
private fun PreviewLoadingInitial() = OctoAppThemeForPreview {
    val state = object : QuickPrintControlsState {
        override val wasError = false
        override val current = ViewState.Loading
        override fun onFileSelected(fileObject: FileObject.File) = Unit
        override fun onStartPrint(fileObject: FileObject.File) = Unit
        override fun onRetry() = Unit
    }

    QuickPrintControls(editState = EditState.ForPreview, state = state)
}

@Preview
@Composable
private fun PreviewLoadingAfterError() = OctoAppThemeForPreview {
    val state = object : QuickPrintControlsState {
        override val wasError = true
        override val current = ViewState.Loading
        override fun onFileSelected(fileObject: FileObject.File) = Unit
        override fun onStartPrint(fileObject: FileObject.File) = Unit
        override fun onRetry() = Unit
    }

    QuickPrintControls(editState = EditState.ForPreview, state = state)
}


@Preview
@Composable
private fun PreviewError() = OctoAppThemeForPreview {
    val state = object : QuickPrintControlsState {
        override val wasError = false
        override val current = ViewState.Error(IOException())
        override fun onFileSelected(fileObject: FileObject.File) = Unit
        override fun onStartPrint(fileObject: FileObject.File) = Unit
        override fun onRetry() = Unit
    }

    QuickPrintControls(editState = EditState.ForPreview, state = state)
}

@Preview
@Composable
private fun PreviewEmpty() = OctoAppThemeForPreview {
    val state = object : QuickPrintControlsState {
        override val wasError = false
        override fun onFileSelected(fileObject: FileObject.File) = Unit
        override fun onStartPrint(fileObject: FileObject.File) = Unit
        override fun onRetry() = Unit
        override val current = ViewState.Data(
            contentKey = "",
            data = emptyList<QuickPrintViewModel.FileLink>()
        )
    }

    QuickPrintControls(editState = EditState.ForPreview, state = state)
}

@Preview
@Composable
private fun PreviewData() = OctoAppThemeForPreview {
    val state = object : QuickPrintControlsState {
        override val wasError = false
        override fun onFileSelected(fileObject: FileObject.File) = Unit
        override fun onStartPrint(fileObject: FileObject.File) = Unit
        override fun onRetry() = Unit
        override val current = ViewState.Data(
            contentKey = "",
            data = listOf(
                QuickPrintViewModel.FileLink(
                    selected = true,
                    fileObject = FileObject.File(
                        display = "File A",
                        name = "FileA",
                        origin = FileOrigin.Local,
                        path = "/",
                        type = "machinecode",
                        typePath = listOf("machinecode"),
                        ref = null,
                        size = 0,
                        thumbnail = null,
                        date = 0,
                        hash = "",
                        gcodeAnalysis = null,
                        prints = FileObject.PrintHistory(failure = 1, success = 0, last = FileObject.PrintHistory.LastPrint(0, success = true))
                    )
                ),
                QuickPrintViewModel.FileLink(
                    selected = false,
                    fileObject = FileObject.File(
                        display = "File B",
                        name = "FileB",
                        origin = FileOrigin.Local,
                        path = "/",
                        type = "machinecode",
                        typePath = listOf("machinecode"),
                        ref = null,
                        size = 0,
                        thumbnail = null,
                        date = 0,
                        hash = "",
                        gcodeAnalysis = null,
                        prints = FileObject.PrintHistory(failure = 1, success = 0, last = FileObject.PrintHistory.LastPrint(0, success = true))
                    )
                ),
                QuickPrintViewModel.FileLink(
                    selected = false,
                    fileObject = FileObject.File(
                        display = "File B",
                        name = "FileB",
                        origin = FileOrigin.Local,
                        path = "/",
                        type = "machinecode",
                        typePath = listOf("machinecode"),
                        ref = null,
                        size = 0,
                        thumbnail = null,
                        date = 0,
                        hash = "",
                        gcodeAnalysis = null,
                        prints = FileObject.PrintHistory(failure = 1, success = 0, last = FileObject.PrintHistory.LastPrint(0, success = false))
                    )
                ),
                QuickPrintViewModel.FileLink(
                    selected = false,
                    fileObject = FileObject.File(
                        display = "File B",
                        name = "FileB",
                        origin = FileOrigin.Local,
                        path = "/",
                        type = "machinecode",
                        typePath = listOf("machinecode"),
                        ref = null,
                        size = 0,
                        thumbnail = null,
                        date = 0,
                        hash = "",
                        gcodeAnalysis = null,
                        prints = FileObject.PrintHistory(failure = 1, success = 0, last = FileObject.PrintHistory.LastPrint(0, success = true))
                    )
                )
            ),
        )
    }

    QuickPrintControls(editState = EditState.ForPreview, state = state)
}
//endregion
