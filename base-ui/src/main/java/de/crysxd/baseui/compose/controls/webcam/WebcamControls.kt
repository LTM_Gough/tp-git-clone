package de.crysxd.baseui.compose.controls.webcam

import android.graphics.Bitmap
import android.net.Uri
import android.widget.ImageView
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.LocalContentColor
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawOutline
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.ViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.ControlsScaffold
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.controls.printconfidence.PrintConfidenceControlsState
import de.crysxd.baseui.compose.controls.webcam.WebcamControlsViewModel.UiState
import de.crysxd.baseui.compose.framework.AnnouncementController
import de.crysxd.baseui.compose.framework.AnnouncementId
import de.crysxd.baseui.compose.framework.LinkButtonRow
import de.crysxd.baseui.compose.framework.LocalOctoPrint
import de.crysxd.baseui.compose.framework.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.staticStateOf
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.menu.base.MenuBottomSheetFragment
import de.crysxd.baseui.menu.webcam.WebcamSettingsMenu
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.ext.asStyleFileSize
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedexternalapis.printconfidence.PrintConfidence
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive

@Composable
fun WebcamActions(
    state: WebcamControlsState,
    confidenceState: PrintConfidenceControlsState,
    editState: EditState,
    modifier: Modifier = Modifier,
) = WebcamControlsBox(
    editState = editState,
    modifier = modifier
) {
    WebcamControlsInternal(state = state, confidenceState = confidenceState)
}

//region State
interface WebcamControlsState {
    val current: UiState
    var grabImage: (suspend () -> Bitmap)?
    val isFullscreen: Boolean
    fun onRetry()
    fun onSwitchWebcam()
    fun onShareImage(img: suspend () -> Bitmap)
    fun onFullscreenClicked()
    fun onTroubleshoot()

    fun getInitialAspectRatio(): Float
    fun updateAspectRatio(ratio: String)

    fun getInitialScaleType(): ImageView.ScaleType
    fun updateScaleToFillType(type: ImageView.ScaleType)
}

@Composable
fun rememberWebcamControlsState(active: Boolean, isFullscreen: Boolean, navController: NavController): WebcamControlsState {
    val activity = OctoAppTheme.octoActivity
    val octoPrintId = LocalOctoPrint.current.id
    val vmFactory = WebcamControlsViewModel.Factory(octoPrintId)
    val vm = viewModel(
        modelClass = WebcamControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory,
        viewModelStoreOwner = activity as? ViewModelStoreOwner ?: LocalViewModelStoreOwner.current ?: throw java.lang.IllegalStateException("No VM holder")
    )

    val grabImage = remember { mutableStateOf<(suspend () -> Bitmap)?>(null) }
    val state = vm.state.collectAsStateWhileActive(active = active, key = "webcam", initial = UiState.Loading())
    return object : WebcamControlsState {
        override val current by state
        override val isFullscreen = isFullscreen
        override var grabImage: (suspend () -> Bitmap)? by grabImage

        override fun onFullscreenClicked() {
            if (isFullscreen) {
                navController.popBackStack()
            } else {
                UriLibrary.getWebcamUri().open()
            }
        }

        override fun onTroubleshoot() {
            UriLibrary.getWebcamTroubleshootingUri().open()
        }

        override fun onRetry() {
            vm.retry()
        }

        override fun onSwitchWebcam() {
            vm.nextWebcam()
        }

        override fun onShareImage(img: suspend () -> Bitmap) {
            activity?.let { vm.shareImage(it, img) }
        }

        override fun getInitialAspectRatio() =
            vm.getInitialAspectRatio()

        override fun updateAspectRatio(ratio: String) {
            vm.storeAspectRatio(ratio)
        }

        override fun getInitialScaleType() =
            vm.getScaleType(isFullscreen, ImageView.ScaleType.FIT_CENTER)

        override fun updateScaleToFillType(type: ImageView.ScaleType) {
            vm.storeScaleType(type, isFullscreen)
        }
    }
}

//endregion
//region Internals
const val LiveDelayThresholdMs = 3_000L

@Composable
fun WebcamControlsInternal(
    state: WebcamControlsState,
    confidenceState: PrintConfidenceControlsState,
) {
    val boxState = remember { AspectRatioBoxScope(state.getInitialAspectRatio()) }
    val warning by remember { derivedStateOf { state.current.warning } }

    WebcamWarning(warningSate = warning) {
        PrintConfidence(confidenceState = confidenceState) {
            AspectRatioBox(
                state = boxState,
                updateInitialAspectRatio = state::updateAspectRatio
            ) {
                //region State
                WebcamView(
                    state = state,
                    boxState = boxState,
                    allowTouch = false,
                )
                //endregion
                //region Actions
                Row(
                    modifier = Modifier
                        .align(Alignment.BottomEnd)
                        .fillMaxWidth()
                        .background(WebcamOverlayBrush)
                ) {
                    WebcamActions(webcamState = state) {
                        Spacer(Modifier.weight(1f))
                    }
                }
                //endregion
            }
        }
    }
}

@Composable
fun WebcamWarning(
    warningSate: State<WebcamControlsViewModel.Warning?>,
    content: @Composable () -> Unit,
) = Column(
    modifier = Modifier
        .clip(MaterialTheme.shapes.large)
        .background(OctoAppTheme.colors.accent)
) {
    content()

    val warningId by remember(warningSate) { derivedStateOf { warningSate.value?.id } }
    AnnouncementController(announcementId = warningId?.let { AnnouncementId.Default(it) }) {
        CompositionLocalProvider(LocalContentColor provides OctoAppTheme.colors.textColoredBackground) {
            val warning = warningSate.value

            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Text(
                    text = warning?.text ?: "",
                    style = OctoAppTheme.typography.base,
                    modifier = Modifier.padding(OctoAppTheme.dimens.margin1),
                    color = LocalContentColor.current,
                )

                val canDismiss = warning?.dismissible == true
                val hasAction = warning?.actionUri != null && warning.actionText != null

                LinkButtonRow(
                    modifier = Modifier.fillMaxWidth(),
                    alignment = Alignment.CenterHorizontally,
                    texts = listOfNotNull(
                        stringResource(id = R.string.hide).takeIf { canDismiss },
                        warning?.actionText.takeIf { hasAction },
                    ),
                    onClicks = listOfNotNull(
                        { hideAnnouncement() }.takeIf { canDismiss },
                        { warning?.actionUri?.open() ?: Unit }.takeIf { hasAction },
                    ),
                    color = LocalContentColor.current
                )
            }
        }
    }
}

@Composable
@OptIn(ExperimentalAnimationApi::class)
fun PrintConfidence(
    confidenceState: PrintConfidenceControlsState,
    content: @Composable () -> Unit,
) {
    val background by animateColorAsState(confidenceState.current?.let { confidenceState.backgroundColorFor(it) } ?: Color.Transparent)
    val shape = MaterialTheme.shapes.large
    val density = LocalDensity.current
    val layoutDirection = LocalLayoutDirection.current

    Column(
        modifier = Modifier
            .clickable(
                enabled = confidenceState.contentClickUrl != null,
                onClick = { confidenceState.contentClickUrl?.open() }
            )
            .drawBehind {
                drawRect(background)
                val out = shape.createOutline(size = size, layoutDirection = layoutDirection, density = density)
                drawOutline(outline = out, style = Fill, color = background)
                drawOutline(outline = out, style = Stroke(width = with(density) { 2.dp.toPx() }), color = Color.White.copy(alpha = 0.05f))
            }
    ) {
        content()

        AnimatedContent(
            targetState = confidenceState.current,
        ) {
            if (it != null) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01),
                    modifier = Modifier
                        .padding(OctoAppTheme.dimens.margin1)
                        .fillMaxWidth(),
                ) {
                    Icon(
                        painter = confidenceState.iconFor(confidence = it),
                        contentDescription = null,
                        tint = Color.Unspecified
                    )
                    ChatBubble(
                        text = it.description,
                        modifier = Modifier.weight(1f)
                    )
                    Text(
                        text = confidenceState.disclaimerFor(confidence = it),
                        style = OctoAppTheme.typography.labelSmall.copy(fontSize = 8.sp),
                        color = OctoAppTheme.colors.whiteTranslucent,
                        maxLines = 2,
                        overflow = TextOverflow.Ellipsis,
                        textAlign = TextAlign.End,
                    )
                }
            }
        }
    }
}

@Composable
private fun ChatBubble(
    text: String,
    modifier: Modifier = Modifier,
) = ConstraintLayout(modifier) {
    val (bubbleStart, bubbleCenter, bubbleEnd, bubbleText) = createRefs()

    createHorizontalChain(bubbleStart, bubbleText, bubbleEnd, chainStyle = ChainStyle.Packed(0f))

    Image(
        painter = painterResource(id = R.drawable.chat_bubble_start),
        contentDescription = null,
        contentScale = ContentScale.FillBounds,
        modifier = Modifier.constrainAs(bubbleStart) {
            centerVerticallyTo(parent)
            start.linkTo(parent.start)
        }
    )
    Image(
        painter = painterResource(id = R.drawable.chat_bubble_center),
        contentDescription = null,
        contentScale = ContentScale.FillBounds,
        modifier = Modifier.constrainAs(bubbleCenter) {
            centerVerticallyTo(parent)
            start.linkTo(bubbleStart.end)
            end.linkTo(bubbleEnd.start)
            width = Dimension.fillToConstraints
        }

    )
    Text(
        text = text,
        style = OctoAppTheme.typography.label,
        color = OctoAppTheme.colors.black,
        overflow = TextOverflow.Ellipsis,
        maxLines = 1,
        modifier = Modifier.constrainAs(bubbleText) {
            centerVerticallyTo(parent)
            width = Dimension.preferredWrapContent
        }
    )

    Image(
        painter = painterResource(id = R.drawable.chat_bubble_end),
        contentDescription = null,
        contentScale = ContentScale.FillBounds,
        modifier = Modifier.constrainAs(bubbleEnd) {
            centerVerticallyTo(parent)
            linkTo(start = bubbleText.end, end = parent.end, bias = 0f)
        }
    )
}

@Composable
private fun AspectRatioBox(
    state: AspectRatioBoxScope,
    updateInitialAspectRatio: (String) -> Unit,
    content: @Composable BoxScope.() -> Unit,
) {
    val animatedAspectRatio by animateFloatAsState(targetValue = state.aspectRatio)

    LaunchedEffect(state.aspectRatio) {
        updateInitialAspectRatio("${state.aspectRatio}:1")
    }

    Box(
        modifier = Modifier
            .aspectRatio(if (animatedAspectRatio.isNaN()) 1f else animatedAspectRatio)
            .fillMaxWidth()
            .clip(MaterialTheme.shapes.large)
            .background(Color.Black),
        contentAlignment = Alignment.Center,
        content = content,
    )
}

class AspectRatioBoxScope(
    initialAspectRatio: Float,
) {
    var aspectRatio: Float by mutableStateOf(initialAspectRatio)
}

class StateHolder {
    var state: UiState? = null
}

@Composable
private fun WebcamControlsBox(
    modifier: Modifier,
    editState: EditState? = null,
    content: @Composable BoxScope.() -> Unit
) = ControlsScaffold(title = stringResource(id = R.string.webcam),
    actionIcon = painterResource(id = R.drawable.ic_round_settings_24),
    actionContentDescription = stringResource(id = R.string.cd_settings),
    editState = editState,
    onAction = { fm, _ -> MenuBottomSheetFragment.createForMenu(WebcamSettingsMenu()).show(fm) }) {
    Box(
        contentAlignment = Alignment.Center,
        content = content,
        modifier = modifier,
    )
}

//endregion
//region Previews
private fun createMockWebcamControlsState(state: UiState, scaleType: ImageView.ScaleType = ImageView.ScaleType.FIT_CENTER) = object : WebcamControlsState {
    override val current = state
    override val isFullscreen = false
    override var grabImage: (suspend () -> Bitmap)? = null
    override fun onRetry() = Unit
    override fun onSwitchWebcam() = Unit
    override fun onShareImage(img: suspend () -> Bitmap) = Unit
    override fun onFullscreenClicked() = Unit
    override fun onTroubleshoot() = Unit
    override fun getInitialAspectRatio(): Float = 1f
    override fun updateAspectRatio(ratio: String) = Unit
    override fun getInitialScaleType() = scaleType
    override fun updateScaleToFillType(type: ImageView.ScaleType) = Unit
}

private fun createMockConfidenceState(confidence: PrintConfidence? = null) = object : PrintConfidenceControlsState {
    override val current: PrintConfidence? = confidence
    override var contentClickUrl = null

    @Composable
    override fun backgroundColorFor(confidence: PrintConfidence) = OctoAppTheme.colors.octoeverywhere

    @Composable
    override fun iconFor(confidence: PrintConfidence): Painter = painterResource(id = R.drawable.ic_gadget_green)

    @Composable
    override fun disclaimerFor(confidence: PrintConfidence) = stringResource(id = R.string.print_confidence_disclaimer_octoeverywhere)
}

@Preview
@Composable
private fun PreviewLoading() = OctoAppThemeForPreview {
    val text = stringResource(
        id = R.string.webcam_widget___data_warning,
        153_438_253L.asStyleFileSize(intOnly = true)
    )

    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            UiState.Loading(
                webcamCount = 2,
                warning = staticStateOf(
                    WebcamControlsViewModel.Warning(
                        id = "test",
                        text = text,
                        dismissible = true,
                        actionText = "Learn more",
                        actionUri = "http://google.com".toUrl()
                    )
                )
            )
        ),
        confidenceState = createMockConfidenceState(),
    )
}

@Preview
@Composable
private fun PreviewErrorNoRetry() = OctoAppThemeForPreview {
    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            UiState.Error(
                webcamCount = 2,
                isManualReconnect = false,
                streamUrl = "http://streamurl:5050/?action=stream",
                exception = Throwable()
            )
        ),
        confidenceState = createMockConfidenceState(),
    )
}

@Preview
@Composable
private fun PreviewErrorRetry() = OctoAppThemeForPreview {
    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            UiState.Error(
                webcamCount = 2,
                isManualReconnect = true,
                streamUrl = "http://streamurl:5050/?action=stream",
                exception = Throwable()
            )
        ),
        confidenceState = createMockConfidenceState(),
    )
}

@Preview
@Composable
private fun PreviewNotAvailable() = OctoAppThemeForPreview {
    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            UiState.WebcamNotAvailable(
                text = R.string.please_configure_your_webcam_in_octoprint,
                webcamCount = 0
            )
        ),
        confidenceState = createMockConfidenceState(),
    )
}

@Preview
@Composable
private fun PreviewRichStreamDisabled() = OctoAppThemeForPreview {
    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            UiState.RichStreamDisabled(webcamCount = 3)
        ),
        confidenceState = createMockConfidenceState(),
    )
}

@Preview
@Composable
private fun PreviewMjpeg() = OctoAppThemeForPreview {
    var enforcedAspectRatio by remember { mutableStateOf<String?>("16:9") }
    val scaleToFill by remember { mutableStateOf(ImageView.ScaleType.CENTER_INSIDE) }
    val context = LocalContext.current

    LaunchedEffect(Unit) {
        while (isActive) {
            delay(2000)
            enforcedAspectRatio = null
            delay(2000)
            enforcedAspectRatio = "16:9"
        }
    }

    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            scaleType = scaleToFill,
            state = UiState.FrameReady(
                webcamCount = 2,
                flipH = false,
                flipV = false,
                enforcedAspectRatio = enforcedAspectRatio,
                fps = staticStateOf(13.37f),
                nextFrameDelayMs = null,
                rotate90 = false,
                showResolution = true,
                frame = staticStateOf(ContextCompat.getDrawable(context, R.drawable.webcam_demo)!!.toBitmap()),
                frameTime = System.currentTimeMillis(),
            ),
        ),
        confidenceState = createMockConfidenceState(
            PrintConfidence(
                description = "Print is going well but this text is way too long!",
                origin = PrintConfidence.Origin.OctoEverywhere,
                confidence = 1f,
                level = PrintConfidence.Level.High
            )
        ),
    )
}

@Preview
@Composable
private fun PreviewMjpeg2() = OctoAppThemeForPreview {
    var enforcedAspectRatio by remember { mutableStateOf<String?>("16:9") }
    val scaleToFill by remember { mutableStateOf(ImageView.ScaleType.CENTER_INSIDE) }
    val context = LocalContext.current

    LaunchedEffect(Unit) {
        while (isActive) {
            delay(2000)
            enforcedAspectRatio = null
            delay(2000)
            enforcedAspectRatio = "16:9"
        }
    }

    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            scaleType = scaleToFill,
            state = UiState.FrameReady(
                webcamCount = 2,
                flipH = false,
                flipV = false,
                enforcedAspectRatio = enforcedAspectRatio,
                fps = staticStateOf(13.37f),
                nextFrameDelayMs = null,
                rotate90 = false,
                showResolution = true,
                frame = staticStateOf(ContextCompat.getDrawable(context, R.drawable.webcam_demo)!!.toBitmap()),
                frameTime = System.currentTimeMillis(),
            ),
        ),
        confidenceState = createMockConfidenceState(
            PrintConfidence(
                description = "Print is going well!",
                origin = PrintConfidence.Origin.OctoEverywhere,
                confidence = 1f,
                level = PrintConfidence.Level.High
            )
        ),
    )
}

@Preview
@Composable
private fun PreviewRich() = OctoAppThemeForPreview {
    var enforcedAspectRatio by remember { mutableStateOf<String?>("4:3") }
    val context = LocalContext.current
    val url = "https://moctobpltc-i.akamaihd.net/hls/live/571329/eight/playlist.m3u8"
    val player = remember { ExoPlayer.Builder(context).build() }

    DisposableEffect(Unit) {
        val mediaItem = MediaItem.fromUri(url)
        val dataSourceFactory = DefaultHttpDataSource.Factory()
        val mediaSourceFactory = DefaultMediaSourceFactory(dataSourceFactory)
        player.setMediaSource(mediaSourceFactory.createMediaSource(mediaItem))
        player.prepare()
        player.play()
        onDispose { player.stop() }
    }

    LaunchedEffect(Unit) {
        while (isActive) {
            delay(2000)
            enforcedAspectRatio = null
            delay(2000)
            enforcedAspectRatio = "4:3"
        }
    }

    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            state = UiState.RichStreamReady(
                webcamCount = 2,
                flipH = false,
                flipV = false,
                enforcedAspectRatio = enforcedAspectRatio,
                rotate90 = false,
                authHeader = null,
                showResolution = true,
                player = player,
                uri = Uri.parse(url),
            )
        ),
        confidenceState = createMockConfidenceState(),
    )
}
//endregion