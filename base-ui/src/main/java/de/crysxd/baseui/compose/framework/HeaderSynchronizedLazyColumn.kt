package de.crysxd.baseui.compose.framework

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandHorizontally
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkHorizontally
import androidx.compose.foundation.background
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyGridScope
import androidx.compose.foundation.lazy.grid.LazyGridState
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.core.view.isVisible
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.common.OctoToolbar
import de.crysxd.baseui.compose.theme.OctoAppTheme

private const val FooterKey = "footer"

@Composable
fun HeaderSynchronizedLazyColumn(
    octoActivity: OctoActivity?,
    toolbarState: OctoToolbar.State,
    modifier: Modifier = Modifier,
    verticalArrangement: Arrangement.Vertical = Arrangement.Top,
    contentPadding: PaddingValues = PaddingValues(
        top = OctoAppTheme.dimens.margin6,
        start = OctoAppTheme.dimens.margin2,
        end = OctoAppTheme.dimens.margin2,
        bottom = OctoAppTheme.dimens.margin2,
    ),
    state: LazyListState = rememberLazyListState(),
    content: LazyListScope.() -> Unit
) = HeaderSynchronizedScrollBox(
    firstVisibleItemIndex = { state.firstVisibleItemIndex },
    firstVisibleItemScrollOffset = { state.firstVisibleItemScrollOffset },
    footerVisible = { state.layoutInfo.visibleItemsInfo.lastOrNull()?.key == FooterKey },
    contentPadding = contentPadding,
    octoActivity = octoActivity,
    toolbarState = toolbarState,
) {
    LazyColumn(
        state = state,
        contentPadding = contentPadding,
        verticalArrangement = verticalArrangement,
        content = {
            content()

            item(key = FooterKey) {
                Spacer(modifier = Modifier.height(1.dp))
            }
        },
        modifier = modifier.testTag("controls:scroller"),
    )
}

@Composable
fun HeaderSynchronizedLazyVerticalGrid(
    octoActivity: OctoActivity?,
    toolbarState: OctoToolbar.State,
    modifier: Modifier = Modifier,
    columnCount: Int,
    verticalArrangement: Arrangement.Vertical = Arrangement.Top,
    contentPadding: PaddingValues = PaddingValues(
        top = OctoAppTheme.dimens.margin6,
        start = OctoAppTheme.dimens.margin2,
        end = OctoAppTheme.dimens.margin2,
        bottom = OctoAppTheme.dimens.margin2,
    ),
    horizontalPadding: Dp = OctoAppTheme.dimens.margin2,
    state: LazyGridState = rememberLazyGridState(),
    content: LazyGridScope.() -> Unit
) = HeaderSynchronizedScrollBox(
    firstVisibleItemIndex = { state.firstVisibleItemIndex },
    firstVisibleItemScrollOffset = { state.firstVisibleItemScrollOffset },
    footerVisible = { state.layoutInfo.visibleItemsInfo.lastOrNull()?.key == FooterKey },
    contentPadding = contentPadding,
    octoActivity = octoActivity,
    toolbarState = toolbarState,
) {
    LazyVerticalGrid(
        state = state,
        contentPadding = contentPadding,
        verticalArrangement = verticalArrangement,
        horizontalArrangement = Arrangement.spacedBy(horizontalPadding),
        content = {
            content()

            item(key = FooterKey, span = { GridItemSpan(columnCount) }) {
                Spacer(modifier = Modifier.height(1.dp))
            }
        },
        modifier = modifier.testTag("controls:scroller"),
        columns = GridCells.Fixed(columnCount)
    )
}

@Composable
fun HeaderSynchronizedScrollBox(
    toolbarState: OctoToolbar.State,
    octoActivity: OctoActivity?,
    firstVisibleItemIndex: () -> Int,
    firstVisibleItemScrollOffset: () -> Int,
    footerVisible: () -> Boolean,
    contentPadding: PaddingValues = PaddingValues(),
    content: @Composable BoxScope.() -> Unit
) {
    val density = LocalDensity.current
    val headerVisible by with(LocalDensity.current) {
        val cutOff = OctoAppTheme.dimens.margin3.toPx()
        derivedStateOf {
            firstVisibleItemIndex() == 0 && firstVisibleItemScrollOffset() < cutOff
        }
    }

    val scrolledToTop by remember(density, contentPadding) {
        derivedStateOf {
            firstVisibleItemIndex() == 0 && firstVisibleItemScrollOffset() < 20
        }
    }

    val scrolledToBottom by remember(density, contentPadding) {
        derivedStateOf {
            footerVisible()
        }
    }

    LaunchedEffect(headerVisible, toolbarState) {
        octoActivity?.octoToolbar?.state = toolbarState.takeIf { headerVisible } ?: OctoToolbar.State.Hidden
        octoActivity?.octo?.isVisible = octoActivity?.octoToolbar?.state != OctoToolbar.State.Hidden
    }

    Box(modifier = Modifier.clip(RectangleShape)) {
        content()
        ScrollEdge(visible = { !scrolledToTop }, alignment = Alignment.TopCenter)
        ScrollEdge(visible = { !scrolledToBottom }, alignment = Alignment.BottomCenter)
    }
}

@Composable
fun BoxScope.ScrollEdge(
    visible: () -> Boolean,
    alignment: Alignment,
) {
    AnimatedVisibility(
        visible = visible(),
        enter = if (isSystemInDarkTheme()) fadeIn() + expandHorizontally(expandFrom = Alignment.CenterHorizontally) else fadeIn(),
        exit = if (isSystemInDarkTheme()) fadeOut() + shrinkHorizontally(shrinkTowards = Alignment.CenterHorizontally) else fadeOut(),
        modifier = Modifier
            .fillMaxWidth()
            .align(alignment)
    ) {
        if (isSystemInDarkTheme()) {
            // Hairline in dark mode
            Box(
                modifier = Modifier
                    .height(2.dp)
                    .background(OctoAppTheme.colors.inputBackground)
                    .fillMaxWidth()
            )
        } else {
            // Drop shadow in light mode
            Box(
                modifier = Modifier
                    .height(10.dp)
                    .offset(y = if (alignment == Alignment.BottomCenter) 10.dp else (-10).dp)
                    .shadow(elevation = if (alignment == Alignment.BottomCenter) 10.dp else 3.dp)
                    .fillMaxWidth()
            )
        }
    }
}