package de.crysxd.baseui.compose.controls.webcam

import android.app.Activity
import android.graphics.Bitmap
import android.net.Uri
import android.widget.ImageView
import androidx.annotation.StringRes
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import de.crysxd.baseui.R
import de.crysxd.baseui.common.NetworkStateProvider
import de.crysxd.baseui.compose.framework.staticStateOf
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_HLS_WEBCAM
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.asStyleFileSize
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.network.SpaghettiDetectiveWebcamConnection
import de.crysxd.octoapp.base.usecase.GetWebcamSettingsUseCase
import de.crysxd.octoapp.base.usecase.GetWebcamSnapshotUseCase
import de.crysxd.octoapp.base.usecase.HandleAutomaticLightEventUseCase
import de.crysxd.octoapp.base.usecase.ShareImageUseCase
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MjpegConnection3
import io.ktor.http.Url
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import kotlin.math.roundToInt

@OptIn(ExperimentalCoroutinesApi::class)
class WebcamControlsViewModel(
    private val instanceId: String,
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPreferences: OctoPreferences,
    private val shareImageUseCase: ShareImageUseCase,
    private val getWebcamSnapshotUseCase: GetWebcamSnapshotUseCase,
    private val getWebcamSettingsUseCase: GetWebcamSettingsUseCase,
    private val handleAutomaticLightEventUseCase: HandleAutomaticLightEventUseCase,
) : ViewModel() {

    companion object {
        private var instanceCounter = 0
    }

    private val networkState = NetworkStateProvider(viewModelScope, BaseInjector.get().context())
    private val tag = "WebcamViewModel/${instanceCounter++}"
    private var latestState: UiState = UiState.Loading(0)
    private val retrySource = MutableStateFlow(0)
    val state = retrySource.flatMapLatest {
        combine(
            BillingManager.isFeatureEnabledFlow(FEATURE_HLS_WEBCAM),
            getDataSaverStateFlow(),
            getWebcamState(),
        ) { hlsEnabled, dataSaverInterval, webcamState ->
            createWebcamFlow(hlsEnabled, webcamState, dataSaverInterval)
        }.flatMapLatest {
            it
        }.onStart {
            emit(UiState.Loading(latestState.webcamCount))
        }.catch {
            // Delay so the user sees at least a bit of loading
            delay(300)
            emit(UiState.Error(true, webcamCount = latestState.webcamCount, exception = it))
        }
    }.onStart {
        emit(UiState.Loading(latestState.webcamCount))

        try {
            handleAutomaticLightEventUseCase.execute(HandleAutomaticLightEventUseCase.Event.WebcamVisible(tag))
        } catch (e: Exception) {
            Timber.e(e)
        }
    }.onCompletion {
        try {
            // Execute blocking as a normal execute switches threads causing the task never to be done as the current scope
            // is about to be terminated
            handleAutomaticLightEventUseCase.executeBlocking(HandleAutomaticLightEventUseCase.Event.WebcamGone(tag))
        } catch (e: Exception) {
            Timber.e(e)
        }
    }.onEach {
        latestState = it
    }.flowOn(
        context = Dispatchers.Default
    ).shareIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 3000)
    )

    // Hash of all fields in octoPreferences that should cause webcam to reconnect
    private fun OctoPrintInstanceInformationV3.relevantHash() = listOf(
        settings?.webcam,
        appSettings?.activeWebcamIndex,
    ).hashCode()

    private suspend fun getWebcamState() = octoPrintRepository.instanceInformationFlow(instanceId = instanceId)
        .distinctUntilChangedBy { it?.relevantHash() }
        .flatMapLatest { info ->
            requireNotNull(info) { "Instance $instanceId not found" }
            getWebcamSettingsUseCase.execute(info).map { ws ->
                val activeWebcamIndex = info.appSettings?.activeWebcamIndex ?: 0
                val preferredSettings = ws.getOrNull(activeWebcamIndex)
                val webcamSettings = preferredSettings ?: ws.firstOrNull()

                if (preferredSettings == null) {
                    switchWebcam(0)
                }

                CompiledWebcamState(
                    resolvedSettings = webcamSettings,
                    webcamCount = ws.size,
                    instanceId = info.id,
                    activeWebcamIndex = activeWebcamIndex,
                )
            }
        }.distinctUntilChanged()

    private suspend fun getDataSaverStateFlow(): Flow<Long> {
        val dataSaverIntervalFlow = octoPreferences.updatedFlow
            .map { octoPreferences.dataSaverWebcamInterval }
            .onStart { emit(octoPreferences.dataSaverWebcamInterval) }
            .distinctUntilChanged()

        val companionInstalledFlow = octoPrintRepository.instanceInformationFlow(instanceId = instanceId)
            .map { it?.hasPlugin(OctoPlugins.OctoApp, "1.0.12") == true }
            .distinctUntilChanged()
            .combine(dataSaverIntervalFlow) { available, interval ->
                interval.takeIf { available } ?: 0
            }

        return networkState.networkState.combine(companionInstalledFlow) { ns, interval ->
            interval.takeIf { ns is NetworkStateProvider.NetworkState.WifiNotConnected } ?: 0
        }
    }

    private suspend fun createWebcamFlow(
        richFlowsEnabled: Boolean,
        webcamState: CompiledWebcamState,
        dataSaverInterval: Long
    ) = flow {
        try {
            Timber.tag(tag).i("Refresh with streamUrl: ${webcamState.resolvedSettings?.description}")
            Timber.tag(tag).i("Webcam count: ${webcamState.webcamCount}")
            Timber.tag(tag).i("Data saver interval: $dataSaverInterval")
            emit(UiState.Loading(webcamState.webcamCount))

            // Check if webcam is configured
            if (webcamState.resolvedSettings == null || !webcamState.resolvedSettings.webcamSettings.webcamEnabled) {
                return@flow emit(
                    UiState.WebcamNotAvailable(
                        webcamCount = 0,
                        text = R.string.please_configure_your_webcam_in_octoprint
                    )
                )
            }

            // Open stream
            when {
                dataSaverInterval > 0 -> emitDataSaverCam(
                    instanceId = webcamState.instanceId,
                    dataSaverInterval = dataSaverInterval,
                    webcamSettings = webcamState.resolvedSettings.webcamSettings,
                    webcamIndex = webcamState.activeWebcamIndex,
                    webcamCount = webcamState.webcamCount,
                )

                else -> when (webcamState.resolvedSettings) {
                    is ResolvedWebcamSettings.HlsSettings -> emitRichFlow(
                        richFlowsEnabled = richFlowsEnabled,
                        url = webcamState.resolvedSettings.url.toString(),
                        basicAuth = webcamState.resolvedSettings.basicAuth,
                        webcamSettings = webcamState.resolvedSettings.webcamSettings,
                        webcamCount = webcamState.webcamCount
                    )

                    is ResolvedWebcamSettings.RtspSettings -> emitRichFlow(
                        richFlowsEnabled = richFlowsEnabled,
                        url = webcamState.resolvedSettings.url,
                        basicAuth = webcamState.resolvedSettings.basicAuth,
                        webcamSettings = webcamState.resolvedSettings.webcamSettings,
                        webcamCount = webcamState.webcamCount
                    )

                    is ResolvedWebcamSettings.MjpegSettings -> emitMjpegFlow(
                        mjpegSettings = webcamState.resolvedSettings,
                        webcamCount = webcamState.webcamCount
                    )

                    is ResolvedWebcamSettings.SpaghettiCamSettings -> emitSpaghettiCam(
                        spaghettiSettings = webcamState.resolvedSettings,
                        webcamCount = webcamState.webcamCount
                    )
                }
            }
        } catch (e: CancellationException) {
            Timber.tag(tag).w("Webcam stream cancelled")
        } catch (e: Exception) {
            Timber.e(e)
            emit(UiState.Error(true, webcamCount = webcamState.webcamCount, exception = e))
            delay(Long.MAX_VALUE)
        }
    }

    private suspend fun FlowCollector<UiState>.emitRichFlow(
        richFlowsEnabled: Boolean,
        url: String,
        basicAuth: String?,
        webcamSettings: WebcamSettings,
        webcamCount: Int
    ) {
        val player by lazy { ExoPlayer.Builder(BaseInjector.get().context()).build() }
        octoPreferences.updatedFlow2.flatMapLatest { settings ->
            flow {
                if (richFlowsEnabled) {
                    emit(
                        UiState.RichStreamReady(
                            uri = Uri.parse(url),
                            player = player,
                            webcamCount = webcamCount,
                            authHeader = basicAuth,
                            flipV = webcamSettings.flipV,
                            flipH = webcamSettings.flipH,
                            rotate90 = webcamSettings.rotate90,
                            showResolution = settings.isShowWebcamResolution,
                            enforcedAspectRatio = webcamSettings.streamRatio.takeIf { settings.isAspectRatioFromOctoPrint },
                        )
                    )
                } else {
                    emit(UiState.RichStreamDisabled(webcamCount = webcamCount))
                }
            }
        }.onStart {
            if (richFlowsEnabled) {
                // Setup player
                Timber.i("Starting rich player: $url")
                val mediaItem = MediaItem.fromUri(url)
                val dataSourceFactory = DefaultHttpDataSource.Factory()
                basicAuth?.let {
                    dataSourceFactory.setDefaultRequestProperties(mapOf("Authorization" to it))
                }
                val mediaSourceFactory = DefaultMediaSourceFactory(dataSourceFactory)
                player.setMediaSource(mediaSourceFactory.createMediaSource(mediaItem))
                player.prepare()
                player.play()
            }
        }.onCompletion {
            Timber.i("Stopping rich player flow")
            player.stop()
        }.flowOn(
            context = Dispatchers.Main
        ).collect {
            emit(it)
        }
    }

    private suspend fun FlowCollector<UiState>.emitMjpegFlow(
        mjpegSettings: ResolvedWebcamSettings.MjpegSettings,
        webcamCount: Int
    ) = emitPerformanceMjpegFlow {
        MjpegConnection3(streamUrl = mjpegSettings.url, name = tag, httpSettings = SharedBaseInjector.get().httpClientSettings()).load()
            .combine(octoPreferences.updatedFlow2) { snapshot, settings ->
                when (snapshot) {
                    is MjpegConnection3.MjpegSnapshot.Loading -> UiState.Loading(webcamCount)

                    is MjpegConnection3.MjpegSnapshot.Frame -> UiState.FrameReady(
                        frame = staticStateOf(snapshot.frame),
                        webcamCount = webcamCount,
                        flipV = mjpegSettings.webcamSettings.flipV,
                        flipH = mjpegSettings.webcamSettings.flipH,
                        rotate90 = mjpegSettings.webcamSettings.rotate90,
                        warning = staticStateOf(
                            when {
                                // More than 100kb per second -> Warning
                                snapshot.analytics.byteCountPerMinute > 100_000_000 -> BaseInjector.get().context().run {
                                    Warning(
                                        id = "webcam_data_usage_2",
                                        dismissible = snapshot.analytics.byteCountPerMinute < 750_000_000,
                                        actionText = getString(R.string.learn_more),
                                        actionUri = UriLibrary.getFaqUri("webcamDataUsage"),
                                        text = getString(
                                            R.string.webcam_widget___data_warning,
                                            snapshot.analytics.byteCountPerMinute.asStyleFileSize(intOnly = true)
                                        )
                                    )
                                }

                                else -> null
                            }
                        ),
                        fps = staticStateOf(snapshot.analytics.fps),
                        showResolution = settings.isShowWebcamResolution,
                        enforcedAspectRatio = mjpegSettings.webcamSettings.streamRatio.takeIf { settings.isAspectRatioFromOctoPrint },
                        frameTime = System.currentTimeMillis(),
                    )
                }
            }.catch {
                Timber.tag(tag).e(it)
                emit(
                    UiState.Error(
                        isManualReconnect = true,
                        streamUrl = mjpegSettings.description,
                        webcamCount = webcamCount,
                        exception = it,
                    )
                )
            }
    }

    private suspend fun FlowCollector<UiState>.emitSpaghettiCam(
        spaghettiSettings: ResolvedWebcamSettings.SpaghettiCamSettings,
        webcamCount: Int
    ) = emitPerformanceMjpegFlow {
        delay(100)
        SpaghettiDetectiveWebcamConnection(
            webcamIndex = spaghettiSettings.webcamIndex,
            octoPrint = octoPrintProvider.octoPrint(),
            name = tag
        ).load().combine(octoPreferences.updatedFlow2) { snapshot, settings ->
            when (snapshot) {
                is SpaghettiDetectiveWebcamConnection.SpaghettiCamSnapshot.Loading -> UiState.Loading(webcamCount)

                is SpaghettiDetectiveWebcamConnection.SpaghettiCamSnapshot.NotWatching -> UiState.WebcamNotAvailable(
                    webcamCount = webcamCount,
                    text = R.string.configure_remote_acces___spaghetti_detective___webcam_not_watching
                )

                is SpaghettiDetectiveWebcamConnection.SpaghettiCamSnapshot.Frame -> UiState.FrameReady(
                    frame = staticStateOf(snapshot.frame),
                    webcamCount = webcamCount,
                    flipV = spaghettiSettings.webcamSettings.flipV,
                    flipH = spaghettiSettings.webcamSettings.flipH,
                    rotate90 = spaghettiSettings.webcamSettings.rotate90,
                    nextFrameDelayMs = snapshot.nextFrameDelayMs,
                    frameTime = System.currentTimeMillis(),
                    fps = staticStateOf(0f),
                    showResolution = settings.isShowWebcamResolution,
                    enforcedAspectRatio = spaghettiSettings.webcamSettings.streamRatio.takeIf { settings.isAspectRatioFromOctoPrint },
                )
            }
        }.catch {
            Timber.tag(tag).e(it)
            emit(
                UiState.Error(
                    isManualReconnect = true,
                    streamUrl = spaghettiSettings.description,
                    webcamCount = webcamCount,
                    exception = it,
                )
            )
        }
    }

    private suspend fun FlowCollector<UiState>.emitDataSaverCam(
        instanceId: String?,
        webcamIndex: Int,
        dataSaverInterval: Long,
        webcamSettings: WebcamSettings,
        webcamCount: Int,
    ) = emitPerformanceMjpegFlow {
        val companionWarning = Warning(
            id = "webcam_companion_not_installed",
            dismissible = false,
            text = BaseInjector.get().context().getString(R.string.wear_webcam___companion_message)
        )
        getWebcamSnapshotUseCase.execute(
            GetWebcamSnapshotUseCase.Params(
                instanceId = instanceId,
                illuminateIfPossible = true,
                interval = dataSaverInterval,
                webcamIndex = webcamIndex,
                maxSize = 720,
            )
        ).combine(octoPreferences.updatedFlow2) { frame, settings ->
            UiState.FrameReady(
                frame = staticStateOf(frame.bitmap),
                webcamCount = webcamCount,
                flipV = false,
                frameTime = System.currentTimeMillis(),
                flipH = false,
                rotate90 = false,
                nextFrameDelayMs = dataSaverInterval,
                warning = staticStateOf(companionWarning.takeIf { frame.advertiseCompanion }),
                fps = staticStateOf(0f),
                showResolution = settings.isShowWebcamResolution,
                enforcedAspectRatio = webcamSettings.streamRatio.takeIf { octoPreferences.isAspectRatioFromOctoPrint },
            )
        }.catch {
            Timber.tag(tag).e(it)
            emit(
                UiState.Error(
                    isManualReconnect = true,
                    streamUrl = null,
                    webcamCount = webcamCount,
                    exception = it,
                )
            )
        }
    }

    private val OctoPreferences.isAspectRatioFromOctoPrint get() = webcamAspectRatioSource == OctoPreferences.VALUE_WEBCAM_ASPECT_RATIO_SOURCE_OCTOPRINT

    fun storeScaleType(scaleType: ImageView.ScaleType, isFullscreen: Boolean) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        octoPrintRepository.updateAppSettings(instanceId) {
            if (isFullscreen) {
                it.copy(webcamFullscreenScaleType = scaleType.ordinal)
            } else {
                it.copy(webcamScaleType = scaleType.ordinal)
            }
        }
    }

    fun storeAspectRatio(ratio: String) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        octoPrintRepository.updateAppSettings(instanceId) {
            it.copy(webcamLastAspectRatio = ratio)
        }
    }

    fun nextWebcam() = switchWebcam(null)

    fun retry() = retrySource.update { it + 1 }

    private fun switchWebcam(index: Int?) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        octoPrintRepository.updateAppSettings(instanceId) {
            val activeIndex = index ?: ((it.activeWebcamIndex + 1) % latestState.webcamCount)
            Timber.tag(tag).i("Switching to webcam $activeIndex")
            it.copy(activeWebcamIndex = activeIndex)
        }
    }

    fun getScaleType(isFullscreen: Boolean, default: ImageView.ScaleType) = ImageView.ScaleType.values()[
            octoPrintRepository.get(instanceId)?.appSettings?.let {
                if (isFullscreen) it.webcamFullscreenScaleType else it.webcamScaleType
            } ?: default.ordinal
    ]

    fun getInitialAspectRatio() = try {
        val aspectRatio = octoPrintRepository.get(instanceId)?.let {
            it.appSettings?.webcamLastAspectRatio ?: it.settings?.webcam?.streamRatio
        } ?: "16:9"
        val (w, h) = aspectRatio.split(":")
        w.toFloat() / h.toFloat()
    } catch (e: Exception) {
        Timber.e(e)
        16 / 9f
    }

    fun shareImage(activity: Activity, bitmap: suspend () -> Bitmap) = viewModelScope.launch(Dispatchers.Default + ExceptionReceivers.coroutineExceptionHandler) {
        val octoPrint = octoPrintRepository.get(instanceId)
        val current = octoPrintProvider.passiveCurrentMessageFlow("share-image").firstOrNull()
        val isPrinting = current?.state?.flags?.isPrinting() == true
        val imageName = listOfNotNull(
            octoPrint?.label,
            current?.job?.file?.name?.takeIf { isPrinting }?.split(".")?.firstOrNull(),
            current?.progress?.completion?.let { "${it.roundToInt()}percent" }?.takeIf { isPrinting },
            SimpleDateFormat("yyyy-MM-dd__hh-mm-ss", Locale.ENGLISH).format(Date())
        ).joinToString("__")
        shareImageUseCase.execute(ShareImageUseCase.Params(context = activity, bitmap = bitmap(), imageName = imageName))
    }

    private suspend fun FlowCollector<UiState>.emitPerformanceMjpegFlow(flowFactory: suspend () -> Flow<UiState>) {
        val frameState = mutableStateOf<Bitmap>(Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888))
        val warningState = mutableStateOf<Warning?>(null)
        val fpsState = mutableStateOf(0f)
        var lastState: UiState? = null

        // Basically what we do here is to reduce the number of UiState.FrameReady to be emitted. The goal is to only
        // have one emitted initially and then to update warning and frames via the states to reduce recomposition to a minimum

        flowFactory().onEach {
            (it as? UiState.FrameReady)?.frame?.value?.let { f -> frameState.value = f }
            (it as? UiState.FrameReady)?.fps?.value?.let { f -> fpsState.value = f }
            (it as? UiState)?.warning?.value?.let { w -> warningState.value = w }

            if (lastState !is UiState.FrameReady || it !is UiState.FrameReady) {
                val s = when (it) {
                    is UiState.FrameReady -> it.copy(frame = frameState, warning = warningState, fps = fpsState)
                    else -> it
                }
                Timber.i("Emit: $it")
                emit(s)
            }

            lastState = it
        }.flowOn(Dispatchers.Default).collect()
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "WebcamControlsViewModel@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = WebcamControlsViewModel(
            instanceId = instanceId,
            octoPrintProvider = BaseInjector.get().octoPrintProvider(),
            octoPreferences = BaseInjector.get().octoPreferences(),
            getWebcamSettingsUseCase = BaseInjector.get().getWebcamSettingsUseCase(),
            getWebcamSnapshotUseCase = BaseInjector.get().getWebcamSnapshotUseCase2(),
            handleAutomaticLightEventUseCase = BaseInjector.get().handleAutomaticLightEventUseCase(),
            octoPrintRepository = BaseInjector.get().octorPrintRepository(),
            shareImageUseCase = BaseInjector.get().shareImageUseCase(),
        ) as T
    }

    private data class CompiledWebcamState(
        val resolvedSettings: ResolvedWebcamSettings?,
        val webcamCount: Int,
        val instanceId: String?,
        val activeWebcamIndex: Int,
    )

    data class Warning(
        val id: String,
        val text: String,
        val actionText: String? = null,
        val actionUri: Url? = null,
        val dismissible: Boolean,
    )

    sealed class UiState(
        open val webcamCount: Int,
        open val warning: State<Warning?> = staticStateOf(null)
    ) {

        companion object {
            private var globalCounter = 0
        }

        val id = globalCounter++
        val canSwitchWebcam: Boolean get() = webcamCount > 1

        data class WebcamNotAvailable(
            @StringRes val text: Int,
            override val webcamCount: Int,
            override val warning: State<Warning?> = staticStateOf(null),
        ) : UiState(webcamCount, warning)

        data class Loading(
            override val webcamCount: Int = 0,
            override val warning: State<Warning?> = staticStateOf(null),
        ) : UiState(webcamCount, warning)

        data class RichStreamDisabled(
            override val webcamCount: Int,
            override val warning: State<Warning?> = staticStateOf(null),
        ) : UiState(webcamCount, warning)

        data class Error(
            val isManualReconnect: Boolean,
            val exception: Throwable,
            val streamUrl: String? = null,
            override val webcamCount: Int,
            override val warning: State<Warning?> = staticStateOf(null),
        ) : UiState(webcamCount, warning)

        data class RichStreamReady(
            val uri: Uri,
            val player: ExoPlayer,
            val authHeader: String?,
            override val webcamCount: Int,
            val flipH: Boolean,
            val flipV: Boolean,
            val rotate90: Boolean,
            val enforcedAspectRatio: String?,
            val showResolution: Boolean,
            override val warning: State<Warning?> = staticStateOf(null),
        ) : UiState(webcamCount, warning)

        data class FrameReady(
            val frame: State<Bitmap>,
            override val webcamCount: Int,
            val fps: State<Float>,
            val flipH: Boolean,
            val flipV: Boolean,
            val rotate90: Boolean,
            val frameTime: Long,
            val nextFrameDelayMs: Long? = null,
            val enforcedAspectRatio: String?,
            val showResolution: Boolean,
            override val warning: State<Warning?> = staticStateOf(null),
        ) : UiState(webcamCount, warning)
    }
}

