package de.crysxd.baseui.compose.theme

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import de.crysxd.baseui.R

@Suppress("Unused")
interface OctoAppColors {
    val activeColorScheme: Color
        @Composable get() = defaultColorScheme
    val activeColorSchemeLight: Color
        @Composable get() = defaultColorSchemeLight

    val activeMenuStyleForeground: Color
        @Composable get() = menuStyleNeutralForeground
    val activeMenuStyleBackground: Color
        @Composable get() = menuStyleNeutralBackground

    // XML Link
    val primary: Color
        @Composable get() = colorResource(R.color.primary)
    val primaryDark: Color
        @Composable get() = colorResource(R.color.primary_dark)
    val accent: Color
        @Composable get() = colorResource(R.color.accent)
    val windowBackground: Color
        @Composable get() = colorResource(R.color.window_background)
    val inputBackground: Color
        @Composable get() = colorResource(R.color.input_background)
    val inputBackgroundAlternative: Color
        @Composable get() = colorResource(R.color.input_background_alternative)
    val lightText: Color
        @Composable get() = colorResource(R.color.light_text)
    val normalText: Color
        @Composable get() = colorResource(R.color.normal_text)
    val darkText: Color
        @Composable get() = colorResource(R.color.dark_text)
    val inverseText: Color
        @Composable get() = colorResource(R.color.inverse_text)
    val colorError: Color
        @Composable get() = colorResource(R.color.color_error)
    val colorHot: Color
        @Composable get() = colorResource(R.color.color_hot)
    val colorCold: Color
        @Composable get() = colorResource(R.color.color_cold)
    val temperatureText: Color
        @Composable get() = colorResource(R.color.temperature_text)
    val textColoredBackground: Color
        @Composable get() = colorResource(R.color.text_colored_background)
    val swimBubbles: Color
        @Composable get() = colorResource(R.color.swim_bubbles)
    val swimLines: Color
        @Composable get() = colorResource(R.color.swim_lines)
    val secondaryButtonText: Color
        @Composable get() = colorResource(R.color.secondary_button_text)
    val primaryButtonBackground: Color
        @Composable get() = colorResource(R.color.primary_button_background)
    val buttonMoreForeground: Color
        @Composable get() = colorResource(R.color.button_more_foreground)
    val analysisBad: Color
        @Composable get() = colorResource(R.color.analysis_bad)
    val analysisNormal: Color
        @Composable get() = colorResource(R.color.analysis_normal)
    val analysisGood: Color
        @Composable get() = colorResource(R.color.analysis_good)
    val buttonDisabled: Color
        @Composable get() = colorResource(R.color.button_disabled)
    val menuPinForeground: Color
        @Composable get() = colorResource(R.color.menu_pin_foreground)
    val menuPinBackground: Color
        @Composable get() = colorResource(R.color.menu_pin_background)
    val appWidgetButtonBackground: Color
        @Composable get() = colorResource(R.color.app_widget_button_background)
    val controlCenterBackground: Color
        @Composable get() = colorResource(R.color.control_center_background)
    val menuStyleSupportForeground: Color
        @Composable get() = colorResource(R.color.menu_style_support_foreground)
    val menuStyleSupportBackground: Color
        @Composable get() = colorResource(R.color.menu_style_support_background)
    val menuStyleSettingsForeground: Color
        @Composable get() = colorResource(R.color.menu_style_settings_foreground)
    val menuStyleSettingsBackground: Color
        @Composable get() = colorResource(R.color.menu_style_settings_background)
    val menuStylePrinterForeground: Color
        @Composable get() = colorResource(R.color.menu_style_printer_foreground)
    val menuStylePrinterBackground: Color
        @Composable get() = colorResource(R.color.menu_style_printer_background)
    val menuStyleOctoprintForeground: Color
        @Composable get() = colorResource(R.color.menu_style_octoprint_foreground)
    val menuStyleOctoprintBackground: Color
        @Composable get() = colorResource(R.color.menu_style_octoprint_background)
    val menuStyleNeutralForeground: Color
        @Composable get() = colorResource(R.color.menu_style_neutral_foreground)
    val menuStyleNeutralBackground: Color
        @Composable get() = colorResource(R.color.menu_style_neutral_background)
    val defaultColorScheme: Color
        @Composable get() = colorResource(R.color.default_color_scheme)
    val defaultColorSchemeLight: Color
        @Composable get() = colorResource(R.color.default_color_scheme_light)
    val redColorScheme: Color
        @Composable get() = colorResource(R.color.red_color_scheme)
    val redColorSchemeLight: Color
        @Composable get() = colorResource(R.color.red_color_scheme_light)
    val orangeColorScheme: Color
        @Composable get() = colorResource(R.color.orange_color_scheme)
    val orangeColorSchemeLight: Color
        @Composable get() = colorResource(R.color.orange_color_scheme_light)
    val yellowColorScheme: Color
        @Composable get() = colorResource(R.color.yellow_color_scheme)
    val yellowColorSchemeLight: Color
        @Composable get() = colorResource(R.color.yellow_color_scheme_light)
    val greenColorScheme: Color
        @Composable get() = colorResource(R.color.green_color_scheme)
    val greenColorSchemeLight: Color
        @Composable get() = colorResource(R.color.green_color_scheme_light)
    val blueColorScheme: Color
        @Composable get() = colorResource(R.color.blue_color_scheme)
    val blueColorSchemeLight: Color
        @Composable get() = colorResource(R.color.blue_color_scheme_light)
    val violetColorScheme: Color
        @Composable get() = colorResource(R.color.violet_color_scheme)
    val violetColorSchemeLight: Color
        @Composable get() = colorResource(R.color.violet_color_scheme_light)
    val whiteColorScheme: Color
        @Composable get() = colorResource(R.color.white_color_scheme)
    val whiteColorSchemeLight: Color
        @Composable get() = colorResource(R.color.white_color_scheme_light)
    val blackColorScheme: Color
        @Composable get() = colorResource(R.color.black_color_scheme)
    val blackColorSchemeLight: Color
        @Composable get() = colorResource(R.color.black_color_scheme_light)
    val gcodeExtrusion: Color
        @Composable get() = colorResource(R.color.gcode_extrusion)
    val gcodePrevious: Color
        @Composable get() = colorResource(R.color.gcode_previous)
    val gcodeRemaining: Color
        @Composable get() = colorResource(R.color.gcode_remaining)
    val gcodeUnsupported: Color
        @Composable get() = colorResource(R.color.gcode_unsupported)
    val gcodeTravel: Color
        @Composable get() = colorResource(R.color.gcode_travel)
    val gcodePrintHead: Color
        @Composable get() = colorResource(R.color.gcode_print_head)
    val snackbarNeutral: Color
        @Composable get() = colorResource(R.color.snackbar_neutral)
    val snackbarPositive: Color
        @Composable get() = colorResource(R.color.snackbar_positive)
    val snackbarNegative: Color
        @Composable get() = colorResource(R.color.snackbar_negative)

    val octoeverywhere: Color
        @Composable get() = colorResource(R.color.octoeverywhere)
    val spaghettiDetective: Color
        @Composable get() = colorResource(R.color.spaghetti_detective)
    val ngrok: Color
        @Composable get() = colorResource(R.color.ngrok)
    val tailscale: Color
        @Composable get() = colorResource(R.color.tailscale)
    val white: Color
        @Composable get() = colorResource(R.color.white)
    val black: Color
        @Composable get() = colorResource(R.color.black)
    val red: Color
        @Composable get() = colorResource(R.color.red)
    val redTranslucent: Color
        @Composable get() = colorResource(R.color.red_translucent)
    val blue: Color
        @Composable get() = colorResource(R.color.blue)
    val blueTranslucent: Color
        @Composable get() = colorResource(R.color.blue_translucent)
    val darkBlue: Color
        @Composable get() = colorResource(R.color.dark_blue)
    val darkBlueTransparent: Color
        @Composable get() = colorResource(R.color.dark_blue_transparent)
    val yellow: Color
        @Composable get() = colorResource(R.color.yellow)
    val yellowTranslucent: Color
        @Composable get() = colorResource(R.color.yellow_translucent)
    val green: Color
        @Composable get() = colorResource(R.color.green)
    val greenTranslucent: Color
        @Composable get() = colorResource(R.color.green_translucent)
    val orange: Color
        @Composable get() = colorResource(R.color.orange)
    val orangeTranslucent: Color
        @Composable get() = colorResource(R.color.orange_translucent)
    val green2: Color
        @Composable get() = colorResource(R.color.green_2)
    val green3: Color
        @Composable get() = colorResource(R.color.green_3)
    val violet: Color
        @Composable get() = colorResource(R.color.violet)
    val lightGrey: Color
        @Composable get() = colorResource(R.color.light_grey)
    val lightGreyTranslucent: Color
        @Composable get() = colorResource(R.color.light_grey_translucent)
    val veryLightGrey: Color
        @Composable get() = colorResource(R.color.very_light_grey)
    val darkGrey: Color
        @Composable get() = colorResource(R.color.dark_grey)
    val darkGrey2: Color
        @Composable get() = colorResource(R.color.dark_grey_2)
    val lightBlue: Color
        @Composable get() = colorResource(R.color.light_blue)
    val blackTranslucent: Color
        @Composable get() = colorResource(R.color.black_translucent)
    val blackTranslucent2: Color
        @Composable get() = colorResource(R.color.black_translucent_2)
    val blackTranslucent3: Color
        @Composable get() = colorResource(R.color.black_translucent_3)
    val whiteTranslucent: Color
        @Composable get() = colorResource(R.color.white_translucent)
    val statusBarScrim: Color
        @Composable get() = colorResource(R.color.status_bar_scrim)
    val whiteTranslucent2: Color
        @Composable get() = colorResource(R.color.white_translucent_2)
    val whiteTranslucent3: Color
        @Composable get() = colorResource(R.color.white_translucent_3)
}