package de.crysxd.baseui.compose.controls.cancelobject

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.gcode.render.models.RenderStyle
import de.crysxd.octoapp.base.usecase.GenerateRenderStyleUseCase
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn

class CancelObjectRenderViewModel(
    instanceId: String,
    octoPreferences: OctoPreferences,
    octoPrintRepository: OctoPrintRepository,
    private val generateRenderStyleUseCase: GenerateRenderStyleUseCase,
) : ViewModel() {

    val state = combine(
        octoPreferences.updatedFlow2.distinctUntilChangedBy { it.gcodePreviewSettings },
        octoPrintRepository.instanceInformationFlow(instanceId).distinctUntilChangedBy { it?.activeProfile }
    ) { prefs, instance ->
        State.Ready(
            settings = prefs.gcodePreviewSettings,
            printerProfile = requireNotNull(instance?.activeProfile) { "Missing printer profile" },
            renderStyle = generateRenderStyleUseCase.execute(instance)
        ) as State
    }.onStart {
        emit(State.Loading)
    }.shareIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(),
        replay = 1
    )

    sealed class State {
        object Loading : State()
        data class Error(val error: Throwable) : State()
        data class Ready(
            val settings: GcodePreviewSettings,
            val renderStyle: RenderStyle,
            val printerProfile: PrinterProfile,
        ) : State()
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "CancelObjectRenderViewModel@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = CancelObjectRenderViewModel(
            instanceId = instanceId,
            octoPrintRepository = BaseInjector.get().octorPrintRepository(),
            generateRenderStyleUseCase = BaseInjector.get().generateRenderStyleUseCase(),
            octoPreferences = BaseInjector.get().octoPreferences()
        ) as T
    }
}