package de.crysxd.baseui.compose.framework

import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.remember

fun <T> staticStateOf(t: T) = object : State<T> {
    override val value get() = t
    override fun hashCode() = t.hashCode()
    override fun equals(other: Any?) = t?.equals(other) ?: (t == other)
}

@Composable
fun <T> rememberValue(t: T) = remember { Holder(t) }

data class Holder<T>(var value: T)