package de.crysxd.baseui.compose.framework

object TestTags {
    object BottomBar {
        val Status: TestTag = "bottom-bar-status"
        val Pause: TestTag = "bottom-bar-pause"
        val Cancel: TestTag = "bottom-bar-cancel"
        val Resume: TestTag = "bottom-bar-resume"
        val Temperatures: TestTag = "bottom-bar-temperatures"
        val MainMenu: TestTag = "bottom-bar-main-menu"
        val Start: TestTag = "bottom-bar-start"
        val SwipeTrack: TestTag = "bottom-bar-swipe-track"
    }
}

typealias TestTag = String