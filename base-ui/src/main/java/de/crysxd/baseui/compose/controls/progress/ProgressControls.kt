package de.crysxd.baseui.compose.controls.progress

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.AlertDialog
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.flowlayout.FlowCrossAxisAlignment
import com.google.accompanist.flowlayout.FlowMainAxisAlignment
import com.google.accompanist.flowlayout.FlowRow
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.ControlsScaffold
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.controls.gcodepreview.GcodeControlsState
import de.crysxd.baseui.compose.controls.gcodepreview.GcodePreviewControlsViewModel
import de.crysxd.baseui.compose.controls.progress.ProgressControlsViewModel.Companion.UPDATE_INTERVAL
import de.crysxd.baseui.compose.framework.LinkButton
import de.crysxd.baseui.compose.framework.LocalOctoPrint
import de.crysxd.baseui.compose.framework.OctoPrintPicassoImage
import de.crysxd.baseui.compose.framework.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.rememberAsFormattedDuration
import de.crysxd.baseui.compose.framework.rememberAsFormattedEta
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.asPrintTimeLeftImagePainter
import de.crysxd.baseui.menu.base.MenuBottomSheetFragment
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_GCODE_PREVIEW
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings
import de.crysxd.octoapp.base.ext.asPrintTimeLeftOriginColor
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.job.ProgressInformation
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_AVERAGE
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_GENIUS
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_MIXED_ANALYSIS
import de.crysxd.octoapp.engine.models.printer.PrinterState
import java.text.DecimalFormat
import kotlin.math.roundToInt

@Composable
fun ProgressControls(
    state: () -> ProgressControlsState,
    gcodeState: () -> GcodeControlsState,
    editState: EditState,
    modifier: Modifier = Modifier
) {
    ControlsScaffold(
        editState = editState,
        title = stringResource(id = R.string.progress_widget___title),
        actionIcon = painterResource(id = R.drawable.ic_round_settings_24),
        actionContentDescription = stringResource(id = R.string.cd_settings),
        onAction = { fm, _ -> MenuBottomSheetFragment.createForMenu(ProgressControlsSettingsMenu(role = ProgressControlsRole.ForNormal)).show(fm) },
        modifier = modifier,
    ) {
        ProgressControlsInternal(state = state, gcodeState = gcodeState)
    }
}

interface ProgressControlsState {
    val active: Boolean
    val current: ProgressControlsViewModel.ViewState
}

@Composable
fun rememberProgressControlsState(active: Boolean): ProgressControlsState {
    val vmFactory = ProgressControlsViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = ProgressControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory,
        viewModelStoreOwner = OctoAppTheme.octoActivity as ViewModelStoreOwner
    )

    val state = vm.state.collectAsStateWhileActive(active = active, key = "progress", initial = vm.stateCache)
    return remember(state) {
        object : ProgressControlsState {
            override val active = active
            override val current get() = state.value
        }
    }
}

//region Internals
private val LocalSmallFont = compositionLocalOf { false }

@Composable
private fun ProgressControlsInternal(
    state: () -> ProgressControlsState,
    gcodeState: () -> GcodeControlsState,
) {
    val horizontalSpacing = OctoAppTheme.dimens.margin12

    ProgressBar(
        completion = { state().current.currentMessage?.progress?.completion },
        flags = { state().current.currentMessage?.state?.flags }
    )

    val showThumbnail by remember(state) {
        derivedStateOf {
            state().current.settings.showThumbnail
        }
    }

    Row(
        modifier = Modifier.padding(
            top = OctoAppTheme.dimens.margin2,
            start = if (showThumbnail) 0.dp else OctoAppTheme.dimens.margin2,
            end = OctoAppTheme.dimens.margin2
        )
    ) {
        //region Preview
        if (showThumbnail) {
            PreviewImage(horizontalSpacing = horizontalSpacing) {
                state().current.resolvedFile?.thumbnail
            }
        }
        //endregion
        //region Values
        ProgressValues(
            mainAxisSpacing = horizontalSpacing,
            state = state,
            gcodeState = gcodeState,
            role = ProgressControlsRole.ForNormal,
        )
        //endregion
    }
}

@Composable
private fun PreviewImage(
    horizontalSpacing: Dp,
    url: () -> String?,
) {
    var showDialog by remember { mutableStateOf(false) }
    OctoPrintPicassoImage(
        modifier = Modifier
            .padding(end = horizontalSpacing)
            .clip(MaterialTheme.shapes.medium)
            .clickable(enabled = url() != null) { showDialog = true }
            .background(MaterialTheme.colors.surface)
            .fillMaxWidth(fraction = 0.25f)
            .aspectRatio(ratio = 1f),
        url = url(),
    )

    if (showDialog) {
        AlertDialog(
            onDismissRequest = { showDialog = false },
            text = {
                OctoPrintPicassoImage(
                    modifier = Modifier
                        .fillMaxWidth()
                        .aspectRatio(1f)
                        .widthIn(max = 600.dp),
                    contentScale = ContentScale.FillWidth,
                    url = url(),
                )
            },
            confirmButton = {
                LinkButton(text = stringResource(id = android.R.string.ok), color = OctoAppTheme.colors.accent) {
                    showDialog = false
                }
            }
        )
    }
}


@Composable
fun ProgressValues(
    modifier: Modifier = Modifier,
    mainAxisSpacing: Dp = OctoAppTheme.dimens.margin12,
    state: () -> ProgressControlsState,
    gcodeState: () -> GcodeControlsState,
    role: ProgressControlsRole,
) {
    val smallFont by remember(state) {
        derivedStateOf { state().current.settings.fontSize == ProgressWidgetSettings.FontSize.Small }
    }

    CompositionLocalProvider(LocalSmallFont provides smallFont) {
        FlowRow(
            modifier = modifier.fillMaxWidth(),
            mainAxisSpacing = mainAxisSpacing,
            lastLineMainAxisAlignment = FlowMainAxisAlignment.Start,
            crossAxisAlignment = FlowCrossAxisAlignment.Start,
            crossAxisSpacing = if (LocalSmallFont.current) OctoAppTheme.dimens.margin01 else OctoAppTheme.dimens.margin1,
        ) {
            TimeInformation(viewStateFactory = { state().current }, role = role)
            GcodeInformation(viewStateFactory = { state().current }, gcodeStateFactory = gcodeState, role = role)
            AdditionalInformation(viewStateFactory = { state().current }, role = role)
        }
    }
}

@Composable
private fun TimeInformation(
    viewStateFactory: () -> ProgressControlsViewModel.ViewState,
    role: ProgressControlsRole,
) {
    val viewState = viewStateFactory()
    val settings = viewState.settingsForRole(role)
    val progress = viewState.currentMessage?.progress?.completion
    val left by viewState.currentMessage?.progress?.printTimeLeft.rememberAsFormattedDuration()
    val used by viewState.currentMessage?.progress?.printTime.rememberAsFormattedDuration()
    val eta by viewState.currentMessage?.progress?.printTimeLeft.rememberAsFormattedEta(
        useCompactDate = settings.etaStyle == ProgressWidgetSettings.EtaStyle.Compact,
        allowRelative = false,
    )
    Value(
        label = stringResource(R.string.progress_widget___title),
        value = stringResource(id = R.string.x_percent, progress ?: 0f),
        largeValue = false,
        visible = role == ProgressControlsRole.ForWebcamFullscreen
    )
    Value(
        label = stringResource(R.string.progress_widget___time_spent),
        value = used,
        largeValue = false,
        visible = settings.showUsedTime
    )
    Value(
        label = stringResource(R.string.progress_widget___time_left),
        value = left,
        largeValue = false,
        visible = settings.showLeftTime
    )
    Value(
        label = stringResource(R.string.progress_widget___eta),
        value = eta,
        largeValue = false,
        visible = settings.etaStyle != ProgressWidgetSettings.EtaStyle.None,
        icon = viewState.currentMessage?.progress?.printTimeLeftOrigin?.asPrintTimeLeftImagePainter(LocalContext.current),
        iconTint = viewState.currentMessage?.progress?.printTimeLeftOrigin?.asPrintTimeLeftOriginColor()?.let { colorResource(id = it) },
    )
}

@Composable
private fun GcodeInformation(
    viewStateFactory: () -> ProgressControlsViewModel.ViewState,
    gcodeStateFactory: () -> GcodeControlsState,
    role: ProgressControlsRole,
) {
    val viewState = viewStateFactory()
    val gcodeState = gcodeStateFactory()
    val settings = viewState.settingsForRole(role)
    val hasGcodeFeatures by BillingManager.isFeatureEnabledFlow(FEATURE_GCODE_PREVIEW).collectAsState(initial = false)

    if (hasGcodeFeatures) {
        val gcodeViewState = gcodeState.current
        val unavailable = stringResource(R.string.progress_widget___gcode_unavailable)
        val largeFile = stringResource(R.string.progress_widget___gcode_large_file)
        val loading = stringResource(R.string.progress_widget___gcode_loading)
        val (layer, zHeight) = when (gcodeViewState) {
            is GcodePreviewControlsViewModel.ViewState.DataReady -> gcodeViewState.state.value.renderContext?.let { context ->
                val layerHeight = DecimalFormat("0.0#").format(context.layerZHeight)
                val progress = context.layerProgress * 100
                val layer = String.format(
                    "%d/%d (%.0f%%)",
                    context.layerNumberDisplay(context.layerNumber),
                    context.layerCountDisplay(context.layerCount),
                    progress
                )
                val zHeight = stringResource(R.string.x_mm, layerHeight)
                layer to zHeight
            } ?: (unavailable to unavailable)
            is GcodePreviewControlsViewModel.ViewState.Error -> unavailable to unavailable
            is GcodePreviewControlsViewModel.ViewState.FeatureDisabled -> unavailable to unavailable
            is GcodePreviewControlsViewModel.ViewState.LargeFileDownloadRequired -> largeFile to largeFile
            is GcodePreviewControlsViewModel.ViewState.Loading -> loading to loading
            is GcodePreviewControlsViewModel.ViewState.PrintingFromSdCard -> unavailable to unavailable
        }

        Value(
            label = stringResource(R.string.progress_widget___layer),
            value = layer,
            largeValue = false,
            visible = settings.showLayer
        )

        Value(
            label = stringResource(R.string.progress_widget___z_height),
            value = zHeight,
            largeValue = false,
            visible = settings.showZHeight
        )
    }
}

@Composable
private fun AdditionalInformation(
    viewStateFactory: () -> ProgressControlsViewModel.ViewState,
    role: ProgressControlsRole,
) {
    val viewState = viewStateFactory()
    val settings = viewState.settingsForRole(role)

    Value(
        label = stringResource(R.string.progress_widget___print_name),
        value = viewState.currentMessage?.job?.file?.display ?: "",
        largeValue = true,
        visible = settings.printNameStyle != ProgressWidgetSettings.PrintNameStyle.None,
        maxLines = if (settings.printNameStyle == ProgressWidgetSettings.PrintNameStyle.Full) 5 else 1
    )
    Value(
        label = stringResource(R.string.progress_widget___printer_message),
        value = viewState.companionMessage?.m117?.takeIf { it.isNotBlank() } ?: stringResource(R.string.progress_widget___printer_message_no_message),
        largeValue = true,
        visible = settings.showPrinterMessage,
        maxLines = 2,
    )
}

@Composable
private fun Value(
    label: String,
    value: String,
    icon: Painter? = null,
    iconTint: Color? = null,
    largeValue: Boolean,
    visible: Boolean,
    maxLines: Int = 1,
) {
    if (visible) {
        val smallFont = LocalSmallFont.current
        Column(modifier = if (largeValue) Modifier.fillMaxWidth() else Modifier) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(
                    text = label,
                    style = if (smallFont) OctoAppTheme.typography.labelSmall else OctoAppTheme.typography.label,
                )
                icon?.let {
                    Icon(
                        painter = it,
                        contentDescription = null,
                        tint = iconTint ?: Color.Unspecified,
                        modifier = Modifier
                            .size(14.dp)
                            .padding(start = OctoAppTheme.dimens.margin0)
                    )
                }
            }

            Text(
                text = value,
                style = if (smallFont) OctoAppTheme.typography.label else OctoAppTheme.typography.base,
                maxLines = maxLines,
                overflow = TextOverflow.Ellipsis,
            )
        }
    }
}

@Composable
private fun ProgressBar(
    completion: () -> Float?,
    flags: () -> PrinterState.Flags?
) = Box(
    modifier = Modifier
        .clip(MaterialTheme.shapes.small)
        .background(MaterialTheme.colors.surface)
        .height(IntrinsicSize.Min)
) {
    val roundedCompletion by remember(completion) {
        derivedStateOf {
            completion()?.let { (it * 100).roundToInt() / 100f }
        }
    }

    val animatedCompletion by animateFloatAsState(
        targetValue = (roundedCompletion ?: 0f) / 100f,
        animationSpec = tween(durationMillis = UPDATE_INTERVAL.toInt())
    )

    val color = OctoAppTheme.colors.activeColorScheme
    val textColorInside = OctoAppTheme.colors.textColoredBackground
    val textColorOutside = OctoAppTheme.colors.darkText
    val textInside = { animatedCompletion < 0.7 }
    val textColor by remember {
        derivedStateOf {
            if (textInside()) textColorOutside else textColorInside
        }
    }
    val textAlign by remember {
        derivedStateOf {
            if (textInside()) TextAlign.Start else TextAlign.End
        }
    }
    var fullWidth by remember { mutableStateOf(0) }
    val textWidth = 150.dp

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .onGloballyPositioned { fullWidth = it.size.width }
            .drawBehind {
                drawRect(
                    color = color,
                    size = Size(width = size.width * animatedCompletion, height = size.height)
                )
            }
    ) {
        ProgressText(
            color = textColor,
            completion = { roundedCompletion },
            flags = flags,
            textAlign = textAlign,
            modifier = Modifier
                .width(textWidth)
                .padding(horizontal = OctoAppTheme.dimens.margin01, vertical = OctoAppTheme.dimens.margin1)
                .offset {
                    val barEnd = (fullWidth * animatedCompletion).roundToInt()
                    if (textAlign == TextAlign.Start) {
                        IntOffset(barEnd, 0)
                    } else {
                        IntOffset(barEnd - textWidth.roundToPx(), 0)
                    }
                }
        )
    }
}

@Composable
private fun ProgressText(
    color: Color,
    flags: () -> PrinterState.Flags?,
    textAlign: TextAlign,
    completion: () -> Float?,
    modifier: Modifier,
) {
    val context = LocalContext.current
    val percentRounded by remember(completion) {
        derivedStateOf { completion()?.roundToInt() }
    }
    val text by remember(completion, flags) {
        derivedStateOf {
            val f = flags()
            when {
                f?.pausing == true -> context.getString(R.string.pausing)
                f?.paused == true -> context.getString(R.string.paused)
                f?.cancelling == true -> context.getString(R.string.cancelling)
                percentRounded != null -> context.getString(R.string.x_percent_int, percentRounded)
                else -> ""
            }
        }
    }

    Text(
        style = OctoAppTheme.typography.sectionHeader,
        color = color,
        text = text,
        maxLines = 1,
        textAlign = textAlign,
        overflow = TextOverflow.Visible,
        modifier = modifier,
    )
}

//endregion

//region Preview
@Composable
@Preview(backgroundColor = 0xFFFFFF)
private fun PreviewInitial() = OctoAppThemeForPreview {
    ProgressControlsInternal(
        state = {
            object : ProgressControlsState {
                override val active = true
                override val current = ProgressControlsViewModel.ViewState(
                    settings = ProgressWidgetSettings(),
                    fullscreenSettings = ProgressWidgetSettings(),
                    companionMessage = null,
                    currentMessage = Message.Current(
                        isHistoryMessage = false,
                        state = PrinterState.State(
                            text = "",
                            flags = PrinterState.Flags(
                                printing = true,
                                paused = false,
                                sdReady = true,
                                ready = true,
                                closedOrError = false,
                                error = false,
                                cancelling = false,
                                pausing = false,
                                operational = false,
                            )
                        ),
                        job = null,
                        progress = ProgressInformation(
                            completion = 0f,
                            filepos = 0,
                            printTimeLeft = 23234230,
                            printTime = 2323333,
                            printTimeLeftOrigin = ORIGIN_GENIUS,
                        ),
                        logs = emptyList(),
                        offsets = emptyMap(),
                        originHash = 0,
                        serverTime = 0,
                        temps = emptyList(),
                    ),
                    resolvedFile = null
                )
            }
        },
        gcodeState = {
            object : GcodeControlsState {
                override val active = true
                override val current = GcodePreviewControlsViewModel.ViewState.Loading(0f)
                override fun retry() = Unit
                override fun allowLargeFileDownloads() = Unit
                override fun openFullscreen() = Unit
            }
        }
    )
}

@Composable
@Preview(backgroundColor = 0xFFFFFF)
private fun PreviewEarly() = OctoAppThemeForPreview {
    ProgressControlsInternal(
        state = {
            object : ProgressControlsState {
                override val active = true
                override val current = ProgressControlsViewModel.ViewState(
                    settings = ProgressWidgetSettings(),
                    fullscreenSettings = ProgressWidgetSettings(),
                    companionMessage = null,
                    currentMessage = Message.Current(
                        isHistoryMessage = false,
                        state = PrinterState.State(
                            text = "",
                            flags = PrinterState.Flags(
                                printing = true,
                                paused = false,
                                sdReady = true,
                                ready = true,
                                closedOrError = false,
                                error = false,
                                cancelling = false,
                                pausing = false,
                                operational = false,
                            )
                        ),
                        job = null,
                        progress = ProgressInformation(
                            completion = 10f,
                            filepos = 0,
                            printTimeLeft = 23234230,
                            printTime = 2323333,
                            printTimeLeftOrigin = ORIGIN_MIXED_ANALYSIS,
                        ),
                        logs = emptyList(),
                        offsets = emptyMap(),
                        originHash = 0,
                        serverTime = 0,
                        temps = emptyList(),
                    )
                )
            }
        },
        gcodeState = {
            object : GcodeControlsState {
                override val active = true
                override val current = GcodePreviewControlsViewModel.ViewState.Loading(0f)
                override fun retry() = Unit
                override fun allowLargeFileDownloads() = Unit
                override fun openFullscreen() = Unit
            }
        }
    )
}

@Composable
@Preview(backgroundColor = 0xFFFFFF)
private fun PreviewLate() = OctoAppThemeForPreview {
    ProgressControlsInternal(
        state = {
            object : ProgressControlsState {
                override val active = true
                override val current = ProgressControlsViewModel.ViewState(
                    settings = ProgressWidgetSettings(),
                    fullscreenSettings = ProgressWidgetSettings(),

                    companionMessage = null,
                    currentMessage = Message.Current(
                        isHistoryMessage = false,
                        state = PrinterState.State(
                            text = "",
                            flags = PrinterState.Flags(
                                printing = true,
                                paused = false,
                                sdReady = true,
                                ready = true,
                                closedOrError = false,
                                error = false,
                                cancelling = false,
                                pausing = false,
                                operational = false,
                            )
                        ),
                        job = null,
                        progress = ProgressInformation(
                            completion = 81f,
                            filepos = 0,
                            printTimeLeft = 23234230,
                            printTime = 2323333,
                            printTimeLeftOrigin = ORIGIN_AVERAGE,
                        ),
                        logs = emptyList(),
                        offsets = emptyMap(),
                        originHash = 0,
                        serverTime = 0,
                        temps = emptyList(),
                    ),
                    resolvedFile = null
                )
            }
        },
        gcodeState = {
            object : GcodeControlsState {
                override val active = true
                override val current = GcodePreviewControlsViewModel.ViewState.Loading(0f)
                override fun retry() = Unit
                override fun allowLargeFileDownloads() = Unit
                override fun openFullscreen() = Unit
            }
        }
    )
}

@Composable
@Preview(backgroundColor = 0xFFFFFF)
private fun PreviewPaused() = OctoAppThemeForPreview {
    ProgressControlsInternal(
        state = {
            object : ProgressControlsState {
                override val active = true
                override val current = ProgressControlsViewModel.ViewState(
                    settings = ProgressWidgetSettings(),
                    fullscreenSettings = ProgressWidgetSettings(),

                    companionMessage = null,
                    currentMessage = Message.Current(
                        isHistoryMessage = false,
                        state = PrinterState.State(
                            text = "",
                            flags = PrinterState.Flags(
                                printing = true,
                                paused = true,
                                sdReady = true,
                                ready = true,
                                closedOrError = false,
                                error = false,
                                cancelling = false,
                                pausing = false,
                                operational = false,
                            )
                        ),
                        job = null,
                        progress = ProgressInformation(
                            completion = 40f,
                            filepos = 0,
                            printTimeLeft = 23234230,
                            printTime = 2323333,
                            printTimeLeftOrigin = null,
                        ),
                        logs = emptyList(),
                        offsets = emptyMap(),
                        originHash = 0,
                        serverTime = 0,
                        temps = emptyList(),
                    ),
                    resolvedFile = null
                )
            }
        },
        gcodeState = {
            object : GcodeControlsState {
                override val active = true
                override val current = GcodePreviewControlsViewModel.ViewState.Loading(0f)
                override fun retry() = Unit
                override fun allowLargeFileDownloads() = Unit
                override fun openFullscreen() = Unit
            }
        }
    )

}
//endregion