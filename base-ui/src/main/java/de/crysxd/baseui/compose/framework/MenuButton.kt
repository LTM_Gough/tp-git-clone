package de.crysxd.baseui.compose.framework

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.menu.base.MenuItemStyle

@Composable
@Deprecated("Do not use....Use MenuItem")
fun MenuButton(
    icon: Painter,
    badge: Int = 0,
    pinned: Boolean = false,
    text: String,
    style: MenuItemStyle = MenuItemStyle.Neutral,
    rightDetail: RightDetail = RightDetail.Nothing,
    onClick: () -> Unit,
) = Row(
    verticalAlignment = Alignment.CenterVertically,
    modifier = Modifier
        .fillMaxWidth()
        .padding(top = OctoAppTheme.dimens.margin01)
        .clip(MaterialTheme.shapes.small)
        .background(colorResource(style.backgroundColor))
        .clickable(
            role = Role.Button,
            onClick = onClick,
            onClickLabel = text,
            indication = rememberRipple(color = Color.White.copy(alpha = 0.66f)),
            interactionSource = remember { MutableInteractionSource() }
        )
) {
    MenuButtonIcon(
        icon = icon,
        badge = badge,
        pinned = pinned,
        style = style
    )
    Text(
        text = text,
        style = OctoAppTheme.typography.buttonSmall,
        color = OctoAppTheme.colors.darkText,
        modifier = Modifier
            .weight(1f)
            .padding(start = OctoAppTheme.dimens.margin1)
    )
}

@Composable
private fun MenuButtonIcon(
    icon: Painter,
    badge: Int,
    pinned: Boolean,
    style: MenuItemStyle,
) = Box(
    contentAlignment = Alignment.BottomEnd,
) {
    Icon(
        painter = icon,
        contentDescription = null,
        tint = colorResource(id = style.highlightColor),
        modifier = Modifier
            .clip(MaterialTheme.shapes.small)
            .background(colorResource(style.backgroundColor))
            .padding(OctoAppTheme.dimens.margin01)
    )

    when {
        badge > 0 -> BadgeBox(background = OctoAppTheme.colors.colorError) {
            Text(
                text = badge.toString(),
                style = OctoAppTheme.typography.labelSmall.copy(fontSize = it),
                color = OctoAppTheme.colors.textColoredBackground,
            )
        }

        pinned -> BadgeBox(background = OctoAppTheme.colors.blackTranslucent2) {
            Icon(
                painter = painterResource(id = R.drawable.ic_round_push_pin_10),
                contentDescription = null,
                tint = OctoAppTheme.colors.textColoredBackground,
            )
        }
    }
}

@Composable
fun BadgeBox(background: Color, content: @Composable (TextUnit) -> Unit) = with(LocalDensity.current) {
    val fontSize = 10.sp
    val height = fontSize.toDp() + 4.dp
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .offset(x = 2.dp, y = (-2).dp)
            .height(height)
            .widthIn(min = height)
            .clip(MaterialTheme.shapes.small)
            .background(background),
    ) {
        content(fontSize)
    }
}

sealed class RightDetail {
    object Nothing : RightDetail()
}

//region Previews
@Composable
@Preview
@Suppress("DEPRECATION")
private fun PreviewNormal() = OctoAppThemeForPreview {
    MenuButton(
        icon = painterResource(id = R.drawable.ic_round_favorite_24),
        text = "Support OctoApp",
        style = MenuItemStyle.Support,
        onClick = {}
    )
}

@Composable
@Preview
@Suppress("DEPRECATION")
private fun PreviewBadge() = OctoAppThemeForPreview {
    MenuButton(
        icon = painterResource(id = R.drawable.ic_octoprint_24px),
        text = "OctoPrint",
        style = MenuItemStyle.OctoPrint,
        badge = 1,
        onClick = {}
    )
}

@Composable
@Preview
@Suppress("DEPRECATION")
private fun PreviewPinned() = OctoAppThemeForPreview {
    MenuButton(
        icon = painterResource(id = R.drawable.ic_round_open_in_browser_24),
        text = "Open OctoPrint",
        pinned = true,
        style = MenuItemStyle.OctoPrint,
        onClick = {}
    )
}
//endregion