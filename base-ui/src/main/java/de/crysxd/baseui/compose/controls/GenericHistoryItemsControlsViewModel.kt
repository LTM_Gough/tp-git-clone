package de.crysxd.baseui.compose.controls

import androidx.lifecycle.ViewModel
import de.crysxd.baseui.OctoActivity
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.OctoPrintProvider
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.first

abstract class GenericHistoryItemsControlsViewModel<T>(
    private val instanceId: String,
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPreferences: OctoPreferences,
    octoPrintRepository: OctoPrintRepository,
) : ViewModel() {

    abstract val items: Flow<List<T>>

    abstract val OctoPreferences.isAlwaysAllowedToShow: Boolean
    abstract val OctoPrintInstanceInformationV3.isNeverAllowedToShow: Boolean

    val visible = combine(
        octoPrintProvider.passiveCurrentMessageFlow(tag = "execute-gcode-controls", instanceId = instanceId),
        octoPreferences.updatedFlow2,
        octoPrintRepository.instanceInformationFlow(instanceId = instanceId)
    ) { current, prefernces, instance ->
        // Widget is visible if we are not printing (printing, pausing, paused, cancelling) or we are paused or we are allowed to always show
        val printing = current.state?.flags?.isPrinting() ?: true
        val paused = current.state?.flags?.paused ?: false
        val alwaysShown = octoPreferences.isAlwaysAllowedToShow
        val neverShown = instance?.isNeverAllowedToShow == true
        !neverShown && (!printing || paused || alwaysShown)
    }.distinctUntilChanged()


    suspend fun needsConfirmation() = octoPrintProvider.passiveCurrentMessageFlow(tag = "execute-gcode-controls-check", instanceId = instanceId)
        .first().state?.flags?.isPrinting() ?: true

    protected fun postMessage(message: OctoActivity.Message) = when (message) {
        is OctoActivity.Message.DialogMessage -> OctoActivity.instance?.showDialog(message)
        is OctoActivity.Message.SnackbarMessage -> OctoActivity.instance?.showSnackbar(message)
    }
}