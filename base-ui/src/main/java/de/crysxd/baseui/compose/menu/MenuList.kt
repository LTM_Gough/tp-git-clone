package de.crysxd.baseui.compose.menu

import android.content.Context
import android.os.Parcel
import androidx.compose.animation.graphics.ExperimentalAnimationGraphicsApi
import androidx.compose.animation.graphics.res.animatedVectorResource
import androidx.compose.animation.graphics.res.rememberAnimatedVectorPainter
import androidx.compose.animation.graphics.vector.AnimatedImageVector
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.SmallPrimaryButton
import de.crysxd.baseui.compose.framework.asAnnotatedString
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.menu.base.Menu
import de.crysxd.baseui.menu.base.MenuHost
import de.crysxd.baseui.menu.base.MenuItem
import de.crysxd.baseui.menu.base.ToggleMenuItem
import de.crysxd.baseui.menu.settings.AutomaticLightsSettingsMenuItem
import de.crysxd.baseui.menu.settings.HelpMenuItem
import de.crysxd.baseui.menu.settings.KeepScreenOnDuringPrintMenuItem
import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.ext.toHtml
import kotlinx.coroutines.runBlocking

@Composable
@OptIn(ExperimentalAnimationGraphicsApi::class)
fun MenuList(
    menu: Menu,
    menuHost: MenuHost,
    modifier: Modifier = Modifier,
    menuId: MenuId,
    state: MenuState = rememberMenuState(menu = menu),
) = Box(Modifier.height(IntrinsicSize.Min)) {
    val context = LocalContext.current

    Column(modifier) {
        //region Header
        if (state.title != null || state.subtitle != null) {
            Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin3))
        }

        if (state.title != null) {
            Text(
                text = state.title.asAnnotatedString(),
                style = OctoAppTheme.typography.title,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center,
            )
        }

        if (state.subtitle != null) {
            Text(
                text = state.subtitle.asAnnotatedString(),
                style = OctoAppTheme.typography.subtitle,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = OctoAppTheme.dimens.margin01),
                textAlign = TextAlign.Center,
            )
        }

        if (state.title != null || state.subtitle != null && state.items.isNotEmpty()) {
            Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin2))
        }
        //endregion
        //region Items
        Column(verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01)) {
            state.items.forEach {
                MenuItem(
                    item = it,
                    host = menuHost,
                    menuId = menuId,
                )
            }
        }
        //endregion
        //region Empty
        if (state.items.isEmpty()) {
            menu.getEmptyStateIcon().takeIf { it != 0 }?.let {
                val vector = AnimatedImageVector.animatedVectorResource(it)
                var atEnd by remember { mutableStateOf(true) }
                LaunchedEffect(Unit) { atEnd = false }
                Image(
                    painter = rememberAnimatedVectorPainter(animatedImageVector = vector, atEnd = atEnd),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = OctoAppTheme.dimens.margin2, vertical = OctoAppTheme.dimens.margin4)
                )
            }

            menu.getEmptyStateActionText(context)?.let { text ->
                menu.getEmptyStateAction(context)?.let { action ->
                    SmallPrimaryButton(
                        text = text,
                        onClick = action,
                        modifier = Modifier
                            .padding(bottom = OctoAppTheme.dimens.margin2)
                            .fillMaxWidth(),
                    )
                }
            }
        }
        //endregion
        //region Footer
        if (state.bottomText != null) {
            val handler = menu.getBottomMovementMethod(menuHost)?.asUriHandler(context) ?: LocalUriHandler.current
            CompositionLocalProvider(LocalUriHandler provides handler) {
                Text(
                    text = state.bottomText.asAnnotatedString(),
                    style = OctoAppTheme.typography.base,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = OctoAppTheme.dimens.margin2),
                    textAlign = TextAlign.Center,
                )
            }
        }
        //endregion
    }
}

//region State
@Composable
fun rememberMenuState(menu: Menu): MenuState {
    val context = LocalContext.current
    val initialState = if (OctoAppTheme.isPreview) {
        runBlocking {
            MenuState(
                title = menu.getTitle(context),
                subtitle = menu.getSubtitle(context),
                items = menu.getMenuItem(),
                bottomText = menu.getBottomText(context),
            )
        }
    } else {
        MenuState(
            title = null,
            subtitle = null,
            items = emptyList(),
            bottomText = null,
        )
    }

    var state by remember { mutableStateOf(initialState) }

    LaunchedEffect(menu) {
        val items = menu.getMenuItem()
        val title = menu.getTitle(context)
        val subtitle = if (items.isEmpty()) menu.getEmptyStateSubtitle(context) else menu.getSubtitle(context)
        state = state.copy(title = title, subtitle = subtitle, items = items)
    }

    return state
}

data class MenuState(
    val title: CharSequence?,
    val subtitle: CharSequence?,
    val items: List<MenuItem>,
    val bottomText: CharSequence?,
)
//endregion

//region Previews
@Composable
@Preview(group = "Menus")
private fun PreviewMenuListFull() = OctoAppThemeForPreview {
    val items = listOf(
        HelpMenuItem(),
        AutomaticLightsSettingsMenuItem(),
        object : ToggleMenuItem by KeepScreenOnDuringPrintMenuItem() {
            override val isChecked = false
        }
    )

    val menu = object : Menu {
        override suspend fun getMenuItem() = items
        override suspend fun getTitle(context: Context) = "Test Menu"
        override suspend fun getSubtitle(context: Context) = "This is only a test!"
        override fun getBottomText(context: Context) = "This is a <b>bottom</b> text with a <a href=\"\">link</a>!".toHtml()
        override fun describeContents() = 0
        override fun writeToParcel(p0: Parcel, p1: Int) = Unit
    }

    MenuList(
        menu = menu,
        menuHost = PreviewHost,
        menuId = MenuId.MainMenu,
    )
}

@Composable
@Preview(group = "Menus")
private fun PreviewMenuListMinimal() = OctoAppThemeForPreview {
    val items = listOf(
        HelpMenuItem(),
        AutomaticLightsSettingsMenuItem(),
        object : ToggleMenuItem by KeepScreenOnDuringPrintMenuItem() {
            override val isChecked = false
        }
    )

    val menu = object : Menu {
        override suspend fun getMenuItem() = items
        override fun describeContents() = 0
        override fun writeToParcel(p0: Parcel, p1: Int) = Unit
    }

    MenuList(
        menu = menu,
        menuHost = PreviewHost,
        menuId = MenuId.MainMenu,
    )
}

@Composable
@Preview(group = "Menus")
private fun PreviewMenuListEmpty() = OctoAppThemeForPreview {
    val menu = object : Menu {
        override suspend fun getMenuItem() = emptyList<MenuItem>()
        override suspend fun getTitle(context: Context) = context.getString(R.string.power_menu___title_neutral)
        override fun getEmptyStateIcon() = R.drawable.octo_power_devices
        override fun getEmptyStateActionText(context: Context) = context.getString(R.string.power_menu___empty_state_action)
        override fun getEmptyStateAction(context: Context) = null
        override fun getEmptyStateSubtitle(context: Context) = context.getString(R.string.power_menu___empty_state_subtitle)
        override fun describeContents() = 0
        override fun writeToParcel(p0: Parcel, p1: Int) = Unit
    }

    MenuList(
        menu = menu,
        menuHost = PreviewHost,
        menuId = MenuId.MainMenu,
    )
}

private val PreviewHost by lazy {
    object : MenuHost {
        override fun requireContext() = throw NotImplementedError()
        override fun pushMenu(subMenu: Menu) = throw NotImplementedError()
        override fun closeMenu() = throw NotImplementedError()
        override fun getNavController() = throw NotImplementedError()
        override fun getMenuActivity() = throw NotImplementedError()
        override fun getMenuFragmentManager() = throw NotImplementedError()
        override fun getHostFragment() = throw NotImplementedError()
        override fun reloadMenu() = throw NotImplementedError()
        override fun isCheckBoxChecked() = throw NotImplementedError()
        override fun suppressSuccessAnimationForNextAction() = throw NotImplementedError()
        override fun consumeSuccessAnimationForNextActionSuppressed() = throw NotImplementedError()
    }
}
//endregion
