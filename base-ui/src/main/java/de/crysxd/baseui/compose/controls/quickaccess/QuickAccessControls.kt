package de.crysxd.baseui.compose.controls.quickaccess

import android.net.Uri
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.spring
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.animation.with
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.navigation.fragment.findNavController
import de.crysxd.baseui.R
import de.crysxd.baseui.common.controls.ControlsFragment
import de.crysxd.baseui.compose.controls.ControlsScaffold
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.framework.AnnouncementController
import de.crysxd.baseui.compose.framework.AnnouncementId
import de.crysxd.baseui.compose.framework.LargeAnnouncement
import de.crysxd.baseui.compose.framework.LocalOctoPrint
import de.crysxd.baseui.compose.framework.collectAsStateWhileActive
import de.crysxd.baseui.compose.menu.MenuList
import de.crysxd.baseui.compose.menu.MenuState
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.menu.base.Menu
import de.crysxd.baseui.menu.base.MenuBottomSheetFragment
import de.crysxd.baseui.menu.base.MenuHost
import de.crysxd.baseui.menu.base.MenuItemLibrary
import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.engine.models.printer.PrinterState
import kotlinx.coroutines.flow.map
import kotlinx.parcelize.Parcelize

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun QuickAccessControls(
    editState: EditState,
    state: QuickAccessState,
) = AnimatedVisibility(
    visible = state.menuState.items.isNotEmpty() || editState.editing,
    enter = fadeIn() + expandVertically(),
    exit = fadeOut() + shrinkVertically()
) {
    ControlsScaffold(
        title = stringResource(id = R.string.widget_quick_access),
        editState = editState,
    ) {
        //region Tutorial
        AnnouncementController(announcementId = AnnouncementId.Default("customization_tutorial_quick_access")) {
            val link = stringResource(id = R.string.quick_access_tutorial_learn_more_link)
            LargeAnnouncement(
                title = stringResource(id = R.string.widget_quick_access___tutorial_title),
                text = stringResource(id = R.string.widget_quick_access___tutorial_detail),
                learnMoreButton = stringResource(id = R.string.widget_quick_access___tutorial_learn_more),
                onLearnMore = { Uri.parse(link).open() },
                modifier = Modifier.padding(bottom = OctoAppTheme.dimens.margin2)
            )
        }
        //endregion
        //region Menu
        AnimatedContent(
            targetState = state,
            transitionSpec = { fadeIn(spring()) with fadeOut(spring()) }
        ) { s ->
            MenuList(
                menu = remember { QuickAccessMenuDummy() },
                state = s.menuState,
                menuHost = s.menuHost,
                modifier = Modifier.animateContentSize(),
                menuId = s.menuId,
            )
        }
        //endregion
    }
}

data class QuickAccessState(
    val menuState: MenuState,
    val menuHost: MenuHost,
    val menuId: MenuId,
)

@Composable
fun rememberQuickAccessState(fragment: ControlsFragment, flags: PrinterState.Flags, active: Boolean): QuickAccessState {
    var reloadTrigger by remember { mutableStateOf(0) }
    val (menuId, destinationId) = when {
        flags.isPrinting() -> MenuId.PrintWorkspace to R.id.workspacePrint
        flags.isOperational() -> MenuId.PrePrintWorkspace to R.id.workspacePrePrint
        else -> MenuId.Other to 0
    }

    val library = remember { MenuItemLibrary() }
    val items by remember(menuId, reloadTrigger, LocalOctoPrint.current) {
        BaseInjector.get().pinnedMenuItemsRepository().observePinnedMenuItems(menuId)
            .map { items -> items.mapNotNull { library[it] }.sortedBy { it.order }.filter { it.isVisible(destinationId) } }
    }.collectAsStateWhileActive(active = active, key = "quickAccess", initial = emptyList())

    val state = MenuState(
        title = null,
        subtitle = null,
        items = items,
        bottomText = null,
    )

    return QuickAccessState(
        menuState = state,
        menuId = menuId,
        menuHost = remember {
            QuickAccessMenuHost(
                controlsFragment = fragment,
                onReload = { reloadTrigger++ }
            )
        }
    )
}

@Parcelize
private class QuickAccessMenuDummy : Menu {
    override suspend fun getMenuItem() = throw IllegalAccessException()
}

private class QuickAccessMenuHost(
    private val controlsFragment: ControlsFragment,
    private val onReload: () -> Unit,
) : MenuHost {
    var nextSuccessSuppressed = false

    override fun requireContext() = controlsFragment.requireContext()
    override fun pushMenu(subMenu: Menu) = MenuBottomSheetFragment.createForMenu(subMenu).show(controlsFragment.childFragmentManager)
    override fun closeMenu() = Unit
    override fun getNavController() = controlsFragment.findNavController()
    override fun getMenuActivity() = controlsFragment.requireActivity()
    override fun getMenuFragmentManager() = controlsFragment.childFragmentManager
    override fun getHostFragment() = controlsFragment
    override fun isCheckBoxChecked() = false
    override fun reloadMenu() = onReload()

    override fun suppressSuccessAnimationForNextAction() {
        nextSuccessSuppressed = true
    }

    override fun consumeSuccessAnimationForNextActionSuppressed(): Boolean {
        val res = nextSuccessSuppressed
        nextSuccessSuppressed = false
        return res
    }

}