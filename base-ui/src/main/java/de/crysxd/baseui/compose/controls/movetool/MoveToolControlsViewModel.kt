package de.crysxd.baseui.compose.controls.movetool

import android.content.Context
import android.text.InputType
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import de.crysxd.baseui.R
import de.crysxd.baseui.common.enter_value.EnterValueFragmentArgs
import de.crysxd.baseui.utils.NavigationResultMediator
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.HomePrintHeadUseCase
import de.crysxd.octoapp.base.usecase.JogPrintHeadUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class MoveToolControlsViewModel(
    private val instanceId: String,
    private val homePrintHeadUseCase: HomePrintHeadUseCase,
    private val jogPrintHeadUseCase: JogPrintHeadUseCase,
    private val octoPrintRepository: OctoPrintRepository,
) : ViewModel() {

    companion object {
        private const val DEFAULT_FEED_RATE = 8000
    }

    var jogResolution: Float = -1f

    fun homeXYAxis() = AppScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        homePrintHeadUseCase.execute(HomePrintHeadUseCase.Params(axis = HomePrintHeadUseCase.Axis.XY, instanceId = instanceId))
    }

    fun homeZAxis() = AppScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        homePrintHeadUseCase.execute(HomePrintHeadUseCase.Params(axis = HomePrintHeadUseCase.Axis.Z, instanceId = instanceId))
    }

    fun jog(x: Direction = Direction.None, y: Direction = Direction.None, z: Direction = Direction.None) =
        AppScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
            jogPrintHeadUseCase.execute(
                JogPrintHeadUseCase.Param(
                    xDistance = x.applyToDistance(jogResolution),
                    yDistance = y.applyToDistance(jogResolution),
                    zDistance = z.applyToDistance(jogResolution),
                    speedMmMin = getFeedRate(x, y, z),
                    instanceId = instanceId,
                )
            )
        }

    fun showSettings(context: Context, navController: NavController) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        val resultX = NavigationResultMediator.registerResultCallback<String?>()
        val resultY = NavigationResultMediator.registerResultCallback<String?>()
        val resultZ = NavigationResultMediator.registerResultCallback<String?>()

        navController.navigate(
            R.id.action_enter_value,
            EnterValueFragmentArgs(
                title = context.getString(R.string.move_settings),
                description = context.getString(R.string.move_settings_description),
                action = context.getString(R.string.update_settings),
                selectAll = true,

                hint = context.getString(R.string.x_feedrate_mm_min),
                value = getXFeedRate().toString(),
                inputType = InputType.TYPE_CLASS_NUMBER,
                resultId = resultX.first,

                hint2 = context.getString(R.string.y_feedrate_mm_min),
                value2 = getYFeedRate().toString(),
                inputType2 = InputType.TYPE_CLASS_NUMBER,
                resultId2 = resultY.first,

                hint3 = context.getString(R.string.z_feedrate_mm_min),
                value3 = getZFeedRate().toString(),
                inputType3 = InputType.TYPE_CLASS_NUMBER,
                resultId3 = resultZ.first,
            ).toBundle()
        )

        withContext(Dispatchers.Default) {
            suspend fun Pair<Int, LiveData<String?>>.readFeedRate() = second.asFlow().first()?.toIntOrNull() ?: DEFAULT_FEED_RATE
            setFeedRates(xFeedRate = resultX.readFeedRate(), yFeedRate = resultY.readFeedRate(), zFeedRate = resultZ.readFeedRate())
        }
    }

    @Suppress("UNUSED_PARAMETER")
    private fun getFeedRate(x: Direction = Direction.None, y: Direction = Direction.None, z: Direction = Direction.None) = when {
        x != Direction.None -> getXFeedRate()
        y != Direction.None -> getYFeedRate()
        z != Direction.None -> getZFeedRate()
        else -> DEFAULT_FEED_RATE
    }

    private fun getXFeedRate() = octoPrintRepository.get(instanceId)?.appSettings?.moveXFeedRate ?: DEFAULT_FEED_RATE
    private fun getYFeedRate() = octoPrintRepository.get(instanceId)?.appSettings?.moveYFeedRate ?: DEFAULT_FEED_RATE
    private fun getZFeedRate() = octoPrintRepository.get(instanceId)?.appSettings?.moveZFeedRate ?: DEFAULT_FEED_RATE

    private fun setFeedRates(xFeedRate: Int, yFeedRate: Int, zFeedRate: Int) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        Timber.i("xFeedRate=$xFeedRate yFeedRate=$yFeedRate zFeedRate=$zFeedRate")
        octoPrintRepository.updateAppSettings(instanceId) {
            it.copy(moveZFeedRate = zFeedRate, moveXFeedRate = xFeedRate, moveYFeedRate = yFeedRate)
        }
    }

    sealed class Direction(val multiplier: Float) {

        fun applyToDistance(distance: Float) = distance * multiplier

        object None : Direction(0f)
        object Positive : Direction(1f)
        object Negative : Direction(-1f)
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "MoveToolControlsViewModel@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = MoveToolControlsViewModel(
            instanceId = instanceId,
            octoPrintRepository = BaseInjector.get().octorPrintRepository(),
            homePrintHeadUseCase = BaseInjector.get().homePrintHeadUseCase(),
            jogPrintHeadUseCase = BaseInjector.get().jogPrintHeadUseCase(),
        ) as T
    }
}