package de.crysxd.baseui.compose.controls.gcodepreview

import androidx.annotation.DrawableRes
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.compose.framework.staticStateOf
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_GCODE_PREVIEW
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.data.repository.GcodeFileRepository
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.data.source.GcodeFileDataSource
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.gcode.parse.models.Move
import de.crysxd.octoapp.base.gcode.render.GcodeRenderContextFactory
import de.crysxd.octoapp.base.gcode.render.models.GcodeRenderContext
import de.crysxd.octoapp.base.gcode.render.models.RenderStyle
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.GenerateRenderStyleUseCase
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.job.JobInformation
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn
import timber.log.Timber

@OptIn(ExperimentalCoroutinesApi::class)
class GcodePreviewControlsViewModel(
    private val instanceId: String,
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPreferences: OctoPreferences,
    private val generateRenderStyleUseCase: GenerateRenderStyleUseCase,
    private val gcodeFileRepository: GcodeFileRepository,
) : ViewModel() {

    init {
        Timber.i("New instance: $instanceId")
    }

    //region Inputs
    private val retryFlow = MutableStateFlow(0L)
    private val triggerFlow = MutableStateFlow<Trigger>(Trigger.Live())
    private val largeDownloadsAllowedUntilFlow = MutableStateFlow(0L)

    //endregion
    //region Data sources
    // We already filter the current message here with minimal impact. If the trigger won't create a different outcome with the new message, no need to refresh.
    private val currentMessageFlow = octoPrintProvider.passiveCurrentMessageFlow(instanceId = instanceId, tag = "gcode-preview-1")
        .combine(triggerFlow) { x, y -> x to y }
        .distinctUntilChangedBy { (current, trigger) -> trigger.createResultId(current) }
        .map { (current, _) -> current }
    private val preferencesFlow = octoPreferences.updatedFlow.map { octoPreferences.gcodePreviewSettings }
    private val printerProfileFlow = octoPrintRepository.instanceInformationFlow(instanceId = instanceId).map { it?.activeProfile ?: PrinterProfile() }
    private val renderStyleFlow = octoPrintRepository.instanceInformationFlow(instanceId = instanceId).map { generateRenderStyleUseCase.execute(it) }
    private val featureEnabledFlow = BillingManager.isFeatureEnabledFlow(FEATURE_GCODE_PREVIEW)
        .distinctUntilChanged()
        .onEach { Timber.i("[GCD] Enabled: $it") }

    //endregion
    //region State computation
    private val activeFileFlow = octoPrintProvider.passiveCurrentMessageFlow(instanceId = instanceId, tag = "gcode-preview-2")
        .mapNotNull { it.job?.file }
        .distinctUntilChangedBy { it.path }
        .onEach { Timber.i("[GCD] Active file: ${it.path} (origin=${it.origin})") }

    private val gcodeFlow = combine(activeFileFlow, featureEnabledFlow, largeDownloadsAllowedUntilFlow) { file, enabled, largeDownloadsAllowedUntil ->
        Timber.i("[GCD] Trigger load: file=${file.path} enabled=$enabled largeDownloadsAllowedUntil=$largeDownloadsAllowedUntil")
        flow {
            when {
                !enabled -> throw FeatureDisabledException()
                file.origin == FileOrigin.SdCard -> throw PrintingFromSdCardException(file.asBasicFile())
                else -> {
                    emit(GcodeFileDataSource.LoadState.Loading(0f))
                    emitAll(gcodeFileRepository.loadFile(file.asBasicFile(), largeDownloadsAllowedUntil > System.currentTimeMillis()))
                }
            }
        }
    }.flatMapLatest { it }.onEach {
        if (it is GcodeFileDataSource.LoadState.Ready) {
            Timber.i("[GCD] Loaded Gcode: layerCount=${it.gcode.layers.size} maxX=${it.gcode.maxX} minX=${it.gcode.minX} maxY=${it.gcode.maxY} minY=${it.gcode.minY}")
        }
    }

    private val renderContextStateFlow: Flow<ViewState> =
        combine(gcodeFlow, preferencesFlow, currentMessageFlow, triggerFlow, activeFileFlow) { gcode, preferences, currentMessage, trigger, file ->
            when (gcode) {
                is GcodeFileDataSource.LoadState.Loading -> ViewState.Loading(gcode.progress)
                is GcodeFileDataSource.LoadState.FailedLargeFileDownloadRequired -> ViewState.LargeFileDownloadRequired(gcode.downloadSize)
                is GcodeFileDataSource.LoadState.Failed -> ViewState.Error(gcode.exception)
                is GcodeFileDataSource.LoadState.Ready -> {
                    val (factoryInstance, fromUser) = trigger.toRenderContextFactory(currentMessage)
                    ViewState.DataReady(
                        // This state is below replaced with a mutableStateOf to reduce refreshes in UI
                        state = staticStateOf(
                            EnrichedRenderContext(
                                renderContext = factoryInstance.extractMoves(
                                    gcode = gcode.gcode,
                                    includePreviousLayer = preferences.showPreviousLayer,
                                    includeRemainingCurrentLayer = preferences.showCurrentLayer,
                                ),
                                isTrackingPrintProgress = !fromUser,
                                exceedsPrintArea = null,
                                unsupportedGcode = null,
                            )
                        ),
                        file = file,
                        settings = preferences,
                    )
                }
            }
        }

    private val gcodeViewStateFlowFactory = {
        val renderContextState = mutableStateOf(EnrichedRenderContext())
        var previousState: ViewState? = null

        combine(renderContextStateFlow, renderStyleFlow, printerProfileFlow) { state, renderStyle, printerProfile ->
            when (state) {
                is ViewState.DataReady -> {
                    renderContextState.value = state.state.value.copy(
                        exceedsPrintArea = exceedsPrintArea(printerProfile, state.state.value.renderContext),
                        unsupportedGcode = containsUnsupportedGcode(state.state.value.renderContext),
                    )

                    val ps = previousState
                    if (ps !is ViewState.DataReady || ps.renderStyle == null || ps.printerProfile == null) {
                        previousState = state
                        state.copy(
                            renderStyle = renderStyle,
                            printerProfile = printerProfile,
                            state = renderContextState,
                        )
                    } else {
                        null
                    }
                }

                else -> state
            }
        }.retry(3) {
            Timber.e(it)
            it !is FeatureDisabledException && it !is PrintingFromSdCardException
        }.filterNotNull()
    }

    val state: Flow<ViewState> = combine(retryFlow, featureEnabledFlow, renderStyleFlow) { _, enabled, style ->
        when {
            !enabled -> flowOf(ViewState.FeatureDisabled(style.background))
            else -> gcodeViewStateFlowFactory()
        }
    }.flatMapLatest {
        it
    }.catch {
        Timber.e(it)
        emit(
            when (it) {
                is FeatureDisabledException -> ViewState.FeatureDisabled(renderStyleFlow.first().background)
                is PrintingFromSdCardException -> ViewState.PrintingFromSdCard(it.file)
                else -> ViewState.Error(it)
            }
        )
    }.flowOn(
        Dispatchers.Default
    ).onStart {
        Timber.i("[GCD] Starting shared Gcode flow")
    }.onCompletion {
        Timber.i("[GCD] Stopping shared Gcode flow")
    }.shareIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(),
        replay = 1
    ).onStart {
        Timber.i("[GCD] Starting Gcode flow")
    }.onCompletion {
        Timber.i("[GCD] Stopping Gcode flow")
    }

    //endregion
    //region Unsupported Gcode
    private fun containsUnsupportedGcode(context: GcodeRenderContext?) = listOfNotNull(
        context?.completedLayerPaths,
        context?.previousLayerPaths,
        context?.remainingLayerPaths
    ).any {
        it.any { path -> path.type == Move.Type.Unsupported && path.moveCount > 0 }
    }

    //endregion
    //region Exceeds print area
    private fun exceedsPrintArea(
        printerProfile: PrinterProfile?,
        context: GcodeRenderContext?,
    ): Boolean {
        context ?: return false
        printerProfile ?: return false
        val bounds = context.gcodeBounds
        bounds ?: return false
        val w = printerProfile.volume.width
        val h = printerProfile.volume.depth

        // Give 5% slack to make sure we don't count homing positions as out of bounds
        val graceDistance = minOf(w, h) * 0.05f

        val minX: Float
        val minY: Float
        val maxX: Float
        val maxY: Float

        when (printerProfile.volume.origin) {
            PrinterProfile.Origin.LowerLeft -> {
                minX = -graceDistance
                maxX = w + graceDistance
                maxY = h + graceDistance
                minY = -graceDistance
            }
            PrinterProfile.Origin.Center -> {
                maxX = (w / 2) + graceDistance
                minX = -maxX
                maxY = (h / 2) + graceDistance
                minY = -maxY
            }
        }

        return bounds.left < minX || bounds.top > maxY || bounds.right > maxX || bounds.bottom < minY
    }
    //endregion

    fun useLive() {
        triggerFlow.value = Trigger.Live()
    }

    fun useManual(layerIndex: Int, layerProgress: Float) {
        triggerFlow.value = Trigger.Manual(layerIndex = layerIndex, layerProgress = layerProgress)
    }

    fun allowLargeDownloads() {
        largeDownloadsAllowedUntilFlow.value = System.currentTimeMillis() + 10_000
    }

    fun retry() {
        retryFlow.value = System.currentTimeMillis()
    }

    //region Internal classes
    private class FeatureDisabledException : SuppressedIllegalStateException("Feature disabled")
    private class PrintingFromSdCardException(val file: FileObject.File) : SuppressedIllegalStateException("Printing from SD card")

    private sealed class Trigger {

        abstract fun toRenderContextFactory(currentMessage: Message.Current): Pair<GcodeRenderContextFactory, Boolean>

        abstract fun createResultId(currentMessage: Message.Current): Int

        data class Live(private val seed: Double = Math.random()) : Trigger() {
            override fun toRenderContextFactory(currentMessage: Message.Current) =
                GcodeRenderContextFactory.ForFileLocation(currentMessage.progress?.filepos?.toInt() ?: Int.MAX_VALUE) to false

            override fun createResultId(currentMessage: Message.Current) =
                currentMessage.progress?.filepos?.toInt() ?: -1
        }

        data class Manual(private val layerIndex: Int, private val layerProgress: Float) : Trigger() {
            override fun toRenderContextFactory(currentMessage: Message.Current) =
                GcodeRenderContextFactory.ForLayerProgress(layerIndex = layerIndex, progress = layerProgress) to true

            override fun createResultId(currentMessage: Message.Current) =
                "$layerIndex:$layerProgress".hashCode()
        }
    }
    //endregion

    sealed class ViewState {
        data class Loading(val progress: Float = 0f) : ViewState()
        data class FeatureDisabled(@DrawableRes val background: Int) : ViewState()
        data class LargeFileDownloadRequired(val downloadSize: Long) : ViewState()
        data class Error(val exception: Throwable) : ViewState()
        data class PrintingFromSdCard(val file: FileObject.File) : ViewState()
        data class DataReady(
            val renderStyle: RenderStyle? = null,
            val file: JobInformation.JobFile? = null,
            val printerProfile: PrinterProfile? = null,
            val settings: GcodePreviewSettings,
            val state: State<EnrichedRenderContext>,
        ) : ViewState()
    }

    data class EnrichedRenderContext(
        val renderContext: GcodeRenderContext? = null,
        val exceedsPrintArea: Boolean? = null,
        val unsupportedGcode: Boolean? = null,
        val isTrackingPrintProgress: Boolean = false,
    )

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "GcodeControlsViewModel@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = GcodePreviewControlsViewModel(
            instanceId = instanceId,
            octoPrintProvider = BaseInjector.get().octoPrintProvider(),
            octoPrintRepository = BaseInjector.get().octorPrintRepository(),
            generateRenderStyleUseCase = BaseInjector.get().generateRenderStyleUseCase(),
            gcodeFileRepository = BaseInjector.get().gcodeFileRepository(),
            octoPreferences = BaseInjector.get().octoPreferences()
        ) as T
    }
}
