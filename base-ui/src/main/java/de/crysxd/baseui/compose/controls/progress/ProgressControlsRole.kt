package de.crysxd.baseui.compose.controls.progress

enum class ProgressControlsRole {
    ForNormal, ForWebcamFullscreen
}
