package de.crysxd.baseui.compose.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Shapes
import androidx.compose.material.ripple.LocalRippleTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTagsAsResourceId
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.navigation.NavController
import de.crysxd.baseui.BuildConfig
import de.crysxd.baseui.OctoActivity
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.FormatDurationUseCase
import de.crysxd.octoapp.base.usecase.FormatEtaUseCase
import kotlinx.coroutines.flow.map
import timber.log.Timber

val LocalOctoAppColors = compositionLocalOf<OctoAppColors> { object : OctoAppColors {} }
val LocalOctoAppDimensions = compositionLocalOf<OctoAppDimensions> { object : OctoAppDimensions {} }
val LocalOctoAppTypography = compositionLocalOf<OctoAppTypography> { object : OctoAppTypography {} }
val LocalFormatEtaUseCase = compositionLocalOf<FormatEtaUseCase> { throw IllegalAccessException("Wrap in OctoAppTheme!") }
val LocalFormatDurationUseCase = compositionLocalOf<FormatDurationUseCase> { throw IllegalAccessException("Wrap in OctoAppTheme!") }
val LocalNavController = compositionLocalOf<() -> NavController> { throw IllegalAccessException("Wrap in OctoAppTheme!") }
val LocalOctoActivity = compositionLocalOf<OctoActivity?> { throw IllegalAccessException("Wrap in OctoAppTheme!") }
val LocalFragmentManager = compositionLocalOf<() -> FragmentManager> { throw IllegalAccessException("Wrap in OctoAppTheme!") }
val LocalCompactLayout = compositionLocalOf<Boolean> { throw IllegalAccessException("Wrap in OctoAppTheme!") }
val LocalPreviewProvider = compositionLocalOf<Boolean> { false }

object OctoAppTheme {
    val colors @Composable get() = LocalOctoAppColors.current
    val dimens @Composable get() = LocalOctoAppDimensions.current
    val typography @Composable get() = LocalOctoAppTypography.current
    val formatEtaUseCase @Composable get() = LocalFormatEtaUseCase.current
    val octoActivity @Composable get() = LocalOctoActivity.current
    val formatDurationUseCase @Composable get() = LocalFormatDurationUseCase.current
    val compactLayout @Composable get() = LocalCompactLayout.current
    val isPreview @Composable get() = LocalPreviewProvider.current

    @Deprecated("Do not use", ReplaceWith(""))
    val navController
        @Composable get() = LocalNavController.current

    @Deprecated("Do not use", ReplaceWith(""))
    val fragmentManager
        @Composable get() = LocalFragmentManager.current
}

@Composable
fun OctoAppThemeForPreview(content: @Composable () -> Unit) = OctoAppTheme(
    formatDurationUseCase = FormatDurationUseCase(LocalContext.current),
    formatEtaUseCase = FormatEtaUseCase(FormatDurationUseCase(LocalContext.current), LocalContext.current),
    viewModelStoreOwner = requireNotNull(LocalViewModelStoreOwner.current),
    navController = { throw IllegalAccessException("Not available in preview") },
    fragmentManager = { throw IllegalAccessException("Not available in preview") },
    octoActivity = null,
    compactLayout = remember { mutableStateOf(false) },
) {
    require(BuildConfig.DEBUG) { "Preview requires debug build, currently ${BuildConfig.BUILD_TYPE}" }

    LaunchedEffect(Unit) {
        Timber.plant(Timber.DebugTree())
    }

    CompositionLocalProvider(LocalPreviewProvider provides true) {
        content()
    }
}

@Composable
@Suppress("KotlinConstantConditions")
@OptIn(ExperimentalComposeUiApi::class)
fun OctoAppTheme(
    colors: OctoAppColors = object : OctoAppColors {},
    dimens: OctoAppDimensions = object : OctoAppDimensions {},
    typography: OctoAppTypography = object : OctoAppTypography {},
    formatEtaUseCase: FormatEtaUseCase = BaseInjector.get().formatEtaUseCase(),
    formatDurationUseCase: FormatDurationUseCase = BaseInjector.get().formatDurationUseCase(),
    compactLayout: State<Boolean> = remember { BaseInjector.get().octoPreferences().updatedFlow2.map { it.compactLayout } }.collectAsState(initial = false),
    viewModelStoreOwner: ViewModelStoreOwner,
    octoActivity: OctoActivity?,
    navController: () -> NavController = { requireNotNull(octoActivity).navController },
    fragmentManager: () -> FragmentManager,
    content: @Composable () -> Unit,
) = MaterialTheme(
    colors = Colors(
        primary = colors.accent,
        primaryVariant = colors.primaryDark,
        secondary = colors.primary,
        secondaryVariant = colors.accent,
        background = colors.windowBackground,
        error = colors.colorError,
        onBackground = colors.normalText,
        onError = colors.textColoredBackground,
        onPrimary = colors.textColoredBackground,
        onSecondary = colors.textColoredBackground,
        onSurface = colors.normalText,
        surface = colors.inputBackground,
        isLight = !isSystemInDarkTheme()
    ),
    shapes = Shapes(
        small = RoundedCornerShape(percent = 50),
        medium = RoundedCornerShape(dimens.cornerRadiusSmall),
        large = RoundedCornerShape(dimens.cornerRadius),
    )
) {
    CompositionLocalProvider(
        LocalOctoAppTypography provides typography,
        LocalOctoAppDimensions provides dimens,
        LocalOctoAppColors provides colors,
        LocalRippleTheme provides OctoAppRippleTheme(colors),
        LocalFormatEtaUseCase provides formatEtaUseCase,
        LocalFormatDurationUseCase provides formatDurationUseCase,
        LocalViewModelStoreOwner provides viewModelStoreOwner,
        LocalFragmentManager provides fragmentManager,
        LocalNavController provides navController,
        LocalCompactLayout provides compactLayout.value,
        LocalOctoActivity provides octoActivity,
    ) {
        Box(
            modifier = Modifier.semantics {
                testTagsAsResourceId = true
            }
        ) {
            content()
        }
    }
}