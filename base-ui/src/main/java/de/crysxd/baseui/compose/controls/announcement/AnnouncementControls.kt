package de.crysxd.baseui.compose.controls.announcement

import android.net.Uri
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.framework.AnnouncementController
import de.crysxd.baseui.compose.framework.AnnouncementId
import de.crysxd.baseui.compose.framework.SmallAnnouncement
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.ext.purchaseOffers
import de.crysxd.octoapp.base.ext.toHtml

@Composable
fun AnnouncementControls(
    editState: EditState,
) = AnimatedVisibility(
    visible = !editState.editing,
    enter = expandVertically(expandFrom = Alignment.Top) + fadeIn(),
    exit = shrinkVertically(shrinkTowards = Alignment.Top) + fadeOut(),
) {
    Column(
        modifier = Modifier.padding(horizontal = OctoAppTheme.dimens.margin2),
        verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1)
    ) {
        //region Sale
        Firebase.remoteConfig.purchaseOffers.activeConfig.advertisementWithData?.let { ad ->
            val message = ad.message.takeIf { b -> b.isNotBlank() } ?: return@let
            AnnouncementController(announcementId = AnnouncementId.Default(ad.id)) {
                SmallAnnouncement(
                    text = message.toHtml(),
                    learnMoreButton = stringResource(id = R.string.learn_more),
                    onLearnMore = { UriLibrary.getPurchaseUri().open() },
                    foregroundColor = OctoAppTheme.colors.red,
                    backgroundColor = OctoAppTheme.colors.redTranslucent,
                    modifier = Modifier.padding(bottom = OctoAppTheme.dimens.margin2)
                )
            }
        }
        //endregion
        //region What's new?
        AnnouncementController(announcementId = AnnouncementId.Default(stringResource(id = R.string.pref_key_version_announcement))) {
            val uri = stringResource(R.string.version_announcement_learn_more_link)
            SmallAnnouncement(
                text = stringResource(id = R.string.announcement___new_version_title),
                learnMoreButton = stringResource(id = R.string.announcement___new_version_learn_more),
                onLearnMore = {
                    Uri.parse(uri).open()
                },
            )
        }
        //endregion
        //region Control Center
        AnnouncementController(announcementId = AnnouncementId.Default(stringResource(id = R.string.pref_key_quick_switch_announcement))) {
            SmallAnnouncement(
                text = stringResource(id = R.string.control_center___announcement),
            )
        }
        //endregion
    }
}

//region Preview
@Preview
@Composable
private fun Preview() = OctoAppThemeForPreview {
    AnnouncementControls(editState = EditState.ForPreview)
}
//endregion
