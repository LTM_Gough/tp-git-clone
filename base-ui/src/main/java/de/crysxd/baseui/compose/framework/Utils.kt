package de.crysxd.baseui.compose.framework

import android.graphics.Typeface
import android.text.Spanned
import android.text.style.StyleSpan
import android.text.style.URLSpan
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.base.usecase.FormatEtaUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import timber.log.Timber

@Composable
fun Int?.rememberAsFormattedEta(useCompactDate: Boolean, allowRelative: Boolean, showLabel: Boolean = false): MutableState<String> {
    val formatted = remember { mutableStateOf("") }
    val uc = OctoAppTheme.formatEtaUseCase

    LaunchedCoroutineEffect(this, useCompactDate) {
        formatted.value = this@rememberAsFormattedEta?.let {
            uc.execute(
                FormatEtaUseCase.Params(
                    secsLeft = it.toLong(),
                    showLabel = showLabel,
                    allowRelative = allowRelative,
                    useCompactDate = useCompactDate,
                )
            )
        } ?: ""
    }

    return formatted
}

fun Modifier.thenIf(condition: Boolean, block: @Composable Modifier.() -> Modifier) = composed {
    if (condition) {
        block()
    } else {
        this
    }
}

@Composable
fun Int?.rememberAsFormattedDuration(): MutableState<String> {
    val formatted = remember { mutableStateOf("") }
    val uc = OctoAppTheme.formatDurationUseCase

    LaunchedCoroutineEffect(this) {
        formatted.value = this@rememberAsFormattedDuration?.let { uc.execute(it.toLong()) } ?: ""
    }

    return formatted
}

@Composable
fun LaunchedCoroutineEffect(
    vararg keys: Any?,
    block: suspend CoroutineScope.() -> Unit
) {
    val scope = rememberCoroutineScope()
    LaunchedEffect(keys = keys) {
        scope.launch {
            try {
                block()
            } catch (e: Exception) {
                Timber.e(e, "Exception in LaunchedCoroutineEffect")
            }
        }
    }
}

@Composable
fun CharSequence.asAnnotatedString() = if (this is AnnotatedString) this else buildAnnotatedString {
    append(this@asAnnotatedString.toString())

    if (this@asAnnotatedString is Spanned) {
        getSpans(0, this@asAnnotatedString.length, StyleSpan::class.java).forEach {
            addStyle(
                start = getSpanStart(it),
                end = getSpanEnd(it),
                style = when (it.style) {
                    Typeface.BOLD -> SpanStyle(fontWeight = FontWeight.Bold)
                    Typeface.ITALIC -> SpanStyle(fontStyle = FontStyle.Italic)
                    Typeface.BOLD_ITALIC -> SpanStyle(fontStyle = FontStyle.Italic, fontWeight = FontWeight.Bold)
                    else -> SpanStyle()
                },
            )
        }

        getSpans(0, this@asAnnotatedString.length, URLSpan::class.java).forEach {
            addStringAnnotation(
                tag = "URL",
                annotation = it.url,
                start = getSpanStart(it),
                end = getSpanEnd(it)
            )
            addStyle(
                start = getSpanStart(it),
                end = getSpanEnd(it),
                style = SpanStyle(
                    color = OctoAppTheme.colors.accent,
                    textDecoration = TextDecoration.Underline
                ),
            )
        }
    }
}

@Suppress("UNCHECKED_CAST")
private object SavedData {

    private val data = mutableMapOf<String, Any?>()
    fun <T : Any?> save(instanceId: String, key: String, item: T) = data.set("$instanceId/$key", item)
    fun <T : Any?> get(instanceId: String, key: String, default: T) = data.getOrElse("$instanceId/$key") { default } as T

}

@Composable
fun <T : Any?> Flow<T>.collectAsStateWhileActive(
    active: Boolean,
    initial: T,
    key: String,
): State<T> {
    val instanceId = LocalOctoPrint.current.id
    val state = remember { mutableStateOf(SavedData.get(instanceId = instanceId, key = key, default = initial)) }

    LaunchedEffect(active, key, instanceId) {
        if (active) {
            state.value = SavedData.get(instanceId = instanceId, key = key, default = initial)
            collect {
                state.value = it
                SavedData.save(instanceId = instanceId, key = key, item = it)
            }
        }
    }

    return state
}
