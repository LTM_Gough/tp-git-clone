package de.crysxd.baseui.menu.settings

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import de.crysxd.baseui.R
import de.crysxd.baseui.common.LinkClickMovementMethod
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.menu.base.Menu
import de.crysxd.baseui.menu.base.MenuHost
import de.crysxd.baseui.menu.base.MenuItem
import de.crysxd.baseui.menu.base.MenuItemStyle
import de.crysxd.baseui.menu.base.SubMenuItem
import de.crysxd.baseui.menu.settings.PrintNotificationsMenu.Companion.notificationMenuBadgeCount
import de.crysxd.baseui.menu.switchprinter.SwitchOctoPrintDisabledMenu
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_CACHES_MENU
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_HELP
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_LIVE_NOTIFICATION
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_SHOW_AUTO_CONNECT_PRINTER_MENU
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_SHOW_CHANGE_OCTOPRINT_MENU
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_SHOW_POWER_SETTINGS_MENU
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_SHOW_UI_SETTINGS_MENU
import de.crysxd.octoapp.base.di.BaseInjector
import kotlinx.parcelize.Parcelize

@Parcelize
class SettingsMenu : Menu {
    override suspend fun getMenuItem() = listOf(
        HelpMenuItem(),
        PrintNotificationMenuItem(),
        ShowAutoConnectPrinterMenu(),
        ChangeOctoPrintInstanceMenuItem(),
        ShowOctoAppLabMenuItem(),
        ShowUiSettingsMenuItem(),
        ShowPowerDeviceSettingsMenuItem(),
        CachesMenuItem(),
    )

    override suspend fun getTitle(context: Context) = context.getString(R.string.main_menu___menu_settings_title)
    override suspend fun getSubtitle(context: Context) = context.getString(R.string.main_menu___submenu_subtitle)
    override fun getBottomText(context: Context) = HtmlCompat.fromHtml(
        context.getString(
            R.string.main_menu___about_text,
            ContextCompat.getColor(context, R.color.dark_text),
            context.packageManager.getPackageInfo(context.packageName, 0).versionName
        ),
        HtmlCompat.FROM_HTML_MODE_COMPACT
    )

    override fun getBottomMovementMethod(host: MenuHost) =
        LinkClickMovementMethod(object : LinkClickMovementMethod.OpenWithIntentLinkClickedListener(host.getMenuActivity()) {
            override fun onLinkClicked(context: Context, url: String?): Boolean {
                return if (url == "privacy") {
                    host.pushMenu(PrivacyMenu())
                    true
                } else {
                    super.onLinkClicked(context, url)
                }
            }
        })
}

class HelpMenuItem : MenuItem {
    override val itemId = MENU_ITEM_HELP
    override var groupId = "help"
    override val order = 101
    override val enforceSingleLine = false
    override val style = MenuItemStyle.Settings
    override val icon = R.drawable.ic_round_help_outline_24

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_help_faq_and_feedback)
    override suspend fun onClicked(host: MenuHost?) {
        host?.getMenuActivity()?.let {
            UriLibrary.getHelpUri().open(it)
        }
        host?.closeMenu()
    }
}

class PrintNotificationMenuItem : SubMenuItem() {
    override val subMenu: Menu get() = PrintNotificationsMenu()
    override val itemId = MENU_ITEM_LIVE_NOTIFICATION
    override var groupId = "default"
    override val order = 117
    override val enforceSingleLine = false
    override val style = MenuItemStyle.Settings
    override val icon = R.drawable.ic_round_notifications_active_24
    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_print_notifications)
    override fun getBadgeCount() = notificationMenuBadgeCount
}

class ShowUiSettingsMenuItem : SubMenuItem() {
    override val itemId = MENU_ITEM_SHOW_UI_SETTINGS_MENU
    override var groupId = "default"
    override val order = 118
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_phonelink_setup_24
    override val subMenu: Menu get() = UiSettingsMenu()

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___menu_ui_settings_title)
}

class ShowPowerDeviceSettingsMenuItem : SubMenuItem() {
    override val itemId = MENU_ITEM_SHOW_POWER_SETTINGS_MENU
    override var groupId = "default"
    override val order = 119
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_power_24
    override val subMenu: Menu get() = PowerSettingsMenu()

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___menu_power_settings_title)
}

class ShowAutoConnectPrinterMenu : SubMenuItem() {
    override val itemId = MENU_ITEM_SHOW_AUTO_CONNECT_PRINTER_MENU
    override var groupId = "default"
    override val order = 120
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_hdr_auto_24px
    override val subMenu: Menu get() = AutoConnectPrinterMenu()

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_auto_connect_printer)
}

open class CachesMenuItem : SubMenuItem() {
    override val itemId = MENU_ITEM_CACHES_MENU
    override var groupId = "default"
    override val order = 131
    override val style: MenuItemStyle = MenuItemStyle.Settings
    override val icon = R.drawable.ic_round_sd_storage_24
    override val subMenu: Menu get() = CachesMenu()

    override fun getTitle(context: Context) = context.getString(R.string.caches_menu___title)
}


class ShowOctoAppLabMenuItem : SubMenuItem() {
    override val itemId = MENU_ITEM_SHOW_CHANGE_OCTOPRINT_MENU
    override var groupId = "default"
    override val order = 132
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_science_24px
    override val subMenu: Menu get() = OctoAppLabMenu()

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___show_octoapp_lab)
}

open class ChangeOctoPrintInstanceMenuItem : SubMenuItem() {
    override val itemId = MENU_ITEM_SHOW_CHANGE_OCTOPRINT_MENU
    override var groupId = "change"
    override val order = 151
    override val style: MenuItemStyle = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_swap_horiz_24
    override val subMenu: Menu get() = SwitchOctoPrintDisabledMenu()
    override fun isVisible(destinationId: Int) = !BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH)

    override fun getTitle(context: Context) =
        context.getString(
            if (BaseInjector.get().octorPrintRepository().getAll().size > 1) {
                R.string.main_menu___item_change_octoprint_instance
            } else {
                R.string.main_menu___item_add_octoprint_instance
            }
        )
}