package de.crysxd.baseui.menu.settings

import android.content.Context
import de.crysxd.baseui.R
import de.crysxd.baseui.menu.base.Menu
import de.crysxd.baseui.menu.base.MenuHost
import de.crysxd.baseui.menu.base.MenuItemStyle
import de.crysxd.baseui.menu.base.ToggleMenuItem
import de.crysxd.octoapp.base.data.models.MenuItems
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.toHtml
import kotlinx.parcelize.Parcelize

@Parcelize
class AutoConnectPrinterMenu : Menu {

    override suspend fun getMenuItem() = listOf(AutoConnectPrinterMenuItem())
    override suspend fun getTitle(context: Context) = context.getString(R.string.main_menu___item_auto_connect_printer)
    override suspend fun getSubtitle(context: Context) = context.getString(R.string.connect_printer___auto_menu___subtitle).toHtml()

}

class AutoConnectPrinterMenuItem : ToggleMenuItem {
    override val isChecked get() = BaseInjector.get().octoPreferences().isAutoConnectPrinter
    override val itemId = MenuItems.MENU_ITEM_AUTO_CONNECT_PRINTER
    override var groupId = ""
    override val order = 107
    override val style = MenuItemStyle.Settings
    override val icon = R.drawable.ic_round_hdr_auto_24px

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_auto_connect_printer)
    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        BaseInjector.get().octoPreferences().isAutoConnectPrinter = enabled
    }
}
