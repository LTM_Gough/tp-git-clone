package de.crysxd.baseui.menu.base

import android.graphics.Rect
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.transition.Transition
import androidx.transition.TransitionManager
import de.crysxd.baseui.BaseBottomSheetDialogFragment
import de.crysxd.baseui.R
import de.crysxd.baseui.databinding.MenuBottomSheetFragmentBinding
import de.crysxd.baseui.di.injectViewModel
import de.crysxd.baseui.ext.optionallyRequestOctoActivity
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.baseui.menu.main.MainMenu
import de.crysxd.baseui.utils.InstantAutoTransition
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.data.models.MenuId
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Runnable
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.UUID


@Suppress("SuspiciousCollectionReassignment")
open class MenuBottomSheetFragment : BaseBottomSheetDialogFragment(), MenuHost {
    override val viewModel by injectViewModel<MenuBottomSheetViewModel>()
    private lateinit var viewBinding: MenuBottomSheetFragmentBinding
    private val adapter = MenuAdapter(
        onClick = ::executeClick,
        onPinItem = ::executeLongClick,
        onSecondaryClick = ::executeSecondaryClick,
        menuId = MenuId.MainMenu,
        onFeatureDisabled = { pushMenu(SupporterPerkMenu(it)) }
    )
    private val showLoadingRunnable = Runnable {
        viewBinding.loadingOverlay.isVisible = true
        viewBinding.loadingOverlay.animate().alpha(if (isLoading) 1f else 0f).withEndAction { viewBinding.loadingOverlay.isVisible = isLoading }.start()
    }
    private var lastClickedMenuItem: MenuItem? = null
    private var suppressSuccessAnimation = false
    private val isLoading
        get() = loadingTokens.isNotEmpty()
    private var loadingTokens = setOf<String>()
        set(value) {
            Timber.i("loading Tokens: $value")
            if (value.isNotEmpty()) {
                Timber.i("Show loading")
                view?.postDelayed(showLoadingRunnable, 200L)
            } else if (value.isEmpty()) {
                Timber.i("Hide loading")
                showLoadingRunnable.run()
                view?.removeCallbacks(showLoadingRunnable)
            }

            field = value
        }

    companion object {
        private const val KEY_MENU = "menu"
        private const val KEY_DESTINATION = "destination"
        fun createForMenu(menu: Menu, @IdRes currentDestination: Int? = null) = MenuBottomSheetFragment().also {
            it.arguments = bundleOf(KEY_MENU to menu, KEY_DESTINATION to currentDestination)
        }
    }

    override fun onResume() {
        super.onResume()
        reloadMenu()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.menuBackStack.forEach { it.onDestroy() }
    }

    private val rootMenu get() = arguments?.getParcelable<Menu>(KEY_MENU) ?: MainMenu()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        MenuBottomSheetFragmentBinding.inflate(inflater, container, false).also { viewBinding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.recyclerView.adapter = adapter
        viewBinding.recyclerView.layoutManager = GridLayoutManager(requireContext(), 2).also {
            it.spanSizeLookup = SpanSizeLookUp()
        }

        if (viewModel.menuBackStack.isEmpty()) {
            pushMenu(rootMenu)
        } else {
            showMenu(viewModel.menuBackStack.last())
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?) = super.onCreateDialog(savedInstanceState).also {
        it.setOnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP && !popMenu()) {
                dismissAllowingStateLoss()
            }
            true
        }
    }

    fun show(fm: FragmentManager) = try {
        show(fm, "main-menu")
    } catch (e: IllegalStateException) {
        Timber.w("Can't show menu, after onSaveInstanceState")
    }

    override fun pushMenu(subMenu: Menu) {
        viewModel.menuBackStack.add(subMenu)
        showMenu(subMenu)
    }

    override fun closeMenu() = dismissAllowingStateLoss()

    override fun getNavController() = findNavController()

    override fun getMenuActivity() = requireOctoActivity()

    override fun getMenuFragmentManager() = childFragmentManager

    override fun getHostFragment(): Fragment? = parentFragment

    private fun showMenu(settingsMenu: Menu, smallChange: Boolean = false) {
        Timber.i("Loading ${settingsMenu::class.java.name}")
        val loadingToken = if (settingsMenu !is MainMenu) UUID.randomUUID().toString() else null

        val internal = suspend {
            try {
                // Check if the menu wants to be shown (e.g. power menu can auto handle some requests)
                loadingToken?.let { loadingTokens += it }
                Timber.i("Start loading")
                if (settingsMenu.shouldShowMenu(this@MenuBottomSheetFragment)) {
                    // Load items
                    val currentDestination = arguments?.getInt(KEY_DESTINATION, 0)?.takeIf { it != 0 } ?: findNavController().currentDestination?.id ?: 0
                    val context = requireContext()
                    val items = withContext(Dispatchers.IO) {
                        settingsMenu.getMenuItem().map { item ->
                            PreparedMenuItem(
                                menuItem = item,
                                title = item.getTitle(context),
                                right = item.getRightDetail(context),
                                description = item.getDescription(context),
                                isVisible = item.isVisible(currentDestination),
                                isEnabled = item.isEnabled(currentDestination),
                                badgeCount = item.getBadgeCount(),
                                isFeatureEnabled = item.billingManagerFeature?.let { BillingManager.isFeatureEnabled(it) } ?: true
                            )
                        }.filter {
                            it.isVisible
                        }.sortedWith(compareBy<PreparedMenuItem> { it.menuItem.order }.thenBy { it.title.toString() })
                    }
                    val subtitle = settingsMenu.getSubtitle(requireContext())
                    val title = settingsMenu.getTitle(requireContext())

                    // Prepare animation
                    loadingToken?.let { loadingTokens -= it }
                    Timber.i("End loading")
                    viewBinding.bottom.movementMethod = null
                    beginDelayedTransition(smallChange) {
                        // Need to be applied after transition to prevent glitches, but also check we are still added to ensure state
                        if (isAdded) {
                            viewBinding.bottom.movementMethod = settingsMenu.getBottomMovementMethod(this@MenuBottomSheetFragment)
                        }
                    }

                    // Header
                    viewBinding.customHeaderContainer.removeAllViews()
                    settingsMenu.getCustomHeaderView(this)?.let {
                        viewBinding.customHeaderContainer.addView(it)
                    }

                    // Show menu
                    val normalIcon = settingsMenu.getNormalStateIcon()
                    val emptyStateIcon = settingsMenu.getEmptyStateIcon()
                    val emptyStateText = settingsMenu.getEmptyStateActionText(context)
                    val emptyStateAction = settingsMenu.getEmptyStateAction(context)
                    adapter.menuItems = items
                    viewBinding.emptyStateIcon.setImageResource(emptyStateIcon)
                    (viewBinding.emptyStateIcon.drawable as? Animatable)?.start()
                    viewBinding.emptyStateAction.text = emptyStateText
                    viewBinding.emptyStateAction.setOnClickListener {
                        try {
                            emptyStateAction?.invoke()
                        } catch (e: Exception) {
                            Timber.e(e)
                            requireOctoActivity().showDialog(e)
                        }
                    }
                    viewBinding.emptyStateAction.isVisible = emptyStateText != null && emptyStateAction != null
                    viewBinding.emptyState.isVisible = emptyStateIcon != 0 && items.isEmpty()
                    viewBinding.recyclerView.isVisible = !viewBinding.emptyState.isVisible
                    viewBinding.title.text = title
                    viewBinding.normalStateIcon.setImageResource(normalIcon)
                    (viewBinding.normalStateIcon.drawable as? Animatable)?.start()
                    viewBinding.normalStateIcon.isVisible = normalIcon != 0 && !viewBinding.emptyState.isVisible
                    viewBinding.title.isVisible = viewBinding.title.text.isNotBlank()
                    viewBinding.subtitle.text = settingsMenu.getEmptyStateSubtitle(context).takeIf { items.isEmpty() } ?: subtitle
                    viewBinding.subtitle.isVisible = viewBinding.subtitle.text.isNotBlank()
                    viewBinding.bottom.text = settingsMenu.getBottomText(requireContext())
                    viewBinding.bottom.isVisible = viewBinding.bottom.text.isNotBlank() && viewBinding.recyclerView.isVisible
                    viewBinding.checkbox.text = settingsMenu.getCheckBoxText(requireContext())
                    viewBinding.checkbox.isVisible = viewBinding.checkbox.text.isNotBlank()
                    viewBinding.checkbox.isChecked = false
                    lastClickedMenuItem = null

                    // Update bottom sheet size
                    forceResizeBottomSheet()
                } else {
                    abortShowMenu(false)
                    if (!consumeSuccessAnimationForNextActionSuppressed()) {
                        lastClickedMenuItem?.let { adapter.playSuccessAnimationForItem(it) }
                    }
                }
            } catch (e: Exception) {
                Timber.e(e, "Error while inflating menu")
                optionallyRequestOctoActivity()?.showDialog(e)
                abortShowMenu()
            } finally {
                loadingToken?.let { loadingTokens -= it }
            }
        }

        // We don't want the loading state to flash in when opening main menu and we also don't need to
        // build it async -> run blocking for main menu
//        if (settingsMenu.shouldLoadBlocking()) {
//            Timber.i("Using blocking method to inflate main menu")
//            runBlocking { internal() }
//        } else {
        Timber.i("Using async method to inflate $settingsMenu")
        // First menu? no overlay background
        viewBinding.loadingOverlay.setBackgroundColor(
            ContextCompat.getColor(requireContext(), if (viewModel.menuBackStack.size == 1) android.R.color.transparent else R.color.black_translucent)
        )
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            internal()
            viewBinding.loadingOverlay.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.black_translucent))
        }
//        }
    }

    private fun abortShowMenu(showPrevious: Boolean = true) {
        if (!popMenu(showPrevious)) {
            viewModel.menuBackStack.last().onDestroy()
            dismissAllowingStateLoss()
        }
    }

    private fun popMenu(showPrevious: Boolean = true): Boolean = if (viewModel.menuBackStack.size <= 1) {
        false
    } else {
        viewModel.menuBackStack.removeLast().onDestroy()
        if (showPrevious) {
            showMenu(viewModel.menuBackStack.last())
        }
        true
    }

    private fun beginDelayedTransition(smallChange: Boolean = false, root: ViewGroup? = view?.rootView as? ViewGroup, endAction: () -> Unit = {}) {
        root?.let {
            // We need a offset if the view does not span the entire screen as the epicenter is in screen coordinates (?)
            val epicenterX = getScreenCenter()
            val epicenterY = 0
            TransitionManager.beginDelayedTransition(
                root,
                InstantAutoTransition(
                    explode = !smallChange,
                    explodeEpicenter = Rect(epicenterX - 200, epicenterY - 200, epicenterX + 200, epicenterY - 200),
                    fadeText = !smallChange
                ).also { t ->
                    t.addListener(
                        object : Transition.TransitionListener {
                            override fun onTransitionStart(transition: Transition) = Unit
                            override fun onTransitionCancel(transition: Transition) = Unit
                            override fun onTransitionPause(transition: Transition) = Unit
                            override fun onTransitionResume(transition: Transition) = Unit
                            override fun onTransitionEnd(transition: Transition) = endAction()
                        }
                    )
                }
            )
        }
    }

    override fun reloadMenu() {
        showMenu(viewModel.menuBackStack.last(), smallChange = true)
    }

    override fun isCheckBoxChecked() = viewBinding.checkbox.isChecked

    override fun suppressSuccessAnimationForNextAction() {
        suppressSuccessAnimation = true
    }

    override fun consumeSuccessAnimationForNextActionSuppressed(): Boolean {
        val value = suppressSuccessAnimation
        suppressSuccessAnimation = false
        return value
    }

    private fun executeLongClick(item: MenuItem, anchor: View) = PinControlsPopupMenu(requireContext(), MenuId.MainMenu).show(item.itemId, anchor) {
        // We need to reload the main menu if a favorite was changed in case it was removed
        reloadMenu()
    }

    private fun executeSecondaryClick(item: MenuItem) {
        Timber.i("loading: $isLoading")
        if (isLoading) return
        val token = UUID.randomUUID().toString()

        viewModel.execute {
            try {
                loadingTokens += token
                lastClickedMenuItem = item
                item.onSecondaryClicked(this@MenuBottomSheetFragment)
            } finally {
                loadingTokens -= token
            }
        }
    }

    private fun executeClick(item: MenuItem) {
        Timber.i("loading: $isLoading")
        if (isLoading) return
        val token = UUID.randomUUID().toString()

        viewModel.execute {
            try {
                loadingTokens += token
                lastClickedMenuItem = item
                MenuItemClickExecutor(this, adapter).execute(item)
            } finally {
                loadingTokens -= token
            }
        }
    }

    private inner class SpanSizeLookUp : GridLayoutManager.SpanSizeLookup() {
        override fun getSpanSize(position: Int) = if (adapter.menuItems[position].menuItem.showAsHalfWidth) 1 else 2
    }
}