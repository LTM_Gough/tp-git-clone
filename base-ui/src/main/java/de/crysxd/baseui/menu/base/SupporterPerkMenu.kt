package de.crysxd.baseui.menu.base

import android.content.Context
import de.crysxd.baseui.R
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.octoapp.base.UriLibrary
import kotlinx.parcelize.Parcelize

@Parcelize
data class SupporterPerkMenu(private val featureName: CharSequence) : Menu {

    override suspend fun getMenuItem(): List<MenuItem> = listOf(
        LearnMoreMenuItem(),
        SupportOctoAppMenuItem()
    )

    override fun shouldLoadBlocking() = true
    override suspend fun getTitle(context: Context) = context.getString(R.string.supporter_perk___title)
    override suspend fun getSubtitle(context: Context) = context.getString(R.string.supporter_perk___description, featureName)

    override fun getNormalStateIcon() = R.drawable.octo_support

    private class LearnMoreMenuItem() : MenuItem {
        override val itemId = "learn_more"
        override var groupId = ""
        override val order = 1
        override val style = MenuItemStyle.Neutral
        override val icon = R.drawable.ic_round_info_24

        override fun getTitle(context: Context) = context.getString(R.string.learn_more)

        override suspend fun onClicked(host: MenuHost?) {
            UriLibrary.getPurchaseUri().open(requireNotNull(host?.getHostFragment()?.requireOctoActivity()))
        }
    }

    private class SupportOctoAppMenuItem() : MenuItem {
        override val itemId = "support"
        override var groupId = ""
        override val order = 2
        override val style = MenuItemStyle.Support
        override val icon = R.drawable.ic_round_favorite_24

        override fun getTitle(context: Context) = context.getString(R.string.support_octoapp)

        override suspend fun onClicked(host: MenuHost?) {
            UriLibrary.getPurchaseUri().open(requireNotNull(host?.getHostFragment()?.requireOctoActivity()))
        }
    }
}