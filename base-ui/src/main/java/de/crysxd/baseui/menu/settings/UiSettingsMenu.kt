package de.crysxd.baseui.menu.settings

import android.content.Context
import android.os.Build
import de.crysxd.baseui.R
import de.crysxd.baseui.common.LinkClickMovementMethod
import de.crysxd.baseui.common.controls.ControlsFragment
import de.crysxd.baseui.menu.base.Menu
import de.crysxd.baseui.menu.base.MenuHost
import de.crysxd.baseui.menu.base.MenuItem
import de.crysxd.baseui.menu.base.MenuItemStyle
import de.crysxd.baseui.menu.base.RevolvingOptionsMenuItem
import de.crysxd.baseui.menu.base.ToggleMenuItem
import de.crysxd.octoapp.base.data.models.AppTheme
import de.crysxd.octoapp.base.data.models.MenuItems
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.SetAppLanguageUseCase
import kotlinx.coroutines.runBlocking
import kotlinx.parcelize.Parcelize
import java.util.Locale

@Parcelize
class UiSettingsMenu : Menu {

    override suspend fun getMenuItem() = listOf(
        ChangeLanguageMenuItem(),
        AppThemeMenuItem(),
        KeepScreenOnDuringPrintMenuItem(),
        CustomizeWidgetsMenuItem(),
        LeftHandModeMenuItem(),
        ShowInstanceInStatusBarMenuItem(),
    )

    override suspend fun getTitle(context: Context) = context.getString(R.string.main_menu___menu_ui_settings_title)
    override suspend fun getSubtitle(context: Context) = context.getString(R.string.main_menu___submenu_subtitle)

    override fun getBottomMovementMethod(host: MenuHost) =
        LinkClickMovementMethod(object : LinkClickMovementMethod.OpenWithIntentLinkClickedListener(host.getMenuActivity()) {
            override fun onLinkClicked(context: Context, url: String?): Boolean {
                return if (url == "privacy") {
                    host.pushMenu(PrivacyMenu())
                    true
                } else {
                    super.onLinkClicked(context, url)
                }
            }
        })
}

class ChangeLanguageMenuItem : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_CHANGE_LANGUAGE
    override var groupId = ""
    override val order = 102
    override val enforceSingleLine = false
    override val style = MenuItemStyle.Settings
    override val icon = R.drawable.ic_round_translate_24

    override fun isVisible(destinationId: Int) = runBlocking {
        BaseInjector.get().getAppLanguageUseCase().execute(Unit).canSwitchLocale
    }

    override fun getTitle(context: Context) = runBlocking {
        BaseInjector.get().getAppLanguageUseCase().execute(Unit).switchLanguageText ?: ""
    }

    override suspend fun onClicked(host: MenuHost?) {
        val newLocale = BaseInjector.get().getAppLanguageUseCase().execute(Unit).switchLanguage?.let { Locale(it) }
        host?.getMenuActivity()?.let {
            BaseInjector.get().setAppLanguageUseCase().execute(SetAppLanguageUseCase.Param(newLocale, it))
        }
    }
}

class CustomizeWidgetsMenuItem : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_CUSTOMIZE_WIDGETS
    override var groupId = ""
    override val order = 103
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_person_pin_24
    override val canRunWithAppInBackground = false

    override fun isVisible(destinationId: Int) =
        destinationId == R.id.workspacePrePrint || destinationId == R.id.workspacePrint || destinationId == R.id.controlsFragment

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_customize_widgets)
    override suspend fun onClicked(host: MenuHost?) {
        (host?.getHostFragment() as? ControlsFragment)?.startEdit()
        host?.closeMenu()
    }
}

class AppThemeMenuItem : RevolvingOptionsMenuItem() {
    private val context = BaseInjector.get().localizedContext()
    override val activeValue get() = BaseInjector.get().octoPreferences().appTheme.name
    override val options = listOfNotNull(
        Option(context.getString(R.string.main_menu___item_app_theme_auto), AppTheme.AUTO.toString()).takeIf { Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q },
        Option(context.getString(R.string.main_menu___item_app_theme_dark), AppTheme.DARK.toString()),
        Option(context.getString(R.string.main_menu___item_app_theme_light), AppTheme.LIGHT.toString()),
    )
    override val itemId = MenuItems.MENU_ITEM_NIGHT_THEME
    override var groupId = ""
    override val order = 105
    override val enforceSingleLine = false
    override val style = MenuItemStyle.Settings
    override val icon = R.drawable.ic_round_dark_mode_24

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_app_theme)

    override suspend fun handleOptionActivated(host: MenuHost?, option: Option) {
        BaseInjector.get().octoPreferences().appTheme = AppTheme.valueOf(option.value)
    }
}

class KeepScreenOnDuringPrintMenuItem : ToggleMenuItem {
    override val isChecked get() = BaseInjector.get().octoPreferences().isKeepScreenOnDuringPrint
    override val itemId = MenuItems.MENU_ITEM_SCREEN_ON_DURING_PRINT
    override var groupId = ""
    override val order = 106
    override val style = MenuItemStyle.Settings
    override val icon = R.drawable.ic_round_brightness_high_24

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_keep_screen_on_during_pinrt_on)
    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        BaseInjector.get().octoPreferences().isKeepScreenOnDuringPrint = enabled
    }
}

class LeftHandModeMenuItem : ToggleMenuItem {
    override val isChecked get() = BaseInjector.get().octoPreferences().leftHandMode
    override val itemId = MenuItems.MENU_ITEM_LEFT_HAND_MODE
    override var groupId = ""
    override val order = 108
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_left_back_hand_24

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_left_hand_mode)
    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        BaseInjector.get().octoPreferences().leftHandMode = enabled
    }
}

class ShowInstanceInStatusBarMenuItem : ToggleMenuItem {
    override val isChecked get() = BaseInjector.get().octoPreferences().showActiveInstanceInStatusBar
    override val itemId = MenuItems.MENU_ITEM_SHOW_INSTNACE_IN_STATUS_BAR
    override var groupId = ""
    override val order = 109
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_label_24

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_show_instance_in_status_bar)
    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        BaseInjector.get().octoPreferences().showActiveInstanceInStatusBar = enabled
    }
}