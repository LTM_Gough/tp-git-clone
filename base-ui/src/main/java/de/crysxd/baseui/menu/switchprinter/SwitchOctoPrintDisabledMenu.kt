package de.crysxd.baseui.menu.switchprinter

import android.content.Context
import de.crysxd.baseui.R
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.baseui.menu.base.Menu
import de.crysxd.baseui.menu.base.MenuHost
import de.crysxd.baseui.menu.base.MenuItem
import de.crysxd.baseui.menu.base.MenuItemStyle
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_ENABLE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_SIGN_OUT
import de.crysxd.octoapp.base.di.BaseInjector
import kotlinx.parcelize.Parcelize

@Parcelize
class SwitchOctoPrintDisabledMenu : Menu {

    override fun shouldLoadBlocking() = true
    override suspend fun getTitle(context: Context) = context.getString(R.string.main_menu___title_quick_switch_disabled)
    override suspend fun getSubtitle(context: Context) = context.getString(R.string.main_menu___subtitle_quick_switch_disabled)

    override suspend fun getMenuItem() = listOf(
        SignOutMenuItem(),
        EnableQuickSwitchMenuItem()
    )

    private class SignOutMenuItem : MenuItem {
        override val itemId = MENU_ITEM_SIGN_OUT
        override var groupId = "sign_out"
        override val order = 151
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = R.drawable.ic_round_login_24

        override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_sign_out)
        override suspend fun onClicked(host: MenuHost?) {
            host?.getHostFragment()?.requireOctoActivity()?.let {
                it.enforceAllowAutomaticNavigationFromCurrentDestination()
                it.controlCenter.dismiss()
            }

            BaseInjector.get().octorPrintRepository().clearActive()
            host?.closeMenu()
            host?.getHostFragment()?.requireOctoActivity()?.controlCenter?.dismiss()
        }
    }

    private class EnableQuickSwitchMenuItem : MenuItem {
        override val itemId = MENU_ITEM_ENABLE_QUICK_SWITCH
        override var groupId = ""
        override val order = 149
        override val canBePinned = false
        override val style = MenuItemStyle.Support
        override val icon = R.drawable.ic_round_favorite_24

        override fun getTitle(context: Context) = context.getString(R.string.main_menu___enable_quick_switch)
        override suspend fun onClicked(host: MenuHost?) {
            OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseScreenOpen, mapOf("trigger" to "switch_menu"))
            host?.getMenuActivity()?.let {
                UriLibrary.getPurchaseUri().open(it)
            }
        }
    }
}
