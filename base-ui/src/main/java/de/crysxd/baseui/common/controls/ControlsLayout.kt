package de.crysxd.baseui.common.controls

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.integerResource
import de.crysxd.baseui.R
import de.crysxd.baseui.common.OctoToolbar
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.framework.HeaderSynchronizedLazyColumn
import de.crysxd.baseui.compose.framework.HeaderSynchronizedLazyVerticalGrid
import de.crysxd.baseui.compose.framework.HeaderSynchronizedScrollBox
import de.crysxd.baseui.compose.framework.ReorderableItem2
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.base.data.models.WidgetType
import kotlinx.coroutines.delay
import org.burnoutcrew.reorderable.ItemPosition
import org.burnoutcrew.reorderable.ReorderableItem
import org.burnoutcrew.reorderable.rememberReorderableLazyGridState
import org.burnoutcrew.reorderable.rememberReorderableLazyListState
import org.burnoutcrew.reorderable.reorderable

@Composable
fun ControlsLayout(
    toolbarState: OctoToolbar.State,
    editing: Boolean,
    onMove: (ItemPosition, ItemPosition) -> Unit,
    items: List<Pair<WidgetType, Boolean>>,
    toggleHidden: (WidgetType) -> Unit,
    content: @Composable (WidgetType, EditState) -> Unit
) {
    val columnSpan = integerResource(id = R.integer.widget_list_span_count)

    if (columnSpan == 1) {
        PhoneLayout(
            toolbarState = toolbarState,
            editing = editing,
            onMove = onMove,
            items = items,
            toggleHidden = toggleHidden,
            content = content,
        )
    } else {
        TabletLayout(
            toolbarState = toolbarState,
            editing = editing,
            onMove = onMove,
            items = items,
            toggleHidden = toggleHidden,
            content = content,
            columCount = columnSpan,
        )
    }
}

@Composable
private fun PhoneLayout(
    toolbarState: OctoToolbar.State,
    editing: Boolean,
    onMove: (ItemPosition, ItemPosition) -> Unit,
    items: List<Pair<WidgetType, Boolean>>,
    toggleHidden: (WidgetType) -> Unit,
    content: @Composable (WidgetType, EditState) -> Unit
) {
    val state = rememberReorderableLazyListState(onMove)
    var lastState by remember { mutableStateOf<OctoToolbar.State?>(null) }
    LaunchedEffect(toolbarState) {
        if (lastState != null) {
            delay(1000)
            state.listState.animateScrollToItem(index = 0)
        }
        lastState = toolbarState
    }

    HeaderSynchronizedLazyColumn(
        state = state.listState,
        octoActivity = OctoAppTheme.octoActivity,
        contentPadding = PaddingValues(),
        toolbarState = if (editing) OctoToolbar.State.Hidden else toolbarState,
        modifier = (if (editing) Modifier.reorderable(state) else Modifier).fillMaxSize()
    ) {
        if (!editing) {
            item("paddingTop") { Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin6)) }
        }

        items(
            items = items,
            key = { it.first },
        ) { type ->
            ReorderableItem(
                reorderableState = state,
                key = type.first,
            ) { dragging ->
                val editState = EditState(
                    editing = editing,
                    dragState = state,
                    dragging = dragging,
                    hidden = type.second,
                    toggleHidden = { toggleHidden(type.first) }
                )

                content(type.first, editState)
            }
        }

        if (!editing) {
            item("paddingBottom") { Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin2)) }
        }
    }
}


@Composable
private fun TabletLayout(
    toolbarState: OctoToolbar.State,
    editing: Boolean,
    columCount: Int,
    onMove: (ItemPosition, ItemPosition) -> Unit,
    items: List<Pair<WidgetType, Boolean>>,
    toggleHidden: (WidgetType) -> Unit,
    content: @Composable (WidgetType, EditState) -> Unit
) {
    if (editing) {
        val state = rememberReorderableLazyGridState(onMove)

        HeaderSynchronizedLazyVerticalGrid(
            state = state.gridState,
            columnCount = columCount,
            octoActivity = OctoAppTheme.octoActivity,
            contentPadding = PaddingValues(),
            toolbarState = OctoToolbar.State.Hidden,
            modifier = Modifier
                .reorderable(state)
                .fillMaxSize(),
        ) {
            items(
                items = items,
                span = { GridItemSpan(1) },
                key = { it.first },
            ) { type ->
                ReorderableItem2(
                    reorderableState = state,
                    key = type.first,
                ) { dragging ->
                    val editState = EditState(
                        editing = true,
                        dragState = state,
                        dragging = dragging,
                        hidden = type.second,
                        toggleHidden = { toggleHidden(type.first) }
                    )

                    content(type.first, editState)
                }
            }
        }
    } else {
        val scrollState = rememberScrollState()
        val columns = List<MutableList<Pair<WidgetType, Boolean>>>(columCount) { mutableListOf() }
        var activeColumn = -1
        items.forEach {
            activeColumn = (activeColumn + 1) % columns.size
            columns[activeColumn].add(it)
        }


        HeaderSynchronizedScrollBox(
            octoActivity = OctoAppTheme.octoActivity,
            toolbarState = toolbarState,
            firstVisibleItemIndex = { 0 },
            firstVisibleItemScrollOffset = { scrollState.value },
            footerVisible = { (scrollState.maxValue - scrollState.value) < 20 }
        ) {
            Row(
                Modifier
                    .fillMaxSize()
                    .verticalScroll(scrollState)
                    .testTag("controls:scroller")
                    .padding(
                        top = OctoAppTheme.dimens.margin6,
                        bottom = OctoAppTheme.dimens.margin2,
                    )
            ) {
                columns.forEach { columnItems ->
                    Column(Modifier.weight(1f)) {
                        columnItems.forEach { type ->
                            val editState = EditState(
                                editing = false,
                                dragState = rememberReorderableLazyListState(onMove = { _, _ -> }),
                                dragging = false,
                                hidden = type.second,
                                toggleHidden = { toggleHidden(type.first) }
                            )

                            content(type.first, editState)
                        }
                    }
                }
            }
        }
    }
}