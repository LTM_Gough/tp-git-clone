package de.crysxd.baseui.common.controls

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.awaitFirstDown
import androidx.compose.foundation.gestures.forEachGesture
import androidx.compose.foundation.gestures.horizontalDrag
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.layout.positionInWindow
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.LocalOctoPrint
import de.crysxd.baseui.compose.framework.SmallPrimaryButton
import de.crysxd.baseui.compose.framework.TestTags
import de.crysxd.baseui.compose.framework.collectAsStateWhileActive
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.menu.base.MenuBottomSheetFragment
import de.crysxd.baseui.menu.main.MainMenu
import de.crysxd.baseui.menu.temperature.TemperatureMenu
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.job.ProgressInformation
import de.crysxd.octoapp.engine.models.printer.PrinterState

@Composable
fun ControlsBottomBarColumn(
    active: Boolean,
    content: @Composable ColumnScope.() -> Unit,
) = ControlsBottomBarSwipeConfirmation {
    Column {
        Column(Modifier.weight(1f)) {
            content()
        }

        val state = rememberControlsState(active = active)
        ControlsBottomBar(state = state)
    }
}

//region BottomBar
private val LocalOriginalLayoutDirection = compositionLocalOf { LayoutDirection.Ltr }
private val LocalSwipeConfirmation = compositionLocalOf<SwipeConfirmationScope> { throw IllegalStateException("Not available") }

@Composable
private fun rememberControlsState(active: Boolean): ControlsBottomBarState {
    val vmFactory = ControlsViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(factory = vmFactory, modelClass = ControlsViewModel::class.java, key = vmFactory.id)
    val state = vm.currentMessage.collectAsStateWhileActive(active = active, initial = null, key = "controls-current")
    val leftyMode = vm.leftyMode.collectAsStateWhileActive(active = active, initial = false, key = "controls-lefty")
    val fragmentManager = OctoAppTheme.fragmentManager

    return remember {
        object : ControlsBottomBarState {
            override val currentMessage by state
            override val leftyMode by leftyMode
            override fun startPrinting() {
                UriLibrary.getFileManagerUri().open()
            }

            override fun togglePausePrint() {
                vm.togglePausePrint()
            }

            override fun cancelPrint() {
                vm.cancelPrint()
            }

            override fun showMainMenu() {
                MenuBottomSheetFragment.createForMenu(
                    menu = MainMenu(),
                    currentDestination = if (state.value?.state?.flags?.isPrinting() == true) R.id.workspacePrint else R.id.workspacePrePrint
                ).show(fragmentManager())
            }

            override fun showTemperaturePresets() {
                MenuBottomSheetFragment.createForMenu(TemperatureMenu()).show(fragmentManager())
            }
        }
    }
}

interface ControlsBottomBarState {
    val currentMessage: Message.Current?
    val leftyMode: Boolean

    fun startPrinting()
    fun togglePausePrint()
    fun cancelPrint()
    fun showMainMenu()
    fun showTemperaturePresets()
}

@Composable
private fun SwipeConfirmationScope.ControlsBottomBar(state: ControlsBottomBarState) {
    CompositionLocalProvider(
        LocalOriginalLayoutDirection provides LocalLayoutDirection.current,
        LocalSwipeConfirmation provides this,
        LocalLayoutDirection provides if (state.leftyMode) LayoutDirection.Rtl else LayoutDirection.Ltr,
    ) {
        val flags = { state.currentMessage?.state?.flags }
        val current = { state.currentMessage }

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp)
                .background(OctoAppTheme.colors.windowBackground)
                .padding(start = OctoAppTheme.dimens.margin1)
        ) {
            StartPrint(flags = flags, onStartPrinting = state::startPrinting)
            ShowTemperaturePresets(flags = flags, onShowTemperaturePresets = state::showTemperaturePresets)
            StopPrinting(flags = flags, onStopPrinting = state::cancelPrint)
            StopPrintingDivider(flags = flags)
            PausePrinting(flags = flags, onPausePrinting = state::togglePausePrint)
            ResumePrinting(flags = flags, onResumePrinting = state::togglePausePrint)
            StateText(current = current)
            MenuButton(onClick = state::showMainMenu)
        }
    }
}

@Composable
private fun RowScope.StartPrint(
    flags: () -> PrinterState.Flags?,
    onStartPrinting: () -> Unit,
) = AnimatedVisibility(visible = flags()?.isPrinting() == false) {
    SmallPrimaryButton(
        text = stringResource(id = R.string.start_printing),
        onClick = onStartPrinting,
        modifier = Modifier
            .padding(end = OctoAppTheme.dimens.margin1)
            .testTag(TestTags.BottomBar.Start),
    )
}

@Composable
private fun RowScope.ShowTemperaturePresets(
    flags: () -> PrinterState.Flags?,
    onShowTemperaturePresets: () -> Unit,
) = AnimatedVisibility(visible = flags()?.isPrinting() == false) {
    BottomBarAction(
        icon = R.drawable.ic_round_local_fire_department_24,
        onClick = onShowTemperaturePresets,
        modifier = Modifier.padding(end = OctoAppTheme.dimens.margin1),
        description = R.string.temperature_menu___title,
        testTag = TestTags.BottomBar.Temperatures
    )
}


@Composable
private fun RowScope.StopPrinting(
    flags: () -> PrinterState.Flags?,
    onStopPrinting: () -> Unit,
) = AnimatedVisibility(visible = flags()?.run { isPrinting() && !cancelling } ?: false) {
    BottomBarAction(
        icon = R.drawable.ic_round_stop_24,
        onSwipe = onStopPrinting,
        modifier = Modifier.padding(end = OctoAppTheme.dimens.margin01),
        description = R.string.cancel_print,
        testTag = TestTags.BottomBar.Cancel
    )
}

@Composable
private fun RowScope.StopPrintingDivider(
    flags: () -> PrinterState.Flags?,
) = AnimatedVisibility(visible = flags()?.run { isPrinting() && !cancelling && !resuming && !pausing } ?: false) {
    Box(
        modifier = Modifier
            .padding(end = OctoAppTheme.dimens.margin01)
            .width(1.dp)
            .height(30.dp)
            .zIndex(100f)
            .background(OctoAppTheme.colors.inputBackground)
    )
}

@Composable
private fun RowScope.PausePrinting(
    flags: () -> PrinterState.Flags?,
    onPausePrinting: () -> Unit,
) = AnimatedVisibility(visible = flags()?.run { isPrinting() && !paused && !pausing && !cancelling && !resuming } ?: false) {
    BottomBarAction(
        icon = R.drawable.ic_round_pause_24,
        onSwipe = onPausePrinting,
        modifier = Modifier.padding(end = OctoAppTheme.dimens.margin1),
        description = R.string.pause,
        testTag = TestTags.BottomBar.Pause
    )
}

@Composable
private fun RowScope.ResumePrinting(
    flags: () -> PrinterState.Flags?,
    onResumePrinting: () -> Unit,
) = AnimatedVisibility(visible = flags()?.run { isPrinting() && !cancelling && !resuming && paused } ?: false) {
    BottomBarAction(
        icon = R.drawable.ic_round_play_arrow_24,
        onSwipe = onResumePrinting,
        modifier = Modifier.padding(end = OctoAppTheme.dimens.margin1),
        description = R.string.resume,
        testTag = TestTags.BottomBar.Resume
    )
}

@Composable
private fun MenuButton(onClick: () -> Unit) {
    val borderWidth = 1.dp
    val shape = RoundedCornerShape(
        topStartPercent = 50,
        bottomStartPercent = 50
    )

    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .offset(x = borderWidth)
            .padding(vertical = OctoAppTheme.dimens.margin01)
            .border(width = borderWidth, color = OctoAppTheme.colors.secondaryButtonText, shape = shape)
            .clip(shape)
            .background(OctoAppTheme.colors.inputBackground)
            .clickable(
                interactionSource = MutableInteractionSource(),
                indication = rememberRipple(color = OctoAppTheme.colors.secondaryButtonText),
                onClick = onClick
            )
            .fillMaxHeight()
            .testTag(TestTags.BottomBar.MainMenu)
            .padding(start = OctoAppTheme.dimens.margin1)
            .padding(horizontal = OctoAppTheme.dimens.margin1),
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_round_menu_24),
            contentDescription = stringResource(id = R.string.cd_main_menu),
            tint = OctoAppTheme.colors.secondaryButtonText,
        )
    }
}

@Composable
private fun RowScope.StateText(
    current: () -> Message.Current?,
) = Crossfade(
    targetState = current()?.run {
        val completion = progress?.completion
        when {
            state.flags.pausing -> stringResource(R.string.pausing)
            state.flags.cancelling -> stringResource(R.string.cancelling)
            state.flags.paused -> stringResource(R.string.paused)
            state.flags.printing && completion != null -> stringResource(R.string.x_percent, completion)
            else -> ""
        }
    } ?: "",
    modifier = Modifier
        .padding(end = OctoAppTheme.dimens.margin1)
        .weight(1f),
) {
    Box(modifier = Modifier.fillMaxWidth()) {
        CompositionLocalProvider(LocalLayoutDirection provides LocalOriginalLayoutDirection.current) {
            Text(
                text = it,
                style = OctoAppTheme.typography.subtitle,
                modifier = Modifier
                    .align(Alignment.CenterEnd)
                    .testTag(TestTags.BottomBar.Status)
            )
        }
    }
}

@Composable
private fun BottomBarAction(
    @DrawableRes icon: Int,
    modifier: Modifier = Modifier,
    @StringRes description: Int,
    onClick: (() -> Unit)? = null,
    onSwipe: (() -> Unit)? = null,
    testTag: String,
) = Box(
    modifier = if (onSwipe != null) {
        modifier.swipeConfirmed(icon = icon, description = description, onSwipe = onSwipe)
    } else {
        modifier.clickConfirmed(onClick = onClick ?: {})
    }.testTag(testTag)
) {
    CompositionLocalProvider(LocalLayoutDirection provides LocalOriginalLayoutDirection.current) {
        Icon(
            painter = painterResource(id = icon),
            contentDescription = stringResource(id = description),
            tint = OctoAppTheme.colors.accent,
            modifier = Modifier.padding(OctoAppTheme.dimens.margin1)
        )
    }
}

private fun Modifier.clickConfirmed(onClick: () -> Unit) = composed {
    clickable(
        onClick = onClick,
        indication = rememberRipple(color = OctoAppTheme.colors.accent, bounded = false),
        interactionSource = MutableInteractionSource()
    )
}

private fun Modifier.swipeConfirmed(@DrawableRes icon: Int, @StringRes description: Int, onSwipe: () -> Unit) = composed {
    val swipeScope = LocalSwipeConfirmation.current
    var centerInWindow by remember { mutableStateOf(Offset.Zero) }
    val usedLayoutDirection = LocalLayoutDirection.current
    val windowWidth = with(LocalDensity.current) { LocalConfiguration.current.screenWidthDp.dp.toPx() }

    onGloballyPositioned {
        // We assume that parent.x=0 and parent.y=windowWidth. If we use positionInWindow(), Ltr Rtl is not honored
        val centerOffset = Offset(x = it.size.width / 2f, y = it.size.height / 2f)
        val piw = it.positionInWindow()
        centerInWindow = if (usedLayoutDirection == LayoutDirection.Ltr) {
            piw + centerOffset
        } else {
            Offset(x = windowWidth - piw.x, y = piw.y) - centerOffset
        }
    }.pointerInput(Unit) {
        forEachGesture {
            while (true) {
                val pointerId = awaitPointerEventScope {
                    awaitFirstDown().id.also {
                        swipeScope.confirm(
                            centerInWindow = centerInWindow,
                            icon = icon,
                            then = onSwipe,
                            description = description,
                            layoutDirection = usedLayoutDirection
                        )
                    }
                }

                awaitPointerEventScope {
                    swipeScope.dragEventManager.onDragStart()
                    horizontalDrag(pointerId) { change -> swipeScope.dragEventManager.onDrag(change) }
                    swipeScope.dragEventManager.onDragEnd()
                }
            }
        }
    }
}
//endregion

//region Preview
@Composable
private fun BottomBarPreview(current: Message.Current?, leftyMode: Boolean) = OctoAppThemeForPreview {
    val state = object : ControlsBottomBarState {
        override val currentMessage = current
        override val leftyMode = leftyMode
        override fun startPrinting() = Unit
        override fun togglePausePrint() = Unit
        override fun cancelPrint() = Unit
        override fun showMainMenu() = Unit
        override fun showTemperaturePresets() = Unit
    }

    ControlsBottomBarSwipeConfirmation {
        Column {
            Spacer(modifier = Modifier.weight(1f))
            ControlsBottomBar(state = state)
        }
    }
}

@Composable
private fun BottomBarPreviewIdle(leftyMode: Boolean) = BottomBarPreview(
    leftyMode = leftyMode,
    current = Message.Current(
        serverTime = 0,
    )
)

@Composable
private fun BottomBarPreviewPrinting(leftyMode: Boolean) = BottomBarPreview(
    leftyMode = leftyMode,
    current = Message.Current(
        state = PrinterState.State(flags = PrinterState.Flags(printing = true)),
        progress = ProgressInformation(completion = 42.1f),
        serverTime = 0,
    )
)

@Composable
private fun BottomBarPreviewPausing(leftyMode: Boolean) = BottomBarPreview(
    leftyMode = leftyMode,
    current = Message.Current(
        state = PrinterState.State(flags = PrinterState.Flags(printing = true, pausing = true)),
        progress = ProgressInformation(completion = 42.1f),
        serverTime = 0,
    )
)

@Composable
private fun BottomBarPreviewPaused(leftyMode: Boolean) = BottomBarPreview(
    leftyMode = leftyMode,
    current = Message.Current(
        state = PrinterState.State(flags = PrinterState.Flags(printing = true, paused = true)),
        progress = ProgressInformation(completion = 42.1f),
        serverTime = 0,
    )
)

@Composable
private fun BottomBarPreviewCancelling(leftyMode: Boolean) = BottomBarPreview(
    leftyMode = leftyMode,
    current = Message.Current(
        state = PrinterState.State(flags = PrinterState.Flags(printing = true, cancelling = true)),
        progress = ProgressInformation(completion = 42.1f),
        serverTime = 0,
    )
)

@Composable
private fun BottomBarPreviewResuming(leftyMode: Boolean) = BottomBarPreview(
    leftyMode = leftyMode,
    current = Message.Current(
        state = PrinterState.State(flags = PrinterState.Flags(printing = true, resuming = true)),
        progress = ProgressInformation(completion = 42.1f),
        serverTime = 0,
    )
)

@Composable
private fun BottomBarInitial(leftyMode: Boolean) = BottomBarPreview(
    leftyMode = leftyMode,
    current = null
)

@Preview
@Composable
private fun BottomBarPreviewIdleLeft() = BottomBarPreviewIdle(leftyMode = true)

@Preview
@Composable
private fun BottomBarPreviewIdleRight() = BottomBarPreviewIdle(leftyMode = false)

@Preview
@Composable
private fun BottomBarPreviewPrintingLeft() = BottomBarPreviewPrinting(leftyMode = true)

@Preview
@Composable
private fun BottomBarPreviewPrintingRight() = BottomBarPreviewPrinting(leftyMode = false)

@Preview
@Composable
private fun BottomBarPreviewPausingLeft() = BottomBarPreviewPausing(leftyMode = true)

@Preview
@Composable
private fun BottomBarPreviewPausingRight() = BottomBarPreviewPausing(leftyMode = false)

@Preview
@Composable
private fun BottomBarPreviewPausedLeft() = BottomBarPreviewPaused(leftyMode = true)

@Preview
@Composable
private fun BottomBarPreviewPausedRight() = BottomBarPreviewPaused(leftyMode = false)

@Preview
@Composable
private fun BottomBarPreviewCancellingLeft() = BottomBarPreviewCancelling(leftyMode = true)

@Preview
@Composable
private fun BottomBarPreviewCancellingRight() = BottomBarPreviewCancelling(leftyMode = false)

@Preview
@Composable
private fun BottomBarPreviewResumingLeft() = BottomBarPreviewResuming(leftyMode = true)

@Preview
@Composable
private fun BottomBarPreviewResumingRight() = BottomBarPreviewResuming(leftyMode = false)

@Preview
@Composable
private fun BottomBarInitialLeft() = BottomBarInitial(leftyMode = true)

@Preview
@Composable
private fun BottomBarInitialRight() = BottomBarInitial(leftyMode = false)
//endregion