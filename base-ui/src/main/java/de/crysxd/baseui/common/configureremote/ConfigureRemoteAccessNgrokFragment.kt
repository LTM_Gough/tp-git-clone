package de.crysxd.baseui.common.configureremote

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import de.crysxd.baseui.R
import de.crysxd.baseui.databinding.ConfigureRemoteAccessNgrokFragmentBinding
import de.crysxd.baseui.di.injectParentViewModel
import de.crysxd.octoapp.engine.exceptions.RemoteServiceConnectionBrokenException.Companion.REMOTE_SERVICE_NGROK
import de.crysxd.octoapp.engine.framework.isNgrokUrl
import de.crysxd.octoapp.sharedcommon.http.framework.withoutBasicAuth

class ConfigureRemoteAccessNgrokFragment : Fragment() {

    private val viewModel by injectParentViewModel<ConfigureRemoteAccessViewModel>()
    private lateinit var binding: ConfigureRemoteAccessNgrokFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        ConfigureRemoteAccessNgrokFragmentBinding.inflate(inflater, container, false).also { binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.viewData.observe(viewLifecycleOwner) { data ->
            val ngrokConnected = data.remoteWebUrl?.isNgrokUrl() == true
            binding.ngrokUrl.text = data.remoteWebUrl.takeIf { it?.isNgrokUrl() == true }?.withoutBasicAuth()?.toString()
            binding.ngrokConnected.isVisible = ngrokConnected
            binding.ngrokUrl.isVisible = ngrokConnected
            binding.description1.isVisible = !ngrokConnected
            binding.description2.isVisible = !ngrokConnected
            binding.usps.isVisible = !ngrokConnected
            binding.failure.bind(data.failure?.takeIf { it.remoteServiceName == REMOTE_SERVICE_NGROK })
        }

        createUsps()
    }

    private fun createUsps() {
        val background = Color.parseColor("#F1F4F6")

        binding.failure.setColors(
            textColorInt = ContextCompat.getColor(requireContext(), R.color.black),
            backgroundColorInt = background,
        )

        binding.usps.configure(
            iconColorInt = ContextCompat.getColor(requireContext(), R.color.light_grey),
            textColorInt = ContextCompat.getColor(requireContext(), R.color.black),
            backgroundColorInt = background
        )
        binding.usps.addUsp(
            iconRes = R.drawable.ic_round_network_check_24,
            description = getString(R.string.configure_remote_acces___ngrok___usp_1)
        )
        binding.usps.addUsp(
            iconRes = R.drawable.ic_round_warning_amber_24,
            description = getString(R.string.configure_remote_acces___ngrok___usp_2)
        )
        binding.usps.addUsp(
            iconRes = R.drawable.ic_round_auto_fix_high_24,
            description = getString(R.string.configure_remote_acces___ngrok___usp_3)
        )
        binding.usps.addUsp(
            iconRes = R.drawable.ic_round_lock_24,
            description = getString(R.string.configure_remote_acces___ngrok___usp_4)
        )
    }
}