package de.crysxd.baseui.common.feedback

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.BaseViewModel
import de.crysxd.baseui.usecase.OpenEmailClientForFeedbackUseCase
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import kotlinx.coroutines.launch

class SendFeedbackViewModel(
    private val createBugReportUseCase: CreateBugReportUseCase,
    private val sendFeedbackUseCase: OpenEmailClientForFeedbackUseCase
) : BaseViewModel() {

    val viewState = MutableLiveData<ViewState>(ViewState.Idle)

    fun sendFeedback(
        context: Context,
        message: String,
        sendPhoneInfo: Boolean,
        sendOctoPrintInfo: Boolean,
        sendLogs: Boolean,
    ) = viewModelScope.launch(coroutineExceptionHandler) {
        viewState.postValue(ViewState.Loading)

        val bugReport = if (sendPhoneInfo || sendOctoPrintInfo || sendLogs) {
            createBugReportUseCase.execute(
                CreateBugReportUseCase.Params(
                    sendPhoneInfo = sendPhoneInfo,
                    sendOctoPrintInfo = sendOctoPrintInfo,
                    sendLogs = sendLogs,
                )
            )
        } else {
            null
        }

        sendFeedbackUseCase.execute(
            OpenEmailClientForFeedbackUseCase.Params(
                message = message,
                context = context,
                bugReport = bugReport
            )
        )
    }.invokeOnCompletion {
        viewState.postValue(ViewState.Done)
    }

    sealed class ViewState {
        object Idle : ViewState()
        object Loading : ViewState()
        object Done : ViewState()
    }
}