package de.crysxd.baseui.common.gcode

import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.BaseViewModel
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_GCODE_PREVIEW
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.data.repository.GcodeFileRepository
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.data.source.GcodeFileDataSource
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.base.gcode.render.GcodeRenderContextFactory
import de.crysxd.octoapp.base.gcode.render.models.GcodeRenderContext
import de.crysxd.octoapp.base.gcode.render.models.RenderStyle
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.GenerateRenderStyleUseCase
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import timber.log.Timber

@Suppress("EXPERIMENTAL_API_USAGE")
class GcodePreviewViewModel(
    octoPrintProvider: OctoPrintProvider,
    octoPrintRepository: OctoPrintRepository,
    octoPreferences: OctoPreferences,
    generateRenderStyleUseCase: GenerateRenderStyleUseCase,
    private val gcodeFileRepository: GcodeFileRepository
) : BaseViewModel() {

    init {
        Timber.i("New instance")
    }

    private var filePendingToLoad: FileObject.File? = null
    private val gcodeFlow = MutableStateFlow<Flow<GcodeFileDataSource.LoadState>?>(emptyFlow())
    private val contextFactoryFlow = MutableStateFlow<(Message.Current) -> Pair<GcodeRenderContextFactory, Boolean>?> { null }
    private val manualViewStateFlow = MutableStateFlow<ViewState>(ViewState.Loading())
    private val renderStyleFlow = octoPrintRepository.instanceInformationFlow().combine(octoPreferences.updatedFlow) { instance, _ ->
        generateRenderStyleUseCase.execute(instance)
    }

    private val printerProfileFlow = octoPrintRepository.instanceInformationFlow().map {
        it?.activeProfile ?: PrinterProfile()
    }

    private val featureEnabledFlow: Flow<Boolean> = BillingManager.billingFlow().map {
        val enabled = isFeatureEnabled()
        Timber.i("Feature enabled: $enabled")
        enabled
    }.onEach {
        if (it) {
            filePendingToLoad?.let { file ->
                filePendingToLoad = null
                downloadGcode(file, true)
            }
        }
    }

    val activeFile = octoPrintProvider.passiveCurrentMessageFlow("gcode_preview_2").mapNotNull {
        it.job?.file ?: return@mapNotNull null
    }.distinctUntilChangedBy { it.path }.asLiveData()

    @OptIn(ExperimentalCoroutinesApi::class)
    private val renderContextFlow: Flow<ViewState> = gcodeFlow.filterNotNull().flatMapLatest { it }
        .combine(octoPrintProvider.passiveCurrentMessageFlow("gcode_preview_1").rateLimit(1000)) { gcodeState, currentMessage ->
            Pair(gcodeState, currentMessage)
        }.combine(contextFactoryFlow) { pair, factory ->
            val (gcodeState, currentMessage) = pair
            val settings = octoPreferences.gcodePreviewSettings

            when (gcodeState) {
                is GcodeFileDataSource.LoadState.Loading -> ViewState.Loading(gcodeState.progress)
                is GcodeFileDataSource.LoadState.FailedLargeFileDownloadRequired -> ViewState.LargeFileDownloadRequired
                is GcodeFileDataSource.LoadState.Failed -> ViewState.Error(gcodeState.exception)
                is GcodeFileDataSource.LoadState.Ready -> {
                    factory(currentMessage)?.let {
                        val (factoryInstance, fromUser) = it
                        ViewState.DataReady(
                            renderContext = factoryInstance.extractMoves(
                                gcode = gcodeState.gcode,
                                includePreviousLayer = settings.showPreviousLayer,
                                includeRemainingCurrentLayer = settings.showCurrentLayer,
                            ),
                            fromUser = fromUser,
                            settings = settings
                        )
                    } ?: ViewState.Loading(1f)
                }
            }
        }.distinctUntilChanged()

    private val internalViewState: Flow<ViewState> =
        combine(featureEnabledFlow, renderContextFlow, renderStyleFlow, printerProfileFlow) { featureEnabled, s1, renderStyle, printerProfile ->
            when {
                !featureEnabled -> ViewState.FeatureDisabled(renderStyle)
                s1 is ViewState.DataReady -> s1.copy(
                    renderStyle = renderStyle,
                    printerProfile = printerProfile,
                    exceedsPrintArea = exceedsPrintArea(printerProfile, s1.renderContext),
                )
                else -> s1
            }
        }.retry(3) {
            Timber.e(it)
            delay(500L)
            true
        }.catch { e ->
            emit(ViewState.Error(e))
        }

    val viewState = merge(manualViewStateFlow, internalViewState)
        .asLiveData(viewModelScope.coroutineContext)

    private fun exceedsPrintArea(
        printerProfile: PrinterProfile?,
        context: GcodeRenderContext?,
    ): Boolean {
        context ?: return false
        printerProfile ?: return false
        val bounds = context.gcodeBounds
        bounds ?: return false
        val w = printerProfile.volume.width
        val h = printerProfile.volume.depth

        // Give 5% slack to make sure we don't count homing positions as out of bounds
        val graceDistance = minOf(w, h) * 0.05f

        val minX: Float
        val minY: Float
        val maxX: Float
        val maxY: Float

        when (printerProfile.volume.origin) {
            PrinterProfile.Origin.LowerLeft -> {
                minX = -graceDistance
                maxX = w + graceDistance
                maxY = h + graceDistance
                minY = -graceDistance
            }
            PrinterProfile.Origin.Center -> {
                maxX = (w / 2) + graceDistance
                minX = -maxX
                maxY = (h / 2) + graceDistance
                minY = -maxY
            }
        }

        return bounds.left < minX || bounds.top > maxY || bounds.right > maxX || bounds.bottom < minY
    }

    private fun isFeatureEnabled() = BillingManager.isFeatureEnabled(FEATURE_GCODE_PREVIEW)

    fun downloadGcode(file: FileObject.File, allowLargeFileDownloads: Boolean) = viewModelScope.launch(coroutineExceptionHandler) {
        Timber.i("Download file: ${file.path}")
        gcodeFlow.value = flowOf(GcodeFileDataSource.LoadState.Loading(0f))

        if (isFeatureEnabled()) {
            gcodeFlow.value = gcodeFileRepository.loadFile(file, allowLargeFileDownloads)
        } else {
            // Feature currently disabled. Store the file to be loaded once the feature got enabled.
            filePendingToLoad = file
        }
    }

    fun useLiveProgress() {
        contextFactoryFlow.value = {
            Pair(
                GcodeRenderContextFactory.ForFileLocation(it.progress?.filepos?.toInt() ?: Int.MAX_VALUE),
                false
            )
        }
    }

    fun useManualProgress(layer: Int, progress: Float) {
        contextFactoryFlow.value = {
            Pair(
                GcodeRenderContextFactory.ForLayerProgress(layerIndex = layer, progress = progress),
                true
            )
        }
    }

    sealed class ViewState {
        data class Loading(val progress: Float = 0f) : ViewState()
        data class FeatureDisabled(val renderStyle: RenderStyle) : ViewState()
        object LargeFileDownloadRequired : ViewState()
        data class Error(val exception: Throwable) : ViewState()
        data class DataReady(
            val renderStyle: RenderStyle? = null,
            val renderContext: GcodeRenderContext? = null,
            val printerProfile: PrinterProfile? = null,
            val fromUser: Boolean? = null,
            val exceedsPrintArea: Boolean? = null,
            val settings: GcodePreviewSettings,
        ) : ViewState()
    }
}