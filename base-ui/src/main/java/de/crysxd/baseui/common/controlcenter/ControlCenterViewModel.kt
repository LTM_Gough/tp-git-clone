package de.crysxd.baseui.common.controlcenter

import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.BaseViewModel
import de.crysxd.baseui.databinding.ControlCenterFragmentBinding
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.ext.filterEventsForMessageType
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.GetWebcamSnapshotUseCase
import de.crysxd.octoapp.engine.EventSource
import de.crysxd.octoapp.engine.models.event.Message
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import timber.log.Timber

class ControlCenterViewModel(
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPrintProvider: OctoPrintProvider,
    private val getWebcamSnapshotUseCase: GetWebcamSnapshotUseCase,
) : BaseViewModel() {

    var viewPool: ControlCenterFragmentBinding? = null
    var viewPoolDark: Boolean? = null

    private var loadCurrentMessagesJob: Job? = null
    private var loadSnapshotsJob: Job? = null
    private val currentMessageFlow = MutableStateFlow<Map<String, Message.Current?>>(emptyMap())
    private val snapshotsFlow = MutableStateFlow<Map<String, GetWebcamSnapshotUseCase.Snapshot?>>(emptyMap())

    val viewState = octoPrintRepository.instanceInformationFlow().combine(octoPrintRepository.allInstanceInformationFlow()) { active, all ->
        InstanceList(
            activeId = active?.id,
            instances = all.values.map { Instance(info = it) }
        )
    }.combine(snapshotsFlow) { data, snapshots ->
        data.copy(instances = data.instances.map { i -> i.copy(snapshot = snapshots[i.info.id]) })
    }.combine(currentMessageFlow) { data, messages ->
        data.copy(instances = data.instances.map { i -> i.copy(lastMessage = messages[i.info.id]) })
    }.onStart {
        loadCurrentMessagesJob?.cancel()
        loadCurrentMessagesJob = loadCurrentMessages()
        loadSnapshotsJob?.cancel()
        loadSnapshotsJob = loadWebcamSnapshots()
        delay(300)
    }.onCompletion {
        loadSnapshotsJob?.cancel()
        loadCurrentMessagesJob?.cancel()
    }.rateLimit(500).asLiveData()


    private fun getCurrentMessages(instanceId: String) = octoPrintProvider.eventFlow(
        tag = "control-center",
        instanceId = instanceId,
        config = EventSource.Config(throttle = 5)
    ).filterEventsForMessageType<Message.Current>().let {
        flow {
            emit(instanceId to null)
            emitAll(it.map { i -> instanceId to i })
        }
    }

    private fun getSnapshot(instanceId: String) = flow {
        emit(instanceId to null)
        val bitmap = getWebcamSnapshotUseCase.execute(
            GetWebcamSnapshotUseCase.Params(
                illuminateIfPossible = true,
                instanceId = instanceId,
                interval = 3_000,
                maxSize = 200,
            )
        ).map {
            instanceId to it
        }
        emitAll(bitmap)
    }.catch {
        Timber.w("Unable to load snapshot for $instanceId: ${it::class.simpleName}: ${it.message}")
    }

    private fun loadWebcamSnapshots(): Job {
        val flows = octoPrintRepository.getAll().map { instance -> getSnapshot(instance.id) }
        val combined = combine(flows) { it.toMap() }
        return viewModelScope.launch {
            combined.onEach {
                snapshotsFlow.emit(it)
            }.onStart {
                Timber.i("Starting to load snapshots")
            }.onCompletion {
                Timber.i("Stopping to load snapshots")
            }.catch {
                Timber.e(it)
            }.collect()
        }
    }

    private fun loadCurrentMessages(): Job {
        val flows = octoPrintRepository.getAll().map { instance -> getCurrentMessages(instance.id) }
        val combined = combine(flows) { it.toMap() }

        // We load the current messages on the viewModelScope to keep the flows active while switching the instance
        // This is important to prevent the websocket from reconnecting because we are not observing events briefly
        return viewModelScope.launch {
            combined.onEach {
                currentMessageFlow.emit(it)
            }.onStart {
                Timber.i("Starting to read current messages")
            }.onCompletion {
                Timber.i("Stopping to read current messages")
            }.retry {
                delay(1000)
                Timber.e(it)
                true
            }.collect()
        }
    }

    fun active(instance: Instance) {
        octoPrintRepository.setActive(instance.info, trigger = "ControlCenter")
    }

    data class InstanceList(
        val instances: List<Instance>,
        val activeId: String?,
    )

    data class Instance(
        val info: OctoPrintInstanceInformationV3,
        val lastMessage: Message.Current? = null,
        val snapshot: GetWebcamSnapshotUseCase.Snapshot? = null,
    )
}