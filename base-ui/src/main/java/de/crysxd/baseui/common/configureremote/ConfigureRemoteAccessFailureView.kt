package de.crysxd.baseui.common.configureremote

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import androidx.annotation.ColorInt
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.databinding.ConfigureRemoteAccessFailureViewBinding
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.octoapp.base.data.models.RemoteConnectionFailure
import timber.log.Timber
import java.text.DateFormat

class ConfigureRemoteAccessFailureView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : LinearLayoutCompat(context, attrs) {

    private val binding = ConfigureRemoteAccessFailureViewBinding.inflate(LayoutInflater.from(context), this)

    init {
        setBackgroundResource(R.drawable.bg_input_no_padding)
        background?.setTint(ContextCompat.getColor(context, R.color.input_background_alternative))
        gravity = Gravity.CENTER_VERTICAL
        orientation = HORIZONTAL
    }

    fun setColors(
        @ColorInt textColorInt: Int,
        @ColorInt backgroundColorInt: Int,
    ) {
        background?.setTint(backgroundColorInt)
        binding.text.setTextColor(textColorInt)
    }

    fun bind(failure: RemoteConnectionFailure?) {
        isVisible = failure != null
        val date = failure?.dateMillis?.let { DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(it) } ?: ""
        binding.text.text = context.getString(R.string.configure_remote_access___failure, date)
        binding.button.setOnClickListener {
            failure ?: return@setOnClickListener
            Timber.i("Showing error for stack: ${failure.errorMessageStack}")
            findFragment<Fragment>().requireOctoActivity().showDialog(
                message = OctoActivity.Message.DialogMessage(
                    text = { failure.errorMessage },
                    neutralButton = { getString(R.string.show_details) },
                    neutralAction = {
                        findFragment<Fragment>().requireOctoActivity().showDialog(
                            message = OctoActivity.Message.DialogMessage(
                                text = { failure.errorMessageStack },
                            )
                        )
                    }
                )
            )
        }
    }
}