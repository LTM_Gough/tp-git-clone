package de.crysxd.baseui.common.configureremote

import android.content.Context
import android.text.InputType
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asFlow
import androidx.lifecycle.asLiveData
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.BaseViewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.common.enter_value.EnterValueFragment
import de.crysxd.baseui.common.enter_value.EnterValueFragmentArgs
import de.crysxd.baseui.utils.NavigationResultMediator
import de.crysxd.octoapp.base.data.models.OctoEverywhereConnection
import de.crysxd.octoapp.base.data.models.RemoteConnectionFailure
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.base.usecase.SetAlternativeWebUrlUseCase
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import io.ktor.http.Url
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.parcelize.Parcelize
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull

class ConfigureRemoteAccessViewModel(
    private val octoPrintRepository: OctoPrintRepository,
    private val setAlternativeWebUrlUseCase: SetAlternativeWebUrlUseCase,
    private val getRemoteServiceConnectUrlUseCase: GetRemoteServiceConnectUrlUseCase,
) : BaseViewModel() {

    private val mutableViewState = MutableLiveData<ViewState>()
    val viewState = mutableViewState.map { it }

    val viewData: LiveData<ViewData>

    private val mutableViewEvents = MutableLiveData<ViewEvent>()
    val viewEvents = mutableViewEvents.map { it }

    init {
        var lastRemoteUrl: Url? = null
        var first = true
        viewData = octoPrintRepository.instanceInformationFlow().map {
            // We did not have a octoeverywhere connection before but now we have one -> Freshly connected. Show success.
            if (it?.alternativeWebUrl != lastRemoteUrl && !first) {
                mutableViewEvents.postValue(ViewEvent.Success())
            }
            first = false
            lastRemoteUrl = it?.alternativeWebUrl

            ViewData(
                remoteWebUrl = it?.alternativeWebUrl,
                octoEverywhereConnection = it?.octoEverywhereConnection,
                failure = it?.remoteConnectionFailure
            )
        }.asLiveData()
    }

    fun getOctoEverywhereAppPortalUrl() = getRemoteServiceSetupUrl(GetRemoteServiceConnectUrlUseCase.RemoteService.OctoEverywhere)

    fun getSpaghettiDetectiveSetupUrl(customInstance: Boolean = false) = viewModelScope.launch(coroutineExceptionHandler) {
        val params = if (customInstance) {
            val (id, result) = NavigationResultMediator.registerResultCallback<String>()
            val context = BaseInjector.get().localizedContext()
            navContoller.navigate(
                R.id.action_enter_value,
                EnterValueFragmentArgs(
                    title = context.getString(R.string.configure_remote_acces___spaghetti_detective___custom_title),
                    action = context.getString(R.string.configure_remote_acces___spaghetti_detective___custom_action),
                    resultId = id,
                    hint = context.getString(R.string.configure_remote_acces___spaghetti_detective___custom_hint),
                    validator = UrlValidator(context.getString(R.string.configure_remote_acces___spaghetti_detective___custom_error)),
                    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_URI,
                    selectAll = true
                ).toBundle()
            )
            val url = result.asFlow().first() ?: return@launch
            GetRemoteServiceConnectUrlUseCase.RemoteService.SpaghettiDetective(url.toUrl())
        } else {
            GetRemoteServiceConnectUrlUseCase.RemoteService.SpaghettiDetective(null)
        }

        getRemoteServiceSetupUrl(params)
    }

    private fun getRemoteServiceSetupUrl(service: GetRemoteServiceConnectUrlUseCase.RemoteService) = viewModelScope.launch(coroutineExceptionHandler) {
        try {
            mutableViewState.postValue(ViewState.Loading)
            val params = GetRemoteServiceConnectUrlUseCase.Params(
                remoteService = service,
                instanceId = requireNotNull(octoPrintRepository.getActiveInstanceSnapshot()?.id)
            )
            val event = when (val result = getRemoteServiceConnectUrlUseCase.execute(params)) {
                is GetRemoteServiceConnectUrlUseCase.Result.Success -> ViewEvent.OpenUrl(result.url)
                is GetRemoteServiceConnectUrlUseCase.Result.Error -> ViewEvent.ShowError(
                    message = result.errorMessage,
                    exception = result.exception,
                    ignoreAction = null
                )
            }
            mutableViewEvents.postValue(event)
        } finally {
            mutableViewState.postValue(ViewState.Idle)
        }
    }

    fun setRemoteUrl(url: String, username: String, password: String, bypassChecks: Boolean) {
        viewModelScope.launch(coroutineExceptionHandler) {
            mutableViewState.postValue(ViewState.Loading)
            val params = SetAlternativeWebUrlUseCase.Params(
                webUrl = url,
                password = password,
                username = username,
                bypassChecks = bypassChecks,
                instanceId = requireNotNull(octoPrintRepository.getActiveInstanceSnapshot()?.id)
            )
            val event = when (val result = setAlternativeWebUrlUseCase.execute(params)) {
                SetAlternativeWebUrlUseCase.Result.Success -> ViewEvent.Success()
                is SetAlternativeWebUrlUseCase.Result.Failure -> ViewEvent.ShowError(
                    message = result.errorMessage,
                    exception = result.exception,
                    ignoreAction = if (result.allowToProceed) {
                        { setRemoteUrl(url = url, username = username, password = password, bypassChecks = true) }
                    } else {
                        null
                    }
                )
            }
            mutableViewEvents.postValue(event)
            mutableViewState.postValue(ViewState.Idle)
        }
    }

    sealed class ViewEvent {
        var consumed = false

        data class ShowError(
            val message: String,
            val exception: Exception,
            val ignoreAction: (() -> Unit)?
        ) : ViewEvent()

        class Success : ViewEvent()

        data class OpenUrl(val url: String) : ViewEvent()
    }

    sealed class ViewState {
        object Idle : ViewState()
        object Loading : ViewState()
    }

    data class ViewData(val remoteWebUrl: Url?, val octoEverywhereConnection: OctoEverywhereConnection?, val failure: RemoteConnectionFailure?)

    @Parcelize
    private class UrlValidator(private val error: String) : EnterValueFragment.ValueValidator {
        override fun validate(context: Context, value: String) = if (value.toHttpUrlOrNull() == null) error else null
    }
}

