package de.crysxd.baseui.common.controls

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.fragment.app.Fragment
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.fragment.findNavController
import de.crysxd.baseui.compose.framework.ComposeContent
import de.crysxd.baseui.compose.framework.LocalOctoPrint
import de.crysxd.baseui.compose.framework.collectAsStateWhileActive
import de.crysxd.octoapp.base.data.models.WidgetPreferences
import de.crysxd.octoapp.base.data.models.WidgetType
import de.crysxd.octoapp.base.data.repository.WidgetPreferencesRepository.Companion.LIST_CONNECT
import de.crysxd.octoapp.base.data.repository.WidgetPreferencesRepository.Companion.LIST_PREPARE
import de.crysxd.octoapp.base.data.repository.WidgetPreferencesRepository.Companion.LIST_PRINT
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.engine.models.printer.PrinterState
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ControlsFragment : Fragment() {

    private val isKeepScreenOn get() = BaseInjector.get().octoPreferences().isKeepScreenOnDuringPrint
    private var active by mutableStateOf(false)
    private var editing by mutableStateOf(false)
    private var editedTypes: List<Pair<WidgetType, Boolean>>? = null
    private var activeDestinationId: String? = null
    private var isPrinting = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Give a minimal delay to build the view and have a smooth transition
        postponeEnterTransition(50, TimeUnit.MILLISECONDS)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = ComposeContent(instanceId = null) {
        val vmFactory = ControlsViewModel.Factory(LocalOctoPrint.current.id)
        val vm = viewModel(factory = vmFactory, modelClass = ControlsViewModel::class.java, key = vmFactory.id)
        val flags by vm.flags.collectAsStateWhileActive(active = active, initial = PrinterState.Flags(), key = "controls-flags")
        val destinationId = flags.asDestinationId()
        isPrinting = flags.isPrinting()
        activeDestinationId = destinationId
        val preferences = remember(destinationId) {
            BaseInjector.get().widgetPreferencesRepository().getWidgetOrderFlow(destinationId)
        }.collectAsState().value ?: WidgetPreferences(destinationId, emptyList())

        LaunchedEffect(destinationId) {
            // Force end editing when destination is changed
            editing = false
        }

        ControlsView(
            editing = editing,
            onStopEditing = { editing = false },
            onTypesChanged = { editedTypes = it },
            preferences = preferences,
            active = active,
            flags = flags,
            navController = findNavController(),
            controlsFragment = this,
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        BaseInjector.get().octoPreferences().updatedFlow.asLiveData().observe(viewLifecycleOwner) {
            updateKeepScreenOn()
        }
    }

    private fun PrinterState.Flags.asDestinationId() = when {
        isPrinting() -> LIST_PRINT
        isOperational() -> LIST_PREPARE
        else -> LIST_CONNECT
    }

    private fun updateKeepScreenOn() {
        if (isKeepScreenOn) {
            Timber.i("Keeping screen on")
            requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        } else {
            Timber.i("Not keeping screen on")
            requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }
    }

    override fun onResume() {
        super.onResume()
        active = true
    }

    override fun onPause() {
        super.onPause()
        active = false
    }

    override fun onStart() {
        super.onStart()
        updateKeepScreenOn()
    }

    override fun onStop() {
        super.onStop()
        requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    fun startEdit() {
        editing = true
    }
}