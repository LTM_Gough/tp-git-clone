package de.crysxd.baseui.di

import android.content.Context
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.map
import com.squareup.picasso.Downloader
import com.squareup.picasso.LruCache
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import de.crysxd.octoapp.base.logging.TimberLogger
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.utils.LoggingInterceptorLogger
import de.crysxd.octoapp.engine.PrinterEngine
import de.crysxd.octoapp.engine.framework.urlFromGivenValue
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.statement.bodyAsChannel
import io.ktor.http.contentType
import io.ktor.utils.io.jvm.javaio.toInputStream
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody.Companion.asResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import okio.buffer
import okio.source
import timber.log.Timber
import java.util.logging.Logger
import com.squareup.picasso.Cache as PicassoCache
import okhttp3.Cache as OkHttpCache

@Module
class PicassoModule {

    @Provides
    @BaseUiScope
    fun providePicassoCache(): PicassoCache = LruCache(10 * 1024 * 1024)

    @Provides
    @BaseUiScope
    fun providePublicPicasso(
        context: Context,
        okHttpCache: OkHttpCache,
        picassoCache: PicassoCache,
    ): Picasso = OkHttpClient.Builder()
        .cache(okHttpCache)
        .addNetworkInterceptor(
            HttpLoggingInterceptor(
                LoggingInterceptorLogger(TimberLogger(Logger.getLogger("Picasso/HTTP")).logger)
            ).setLevel(HttpLoggingInterceptor.Level.HEADERS)
        )
        .build()
        .let { okHttp ->
            Picasso.Builder(context)
                .memoryCache(picassoCache)
                .downloader(OkHttp3Downloader(okHttp))
                .build()
        }


    @Provides
    @BaseUiScope
    fun providePicasso(
        context: Context,
        octoPrintProvider: OctoPrintProvider,
        picassoCache: PicassoCache,
    ): LiveData<Picasso?> = octoPrintProvider.octoPrintFlow().asLiveData().map {
        it?.let { octoPrint ->
            Picasso.Builder(context)
                .downloader(PrinterEngineDownloader(octoPrint))
                .memoryCache(picassoCache)
                .requestTransformer { request ->
                    if (request.uri.scheme == "file") {
                        request
                    } else {
                        request.uri?.let { uri ->
                            // Real host and protocol comes from the downloader. We only need the path
                            val newUri = uri.buildUpon()
                                .scheme("http")
                                .authority("mock")
                                .build()
                            Timber.d("Mapping $uri -> $newUri")
                            request.buildUpon()
                                .setUri(Uri.parse(newUri.toString()))
                                .build()
                        } ?: request
                    }
                }.build()
        }
    }

    private class PrinterEngineDownloader(private val engine: PrinterEngine) : Downloader {
        override fun load(request: Request): Response = runBlocking {
            val response = engine.genericRequest { baseUrl, httpClient ->
                httpClient.get {
                    urlFromGivenValue(baseUrl = baseUrl, request.url.toString())
                    request.url.queryParameterNames.forEach {
                        parameter(it, request.url.queryParameter(it) ?: "")
                    }
                }
            }
            val body = response.bodyAsChannel().toInputStream().source().buffer().asResponseBody(contentType = response.contentType()?.toString()?.toMediaTypeOrNull())

            Response.Builder()
                .body(body)
                .request(request)
                .protocol(Protocol.HTTP_1_0)
                .message("OK")
                .code(response.status.value)
                .header("Cache-Control", "public;max-age=31536000")
                .also {
                    response.headers.forEach { key, values ->
                        values.forEach { value ->
                            it.header(key, value)
                        }
                    }
                }
                .build()
        }

        override fun shutdown() = Unit
    }
}