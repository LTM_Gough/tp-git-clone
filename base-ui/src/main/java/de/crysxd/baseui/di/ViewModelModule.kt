package de.crysxd.baseui.di

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import de.crysxd.baseui.BaseViewModelFactory
import de.crysxd.baseui.common.NetworkStateViewModel
import de.crysxd.baseui.common.configureremote.ConfigureRemoteAccessSpaghettiDetectiveViewModel
import de.crysxd.baseui.common.configureremote.ConfigureRemoteAccessViewModel
import de.crysxd.baseui.common.controlcenter.ControlCenterViewModel
import de.crysxd.baseui.common.enter_value.EnterValueViewModel
import de.crysxd.baseui.common.feedback.SendFeedbackViewModel
import de.crysxd.baseui.common.gcode.GcodePreviewViewModel
import de.crysxd.baseui.common.terminal.TerminalViewModel
import de.crysxd.baseui.menu.base.MenuBottomSheetViewModel
import de.crysxd.baseui.purchase.PurchaseViewModel
import de.crysxd.baseui.timelapse.TimelapseArchiveViewModel
import de.crysxd.baseui.timelapse.TimelapsePlaybackViewModel
import de.crysxd.baseui.usecase.OpenEmailClientForFeedbackUseCase
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.GcodeFileRepository
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.data.repository.SerialCommunicationLogsRepository
import de.crysxd.octoapp.base.data.repository.TimelapseRepository
import de.crysxd.octoapp.base.di.ViewModelKey
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import de.crysxd.octoapp.base.usecase.ExecuteGcodeCommandUseCase
import de.crysxd.octoapp.base.usecase.GenerateRenderStyleUseCase
import de.crysxd.octoapp.base.usecase.GetGcodeShortcutsUseCase
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.base.usecase.GetTerminalFiltersUseCase
import de.crysxd.octoapp.base.usecase.GetWebcamSnapshotUseCase
import de.crysxd.octoapp.base.usecase.SetAlternativeWebUrlUseCase
import javax.inject.Provider

@Module
open class ViewModelModule {

    @Provides
    fun bindViewModelFactory(creators: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>): BaseViewModelFactory =
        BaseViewModelFactory(creators)

    @Provides
    @IntoMap
    @ViewModelKey(EnterValueViewModel::class)
    open fun provideEnterValueViewModel(): ViewModel = EnterValueViewModel()

    @Provides
    @IntoMap
    @ViewModelKey(SendFeedbackViewModel::class)
    open fun provideSendFeedbackViewModel(
        sendUseCase: OpenEmailClientForFeedbackUseCase,
        bugReportUseCase: CreateBugReportUseCase,
    ): ViewModel = SendFeedbackViewModel(
        createBugReportUseCase = bugReportUseCase,
        sendFeedbackUseCase = sendUseCase
    )

    @Provides
    @IntoMap
    @ViewModelKey(TerminalViewModel::class)
    open fun provideTerminalViewModel(
        getGcodeShortcutsUseCase: GetGcodeShortcutsUseCase,
        executeGcodeCommandUseCase: ExecuteGcodeCommandUseCase,
        serialCommunicationLogsRepository: SerialCommunicationLogsRepository,
        getTerminalFiltersUseCase: GetTerminalFiltersUseCase,
        octoPrintProvider: OctoPrintProvider,
        octoPrintRepository: OctoPrintRepository,
        octoPreferences: OctoPreferences,
    ): ViewModel = TerminalViewModel(
        getGcodeShortcutsUseCase,
        executeGcodeCommandUseCase,
        serialCommunicationLogsRepository,
        getTerminalFiltersUseCase,
        octoPrintProvider,
        octoPrintRepository,
        octoPreferences,
    )

    @Provides
    @IntoMap
    @ViewModelKey(GcodePreviewViewModel::class)
    open fun provideGcodePreviewViewModel(
        octoPrintProvider: OctoPrintProvider,
        octoPrintRepository: OctoPrintRepository,
        generateRenderStyleUseCase: GenerateRenderStyleUseCase,
        gcodeFileRepository: GcodeFileRepository,
        octoPreferences: OctoPreferences,
    ): ViewModel = GcodePreviewViewModel(
        octoPrintRepository = octoPrintRepository,
        octoPrintProvider = octoPrintProvider,
        generateRenderStyleUseCase = generateRenderStyleUseCase,
        gcodeFileRepository = gcodeFileRepository,
        octoPreferences = octoPreferences,
    )

    @Provides
    @IntoMap
    @ViewModelKey(PurchaseViewModel::class)
    open fun providePurchaseViewModel(
    ): ViewModel = PurchaseViewModel(
    )

    @Provides
    @IntoMap
    @ViewModelKey(NetworkStateViewModel::class)
    open fun provideNetworkStateViewModel(
        application: Application
    ): ViewModel = NetworkStateViewModel(
        application = application
    )

    @Provides
    @IntoMap
    @ViewModelKey(MenuBottomSheetViewModel::class)
    open fun provideMenuBottomSheetViewModel(): ViewModel = MenuBottomSheetViewModel()

    @Provides
    @IntoMap
    @ViewModelKey(ConfigureRemoteAccessViewModel::class)
    open fun provideConfigureRemoteAccessViewModel(
        octoPrintRepository: OctoPrintRepository,
        setAlternativeWebUrlUseCase: SetAlternativeWebUrlUseCase,
        getRemoteServiceConnectUrlUseCase: GetRemoteServiceConnectUrlUseCase,
    ): ViewModel = ConfigureRemoteAccessViewModel(
        octoPrintRepository = octoPrintRepository,
        setAlternativeWebUrlUseCase = setAlternativeWebUrlUseCase,
        getRemoteServiceConnectUrlUseCase = getRemoteServiceConnectUrlUseCase,
    )

    @Provides
    @IntoMap
    @ViewModelKey(ConfigureRemoteAccessSpaghettiDetectiveViewModel::class)
    fun provideConfigureRemoteAccessSpaghettiDetectiveViewModel(
        octoPrintProvider: OctoPrintProvider,
        octoPrintRepository: OctoPrintRepository,
    ): ViewModel = ConfigureRemoteAccessSpaghettiDetectiveViewModel(
        octoPrintProvider = octoPrintProvider,
        octoPrintRepository = octoPrintRepository
    )

    @Provides
    @IntoMap
    @ViewModelKey(TimelapseArchiveViewModel::class)
    fun provideTimelapseArchiveViewModel(
        timelapseRepository: TimelapseRepository,
        picasso: LiveData<Picasso?>,
    ): ViewModel = TimelapseArchiveViewModel(
        timelapseRepository = timelapseRepository,
        picasso = picasso,
    )

    @Provides
    @IntoMap
    @ViewModelKey(TimelapsePlaybackViewModel::class)
    fun provideTimelapsePlaybackViewModel(): ViewModel = TimelapsePlaybackViewModel()


    @Provides
    @IntoMap
    @ViewModelKey(ControlCenterViewModel::class)
    fun provideControlCenterViewModel(
        octoPrintRepository: OctoPrintRepository,
        octoPrintProvider: OctoPrintProvider,
        getWebcamSnapshotUseCase: GetWebcamSnapshotUseCase,
    ): ViewModel = ControlCenterViewModel(
        octoPrintRepository = octoPrintRepository,
        octoPrintProvider = octoPrintProvider,
        getWebcamSnapshotUseCase = getWebcamSnapshotUseCase
    )
}