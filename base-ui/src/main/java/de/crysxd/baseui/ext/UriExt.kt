package de.crysxd.baseui.ext

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import androidx.navigation.NavOptions
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import io.ktor.http.Url
import timber.log.Timber

var mainActivityClass: Class<out Activity>? = null

fun Uri.open() = OctoActivity.instance?.let { open(it) } ?: Timber.w("NO ACTIVITY!")

fun Url.open() = Uri.parse(toString()).open()

fun Url.open(octoActivity: Activity, allowCustomTabs: Boolean = true) = Uri.parse(toString()).open(octoActivity, allowCustomTabs)

fun Uri.open(octoActivity: Activity, allowCustomTabs: Boolean = true) {
    Timber.i("LINK: $this")
    try {
        when {
            this.host == "app.octoapp.eu" -> {
                Timber.i("Opening in-app link: $this")
                (octoActivity as? OctoActivity)?.navController?.navigate(
                    this,
                    NavOptions.Builder()
                        .setEnterAnim(R.anim.enterAnim)
                        .setExitAnim(R.anim.exitAnim)
                        .setPopEnterAnim(R.anim.popEnterAnim)
                        .setPopExitAnim(R.anim.popExitAnim)
                        .setLaunchSingleTop(true)
                        .build()
                ) ?: let {
                    val intent = Intent(octoActivity, mainActivityClass)
                    intent.data = this
                    intent.action = Intent.ACTION_VIEW
                    octoActivity.startActivity(intent)
                }
            }

            (scheme == "http" || scheme == "https") && allowCustomTabs -> {
                try {
                    Timber.i("Opening custom tab link: $this")
                    val builder = CustomTabsIntent.Builder()
                    val customTabsIntent = builder.build()
                    customTabsIntent.launchUrl(octoActivity, this)
                } catch (e: java.lang.Exception) {
                    open(octoActivity, false)
                }
            }

            else -> {
                Timber.i("Opening external link: $this")
                val intent = Intent(Intent.ACTION_VIEW, this).also { it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) }
                octoActivity.startActivity(intent)
            }
        }
    } catch (e: Exception) {
        // This is an erroneous exception introduced with navigation 2.4.5, suppress. We do not interact with the backstack and the navigation still works.
        if (e.message?.contains(" is on the NavController's back stack") != true) {
            Timber.e(e)
            val message = OctoActivity.Message.DialogMessage(
                text = { getString(R.string.help___content_not_available) }
            )
            (octoActivity as? OctoActivity)?.showDialog(message) ?: Toast.makeText(octoActivity, message.text(octoActivity), Toast.LENGTH_SHORT).show()
        } else {
            Timber.w("Suppressed erroneous Exception from navigation: ${e.message}")
        }
    }
}