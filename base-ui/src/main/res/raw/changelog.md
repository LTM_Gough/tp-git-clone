**1.16-beta16**

- Create visual representation for the Cancel Object plugin on the print bed
- Performance improvements for Gcode preview, reduced memory usage

**1.16-beta15**

- Allow Wear OS to send Gcode from history
- Add Cancel Object to Wear OS
- Small performance improvements
- Fixed a glitch with the bottom bar in the prepare and print screens

**1.16-beta13**

- Fix beta deployment

**1.16-beta12**

- Add actual release notes

**1.16-beta11**

- Ensure correct format of release notes

**1.16-beta10**

- Publish release notes for beta releases in Google Play

**1.16-beta09**

- Fix Google Play deployment

**1.16-beta08**

- Added option to show name of active OctoPrint in status bar
- Added option to ignore first layer when counting Gcode layers
- Added support for CancelObject plugin
- Restructured settings to allow more options
- Fixed Gcode preview stuck after using sliders during print
- Fixed print progress percentage of progress bar differs from bottom bar
- Fixed manual dark mode from settings
- Changes up to 1.15.25

**1.16-beta07**

- Performance improvements using Baseline Profiles

**1.16-beta06**

- Fixed issue causing controls to change order when switching between prepare and print
- Fixed issue causing controls to not be sortable and to crash the app when trying to move the last control
- Fixed compatibility issue between OctoApp and current dev version of OctoPrint 1.9
- Fixed crash that happened in background after tune menu was used
- Targeting Android 13 and handling notification permission correctly
- Adding monochrome launcher icon for Android 13's home screen

**1.16-beta05**

- Fixed an issue limiting spools from SpoolManager to at most 100
- Added "Quick Print" controls to the prepare stage
- Updated all dependencies

**1.16-beta04**

⚠️ High risk of bugs

- Convert "controls" screens for prepare and print to new view framework
- Improved webcam fullscreen with more print information
- Tune menu now shows which values are saved already and which are not yet saved
- Many small improvement and changes in print and prepare screens

**1.16-beta03**

⚠️ High risk of bugs

- Convert "controls" screens for prepare and print to new view framework
- Improved webcam fullscreen with more print information
- Tune menu now shows which values are saved already and which are not yet saved
- Many small improvement and changes in print and prepare screens

**1.16-beta02**

- All improvements and fixes of 1.15.12

**1.16-beta01**

- Identical to 1.15.0

**1.15.25**

- After a remote connection was automatically removed, show the reason in the setup screen
- Fixed a bug causing Octolapse to show up after webcam was rotated
- Foxed a bug causing Octolapse to show up although snapshot plan doesn't need to be confirmed

**1.15.24**

- Fixed compatibility issue between OctoApp and current dev version of OctoPrint 1.9
- Fixed issue causing webcam snapshots to be flipped incorrectly

**1.15.23**

- Disable HTTP caching, seems to lead to issues with auto connecting printers

**1.15.22**

- Fix file uploads to folders with spaces in their pah causes folders with %20 to be created
- Fix sort order in file search not matching settings

**1.15.21**

- Potential fix for some crashes caused by Wear OS support
- Fixed Obico webcam

**1.15.20**

- Solve issues with data saver not falling back on other solutions
- Add data saver webcam to troubleshooting wizard

**1.15.19**

- Fixed help page to not shown

**1.15.18**

- Fixed an issue causing OctoPrint configuration to be persisted correctly
- Fixed an issue blocking the UI thread when opening the main menu
- Fixed an issue causing corrupted Gcode caches to be used

**1.15.17**

- Fix Prusa temperature display
- Fix selected file not highlighted in file list

**1.15.16**

- Warn about high webcam data usage
- Remove widget clipping for all devices below Android 12 to solve all issues

**1.15.15**

- Fix bug in internal data upgrade when coming from an older version

**1.15.14**

- Attempt to fix widget again

**1.15.13**

- Another attempt of fixing widgets

**1.15.12**

- Fix scroll issue in terminal window

**1.15.11**

- Fix Widgets in OnePlus devices with Android 11 or older

**1.15.10**

- Fix Widgets in Xiaomi devices with Android 11 or older

**1.15.9**

- Small bug fixes

**1.15.8**

- Fix webcam not restartig after app was reopened

**1.15.7**

- Drop old DNS cache by using a new file

**1.15.6**

- Fix issue with Wear DNS cache

**1.15.5**

- Fixed some smaller issues

**1.15.4**

- No changes, attempt to fix Play Store release

**1.15.3**

- No changes, attempt to fix Play Store release

**1.15.2**

- No changes, attempt to fix Play Store release

**1.15.1**

- No changes, attempt to fix Play Store release

**1.15.0**

- Changes of 1.14.7

**1.15-beta09**

- Update translations
- Rebranding from The Spaghetti Detective to Obico
- Finalising 1.15 release

**1.15-beta08**

- Data saver webcam option added
- Updated translations

**1.15-beta04**

- Many changes under the hood for the upcoming Wear OS app
- Ability to hide additional temperatures

**1.15-beta03**

- File search
- Many changes under the hood for the upcoming Wear OS app

⚠️ This release has a higher than usual risk of bugs, although all automated checks passed!

**1.15-beta02**

- Many changes under the hood for the upcoming Wear OS app

⚠️ This release has a higher than usual risk of bugs, although all automated checks passed!

**1.15-beta01**

- All changes of 1.14 stable releases

**1.14.17**

- Reset tune menu value when switching OctoPrint

**1.14.16**

- Automatically hide random temperature components generated by Prusa debug output

**1.14.15**

- Add option to hide new additional temperatures

**1.14.14**

- Fixed a bug introduced with 1.14.13 causing print head movements to fail

**1.14.13**

- Some small bug fixes
- Enclosure plugin temperatures
- Lower case Gcode input

**1.14.12**

- Fix crash
- Support enclosure plugin

**1.14.10**

- Fixed bug causing rotation to change active OctoPrint if app was launched from notification or widget
- New feature gallery

**1.14.9**

- Fixed print completed notification not removed when next print starts
- Fixed live notification not showing print done
- Fixed error message with OctoHue

**1.14.8**

- Add support for OctoHue

**1.14.7**

- Fix wrong handling of invalid remote configurations

**1.14.6**

- Fix Ophom off switch

**1.14.5**

- Prevent ngrok from being used as primary URL
- Add support for Ophom plugin
- Potential fix for progress missalignement

**1.14.4**

- Improvements to swipe detection to open control center
- Improvements to touch tracking of sliders in Gcode preview
- Small textual fixes around HTTPS certificate errors
- Fixed some crashes

**1.14.3**

- Fix deployment issue

**1.14.2**

- Fixed smaller issues
- Fixed a bug causing the access request not to show up in OctoPrint

**1.14.1**

- Fixed bug causing lefty mode to not be correctly applied right away

**1.14-beta10**

- Sort selected material to the top

**1.14-beta09**

- Changes from 1.13.14
- Added colors for Filament Manager

**1.14-beta08**

- Changes up to 1.13.11
- Fixed issue when downloading timelapse files
- Fixed issue causing the "turn PSU on" button to not be shown when connecting the printer from time to time

**1.14-beta06**

- Automatically configure ngrok
- First iteration of USPs for remote services
- Fix for USB Relay Control
- Fix for empty texts in Spanish, Italian, Dutch and French
- Support for GBRL plugin

**1.14-beta05**

- Support for USB Relay Control plugin
- Automatic ngrok configuration
- Support from GBRL plugin

**1.14-beta03**

- Small improvements around notifications
- Don't cache timelapse files in HTTP cache

**1.14-beta02**

- Support for MMU2 Filament Select plugin

**1.14-beta01**

- Support for Octolapse to show snapshot plans

**1.13.14**

- Fixed some minor bugs

**1.13.13**

- Fixed layout issue on Frenxh devices with small display

**1.13.12**

- Fix a crash
- Fix wrong power device uses with Tuya plugin

**1.13.11**

- Add lefty mode for bottom toolbar
- Add option to reset power device defaults

**1.13.10**

- Fix issue with SpoolManager if no spool is selected
- Fix issues causing notification to be stuck

**1.13.8**

- Fixed ngrok handling when tunnel is gone
- Allow connection of OE without local access

**1.13.7**

- Fixed issue causing Gcode files to be cached twice
- Fixed issue causing temperature controls to be created more than once
- Fixed scrolling issue in timelapse archive

**1.13.6**

- Fix control center swipe interfering with Pixel 4a back navigation

**1.13.5**

- Fixed OctoEverywhere disconnected flashing up in control center
- Fixed websocket upgrade error spammed continuously

**1.13.4**

- Fixed bug with special chars in file names
- Fixed issue causing old temperature presets to still show up when pinned
- Fixed wrong spool weight for Filament Manager

**1.13.3**

- Added option for compact layout
- Limit swipe from edges of display
- Small bug fixes

**1.13.2**

- Small bug fixes
- Fixed a bug causing a user to not be able to create widgets if the app isn't running in the background

**1.13.1**

- Fixed some small bugs

**1.13.0**

- No changes from 1.13-beta19

**1.13-beta19**

- Fix issues around Basic Auth

**1.13-beta18**

- Fixes to logging around network requests

**1.13-beta17**

- Small improvements for Basic Auth

**1.13-beta15**

- Network debug options
- Finalising for release

**1.13-beta14**

- Update Spanish translations

**1.13-beta13**

- Improvements for control center

**1.13-beta12**

- Changes of 1.12.13
- Updated Spanish translations
- Support for Wemo plugin

**1.13-beta11**

- Add control center to quickly switch between instances and the status of all OctoPrint (swipe left or right on any screen to open)

**1.13-beta10**

- Add option to enforce IPv4
- Fixed a bug causing the wrong webcam feed to load after the OctoPrint instance was switched

**1.13-beta09**

- Fixed webcam cutting out

**1.13-beta08**

- Much improved webcam performance for MJPEG streams with large frame sizes of up to 1 MB
- Updated Spanish translations

**1.13-beta07**

- Debug options for DNS and network traffic
- Debug options for webcam

**1.13-beta06**

- Do not persist error indicating a broken setup (api key invalid, SSL issue, ...)
- Force call timeout when testing OctoPrint connections

**1.13-beta05**

- Changes of 1.12.10
- Debug option for Jim

**1.13-beta04**

- Small bugfixes

**1.13-beta03**

- Changes up to 1.12.7
- Replaced pause button with toolbar

**1.13-beta02**

- Support for Timelapse controls
- Timelapse archive (play, share, delete)

**1.13-beta01**

- Selective subscription to web socket to reduce data usage and system load (requires OctoPrint 1.8)

**1.12.16**

- Fix issues with power devices containing / in their id

**1.12.15**

- Fixed issues with Gcode in landscape mode
- Fixed issues with pinned items from the Tasmota and WS281 plugins
- Remove transparency from lines in Gcode preview

**1.12.14**

- Hide unavailable power devices
- Fix URL escape for file paths

**1.12.13**

- Add support for manual dark mode on Android 10+

**1.12.12**

- Show weight and color of spools

**1.12.11**

- Allow TSD connection via mobile data

**1.12.10**

- Fix issue causing path to be escaped in file URLs
- Fixed issue causing a low level network error to surface when attempting to resolve UPnP device on a mobile network

**1.12.9**

- Honor reverse axis options

**1.12.8**

- Be more lenient with purchases reported by Google Play

**1.12.7**

- Many bugfixes around widgets
- Small improvements around webcam fullscreen layout
- Webcam fullscreen can be opened with double tap

**1.12.6**

- Support for custom TSD instances
- Slightly updated fullscreen webcam layout

**1.12.5**

- Cache resolved IP addresses to disk to make mDNS and UPnP more reliable

**1.12.4**

- Small bug fixes
- Added "Add second OctoPrint" to OctoPrint menu as well

**1.12.3**

- Small bug fixes
- Disable remote notifications if Google Play services causes crash to prevent repetitive crashes

**1.12.2**

- Cool down option in printer menu
- Small bug fixes

**1.12.1**

- Small bug fixed
- Support for temperature offsets

**1.12.0**

- Update release notes
- Some more improvements around TSD integration

**1.12-beta19**

- Fixed a bug with TSD webcam feed
- Fixed a bug causing the UPnP resolver to block the port

**1.12-beta17**

- Fixed some crashes and ANRs

**1.12-beta16**

- Small bug fixes

**1.12-beta15**

- Improvements for TSD integration

**1.12-beta14**

- Improvement to TSD integration
- Added translations

**1.12-beta13**

- Support for Spaghetti Detective Tunnel (still WIP)

**1.12-beta12**

- Fixed camera image rotated clockwise instead of counter-clockwise
- Fixed opening a power device menu queries state of all power devices
- Added support for OctoRelay plugin to be used with automatic lights

**1.12-beta11**

- Add plugin library

**1.12-beta10**

- Support for RTSP webcam streams
- Improved zooming on webcam images
- Customisable view for print progress
- Added option to show layer number and progress next to print progress
- Added option for smaller font in print progress details
- Added option for compact ETA format if print is not finished in current day
- Added option to show file thumbnail next to print progress

**1.12-beta09**

- Changes from 1.11.12 to 1.11.15
- Do not request M290

**1.12-beta07**

- Changes from 1.11.12

**1.12-beta06**

- Changes from 1.11.11
- Fixed a bug in the reconnection routine, causing issues with the red "reconnecting" banner

**1.12-beta05**

- All fixes of 1.11

**1.12-beta04**

- Added support for uploading files
- Added support for downloading files
- Added support for renaming files and folders
- Added support for moving and copying files and folders
- Added sorting options for files
- Highlighting the currently selected file on top of the files list

**1.12-beta03**

- Fixed some visual issues and improved some animations
- Removed the delay before getting a confirmation when using a Gcode shortcut
- Fixed a bug causing the terminal to show events multiple times and in the wrong order
- Fixed a bug causing the app to reconnect the web socket when using the PSU Control plugin
- Added a button to copy the response to a Gcode in the terminal

**1.12-beta02**

- Added baby step controls in tune menu
- Added options for X an Y feedrate when moving the tool manually

**1.12-beta01**

- Added options to the Gcode preview (render quality, show previous layer, show current layer)
- Added option to apply temperature presents to only the bed/hotends/chamber

**1.11.19**

- Fixed some crashes and ANRs

**1.11.17**

- Added hint that syncing purchases can take a while
- Added hint to 404 result that OctoPrint might be outdated
- Reduce Youtube API usage

**1.11.16**

- Ensure "reconnecting" banner is always hidden once reconnected

**1.11.15**

- Fix DNS SD issues on Android 12 (.local domains)
- Fixed bug causing previous layer to show current layer
- Improved logging for current messages and temperatures

**1.11.14**

- Fixed bugt preventing extrusion

**1.11.13**

- Fixed bug in Gcode preview causing layer progress to be frozen at 50%

**1.11.12**

- Remove animations from webcam to fix #922

**1.11.11**

- Fix bug causing app to switch to backup connection when a network request gets cancelled
- Reduce bandwidth used by media files

**1.11.10**

- Potential fix for black webcam (#922)

**1.11.9**

- Add debugging options for black webcam stream
- Ensure webcam connection properly closed

**1.11.8**

- Fixed a crash and some other small bugs

**1.11.7**

- Removed an optimization which made requests to PsuControl plugin faster but caused issues for some users
- Fixed a bug causing internal Gcode cache errors to be exposed. Now the cache is cleared and the data loaded from remote automatically.

**1.11.6**

- Fixed some crashes and added logs for open issues

**1.11.5**

- Added more logs for crash

**1.11.4**

- Added additional logs for crash

**1.11.3**

- Fixed multiple smaller crashes and other issues

**1.11.2**

- Fixed rare crashes and some minor issues

**1.11.1**

- Fixed a bug causing lines in the Gcode preview to be misaligned
- Fixed a crash when manually entering the API key

**1.11.0**

- Updated release notes and version announcement

**1.11-beta14**

- Fixed a bug causing the connection state (local or remote connection) not to be shown if the state was changed by the notification
- Fixed a bug causing the OctoEverywhere banner to be shown if the OctoPrint instance was turned off
- Fixed a bug causing a random SSL error to be shown when OctoEverywhere failed to upgrade the websocket (usually while OctoPrint was booting)
- Fixed a bug causing remote notifications to not be cleared after the print was done
- Fixed a bug causing the remote notification for the print progress not to be shown

**1.11-beta13**

- Small bug fixes

**1.11-beta12**

- Fixed a crash on start up
- Change from 1.10.23

**1.11-beta11**

- Many small bug fixes
- Fixed the error flow if the remote URL has SSL issues or Basic Auth changed

**1.11-beta09**

- Internally restructured the app (big time)
- New tutorials section in main menu (replacing news)
- Showing badges in the main menu to guide the user to the companion plugin and OctoEverywhere setup
- Fixed a bug causing the print progress and temperatures not to update after the app was in the background
- Fixed performance issues on the login screen
- Fixed a bug causing the notification to not updated after reconnecting
- Fixed a bug causing the remote connection to not being removable

**1.11-beta08**

- Refine Material You design
- Changes from 1.10.17

**1.11-beta07**

- Support for OctoApp companion plugin. You can get the plugin here: https://github.com/crysxd/OctoApp-Plugin
- New notification system with push notification via internet (companion plugin required) and support for Android 12
- App is now targeting Android 12
- Material You design on Android 12. The colours are now derived from the system colours, the color scheme of OctoPrint takes precedence. PLEASE GIVE ME FEEDBACK! Not
  sure about this...maybe the toolbar and the octopus should stay green?

**1.11-beta06**

- Changes from 1.10.16
- Fixed crash during homescreen shortcut creation
- Fixed an issue causing the octopus to have the wrong color
- Fixed an issue causing the app to loose track of the login flow when returning from the web interface after grating access (only happens if one OctoPrint was already
  connected)

**1.11-beta05**

- Fixed a bug causing language changes to not being applied
- Updated Android Gradle Plugin to 7.0

**1.11-beta04**

- Fixed bug that URLs which can't be pinged are not possible to connect
- Changes from 1.10.14

**1.11-beta03**

- All changes of 1.10, up to 1.10.12
- New internal data structures to improve reliability

**1.11-beta02**

- Changes from 1.10.5

**1.11-beta01**

- First beta of 1.11, no changes from 1.10.0

**1.10.23**

- Fixed different small issues
- Fixed an issue causing the Gcode preview to drift out of sync if it was sliced on Windows

**1.10.22**

- Fix remote access setup does not store basic auth

**1.10.21**

- Fixed a bug causing the remote connection setup to drop the port

**1.10.20**

- Added a OctoApp Lab option to bypass the terminal block during prints

**1.10.19**

- Added additional logs around navigation and progress view
- Fixed a bug causing the app not to switch back from a remote to the local connection

**1.10.18**

- Add file menu (long press) with delete option

**1.10.17**

- Increase webcam timeouts to accommodate slower OctoEverywhere webcams
- Fixed an issue with DroidCam webcams

**1.10.16**

- Hide empty, disabled and template spools from SpoolManager

**1.10.15**

- Fix bug causing online check request to be spamed, potentially causing issues with other requests

**1.10.14**

- Small bug fixes

**1.10.13**

- Better internal URL handling to reduce risk of bugs
- Support for WLED plugin

**1.10.12**

- Improve return from OctoEverywhere setup flow to ensure browser window is closed
- Fix issue causing the app not to switch to OctoEverywhere if port 80 is part of the local URL

**1.10.11**

- Smaller bug fixes and optimizations

**1.10.10**

- Fix issue with Tradfri device ID, now accepting numbers and texts as ID
- Fix issue with remote access and .local or UPnP primary web URLs (would always switch to remote)
- Fix issue with UPnP device discovery slowing down the initial connection time

**1.10.9**

- Disable M115 request when not needed, allow to suppress the request completely

**1.10.8**

- Fix issue with Gcode cache resulting in NumberFormatException (since 1.10.7)

**1.10.7**

- Fixed bug with Tradfri plugin caused by plugin changes
- Fixed bug with Gcode cache causing the cache to be corrupted

**1.10.6**

- Fix handling of special chars in basic auth credentials

**1.10.5**

- Resolve UPnP addresses when OctoPrint is opened in the web
- Simplify the "grant access" step in login to remove confusing elements
- Add basic instructions for API keys
- Add checks for OctoEverywhere URLs when manually setting up remote access
- Fixed a bug causing UPnP discovery to be stuck, blocking further attempts

**1.10.4**

- Fix dark mode issues in the Gcode preview on Xiaomi devices

**1.10.3**

- Fix webcam bug introduced with 1.10.2

**1.10.2**

- Fix some crashes with the new version
- Potential fix for an issue where the mDNS deamon is not available
- Ensure UPnP instances have "nice" names in the UI
- Fix a buffer overflow issue causing the webcam stream to temporarily stop
- Fix an issue causing the app not to move to the print space after starting a print with the app

**1.10.1**

- Fix crash when opening help and FAQ section
- Fix random crash on startup
- Fix spool selection not shown (due to error) if no spool is selected

**1.10.0**

- Final touches for 1.10 release

**1.10-beta11**

- Final fixes for 1.10

**1.10-beta10**

- Added version announcement for 1.10
- Added quick access widget to pin menu items to the home screen

**1.10-beta09**

- Minor bug fixes in login flow
- Fix a bug causing the toolbar to be discolored in dark mode

**1.10-beta08**

- Updated localizations
- Added Quick access for menu options from the main screen
- Changes from 1.9.5

**1.10-beta07**

- Let webcam widget use the new webcam connection to add support for mDNS domains

**1.10-beta06**

- Added Apple mDNS stack to bypass .local resolution
- Added auto discovery with UPnP as backup for DNS-SD, OctoApp will store the UPnP id instead of an IP to allow changing IP addresses
- Update of depdenencies

**1.10-beta05**

- Automatically paste URLs from clipboard during login
- Automatically remove /login from URLs during login

**1.10-beta04**

- Improve probing for discovered instances, do not rely on Application Keys plugin
- Do not show Upnp and Bonjour for the same instance

**1.10-beta03**

- Changes from 1.9.4
- Support for manual API key (using QR code or typing)
- Support for Upnp device discovery
- New webcam troubleshooting wizard based on the new login network test

**1.10-beta02**

- New login flow (still WIP) with Bonjour/DNS-SD/Avahi and Application Key flow support (popup in web interface to grant access). This is still WIP with missing
  translations and preliminary strings as well as missing support for manual API key entry
- Changes from 1.9.3

**1.10-beta01**

- No changes from 1.9.0

**1.9.6**

- Fix crash introduced with 1.9.5

**1.9.5**

- Fixed bug causing materials not the be activated with SpoolManager 1.4.3
- Refreshing the activated material after a material was selected

**1.9.4**

- Support for OctoRelay
- Added "Toggle" option for power devices (where supported)

**1.9.3**

- Fixed multiple smaller issues and improved logs

**1.9.2**

- Fix two rare crashes

**1.9.1**

- Allow to use aspect ratio from webcam image instead of OctoPrint settings
- Small bug fixes

**1.9.0**

- Fixed SpoolManager integration

**1.9-beta13**

- Support myStrom plugin

**1.9-beta12**

- Add support for GpioControls plugin

**1.9-beta11**

- Revert deployment changes
- Ensure material names are always unique by adding the material or vendor to the name

**1.9-beta10**

- New release process, no functional changes

**1.9-beta09**

- New release process, no functional changes

**1.9-beta08**

- Update translations for new features
- Improves synchronisation for automatic lights

**1.9-beta07**

- Fix build issue

**1.9-beta04**

- Support for WS281x plugin (turn lights ans torch on or off)
- New setting to turn a device (light) automatically on when the webcam is active and off when it's not. Can be used to illuminate print when webcam is active.

**1.9-beta03**

- Added a "experimental webcam" option to OctoLab enabling a completely new webcam implementation which uses less resources and should have lower latency (enabled by
  default for beta testers)

**1.9-beta02**

- Changes from 1.8.3
- # 556 Added a menu item "Show files" to allow file access during prints and if no printer is connected
- # 567 Showing the extrude controls when a print is paused

**1.9-beta01**

- No changes from 1.8.0

**1.8.4**

- Fix an issue causing an error dialog if a temperature profile with settings for chamber or bed was applied but the printer has no chamber or bed

**1.8.3**

- Fixed issue causing the "rotate app" and "notification battery saver" toggles to be linked

**1.8.2**

- Fix crash if webcam URL contains illegal characters
- Make password for basic auth optional
- Trim whitespaces and the beginning and end of URLs

**1.8.1**

- Fix uncommon crash

**1.8.0**

- Update introduction video

**1.8-beta13**

- Fixed crash if webcam URL is a relative path

**1.8-beta12**

- Added support for PrintTimeGenius, resulting in correct progress information when the plugin is installed
- Added support for basic auth in HLS streams

**1.8-beta11**

- Let webcam switch connection to remote connection when local connection is not available

**1.8-beta10**

- Changes from 1.7.6
- Changes from 1.7.7

**1.8-beta09**

- Added translations
- Fixed a view glitch in the "Configure remote access" screen after OctoEverywhere got connected
- Added a hint for users with the OctoEverywhere plugin telling them to connect it to OctoApp

**1.8-beta08**

- Fixed navigation issue when opening fullscreen webcam view
- Fixed an issue causing shared nozzles to show up with individual temperature controls
- Added a button to switch OctoPrint if it's not available

**1.8-beta07**

- Small visual fixes
- Fixed file downloads for OctoEverywhere

**1.8-beta06**

- Disconnect print notification 60s after the screen was turned off to reduce battery consumption. Multiple users complained about high battery use. Notification
  reconnects when the screen is turned back on

**1.8-beta05**

- Changes from 1.7.5
- Allow changing temperature of all hotends and chamber
- Added a chart in the background of the temperature control

**1.8-beta04**

- Add error handling for OctoEverywhere
- Eliminated a race condition causing the OctoEverywhere configuration not to be saved after connection

**1.8-beta03**

- Changes from 1.7.3
- New in app navigation system based on deep links allowing me to link to things in the app (e.g. https://app.octoapp.eu/help)

**1.8-beta02**

- Added support for a remote URL which will be used if the primary, local URL can't be reached. This can used to smartly switch between a globally accessible but slower
  and a local but fast connection. When the remote connection is used, the app will periodically test wether the local connection is back online.
- Integrated a SSO option for OctoEverywhere allowing OctoApp to be connected without any configuration. This will be treated as a remote URL, the app thus will prefer
  the local connection but fallback on OctoEverywhere.

**1.8-beta01**

- Changes from 1.7.0

**1.7.7**

- Fixed an issue causing the app to be stuck on the connect screen. This might have been commonly triggered by the dark mode on Android 5 to 9.

**1.7.6**

- Fixed an issue causing the disconnected notification not to go away once reconnected
- Potential fix for the disconnected notification to not be dismissible

**1.7.5-test01**

- Removed dark mode for Android 9 as potential fix for an issue

**1.7.5**

- Potential fix for crash related to print notification
- Allow OctoPrint instances to be removed from OctoApp

**1.7.4**

- Fix view issue in checkout screen

**1.7.3**

- Fix a uncommon crash if the app is opened without internet connection/connection to OctoPrint not possible
- Fix a issue causing error messages to not be masked completely

**1.7.2**

- Fix fullscreen webcam toggling between landscape and portrait when rotated

**1.7.1**

- Fix intro video URL

**1.7.0**

- Fixed issues with reordering and hiding controls introduced with the last versions

**1.7-beta22**

- Added hint for .local domains in trouble shooting wizard
- Now sharing webcam connection between fullscreen and small webcam view

**1.7-beta21**

- Fixed a issues causing the controls to reload when the app is reopened, causing a reconnection of the webcam after a couple of ms potentially leaking a webcam stream
- Fixed a issues causing the webcam stream to reconnect after a second of reopening the app, potentially leaking a webcam connection

**1.7-beta20**

- Prevent the app from opening multiple parallel connections to the webcam, leaking old connections
- Added the "Open terminal" menu item back which vanished at some point in the past...
- Added a brief explanation to the emergency stop dialog to prevent confusion about what it's doing

**1.7-beta19**

- Refined reordering of widgets
- Allow widgets to be hidden

**1.7-beta18**

- Allow widgets in print and prepare screen to be reordered by Drag & Drop
- A lot of small bug fixes. Thanks all for reporting!

**1.7-beta17**

- Fix uncommon crash on startup

**1.7-beta16**

- Completely new UI layout system, this removed the grid layout for tablet for now but results in higher performance
- New cache system for Gcode files. Files are now cached and loaded by layer massively reducing the amount of RAM used and speeding speeding up the loading time from
  cache. Also, Gcode now uses around 40% less disk space to cache.

**1.7-beta15**

- Cache Gcode by layer, reducing RAM usage drastically
- Fix "turn PSU on" button not showing up

**1.7-beta13**

- Fixed bug in Gcoed viewer prevent some files to be loaded

**1.7-beta12**

- Support for IJ form arcs in Gcode preview (Arc welder)
- Changes from 1.6.11

**1.7-beta11**

- Improved caching of state so the app will reopen quicker during a print, usually instantly snapping open with all data visible
- Improved messaging in connect state for network issues

**1.7-beta10**

- Changes from 1.6.10

**1.7-beta09**

- Changes from 1.6.9
- Fixed a issue causing the progress in print information to show up with a 5s delay

**1.7-beta08**

- Added FAQ page

**1.7-beta07**

- Changes from 1.6.7
- Empty states for power devices and materials
- New execution of tasks in the menu, now keeps the menu open so the next action can be pressed right away
- Better success feedback for menu items

**1.7-beta06**

- Rolled back changes to the streaming stack as they proved ineffective. This solves some problem with the streaming in 1.7 beta versions
- Added a empty state for the power device menu
- Add support for multicam plugin
- Changes from 1.6.6

**1.7-beta05**

- Changes from 1.6.5
- New power controls menu

**1.7-beta04**

- Changes from 1.6.4
- Fix issue with FilamentManager spool selection

**1.7-beta03**

- Changes from 1.6.3

**1.7-beta02**

- Add support for SpoolManager and FilamentManager

**1.7-beta01**

- All new MJPEG connection which is more efficient with memory and should allow for higher and more stable frame rates
- Fixed an issue in MJPEG connection where boundaries enclosed by quotes were not detected

**1.6.11**

- Removed warning about dysfunctional websocket

**1.6.10**

- Fixed a common crash introduced with 1.6.9
- Some minor fixes

**1.6.9**

- Fixed a issue causing the print notification to disconnect when the devices enters deep sleep
- Fixed a issue where the wrong date/time format could be used (e.g. 12h instead of 24h)
- Fixed a issue causing Gcode files with arcs (e.g. Arc Welder) to not properly update in the Gcode viewer
- Fixed a issue causing the layer number in the fullscreen Gcode preview to be ahead by one
- Added preliminary support for arcs, interpolating them as one line in red color

**1.6.8**

- Fixed issue with Tasmota smart devices

**1.6.7**

- Fixed a issue causing the webcam to not show up on the first start of the app

**1.6.6**

- Add support for printers with center origin for Gcode preview
- Fixed a issue causing logs to be spammed for some instances

**1.6.5**

- Fixed an issue causing the "Open OctoPrint" to not work
- Fixed an issue causing the "Privacy menu" link to crash the app on Samsung/Android 11
- Fixed an issue causing a rogue print notification to show up, either in "Disconnected" or "Printing 0% progress" state
- Fixed an issue causing the print notification to not reconnected when a WiFi connection is established after a disconnect

**1.6.4**

- Fix layout issue on Android 5, 6 and 7
- Improve error messages

**1.6.3**

- Fixes a hilarious bug causing subscription users being prevent from creating a single widget

**1.6.2**

- Fixed one common and a couple of uncommon crashes

**1.6.1**

- Fixed an issue causing the webcam widget to not load images since 1.6-beta13
- Fixed an issue where deleted widgets were still updated

**1.6.0**

- Small layout fix for the progress widget in the smallest layout stage

**1.6-beta14**

- Final beta on Google Play
- Add in-app-messaging to announce new beta
- Fixes from 1.6.2

**1.6-beta13**

- Improved offline handling of the print notification. Perviously, it continuously tried to reconnect causing a high battery usage in this state. Now, it will move to a
  disconnected state after 3 minutes and attempt to reconnect the next time a WiFi connection is established or the app is opened.
- If only one OctoPrint is available, the selection dialog to pick a instance while creating a widget now auto selects the only available instance
- Added support for Tuya smart plugs
- Fixed a issue causing the Tasmoa smart plug integration to be disabled
- Added a privacy menu allowing the user to disable reporting and a link to the privacy statement
- Improved scrubbing of sensitive data on a low level by replacing exceptions from the network level with a "proxy" containing a scrubbed error message
- Ensure hostnames of webcam connections are scrubbed if they differ from the main OctoPrint instance
- Fixed an issue causing widgets to not use the language set in the app but the device language (if available)
- Improved connection timeout handling
- Fixed a issue causing the widgets being stuck in the "Updating..." state
- Removed basic auth information from URLs shown in the app

**1.6-beta12**

- Changes from 1.5.9
- Fixed an issue causing the app to open multiple connections (up to 3 observed) to the webcam in rapid succession when the app was started or opened after some time in
  background

**1.6-beta11**

- Progress widget
- All OctoPrint color themes are reflected in the app and widgets
- Fixed a bug causing the mascot in the toolbar to be misplaced/mis-sized
- Improved translations
- Moved French translation out of beta

**1.6-beta10**

- Refined menu item design, pin icon now on the other side
- Added toggles to settings items for better UX
- Updated French translations
- Added option to disabled auto printer connect on app start
- Added second cancel option in menu which will keep the temperatures set
- Allowing sub menus (change OctoPrint, temp presets) to be pinned to start
- Fixed an issue causing the power controls dialog to be only partly visible on some devices

**1.6-beta09**

- Fixed webcam connection does not honor Basic Auth in URL

**1.6-beta08**

- Changes from 1.5.7

**1.6-beta07**

- Changes from 1.5.5
- Fixed issue with app icon colors
- Added notification when filament change is due

**1.6-beta06**

- Fixed refresh and live action for webcam widget not working on Samsung phones

**1.6-beta05**

- Changes from 1.5.4
- Added Webcam widget

**1.6-beta03**

- Changes from 1.5.2

**1.6-beta02**

- Changes from 1.5.1

**1.6-beta01**

- No changes from 1.5.0

**1.5.9**

- Fixed an issue where the app was unable to extract untrusted certificates from a connection if the certificate did not provide a "alternative subject name". This
  caused the "do you want to trust this server?" dialog not to show up during sign in

**1.5.8**

- Fixed an issue where the Gcode cache would ignore re-uploaded files and stick to the old version
- Fixed an issue causing a crash if the "Learn how to sign in" button on sign in was clicked right after installation

**1.5.7**

- Prevent the error message "Unable to update capabilities" from showing up when the server is not connected
- Fix a crash caused when this message was shown after the app was closed

**1.5.6**

- Added indicator in file list for outcome of last print

**1.5.5**

- Layout fixes for tablets
- Some crash fixes

**1.5.4**

- Fix color issue in app icon

**1.5.3**

- Let OctoApp use OctoPrint's color (appearance settings)
- Let OctoApp use OctoPrint's name in multi-printer menu

**1.5.2**

- Fixed a bug on Android 9 causing the dark mode to not work
- Added OctoPrint system commands

**1.5.1**

- Fixed an issue causing the temperature presets not to show up during prints
- Added a OctoPrint menu
- Allowed file names to break into a second line

**1.5.0**

- Suppress 500 errors from Tradfri Plugin
- Added announcement/new version section
- Resolved bugs in menu navigation

**1.5-beta10**

- Changes from 1.4.8
- Add missing translations
- Store preferences for webcam, terminal filters and default power devices per OctoPrint isntance

**1.5-beta09**

- Changes from 1.4.7

**1.5-beta08**

- Support temperature presets

**1.5-beta07**

- Many small bug fixes around new features
- Targeting Android 11 (previously 10)
- Updated all dependencies

**1.5-beta06**

- Fixed and issue with app preferences causing a crash

**1.5-beta05**

- Improved menu to switch OctoPrint instances
- Render Gcode preview in print controls screen asynchrounously to prevent the UI from stuttering
- Limit live Gcode updates to once every second
- In the fullscreen Gcode view, switch to async rendering if a frame takes longer than 20ms to render
- Allow the trouble shooting wizard to be used with the webcam URL

**1.5-beta04**

- Resolved visual issues introduced with 1.5-beta03
- Prepared "quick switch" to quickly switch between multiple instances
- Added support for Tasmota smart plugs
- Some performance optimizations
- Changes from 1.4.5

**1.5-beta03**

- Fixed a bug causing an error message when opening the power controls dialog
- Added a new, customizable main menu
- Slight color adjustments in dark mode

**1.5-beta02**

- Support for IKEA Trådfri and TP-Link smart plugs
- Small visual overhaul (WIP) with change to new Material Theme

**1.5-beta01**

- Changes from 1.4.1

**1.5-alpha05**

- Changes from 1.4 up to 1.4-beta12

**1.5-alpha04**

- Changes from 1.4-beta07

**1.5-alpha03**

- Changes from 1.4-beta06

**1.5-alpha02**

- Changes from 1.4-beta05

**1.5-alpha01**

- Allow quick switching between OctoPrint instances (preliminary solution)

**1.4.8**

- Resolved various uncommon crashes
- Resolved an issue in Italian translation

**1.4.7**

- Fix a issue causing the Authentication header not to be set with Basic Auth credentials
- Fix a issue causing the API key not to be purged from logs before sign in completed successfully
- Purge Basic Auth information from logs
- Purge host names from logs

**1.4.6**

- Resolved uncommon crashes

**1.4.5**

- Removed manual recycling of webcam frames as this seems to cause a crash

**1.4.4**

- Fixed a rare crash on login screen
- Fixed a issue causing the app to freeze and crash

**1.4.3**

- Fixed a bug causing the help link on the sign in page to be broken

**1.4.2**

- Fixed a bug causing the app to crash (related to webcam)

**1.4.1**

- Fixed Google Play Deploy, no functional changes

**1.4.0**

- Update German and Dutch translations
- Fix bug causing the "Hide" button for disabled Gcode preview to not work

**1.4-beta12**

- Fixed common crash
- Fixed thumbnails for files not loaded

**1.4-beta11**

- Fixed deployment issue, no changes to 1.4-beta10

**1.4-beta10**

- Allow print notification to be closed

**1.4-beta09**

- Completed supporter feature
- Fixed a issue causing the HLS stream to not stop playing when the app is closed
- Fixed a crash in the webcam's error state
- Added translations for German (final), Italian (beta), Spanish (beta), Dutch (beta) and French (beta). User can switch between device language and English. Final
  languages will be used as default whereas beta languages will only be used after user's opt-in
- Fixed reload of file list after an error occurred while loading the files
- Fixed a issue in Gcode parsing causing the comments to be considered part of the command
- Changed title font from `Roboto` to `Ubuntu`

**1.4-beta08**

- Fixed a bug causing the app to reset the navigation to beginning when re-entering the app or when the system recreates the UI (e.g. when switching from light to dark
  theme)
- Added a special disconnected state instead of jumping to the connect state. The app will stay in the current screen but show a "disconnected" banner until the
  connection is restored
- Prepared supporters feature
- The login and connect screens will show a warning if no WiFi is connected (common cause of connection issues)
- Added a link with a manual to the sign in screen to help beginners
- Fixed a bug with the Gcode history (it was not recording new elements)

**1.4-beta07**

- Changes from 1.3.15
- Solved some crashes in new features
- Added an improved long-press menu for Gcode shortcuts in the terminal window and the Gcode command widget
- Added a tutorial hint for Gcode shortcuts
- Fixed an issue causing two "live" badges to be shown in fullscreen webcam
- Added in-memory cache for parsed Gcode to speed up the view after the app is brought back to the foreground

**1.4-beta06**

- Allowed Z feedrate to be changed for manual moves
- Added a option to set a label for Gcode shortcuts
- Terminal does now accept multiple commands at once (one per line) which also create one combined shortcut
- Gcode shortcuts can now be removed
- Fixed a bug causing the long press action for Gcode commands in terminal not to trigger
- Fixed a bug causing the Gcode shortcuts on the main screen not to update when they were changed in terminal screen

**1.4-beta05**

- Added Gcode live render during print (including full screen option)
- Added HLS support for webcams
- Added support for zooming and scrolling in the fullscreen webcam view
- General bug fixed and optimizations

**1.4-beta04**

- Improved checks for wether the printer is connected in response to reports of the app reconnecting the printer during a print

**1.4-beta03**

- Changes from 1.3.12

**1.4-beta02**

- All changes from 1.3.10
- Better scrolling behavior on file details page

**1.4-beta01**

- Added Gcode preview in file details

**1.3.16**

- Fixed issue with login at OctoEverywhere

**1.3.15**

- Added a special error message if the server requires Basic Auth during sing in
- Added handling for old OctoPrint versions returning 204 for wrong API keys
- Removed warning about admin rights
- Fixed an layout issue causing the progress percent not to be shown

**1.3.13**

- Improved checks for wether the printer is connected in response to reports of the app reconnecting the printer during a print

**1.3.12**

- Confirmed support for OctoPrint 1.5.1 (removes warning during login)
- Fix bug causing empty print progress to be shown when the webcam was started in full screen with no active print
- Added mandatory message input to feedback dialog to reduce spam

**1.3.11**

- Optimized sing in analytics

**1.3.10**

- Fix flickering in the user interface while connecting to the printer

**1.3.9**

- Fixed uncommon crashes
- Fixed an issue causing the app not to show the connection progress while the printer is connecting
- Fixed an issue causing the app to continuously request the printer's connection state even after the printer was connected and the app even closed

**1.3.8**

- Increased connection and ping/pong timeout for websocket to 10s

**1.3.7**

- Fix crash in file explorer

**1.3.6**

- No changes from 1.3.5, fixed Google Play release pipeline

**1.3.5**

- Fix crash introduced with 1.3.4

**1.3.4**

- Multiple crashes and other issues resolved

**1.3.3**

- Added a "Trouble shooting" button to error dialogs in the sign in screen. A series of checks will be performed to narrow down the issue and matching suggestions are
  shown

**1.3.2**

- Fixed connection issue while printing from SD card

**1.3.1**

- Fixed an issue causing the print completed notification to not have any sound

**1.3.0**

- Now suppressing error messages about the connection if the disconnect state is already active
- If no update about the print progress is received for over one minute, the print notification switches to the disconnect mode
- The fullscreen webcam view now overrides any screen orientation lock if the device is physically rotated and the rotated orientation is better suited for the webcam
  stream's aspect ratio

**1.3-beta03**

- Added detail screen for files before starting the print

**1.3-beta02**

- Show print progress on webcam fullscreen
- Allow the webcam image to be scaled up and down by double tapping the image (fit/crop)
- Show an error message when the web socket is stalled
- Show an error message when the web socket is not receiving any messages (common issue for poorly configured proxies)

**1.3-beta01**

- Ask user to manually trust a server if Android does not trust the SSL certificate
- Allow user to sign out if OctoPrint is disconnected

**1.2.5**

- Added a "show details" button to error dialogs to show the underlying issues
- Generic errors, i.e. network errors, now show the actual error message (i.e. no route to host)
- Fixed some crashes

**1.2.4**

- Fixed an issue which could lead to the webcam stream and other live data to stall after returning from terminal

**1.2.3**

- Fixed an issue on Android 6 where a trailing slash in the OctoPrint URL was not properly handled resulting in a double slash
- Fixed an issue on Android 6, 7 and 8 (exlcuding 8.1) causing the navigation bar to be white with white icons
- Fixed an issue on Android 6 causing an animation glitch on the sign in screen
- Fixed an issue on Android causing the API key input to gain focus immediatly after the sign in screen is shown

**1.2.2**

- Fixed feedback dialog

**1.2.1**

- Fixed crash in connect state

**1.2.0**

- Fix minor issues

**1.2-beta07**

- Allow terminal access during prints but prevent command input while printing

**1.2-beta06**

- Webcams with 4:3 aspect ratio set in OctoPrint settings are now shown as 4:3
- Fixed an issue causing the scroll position to be lost after returning from the tune menu, terminal or file selection

**1.2-beta05**

- Fix release issue with Google Play

**1.2-beta04**

- Changes from 1.1.3
- Removed inappropriate error messages if app loses connection in background
- Fixed action button became transparent while cancelling or pausing
- Showing cancel state in progress widget
- Trust SSL certificates installed by user to allow HTTPS on Android 8 and up

**1.2-beta03**

- Resolved an issue causing an error dialog to show up when opening the OctoPrint web interface from the overflow menu
- Resolved an issue causing an error dialog to show up if the app looses the connection in the background
- Resolved an issue causing the main action button to jump to the top of the screen in certain situations
- Resolved an issue causing the print completed notification to show up multiple times
- Removed the green tint effect from preview images as the tint did only work for yellow base images but the images can have any color

**1.2-beta02**

- Resolved an issue causing the webcam to hang after returning from the tune menu
- Resolved an issue causing all webcam frames to be processed on the UI thread causing stutters while scrolling

**1.2-beta01**

- Added terminal to be accessed from the prepare workspace
- Improved UI performance, screens appear quicker and animations are more fluent
- Gcodes can be marked as favourite by long-pressing (prepare workspace and terminal)
- Before extruding, OctoApp checks for the printer's cold extrude settings with M302
- Remove "Change Filament" option from menu as not every printer supports this

**1.2-alpha01**

- Added "Tune" menu allowing to set fan speed, feed rate and flow rate
- After sending a Gcode, you can now see the printer's response
- Internal restructuring
- Fixes from 1.1.1 and 1.1.2

**1.1.3**

- Fixed a issue with the webcam URL where URL queries where not properly encoded

**1.1.2**

- Fixed an issue where webcam stream URLs with queries got not resolved correctly

**1.1.1**

- Fixed an issue where thumbnail and relative webcam URLs are not correctly parsed for Octoprint instances not hosted in the servers root

**1.1.0**

- No changes from 1.1-beta10

**1.1-beta10**

- Fix crash if app does not recognize ETA estimation method

**1.1-beta09**

- Fixed an issue where connection failed if the "AUTO" port failed to connect

**1.1-beta08**

- Fixed typos
- Solved an issue where the app would show "printer offline" if the app is started during a print
- Set jog speed to 4000mm/min (used to be min speed)
- App now sends `reauth` message to event stream when required
- ETA and print name are now shown
- Estimation is now shown in the same fashion as in the web interface (colored dot)
- When the printer is offline, a button to open the web interface was added
- Set supported OctoPrint version to 1.4.2
- The Gcode shortcuts show now the last 5 used gcodes
- Smaller stability issues solved

**1.1-beta07**

- Remove faulty URL verification

**1.1-beta06**

- Improve logging for validation issues

**1.1-beta05**

- Fix issue causing the launcher icon to have dark mode colors
- Fix an issue in dark mode causing error texts and loading indicators of the webcam to be invisible
- Fix an issue causing the login on instances where OctoPrint is in an sub-path to fail

**1.1-beta04**

- Resolved issue with login for servers where OctoPrint is installed in a sub-path

**1.1-beta03**

- Update login mask to allow usage of instances which are hosted at a sub-path of the server (e.g. `http://mydomain.com/octoprint`)
- Give better hints on where to get the login info from
- Show ETA of prints in notification when connection is lost
- Fix an issue where on some devices disconnect messages play a sounds (now silent)

**1.1-beta02**

- Fix crash on login screen

**1.1-beta01**

- Add device and app info to feedback reports
- Hide webcam widget when no webcam is configured
- Refinements to dark mode
- Improve animation performance when switching workspaces
- Improve file list screen, allowing for pull-to-refresh and adding an error state
- Hiding global action button whens scrolling down to allowing the entire screen to be used
- Fix an issue with thumbnails causing them to be uni-colored

**1.1-alpha05**

- Fixed an issue where every time the progress notification was updated a sound was played
- Fixed an issue where the app would not properly handle relative URLs for the webcam stream
- Added dark mode (depends on system settings)

**1.1-alpha04**

- Use "AUTO" as port for connections (used to be always the first available port)
- Support Thumbnails in the file list
- Update style off "Turn off PSU" button in connect workspace to make it more distinguishable from the " Turn PSU on" button

**1.1-alpha03**

- Fixed versioning issue with 1.0-alpha02

**1.1-alpha02**

- Show progress notification during prints
- Applied changes from 1.0-beta21

**1.1-alpha1**

- Add webcam widget

**1.0.0**

- No changes from 1.0-beta22

**1.0-beta22**

- Use "AUTO" as port for connections (used to be always the first available port)

**1.0-beta21**

- Solved layout issues in print state

**1.0-beta20**

- Fixed an issue causing new users to be stuck in login since 1.0-beta19

**1.0-beta19**

- Resolved an issue where users with enabled authentication enabled on their OctoPrint instance got stuck in the connect state
- Added warnings after sign in if the OctoPrint instance is a newer version than tested or the user has no admin rights

**1.0-beta18**

- Add explicit state for when printer is connected but app did not load UI yet

**1.0-beta17**

- Fixed a connection issue occuring with the virtual printer
- Resolved and error occuring when attempting to list files but no files are available
- Resolved an issue causing PSU controls to be shown in the context menu although the PSU plugin is not available

**1.0-beta16**

- Improved logging on network level

**1.0-beta15**

- Fixed crash when request to conenct printer timed out after 10s

**1.0-beta14**

- More logs and potential fixes for timeout crash
- Fixed an issue causing "-1" to be the default port in the sign in mask

**1.0-beta12**

- Log errors occured in `EventWebSocket`
- Do not log exceptions with level "warn" or less to Firebase

**1.0-beta11**

Bump version to 1.0-beta11

**1.0-beta10**

Bump version to 1.0-beta09

**1.0-beta09**

Bump version to 1.0-beta09

**1.0-beta08**

Merge branch '52-crash-when-app-is-closed-during-check-for-update' into 'master'

Resolve "Crash when app is closed during check for update"

Closes #52

See merge request crysxd/octoapp!22

**1.0-beta07**

Merge branch '46-stuck-after-login' into 'master'

Resolve "Stuck after login"

Closes #46

See merge request crysxd/octoapp!19

**1.0-beta06**

Bump version to 1.0-beta06

**1.0-beta05**

Fix wrong docker image used

**1.0-beta04**

Bump version to 1.0-beta04

**1.0-beta03**

Bump version to 1.0-beta3

**1.0-beta02**

**1.0-beta01**

Merge branch '22-finish-up-release-1-0' into 'master'

Resolve "Finish up release 1.0"

Closes #22

See merge request crysxd/octoapp!12

**1.0-alpha30**

**1.0-alpha29**

**1.0-alpha28**

**1.0-alpha27**

**1.0-alpha26**

**1.0-alpha25**

**1.0-alpha24**

**1.0-alpha23**

**1.0-alpha22**

**1.0-alpha21**

**1.0-alpha20**

**1.0-alpha19**

**1.0-alpha18**

**1.0-alpha17**

**1.0-alpha16**

**1.0-alpha15**

**1.0-alpha14**

**1.0-alpha13**

**1.0-alpha12**

**1.0-alpha11**

**1.0-alpha9**

Bump version to 1.0-alpha9

**1.0-alpha8**

Bump version to 1.0-alpha8

**1.0-alpha7**

Go back to Gradle 6.5 and build new version

**1.0-alpha6**

Bump verion to 1.0-alpha6

**1.0-alpha5**

Bump version to 1.0-alpha5

**1.0-alpha3**

Bump version to 1.0-alpa3

**1.0-alpha2**

Build debug and release builds

**1.0-alpah10**

Bump version to 1.0-alpha10

**1-6-beta04**

- No changes from 1.6-beta03, fixed Google Play Release

