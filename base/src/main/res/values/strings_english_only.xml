<?xml version="1.0" encoding="utf-8"?>
<resources>
    <!-- Privacy menu -->
    <string name="privacy_menu___title" translatable="false">Privacy</string>
    <string name="privacy_menu___subtitle" translatable="false">OctoApp does not collect any personalized information. I do not have any data about you, such as your email address. Even if you have an active subscription, you are completely anonymous.</string>
    <string name="privacy_menu___bottom_text" translatable="false"><![CDATA[All data collected is in the app\'s legitimate interest. If you have any questions, please reach out to <a href="mailto">hello@octoapp.eu</a><br><br><a href="https://octoapp-4e438.web.app/privacy">Data privacy statement</a><br><br>OctoApp is an <a href="https://gitlab.com/crysxd/octoapp">open source</a> project.]]></string>
    <string name="privacy_menu___crash_reporting_title" translatable="false">Anonymous crash reporting</string>
    <string name="privacy_menu___crash_reporting_description" translatable="false">If enabled, OctoApp sends automated crash reports to Firebase Crashlytics, helping me make the app stable for all users and devices. These reports are anonymous and deleted after 90 days. They contain generic information about your device, such as the model or Android version and the most recent logs collected by the app. Crash reports do not contain any sensitive data as all API keys, host names or Basic Auth credentials are scrubbed (best effort) when any error is created and again before the logs are cached.</string>
    <string name="privacy_menu___analytics_title" translatable="false">Anonymous usage statistics</string>
    <string name="privacy_menu___analytics_description" translatable="false">If enabled, OctoApp sends anonymous usage information to Firebase Analytics which will be deleted after 90 days. This information helps me in determining which features of the app are used the most, and where I should spend time on improving to benefit users. This information does not contain any data except which features of the app are used and basic information about your setup, such as which plugins are installed or the OctoPrint version. The data is not linked to any advertisement profile or anything similar. If you reinstall the app or switch your device, I do not have any means to connect your data.</string>

    <!-- Discover -->
    <string name="sign_in___discovery___octoeverywhere_error_comply" translatable="false">Connect local first</string>
    <string name="sign_in___discovery___octoeverywhere_error_ignore" translatable="false">Connect OctoEverywhere now</string>
    <string name="sign_in___discovery___octoeverywhere_error_message" translatable="false">To get the most out of OctoApp and OctoEverywhere, connect your printer using the local web URL first. Once everything is set up, you can log in with your OctoEverywhere account.</string>
    <string name="sign_in___discovery___octoeverywhere_error_message_addition_shared_connection" translatable="false"><![CDATA[You need to create a shared connection on the OctoEverywhere webpage, normal OctoEverywhere URLs can\'t be used.<br><br>To get the most out of OctoApp and OctoEverywhere, connect your printer using the local web URL first. Once everything is set up, you can log in with your OctoEverywhere account.]]></string>
    <string name="sign_in___discovery___ngrok_error_message" translatable="false">You can\'t use an ngrok URL to sign in as this can have unintended side effects. Please connect your OctoPrint using the local URL first, OctoApp will then configure ngrok automatically.</string>

    <!-- Probe -->
    <string name="sign_in___probe_finding___title_basic_auth" translatable="false">Basic Authentication required</string>
    <string name="sign_in___probe_finding___explainer_basic_auth" translatable="false">Please enter your Basic Auth credentials below, not your OctoPrint user and password.</string>
    <string name="sign_in___probe_finding___title_dns_failure" translatable="false">Unable to resolve **%s**</string>
    <string name="sign_in___probe_finding___explainer_dns_failure" translatable="false">Android is not able to resolve the IP address for **%1$s**. Check following things to resolve the issue:\n\n- Ensure there is no typo\n- Make sure you are connected to the internet\n- Make sure you are on the correct WiFi network\n- Try to open [%2$s](%2$s) in your browser\n- Try using the **IP address** instead of **%1$s**</string>
    <string name="sign_in___probe_finding___title_local_dns_failure" translatable="false">Unable to resolve **%s**</string>
    <string name="sign_in___probe_finding___explainer_local_dns_failure" translatable="false">Android is not able to resolve the IP address for **%1$s**. Android devices are sometimes configured by the manufacturer in a way that they cannot reliably resolve .home, .lan or .local domains. Check following things to resolve the issue:\n\n- Ensure there is no typo\n- Make sure you are on the correct WiFi network\n- Try to open [%2$s](%2$s) in your browser\n- Try using the **IP address** instead of **%1$s**. You can use the [ipOnConnect plugin](https://plugins.octoprint.org/plugins/ipOnConnect/) to see your OctoPrint\'s IP address on your printer\'s display.</string>
    <string name="sign_in___probe_finding___title_host_unreachable" translatable="false">Unable to connect to **%s**</string>
    <string name="sign_in___probe_finding___explainer_host_unreachable" translatable="false">OctoApp resolved the IP of **%1$s**, but **%2$s** can\'t be reached within 2 seconds. Check following things to resolve the issue:\n\n- Make sure the machine hosting the server is turned on and connected\n- If your server is only available locally, make sure you are connected to the correct WiFi network\n- Try to open [%3$s](%3$s) in your browser</string>
    <string name="sign_in___probe_finding___title_https_not_trusted" translatable="false">Android does not trust **%s**</string>
    <string name="sign_in___probe_finding___explainer_https_not_trusted_weak_hostname_verification" translatable="false">The SSL certificate presented by your OctoPrint can not be used to establish a HTTPS connection because Android does not trust the certificate. This is a common issue for self-signed certificates that were not installed on this phone.\n\nAdditionally, the certificate uses a deprecated format that does not comply to modern security standards. A "subject alternative name" (SAN) field is required from Android 9 onwards.\n\nOctoApp can add this certificate to a trusted list and will bypass Android’s check for this certificate.</string>
    <string name="sign_in___probe_finding___explainer_https_not_trusted" translatable="false">The SSL certificate presented by your OctoPrint can not be used to establish a HTTPS connection because Android does not trust the certificate. This is a common issue for self-signed certificates that were not installed on this phone.\n\nOctoApp can add this certificate to a trusted list and will bypass Android’s check for this certificate.</string>
    <string name="sign_in___probe_finding___explainer_https_not_trusted_no_cert" translatable="false">OctoApp was not able to communicate with **%s** over HTTPS. This indicates an incorrectly configured server as OctoApp also failed to extract the certificate from your server. Make sure whether you should use **HTTP instead of HTTP\_S\_!**</string>
    <string name="sign_in___probe_finding___title_url_syntax" translatable="false">URL syntax error</string>
    <string name="sign_in___probe_finding___explainer_url_syntax" translatable="false">The URL **%1$s** seems to contain a syntax error and can\'t be parsed. Android reports following error:\n\n**%2$s**</string>
    <string name="sign_in___probe_finding___title_octoprint_not_found" translatable="false">OctoPrint not found</string>
    <string name="sign_in___probe_finding___explainer_octoprint_not_found" translatable="false">OctoApp was able to connect to **%1$s** but received a 404 response. Check following things to resolve the issue:\n\n- Make sure you use the correct port and host\n\n- Make sure that you use the correct path in the URL\n\n- Make sure you have a recent version of OctoPrint, OctoApp is not compatible with very old versions (e.g. **PrusaPrint OctoPrint**)\n\n- Try to open [%1$s](**%1$s**) in your browser</string>
    <string name="sign_in___probe_finding___title_port_closed" translatable="false">Connected to **%1$s** but port **%2$d** is closed</string>
    <string name="sign_in___probe_finding___explainer_port_closed" translatable="false">OctoApp was able to connect to **%1$s** but port **%2$d** is closed. Check following things to resolve the issue:\n\n\n- Make sure **%2$d** is correct. If you don\'t specify the port explicitly, OctoApp will use 80 for HTTP and 443 for HTTPS\n\n- Try to open [%3$s](%3$s) in your browser</string>
    <string name="sign_in___probe_finding___title_failed_to_connect_via_http" translatable="false">Failed to connect to **%s** via HTTP</string>
    <string name="sign_in___probe_finding___explainer_failed_to_connect_via_http" translatable="false">OctoApp was able to communicate with **%1$s**, but when trying to establish a HTTP(S) connection an unexpected error occurred. Android reports following issue:\n\n**%2$s**\n\nYou can **long-press \"Need help?\"** above to get support (please include logs).</string>
    <string name="sign_in___probe_finding___title_unexpected_issue" translatable="false">Unexpected issue</string>
    <string name="sign_in___probe_finding___explainer_unexpected_issue" translatable="false">OctoApp encountered an unexpected error. Android reports following issue:\n\n**%1$s**\n\nYou can **long-press \"Need help?\"** above to get support (please include logs).</string>
    <string name="sign_in___probe_finding___title_might_not_be_octoprint" translatable="false">**%s** might not be an OctoPrint</string>
    <string name="sign_in___probe_finding___explainer_might_not_be_octoprint" translatable="false">OctoApp was able to communicate with **%1$s**, but the server seems not to be a recent version of OctoPrint.\n\nYou can continue, but other issues may arise in the following steps.</string>
    <string name="sign_in___probe_finding___title_websocket_upgrade_failed" translatable="false">HTTP access to **%s** works, but web socket broken</string>
    <string name="sign_in___probe_finding___explainer_websocket_upgrade_failed" translatable="false">OctoApp can communicate with **%1$s**, but the web socket failed to connect with response code **%2$d**. This is a very common issue with incorrectly configured reverse proxy setups. Check following things to resolve the issue:\n\n- Make sure your proxy sets the `Upgrade: WebSocket` header for `%3$s` as it is not forwarded to OctoPrint by default\n- Refer to the [configuration examples in the OctoPrint community](https://community.octoprint.org/t/reverse-proxy-configuration-examples/1107) for Nginx, HAProxy, Apache and others</string>

    <!-- Webcam test -->
    <string name="help___webcam_troubleshooting___title_basic_auth" translatable="false">Basic Authentication required</string>
    <string name="help___webcam_troubleshooting___explainer_basic_auth" translatable="false">You need to add the Basic Authentication credentials to the webcam stream URL in your OctoPrint settings (webinterface). Use this URL and fill in your username and password:\n\n%1$s</string>
    <string name="help___webcam_troubleshooting___title_dns_failure" translatable="false">@string/sign_in___probe_finding___title_dns_failure</string>
    <string name="help___webcam_troubleshooting___explainer_dns_failure" translatable="false">@string/sign_in___probe_finding___explainer_dns_failure</string>
    <string name="help___webcam_troubleshooting___title_local_dns_failure" translatable="false">@string/sign_in___probe_finding___title_local_dns_failure</string>
    <string name="help___webcam_troubleshooting___explainer_local_dns_failure" translatable="false">@string/sign_in___probe_finding___explainer_local_dns_failure</string>
    <string name="help___webcam_troubleshooting___title_host_unreachable" translatable="false">@string/sign_in___probe_finding___title_host_unreachable</string>
    <string name="help___webcam_troubleshooting___explainer_host_unreachable" translatable="false">@string/sign_in___probe_finding___explainer_host_unreachable</string>
    <string name="help___webcam_troubleshooting___title_https_not_trusted" translatable="false">@string/sign_in___probe_finding___title_https_not_trusted</string>
    <string name="help___webcam_troubleshooting___explainer_https_not_trusted_weak_hostname_verification" translatable="false">@string/sign_in___probe_finding___explainer_https_not_trusted_weak_hostname_verification</string>
    <string name="help___webcam_troubleshooting___explainer_https_not_trusted" translatable="false">@string/sign_in___probe_finding___explainer_https_not_trusted</string>
    <string name="help___webcam_troubleshooting___title_url_syntax" translatable="false">@string/sign_in___probe_finding___title_url_syntax</string>
    <string name="help___webcam_troubleshooting___explainer_url_syntax" translatable="false">@string/sign_in___probe_finding___explainer_url_syntax</string>
    <string name="help___webcam_troubleshooting___title_webcam_not_found" translatable="false">Webcam not found</string>
    <string name="help___webcam_troubleshooting___explainer_webcam_not_found" translatable="false">OctoApp was able to connect to **%1$s** but received a 404 response. Check following things to resolve the issue:\n\n- Make sure you use the correct port and host\n\n- Make sure that you use the correct path in the URL. If the stream URL you entered in the OctoPrint settings starts with `/`, OctoApp will treat the stream URL as a absolute path of OctoPrint host. If the stream URL you entered starts with `http`, OctoApp will treat the stream URL as a complete URL and not modify it. In any other case, OctoApp treats the stream URL as a relative path which is appended to your OctoPrint\'s web url.\n- If you try to reach your webcam from outside your home and you configured remote access with OctoApp, be aware that you need to host the webcam on the same host as OctoPrint or you need to expose your webcam separatly to the internet and use a absolute URL as stream URL\n- Try to open [%1$s](**%1$s**) in your browser</string>
    <string name="help___webcam_troubleshooting___title_port_closed" translatable="false">@string/sign_in___probe_finding___title_port_closed</string>
    <string name="help___webcam_troubleshooting___explainer_port_closed" translatable="false">@string/sign_in___probe_finding___explainer_port_closed</string>
    <string name="help___webcam_troubleshooting___title_failed_to_connect_via_http" translatable="false">@string/sign_in___probe_finding___title_failed_to_connect_via_http</string>
    <string name="help___webcam_troubleshooting___explainer_failed_to_connect_via_http" translatable="false">@string/sign_in___probe_finding___explainer_failed_to_connect_via_http</string>
    <string name="help___webcam_troubleshooting___title_unexpected_issue" translatable="false">@string/sign_in___probe_finding___title_unexpected_issue</string>
    <string name="help___webcam_troubleshooting___explainer_unexpected_issue" translatable="false">@string/sign_in___probe_finding___explainer_unexpected_issue</string>
    <string name="help___webcam_troubleshooting___title_might_not_be_webcam" translatable="false">**%s** might not be an webcam</string>
    <string name="help___webcam_troubleshooting___explainer_might_not_be_webcam" translatable="false">OctoApp was able to communicate with **%1$s**, but the server does not seem to be a webcam as the connection was instantly closed or no image was received.\n\nIf you are sure that **%2$s** is the correct host, make sure that **%1$s** is the correct path and port. If not specified, OctoApp will use port 80 for HTTP and port 443 for HTTPS.</string>
    <string name="help___webcam_troubleshooting___continue" translatable="false">Continue</string>
    <string name="help___webcam_troubleshooting___try_again" translatable="false">Try again</string>
    <string name="help___webcam_troubleshooting___trust_and_try_again" translatable="false">Trust server</string>
    <string name="help___webcam_troubleshooting___only_mjpeg_supported" translatable="false">At the moment troubleshooting is only available for MJPEG webcams</string>
    <string name="help___webcam_troubleshooting___loading_title" translatable="false">Testing your webcam…</string>
    <string name="help___webcam_troubleshooting___help" translatable="false">Need Help?</string>
    <string name="help___webcam_troubleshooting___title_webcam_is_working" translatable="false">Webcam is working</string>
    <string name="help___webcam_troubleshooting___explainer_webcam_is_working" translatable="false">OctoApp was able to load images with %.1f FPS from your webcam:</string>
    <string name="help___webcam_troubleshooting___explainer_data_saver_not_working" translatable="false">OctoApp is unable to use the data saver webcam mode because of following error:</string>
    <string name="help___webcam_troubleshooting___explainer_data_saver_working" translatable="false"><![CDATA[OctoApp was also able to use the data saver mode with images %1$s with a resolution of <b>%2$d x %3$d px</b>:]]></string>
    <string name="help___webcam_troubleshooting___explainer_data_saver_source_companion" translatable="false"><![CDATA[from the <b>OctoApp Companion plugin</b>]]></string>
    <string name="help___webcam_troubleshooting___explainer_data_saver_source_snapshot" translatable="false"><![CDATA[from <a href="%1$s">%1$s</a>]]></string>
    <string name="help___webcam_troubleshooting___explainer_data_saver_source_mjpeg" translatable="false"><![CDATA[from single frames of <a href="%1$s">%1$s</a>]]></string>
    <!-- Help -->
    <string name="help___status_x" translatable="false">Status: %s</string>
    <string name="help___octoprint_community" translatable="false">OctoPrint community</string>
    <string name="help___octoprint_discord" translatable="false">OctoPrint Discord</string>
    <string name="help___report_a_bug" translatable="false">I want to report a bug</string>
    <string name="help___ask_a_question" translatable="false">I have an other question</string>
    <string name="help___title" translatable="false"><![CDATA[FAQ & Help]]></string>
    <string name="help___introduction_part_2" translatable="false">If you are new to OctoApp, you can check out this introduction to get started with the app’s concept!</string>
    <string name="help___introduction_part_1" translatable="false">Hi! I’m Chris, the developer of OctoApp! Thanks for using the app. If you don’t find what you need below, you can always reach out! 🖖</string>
    <string name="help___introduction_video_caption" translatable="false">1 minute introduction</string>
    <string name="help___faq_title" translatable="false">FAQ</string>
    <string name="help___bugs_title" translatable="false">Known bugs in this version</string>
    <string name="help___contact_title" translatable="false">Still no answers?</string>
    <string name="help___contact_detail_information" translatable="false">I speak English and German. My current local time is %1$s (%2$s).</string>

    <!-- OctoLab -->
    <string name="lab_menu___title" translatable="false">OctoApp Lab</string>
    <string name="lab_menu___subtitle" translatable="false">Here you can find experimental settings and things I do not officially support but which you can use at your own will. Features might be removed without prior notice.</string>
    <string name="lab_menu___allow_battery_saver_title" translatable="false">Notification battery saver</string>
    <string name="lab_menu___changelog_title" translatable="false">Changelog</string>
    <string name="lab_menu___allow_battery_saver_description" translatable="false">With version 1.8, I introduced a battery saving mechanic for the print notification as it was consuming a lot of power on some devices. You can disable it to return to the old behaviour. This might resolve issues with missing filament change notifications.</string>
    <string name="lab_menu___allow_to_rotate_title" translatable="false">Allow OctoApp to rotate</string>
    <string name="lab_menu___allow_to_rotate_description" translatable="false">OctoApp can rotate into landscape mode. The UI is not optimized and rotating might cause issues on some screens. Will take affect after restart.</string>
    <string name="lab_menu___suppress_m115_request_title" translatable="false">Suppress M115 request</string>
    <string name="lab_menu___suppress_m115_request_description" translatable="false">By default OctoApp requests M115 when a printer is connected or the app is opened with a connected printer. This is used to style the Gcode preview for your printer. Suppressing the M115 will result in a generic print bed to be used.</string>
    <string name="lab_menu___allow_terminal_during_print_title" translatable="false">Allow terminal during print</string>
    <string name="lab_menu___allow_terminal_during_print_description" translatable="false">Usually OctoApp does not allow you to send Gcode commands from the terminal if a print is active. Enabling this setting bypasses the check. Be cautious.</string>
    <string name="lab_menu___suppress_remote_notification_init" translatable="false">Suppress remote notifications</string>
    <string name="lab_menu___suppress_remote_notification_init_description" translatable="false">On some devices with after-market Google Play services OctoApp can crash while setting up remote notifications. This options suppresses remote notifications from being initialized, thus preventing the crash.</string>
    <string name="lab_menu___debug_network_logging" translatable="false">Debug network logging</string>
    <string name="lab_menu___debug_network_logging_description" translatable="false"><![CDATA[
        Enabled extended logging around network requests and DNS. This will also <b>remove the obfuscation of IP addresses and hostnames and may leak credentials</b> in logs and crash reports. Only enable this when asked to, takes affect after restart.
    ]]></string>
    <string name="lab_menu___enforce_ip_v4" translatable="false">Enforce IPv4</string>
    <string name="lab_menu___enforce_ip_v4_description" translatable="false">Suppress the use of IPv6 addresses. If no IPv4 for a domain can be resolved, a connection won\'t be possible. Applies immediately.</string>
    <string name="lab_menu___record_webcam_traffic_for_debug" translatable="false">Record webcam traffic</string>
    <string name="lab_menu___record_webcam_traffic_for_debug_description" translatable="false">Records 5MiB of raw network traffic for debug purposes of the MJPEG connection and opens a share dialog. This happens when the webcam is shown next or right away if already shown.</string>
    <string name="lab_menu___compact_layout" translatable="false">Compact layout</string>
    <string name="lab_menu___compact_layout_description" translatable="false">Removes the titles in the prepare and print workspaces to save space. Applies after restart.</string>

    <!-- Play services -->
    <string name="announcement_play_services_error___text" translatable="false">Remote notifications disabled because Play Services crashed</string>
    <string name="announcement_play_services_error___learn_more" translatable="false">Learn more</string>

    <!-- Remote access -->
    <string name="configure_remote_acces___manual___error_normal_octoeverywhere_url" translatable="false">You can\'t use a normal OctoEverywhere URL. Please create a shared connection URL on octoeverywhere.com or directly sign in with OctoEverywhere (preferred)</string>
    <string name="configure_remote_acces___manual___error_shared_octoeverywhere_url" translatable="false">It\'s better to directly sign in with OctoEverywhere than using a shared connection! You can proceed if you want.</string>
    <string name="configure_remote_acces___manual___error_unable_to_verify" translatable="false">Unable to verify that the remote URL points to the same instance</string>
    <string name="configure_remote_acces___manual___error_unable_to_connect" translatable="false">Unable to connect</string>
    <string name="configure_remote_acces___manual___error_invalid_url" translatable="false">Please provide a valid URL (including http:// or https://)</string>
    <string name="plugin_library___octoapp_tutorial" translatable="false">OctoApp Tutorial</string>
    <string name="plugin_library___configure" translatable="false">Configure</string>
    <string name="plugin_library___plugin_page" translatable="false">Plugin page</string>
    <string name="plugin_library___title" translatable="false">Supported Plugins</string>
    <string name="plugin_library___description" translatable="false">OctoApp supports many OctoPrint plugins. You can use plugins to customize OctoApp and add more functionality to it.</string>
    <string name="configure_remote_acces___spaghetti_detective___custom_button" translatable="false">Use custom instance</string>
    <string name="configure_remote_acces___spaghetti_detective___custom_title" translatable="false">Use custom TSD</string>
    <string name="configure_remote_acces___spaghetti_detective___custom_action" translatable="false">Connect</string>
    <string name="configure_remote_acces___spaghetti_detective___custom_hint" translatable="false">Base URL</string>
    <string name="configure_remote_acces___spaghetti_detective___custom_error" translatable="false">Enter a valid HTTP(S) URL</string>

    <!-- Misc -->
    <string name="print_confidence_disclaimer_octoeverywhere" translatable="false">Gadget by\nOctoEverywhere</string>
    <string name="print_confidence_disclaimer_obico" translatable="false">powered by\nObico</string>
</resources>