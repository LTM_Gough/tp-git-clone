package de.crysxd.octoapp.base.migrations

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.di.SharedBaseInjector
import timber.log.Timber

class PinnedMenuItemMigration(val context: Context) {

    fun migrate() = try {
        val ds = LocalPinnedMenuItemsDataSource(context)
        if (ds.hasAny()) {
            MenuId2.values().forEach {
                Timber.i("PinnedMenuItemMigration/$it running...")
                val items = ds.load(it)
                val newKey = when (it) {
                    MenuId2.MainMenu -> MenuId.MainMenu
                    MenuId2.PrintWorkspace -> MenuId.PrintWorkspace
                    MenuId2.PrePrintWorkspace -> MenuId.PrePrintWorkspace
                    MenuId2.Widget -> MenuId.Widget
                    MenuId2.Other -> MenuId.Other
                }
                SharedBaseInjector.get().pinnedMenuItemRepository.import(newKey, items)
                Timber.i("Mapped ${items?.size} items")
            }
        } else {
            Timber.i("PinnedMenuItemMigration is done")
        }
        ds.delete()
    } catch (e: Exception) {
        Timber.e(e, "Failed migration")
    }

    private class LocalPinnedMenuItemsDataSource(
        private val context: Context,
        private val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context),
    ) {

        fun hasAny() = MenuId2.values().any {
            sharedPreferences.contains(getKey(it))
        }

        fun delete() = sharedPreferences.edit {
            MenuId2.values().forEach {
                remove(getKey(it))
            }
        }

        fun load(menuId: MenuId2): Set<String>? = getKey(menuId)?.let {
            sharedPreferences.getStringSet(it, null)
        }

        private fun getKey(menuId: MenuId2) = when (menuId) {
            MenuId2.MainMenu -> "pinned_menu_items"
            MenuId2.PrePrintWorkspace -> "pinned_menu_items_preprint"
            MenuId2.PrintWorkspace -> "pinned_menu_items_print"
            MenuId2.Widget -> "pinned_menu_items_widget"
            MenuId2.Other -> null
        }
    }

    enum class MenuId2 {
        MainMenu,
        PrintWorkspace,
        PrePrintWorkspace,
        Widget,
        Other
    }
}