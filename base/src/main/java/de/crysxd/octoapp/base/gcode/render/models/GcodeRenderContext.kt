package de.crysxd.octoapp.base.gcode.render.models

import android.graphics.PointF
import android.graphics.RectF

data class GcodeRenderContext(
    val previousLayerPaths: List<GcodePath>?,
    val completedLayerPaths: List<GcodePath>,
    val remainingLayerPaths: List<GcodePath>?,
    val printHeadPosition: PointF?,
    val gcodeBounds: RectF?,
    val layerCount: Int,
    val layerNumber: Int,
    val layerZHeight: Float,
    val layerProgress: Float,
    val layerNumberDisplay: (Int) -> Int,
    val layerCountDisplay: (Int) -> Int,
) {
    companion object {
        val Empty
            get() = GcodeRenderContext(
                previousLayerPaths = null,
                completedLayerPaths = emptyList(),
                remainingLayerPaths = null,
                printHeadPosition = null,
                gcodeBounds = null,
                layerCountDisplay = { it },
                layerNumberDisplay = { it },
                layerCount = 0,
                layerNumber = 0,
                layerProgress = 0f,
                layerZHeight = 0f,
            )
    }
}