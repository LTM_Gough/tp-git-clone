package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.exceptions.MissingPluginException
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import timber.log.Timber
import javax.inject.Inject


class TriggerInitialCancelObjectMessageUseCase @Inject constructor(
    val octoPrintProvider: OctoPrintProvider,
    val octoPrintRepository: OctoPrintRepository,
) : UseCase<TriggerInitialCancelObjectMessageUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, timber: Timber.Tree) {
        if (octoPrintRepository.get(param.instanceId)?.hasPlugin(OctoPlugins.CancelObject) != true) {
            throw MissingPluginException(OctoPlugins.CancelObject)
        }

        octoPrintProvider.octoPrint(param.instanceId).asOctoPrint().cancelObjectApi.triggerInitialMessage()
    }

    data class Params(
        val instanceId: String?
    )
}