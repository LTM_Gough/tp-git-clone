package de.crysxd.octoapp.base.data.repository

import android.content.SharedPreferences
import androidx.core.content.edit
import de.crysxd.octoapp.base.ext.nextAfter

class NotificationIdRepository(
    private val sharedPreferences: SharedPreferences,
    private val octoPrintRepository: OctoPrintRepository,
) {
    companion object {
        private val UPLOAD_STATUS_NOTIFICATION = 14_998
        private const val PRINT_STATUS_FALLBACK_NOTIFICATION_ID = 14_999
        private const val PRINT_EVENT_FALLBACK_NOTIFICATION_ID = 14_998
        private const val PRINT_COMPLETED_FALLBACK_NOTIFICATION_ID = 14_997
        private const val BEEP_FALLBACK_NOTIFICATION_ID = 14_996
        private val PRINT_STATUS_NOTIFICATION_ID_RANGE = 15_000..15_099
        private val PRINT_EVENT_NOTIFICATION_ID_RANGE = 15_100..15_199
        private val PRINT_EVENT_COMPLETED_ID_RANGE = 15_200..15_299
        private val UPDATE_NOTIFICATION_ID_RANGE = 15_200..15_299
        private val BEEP_NOTIFICATION_ID_RANGE = 15_200..15_299
        private const val REQUEST_ACCESS_COMPLETED_NOTIFICATION_ID = 3432
        private const val WEAR_OS_BUG_REPORT_NOTIFICATION_ID = 3433

        private const val KEY_PRINT_STATUS_NOTIFICATION_PREFIX = "notification-id-"
        private const val KEY_BEEP_NOTIFICATION_PREFIX = "beep-notification-id-"
        private const val KEY_PRINT_EVENT_NOTIFICATION_PREFIX = "event-notification-id-"
        private const val KEY_PRINT_COMPLETED_NOTIFICATION_PREFIX = "completed-notification-id-"
        private const val KEY_LAST_UPDATE_NOTIFICATION_ID = "last-update-notification-id"
    }

    val requestAccessCompletedNotificationId = REQUEST_ACCESS_COMPLETED_NOTIFICATION_ID

    fun getPrintStatusNotificationId(instanceId: String?) =
        generateNotificationId(instanceId, KEY_PRINT_STATUS_NOTIFICATION_PREFIX, PRINT_STATUS_NOTIFICATION_ID_RANGE) ?: PRINT_STATUS_FALLBACK_NOTIFICATION_ID

    fun getPrintEventNotificationId(instanceId: String?) =
        generateNotificationId(instanceId, KEY_PRINT_EVENT_NOTIFICATION_PREFIX, PRINT_EVENT_NOTIFICATION_ID_RANGE) ?: PRINT_EVENT_FALLBACK_NOTIFICATION_ID

    fun getPrintCompletedNotificationId(instanceId: String?) =
        generateNotificationId(instanceId, KEY_PRINT_COMPLETED_NOTIFICATION_PREFIX, PRINT_EVENT_COMPLETED_ID_RANGE) ?: PRINT_COMPLETED_FALLBACK_NOTIFICATION_ID

    fun getBeepNotificationId(instanceId: String?) =
        generateNotificationId(instanceId, KEY_BEEP_NOTIFICATION_PREFIX, BEEP_NOTIFICATION_ID_RANGE) ?: BEEP_FALLBACK_NOTIFICATION_ID

    fun nextUpdateNotificationId(): Int {
        val last = sharedPreferences.getInt(KEY_LAST_UPDATE_NOTIFICATION_ID, 0)
        val next = UPDATE_NOTIFICATION_ID_RANGE.nextAfter(last)

        sharedPreferences.edit {
            putInt(KEY_LAST_UPDATE_NOTIFICATION_ID, next)
        }
        return next
    }

    fun getUploadStatusNotificationId() = UPLOAD_STATUS_NOTIFICATION

    fun getWearOsBugReportNotificationId() = WEAR_OS_BUG_REPORT_NOTIFICATION_ID

    private fun generateNotificationId(instanceId: String?, settingsPrefix: String, range: IntRange): Int? {
        instanceId ?: return null
        val key = "$settingsPrefix$instanceId"

        // Clean up
        val notificationIdKeys = sharedPreferences.all.keys.filter { it.startsWith(settingsPrefix) }
        val toDelete = notificationIdKeys.filter {
            val id = it.removePrefix(settingsPrefix)
            octoPrintRepository.getAll().none { it.id == id }
        }
        sharedPreferences.edit {
            toDelete.forEach { remove(it) }
        }

        // Find allocated or create new
        return sharedPreferences.getInt(key, -1).takeIf { it >= 0 } ?: let {
            // Nothing allocated yet...allocate a new notification id for this instance
            // Find min free
            val notificationIds = sharedPreferences.all.filter { it.key.startsWith(settingsPrefix) }.mapNotNull { it.value as? Int }
            val notificationId = range.firstOrNull {
                !notificationIds.contains(it)
            } ?: (notificationIds.maxOf { it } + 1)

            // Store and return
            sharedPreferences.edit { putInt(key, notificationId) }
            notificationId
        }
    }
}