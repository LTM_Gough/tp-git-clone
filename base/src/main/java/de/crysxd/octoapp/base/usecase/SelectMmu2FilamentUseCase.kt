package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.asOctoPrint
import timber.log.Timber
import javax.inject.Inject

class SelectMmu2FilamentUseCase @Inject constructor(private val octoPrintProvider: OctoPrintProvider) : UseCase<SelectMmu2FilamentUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, timber: Timber.Tree) {
        octoPrintProvider.octoPrint().asOctoPrint().mmu2FilamentSelectApi.selectChoice(param.choice)
    }

    sealed class Params(val choice: Int, val toolIndexLabel: Int) {
        object Tool0 : Params(choice = 0, toolIndexLabel = 1)
        object Tool1 : Params(choice = 1, toolIndexLabel = 2)
        object Tool2 : Params(choice = 2, toolIndexLabel = 3)
        object Tool3 : Params(choice = 3, toolIndexLabel = 4)
        object Tool4 : Params(choice = 4, toolIndexLabel = 5)
        object Cancel : Params(choice = 5, toolIndexLabel = -1)

        companion object {
            fun forToolIndex(toolIndex: Int) = when (toolIndex) {
                0 -> Tool0
                1 -> Tool1
                2 -> Tool2
                3 -> Tool3
                4 -> Tool4
                else -> throw IllegalArgumentException("Unsupported tool index $toolIndex")
            }
        }
    }
}