package de.crysxd.octoapp.base.di.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import de.crysxd.octoapp.base.di.BaseScope
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GetAppLanguageUseCase
import java.util.Locale
import javax.inject.Named

@Module
open class AndroidModule(private val app: Application) {

    companion object {
        const val LOCALIZED = "localized"
    }

    @Provides
    open fun provideApp() = app

    @Provides
    open fun provideContext(): Context = app.applicationContext

    @Provides
    @Named(LOCALIZED)
    open fun provideLocalizedContext(appLanguageUseCase: GetAppLanguageUseCase, context: Context): Context {
        val language = Locale.forLanguageTag(appLanguageUseCase.executeBlocking(Unit).appLanguage)

        return language.let {
            val config = context.resources.configuration
            config.setLocale(it)
            context.createConfigurationContext(config)
        } ?: context
    }

    @Provides
    open fun sharedPreferences(context: Context): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)

    @Provides
    @BaseScope
    open fun provideOctoPreferences(
        sharedPreferences: SharedPreferences,
    ) = SharedBaseInjector.get().preferences
}