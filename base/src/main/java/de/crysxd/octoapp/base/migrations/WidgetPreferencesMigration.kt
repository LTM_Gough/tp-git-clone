package de.crysxd.octoapp.base.migrations

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import de.crysxd.octoapp.base.data.repository.WidgetPreferencesRepository
import de.crysxd.octoapp.base.di.SharedBaseInjector
import timber.log.Timber
import de.crysxd.octoapp.base.data.models.WidgetPreferences as New
import de.crysxd.octoapp.base.data.models.WidgetType as NewType

class WidgetPreferencesMigration(private val context: Context) {

    fun migrate() {
        listOf(
            WidgetPreferencesRepository.LIST_PRINT,
            WidgetPreferencesRepository.LIST_CONNECT,
            WidgetPreferencesRepository.LIST_PREPARE,
        ).forEach {
            val ds = WidgetPreferencesDataSource(context = context, listId = it)
            if (ds.hasAny()) {
                Timber.i("WidgetPreferencesMigration/$it running...")
                ds.loadOrder(it)?.let { old ->
                    val new = New(
                        listId = old.listId,
                        hidden = old.hidden.map { it.map() },
                        items = old.items.map { it.map() },
                    )
                    Timber.i("Mapped $it")
                    SharedBaseInjector.get().widgetPreferencesRepository.setWidgetOrder(it, new)
                    ds.delete()
                }
            }
            Timber.i("WidgetPreferencesMigration/$it is done")
        }

    }

    private class WidgetPreferencesDataSource(
        private val context: Context,
        private val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context),
        private val gson: Gson = Gson(),
        listId: String
    ) {
        private val key = "widget_prefs_2_$listId"

        fun hasAny(): Boolean {
            return sharedPreferences.getString(key, "[]") != "[]"
        }

        fun delete() {
            sharedPreferences.edit { remove(key) }
        }

        fun loadOrder(listId: String): WidgetPreferences? {
            val string = sharedPreferences.getString(key, null)
            string ?: return null
            return gson.fromJson(string, WidgetPreferences::class.java)
        }
    }


    private data class WidgetPreferences(
        val listId: String,
        val items: List<WidgetType>,
        val hidden: List<WidgetType>,
    )

    private enum class WidgetType {
        @SerializedName("AnnouncementWidget")
        AnnouncementWidget,

        @SerializedName("ControlTemperatureWidget")
        ControlTemperatureWidget,

        @SerializedName("ExtrudeWidget")
        ExtrudeWidget,

        @SerializedName("GcodePreviewWidget")
        GcodePreviewWidget,

        @SerializedName("MoveToolWidget")
        MoveToolWidget,

        @SerializedName("PrePrintQuickAccessWidget")
        PrePrintQuickAccessWidget,

        @SerializedName("PrintQuickAccessWidget")
        PrintQuickAccessWidget,

        @SerializedName("ProgressWidget")
        ProgressWidget,

        @SerializedName("QuickAccessWidget")
        QuickAccessWidget,

        @SerializedName("SendGcodeWidget")
        SendGcodeWidget,

        @SerializedName("TuneWidget")
        TuneWidget,

        @SerializedName("WebcamWidget")
        WebcamWidget,

        @SerializedName("QuickPrint")
        QuickPrint,

        @SerializedName("CancelObject")
        CancelObject,
    }

    private fun WidgetType.map(): NewType = when (this) {
        WidgetType.AnnouncementWidget -> NewType.AnnouncementWidget
        WidgetType.ControlTemperatureWidget -> NewType.ControlTemperatureWidget
        WidgetType.ExtrudeWidget -> NewType.ExtrudeWidget
        WidgetType.GcodePreviewWidget -> NewType.GcodePreviewWidget
        WidgetType.MoveToolWidget -> NewType.MoveToolWidget
        WidgetType.PrePrintQuickAccessWidget -> NewType.PrePrintQuickAccessWidget
        WidgetType.PrintQuickAccessWidget -> NewType.PrintQuickAccessWidget
        WidgetType.ProgressWidget -> NewType.ProgressWidget
        WidgetType.QuickAccessWidget -> NewType.QuickAccessWidget
        WidgetType.SendGcodeWidget -> NewType.SendGcodeWidget
        WidgetType.TuneWidget -> NewType.TuneWidget
        WidgetType.WebcamWidget -> NewType.WebcamWidget
        WidgetType.QuickPrint -> NewType.QuickPrint
        WidgetType.CancelObject -> NewType.CancelObject
    }
}