package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.models.connection.ConnectionState
import timber.log.Timber
import javax.inject.Inject

class GetPrinterConnectionUseCase @Inject constructor(
    private val octoPrintProvider: OctoPrintProvider
) : UseCase<GetPrinterConnectionUseCase.Params, ConnectionState>() {

    override suspend fun doExecute(param: Params, timber: Timber.Tree) =
        octoPrintProvider.octoPrint(param.instanceId).connectionApi.getConnection()

    data class Params(
        val instanceId: String? = null
    )
}