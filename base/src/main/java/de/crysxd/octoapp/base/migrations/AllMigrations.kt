package de.crysxd.octoapp.base.migrations

import android.content.Context
import timber.log.Timber

class AllMigrations(val context: Context) {

    fun migrate() {
        Timber.i("Running migrations...")
        OctoPreferencesMigration(context).migrate()
        ExtrusionHistoryMigration(context).migrate()
        GcodeHistoryMigration(context).migrate()
        OctoPrintInstanceInformationMigration(context).migrate()
        DnsCacheMigration(context).migrate()
        WidgetPreferencesMigration(context).migrate()
        PinnedMenuItemMigration(context).migrate()
    }
}