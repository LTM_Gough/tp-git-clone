package de.crysxd.octoapp.base.ext

import androidx.core.text.HtmlCompat
import de.crysxd.octoapp.base.R
import de.crysxd.octoapp.base.utils.ComparableVersion
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_ANALYSIS
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_AVERAGE
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_ESTIMATE
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_GENIUS
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_LINEAR
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_MIXED_ANALYSIS
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_MIXED_AVERAGE
import java.net.URLDecoder
import java.net.URLEncoder

fun String?.asPrintTimeLeftOriginColor() = when (this) {
    ORIGIN_LINEAR -> R.color.analysis_bad
    ORIGIN_ANALYSIS, ORIGIN_MIXED_ANALYSIS -> R.color.analysis_normal
    ORIGIN_AVERAGE, ORIGIN_MIXED_AVERAGE, ORIGIN_ESTIMATE -> R.color.analysis_good
    ORIGIN_GENIUS -> R.color.yellow
    else -> android.R.color.transparent
}

fun String.urlEncode(): String = URLEncoder.encode(this, "UTF-8")
fun String.urlDecode(): String = URLDecoder.decode(this, "UTF-8")
fun String.toHtml() = HtmlCompat.fromHtml(this.replace("\n", "<br>"), HtmlCompat.FROM_HTML_MODE_COMPACT)
fun String.asVersion() = ComparableVersion(this)