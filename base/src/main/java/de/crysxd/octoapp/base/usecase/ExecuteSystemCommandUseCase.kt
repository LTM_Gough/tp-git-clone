package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.models.system.SystemCommand
import timber.log.Timber
import javax.inject.Inject

class ExecuteSystemCommandUseCase @Inject constructor(private val octoPrintProvider: OctoPrintProvider) : UseCase<ExecuteSystemCommandUseCase.Params, Unit>() {
    override suspend fun doExecute(param: Params, timber: Timber.Tree) {
        octoPrintProvider.octoPrint(param.instanceId).systemApi.executeSystemCommand(param.systemCommand)
    }

    data class Params(
        val systemCommand: SystemCommand,
        val instanceId: String? = null,
    )
}