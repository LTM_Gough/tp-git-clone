package de.crysxd.octoapp.base.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.datetime.Instant
import kotlinx.parcelize.Parcelize
import kotlin.math.roundToLong

@Parcelize
data class FcmPrintEvent(
    val fileName: String?,
    val progress: Float?,
    val type: Type?,
    val serverTime: Double?,
    val serverTimePrecise: Double?,
    val timeLeft: Long?,
    val printId: String?,
) : Parcelable {

    val serverTimeFixed: Instant
        get() = when {
            serverTimePrecise != null -> Instant.fromEpochMilliseconds((serverTimePrecise * 1000).roundToLong())
            serverTime != null -> Instant.fromEpochSeconds(serverTime.roundToLong())
            else -> Instant.fromEpochSeconds(0)
        }

    enum class Type {
        @SerializedName("printing")
        Printing,

        @SerializedName("paused")
        Paused,

        @SerializedName("paused_gcode")
        PausedFromGcode,

        @SerializedName("beep")
        Beep,

        @SerializedName("completed")
        Completed,

        @SerializedName("filament_required")
        FilamentRequired,

        @SerializedName("idle")
        Idle,

        @SerializedName("mmu_filament_selection_started")
        Mmu2FilamentSelectionStarted,

        @SerializedName("mmu_filament_selection_completed")
        Mmu2FilamentSelectionCompleted,
    }
}

