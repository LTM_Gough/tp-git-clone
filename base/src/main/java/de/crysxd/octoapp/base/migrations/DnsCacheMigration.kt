package de.crysxd.octoapp.base.migrations

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.base.network.CachedLocalDnsResolver
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Instant
import timber.log.Timber
import java.io.File
import java.io.FileNotFoundException
import java.util.Date

class DnsCacheMigration(private val context: Context) {

    fun migrate() = runBlocking(Dispatchers.IO) {
        val cacheFile = File(context.cacheDir, "dns2.json")

        try {
            if (cacheFile.exists()) {
                Timber.i("DnsCacheMigration running...")
                val entries = loadCache(cacheFile).mapValues {
                    CachedDns.Entry(
                        hostname = it.value.hostname,
                        resolvedIpString = it.value.resolvedIpString,
                        validUntil = Instant.fromEpochMilliseconds(it.value.validUntil.time)
                    )
                }
                val dns = SharedBaseInjector.get().dnsResolver as? CachedLocalDnsResolver ?: return@runBlocking Timber.w("Cancelling migration, not a CachedLocalDnsResolver")
                dns.importAll(entries)
                Timber.i("Migrated ${entries.size} DNS entries")
                cacheFile.delete()
            } else {
                Timber.i("DnsCacheMigration is done")
            }
        } catch (e: Exception) {
            Timber.e(e, "Failed migration")
        }
    }

    private fun loadCache(cacheFile: File) = cacheFile.inputStream().bufferedReader().use {
        val gson = Gson()
        val type = object : TypeToken<Map<String, Entry>>() {}.type
        gson.fromJson<Map<String, Entry>>(it.readText(), type) ?: throw FileNotFoundException()
    }


    private data class Entry(
        val hostname: String,
        val resolvedIpString: List<String>,
        val validUntil: Date,
    )
}