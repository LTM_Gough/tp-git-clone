package de.crysxd.octoapp.base.di.modules

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.OctoPrintInstanceInformationSerializer
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.data.source.LocalGcodeFileDataSource
import de.crysxd.octoapp.base.data.source.LocalMediaFileDataSource
import de.crysxd.octoapp.base.data.source.LocalPinnedMenuItemsDataSource
import de.crysxd.octoapp.base.data.source.RemoteGcodeFileDataSource
import de.crysxd.octoapp.base.data.source.RemoteMediaFileDataSource
import de.crysxd.octoapp.base.di.BaseScope
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector

@Module
class DataSourceModule {

    @Provides
    fun provideOctoPrintInstanceInformationSerialized() = OctoPrintInstanceInformationSerializer(
        json = SharedCommonInjector.get().json
    )

    @Provides
    fun provideLocalPinnedMenuItemsDataSource(
        sharedPreferences: SharedPreferences,
    ): LocalPinnedMenuItemsDataSource = LocalPinnedMenuItemsDataSource(
        sharedPreferences = sharedPreferences,
    )

    @Provides
    @BaseScope
    fun provideLocalGcodeFileDataSource(
        context: Context,
        octoPreferences: OctoPreferences,
    ) = LocalGcodeFileDataSource(
        context = context,
        gson = Gson(),
        octoPreferences = octoPreferences
    )

    @Provides
    @BaseScope
    fun provideRemoteGcodeFileDataSource(
        octoPrintProvider: OctoPrintProvider,
        octoPrintRepository: OctoPrintRepository,
        local: LocalGcodeFileDataSource,
        context: Context,
    ) = RemoteGcodeFileDataSource(
        octoPrintProvider = octoPrintProvider,
        localDataSource = local,
        context = context,
        octoPrintRepository = octoPrintRepository,
    )

    @Provides
    @BaseScope
    fun provideLocalMediaFileDataSource(
        context: Context,
        octoPreferences: OctoPreferences,
    ) = LocalMediaFileDataSource(
        context = context,
        octoPreferences = octoPreferences,
    )

    @Provides
    @BaseScope
    fun provideRemoteMediaFileDataSource(
        octoPrintProvider: OctoPrintProvider,
        octoPrintRepository: OctoPrintRepository,
    ) = RemoteMediaFileDataSource(
        octoPrintRepository = octoPrintRepository,
        octoPrintProvider = octoPrintProvider,
    )
}