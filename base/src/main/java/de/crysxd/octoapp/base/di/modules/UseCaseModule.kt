package de.crysxd.octoapp.base.di.modules

import dagger.Module
import dagger.Provides
import de.crysxd.octoapp.base.di.SharedBaseInjector

@Module
class UseCaseModule {

    @Provides
    fun getWebcamSettingsUseCase() = SharedBaseInjector.get().getWebcamSettingsUseCase()

    @Provides
    fun getActiveHttpUrlUseCase() = SharedBaseInjector.get().getActiveHttpUrlUseCase()

    @Provides
    fun applyWebcamTransformationsUseCase() = SharedBaseInjector.get().applyWebcamTransformationsUseCase()

    @Provides
    fun getPowerDevicesUseCase() = SharedBaseInjector.get().getPowerDevicesUseCase()

    @Provides
    fun handleAutomaticLightEventUseCase() = SharedBaseInjector.get().handleAutomaticLightEventUseCase()

    @Provides
    fun testFullNetworkStackUseCase() = SharedBaseInjector.get().testFullNetworkStackUseCase()

    @Provides
    fun discoverOctoPrintUseCase() = SharedBaseInjector.get().discoverOctoPrintUseCase()

    @Provides
    fun updateInstanceCapabilitiesUseCase() = SharedBaseInjector.get().updateInstanceCapabilitiesUseCase()

    @Provides
    fun getAppLanguageUseCase() = SharedBaseInjector.get().getAppLanguageUseCase()

    @Provides
    fun requestApiAccessUseCase() = SharedBaseInjector.get().requestApiAccessUseCase()

    @Provides
    fun setTargetTemperaturesUseCase() = SharedBaseInjector.get().setTargetTemperaturesUseCase()

    @Provides
    fun setTemperatureOffsetUseCase() = SharedBaseInjector.get().setTemperatureOffsetUseCase()

    @Provides
    fun loadFilesUseCase() = SharedBaseInjector.get().loadFilesUseCase()

    @Provides
    fun cancelPrintJobUseCase() = SharedBaseInjector.get().cancelPrintJobUseCase()

    @Provides
    fun startPrintJobUseCase() = SharedBaseInjector.get().startPrintJobUseCase()

    @Provides
    fun togglePausePrintJobUseCase() = SharedBaseInjector.get().togglePausePrintJobUseCase()

    @Provides
    fun getCurrentPrinterProfileUseCase() = SharedBaseInjector.get().getCurrentPrinterProfileUseCase()

    @Provides
    fun getWebcamSnapshotUseCase() = SharedBaseInjector.get().getWebcamSnapshotUseCase()

    @Provides
    fun createBugReportUseCase() = SharedBaseInjector.get().createBugReportUseCase()

    @Provides
    fun getRemoteServiceConnectUrlUseCase() = SharedBaseInjector.get().getRemoteServiceConnectUrlUseCase()

    @Provides
    fun handleOctoEverywhereAppPortalSuccessUseCase() = SharedBaseInjector.get().handleOctoEverywhereAppPortalSuccessUseCase()

    @Provides
    fun handleSpaghettiDetectiveAppPortalSuccessUseCase() = SharedBaseInjector.get().handleSpaghettiDetectiveAppPortalSuccessUseCase()

    @Provides
    fun setAlternativeWebUrlUseCase() = SharedBaseInjector.get().setAlternativeWebUrlUseCase()

    @Provides
    fun updateNgrokTunnelUseCase() = SharedBaseInjector.get().updateNgrokTunnelUseCase()

}