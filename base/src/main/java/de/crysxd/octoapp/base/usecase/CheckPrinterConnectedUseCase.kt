package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.exceptions.PrinterNotOperationalException
import timber.log.Timber
import javax.inject.Inject

class CheckPrinterConnectedUseCase @Inject constructor(
    private val octoPrintProvider: OctoPrintProvider
) : UseCase<Unit, Boolean>() {

    override suspend fun doExecute(param: Unit, timber: Timber.Tree) = try {
        octoPrintProvider.octoPrint().printerApi.getPrinterState()
        true
    } catch (e: PrinterNotOperationalException) {
        false
    }
}