package de.crysxd.octoapp.base.data.source

import android.content.SharedPreferences
import androidx.core.content.edit

class LocalPinnedMenuItemsDataSource(
    private val sharedPreferences: SharedPreferences,
) {

    fun store(menuId: de.crysxd.octoapp.base.data.models.MenuId, items: Set<String>) = sharedPreferences.edit {
        getKey(menuId)?.let {
            putStringSet(it, items)
        }
    }

    fun load(menuId: de.crysxd.octoapp.base.data.models.MenuId): Set<String>? = getKey(menuId)?.let {
        sharedPreferences.getStringSet(it, null)
    }

    private fun getKey(menuId: de.crysxd.octoapp.base.data.models.MenuId) = when (menuId) {
        de.crysxd.octoapp.base.data.models.MenuId.MainMenu -> "pinned_menu_items"
        de.crysxd.octoapp.base.data.models.MenuId.PrePrintWorkspace -> "pinned_menu_items_preprint"
        de.crysxd.octoapp.base.data.models.MenuId.PrintWorkspace -> "pinned_menu_items_print"
        de.crysxd.octoapp.base.data.models.MenuId.Widget -> "pinned_menu_items_widget"
        de.crysxd.octoapp.base.data.models.MenuId.Other -> null
    }
}