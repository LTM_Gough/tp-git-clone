package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.models.commands.ConnectionCommand
import timber.log.Timber
import javax.inject.Inject


class AutoConnectPrinterUseCase @Inject constructor(
    private val octoPrintProvider: OctoPrintProvider
) : UseCase<AutoConnectPrinterUseCase.Params, Unit>() {

    companion object {
        const val AUTO_PORT = "AUTO"
    }

    override suspend fun doExecute(param: Params, timber: Timber.Tree) {
        octoPrintProvider.octoPrint(instanceId = param.instanceId).connectionApi.executeConnectionCommand(
            ConnectionCommand.Connect(
                port = param.port
            )
        )
    }

    data class Params(
        val port: String = AUTO_PORT,
        val instanceId: String? = null
    )
}