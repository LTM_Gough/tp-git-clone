package de.crysxd.octoapp.base.data.repository

import android.content.ContentValues
import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Size
import android.util.TypedValue
import android.webkit.MimeTypeMap
import androidx.annotation.RequiresApi
import androidx.core.net.toUri
import de.crysxd.octoapp.base.data.source.LocalMediaFileDataSource
import de.crysxd.octoapp.base.data.source.RemoteMediaFileDataSource
import de.crysxd.octoapp.base.ext.collectSaveWithRetry
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message.Event.MovieDone
import de.crysxd.octoapp.engine.models.event.Message.Event.MovieFailed
import de.crysxd.octoapp.engine.models.event.Message.Event.MovieRendering
import de.crysxd.octoapp.engine.models.timelapse.TimelapseConfig
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import de.crysxd.octoapp.engine.models.timelapse.TimelapseStatus
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException

class TimelapseRepository(
    private val context: Context,
    private val octoPrintProvider: OctoPrintProvider,
    private val localMediaFileDataSource: LocalMediaFileDataSource,
    private val remoteMediaFileDataSource: RemoteMediaFileDataSource,
) {

    // The int is used to push forced updates after a thumbnail was fetched
    private val state = MutableStateFlow<Pair<TimelapseStatus, Int>?>(null)

    init {
        AppScope.launch {
            octoPrintProvider.passiveEventFlow().retry { delay(1000); true }.collectSaveWithRetry {
                if (it is Event.MessageReceived && (it.message is MovieFailed || it.message is MovieDone || it.message is MovieRendering)) {
                    fetchLatest()
                }
            }
        }
    }

    suspend fun update(block: TimelapseConfig.() -> TimelapseConfig) {
        val current = state.value
        requireNotNull(current) { "Timelapse state not loaded" }
        requireNotNull(current.first.config) { "Timelapse config not loaded" }
        val new = block(current.first.config)
        Timber.d("Updating timelapse config $current -> $new")
        state.value = octoPrintProvider.octoPrint().timelapseApi.updateConfig(new) to (state.value?.second ?: 0)
    }

    suspend fun delete(timelapseFile: TimelapseFile) {
        state.value = (octoPrintProvider.octoPrint().timelapseApi.delete(timelapseFile)) to (state.value?.second ?: 0)
    }

    suspend fun fetchLatest(): TimelapseStatus {
        val latest = octoPrintProvider.octoPrint().timelapseApi.getStatus()
        state.value = latest to (state.value?.second ?: 0)
        Timber.d("Got latest timelapse config $latest")
        return latest
    }

    suspend fun download(timelapseFile: TimelapseFile) = withContext(Dispatchers.IO) {
        val url = timelapseFile.downloadPath ?: throw SuppressedIllegalStateException("Timelapse has no URL")

        return@withContext localMediaFileDataSource.load(url) ?: let {
            Timber.i("Downloading: $url")
            val file = localMediaFileDataSource.store(url, remoteMediaFileDataSource.loadFromOctoPrint(encodedPath = url))

            try {
                Timber.i("Generating thumbnail for: $url")
                requireNotNull(file) { "Just downloaded file is null" }
                createThumbnail(file)?.let {
                    val bytes = ByteArrayOutputStream()
                    it.compress(Bitmap.CompressFormat.JPEG, 80, bytes)
                    localMediaFileDataSource.store("thumb:$url", bytes.toByteArray().inputStream())
                }

                // Update flow so thumbnail is refreshed in UI
                state.value = state.value?.let { it.first to it.second + 1 }
            } catch (e: Exception) {
                Timber.e(e)
            }

            file
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    suspend fun pushToGallery(timelapseFile: TimelapseFile, file: File) = withContext(Dispatchers.IO) {
        val url = timelapseFile.downloadPath ?: throw SuppressedIllegalStateException("Timelapse has no URL")
        Timber.i("Pushing $url to gallery")

        // Create values
        val filename = timelapseFile.name
        val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(url))
        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
            put(MediaStore.MediaColumns.MIME_TYPE, mimeType)
            timelapseFile.date?.let { put(MediaStore.MediaColumns.DATE_TAKEN, it) }
            put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_MOVIES)
            put(MediaStore.Video.Media.IS_PENDING, 1)
        }

        // Create Uri
        val (videoUri, out) = context.contentResolver.let { resolver ->
            val uri = resolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, contentValues) ?: throw IOException("Unable to create URI")
            val fos = uri.let { resolver.openOutputStream(it) } ?: throw IOException("Unable to open output")
            uri to fos
        }

        // Copy file
        file.inputStream().copyTo(out)

        // Push to gallery
        contentValues.clear()
        contentValues.put(MediaStore.Video.Media.IS_PENDING, 0)
        context.contentResolver.update(videoUri, contentValues, null, null)
    }

    suspend fun getThumbnailUri(timelapseFile: TimelapseFile) = withContext(Dispatchers.IO) {
        try {
            timelapseFile.downloadPath ?: return@withContext null

            val url = remoteMediaFileDataSource.ensureAbsoluteUrl(timelapseFile.downloadPath)
            return@withContext localMediaFileDataSource.load("thumb:$url")?.toUri()
        } catch (e: Exception) {
            Timber.e(e)
            null
        }
    }

    private fun createThumbnail(file: File) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        ThumbnailUtils.createVideoThumbnail(file, Size(200f.toPx(), 200f.toPx()), null)
    } else {
        @Suppress("Deprecation")
        ThumbnailUtils.createVideoThumbnail(file.path, MediaStore.Images.Thumbnails.MINI_KIND)
    }

    private fun Float.toPx() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, Resources.getSystem().displayMetrics).toInt()

    fun peek() = state.value

    fun flow() = state.asStateFlow().map { it?.first }
}