package de.crysxd.octoapp.base.data.source

import android.content.Context
import android.net.ConnectivityManager
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.ext.asStyleFileSize
import de.crysxd.octoapp.base.gcode.parse.GcodeParser
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.measureTime
import de.crysxd.octoapp.engine.models.files.FileObject
import io.ktor.utils.io.jvm.javaio.toInputStream
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.shareIn
import timber.log.Timber

class RemoteGcodeFileDataSource(
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
    private val localDataSource: LocalGcodeFileDataSource,
    private val context: Context,
) {

    private var flowCaches = mutableMapOf<String, Flow<GcodeFileDataSource.LoadState>>()

    fun loadFile(file: FileObject.File, allowLargeFileDownloads: Boolean) = flowCaches[file.cacheKey]?.let {
        Timber.i("Reusing download flow for ${file.cacheKey} ")
        it
    } ?: let {
        Timber.i("Starting download flow for ${file.cacheKey} (available: ${flowCaches.keys} @ $this)")
        flow {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val onMeteredNetwork = connectivityManager.isActiveNetworkMetered

            val maxFileSize = (octoPrintRepository.getActiveInstanceSnapshot()?.settings ?: octoPrintProvider.octoPrint().settingsApi.getSettings())
                .plugins.gcodeViewer?.let {
                    if (onMeteredNetwork) it.mobileSizeThreshold else it.sizeThreshold
                }.also {
                    Timber.i("Current network is metered: $onMeteredNetwork -> max file size for direct download is ${it?.asStyleFileSize()}")
                }

            if (maxFileSize != null && !allowLargeFileDownloads && (file.size ?: Long.MAX_VALUE) > maxFileSize) {
                return@flow emit(GcodeFileDataSource.LoadState.FailedLargeFileDownloadRequired(file.size ?: -1))
            }

            emit(GcodeFileDataSource.LoadState.Loading(0f))

            measureTime("Download and parse file") {
                try {
                    Timber.i("Downloading and parsing ${file.path}")
                    val gcode = octoPrintProvider.octoPrint().filesApi.downloadFile(file).toInputStream().use { input ->
                        localDataSource.createCacheForFile(file).use { cache ->
                            GcodeParser(
                                content = input,
                                totalSize = file.size ?: Long.MAX_VALUE,
                                progressUpdate = { progress ->
                                    emit(GcodeFileDataSource.LoadState.Loading(progress))
                                    Timber.v("Parsing ${file.path}: $progress")
                                },
                                layerSink = { cache.cacheLayer(it) }
                            ).parseFile().also {
                                Timber.i("[GCD] Parsed ${file.path} with ${it.layers.size} layers")
                            }
                        }
                    }
                    emit(GcodeFileDataSource.LoadState.Ready(gcode))
                } catch (e: Exception) {
                    Timber.e(e)
                    emit(GcodeFileDataSource.LoadState.Failed(e))
                }
            }
        }.flowOn(Dispatchers.IO).onCompletion {
            Timber.i("Removing download flow for ${file.cacheKey}")
            flowCaches.remove(file.cacheKey)
        }.catch {
            Timber.e(it, "Failed to download Gcode")
            emit(GcodeFileDataSource.LoadState.Failed(it))
        }.shareIn(AppScope, started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 3000), replay = 1).also {
            Timber.i("Storing download flow for ${file.cacheKey}")
            flowCaches[file.cacheKey] = it
        }
    }

    private val FileObject.File.cacheKey get() = "$path-$date"
}