package de.crysxd.octoapp.base.migrations

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import com.google.gson.reflect.TypeToken
import de.crysxd.octoapp.base.data.models.AppSettings
import de.crysxd.octoapp.base.data.models.OctoEverywhereConnection
import de.crysxd.octoapp.base.data.models.RemoteConnectionFailure
import de.crysxd.octoapp.base.data.source.DataSource
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.models.version.VersionInfo
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import io.ktor.http.Url
import timber.log.Timber
import java.lang.reflect.Type
import java.util.UUID
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3 as NewItem

class OctoPrintInstanceInformationMigration(private val context: Context) {

    fun migrate() = try {
        val ds = LocalOctoPrintInstanceInformationSource(context)
        if (ds.hasAny()) {
            Timber.i("OctoPrintInstanceInformationMigration running...")
            val items = ds.get()
            val mapped = items.map { octo ->
                NewItem(
                    webUrl = octo.webUrl,
                    alternativeWebUrl = octo.alternativeWebUrl,
                    settings = null,
                    id = octo.id,
                    apiKey = octo.apiKey,
                    remoteConnectionFailure = octo.remoteConnectionFailure?.let {
                        RemoteConnectionFailure(
                            errorMessageStack = it.errorMessageStack,
                            errorMessage = it.errorMessage,
                            dateMillis = it.dateMillis,
                            remoteServiceName = it.remoteServiceName,
                            stackTrace = it.stackTrace
                        )
                    },
                    activeProfile = null,
                    appSettings = octo.appSettings,
                    availablePlugins = octo.availablePlugins,
                    capabilitiesFetchedAt = octo.capabilitiesFetchedAt,
                    m115Response = octo.m115Response,
                    notificationId = octo.notificationId,
                    octoEverywhereConnection = octo.octoEverywhereConnection?.let {
                        OctoEverywhereConnection(
                            apiToken = it.apiToken,
                            basicAuthPassword = it.basicAuthPassword,
                            basicAuthUser = it.basicAuthUser,
                            bearerToken = it.bearerToken,
                            connectionId = it.connectionId,
                        )
                    },
                    version = octo.version?.let {
                        VersionInfo(
                            apiVersion = it.apiVersion,
                            serverVersionText = it.serverVersionText,
                            severVersion = it.severVersion
                        )
                    },
                )
            }
            Timber.i("Mapped ${mapped.size} items")
            SharedBaseInjector.get().printerConfigRepository.import(mapped)
        }
        Timber.i("OctoPrintInstanceInformationMigration is done")
        ds.delete()
    } catch (e: Exception) {
        Timber.e(e, "Failed OctoPrintInstanceInformationMigration")
    }

    private data class OctoPrintInstanceInformationV1(
        val hostName: String,
        val port: Int,
        val apiKey: String,
        val supportsPsuPlugin: Boolean = false,
        val supportsWebcam: Boolean = false,
        val apiKeyWasInvalid: Boolean = false
    )

    private data class OctoPrintInstanceInformationV2(
        val webUrl: String,
        val alternativeWebUrl: String? = null,
        val apiKey: String,
        // m115Response is only updated if Gcode Preview feature is enabled
        val m115Response: String? = null,
        val systemCommands: List<SystemCommand2>? = null,
        val appSettings: AppSettings? = null,
        val octoEverywhereConnection: OctoEverywhereConnection2? = null,
    ) {
        constructor(legacy: OctoPrintInstanceInformationV1) : this(
            webUrl = "http://${legacy.hostName}:${legacy.port}",
            apiKey = legacy.apiKey,
        )
    }

    private data class OctoPrintInstanceInformationV3(
        val id: String,
        val version: VersionInfo2? = null,
        val capabilitiesFetchedAt: Long? = null,
        val notificationId: Int? = null,
        val webUrl: Url,
        val alternativeWebUrl: Url? = null,
        val apiKey: String,
        val m115Response: String? = null,
        val systemCommands: List<SystemCommand2>? = null,
        val appSettings: AppSettings? = null,
        val availablePlugins: Map<String, String?>? = null,
        val octoEverywhereConnection: OctoEverywhereConnection2? = null,
        val remoteConnectionFailure: RemoteConnectionFailure2? = null
    ) {
        constructor(legacy: OctoPrintInstanceInformationV2) : this(
            id = UUID.randomUUID().toString(),
            webUrl = legacy.webUrl.toUrl(),
            notificationId = null,
            alternativeWebUrl = legacy.alternativeWebUrl?.toUrl(),
            apiKey = legacy.apiKey,
            m115Response = legacy.m115Response,
            systemCommands = legacy.systemCommands,
            appSettings = legacy.appSettings,
            octoEverywhereConnection = legacy.octoEverywhereConnection,
        )

        override fun toString() = StringBuilder().also {
            it.append("OctoPrintInstanceInformationV3(")
            it.append("id=$id ")
            it.append("webUrl=$webUrl ")
            it.append("alternativeWebUrl=$alternativeWebUrl ")
            it.append("notificationId=$notificationId ")
            it.append("apiKey=$apiKey ")
            it.append("m115Response=${if (m115Response == null) null else "..."} ")
            it.append("settings=OMITTED ")
            it.append("activeProfile=OMITTED ")
            it.append("systemCommands=${if (systemCommands == null) null else "..."} ")
            it.append("appSettings=$appSettings ")
            it.append("octoEverywhereConnection=${if (octoEverywhereConnection == null) null else "..."}\" ")
            it.append("remoteConnectionFailure=$remoteConnectionFailure")
            it.append(")")
        }.toString()
    }

    private data class RemoteConnectionFailure2(
        val errorMessage: String,
        val errorMessageStack: String,
        val stackTrace: String,
        val remoteServiceName: String,
        val dateMillis: Long = 0
    )

    private data class OctoEverywhereConnection2(
        val connectionId: String,
        val apiToken: String,
        val bearerToken: String,
        val basicAuthUser: String,
        val basicAuthPassword: String,
    )

    private data class SystemCommand2 constructor(
        val name: String,
        val action: String,
        val source: String,
        val confirmation: String?,
    )

    private data class VersionInfo2(
        val apiVersion: String? = null,
        val severVersion: String? = null,
        val serverVersionText: String? = null,
    )

    private class LocalOctoPrintInstanceInformationSource(
        private val context: Context,
        private val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context),
        private val gson: Gson = GsonBuilder()
            .registerTypeAdapter(Url::class.java, UrlAdapter())
            .create()
    ) : DataSource<List<OctoPrintInstanceInformationV3>> {

        companion object {
            // V1 Model in original store
            private const val KEY_LEGACY_V0_HOST_NAME = "octorpint_host_name"
            private const val KEY_LEGACY_V0_PORT = "octoprint_port"
            private const val KEY_LEGACY_V0_API_KEY = "octoprint_api_key"
            private const val KEY_LEGACY_V0_SUPPORTS_PSU_PLUGIN = "octoprint_supports_psu_plugin"
            private const val KEY_LEGACY_V0_API_KEY_WAS_INVALID = "octoprint_api_key_was_invalid"

            // V1 model in single store
            private const val KEY_LEGACY_INSTANCE_INFORMATION_V1 = "octorpint_instance_information"

            // V2 model in single store
            private const val KEY_LEGACY_INSTANCE_INFORMATION_V2S = "octorpint_instance_information_v2"

            // V2 model in list store
            private const val KEY_LEGACY_INSTANCE_INFORMATION_V2L = "octorpint_instance_information_v3"

            // V3 model
            private const val KEY_INSTANCE_INFORMATION_V3 = "octorpint_instance_information_v4"

            // Notification id range
            private val NOTIFICATION_ID_RANGE = 15_000..15_999
        }

        fun hasAny(): Boolean {
            runUpgrades()
            return sharedPreferences.getString(KEY_INSTANCE_INFORMATION_V3, "[]") != "[]"
        }

        fun delete() {
            clearAllLegacy()
            sharedPreferences.edit { remove(KEY_INSTANCE_INFORMATION_V3) }
        }

        override fun store(t: List<OctoPrintInstanceInformationV3>?) = if (t == null) {
            sharedPreferences.edit { remove(KEY_INSTANCE_INFORMATION_V3) }
        } else {
            sharedPreferences.edit { putString(KEY_INSTANCE_INFORMATION_V3, gson.toJson(t)) }
        }

        private fun runUpgrades() {
            try {
                upgradeV0ToV3()
                upgradeV1ToV3()
                upgradeV2ToV3()
                upgradeWithNotificationId()
            } catch (e: Exception) {
                Timber.e(e)
            } finally {
                clearAllLegacy()
            }
        }

        override fun get(): List<OctoPrintInstanceInformationV3> = try {
            // Load
            val raw = sharedPreferences.getString(KEY_INSTANCE_INFORMATION_V3, null) ?: "[]"
            gson.fromJson(raw, object : TypeToken<List<OctoPrintInstanceInformationV3>>() {}.type)
        } catch (e: Exception) {
            Timber.e(e)
            emptyList()
        }

        private fun upgradeV0ToV3() = if (
            sharedPreferences.contains(KEY_LEGACY_V0_API_KEY) &&
            sharedPreferences.contains(KEY_LEGACY_V0_HOST_NAME) &&
            sharedPreferences.contains(KEY_LEGACY_V0_PORT)
        ) {
            // Load
            val v1 = OctoPrintInstanceInformationV1(
                sharedPreferences.getString(KEY_LEGACY_V0_HOST_NAME, "") ?: "",
                sharedPreferences.getInt(KEY_LEGACY_V0_PORT, 80),
                sharedPreferences.getString(KEY_LEGACY_V0_API_KEY, "") ?: "",
                sharedPreferences.getBoolean(KEY_LEGACY_V0_SUPPORTS_PSU_PLUGIN, false),
                sharedPreferences.getBoolean(KEY_LEGACY_V0_API_KEY_WAS_INVALID, false)
            )

            // Upgrade
            val v3 = OctoPrintInstanceInformationV3(OctoPrintInstanceInformationV2(v1))
//        sensitiveDataMask.registerInstance(v3)
            Timber.i("Upgrading from V0 -> V3 (v0=$v1, v3=$v3)")
            store(listOf(v3))
        } else {
            null
        }

        private fun upgradeV1ToV3() {
            if (sharedPreferences.contains(KEY_LEGACY_INSTANCE_INFORMATION_V1)) {
                sharedPreferences.getString(KEY_LEGACY_INSTANCE_INFORMATION_V1, null)?.let {
                    gson.fromJson(it, OctoPrintInstanceInformationV1::class.java)
                }?.let {
                    val v3 = OctoPrintInstanceInformationV3(OctoPrintInstanceInformationV2(it))
//                sensitiveDataMask.registerInstance(v3)
                    Timber.i("Upgrading from V1 -> V3 (v1=$it, v3=$v3)")
                    store(listOf(v3))
                }
            }
        }

        private fun upgradeV2ToV3() {
            // Signle item update
            if (sharedPreferences.contains(KEY_LEGACY_INSTANCE_INFORMATION_V2S)) {
                sharedPreferences.getString(KEY_LEGACY_INSTANCE_INFORMATION_V2S, null)?.let {
                    gson.fromJson(it, OctoPrintInstanceInformationV2::class.java)
                }?.let {
                    val v3 = OctoPrintInstanceInformationV3(it)
//                sensitiveDataMask.registerInstance(v3)
                    Timber.i("Upgrading from single V2 -> V3 (v2=$it, v3=$v3)")
                    store(listOf(v3))
                }
            }

            // List update
            if (sharedPreferences.contains(KEY_LEGACY_INSTANCE_INFORMATION_V2L)) {
                val v2 = gson.fromJson<List<OctoPrintInstanceInformationV2>>(
                    sharedPreferences.getString(KEY_LEGACY_INSTANCE_INFORMATION_V2L, "[]"),
                    object : TypeToken<List<OctoPrintInstanceInformationV2>>() {}.type
                )

                v2.map {
                    OctoPrintInstanceInformationV3(it)
                }.let {
                    // Store and delete
                    it.forEachIndexed { index, v3 ->
//                    sensitiveDataMask.registerInstance(v3)
                        Timber.i("Upgrading from V2 -> V3 (v2=${v2[index]}, v3=$v3)")
                    }
                    store(it)
                }
            }
        }

        private fun upgradeWithNotificationId() {
            // Load
            val instances = gson.fromJson<List<OctoPrintInstanceInformationV3>>(
                sharedPreferences.getString(KEY_INSTANCE_INFORMATION_V3, "[]"),
                object : TypeToken<List<OctoPrintInstanceInformationV3>>() {}.type
            ).associateBy { it.id }.toMutableMap()

            // Generate unique notification ids
            instances.values.forEach {
                if (it.notificationId == null) {
                    // New id: max + 1
                    val id = NOTIFICATION_ID_RANGE.first + ((instances.maxOf {
                        it.value.notificationId ?: NOTIFICATION_ID_RANGE.first
                    } + 1) % NOTIFICATION_ID_RANGE.last)
                    instances[it.id] = it.copy(notificationId = id)
                }
            }

            // Store
            store(instances.values.toList())
        }

        private fun clearAllLegacy() {
            sharedPreferences.edit {
                remove(KEY_LEGACY_V0_HOST_NAME)
                remove(KEY_LEGACY_V0_PORT)
                remove(KEY_LEGACY_V0_API_KEY)
                remove(KEY_LEGACY_V0_SUPPORTS_PSU_PLUGIN)
                remove(KEY_LEGACY_V0_API_KEY_WAS_INVALID)
                remove(KEY_LEGACY_INSTANCE_INFORMATION_V1)
                remove(KEY_LEGACY_INSTANCE_INFORMATION_V2L)
                remove(KEY_LEGACY_INSTANCE_INFORMATION_V2S)
            }
        }
    }

    private class UrlAdapter : JsonSerializer<Url>, JsonDeserializer<Url> {
        override fun serialize(value: Url?, p1: Type?, context: JsonSerializationContext?) = JsonPrimitive(value.toString())
        override fun deserialize(value: JsonElement, p1: Type, context: JsonDeserializationContext) = value.asString.toUrl()
    }
}
