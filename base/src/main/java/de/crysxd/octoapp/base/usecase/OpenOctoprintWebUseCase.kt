package de.crysxd.octoapp.base.usecase

import android.content.Context
import android.content.Intent
import android.net.Uri
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.base.utils.UPNP_ADDRESS_PREFIX
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import de.crysxd.octoapp.sharedcommon.http.framework.withHost
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.withContext
import okhttp3.HttpUrl
import timber.log.Timber
import javax.inject.Inject

class OpenOctoprintWebUseCase @Inject constructor(
    private val getActiveHttpUrlUseCase: GetActiveHttpUrlUseCase,
    private val octoPrintRepository: OctoPrintRepository,
    private val localDnsResolver: CachedDns,
    private val context: Context
) : UseCase<OpenOctoprintWebUseCase.Params, Uri?>() {

    override suspend fun doExecute(param: Params, timber: Timber.Tree): Uri? {
        val webUrl = param.octoPrintWebUrl?.toString()?.toUrlOrNull() ?: getActiveHttpUrlUseCase.execute(octoPrintRepository.get(param.instanceId)).first()
        val host = webUrl.host
        val resolvedUrl = if (host.startsWith(UPNP_ADDRESS_PREFIX) || host.endsWith(".local")) {
            val resolvedHost = withContext(Dispatchers.IO) { localDnsResolver.lookup(host).first() }
            webUrl.withHost(resolvedHost.hostAddress!!)
        } else {
            webUrl
        }

        val uri = Uri.parse(resolvedUrl.toString())

        if (!param.returnUriOnly) {
            val intent = Intent(Intent.ACTION_VIEW, uri).also { it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) }
            context.startActivity(intent)
        }

        return uri
    }

    data class Params(
        val octoPrintWebUrl: HttpUrl? = null,
        val instanceId: String? = null,
        val returnUriOnly: Boolean = false,
    )
}