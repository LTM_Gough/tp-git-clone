package de.crysxd.octoapp.base.utils

import android.annotation.SuppressLint
import android.content.Intent
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.DiscoverOctoPrintUseCase
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import java.util.UUID

object BenchmarkIntentHandler {

    @SuppressLint("VisibleForTests")
    fun handle(intent: Intent?) = with(BaseInjector.get()) {
        intent ?: return@with

        if (intent.getBooleanExtra("benchmark_reset", false)) {
            octoPreferences().reset()
            octorPrintRepository().clear()
        }

        if (intent.getBooleanExtra("benchmark_all_features", false)) {
            BillingManager.enabledForTest = true
        }

        if (intent.getBooleanExtra("benchmark_suppress_discover", false)) {
            DiscoverOctoPrintUseCase.suppressDiscovery = true
        }

        intent.getStringExtra("benchmark_instance")?.let {
            val (url, apiKey) = it.split(",")
            octorPrintRepository().setActive(
                trigger = "benchmark-intent",
                instance = OctoPrintInstanceInformationV3(
                    id = UUID.randomUUID().toString(),
                    webUrl = url.toUrl(),
                    apiKey = apiKey
                )
            )
        }
    }
}