package de.crysxd.octoapp.base.usecase

import androidx.test.platform.app.InstrumentationRegistry
import com.github.druk.dnssd.DNSSDEmbedded
import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.reset
import com.nhaarman.mockitokotlin2.whenever
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.network.CachedLocalDnsResolver
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.octoprint.LoggingConfig
import de.crysxd.octoapp.octoprint.OctoPrint
import okhttp3.HttpUrl.Companion.toHttpUrl
import org.junit.After
import org.junit.Test

class TestFullNetworkStackUseCaseTest {

    private val context = InstrumentationRegistry.getInstrumentation().targetContext
    private val octoPrintProvider = mock<OctoPrintProvider>()
    private val target = TestFullNetworkStackUseCase(
        octoPrintProvider = octoPrintProvider,
        getWebcamSnapshotUseCase = mock(),
        getWebcamSettingsUseCase = mock(),
        octoPrintRepository = mock(),
        localDnsResolver = CachedLocalDnsResolver(
            context = context,
            dnssd = DNSSDEmbedded(context),
            octoPreferences = mock {
                on { enforceIPv4 } doReturn false
            }
        )
    )

    @After
    fun tearDown() = reset(octoPrintProvider)

    @Test
    fun WHEN_checking_host_that_cant_be_pinged_THEN_result_is_other_than_not_reachable() {
        //region GIVEN
        val url = "https://wrong.host.badssl.com"
        whenever(octoPrintProvider.createAdHocOctoPrint(any(), any())).thenAnswer {
            val instance = it.arguments[0] as OctoPrintInstanceInformationV3
            assertThat(instance.webUrl).isEqualTo(url.toHttpUrl())
            assertThat(instance.apiKey).isEqualTo("notanapikey")
            OctoPrint(
                id = null,
                rawWebUrl = instance.webUrl,
                apiKey = instance.apiKey,
                logging = LoggingConfig.Verbose,
            )
        }
        val params = TestFullNetworkStackUseCase.Target.OctoPrint(
            webUrl = url,
            apiKey = ""
        )
        //endregion
        //region WHEN
        val finding = target.executeBlocking(params)
        //endregion
        //region THEN
        assertThat(finding).isInstanceOf(TestFullNetworkStackUseCase.Finding.HttpsNotTrusted::class.java)
        //endregion
    }
}