package de.crysxd.octoapp.filemanager.ui.search

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.asLiveData
import de.crysxd.baseui.BaseFragment
import de.crysxd.baseui.InsetAwareScreen
import de.crysxd.baseui.common.OctoToolbar
import de.crysxd.baseui.ext.clearFocusAndHideSoftKeyboard
import de.crysxd.baseui.ext.requestFocusAndOpenSoftKeyboard
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.baseui.menu.base.MenuBottomSheetFragment
import de.crysxd.octoapp.filemanager.databinding.SearchFileFragmentBinding
import de.crysxd.octoapp.filemanager.di.injectViewModel
import de.crysxd.octoapp.filemanager.menu.FileActionsMenu
import de.crysxd.octoapp.filemanager.ui.select.SelectFileAdapter
import de.crysxd.octoapp.filemanager.ui.select.SelectFileViewModel

class SearchFileFragment : BaseFragment(), InsetAwareScreen {

    private lateinit var binding: SearchFileFragmentBinding
    override val viewModel: SearchFileViewModel by injectViewModel()
    private val selectViewModel: SelectFileViewModel by injectViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        SearchFileFragmentBinding.inflate(inflater, container, false).also { binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        connectViewModel(selectViewModel)

        val adapter = SelectFileAdapter(
            context = requireContext(),
            onFileSelected = { selectViewModel.selectFile(it) },
            onFileMenuOpened = { MenuBottomSheetFragment.createForMenu(FileActionsMenu(it)).show(childFragmentManager) },
            onAddItemClicked = {},
            onHideThumbnailHint = {},
            onRetry = { viewModel.retry() },
            onShowThumbnailInfo = {},
            onSortOptionsClicked = {},
            onSearch = {},
        )
        binding.list.adapter = adapter
        selectViewModel.picasso.observe(viewLifecycleOwner) {
            adapter.picasso = it
        }

        viewModel.viewState.asLiveData().observe(viewLifecycleOwner) {
            when (it) {
                is SearchFileViewModel.ViewState.Failed -> adapter.showError()

                SearchFileViewModel.ViewState.Loading -> adapter.showLoading()

                is SearchFileViewModel.ViewState.Loaded -> adapter.showFiles(
                    files = it.files.map { SelectFileViewModel.FileWrapper.FileObjectWrapper(it) },
                    showThumbnailHint = false,
                    isSearch = true,
                    folderName = null,
                )
            }
        }

        binding.buttonClear.setOnClickListener {
            binding.searchInput.setText("")
        }

        binding.searchInput.doOnTextChanged { text, _, _, _ ->
            binding.buttonClear.isVisible = text?.isNotEmpty() == true
            viewModel.search(term = text?.toString() ?: "")
        }
    }

    override fun onStart() {
        super.onStart()
        requireOctoActivity().octoToolbar.state = OctoToolbar.State.Hidden
        requireOctoActivity().octo.isVisible = false
        binding.searchInput.requestFocusAndOpenSoftKeyboard()
    }

    override fun onStop() {
        super.onStop()
        binding.searchInput.clearFocusAndHideSoftKeyboard()
    }

    override fun handleInsets(insets: Rect) {
        binding.container.updatePadding(top = insets.top)
    }
}
