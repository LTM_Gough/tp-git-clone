package de.crysxd.octoapp.filemanager.ui.search

import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.BaseViewModel
import de.crysxd.octoapp.base.data.models.FileListState
import de.crysxd.octoapp.base.ext.filterEventsForMessageType
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.base.ext.searchInTree
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch
import timber.log.Timber

@OptIn(ExperimentalCoroutinesApi::class)
class SearchFileViewModel(
    private val loadFilesUseCase: LoadFilesUseCase,
    private val octoPrintProvider: OctoPrintProvider,
) : BaseViewModel() {

    private val retryTrigger = MutableStateFlow(1)
    private val searchTermFlow = MutableStateFlow("")
    val viewState = retryTrigger.flatMapLatest {
        loadFilesUseCase.execute(LoadFilesUseCase.Params.All(fileOrigin = FileOrigin.Local)).combine(searchTermFlow.rateLimit(300)) { fileState, term ->
            when (fileState) {
                is FileListState.Error -> ViewState.Failed(fileState.exception)
                FileListState.Loading -> ViewState.Loading
                is FileListState.Loaded -> {
                    require(fileState.file is FileObject.Folder) { "Received file instead of folder!" }
                    val allFiles = (fileState.file as FileObject.Folder).children
                    val filteredFiles = fileState.file.searchInTree(term)
                    ViewState.Loaded(filteredFiles)
                }
            }
        }
    }

    init {
        viewModelScope.launch(coroutineExceptionHandler) {
            octoPrintProvider.eventFlow("search-files")
                .filterEventsForMessageType<Message.Event.UpdatedFiles>()
                .collect {
                    Timber.i("Files changed, reloading...")
                    retry()
                }
        }
    }

    fun retry() = viewModelScope.launch(coroutineExceptionHandler) {
        retryTrigger.value++
    }

    fun search(term: String) = viewModelScope.launch(coroutineExceptionHandler) {
        searchTermFlow.value = term
    }

    sealed class ViewState {
        object Loading : ViewState()
        data class Failed(val e: Throwable) : ViewState()
        data class Loaded(val files: List<FileObject>) : ViewState()
    }
}