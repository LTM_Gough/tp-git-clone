package de.crysxd.octoapp.tests

import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl

object TestEnvironmentLibrary {

    // Has SpoolManager
    val Frenchie
        get() = OctoPrintInstanceInformationV3(
            id = "frenchie",
            webUrl = "http://${BuildConfig.TEST_ENV_DOMAIN}:5005".toUrl(),
            apiKey = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        )

    // Vanilla
    val Terrier
        get() = OctoPrintInstanceInformationV3(
            id = "terrier",
            webUrl = "http://${BuildConfig.TEST_ENV_DOMAIN}:5004".toUrl(),
            apiKey = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        )

    @Suppress("unused")
    @Deprecated("Not a stable environment", replaceWith = ReplaceWith("Terrier"))
    val Beagle
        get() = OctoPrintInstanceInformationV3(
            id = "beagle",
            webUrl = "http://${BuildConfig.TEST_ENV_DOMAIN}:5001".toUrl(),
            apiKey = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        )

    val Dachshund
        get() = OctoPrintInstanceInformationV3(
            id = "dachshund",
            webUrl = "http://${BuildConfig.TEST_ENV_DOMAIN}:5003".toUrl(),
            apiKey = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        )

    val Corgi
        get() = OctoPrintInstanceInformationV3(
            id = "corgi",
            webUrl = "http://${BuildConfig.TEST_ENV_DOMAIN}:5002".toUrl(),
            apiKey = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        )

    val CorgiRemote
        get() = OctoPrintInstanceInformationV3(
            id = "corgi",
            webUrl = "http://127.0.0.1:4444".toUrl(),
            alternativeWebUrl = BuildConfig.TEST_OCTOEVERYWHERE_URL.toUrl(),
            apiKey = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        )
}

