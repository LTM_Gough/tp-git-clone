package de.crysxd.octoapp.tests.rules

import android.app.Application
import androidx.test.platform.app.InstrumentationRegistry
import com.google.firebase.ktx.Firebase
import com.google.firebase.ktx.initialize
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.NetworkModule
import de.crysxd.octoapp.base.di.SharedBaseComponent
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.network.AndroidNetworkServiceDiscovery
import de.crysxd.octoapp.base.network.CachedLocalDnsResolver
import de.crysxd.octoapp.base.network.dnssd.DnsSd
import de.crysxd.octoapp.sharedcommon.di.PlatformModule
import de.crysxd.octoapp.sharedcommon.di.SharedCommonComponent
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import timber.log.Timber

class InitBenchmarkAppRule : TestRule {

    init {
        Timber.uprootAll()
        Timber.plant(Timber.DebugTree())
    }

    override fun apply(base: Statement, description: Description) = object : Statement() {
        override fun evaluate() {
            try {
                resetDi()
                base.evaluate()
            } finally {
                resetDi()
            }
        }
    }

    private fun resetDi() {
        val app = InstrumentationRegistry.getInstrumentation().context.applicationContext as Application
        Firebase.initialize(app)

        SharedCommonInjector.setComponent(
            SharedCommonComponent(
                platformModule = PlatformModule(app)
            )
        )

        SharedBaseInjector.setComponent(
            SharedBaseComponent(
                sharedCommonComponent = SharedCommonInjector.get(),
                networkModule = NetworkModule(
                    networkServiceDiscovery = AndroidNetworkServiceDiscovery(app, DnsSd.create(app)),
                    dns = CachedLocalDnsResolver(app, DnsSd.create(app))
                )
            )
        )

        BaseInjector.init(app)
    }
}