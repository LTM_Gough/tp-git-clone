package de.crysxd.octoapp.tests.ext

import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository

fun OctoPrintRepository.setActive(instanceInformationV3: OctoPrintInstanceInformationV3) =
    setActive(instanceInformationV3, trigger = "TestFramework")