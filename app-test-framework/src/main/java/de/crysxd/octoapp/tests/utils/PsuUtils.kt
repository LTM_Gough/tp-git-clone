package de.crysxd.octoapp.tests.utils

import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.di.BaseInjector
import kotlinx.coroutines.runBlocking

object PsuUtils {
    fun OctoPrintInstanceInformationV3.turnAllOff() = runBlocking {
        val octoPrint = BaseInjector.get().octoPrintProvider().createAdHocOctoPrint(this@turnAllOff)
        val settings = octoPrint.settingsApi.getSettings()
        octoPrint.powerDevicesApi.getDevices(settings).forEach { it.turnOff() }
    }
}