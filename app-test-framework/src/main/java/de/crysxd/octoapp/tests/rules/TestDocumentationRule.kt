package de.crysxd.octoapp.tests.rules

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context.NOTIFICATION_SERVICE
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.Base64
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.screenshot.Screenshot
import de.crysxd.octoapp.base.logging.CachedAntiLog
import de.crysxd.octoapp.tests.R
import io.github.aakira.napier.LogLevel
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.util.Locale

@SuppressLint("VisibleForTests", "NewApi")
class TestDocumentationRule(var showNotification: Boolean = true) : TestWatcher() {

    companion object {
        private val counters = mutableMapOf<String, Int>()
        var successMetric: String? = null
    }

    private val id = String.format(Locale.ROOT, "%x", (0..1024).random())
    private val notificationId = 9203
    private lateinit var activeDescription: Description

    private val api by lazy {
        Retrofit.Builder()
            .client(OkHttpClient.Builder().build())
            .baseUrl("http://10.0.2.2:9900")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(Api::class.java)
    }

    private fun getNotificationManager(): NotificationManager {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        return context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
    }

    override fun starting(description: Description) {
        super.starting(description)
        activeDescription = description
        doStart()
    }

    override fun failed(e: Throwable?, description: Description) {
        super.failed(e, description)
        doFailure(e)
    }

    override fun succeeded(description: Description) {
        super.succeeded(description)
        //region Cancel notification
        Handler(Looper.getMainLooper()).post {
            getNotificationManager().cancel(notificationId)
        }
        //endregion
        //region Drop recording
        Timber.i("Drop recording")
        description.sendCommand(command = "success", details = successMetric)
        //endregion
    }

    fun doFailure(e: Throwable?) {
        //region Get screenshot and logs
        val bitmap = Screenshot.capture().bitmap
        val screenshotBytes = ByteArrayOutputStream()
        val logs = CachedAntiLog.getCache()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            bitmap.compress(Bitmap.CompressFormat.WEBP_LOSSY, 60, screenshotBytes)
        } else {
            @Suppress("DEPRECATION")
            bitmap.compress(Bitmap.CompressFormat.WEBP, 60, screenshotBytes)
        }
        //endregion
        //region Cancel notification
        Handler(Looper.getMainLooper()).post {
            getNotificationManager().cancel(notificationId)
        }
        //endregion
        //region Save recording
        Timber.i("Save recording")
        activeDescription.sendCommand(
            command = "failure",
            screenshot = Base64.encodeToString(screenshotBytes.toByteArray(), Base64.NO_WRAP),
            logs = logs,
            details = e?.message?.lines()?.firstOrNull()
        )
        //endregion
    }

    fun doStart() {
        //region Count attempts
        successMetric = null
        val attempt = counters.getOrDefault(activeDescription.testId, 0) + 1
        counters[activeDescription.testId] = attempt
        //endregion
        Timber.w("Starting ${activeDescription.testName}")
        //region Reset Timber
        CachedAntiLog.clear()
        CachedAntiLog.minPriority = LogLevel.VERBOSE
        //endregion
        //region Trigger recording
        Timber.i("Trigger recording")
        activeDescription.sendCommand("start")
        //endregion
        //region Show notification
        if (showNotification) {
            Handler(Looper.getMainLooper()).post {
                val manager = getNotificationManager()
                val channel = "tests"
                val c = NotificationChannel(id, "Tests", NotificationManager.IMPORTANCE_HIGH)
                manager.createNotificationChannel(c)
                val notification = Notification.Builder(InstrumentationRegistry.getInstrumentation().targetContext, channel)
                    .setContentTitle("#$attempt: " + activeDescription.className)
                    .setContentText(activeDescription.methodName)
                    .setSmallIcon(R.drawable.ic_test_notification)
                    .setColor(Color.MAGENTA)
                    .build()
                manager.notify(notificationId, notification)
            }
        }
        //endregion
    }

    private val Description.testId get() = "$className#$methodName"
    private val Description.testName get() = "${className.split(".").last()}___${methodName}___${id}___${counters[testId]}"

    private fun Description.sendCommand(command: String, screenshot: String? = null, logs: String? = null, details: String? = null) = runBlocking {
        Timber.i("Documentation command: $command")
        try {
            api.sendCommand(
                Command(
                    command = command,
                    testId = testName,
                    testClass = className,
                    testMethod = methodName,
                    attempt = counters[testId] ?: -1,
                    screenshot = screenshot,
                    logs = logs,
                    details = details,
                )
            )
        } catch (e: Exception) {
            Timber.e("Failed to document test! (${e::class.simpleName}: ${e.message}")
        }
    }

    interface Api {
        @POST("/")
        suspend fun sendCommand(@Body command: Command)
    }

    data class Command(
        val command: String,
        val testId: String,
        val testClass: String,
        val testMethod: String,
        val attempt: Int,
        val screenshot: String?,
        val logs: String?,
        val details: String?,
    )
}
