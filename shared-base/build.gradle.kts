plugins {
    kotlin("multiplatform")
    id("com.android.library")
    id("kotlin-parcelize")
    kotlin("plugin.serialization") version "1.7.20"
}

kotlin {
    android()

    val wrappedProjects = listOf(
        ":shared-common",
        ":shared-engines",
        ":shared-external-apis"
    )

    listOf(
        iosX64(),
        iosArm64(),
        iosSimulatorArm64()
    ).forEach {
        it.binaries.framework {
            baseName = "OctoAppBase"
            wrappedProjects.forEach { p ->
                export(project(p))
            }
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                wrappedProjects.forEach { p ->
                    api(project(p))
                }

                implementation("com.russhwolf:multiplatform-settings:1.0.0-RC")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        val androidMain by getting {
            dependsOn(commonMain)
            dependencies {
                val firebaseVersion = project.rootProject.ext.get("firebase_version") as String
                val billingVersion = project.rootProject.ext.get("billing_version") as String

                // Firebase
                implementation(project.dependencies.platform("com.google.firebase:firebase-bom:$firebaseVersion"))
                api("com.google.firebase:firebase-auth-ktx")
                api("com.google.firebase:firebase-analytics-ktx")
                api("com.google.firebase:firebase-config-ktx")
                api("com.google.firebase:firebase-crashlytics")
                api("com.google.firebase:firebase-messaging-ktx")

                // Billing
                api("com.android.billingclient:billing:$billingVersion")
                api("com.android.billingclient:billing-ktx:$billingVersion")
                api("com.google.android.gms:play-services-wearable:17.1.0")

                // Local backup DNS
                implementation("com.qiniu:happy-dns:0.2.18")
                implementation("com.github.andriydruk:dnssd:0.9.16")
            }
        }
        val androidTest by getting
        val iosX64Main by getting
        val iosArm64Main by getting
        val iosSimulatorArm64Main by getting
        val iosMain by creating {
            dependsOn(commonMain)
            iosX64Main.dependsOn(this)
            iosArm64Main.dependsOn(this)
            iosSimulatorArm64Main.dependsOn(this)
        }
        val iosX64Test by getting
        val iosArm64Test by getting
        val iosSimulatorArm64Test by getting
        val iosTest by creating {
            iosX64Test.dependsOn(this)
            iosArm64Test.dependsOn(this)
            iosSimulatorArm64Test.dependsOn(this)
        }
    }
}

android {
    namespace = "de.crysxd.octoapp.sharedbase"
    compileSdk = project.rootProject.ext.get("target_sdk") as Int
    defaultConfig {
        minSdk = project.rootProject.ext.get("min_sdk") as Int
        targetSdk = project.rootProject.ext.get("target_sdk") as Int
    }
}
