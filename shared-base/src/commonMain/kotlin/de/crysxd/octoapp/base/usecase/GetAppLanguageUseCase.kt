package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.sharedcommon.Platform


class GetAppLanguageUseCase(
    private val octoPreferences: OctoPreferences,
    private val platform: Platform,
) : UseCase2<Unit, GetAppLanguageUseCase.Result>() {

    init {
        suppressLogging = true
    }

    override suspend fun doExecute(param: Unit, logger: Logger): Result {
        val confirmedLanguages = listOf("de", "fr", "es") // If device language is listed here, it will be used as default
        val deviceLanguage = platform.deviceLanguage
        val appLanguage = octoPreferences.appLanguage ?: deviceLanguage.takeIf { confirmedLanguages.contains(it) } ?: "en"

        logger.i("Device language: $deviceLanguage")
        logger.i("App language: $appLanguage")

        val switchLanguageText = when {
            appLanguage != "en" -> "Use OctoApp in English"
            deviceLanguage == "de" -> "Nutze OctoApp in Deutsch"
            deviceLanguage == "nl" -> "Gebruik in het Nederlands (Beta)"
            deviceLanguage == "fr" -> "Utilisation en français"
            deviceLanguage == "es" -> "Uso en español (Beta)"
            deviceLanguage == "it" -> "Uso in italiano (Beta)"
            else -> null
        }

        val switchLanguageLocale = when {
            appLanguage != "en" -> "en"
            listOf("de", "nl", "it", "es", "fr").contains(deviceLanguage) -> deviceLanguage
            else -> null
        }

        return Result(
            appLanguage = appLanguage,
            canSwitchLocale = switchLanguageText != null,
            switchLanguageText = switchLanguageText,
            switchLanguage = switchLanguageLocale
        )
    }

    data class Result(
        val appLanguage: String,
        val canSwitchLocale: Boolean,
        val switchLanguageText: String?,
        val switchLanguage: String?
    )
}