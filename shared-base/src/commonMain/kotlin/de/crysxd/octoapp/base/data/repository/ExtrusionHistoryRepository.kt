package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.data.models.ExtrusionHistoryItem
import de.crysxd.octoapp.base.data.repository.base.HistoryRepository
import de.crysxd.octoapp.base.io.SettingsStore
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json


class ExtrusionHistoryRepository(
    settings: SettingsStore,
    json: Json,
) : HistoryRepository<ExtrusionHistoryItem, Int>(
    settings = settings.forNameSpace("extrusion-history"),
    defaults = listOf(-10, 10, 50, 100, 120).map { ExtrusionHistoryItem(it) },
    key = "history",
    json = json,
) {

    override val ExtrusionHistoryItem.order get() = lastUsed
    override val ExtrusionHistoryItem.id get() = distanceMm

    override fun createNew(id: Int) = ExtrusionHistoryItem(distanceMm = id)
    override fun Json.serialize(list: List<ExtrusionHistoryItem>): String = encodeToString(list)
    override fun Json.deserialize(text: String): List<ExtrusionHistoryItem> = decodeFromString(text)

    suspend fun toggleFavourite(distanceMm: Int) = updateHistory(distanceMm) {
        it.copy(isFavorite = !it.isFavorite)
    }

}