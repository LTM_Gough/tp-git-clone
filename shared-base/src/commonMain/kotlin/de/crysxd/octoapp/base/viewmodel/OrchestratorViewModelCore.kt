package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.composeErrorMessage
import de.crysxd.octoapp.base.ext.composeMessageStack
import de.crysxd.octoapp.base.network.BrokenSetupException
import de.crysxd.octoapp.base.usecase.UpdateInstanceCapabilitiesUseCase
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.models.connection.ConnectionType
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.sync.Mutex
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class OrchestratorViewModelCore {

    private val tag = "Orchestrator"
    private val configRepository = SharedBaseInjector.get().printerConfigRepository
    private val printerProvider = SharedBaseInjector.get().octoPrintProvider
    private val preferences = SharedBaseInjector.get().preferences
    private val updateInstanceCapabilitiesUseCase get() = SharedBaseInjector.get().updateInstanceCapabilitiesUseCase()
    private val activeConfig = configRepository.instanceInformationFlow()

    private var lastSuccessfulCapabilitiesUpdate = Instant.DISTANT_PAST
    private var lastCapabilitiesUpdateConnectionType: ConnectionType? = null
    private var capabilityUpdateMutex = Mutex()
    private val manualNavigationState = MutableStateFlow<Pair<NavigationState, Instant>?>(null)

    private val showInstanceInBannerFlow = preferences.updatedFlow2
        .map { it.showActiveInstanceInStatusBar }
        .distinctUntilChanged()

    val connectionState = activeConfig
        .filterNotNull()
        .map { it }
        .flatMapLatest { instance -> printerProvider.eventFlow(tag = "orchestrator", instanceId = instance.id).map { instance to it } }
        .onEach { (instance, event) -> processEvent(instance.id, event) }
        .filter { (_, event) -> event is Event.Connected || event is Event.Disconnected }
        .combine(showInstanceInBannerFlow) { other, showLabelInBanner -> Triple(other.first, other.second, showLabelInBanner) }
        .map { (instance, event, showLabelInBanner) ->
            ConnectionState(
                connectionType = (event as? Event.Connected)?.connectionType,
                instanceLabel = instance.label,
                instanceColor = instance.settings?.appearance?.color ?: "default",
                showLabelInBanner = showLabelInBanner,
            )
        }

    val errorEvents = ExceptionReceivers.flow.map {
        Napier.w(tag = tag, message = "Showing error dialog for ${it::class.simpleName} (${it.message})")
        ErrorEvent(
            message = it.composeErrorMessage(),
            messageString = it.composeErrorMessage().toString(),
            detailsMessage = it.composeMessageStack(),
            detailsMessageString = it.composeMessageStack().toString(),
            action = when {
                it is BrokenSetupException && it.needsRepair -> {
                    { manualNavigationState.value = NavigationState.Repair(it.instance.id) to Clock.System.now() }
                }
                else -> null
            }
        )
    }

    val navigationState = activeConfig.map {
        Napier.i(tag = tag, message = "Mapping config: $it")

        when (it) {
            null -> NavigationState.Login
            else -> NavigationState.Active(instanceId = it.id, colorName = it.settings?.appearance?.color, instanceLabel = it.label)
        } to Clock.System.now()
    }.combine(manualNavigationState) { auto, manual ->
        (manual?.takeIf { it.second > auto.second } ?: auto).first
    }

    suspend fun onAppOpen() {
        Napier.i(tag = tag, message = "UI started")
        activeConfig.first()?.id?.let { id ->
            considerUpdateCapabilities(trigger = "app-open", instanceId = id)
        } ?: Napier.d(tag = tag, message = "No instance active, skipping capabilities update")
    }

    suspend fun onAppClose() {
        Napier.i(tag = tag, message = "UI closed")
    }

    private suspend fun processEvent(instanceId: String, event: Event) = when (event) {
        is Event.Connected -> {
            Napier.i(tag = tag, message = "CONNECTED to $instanceId")
            if (lastCapabilitiesUpdateConnectionType != event.connectionType) {
                // If the connection type changes, we force refresh capabilities
                updateCapabilities(trigger = "connected-new-type", instanceId = instanceId)
            } else {
                // If the connection type stayed the same, we consider the update
                considerUpdateCapabilities(trigger = "connected", instanceId = instanceId)
            }
        }

        is Event.MessageReceived -> processMessageReceived(message = event.message, instanceId = instanceId)

        is Event.Disconnected -> {
            Napier.i(tag = tag, message = "DISCONNECTED from $instanceId")
        }
    }

    private suspend fun processMessageReceived(message: Message, instanceId: String) = when (message) {
        is Message.Event.SettingsUpdated,
        is Message.Event.PrinterProfileModified,
        is Message.Event.PrinterConnected -> updateCapabilities(trigger = "settings-update", instanceId = instanceId)
        else -> Unit
    }

    private suspend fun considerUpdateCapabilities(trigger: String, instanceId: String) {
        if (Clock.System.now() > (lastSuccessfulCapabilitiesUpdate + 30.seconds)) {
            updateCapabilities(trigger = trigger, instanceId = instanceId)
        } else {
            Napier.i(tag = tag, message = "Capabilities requested from $trigger but updated ${Clock.System.now() - lastSuccessfulCapabilitiesUpdate} ago, skipping")
        }
    }

    private suspend fun updateCapabilities(trigger: String, instanceId: String) {
        if (capabilityUpdateMutex.tryLock()) {
            try {
                Napier.w(tag = tag, message = "Capability update triggered from $trigger")
                updateInstanceCapabilitiesUseCase.execute(UpdateInstanceCapabilitiesUseCase.Params(instanceId = instanceId))
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to update capabilities", throwable = e)
            } finally {
                capabilityUpdateMutex.unlock()
            }
        } else {
            Napier.w(tag = tag, message = "Capability update busy, skipping")
        }
    }

    data class ErrorEvent(
        val message: CharSequence,
        val messageString: String,
        val detailsMessage: CharSequence?,
        val detailsMessageString: String?,
        val action: (() -> Unit)?,
    )

    data class ConnectionState(
        val connectionType: ConnectionType?,
        val instanceLabel: String,
        val instanceColor: String,
        val showLabelInBanner: Boolean,
    )

    sealed class NavigationState {
        object Login : NavigationState()

        data class Active(
            val instanceId: String,
            val colorName: String?,
            val instanceLabel: String
        ) : NavigationState()

        data class Repair(
            val instanceId: String
        ) : NavigationState()
    }
}