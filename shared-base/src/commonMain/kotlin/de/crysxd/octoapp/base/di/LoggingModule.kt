package de.crysxd.octoapp.base.di

import de.crysxd.octoapp.base.logging.SensitiveDataMask
import de.crysxd.octoapp.sharedcommon.di.BaseModule
import org.koin.dsl.module


class LoggingModule : BaseModule {

    private fun provideSensitiveDataMask() = SensitiveDataMask

    override val koinModule = module {
        single { provideSensitiveDataMask() }
    }
}