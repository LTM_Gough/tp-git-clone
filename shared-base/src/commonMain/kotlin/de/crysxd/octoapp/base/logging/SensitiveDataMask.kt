package de.crysxd.octoapp.base.logging

import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.http.framework.redactLoggingString
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import io.ktor.client.call.NoTransformationFoundException
import io.ktor.http.Url
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

object SensitiveDataMask {

    private val lock = Mutex()
    private val masks = mutableMapOf<String, (String) -> String>()

    suspend fun registerWebUrl(webUrl: Url?) = lock.withLock {
        masks[webUrl.toString()] = {
            webUrl?.redactLoggingString(it) ?: it
        }
    }

    suspend fun registerApiKey(apiKey: String) = lock.withLock {
        if (apiKey.isNotBlank()) {
            masks[apiKey] = {
                it.replace(apiKey, "\${api_key}")
            }
        }
    }

    suspend fun registerInstance(instance: OctoPrintInstanceInformationV3) {
        registerWebUrl(instance.webUrl)
        registerWebUrl(instance.alternativeWebUrl)
        registerWebUrl(instance.settings?.webcam?.streamUrl?.toUrlOrNull())
        registerApiKey(instance.apiKey)
        instance.settings?.plugins?.multiCamSettings?.profiles?.forEach { webcam ->
            registerWebUrl(webcam.streamUrl?.toUrlOrNull())
        }
    }

    suspend fun mask(input: String): String = lock.withLock {
        var output = input

        masks.forEach {
            output = it.value(output)
        }

        return output
    }

    suspend fun needsMasking(throwable: Throwable?): Boolean {
        // Already masked
        if (throwable == null) return false
        if (throwable is NetworkException) return false

        // Known offenders
        if (throwable is NoTransformationFoundException) return true

        // Test
        return mask(throwable.message ?: "") != throwable.message || needsMasking(throwable.cause)
    }
}