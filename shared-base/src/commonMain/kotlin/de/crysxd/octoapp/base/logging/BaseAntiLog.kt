package de.crysxd.octoapp.base.logging

import de.crysxd.octoapp.base.utils.AppScope
import io.github.aakira.napier.Antilog
import io.github.aakira.napier.LogLevel
import io.github.aakira.napier.LogLevel.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

@OptIn(ExperimentalCoroutinesApi::class)
abstract class BaseAntiLog : Antilog() {

    abstract val name: String
    abstract val writeTime: Boolean
    abstract val writeExceptionAsText: Boolean
    abstract val minPriority: LogLevel
    abstract val timeZone: TimeZone

    private val mutex = Mutex()
    private val context by lazy { newSingleThreadContext(name) }
    private val tagLength = 28
    open val lineLength = 150
    private val exceptionFilter = ExceptionFilter()

    abstract fun writeLine(line: String)
    abstract fun writeException(t: Throwable)

    override fun performLog(priority: LogLevel, tag: String?, throwable: Throwable?, message: String?) {
        if (priority < minPriority) return

        AppScope.launch(context) {
            try {
                mutex.withLock {
                    performLogInner(priority, tag, throwable, message)
                }
            } catch (e: Throwable) {
                println("WARNING! Exception in logging:")
                e.printStackTrace()
            }
        }
    }

    private suspend fun performLogInner(priority: LogLevel, tag: String?, throwable: Throwable?, message: String?) {
        val time = if (writeTime) {
            val date = Clock.System.now()
            date.toLocalDateTime(timeZone).time.formatted()
        } else {
            null
        }
        val tagShortened = (tag.takeUnless { it == "TimberAntiLog" } ?: "???").take(tagLength)
        val tagPadded = tagShortened + " ".repeat(tagLength - tagShortened.length)
        val prefix = listOfNotNull(priority.char, time, tagPadded, "").joinToString(" | ")
        val prefixEmpty by lazy { priority.char + " ".repeat(prefix.length - 4) + "| " }
        val noMessage = message.isNullOrBlank()
        val maskedMessage = message?.takeUnless { noMessage }?.let { SensitiveDataMask.mask(it) }

        maskedMessage?.chunked(lineLength)?.forEachIndexed { index, s ->
            writeLine("${if (index == 0) prefix else prefixEmpty}$s")
        }

        throwable?.let {
            when (exceptionFilter.filter(it)) {
                ExceptionFilter.Result.Drop -> return@let

                ExceptionFilter.Result.Suppress -> {
                    writeLine("$prefixEmpty${it::class.qualifiedName}: ${SensitiveDataMask.mask(it.message ?: "")}")
                    it.stackTraceToString().split("\n").filter { it.startsWith("Caused by") }.forEach { s ->
                        writeLine("$prefixEmpty$s")
                    }
                }

                ExceptionFilter.Result.Log -> {
                    val mask: suspend (String) -> String = if (SensitiveDataMask.needsMasking(it)) {
                        { s -> SensitiveDataMask.mask(s) }
                    } else {
                        { s -> s }
                    }
                    if (writeExceptionAsText) {
                        it.stackTraceToString().split("\n").forEachIndexed { index, s ->
                            if (index == 0) {
                                writeLine("${if (index == 0 && noMessage) prefix else prefixEmpty}${mask(s)}")
                            } else {
                                writeLine("$prefixEmpty${mask(s)}")
                            }
                        }
                    } else {
                        writeException(it)
                        writeLine("$prefixEmpty${it::class.qualifiedName}: ${mask(it.message ?: "")}")
                    }
                }
            }
        }
    }

    private fun LocalTime.formatted() = "${hour.toDigitString()}:${minute.toDigitString()}:${second.toDigitString()}"

    private fun Int.toDigitString(digits: Int = 2): String {
        val s = "$this"
        val prefix = "0".repeat(digits - s.length)
        return "$prefix$s"
    }

    private val LogLevel.char
        get() = when (this) {
            VERBOSE -> "🤍"
            DEBUG -> "💙"
            INFO -> "💚"
            WARNING -> "💛"
            ERROR -> "🧡"
            ASSERT -> "❤️"
        }
}