package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.engine.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.engine.framework.isNgrokUrl
import de.crysxd.octoapp.sharedcommon.http.framework.withBasicAuth
import io.ktor.http.Url

class RemoteAccessNgrokViewModelCore(instanceId: String) : RemoteAccessBaseViewModelCore(instanceId) {

    override val tag = "RemoteAccessNgrokViewModelCore"
    override val remoteServiceName = RemoteServiceConnectionBrokenException.REMOTE_SERVICE_NGROK
    override fun Url.isMyUrl() = isNgrokUrl()
    override fun modifyUrl(url: Url) = url.withBasicAuth(user = "xxx", password = "xxx")
}