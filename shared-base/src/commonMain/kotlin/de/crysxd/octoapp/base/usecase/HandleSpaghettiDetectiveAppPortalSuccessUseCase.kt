package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase.Companion.INSTANCE_ID_PATH_PREFIX
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import io.ktor.http.Url

class HandleSpaghettiDetectiveAppPortalSuccessUseCase(
    val octoPrintRepository: OctoPrintRepository
) : UseCase2<Url, Unit>() {

    override suspend fun doExecute(param: Url, logger: Logger) {
        try {
            logger.i("Handling connection result for Spaghetti Detective")
            val tunnelUrl = param.parameters["tunnel_endpoint"]?.toUrlOrNull()
            val instanceId = param.pathSegments.firstOrNull { it.startsWith(INSTANCE_ID_PATH_PREFIX) }?.removePrefix(INSTANCE_ID_PATH_PREFIX)
                ?: throw IllegalStateException("No instance id found in URL")

            val isSuccess = tunnelUrl != null
            if (!isSuccess) {
                throw IllegalStateException("Connection was not successful")
            }

            octoPrintRepository.update(id = instanceId) {
                it.copy(
                    alternativeWebUrl = tunnelUrl,
                    octoEverywhereConnection = null,
                    remoteConnectionFailure = null,
                )
            }
        } catch (e: Exception) {
            OctoAnalytics.logEvent(OctoAnalytics.Event.SpaghettiDetectiveConnectFailed)
            throw e
        }

        OctoAnalytics.logEvent(OctoAnalytics.Event.SpaghettiDetectiveConnected)
        OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.SpaghettiDetectiveUser, "true")
        OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.RemoteAccess, "spaghetti_detective")
        logger.i("Stored connection info for Spaghetti Detective")
    }
}