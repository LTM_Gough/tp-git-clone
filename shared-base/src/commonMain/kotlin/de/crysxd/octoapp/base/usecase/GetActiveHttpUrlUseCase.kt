package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.OctoPrintProvider
import io.ktor.http.Url
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map

class GetActiveHttpUrlUseCase(
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
) : UseCase2<OctoPrintInstanceInformationV3?, Flow<Url>>() {

    @OptIn(ExperimentalCoroutinesApi::class)
    override suspend fun doExecute(param: OctoPrintInstanceInformationV3?, logger: Logger): Flow<Url> {
        val instanceId = (param ?: octoPrintRepository.getActiveInstanceSnapshot() ?: throw IllegalStateException("No OctoPrint active")).id
        val octoPrint = param?.let {
            octoPrintProvider.octoPrintFlow(param.id)
        } ?: let {
            octoPrintProvider.octoPrintFlow()
        }

        return octoPrint.flatMapLatest {
            it ?: return@flatMapLatest emptyFlow()
            // Probe connection to ensure primary or alternative URL is active
            if (octoPrintProvider.getCurrentConnection(instanceId) == null) {
                logger.d("OctoPrint not connected, probing connection")
                it.versionApi.getVersion()
            } else {
                logger.d("OctoPrint connected, relying on active web url")
            }
            it.baseUrl
        }.map {
            it
        }
    }
}