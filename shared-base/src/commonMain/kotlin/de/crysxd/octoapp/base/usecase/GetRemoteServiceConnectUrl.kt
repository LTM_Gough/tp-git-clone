package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.utils.texts.getString
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.exceptions.PrinterApiException
import de.crysxd.octoapp.engine.exceptions.PrinterUnavailableException
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngine
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import io.ktor.http.encodeURLQueryComponent


class GetRemoteServiceConnectUrlUseCase constructor(
    private val octoPrintProvider: OctoPrintProvider
) : UseCase2<GetRemoteServiceConnectUrlUseCase.Params, GetRemoteServiceConnectUrlUseCase.Result>() {

    companion object {
        const val OCTOEVERYWHERE_APP_PORTAL_CALLBACK_PATH = "connect-octoeverywhere"
        const val SPAGHETTI_DETECTIVE_APP_PORTAL_CALLBACK_PATH = "connect-spaghetti-detective"
        const val INSTANCE_ID_PATH_PREFIX = "instance-"
    }

    override suspend fun doExecute(param: Params, logger: Logger) = try {
        val service = param.remoteService
        val printerId = service.getPrinterId(octoPrintProvider.octoPrint().asOctoPrint())
        val callbackPath = service.getCallbackPath(param.instanceId)
        logger.w("Printer id $printerId")
        logger.w("callback $callbackPath")
        val url = service.getConnectUrl()
            .replace("{{{printerid}}}", (printerId ?: "").encodeURLQueryComponent(encodeFull = true))
            .replace("{{{callbackPath}}}", callbackPath.encodeURLQueryComponent(encodeFull = true))
        logger.w("Url $url")

        Result.Success(url)
    } catch (e: Exception) {
        val message = param.remoteService.getMessageForException(e) ?: getString("error_general")
        Result.Error(message, e)
    }

    class OctoEverywhereNotInstalledException : SuppressedIllegalStateException("OctoEverywhere not installed")

    sealed class RemoteService {
        abstract suspend fun getPrinterId(octoPrint: OctoPrintEngine): String?
        abstract fun getConnectUrl(): String
        abstract fun getCallbackPath(instanceId: String): String
        abstract fun recordStartEvent()
        abstract fun getMessageForException(e: Throwable): String?

        object OctoEverywhere : RemoteService() {
            private const val tag = "OctoEverywhere"
            override fun getConnectUrl() = OctoConfig.get(OctoConfigField.OctoEverywhereAppPortalUrl)
            override fun getCallbackPath(instanceId: String) = listOf(OCTOEVERYWHERE_APP_PORTAL_CALLBACK_PATH, "$INSTANCE_ID_PATH_PREFIX$instanceId").joinToString("/")
            override fun recordStartEvent() = OctoAnalytics.logEvent(OctoAnalytics.Event.OctoEverywhereConnectStarted)
            override suspend fun getPrinterId(octoPrint: OctoPrintEngine): String? = try {
                octoPrint.octoEverywhereApi.getInfo().printerId
            } catch (e: PrinterApiException) {
                if (e.responseCode == 400 || e.responseCode == 404) {
                    OctoAnalytics.logEvent(OctoAnalytics.Event.OctoEverywherePluginMissing)
                    throw OctoEverywhereNotInstalledException()
                } else {
                    Napier.e(tag = tag, throwable = e, message = "Unable to determine printer ID")
                    null
                }
            } catch (e: Exception) {
                Napier.e(tag = tag, throwable = e, message = "Unable to determine printer ID")
                null
            }

            override fun getMessageForException(e: Throwable) = when (e) {
                is OctoEverywhereNotInstalledException -> getString("configure_remote_acces___octoeverywhere___error_install_plugin")
                is PrinterUnavailableException -> getString("configure_remote_acces___octoeverywhere___error_no_connection")
                else -> null
            }
        }

        data class SpaghettiDetective(val baseUrl: Url?) : RemoteService() {
            private val tag = "SpaghettiDetective"

            override suspend fun getPrinterId(octoPrint: OctoPrintEngine) = try {
                octoPrint.spaghettiDetectiveApi.getPluginStatus().linkedPrinter?.id ?: ""
            } catch (e: Exception) {
                OctoAnalytics.logEvent(OctoAnalytics.Event.SpaghettiDetectivePluginMissing)
                Napier.e(tag = tag, throwable = e, message = "Unable to determine printer ID")
                null
            }

            override fun recordStartEvent() = OctoAnalytics.logEvent(OctoAnalytics.Event.SpaghettiDetectiveConnectStarted)
            override fun getCallbackPath(instanceId: String) =
                listOf(SPAGHETTI_DETECTIVE_APP_PORTAL_CALLBACK_PATH, "$INSTANCE_ID_PATH_PREFIX$instanceId").joinToString("/")

            override fun getConnectUrl() = (baseUrl?.toString() ?: "https://app.obico.io/").removeSuffix("/") + "/" + OctoConfig.get(OctoConfigField.ObicoAppPortalUrl)
            override fun getMessageForException(e: Throwable): String? = null
        }
    }

    data class Params(
        val remoteService: RemoteService,
        val instanceId: String,
    )

    sealed class Result {
        data class Error(val errorMessage: String, val exception: Exception) : Result()
        data class Success(val url: String) : Result()
    }
}