package de.crysxd.octoapp.base.data.repository.base

import com.russhwolf.settings.Settings
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json

abstract class HistoryRepository<T : Any, Id>(
    private val settings: Settings,
    private val json: Json,
    private val key: String,
    private val defaults: List<T>,
) {

    protected abstract val T.order: Long
    protected abstract val T.id: Id
    protected abstract fun createNew(id: Id): T

    private val historyFlow = MutableStateFlow(defaults)
    val history get() = historyFlow.asStateFlow()

    init {
        store(load())
        pushUpdateToChannel()
    }

    protected abstract fun Json.serialize(list: List<T>): String

    protected abstract fun Json.deserialize(text: String): List<T>

    private fun store(list: List<T>) = settings.putString(key = key, value = json.serialize(list))

    private fun load(): List<T> = settings.getStringOrNull(key = key)?.takeUnless { it.isEmpty() }?.let { json.deserialize(it) } ?: defaults

    private fun pushUpdateToChannel() {
        historyFlow.value = load().sortedByDescending { it.order }
    }

    suspend fun updateHistory(id: Id, update: (T) -> T?) = withContext(Dispatchers.Default) {
        val history = load().toMutableList()
        val old = history.firstOrNull { it.id == id } ?: createNew(id)
        history.remove(old)
        update(old)?.let(history::add)
        store(history)
        pushUpdateToChannel()
    }

    suspend fun removeEntry(id: Id) = updateHistory(id) {
        null
    }

    fun import(history: List<T>) {
        store(history)
        pushUpdateToChannel()
    }
}