package de.crysxd.octoapp.base.utils.image

import de.crysxd.octoapp.sharedexternalapis.mjpeg.Image

expect fun Image.applyTransformations(rotate90: Boolean, flipH: Boolean, flipV: Boolean): Image