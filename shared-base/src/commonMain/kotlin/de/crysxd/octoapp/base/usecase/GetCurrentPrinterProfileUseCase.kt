package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.models.printer.PrinterProfile

class GetCurrentPrinterProfileUseCase(
    private val octoPrintProvider: OctoPrintProvider
) : UseCase2<GetCurrentPrinterProfileUseCase.Params, PrinterProfile>() {

    override suspend fun doExecute(param: Params, logger: Logger): PrinterProfile {
        val response = octoPrintProvider.octoPrint(param.instanceId).printerProfileApi.getPrinterProfiles()
        return response.values.firstOrNull { it.current }
            ?: response.values.firstOrNull { it.default }
            ?: PrinterProfile(id = "fallback")
    }

    data class Params(
        val instanceId: String?,
    )
}