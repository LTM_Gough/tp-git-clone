package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import de.crysxd.octoapp.base.models.Faq
import de.crysxd.octoapp.base.models.KnownBug
import io.github.aakira.napier.Napier
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class HelpLauncherViewModelCore {

    private val tag = "HelpLauncherViewModelCore"

    val faq: List<Faq>
        get() = try {
            Json.decodeFromString(OctoConfig.get(OctoConfigField.Faq))
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed to decode FAQ", throwable = e)
            emptyList()
        }

    val knownBugs: List<KnownBug>
        get() = try {
            Json.decodeFromString(OctoConfig.get(OctoConfigField.KnownBugs))
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed to decode known bugs. Unknown bug?", throwable = e)
            emptyList()
        }

    val introUrl: String = OctoConfig.get(OctoConfigField.IntroVideoUrl)
}