package de.crysxd.octoapp.base.network

import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException

class BrokenSetupException(
    val original: NetworkException,
    override val userMessage: String,
    val needsRepair: Boolean,
    val instance: OctoPrintInstanceInformationV3,
) : Exception("Broken setup: ${original.message}", original), UserMessageException