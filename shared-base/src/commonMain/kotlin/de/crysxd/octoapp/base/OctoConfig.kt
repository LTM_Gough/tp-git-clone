package de.crysxd.octoapp.base

expect object OctoConfig {
    fun getLong(field: OctoConfigField<Long>): Long
    fun get(field: OctoConfigField<String>): String
    fun getBoolean(field: OctoConfigField<Boolean>): Boolean
}

sealed class OctoConfigField<T>(val key: String, val default: T) {
    object ConnectionTimeout : OctoConfigField<Long>("connection_timeout_ms", default = 0)
    object ReadWriteTimeout : OctoConfigField<Long>("read_write_timeout_ms", default = 0)
    object WebSocketPingPongTimeout : OctoConfigField<Long>("web_socket_ping_pong_timeout_ms", default = 0)
    object SignInHelpUrl : OctoConfigField<String>("help_url_sign_in", "https://youtu.be/mlWfaCuvDL8")
    object DefaultPlugins : OctoConfigField<String>("default_plugins", default = "")
    object AndroidNewDnsSdEnabled : OctoConfigField<Boolean>("new_dnssd_enabled", default = true)
    object Faq : OctoConfigField<String>("faq", default = "[]")
    object KnownBugs : OctoConfigField<String>("known_bugs", default = "[]")
    object ContactTimeZone : OctoConfigField<String>("contact_timezone", default = "CET")
    object ContactEmail : OctoConfigField<String>("contact_email", default = "hello@octoapp.eu")
    object IntroVideoUrl : OctoConfigField<String>("introduction_video_url", default = "https://youtu.be/lKJhWnLUrHA")
    object OctoEverywhereAppPortalUrl : OctoConfigField<String>(
        "octoeverywhere_app_portal_url",
        default = "https://octoeverywhere.com/appportal/v1?appId=octoapp&authType=enhanced&returnUrl=octoapp%3A%2F%2Fapp.octoapp.eu%2F{{{callbackPath}}}&printerId={{{printerid}}}&appLogoUrl=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Foctoapp-4e438.appspot.com%2Fo%2Fresources%252Foctoeverywhere%252Flogo.png%3Falt%3Dmedia%26token%3D22e614ef-87a9-44a9-96d1-54a3f81dfcad"
    )

    object ObicoAppPortalUrl : OctoConfigField<String>(
        "spaghetti_detective_app_portal_url",
        default = "tunnels/new/?app=OctoApp&printer_id={{{printerid}}}&success_redirect_url=octoapp%3A%2F%2Fapp.octoapp.eu%2F{{{callbackPath}}}"
    )
}

