package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.utils.image.applyTransformations
import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import de.crysxd.octoapp.sharedexternalapis.mjpeg.Image

class ApplyWebcamTransformationsUseCase : UseCase2<ApplyWebcamTransformationsUseCase.Params, Image>() {

    init {
        suppressLogging = true
    }

    override suspend fun doExecute(param: Params, logger: Logger) = param.frame.applyTransformations(
        rotate90 = param.settings.rotate90,
        flipH = param.settings.flipH,
        flipV = param.settings.flipV,
    )

    data class Params(
        val frame: Image,
        val settings: WebcamSettings,
    )
}