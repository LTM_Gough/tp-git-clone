package de.crysxd.octoapp.base.di

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.base.network.DetectBrokenSetupInterceptor
import de.crysxd.octoapp.base.network.NetworkServiceDiscovery
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.di.BaseModule
import de.crysxd.octoapp.sharedcommon.http.config.Dns
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.KeyStoreProvider
import de.crysxd.octoapp.sharedcommon.http.config.LogLevel
import de.crysxd.octoapp.sharedcommon.http.config.ProxySelector
import de.crysxd.octoapp.sharedcommon.http.config.Timeouts
import org.koin.dsl.module

class NetworkModule(
    private val networkServiceDiscovery: NetworkServiceDiscovery,
    private val dns: CachedDns,
) : BaseModule {

    private fun provideBrokenSetupInterceptorFactory(
        printerConfigRepository: OctoPrintRepository,
    ) = DetectBrokenSetupInterceptor.Factory { octoPrintId ->
        DetectBrokenSetupInterceptor(printerConfigRepository, octoPrintId)
    }

    private fun provideHttpSettings(
        keyStore: KeyStoreProvider,
        proxySelector: ProxySelector,
        cachedDns: CachedDns,
    ) = HttpClientSettings(
        timeouts = Timeouts(),
        logLevel = LogLevel.Production,
        keyStore = keyStore,
        proxySelector = proxySelector,
        dns = cachedDns.takeUnless { it == CachedDns.Noop },
    )

    override val koinModule = module {
        single { networkServiceDiscovery }
        single { OctoPrintProvider(get(), get(), get(), OctoAnalytics, get(), get()) }
        single { provideBrokenSetupInterceptorFactory(get()) }
        single { createKeyStoreProvider(get()) }
        single { createProxySelector(get(), get<CachedDns>()) }
        single { dns }

        factory { provideHttpSettings(get(), get(), get()) }
    }
}

internal expect fun createKeyStoreProvider(platform: Platform): KeyStoreProvider
internal expect fun createProxySelector(platform: Platform, dns: Dns): ProxySelector