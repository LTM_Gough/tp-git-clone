package de.crysxd.octoapp.base.data.models

data class SerialCommunication(
    val content: String,
    val date: Long,
    val serverDate: Long?,
    val source: Source
) {

    sealed class Source {
        object OctoPrint : Source()
        object OctoAppInternal : Source()
        object User : Source()
    }
}