package de.crysxd.octoapp.base.models

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import kotlinx.serialization.Serializable

@Serializable
@CommonParcelize
data class Faq(
    val id: String? = null,
    val hidden: Boolean? = null,
    val title: String? = null,
    val content: String? = null,
    val youtubeUrl: String? = null,
    val youtubeThumbnailUrl: String? = null,
) : CommonParcelable
