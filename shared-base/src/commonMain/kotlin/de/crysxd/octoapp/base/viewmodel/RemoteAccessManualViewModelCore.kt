package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.engine.framework.isNgrokUrl
import de.crysxd.octoapp.engine.framework.isOctoEverywhereUrl
import de.crysxd.octoapp.engine.framework.isSpaghettiDetectiveUrl
import io.ktor.http.Url

class RemoteAccessManualViewModelCore(instanceId: String) : RemoteAccessBaseViewModelCore(instanceId) {

    override val tag = "RemoteAccessManualViewModelCore"
    override val remoteServiceName = "none"
    override fun Url.isMyUrl() = !isNgrokUrl() && !isOctoEverywhereUrl() && !isSpaghettiDetectiveUrl()

    suspend fun setManual(
        url: String,
        user: String,
        password: String,
        skipTest: Boolean
    ) = applyUrl(
        url = url,
        user = user,
        password = password,
        skipTest = skipTest,
    )
}