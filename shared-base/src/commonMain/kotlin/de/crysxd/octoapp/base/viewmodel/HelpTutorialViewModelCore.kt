package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.sharedexternalapis.tutorials.model.Tutorial
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.update
import kotlinx.datetime.Instant

@OptIn(ExperimentalCoroutinesApi::class)
class HelpTutorialViewModelCore {

    private val tag = "TutorialViewModelCore"
    private val tutorialsRepository = SharedBaseInjector.get().tutorialsRepository
    private val loadTrigger = MutableStateFlow(0 to false)
    val state = loadTrigger.flatMapLatest { (_, skipCache) ->
        flow {
            emit(FlowState.Loading())
            val list = TutorialsList(
                tutorials = tutorialsRepository.getTutorials(skipCache),
                seenUntil = tutorialsRepository.getTutorialsSeenUpUntil()
            )
            emit(FlowState.Ready(list))
        }
    }.catch {
        emit(FlowState.Error(it))
        Napier.e(tag = tag, message = "Failed to load tutorials", throwable = it)
    }.onEach {
        if (it is FlowState.Ready) {
            tutorialsRepository.markTutorialsSeen()
        }
    }

    suspend fun reloadAndWait() {
        reload()
        delay(500)
        state.filter { it !is FlowState.Loading }.first()
    }

    fun reload() {
        loadTrigger.update { (it.first + 1) to true }
    }

    data class TutorialsList(
        val tutorials: List<Tutorial>,
        val seenUntil: Instant,
    )
}