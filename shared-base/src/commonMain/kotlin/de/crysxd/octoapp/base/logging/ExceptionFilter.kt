package de.crysxd.octoapp.base.logging

import de.crysxd.octoapp.base.exceptions.UnknownHostException
import de.crysxd.octoapp.engine.exceptions.PrinterApiException
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedException
import io.ktor.utils.io.errors.IOException
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.channels.ClosedReceiveChannelException

class ExceptionFilter {

    private var last: Throwable? = null

    fun filter(t: Throwable?): Result = when {
        t == null -> Result.Drop
        t == last -> Result.Drop
        t.isDropped() -> Result.Drop
        t.isSuppressed() -> Result.Suppress
        else -> Result.Log
    }.also {
        last = t
    }

    private fun Throwable.isDropped(): Boolean =
        cause?.isDropped() == true ||
                this is SuppressedException ||
                this is ClosedReceiveChannelException ||
                this is CancellationException ||
                this !is IOException ||
                this !is okio.IOException ||
                this::class.simpleName == "FirebaseRemoteConfigClientException" ||
                this::class.simpleName == "URISyntaxException"

    private fun Throwable.isSuppressed() = this is NetworkException ||
            this is UnknownHostException ||
            this is SuppressedException ||
            this is PrinterApiException

    enum class Result {
        Log, Suppress, Drop
    }
}