package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_CANCEL_PRINT
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_CONFIGURE_REMOTE_ACCESS
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_CUSTOMIZE_WIDGETS
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_HELP
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_OPEN_OCTOPRINT
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_OPEN_TERMINAL
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_PLUGINS
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_SHOW_FILES
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_TURN_PSU_OFF
import de.crysxd.octoapp.base.io.SettingsStore
import de.crysxd.octoapp.base.io.getSerializable
import de.crysxd.octoapp.base.io.putSerializable
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class PinnedMenuItemRepository(
    settingsStore: SettingsStore
) {

    private val settings = settingsStore.forNameSpace("pinned-menu-items")

    private val defaults = mapOf(
        MenuId.MainMenu to setOf(
            MENU_ITEM_OPEN_TERMINAL,
            MENU_ITEM_CANCEL_PRINT,
            MENU_ITEM_SHOW_FILES,
            MENU_ITEM_CONFIGURE_REMOTE_ACCESS,
            MENU_ITEM_CUSTOMIZE_WIDGETS,
            MENU_ITEM_TURN_PSU_OFF,
            MENU_ITEM_HELP,
            MENU_ITEM_PLUGINS
        ),
        MenuId.PrePrintWorkspace to setOf(
            MENU_ITEM_OPEN_OCTOPRINT,
            MENU_ITEM_SHOW_FILES,
            MENU_ITEM_CUSTOMIZE_WIDGETS,
            MENU_ITEM_HELP
        ),
        MenuId.PrintWorkspace to setOf(
            MENU_ITEM_OPEN_OCTOPRINT,
            MENU_ITEM_OPEN_TERMINAL,
            MENU_ITEM_HELP
        ),
        MenuId.Widget to setOf(
            MENU_ITEM_OPEN_OCTOPRINT,
            MENU_ITEM_SHOW_FILES,
            MENU_ITEM_HELP
        )
    )
    private val flows = mutableMapOf<MenuId, MutableStateFlow<Set<String>>>()

    fun isPinned(itemId: String, menuId: MenuId) = getPinnedMenuItems(menuId).contains(itemId)

    fun checkPinnedState(itemId: String) = MenuId.values().filter {
        // We can't store other
        it != MenuId.Other
    }.map {
        it to isPinned(itemId = itemId, menuId = it)
    }

    fun toggleMenuItemPinned(menuId: MenuId, itemId: String) = setMenuItemPinned(menuId = menuId, itemId = itemId, !isPinned(itemId = itemId, menuId = menuId))

    fun setMenuItemPinned(menuId: MenuId, itemId: String, pinned: Boolean) {
        val data = getPinnedMenuItems(menuId).toMutableSet()
        data.remove(itemId)
        if (pinned) {
            data.add(itemId)
        }
        settings.putSerializable(menuId.key, data)
        getFlow(menuId).value = getPinnedMenuItems(menuId)
    }

    fun import(menuId: MenuId, items: Set<String>?) {
        settings.putSerializable(menuId.key, items)
    }

    fun getPinnedMenuItems(menuId: MenuId) = settings.getSerializable(menuId.key, defaults[menuId] ?: emptySet())

    private fun getFlow(menuId: MenuId) = flows.getOrPut(menuId) {
        MutableStateFlow(getPinnedMenuItems(menuId))
    }

    fun observePinnedMenuItems(menuId: MenuId) = getFlow(menuId).asStateFlow()

    private val MenuId.key
        get() = when (this) {
            MenuId.MainMenu -> "main_menu"
            MenuId.PrePrintWorkspace -> "pre_print"
            MenuId.PrintWorkspace -> "print"
            MenuId.Widget -> "widget"
            MenuId.Other -> "other"
        }
}