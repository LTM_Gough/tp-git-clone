package de.crysxd.octoapp.base.data.models

import de.crysxd.octoapp.engine.framework.isBasedOn
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.models.system.SystemCommand
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.version.VersionInfo
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.http.framework.UPNP_ADDRESS_PREFIX
import de.crysxd.octoapp.sharedcommon.utils.UrlSerializer
import de.crysxd.octoapp.sharedcommon.utils.asVersion
import io.ktor.http.Url
import kotlinx.serialization.Serializable
import kotlin.reflect.KClass

@Serializable
data class OctoPrintInstanceInformationV3(
    val id: String,
    val version: VersionInfo? = null,
    val capabilitiesFetchedAt: Long? = null,
    val notificationId: Int? = null,
    @Serializable(with = UrlSerializer::class) val webUrl: Url,
    @Serializable(with = UrlSerializer::class) val alternativeWebUrl: Url? = null,
    val apiKey: String,
    val m115Response: String? = null,
    val systemInfo: SystemInfo? = null,
    val settings: Settings? = null,
    val activeProfile: PrinterProfile? = null,
    val systemCommands: List<SystemCommand>? = null,
    val appSettings: AppSettings? = null,
    val availablePlugins: Map<String, String?>? = null,
    val octoEverywhereConnection: OctoEverywhereConnection? = null,
    val remoteConnectionFailure: RemoteConnectionFailure? = null,
    val lastModifiedAt: Long? = null,
) {

    val label
        get() = settings?.appearance?.name?.takeIf {
            it.isNotBlank()
        } ?: webUrl.let { url ->
            val host = url.host
            val port = ":${url.port}".takeIf { url.protocol.defaultPort != url.port } ?: ""
            if (host.startsWith(UPNP_ADDRESS_PREFIX)) {
                "OctoPrint via UPnP (${host.hashCode().toString(radix = 16).take(3)})"
            } else {
                "$host$port"
            }
        }

    @Deprecated("Do not use", replaceWith = ReplaceWith("id"))
    fun isForWebUrl(webUrl: Url) = webUrl.isBasedOn(this.webUrl) || webUrl.isBasedOn(this.alternativeWebUrl)

    @Deprecated("Do not use", replaceWith = ReplaceWith("id"))
    fun isForPrimaryWebUrl(webUrl: Url) = webUrl.isBasedOn(this.webUrl)


    fun hasPlugin(plugin: String, minVersion: String? = null, maxVersion: String? = null): Boolean {
        // We don't have a real plugin list...fall back on settings check
        if (availablePlugins == null) {
            return when (plugin) {
                OctoPlugins.Obico -> settings?.plugins?.spaghettiDetective != null
                OctoPlugins.CancelObject -> settings?.plugins?.cancelObject != null
                OctoPlugins.Mmu2FilamentSelect -> settings?.plugins?.mmu2FilamentSelect != null
                OctoPlugins.MultiCam -> settings?.plugins?.multiCamSettings != null
                OctoPlugins.Ngrok -> settings?.plugins?.ngrok != null
                OctoPlugins.UploadAnything -> settings?.plugins?.uploadAnything != null
                OctoPlugins.OctoApp -> settings?.plugins?.octoAppCompanion != null
                OctoPlugins.OctoEverywhere -> settings?.plugins?.octoEverywhere != null
                else -> false
            }
        }

        // Use plugin list for check
        val hasPlugin = availablePlugins.containsKey(plugin)
        val version = availablePlugins[plugin]

        // Lenient version check...we assume it's ok if we don't know the version (shouldn't happen)
        val matchesMinVersion = when {
            minVersion == null -> true
            version == null -> true
            else -> version.asVersion() >= minVersion.asVersion()
        }
        val matchesMaxVersion = when {
            maxVersion == null -> true
            version == null -> true
            else -> maxVersion.asVersion() >= version.asVersion()
        }

        return hasPlugin && matchesMinVersion && matchesMaxVersion
    }

    override fun toString() = StringBuilder().also {
        it.append("OctoPrintInstanceInformationV3(")
        it.append("id=$id ")
        it.append("webUrl=$webUrl ")
        it.append("alternativeWebUrl=$alternativeWebUrl ")
        it.append("notificationId=$notificationId ")
        it.append("apiKey=$apiKey ")
        it.append("m115Response=${if (m115Response == null) null else "..."} ")
        it.append("settings=$settings ")
        it.append("activeProfile=$activeProfile ")
        it.append("systemCommands=${if (systemCommands == null) null else "..."} ")
        it.append("appSettings=$appSettings ")
        it.append("octoEverywhereConnection=${if (octoEverywhereConnection == null) null else "..."}\" ")
        it.append("remoteConnectionFailure=$remoteConnectionFailure")
        it.append(")")
    }.toString()
}

fun OctoPrintInstanceInformationV3?.hasPlugin(plugin: KClass<out Settings.PluginSettings>) = when (plugin) {
    Settings.SpaghettiDetective::class -> this?.settings?.plugins?.spaghettiDetective != null
    Settings.CancelObject::class -> this?.settings?.plugins?.cancelObject != null
    Settings.Mmu2FilamentSelect::class -> this?.settings?.plugins?.mmu2FilamentSelect != null
    Settings.MultiCam::class -> this?.settings?.plugins?.multiCamSettings != null
    Settings.Ngrok::class -> this?.settings?.plugins?.ngrok != null
    Settings.UploadAnything::class -> this?.settings?.plugins?.uploadAnything != null
    Settings.OctoAppCompanion::class -> this?.settings?.plugins?.octoAppCompanion != null
    Settings.OctoEverywhere::class -> this?.settings?.plugins?.octoEverywhere != null
    else -> false
}