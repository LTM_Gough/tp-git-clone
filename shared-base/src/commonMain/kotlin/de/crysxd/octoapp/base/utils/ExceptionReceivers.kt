package de.crysxd.octoapp.base.utils

import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

object ExceptionReceivers {

    private val Tag = "ExceptionReceivers"
    private val mutableFlow = MutableSharedFlow<Throwable>(replay = 0)
    val flow = mutableFlow.asSharedFlow()

    val coroutineExceptionHandler = CoroutineExceptionHandler { _, e ->
        dispatchException(e)
        Napier.e(tag = Tag, throwable = e, message = "Exception was reported")
    }

    inline fun runWithErrorMessage(tag: String, block: () -> Unit) = try {
        block()
    } catch (e: Exception) {
        dispatchException(e)
        Napier.e(tag = tag, message = "Caught exception", throwable = e)
    }

    fun dispatchException(throwable: Throwable): Boolean {
        AppScope.launch {
            mutableFlow.emit(throwable)
        }

        return mutableFlow.subscriptionCount.value > 0
    }
}