package de.crysxd.octoapp.base.models

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import kotlinx.serialization.Serializable

@Serializable
@CommonParcelize
class KnownBug(
    val id: String? = null,
    val title: String? = null,
    val status: String? = null,
    val content: String? = null,
) : CommonParcelable