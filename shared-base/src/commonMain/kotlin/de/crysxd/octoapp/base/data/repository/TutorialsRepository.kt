package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedexternalapis.tutorials.TutorialApi
import de.crysxd.octoapp.sharedexternalapis.tutorials.model.Tutorial
import io.github.aakira.napier.Napier
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.days

class TutorialsRepository(
    private val api: TutorialApi,
    private val octoPreferences: OctoPreferences,
    private val platform: Platform,
) {
    companion object {
        private val MAX_AGE_FOR_NEW = 30.days
    }

    private val tag = "TutorialsRepository"
    private var cached: List<Tutorial>? = null

    suspend fun getTutorials(skipCache: Boolean) = cached.takeUnless { skipCache } ?: let {
        val new = api.getTutorials(skipCache, platform.appBuild)
        cached = new
        new
    }

    fun getNewTutorialsCount() = cached?.count {
        val oldestForNew = Clock.System.now() - MAX_AGE_FOR_NEW
        val seenUpUntil = getTutorialsSeenUpUntil()
        it.publishedAt > oldestForNew && it.publishedAt > seenUpUntil
    }?.also {
        Napier.i(tag = tag, message = "$it new tutorials")
    } ?: 0

    fun getTutorialsSeenUpUntil() = octoPreferences.tutorialsSeenAt ?: Instant.fromEpochMilliseconds(1L)

    fun markTutorialsSeen() {
        Napier.i(tag = tag, message = "Marking tutorials as seen")
        octoPreferences.tutorialsSeenAt = Clock.System.now()
    }
}