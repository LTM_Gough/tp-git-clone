package de.crysxd.octoapp.base.data.models

import kotlinx.serialization.Serializable

@Serializable
data class GcodePreviewSettings(
    val showPreviousLayer: Boolean = false,
    val showCurrentLayer: Boolean = false,
    val startCountingLayersAtZero: Boolean = false,
    val quality: Quality = Quality.Medium,
) {
    @Serializable
    enum class Quality {
        Low, Medium, Ultra
    }
}