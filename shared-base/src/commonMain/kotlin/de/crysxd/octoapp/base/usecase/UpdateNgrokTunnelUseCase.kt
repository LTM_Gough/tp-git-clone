package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.framework.isNgrokUrl
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import io.ktor.http.URLBuilder
import io.ktor.http.URLProtocol

class UpdateNgrokTunnelUseCase(
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
) : UseCase2<UpdateNgrokTunnelUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        val tunnelHost = (param as? Params.Tunnel)?.tunnel?.let {
            if (it.startsWith("http")) it.toUrl().host else it
        } ?: octoPrintProvider.octoPrint(param.instanceId).asOctoPrint().ngrokApi.getActiveTunnel().tunnel
        val setting = octoPrintRepository.get(param.instanceId)?.settings?.plugins?.ngrok
            ?: octoPrintProvider.octoPrint(param.instanceId).settingsApi.getSettings().plugins.ngrok
        val tunnelUrl = tunnelHost?.takeUnless { it.isBlank() }?.let { th ->
            URLBuilder().apply {
                protocol = URLProtocol.HTTPS
                host = th
                user = setting?.authName
                password = setting?.authName
            }.build()
        }
        logger.i("$tunnelHost => $tunnelUrl")
        require(tunnelUrl == null || tunnelUrl.isNgrokUrl()) { "Whatever we did...it's wrong. Result not an ngrok URL" }

        octoPrintRepository.update(id = param.instanceId) {
            // We only update if we have no remote connection or it's already a ngrok URL
            if (it.alternativeWebUrl == null || it.alternativeWebUrl.isNgrokUrl()) {
                logger.i("Update ngrok tunnel to $tunnelUrl")
                it.copy(
                    alternativeWebUrl = tunnelUrl,
                    octoEverywhereConnection = null,
                    remoteConnectionFailure = it.remoteConnectionFailure.takeIf { tunnelUrl == null })
            } else {
                logger.i("Not configured for ngrok, will not touch (url=${it.alternativeWebUrl})")
                it
            }
        }
    }

    sealed class Params {
        abstract val instanceId: String

        data class FetchConfig(override val instanceId: String) : Params()
        data class Tunnel(val tunnel: String?, override val instanceId: String) : Params()
    }
}