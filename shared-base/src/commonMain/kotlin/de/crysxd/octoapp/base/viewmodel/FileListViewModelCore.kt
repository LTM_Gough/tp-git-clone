package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.base.data.models.FileListState
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.usecase.StartPrintJobUseCase
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.utils.runForAtLeast
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.datetime.Clock
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class FileListViewModelCore(private val instanceId: String) {

    private val tag = "FileListViewModelCore"
    private val loadFilesUseCase = SharedBaseInjector.get().loadFilesUseCase()
    private val startPrintJobUseCase = SharedBaseInjector.get().startPrintJobUseCase()
    private val printerProvider = SharedBaseInjector.get().octoPrintProvider
    private val printerConfig = SharedBaseInjector.get().printerConfigRepository
    private val preferences = SharedBaseInjector.get().preferences
    private var file: FileObject.File? = null

    private val loadTrigger = MutableStateFlow<LoadTrigger?>(null)
    private val flags = printerProvider.passiveCurrentMessageFlow(tag = "fileList", instanceId = instanceId).map {
        it.state.flags
    }.distinctUntilChanged()
    val fileListState = loadTrigger
        .filterNotNull()
        .flatMapLatest {
            runForAtLeast(duration = if (it.skipCache) 500.milliseconds else 0.seconds) {
                loadFilesUseCase.execute(
                    param = LoadFilesUseCase.Params.ForPath(
                        path = it.path,
                        instanceId = instanceId,
                        fileOrigin = FileOrigin.Local,
                        skipCache = it.skipCache
                    )
                )
            }
        }.onEach {
            file = (it as? FileListState.Loaded)?.file as? FileObject.File
        }.combine(flags) { state, flags ->
            State(
                canStartPrint = flags.isOperational() && !flags.isPrinting(),
                state = state,
            )
        }.catch {
            ExceptionReceivers.dispatchException(it)
            emit(State(state = FileListState.Error(it), canStartPrint = false))
        }

    private val hideInformAboutThumbnailsUntil = preferences.updatedFlow2.map { it.hideThumbnailHintUntil }
    val informAboutThumbnails = printerConfig.instanceInformationFlow(instanceId).combine(hideInformAboutThumbnailsUntil) { config, hideUntil ->
        val hasPrusa = config?.hasPlugin(OctoPlugins.PrusaSlicerThumbnails) == true
        val hasCura = config?.hasPlugin(OctoPlugins.UltimakerFormatPackage) == true
        val hasAny = hasCura || hasPrusa
        val isHidden = hideUntil > Clock.System.now()

        !hasAny && !isHidden
    }

    fun loadFiles(path: String, skipCache: Boolean) {
        loadTrigger.value = LoadTrigger(path = path, skipCache = skipCache, id = (loadTrigger.value?.id ?: 0) + 1)
    }

    fun dismissThumbnailInformation() {
        preferences.hideThumbnailHintUntil = Clock.System.now() + 30.days
    }

    fun startPrint(confirmedMaterial: Boolean, confirmedTimelapse: Boolean) = flow {
        emit(StartPrintResult.Loading)
        val res = startPrintJobUseCase.execute(
            StartPrintJobUseCase.Params(
                file = requireNotNull(file) { "Illegal state: file is null" },
                materialSelectionConfirmed = confirmedMaterial,
                timelapseConfigConfirmed = confirmedTimelapse,
            )
        )

        when (res) {
            StartPrintJobUseCase.Result.MaterialSelectionRequired -> emit(StartPrintResult.MaterialConfirmationRequired)
            StartPrintJobUseCase.Result.PrintStarted -> emit(StartPrintResult.Started)
            StartPrintJobUseCase.Result.TimelapseConfigRequired -> emit(StartPrintResult.TimelapseConfirmationRequired)
        }
    }.catch {
        Napier.e(tag = tag, message = "Failed to start print job", throwable = it)
        ExceptionReceivers.dispatchException(it)
        emit(StartPrintResult.Failed)
    }

    data class State(
        val state: FileListState,
        val canStartPrint: Boolean,
    )

    sealed class StartPrintResult {
        object Loading : StartPrintResult()
        object Failed : StartPrintResult()
        object MaterialConfirmationRequired : StartPrintResult()
        object TimelapseConfirmationRequired : StartPrintResult()
        object Started : StartPrintResult()
    }

    private data class LoadTrigger(
        val path: String,
        val id: Int,
        val skipCache: Boolean,
    )
}