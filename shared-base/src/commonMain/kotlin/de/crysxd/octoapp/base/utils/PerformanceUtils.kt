package de.crysxd.octoapp.base.utils

import io.github.aakira.napier.Napier
import kotlinx.datetime.Clock

inline fun <T> measureTime(traceName: String, block: () -> T): T {
    Napier.v(tag = "Performance", message = "[$traceName] started")
    val start = Clock.System.now()
    return try {
        block()
    } finally {
        val end = Clock.System.now()
        val ns = ((end - start).inWholeNanoseconds / 1_000_000f).toString()
        val (whole, fraction) = ns.split(".")
        val text = "$whole.${fraction.take(3).takeIf { it.isNotBlank() } ?: "0"}"
        Napier.v(tag = "Performance", message = "[$traceName] completed in $text ms")
    }
}