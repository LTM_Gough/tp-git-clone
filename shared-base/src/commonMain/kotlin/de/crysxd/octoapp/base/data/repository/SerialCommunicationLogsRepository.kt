package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.data.models.SerialCommunication
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.EventSource
import de.crysxd.octoapp.engine.EventSource.Config.Companion.ALL_LOGS
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Clock


class SerialCommunicationLogsRepository(
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
) {

    companion object {
        const val MAX_COMMUNICATION_ENTRIES = 1000
    }

    private val tag = "SerialCommunicationLogsRepository"
    private val allLogs = mutableMapOf<String?, MutableList<SerialCommunication>>()
    private val allMutex = mutableMapOf<String?, Mutex>()
    private val allFlows = mutableMapOf<String?, MutableSharedFlow<SerialCommunication?>>()
    private val collectionJobs = mutableMapOf<String?, Job>()

    init {
        AppScope.launch(Dispatchers.Default) {
            collectForInstance(null)

            // Keep a tap on the instances and make sure to always passively collect all of them
            octoPrintRepository.allInstanceInformationFlow().collectLatest { all ->
                // Cancel outdated ones if a instance is deleted
                collectionJobs.filter { !all.containsKey(it.key) }.forEach {
                    it.value.cancel()
                    collectionJobs.remove(it.key)
                }

                // Start any new jobs
                all.filter { !collectionJobs.containsKey(it.key) }.forEach {
                    collectionJobs[it.key] = collectForInstance(it.value.id)
                }
            }
        }
    }

    private fun collectForInstance(instanceId: String?) = AppScope.launch(Dispatchers.Default) {
        val logs = getLogs(instanceId)
        val flow = getFlow(instanceId)
        val mutex = getMutex(instanceId)

        octoPrintProvider.passiveEventFlow(instanceId = instanceId)
            .mapNotNull { it as? Event.MessageReceived }
            .mapNotNull { it.message as? Message.Current }
            .onEach {
                mutex.withLock {
                    if (it.isHistoryMessage) {
                        Napier.i(tag = tag, message = "History message received, clearing cache")
                        logs.clear()
                    }

                    val newLogs = it.logs.map { log ->
                        SerialCommunication(
                            content = log,
                            date = Clock.System.now().toEpochMilliseconds(),
                            serverDate = it.serverTime,
                            source = SerialCommunication.Source.OctoPrint
                        )
                    }

                    logs.addAll(newLogs)
                    newLogs.forEach { sc -> flow.emit(sc) }

                    if (logs.size > MAX_COMMUNICATION_ENTRIES) {
                        logs.removeAll(logs.take(logs.size - MAX_COMMUNICATION_ENTRIES))
                    }
                }
            }
            .retry {
                Napier.e(tag = tag, throwable = it, message = "Exception in serial communication flow")
                delay(1000)
                true
            }
            .collect()
    }

    private fun getFlow(instanceId: String?) = allFlows.getOrPut(instanceId) { MutableSharedFlow(0, 10, BufferOverflow.SUSPEND) }

    private fun getLogs(instanceId: String?) = allLogs.getOrPut(instanceId) { mutableListOf() }

    private fun getMutex(instanceId: String?) = allMutex.getOrPut(instanceId) { Mutex() }

    suspend fun addInternalLog(log: String, fromUser: Boolean, instanceId: String? = null) {
        getFlow(instanceId).emit(
            SerialCommunication(
                content = log,
                serverDate = null,
                date = Clock.System.now().toEpochMilliseconds(),
                source = if (fromUser) SerialCommunication.Source.User else SerialCommunication.Source.OctoAppInternal
            )
        )
    }

    fun passiveFlow(includeOld: Boolean = false, instanceId: String? = null) = flow {
        if (includeOld) {
            all().forEach { emit(it) }
        }
        emitAll(getFlow(instanceId))
    }.filterNotNull()

    fun activeFlow(includeOld: Boolean = false, instanceId: String? = null): Flow<SerialCommunication> {
        val flow = flow {
            if (includeOld) {
                all().forEach { emit(it) }
            }
            emitAll(getFlow(instanceId).filterNotNull())
        }

        return octoPrintProvider.eventFlow(
            tag = "activeTerminalFlow",
            instanceId = instanceId,
            config = EventSource.Config(requestTerminalLogs = listOf(ALL_LOGS))
        ).combine(flow) { _, v ->
            Napier.i(tag = tag, message = v.content)
            v
        }
    }

    suspend fun all(instanceId: String? = null) = getMutex(instanceId).withLock {
        getLogs(instanceId).toList()
    }
}