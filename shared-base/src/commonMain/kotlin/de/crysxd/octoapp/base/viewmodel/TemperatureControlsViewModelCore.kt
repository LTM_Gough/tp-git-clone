package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.base.data.repository.TemperatureDataRepository
import de.crysxd.octoapp.base.di.SharedBaseInjector
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach

class TemperatureControlsViewModelCore(val instanceId: String) {

    private val keyComponentCount = "component_count_$instanceId"
    private val settings = SharedBaseInjector.get().settings.forNameSpace("temperature_controls")
    private val temperatureRepository = SharedBaseInjector.get().temperatureDataRepository


    var initialTemperatureComponentCount: Int
        get() = settings.getInt(keyComponentCount, 2)
        private set(value) {
            settings.putInt(keyComponentCount, value)
        }

    // A small cache layer to not read/write from.to the settings the entire time
    private var initialTemperatureComponentCountCache = initialTemperatureComponentCount
        set(value) {
            if (value != field) {
                field = value
                initialTemperatureComponentCount = value
            }
        }

    val temperatures = temperatureRepository.flow(instanceId = instanceId).onEach {
        initialTemperatureComponentCountCache = it.size
    }.map { components ->
        State(components.filter { !it.isHidden })
    }

    data class State(
        val components: List<TemperatureDataRepository.TemperatureSnapshot>
    )
}