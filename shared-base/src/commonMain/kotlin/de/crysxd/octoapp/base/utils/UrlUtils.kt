package de.crysxd.octoapp.base.utils

import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import io.ktor.http.URLBuilder

fun urlFromStringInput(input: String): String? {
    val trimmed = input.trim()
    if (trimmed.isBlank()) {
        return null
    }

    val upgradedWebUrl = if (!trimmed.startsWith("http://") && !trimmed.startsWith("https://")) {
        "http://${trimmed}"
    } else {
        trimmed
    }
    val loginMarker = "/login/?redirect="
    val withoutLogin = if (upgradedWebUrl.contains(loginMarker)) {
        upgradedWebUrl.take(upgradedWebUrl.indexOf(loginMarker))
    } else {
        upgradedWebUrl
    }
    val result = withoutLogin.toUrlOrNull()?.let {
        URLBuilder(it).apply {
            parameters.clear()
        }.buildString()
    }

    return if (result == "http://localhost") {
        null
    } else {
        result
    }
}