package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MjpegConnection3
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart

@OptIn(ExperimentalCoroutinesApi::class)
class WebcamControlsViewModelCore(instanceId: String?) {

    private val getWebcamSettingsUseCase get() = SharedBaseInjector.get().getWebcamSettingsUseCase()
    private val configRepository = SharedBaseInjector.get().printerConfigRepository
    private val httpClientSettings = SharedBaseInjector.get().httpClientSettings()

    val state = configRepository.instanceInformationFlow(instanceId)
        .filterNotNull()
        .flatMapLatest { getWebcamSettingsUseCase.execute(it) }
        .flatMapLatest { settings ->
            val mjpeg = settings.firstNotNullOfOrNull { it as? ResolvedWebcamSettings.MjpegSettings }
                ?: return@flatMapLatest flowOf(UiState.Error(IllegalStateException("No webcam")))
            val connection = MjpegConnection3(
                httpSettings = httpClientSettings,
                streamUrl = mjpeg.url,
                name = "WebcamControlsViewModel",
                throwExceptions = true,
                maxSize = null
            )

            connection.load().map { ms ->
                when (ms) {
                    is MjpegConnection3.MjpegSnapshot.Frame -> UiState.Frame(
                        frame = ms,
                        flipH = mjpeg.webcamSettings.flipH,
                        flipV = mjpeg.webcamSettings.flipV,
                        rotate90 = mjpeg.webcamSettings.rotate90,
                    )
                    MjpegConnection3.MjpegSnapshot.Loading -> UiState.Loading
                }
            }
        }.onStart {
            emit(UiState.Loading)
        }.catch {
            emit(UiState.Error(it))
        }

    sealed class UiState {
        object Loading : UiState()
        data class Frame(val frame: MjpegConnection3.MjpegSnapshot.Frame, val flipH: Boolean, val flipV: Boolean, val rotate90: Boolean) : UiState()
        data class Error(val throwable: Throwable) : UiState()
    }
}