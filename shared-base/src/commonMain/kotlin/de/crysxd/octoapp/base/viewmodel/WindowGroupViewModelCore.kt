package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.withoutQuery
import io.github.aakira.napier.Napier
import io.ktor.http.Url

class WindowGroupViewModelCore {

    private val tag = "WindowGroupViewModelCore"
    private val handleOctoEverywhereAppPortalSuccessUseCase get() = SharedBaseInjector.get().handleOctoEverywhereAppPortalSuccessUseCase()
    private val handleSpaghettiDetectiveAppPortalSuccessUseCase get() = SharedBaseInjector.get().handleSpaghettiDetectiveAppPortalSuccessUseCase()

    suspend fun consumeDeeplink(link: String): Boolean = try {
        Napier.i(tag = tag, message = "Opening URL: ${link.toUrl().withoutQuery()}")
        Napier.v(tag = tag, message = "Detailed URL: ${link.toUrl()}")

        val url = Url(link)
        val pathSegment = url.pathSegments.firstOrNull { it.isNotBlank() }
        Napier.i(tag = tag, message = "Path segment: $pathSegment")


        when (pathSegment) {
            GetRemoteServiceConnectUrlUseCase.OCTOEVERYWHERE_APP_PORTAL_CALLBACK_PATH -> {
                handleOctoEverywhereAppPortalSuccessUseCase.execute(link.toUrl())
                true
            }

            GetRemoteServiceConnectUrlUseCase.SPAGHETTI_DETECTIVE_APP_PORTAL_CALLBACK_PATH -> {
                handleSpaghettiDetectiveAppPortalSuccessUseCase.execute(link.toUrl())
                true
            }

            else -> {
                Napier.i(tag = tag, message = "No default handling for URL: ${url.withoutQuery()}")
                false
            }
        }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to handle deep link", throwable = e)
        ExceptionReceivers.dispatchException(e)
        true
    }
}