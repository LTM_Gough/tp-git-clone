package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.data.models.OctoEverywhereConnection
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase.Companion.INSTANCE_ID_PATH_PREFIX
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.withBasicAuth
import io.ktor.http.Url

class HandleOctoEverywhereAppPortalSuccessUseCase(
    val octoPrintRepository: OctoPrintRepository
) : UseCase2<Url, Unit>() {

    override suspend fun doExecute(param: Url, logger: Logger) {
        try {
            logger.i("Handling connection result for OctoEverywhere")
            val isSuccess = param.parameters["success"]?.toBoolean() == true
            if (!isSuccess) {
                throw IllegalStateException("Connection was not successful")
            }
            val connectionId = param.parameters["id"] ?: throw IllegalStateException("No connection id given")
            val url = param.parameters["url"]?.toUrl() ?: throw IllegalStateException("No url given")
            val authUser = param.parameters["authbasichttpuser"] ?: throw IllegalStateException("No auth user given")
            val authPw = param.parameters["authbasichttppassword"] ?: throw IllegalStateException("No auth pw given")
            val authToken = param.parameters["authBearerToken"] ?: throw IllegalStateException("No auth token given")
            val apiToken = param.parameters["appApiToken"] ?: throw IllegalStateException("No api token given")
            val fullUrl = url.withBasicAuth(user = authUser, password = authPw)
            val instanceId = param.pathSegments.firstOrNull { it.startsWith(INSTANCE_ID_PATH_PREFIX) }?.removePrefix(INSTANCE_ID_PATH_PREFIX)
                ?: throw IllegalStateException("No instance id found in URL")
            logger.i("Handling for instance id $instanceId")

            octoPrintRepository.update(id = instanceId) {
                it.copy(
                    alternativeWebUrl = fullUrl,
                    remoteConnectionFailure = null,
                    octoEverywhereConnection = OctoEverywhereConnection(
                        connectionId = connectionId,
                        apiToken = apiToken,
                        basicAuthPassword = authPw,
                        basicAuthUser = authUser,
                        bearerToken = authToken,
                    ),
                )
            }
        } catch (e: Exception) {
            OctoAnalytics.logEvent(OctoAnalytics.Event.OctoEverywhereConnectFailed)
            throw e
        }

        OctoAnalytics.logEvent(OctoAnalytics.Event.OctoEverywhereConnected)
        OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.OctoEverywhereUser, "true")
        OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.RemoteAccess, "octoeverywhere")
        logger.i("Stored connection info for OctoEverywhere")
    }
}