package de.crysxd.octoapp.base.di

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.ExtrusionHistoryRepository
import de.crysxd.octoapp.base.data.repository.FileListRepository
import de.crysxd.octoapp.base.data.repository.GcodeHistoryRepository
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.data.repository.PinnedMenuItemRepository
import de.crysxd.octoapp.base.data.repository.SerialCommunicationLogsRepository
import de.crysxd.octoapp.base.data.repository.TemperatureDataRepository
import de.crysxd.octoapp.base.data.repository.TutorialsRepository
import de.crysxd.octoapp.base.data.repository.WidgetPreferencesRepository
import de.crysxd.octoapp.sharedcommon.di.BaseModule
import de.crysxd.octoapp.sharedexternalapis.tutorials.TutorialApi
import org.koin.dsl.module


class RepositoryModule : BaseModule {

    override val koinModule = module {
        single { ExtrusionHistoryRepository(get(), get()) }
        single { GcodeHistoryRepository(get(), get()) }
        single { OctoPrintRepository(get(), get(), get()) }
        single { OctoPreferences(get()) }
        single { WidgetPreferencesRepository(get()) }
        single { FileListRepository(get(), get()) }
        single { SerialCommunicationLogsRepository(get(), get()) }
        single { TemperatureDataRepository(get(), get()) }
        single { TutorialsRepository(TutorialApi(), get(), get()) }
        single { PinnedMenuItemRepository(get()) }
    }
}