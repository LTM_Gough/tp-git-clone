package de.crysxd.octoapp.base.data.models

import kotlinx.serialization.SerialName

@kotlinx.serialization.Serializable
enum class WidgetType {

    @SerialName("AnnouncementWidget")
    AnnouncementWidget,

    @SerialName("ControlTemperatureWidget")
    ControlTemperatureWidget,

    @SerialName("ExtrudeWidget")
    ExtrudeWidget,

    @SerialName("GcodePreviewWidget")
    GcodePreviewWidget,

    @SerialName("MoveToolWidget")
    MoveToolWidget,

    @SerialName("PrePrintQuickAccessWidget")
    PrePrintQuickAccessWidget,

    @SerialName("PrintQuickAccessWidget")
    PrintQuickAccessWidget,

    @SerialName("ProgressWidget")
    ProgressWidget,

    @SerialName("QuickAccessWidget")
    QuickAccessWidget,

    @SerialName("SendGcodeWidget")
    SendGcodeWidget,

    @SerialName("TuneWidget")
    TuneWidget,

    @SerialName("WebcamWidget")
    WebcamWidget,

    @SerialName("QuickPrint")
    QuickPrint,

    @SerialName("CancelObject")
    CancelObject,
}