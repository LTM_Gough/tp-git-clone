package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.data.models.FileListState
import de.crysxd.octoapp.base.ext.collectSaveWithRetry
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.launch
import okio.FileNotFoundException

class FileListRepository(
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
) {

    private val tag = "FileListRepository"
    private val fileCache = mutableMapOf<String, MutableStateFlow<FileListState>>()
    private val collectionJobs = mutableMapOf<String?, Job>()

    init {
        AppScope.launch(Dispatchers.Default) {
            // Keep a tap on the instances and make sure to always passively collect all of them
            octoPrintRepository.allInstanceInformationFlow().collectSaveWithRetry { all ->
                // Cancel outdated ones if a instance is deleted
                collectionJobs.filter { !all.containsKey(it.key) }.forEach {
                    it.value.cancel()
                    collectionJobs.remove(it.key)
                }
                // Start any new jobs
                all.filter { !collectionJobs.containsKey(it.key) }.forEach { collectionJobs[it.key] = collectEventsForInstance(it.value.id) }
            }
        }
    }

    private fun collectEventsForInstance(instanceId: String) = AppScope.launch(Dispatchers.Default) {
        Napier.i(tag = tag, message = "Observing $instanceId for file changes change")
        octoPrintProvider.passiveEventFlow(instanceId = instanceId)
            .mapNotNull { it as? Event.MessageReceived }
            .mapNotNull { it.message as? Message.Event.UpdatedFiles }
            .collectSaveWithRetry {
                if (createOrGet(instanceId).value is FileListState.Loaded) {
                    Napier.i(tag = tag, message = "Reloading files for $instanceId after change")
                    getAllFiles(instanceId, skipCache = true).filter { it is FileListState.Loading }.first()
                }
            }
    }.also { job ->
        job.invokeOnCompletion {
            Napier.i(tag = tag, message = "Stop observing $instanceId for file changes change ($it)")
        }
    }

    fun getAllFiles(instanceId: String, skipCache: Boolean = false): StateFlow<FileListState> {
        val flow = createOrGet(instanceId)
        val current = flow.value

        if ((skipCache && current !is FileListState.Loading) || (current is FileListState.Error && current.exception is UninitializedException)) {
            flow.value = FileListState.Loading
            loadFiles(instanceId)
        }

        return flow.asStateFlow()
    }

    fun getFiles(instanceId: String, path: String? = null, skipCache: Boolean = false) = getAllFiles(instanceId = instanceId, skipCache = skipCache)
        .map {
            when (it) {
                is FileListState.Error -> it
                FileListState.Loading -> it
                is FileListState.Loaded -> {
                    if (path == null) {
                        it
                    } else {
                        it.copy(file = (it.file as FileObject.Folder).findPath(path) ?: throw FileNotFoundException("Unable to find $path for $instanceId"))
                    }
                }
            }
        }.catch {
            Napier.e(tag = tag, message = "Exception while loading files from $instanceId", throwable = it)
            emit(FileListState.Error(it))
        }

    private fun FileObject.findPath(pathToFind: String): FileObject? = this.takeIf { it.path == pathToFind }
        ?: (this as? FileObject.Folder)?.children?.firstOrNull { it.path == pathToFind }
        ?: (this as? FileObject.Folder)?.children?.firstNotNullOfOrNull { it.findPath(pathToFind) }

    private fun loadFiles(instanceId: String) = AppScope.launch {
        Napier.i(tag = tag, message = "Loading files for $instanceId")
        val flow = createOrGet(instanceId)
        try {
            flow.value = FileListState.Loading
            val response = octoPrintProvider.octoPrint(instanceId).filesApi.getAllFiles(FileOrigin.Local)
            flow.value = FileListState.Loaded(
                FileObject.Folder(
                    path = "/",
                    display = "root",
                    children = response.files,
                    size = response.total ?: 0,
                    name = instanceId,
                    origin = FileOrigin.Local,
                    ref = FileObject.Reference(
                        download = null,
                        resource = ""
                    ),
                    type = "",
                    typePath = emptyList()
                )
            )
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Exception while loading files from $instanceId", throwable = e)
            flow.value = FileListState.Error(e)
        }
    }

    private fun createOrGet(instanceId: String) = fileCache.getOrPut(instanceId) { MutableStateFlow(FileListState.Error(UninitializedException())) }

    private class UninitializedException : IllegalStateException()
}
