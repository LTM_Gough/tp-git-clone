package de.crysxd.octoapp.base.exceptions

import io.ktor.utils.io.errors.IOException

class UnknownHostException(message: String) : IOException(message)