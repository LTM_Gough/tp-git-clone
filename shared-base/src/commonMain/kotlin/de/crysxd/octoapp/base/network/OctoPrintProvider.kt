package de.crysxd.octoapp.base.network

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField.ConnectionTimeout
import de.crysxd.octoapp.base.OctoConfigField.ReadWriteTimeout
import de.crysxd.octoapp.base.OctoConfigField.WebSocketPingPongTimeout
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.EventSource
import de.crysxd.octoapp.engine.PrinterEngine
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.LogLevel
import de.crysxd.octoapp.sharedcommon.http.config.Timeouts
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.reflect.KClass
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class OctoPrintProvider(
    private val detectBrokenSetupInterceptorFactory: DetectBrokenSetupInterceptor.Factory,
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPreferences: OctoPreferences,
    private val analytics: OctoAnalytics,
    private val platform: Platform,
    val httpSettings: HttpClientSettings,
) {
    companion object {
        private var counter = 0
    }

    private val tag = "OctoPrintProvider/${counter++}"
    private val octoPrintMutex = Mutex()
    private var octoPrintCache = mutableMapOf<String, Pair<String, PrinterEngine>>()
    private val cachedMessageFlows = mutableMapOf<String, MutableStateFlow<Map<KClass<out Message>, MessageWrapper<*>>>>()
    private val connectEventFlow = mutableMapOf<String, MutableStateFlow<Event.Connected?>>()
    private val activeInstanceId get() = octoPrintRepository.getActiveInstanceSnapshot()?.id

    init {
        // Passively collect data for the analytics profile
        // The passive event flow does not actively open a connection but piggy-backs other Flows
        AppScope.launch { fillPassiveFlows() }
    }

    private val OctoPrintInstanceInformationV3?.cacheKey get() = this?.apiKey + this?.webUrl + this?.alternativeWebUrl

    fun octoPrintFlow(instanceId: String? = null) = octoPrintRepository.instanceInformationFlow(instanceId).distinctUntilChangedBy {
        it.cacheKey
    }.map { instance ->
        octoPrintMutex.withLock {
            val cached = octoPrintCache[instance?.id]

            when {
                // We don't have information for the instance, clean up
                // Use instanceId not instance.id!
                instance == null -> {
                    Napier.d(tag = tag, message = "Instance for ${instance.cacheKey} is null, clearing cached passive flows")
                    instanceId?.let {
                        createConnectEventFlow(it).tryEmit(null)
                        createCacheMessageFlow(it).tryEmit(emptyMap())
                    }
                    null
                }

                // Use instance.id to ensure handover when active changes!
                cached == null || cached.first != instance.cacheKey -> {
                    val octoPrint = createAdHocOctoPrint(instance, brokenSetupHandlingEnabled = true)
                    Napier.d(tag = tag, message = "Created new OctoPrint: ${instance.cacheKey} -> $octoPrint")
                    octoPrintCache[instance.id]?.second?.destroy()
                    octoPrintCache[instance.id] = instance.cacheKey to octoPrint
                    octoPrint
                }

                else -> {
                    Napier.d(tag = tag, message = "Took OctoPrint from cache: ${cached.first} -> ${cached.second}")
                    cached.second
                }
            }
        }
    }

    suspend fun octoPrint(instanceId: String? = null) = octoPrintFlow(instanceId).first() ?: throw SuppressedIllegalStateException("OctoPrint $instanceId not found")

    fun getCurrentConnection(instanceId: String?) = (instanceId ?: activeInstanceId)?.let { createConnectEventFlow(it).value }

    fun getLastCurrentMessage(instanceId: String?) = (instanceId ?: activeInstanceId)?.let { createCacheMessageFlow(it).value[Message.Current::class]?.message }

    fun passiveConnectionEventFlow(tag: String, instanceId: String? = null) = instanceIdFlow(instanceId)
        .flatMapLatest { it?.let { createConnectEventFlow(it) } ?: emptyFlow() }
        .onStart { Napier.i(tag = this@OctoPrintProvider.tag, message = "Started connection event flow for $tag on instance $instanceId") }
        .onCompletion { Napier.i(tag = this@OctoPrintProvider.tag, message = "Completed connection event flow for $tag on instance $instanceId") }
        .handleErrors()

    fun passiveCurrentMessageFlow(tag: String, instanceId: String? = null) =
        passiveCachedMessageFlow(tag = tag, clazz = Message.Current::class, instanceId = instanceId).filterNotNull()

    @Suppress("UNCHECKED_CAST")
    fun <T : Message> passiveCachedMessageFlow(tag: String, clazz: KClass<T>, instanceId: String? = null) = instanceIdFlow(instanceId)
        .flatMapLatest { it?.let { createCacheMessageFlow(it) } ?: emptyFlow() }
        .distinctUntilChangedBy { it[clazz]?.counter }
        .map { it[clazz]?.message as T? }
        .onStart { Napier.i(tag = this@OctoPrintProvider.tag, message = "Started companion message flow for $tag on instance $instanceId") }
        .onCompletion { Napier.i(tag = this@OctoPrintProvider.tag, message = "Completed companion message flow for $tag on instance $instanceId ") }
        .handleErrors()

    fun eventFlow(tag: String, instanceId: String? = null, config: EventSource.Config = EventSource.Config()) = octoPrintFlow(instanceId)
        .flatMapLatest { it?.eventSource?.eventFlow(tag, config) ?: emptyFlow() }
        .catch { e -> Napier.e(tag = this@OctoPrintProvider.tag, message = "Error in OctoPrintProvider", throwable = e) }
        .onStart { Napier.i(tag = this@OctoPrintProvider.tag, message = "Started event flow for $tag on instance $instanceId") }
        .onCompletion { Napier.i(tag = this@OctoPrintProvider.tag, message = "Completed event message flow for $tag on instance $instanceId ") }
        .handleErrors()

    fun passiveEventFlow(instanceId: String? = null) = octoPrintFlow(instanceId)
        .flatMapLatest { it?.eventSource?.passiveEventFlow() ?: emptyFlow() }
        .handleErrors()

    private fun <T> Flow<T>.handleErrors() = retry {
        Napier.e(tag = tag, message = "Error in OctoPrintProvider", throwable = it)
        delay(1000)
        true
    }

    private fun instanceIdFlow(instanceId: String?) =
        (instanceId?.let { flowOf(instanceId) } ?: octoPrintRepository.instanceInformationFlow().map { it?.id }).distinctUntilChanged()

    private fun createConnectEventFlow(instanceId: String) = connectEventFlow.getOrPut(instanceId) { MutableStateFlow(null) }

    private fun createCacheMessageFlow(instanceId: String) = cachedMessageFlows.getOrPut(instanceId) { MutableStateFlow(mutableMapOf()) }

    private fun updateAnalyticsProfileWithEvents(event: Event) {
        when {
            event is Event.MessageReceived && event.message is Message.Event.FirmwareData -> {
                val data = event.message as Message.Event.FirmwareData

                analytics.logEvent(
                    OctoAnalytics.Event.PrinterFirmwareData, mapOf(
                        "firmware_name" to (data.firmwareName ?: "unspecified"),
                        "machine_type" to (data.machineType ?: "unspecified"),
                        "extruder_count" to (data.extruderCount?.toLong() ?: 0)
                    )
                )
                analytics.setUserProperty(OctoAnalytics.UserProperty.PrinterFirmwareName, data.firmwareName)
                analytics.setUserProperty(OctoAnalytics.UserProperty.PrinterMachineType, data.machineType)
                analytics.setUserProperty(OctoAnalytics.UserProperty.PrinterExtruderCount, data.extruderCount.toString())
            }

            event is Event.MessageReceived && event.message is Message.Connected -> {
                OctoAnalytics.logEvent(OctoAnalytics.Event.OctoprintConnected)
                OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.OctoPrintVersion, (event.message as Message.Connected).version)
            }
        }
    }

    private suspend fun fillPassiveFlows() {
        octoPrintRepository.instanceInformationFlow().map {
            // Get all ids, distinct until changed by adding/removing one
            octoPrintRepository.getAll().map { it.id }.sorted()
        }.distinctUntilChanged().flatMapLatest { ids ->
            // Create passive flow for each and combine them
            val flows = ids.map { createFillPassiveFlows(it) }
            combine(flows) { it.toList() }
        }.handleErrors().collect()
    }

    private fun createFillPassiveFlows(instanceId: String) = passiveEventFlow(instanceId).onEach { event ->
        if (instanceId == activeInstanceId) {
            updateAnalyticsProfileWithEvents(event)
        }

        val cacheFlow = createCacheMessageFlow(instanceId)

        (event as? Event.MessageReceived)?.let { messageEvent ->
            // We cache the latest message of any type
            val upgradedMessage = if (messageEvent.message is Message.Current) {
                // If the last message had data the new one is lacking, upgrade the new one so the cached message
                // is always holding all information required
                val message = messageEvent.message as Message.Current
                val last = cacheFlow.value[Message.Current::class]?.message as Message.Current?
                message.copy(
                    temps = message.temps.takeIf { it.isNotEmpty() } ?: last?.temps ?: emptyList(),
                    progress = message.progress ?: last?.progress,
                    state = message.state,
                    job = message.job ?: last?.job,
                )
            } else {
                messageEvent.message
            }

            // We keep an counter in the wrapper so the flow refreshes even when we push the same value twice but still use MutableStateFlow to have `value`
            val value = cacheFlow.value
            val wrapper = MessageWrapper(upgradedMessage, (value[upgradedMessage::class]?.counter ?: -1) + 1)
            cacheFlow.value = value.toMutableMap().apply { put(upgradedMessage::class, wrapper) }
        }

        ((event as? Event.Connected))?.let {
            Napier.i(tag = tag, message = "Connected $instanceId @ ${it.connectionType}")
            createConnectEventFlow(instanceId).value = it
            cacheFlow.value = emptyMap()
        }

        ((event as? Event.Disconnected))?.let {
            Napier.i(tag = tag, message = "Disconnected $instanceId")
            createConnectEventFlow(instanceId).value = null
            it.exception?.let(detectBrokenSetupInterceptorFactory.buildFor(instanceId)::inspect)
        }
    }

    fun createAdHocOctoPrint(instance: OctoPrintInstanceInformationV3, brokenSetupHandlingEnabled: Boolean = false): PrinterEngine {
        return OctoPrintEngineBuilder {
            val longTimeout = 60.seconds.takeIf { octoPreferences.longTimeouts }

            baseUrls = listOfNotNull(
                instance.webUrl.toString(),
                instance.alternativeWebUrl?.toString(),
            )

            allowWebSocketTransport = octoPreferences.websocketTransportAllowed

            httpClientSettings = httpSettings.copy(
                logLevel = when {
                    platform.debugBuild -> LogLevel.Debug
                    octoPreferences.debugNetworkLogging -> LogLevel.Verbose
                    else -> LogLevel.Production
                },
                timeouts = Timeouts(
                    connectionTimeout = longTimeout ?: OctoConfig.getLong(ConnectionTimeout).milliseconds.coerceAtLeast(5.seconds),
                    webSocketPingPongTimeout = longTimeout ?: OctoConfig.getLong(WebSocketPingPongTimeout).milliseconds.coerceAtLeast(5.seconds),
                    requestTimeout = longTimeout ?: OctoConfig.getLong(ReadWriteTimeout).milliseconds.coerceAtLeast(5.seconds),
                )
            )

            interpolateEvents = platform.lowPowerDevice
            apiKey = instance.apiKey

            if (brokenSetupHandlingEnabled) {
                exceptionInspector = detectBrokenSetupInterceptorFactory.buildFor(instance.id)
            }
        }
    }

    private data class MessageWrapper<T : Message>(val message: T, val counter: Int)
}
