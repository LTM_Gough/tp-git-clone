package de.crysxd.octoapp.base.utils

// Can't use <Nothing> because of Swift
sealed class FlowState<T> {
    class Loading<T> : FlowState<T>()
    data class Error<T>(val throwable: Throwable) : FlowState<T>()
    data class Ready<T>(val data: T) : FlowState<T>()
}