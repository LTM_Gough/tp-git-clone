package de.crysxd.octoapp.base.data.models

import de.crysxd.octoapp.engine.models.files.FileObject


sealed class FileListState {
    object Loading : FileListState()
    data class Error(val exception: Throwable) : FileListState()
    data class Loaded(val file: FileObject) : FileListState()
}