package de.crysxd.octoapp.base

import de.crysxd.octoapp.base.utils.texts.getString
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import io.ktor.http.decodeURLQueryComponent
import io.ktor.http.encodeURLQueryComponent
import io.ktor.util.decodeBase64Bytes
import io.ktor.util.encodeBase64
import io.ktor.utils.io.core.toByteArray

object UriLibrary {
    private val tag = "UriLibrary"

    private fun getUri(id: String, vararg placeholder: String) =
        ("http://" + getString(id)).let {
            var out = it
            for (i in placeholder.indices step 2) {
                out = out.replace(placeholder[i], placeholder[i + 1])
            }
            out
        }.toUrl()

    fun isInAppLink(url: Url) = url.host == "app.octoapp.eu"

    fun isInAppLink(urlString: String) = urlString.toUrlOrNull()?.host == "app.octoapp.eu"

    fun getSignInHelpUri(): Url =
        OctoConfig.get(OctoConfigField.SignInHelpUrl).toUrl()

    fun getConfigureRemoteAccessUri(): Url =
        getUri("uri___configure_remote_access")

    fun getHelpUri(): Url =
        getUri("uri___help")

    fun getTimelapseArchiveUri(): Url =
        getUri("uri___timelapse_archive")

    fun getTutorialsUri(): Url =
        getUri("uri___tutorials")

    fun getWebcamTroubleshootingUri(): Url =
        getUri("uri___webcam_troubleshooting")

    fun getFileManagerUri(instanceId: String? = null, path: String? = null, label: String? = null): Url =
        getUri(
            "uri___file_manager",
            "{instanceId}", instanceId ?: "",
            "{path}", secureEncode(path ?: "/"),
            "{label}", secureEncode(label ?: "/")
        )

    fun getTerminalUri(): Url =
        getUri("uri___terminal")

    fun getWebcamUri(): Url =
        getUri("uri___webcam")

    fun getPluginLibraryUri(category: String? = null): Url =
        getUri("uri___plugin_library", "{category}", category ?: "")

    fun getFixOctoPrintConnectionUri(baseUrl: Url, instanceId: String): Url =
        getUri(
            "uri___fix_octoprint_connection",
            "{baseUrl}",
            secureEncode(baseUrl.toString()),
            "{instanceId}",
            instanceId
        )

    fun getFaqUri(faqId: String) =
        getUri("uri___faq", "{faqId}", secureEncode(faqId))

    fun getContactUri(bugReport: Boolean) =
        getUri("uri___contact", "{bugReport}", bugReport.toString())

    fun getPurchaseUri(): Url =
        getUri("uri___purchase")

    fun getLaunchAppUri(): Url =
        getUri("uri___launch_app")

    fun isActiveInstanceRequired(uri: Url) = when (uri.encodedPath) {
        getConfigureRemoteAccessUri().encodedPath -> true
        else -> false
    }

    // When passing URLs as query params, weird shit is happening where Android would "double decode" URLs in params causing
    // problems if the original URL contains encoded parts. To circumvent this we encode as Base64
    fun secureEncode(value: String): String = value.toByteArray().encodeBase64().encodeURLQueryComponent(encodeFull = true)
    fun secureDecode(value: String): String = io.ktor.utils.io.core.String(value.decodeURLQueryComponent().decodeBase64Bytes())
    fun secureDecodeParam(url: Url, name: String, defaultValue: String) = try {
        url.parameters[name]?.let { secureDecode(it) }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed secure decode", throwable = e)
        null
    } ?: defaultValue
}