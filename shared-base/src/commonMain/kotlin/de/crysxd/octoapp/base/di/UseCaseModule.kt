package de.crysxd.octoapp.base.di

import de.crysxd.octoapp.base.usecase.ApplyWebcamTransformationsUseCase
import de.crysxd.octoapp.base.usecase.CancelPrintJobUseCase
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import de.crysxd.octoapp.base.usecase.DiscoverOctoPrintUseCase
import de.crysxd.octoapp.base.usecase.GetActiveHttpUrlUseCase
import de.crysxd.octoapp.base.usecase.GetAppLanguageUseCase
import de.crysxd.octoapp.base.usecase.GetCurrentPrinterProfileUseCase
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.base.usecase.GetWebcamSettingsUseCase
import de.crysxd.octoapp.base.usecase.GetWebcamSnapshotUseCase
import de.crysxd.octoapp.base.usecase.HandleAutomaticLightEventUseCase
import de.crysxd.octoapp.base.usecase.HandleOctoEverywhereAppPortalSuccessUseCase
import de.crysxd.octoapp.base.usecase.HandleSpaghettiDetectiveAppPortalSuccessUseCase
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.usecase.RequestApiAccessUseCase
import de.crysxd.octoapp.base.usecase.SetAlternativeWebUrlUseCase
import de.crysxd.octoapp.base.usecase.SetTargetTemperaturesUseCase
import de.crysxd.octoapp.base.usecase.SetTemperatureOffsetUseCase
import de.crysxd.octoapp.base.usecase.StartPrintJobUseCase
import de.crysxd.octoapp.base.usecase.TestFullNetworkStackUseCase
import de.crysxd.octoapp.base.usecase.TogglePausePrintJobUseCase
import de.crysxd.octoapp.base.usecase.UpdateInstanceCapabilitiesUseCase
import de.crysxd.octoapp.base.usecase.UpdateNgrokTunnelUseCase
import de.crysxd.octoapp.sharedcommon.di.BaseModule
import org.koin.dsl.module

class UseCaseModule : BaseModule {

    override val koinModule = module {
        factory { GetActiveHttpUrlUseCase(get(), get()) }
        factory { GetWebcamSettingsUseCase(get(), get(), get(), get()) }
        factory { ApplyWebcamTransformationsUseCase() }
        factory { GetPowerDevicesUseCase(get(), get()) }
        factory { HandleAutomaticLightEventUseCase(get(), get()) }
        factory { TestFullNetworkStackUseCase(get(), get(), get(), get(), get(), get()) }
        factory { DiscoverOctoPrintUseCase(get(), get(), get()) }
        factory { RequestApiAccessUseCase(get()) }
        factory { GetAppLanguageUseCase(get(), get()) }
        factory { UpdateInstanceCapabilitiesUseCase(get(), get(), get(), get(), get()) }
        factory { GetWebcamSnapshotUseCase(get(), get(), get(), get(), get(), get(), get()) }
        factory { TogglePausePrintJobUseCase(get()) }
        factory { CancelPrintJobUseCase(get(), get()) }
        factory { SetTargetTemperaturesUseCase(get(), get()) }
        factory { SetTemperatureOffsetUseCase(get(), get()) }
        factory { GetCurrentPrinterProfileUseCase(get()) }
        factory { LoadFilesUseCase(get(), get()) }
        factory { StartPrintJobUseCase(get(), get(), get()) }
        factory { CreateBugReportUseCase(get(), get(), get(), get()) }
        factory { GetRemoteServiceConnectUrlUseCase(get()) }
        factory { HandleOctoEverywhereAppPortalSuccessUseCase(get()) }
        factory { HandleSpaghettiDetectiveAppPortalSuccessUseCase(get()) }
        factory { SetAlternativeWebUrlUseCase(get(), get()) }
        factory { UpdateNgrokTunnelUseCase(get(), get()) }
    }
}