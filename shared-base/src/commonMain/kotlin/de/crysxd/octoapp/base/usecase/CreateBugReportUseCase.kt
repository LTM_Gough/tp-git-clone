package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.logging.CachedAntiLog
import de.crysxd.octoapp.base.logging.SensitiveDataMask
import de.crysxd.octoapp.sharedcommon.JavaSerializable
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class CreateBugReportUseCase(
    private val printerConfigRepository: OctoPrintRepository,
    private val getAppLanguageUseCase: GetAppLanguageUseCase,
    private val octoPreferences: OctoPreferences,
    private val platform: Platform,
) : UseCase2<CreateBugReportUseCase.Params, CreateBugReportUseCase.BugReport>() {

    @Suppress("BlockingMethodInNonBlockingContext")
    override suspend fun doExecute(param: Params, logger: Logger) = withContext(Dispatchers.SharedIO) {
        val files = mutableListOf<BugReport.File>()

        if (param.sendLogs) {
            files += createLogFile()
        }

        if (param.sendPhoneInfo) {
            files += createPhoneInfoFile()
            files += createSettingsFile()
        }

        if (param.sendOctoPrintInfo) {
            files += createOctoPrintFiles()
        }

        createBillingFile()?.let { files += it }

        BugReport(
            files = files,
            appVersion = platform.appVersion,
            platformName = platform.platformName,
        )
    }

    private fun createLogFile(): BugReport.File = BugReport.File(
        bytes = CachedAntiLog.getCache().encodeToByteArray(),
        name = "logs.log",
    )

    private suspend fun createPhoneInfoFile(): BugReport.File {
        val text = StringBuilder()
        val appLanguage = getAppLanguageUseCase.execute(Unit).appLanguage

        text.appendLine("device_model = ${platform.deviceModel}")
        text.appendLine("device_name = ${platform.deviceName}")
        text.appendLine("device_language = ${platform.deviceLanguage}")
        text.appendLine("platform_name = ${platform.platformName}")
        text.appendLine("platform_version = ${platform.platformVersion}")
        text.appendLine("app_version = ${platform.appVersion}")
        text.appendLine("app_build = ${platform.appBuild}")
        text.appendLine("app_language = $appLanguage")

        return BugReport.File(
            bytes = text.toString().encodeToByteArray(),
            name = "device.txt",
        )
    }

    private fun createBillingFile(): BugReport.File? {
        val purchases = BillingManager.getPurchases().takeIf { it.isNotEmpty() } ?: return null

        return BugReport.File(
            bytes = purchases.joinToString("\n").encodeToByteArray(),
            name = "billing.txt",
        )
    }

    private fun createSettingsFile(): BugReport.File {
        val text = StringBuilder()
        val settings = octoPreferences.settings

        fun <T> save(block: () -> T?) = try {
            block()
        } catch (e: Exception) {
            null
        }

        fun getAnyValue(key: String) = try {
            save { settings.getStringOrNull(key) }
                ?: save { settings.getBooleanOrNull(key) }
                ?: save { settings.getIntOrNull(key) }
                ?: save { settings.getLongOrNull(key) }
                ?: save { settings.getDoubleOrNull(key) }
                ?: save { settings.getFloatOrNull(key) }
        } catch (e: Exception) {
            "ERROR"
        }

        settings.keys.forEach {
            text.appendLine("$it = ${getAnyValue(it)}")
        }

        return BugReport.File(
            bytes = text.toString().encodeToByteArray(),
            name = "settings.txt",
        )
    }

    private suspend fun createOctoPrintFiles(): List<BugReport.File> {
        val activeId = printerConfigRepository.getActiveInstanceSnapshot()?.id
        return printerConfigRepository.getAll().map {
            BugReport.File(
                bytes = try {
                    SensitiveDataMask.mask(Json.encodeToString(it))
                } catch (e: Exception) {
                    SensitiveDataMask.mask(e.stackTraceToString())
                }.encodeToByteArray(),
                name = "${if (activeId == it.id) "active_" else ""}${it.id}.json",
            )
        }
    }

    data class Params(
        val sendPhoneInfo: Boolean = true,
        val sendLogs: Boolean = true,
        val sendOctoPrintInfo: Boolean = true,
    )

    @Serializable
    data class BugReport(
        val files: List<File>,
        val appVersion: String,
        val platformName: String,
    ) : JavaSerializable {
        @Serializable
        class File(
            val bytes: ByteArray,
            val name: String,
        ) : JavaSerializable
    }
}