package de.crysxd.octoapp.base.billing

import kotlinx.coroutines.flow.Flow

expect object BillingManager {

    fun isFeatureEnabled(feature: String): Boolean
    fun isFeatureEnabledFlow(feature: String): Flow<Boolean>
    fun shouldAdvertisePremium(): Boolean
    fun onResume()
    fun onPause()
    fun getPurchases(): Set<String>
}

const val FEATURE_AUTOMATIC_LIGHTS = "auto_lights"
const val FEATURE_QUICK_SWITCH = "quick_switch"
const val FEATURE_GCODE_PREVIEW = "gcode_preview"
const val FEATURE_HLS_WEBCAM = "hls_webcam"
const val FEATURE_INFINITE_WIDGETS = "infinite_app_widgets"
const val FEATURE_FULL_WEBCAM_RESOLUTION = "full_webcam_resolution"
const val FEATURE_FILE_MANAGEMENT = "file_management"
const val FEATURE_WEAR_OS_APP = "wear_os_app"