package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.utils.texts.getString
import de.crysxd.octoapp.engine.framework.isOctoEverywhereUrl
import de.crysxd.octoapp.engine.framework.isSharedOctoEverywhereUrl
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.withBasicAuth

class SetAlternativeWebUrlUseCase(
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
) : UseCase2<SetAlternativeWebUrlUseCase.Params, SetAlternativeWebUrlUseCase.Result>() {

    override suspend fun doExecute(param: Params, logger: Logger): Result {
        val octoPrint = octoPrintProvider.octoPrint()

        val url = if (param.webUrl.isNotEmpty()) {
            try {
                param.webUrl.toUrl().withBasicAuth(user = param.username, password = param.password)
            } catch (e: Exception) {
                logger.w(e)
                OctoAnalytics.logEvent(OctoAnalytics.Event.RemoteConfigManuallySetFailed)
                return Result.Failure(getString("configure_remote_acces___manual___error_invalid_url"), e)
            }
        } else {
            null
        }

        if (!param.bypassChecks && url != null) {
            val isOe = url.isOctoEverywhereUrl()
            val isShared = url.isSharedOctoEverywhereUrl()
            when {
                isOe && !isShared -> return Result.Failure(
                    errorMessage = getString("configure_remote_acces___manual___error_normal_octoeverywhere_url"),
                    allowToProceed = false,
                    exception = InvalidAlternativeUrlException("Given URL is a standard OctoEverywhere URL")
                )
                isOe && isShared -> return Result.Failure(
                    errorMessage = getString("configure_remote_acces___manual___error_shared_octoeverywhere_url"),
                    allowToProceed = true,
                    exception = InvalidAlternativeUrlException("Given URL is a shared OctoEverywhere URL")
                )
            }

            val settings = try {
                val config = octoPrintRepository.get(param.instanceId) ?: throw IllegalStateException("Didn't find config for ${param.instanceId}")
                val octoprint = octoPrintProvider.createAdHocOctoPrint(config.copy(webUrl = url))
                octoprint.settingsApi.getSettings()
            } catch (e: Exception) {
                logger.e(e)
                OctoAnalytics.logEvent(OctoAnalytics.Event.RemoteConfigManuallySetFailed)
                return Result.Failure(getString("configure_remote_acces___manual___error_unable_to_connect"), e)
            }

            try {
                val remoteUuid = settings.plugins.discovery?.uuid
                val localUuid = octoPrint.settingsApi.getSettings().plugins.discovery?.uuid

                if (localUuid != remoteUuid) {
                    throw InvalidAlternativeUrlException("Upnp UUIDs for primary and alternate URLs differ: $localUuid <--> $remoteUuid")
                }
            } catch (e: Exception) {
                OctoAnalytics.logEvent(OctoAnalytics.Event.RemoteConfigManuallySetFailed)
                return Result.Failure(getString("configure_remote_acces___manual___error_unable_to_verify"), e, allowToProceed = true)
            }
        }

        octoPrintRepository.update(param.instanceId) {
            it.copy(alternativeWebUrl = url, octoEverywhereConnection = null, remoteConnectionFailure = null)
        }

        OctoAnalytics.logEvent(OctoAnalytics.Event.RemoteConfigManuallySet)
        OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.RemoteAccess, "manual")
        return Result.Success
    }

    data class Params(
        val instanceId: String,
        val webUrl: String,
        val username: String,
        val password: String,
        val bypassChecks: Boolean
    )

    sealed class Result {
        object Success : Result()
        data class Failure(
            val errorMessage: String,
            val exception: Exception,
            val allowToProceed: Boolean = false
        ) : Result()
    }

    class InvalidAlternativeUrlException(message: String) : SuppressedIllegalStateException(message)
}