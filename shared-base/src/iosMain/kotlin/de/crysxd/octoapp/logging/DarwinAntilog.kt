package de.crysxd.octoapp.logging

import io.github.aakira.napier.Antilog
import io.github.aakira.napier.LogLevel
import io.github.aakira.napier.LogLevel.ASSERT
import io.github.aakira.napier.LogLevel.DEBUG
import io.github.aakira.napier.LogLevel.ERROR
import io.github.aakira.napier.LogLevel.INFO
import io.github.aakira.napier.LogLevel.VERBOSE
import io.github.aakira.napier.LogLevel.WARNING
import kotlinx.datetime.Clock
import kotlinx.datetime.toNSDate
import platform.Foundation.NSDateFormatter
import platform.Foundation.NSLocale
import platform.Foundation.NSLock
import platform.Foundation.NSTimeZone
import platform.Foundation.autoupdatingCurrentLocale
import platform.Foundation.localTimeZone

object DarwinAntilog : Antilog() {
    private val tagLength = 28
    private val buffer = " ".repeat(tagLength)
    private val lock = NSLock()

    private val dateFormatter = NSDateFormatter().apply {
        timeZone = NSTimeZone.localTimeZone
        locale = NSLocale.autoupdatingCurrentLocale
        dateFormat = "HH:mm:ss.SSS"
    }

    override fun performLog(priority: LogLevel, tag: String?, throwable: Throwable?, message: String?) {
        try {
            lock.lock()

            val time = dateFormatter.stringFromDate(Clock.System.now().toNSDate())
            val fixedTag = ((tag ?: "") + buffer).take(tagLength)
            val level = when (priority) {
                VERBOSE -> return //"🤍"
                DEBUG -> "💙"
                INFO -> "💚"
                WARNING -> "💛"
                ERROR -> "🧡"
                ASSERT -> "❤️"
            }
            val prefix = "$time $fixedTag $level "
            message?.split("\n")?.flatMap { it.chunked(128) }?.forEach { s ->
                println("📱 $prefix$s")
            }
            throwable?.stackTraceToString()?.split("\n")?.let { s ->
                println("📱 $prefix$s")
            }
        } finally {
            lock.unlock()
        }
    }
}