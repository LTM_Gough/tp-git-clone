package de.crysxd.octoapp.base.io

import com.russhwolf.settings.NSUserDefaultsSettings
import com.russhwolf.settings.Settings
import de.crysxd.octoapp.sharedcommon.Platform
import platform.Foundation.NSUserDefaults

actual class SettingsStore actual constructor(private val platform: Platform) {
    actual fun forNameSpace(nameSpace: String): Settings = NSUserDefaultsSettings(
        NSUserDefaults(suiteName = "${platform.appId}.$nameSpace")
    )
}