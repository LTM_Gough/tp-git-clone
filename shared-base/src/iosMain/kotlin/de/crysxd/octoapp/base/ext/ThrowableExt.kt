package de.crysxd.octoapp.base.ext

import de.crysxd.octoapp.base.utils.texts.getString
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.ProxyException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import io.ktor.client.engine.darwin.DarwinHttpRequestException

actual fun Throwable.composeErrorMessage(): CharSequence = when (this) {
    is ProxyException -> original.composeErrorMessage()
    is UserMessageException -> userMessage
    is NetworkException -> userFacingMessage ?: message ?: technicalMessage
    is DarwinHttpRequestException -> origin.localizedDescription
    else -> getString("error_general")
}


actual fun Throwable.composeTechnicalErrorMessage(): CharSequence = when (this) {
    is ProxyException -> original.composeTechnicalErrorMessage()
    is NetworkException -> when (val oc = originalCause) {
        // Prefer iOS's error message in case it's a DarwinHttpRequestException
        is DarwinHttpRequestException -> oc.composeTechnicalErrorMessage()
        else -> technicalMessage
    }
    is DarwinHttpRequestException -> origin.localizedDescription
    else -> message ?: ""
}