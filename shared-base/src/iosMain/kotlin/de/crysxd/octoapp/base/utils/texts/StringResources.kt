package de.crysxd.octoapp.base.utils.texts

import de.crysxd.octoapp.sharedcommon.ext.format
import platform.Foundation.NSBundle

actual fun getString(id: String, vararg formatArgs: Any): String {
    fun String.takeIfSuccess() = takeUnless { it == id || it.isEmpty() }
    val format = NSBundle.mainBundle.localizedStringForKey(id, id, null).takeIfSuccess()
        ?: NSBundle.mainBundle.localizedStringForKey(id, id, "EnglishOnly").takeIfSuccess()
        ?: NSBundle.mainBundle.localizedStringForKey(id, id, "Uris").takeIfSuccess()

    return format
        ?.format(*formatArgs)
        ?.takeIfSuccess()
        ?.parseHtml()
        ?.toString()
        ?: id
}

private val htmlLinkRegex by lazy { Regex("<a.*?href=[\"']([^\"']*)[\"'][^>]*>([^<]*)</a>") }

actual fun CharSequence.parseHtml(): CharSequence = this.toString()
    .replace("<b>", "**")
    .replace("</b>", "**")
    .replace("<small>", "")
    .replace("</small>", "")
    .replace("<br>", "\n")
    .replace("<![CDATA[", "\n")
    .replace("]]>", "\n")
    .replace("&amp;", "&")
    .let {
        if (it.contains("</a>")) {
            it.replace(htmlLinkRegex, "[$2]($1)")
        } else {
            it
        }
    }
