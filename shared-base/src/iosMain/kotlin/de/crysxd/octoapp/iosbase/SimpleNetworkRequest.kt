package de.crysxd.octoapp.iosbase

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.framework.urlFromGivenValue
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.utils.io.core.Input
import io.ktor.utils.io.core.readBytes
import kotlinx.cinterop.allocArrayOf
import kotlinx.cinterop.memScoped
import platform.Foundation.NSData
import platform.Foundation.create

@Throws(Throwable::class)
suspend fun simpleNetworkRequest(instanceId: String, path: String): NSData {
    val printer = SharedBaseInjector.get().octoPrintProvider.octoPrint(instanceId = instanceId)
    return printer.genericRequest { url, httpClient ->
        httpClient.get {
            urlFromGivenValue(baseUrl = url, givenValue = path)
        }.body<Input>().readBytes().toNsData()
    }
}

fun ByteArray.toNsData(): NSData = memScoped {
    NSData.create(bytes = allocArrayOf(this@toNsData), length = this@toNsData.size.toULong())
}