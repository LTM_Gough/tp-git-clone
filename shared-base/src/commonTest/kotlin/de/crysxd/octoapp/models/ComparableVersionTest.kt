package de.crysxd.octoapp.models

import de.crysxd.octoapp.sharedcommon.utils.asVersion
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ComparableVersionTest {

    @Test
    fun WHEN_same_version_is_compared_THEN_result_is_correct() {
        assertFalse("1.0".asVersion() > "1.0".asVersion())
        assertFalse("1.0".asVersion() < "1.0".asVersion())

        assertFalse("1.0.0".asVersion() > "1.0.0".asVersion())
        assertFalse("1.0.0".asVersion() < "1.0.0".asVersion())

        assertFalse("1".asVersion() > "1".asVersion())
        assertFalse("1".asVersion() < "1".asVersion())
    }

    @Test
    fun WHEN_larger_version_is_compared_THEN_result_is_correct() {
        assertTrue("1.1".asVersion() > "1.0".asVersion())
        assertFalse("1.1".asVersion() < "1.0".asVersion())

        assertTrue("1.0.1".asVersion() > "1.0.0".asVersion())
        assertFalse("1.0.1".asVersion() < "1.0.0".asVersion())

        assertTrue("2".asVersion() > "1.0.4".asVersion())
        assertFalse("2".asVersion() < "1.0.4".asVersion())

        assertTrue("2.036".asVersion() > "1.0.4".asVersion())
        assertFalse("2.036".asVersion() < "1.0.4".asVersion())

        assertTrue("1".asVersion() > "0".asVersion())
        assertFalse("1".asVersion() < "0".asVersion())
    }

    @Test
    fun WHEN_beta_version_is_compared_THEN_result_is_correct() {
        assertFalse("1.1".asVersion() > "1.1-beta09".asVersion())
        assertTrue("1.1".asVersion() < "1.1-beta09".asVersion())

        assertFalse("1.0.1".asVersion() > "1.0.beta09".asVersion())
        assertTrue("1.0.1".asVersion() < "1.0.beta09".asVersion())

        assertFalse("1".asVersion() > "1-beta09".asVersion())
        assertFalse("1".asVersion() < "0-beta09".asVersion())
    }

    @Test
    fun WHEN_beta_version_is_compared_to_alpha_THEN_result_is_correct() {
        assertFalse("1.1-alpha09".asVersion() > "1.1-beta09".asVersion())
        assertTrue("1.1-alpha09".asVersion() < "1.1-beta09".asVersion())

        assertFalse("1.0.1-alpha09".asVersion() > "1.0.beta09".asVersion())
        assertTrue("1.0.1-alpha09".asVersion() < "1.0.beta09".asVersion())

        assertFalse("1-alpha09".asVersion() > "1-beta09".asVersion())
        assertTrue("1-alpha09".asVersion() < "1-beta09".asVersion())
    }
}