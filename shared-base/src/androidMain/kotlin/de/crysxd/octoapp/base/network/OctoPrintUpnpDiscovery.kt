package de.crysxd.octoapp.base.network

import android.content.Context
import android.net.wifi.WifiManager
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.utils.closeQuietly
import de.crysxd.octoapp.sharedcommon.http.framework.UPNP_ADDRESS_PREFIX
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.regex.Pattern

class OctoPrintUpnpDiscovery(
    context: Context,
    tagSuffix: String? = null,
) {
    companion object {
        private const val DISCOVER_TIMEOUT = 3000L
        private const val SOCKET_TIMEOUT = 500
        private const val PORT = 1900
        private const val ADDRESS = "239.255.255.250"
        private const val LINE_END = "\r\n"
        private const val QUERY = "M-SEARCH * HTTP/1.1" + LINE_END +
                "HOST: 239.255.255.250:1900" + LINE_END +
                "MAN: \"ssdp:discover\"" + LINE_END +
                "MX: 1" + LINE_END +
                "ST: ssdp:all" + LINE_END +
                LINE_END
    }

    private val tag = listOfNotNull("OctoPrintUpnpDiscovery", tagSuffix).joinToString("/")
    private val wifi = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
    private val uuidPattern = Pattern.compile("[uU][sS][nN]:.*[uU][uU][iI][dD]:([\\-0-9a-zA-Z]{36})")

    suspend fun discover(targetHost: String? = null, callback: (Service) -> Unit) = withContext(Dispatchers.IO) {
        val lock = wifi.createMulticastLock("OctoPrintUpnpDiscovery")

        try {
            lock.acquire()
            discoverWithMulticastLock(targetHost, callback)
        } finally {
            lock.release()
        }
    }

    private suspend fun discoverWithMulticastLock(targetHost: String?, callback: (Service) -> Unit) = withContext(Dispatchers.IO) {
        Napier.i(tag = tag, message = "Opening port $PORT for UPnP, searching for ${DISCOVER_TIMEOUT}ms")
        val start = System.currentTimeMillis()
        DatagramSocket(PORT).use { socket ->
            val job = SupervisorJob()
            try {
                val cancelJob = async(job) {
                    delay(DISCOVER_TIMEOUT)
                    Napier.w(tag = tag, message = "Force closing socket")
                    socket.closeQuietly()
                }

                val discoverJob = async(job) {
                    withContext(Dispatchers.IO) {
                        socket.reuseAddress = true
                        val group = InetAddress.getByName(ADDRESS)
                        val queryBytes = QUERY.toByteArray()
                        val datagramPacketRequest = DatagramPacket(queryBytes, queryBytes.size, group, PORT)
                        socket.soTimeout = SOCKET_TIMEOUT
                        socket.send(datagramPacketRequest)
                        while (!socket.isClosed) {
                            val host = readNextResponse(socket, callback)

                            // We discovered our target
                            if (targetHost != null && targetHost == host) {
                                Napier.i(tag = tag, message = "Discovered target, stopping")
                                socket.closeQuietly()
                                break
                            } else {
                                Napier.v(tag = tag, message = "Not target: $host <--> $targetHost")
                            }
                        }
                    }
                }

                discoverJob.await()
                cancelJob.cancelAndJoin()
            } finally {
                job.cancel()
                Napier.i(tag = tag, message = "Closing port $PORT for UPnP after ${System.currentTimeMillis() - start}ms")
            }
        }
    }

    private fun readNextResponse(socket: DatagramSocket, callback: (Service) -> Unit): String? {
        try {
            val datagramPacket = DatagramPacket(ByteArray(1024), 1024)
            socket.receive(datagramPacket)
            val response = String(datagramPacket.data, 0, datagramPacket.length)
            val isOk = response.uppercase().startsWith("HTTP/1.1 200")
            val uuidMatcher = uuidPattern.matcher(response)
            if (isOk) {
                val uuid = if (uuidMatcher.find()) {
                    uuidMatcher.group(1)
                } else {
                    Napier.v(tag = tag, message = "No uuid in:\n$response")
                    return null
                }
                Napier.v(tag = tag, message = "Discovered: $uuid")
                val device = Service(
                    upnpHostname = "$UPNP_ADDRESS_PREFIX$uuid".lowercase(),
                    address = datagramPacket.address,
                    upnpId = uuid
                )

                SharedBaseInjector.get().dnsResolver.addCacheEntry(
                    CachedDns.Entry(
                        hostname = device.upnpHostname,
                        resolvedIpString = listOfNotNull(device.address.hostAddress),
                    )
                )
                callback(device)
                return device.upnpHostname
            }
        } catch (e: SocketTimeoutException) {
            // Expected
        } catch (e: CancellationException) {
            // Expected
        } catch (e: SocketException) {
            // Socket closed
        } catch (e: Exception) {
            Napier.e(tag = tag, throwable = e, message = "Failure in UPnP discovery")
        }

        return null
    }

    data class Service(
        val upnpHostname: String,
        val address: InetAddress,
        val upnpId: String,
    )
}