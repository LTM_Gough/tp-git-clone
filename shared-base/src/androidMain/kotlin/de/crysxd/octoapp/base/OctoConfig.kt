package de.crysxd.octoapp.base

import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig.DEFAULT_VALUE_FOR_LONG
import com.google.firebase.remoteconfig.FirebaseRemoteConfig.DEFAULT_VALUE_FOR_STRING
import com.google.firebase.remoteconfig.ktx.remoteConfig
import io.github.aakira.napier.Napier

actual object OctoConfig {
    actual fun getLong(field: OctoConfigField<Long>): Long = try {
        Firebase.remoteConfig.getLong(field.key)
            .takeUnless { it == DEFAULT_VALUE_FOR_LONG }
            ?: field.default
    } catch (e: IllegalStateException) {
        Napier.w(tag = "OctoConfig", message = "Firebase not initialized, using defaults")
        field.default
    }

    actual fun get(field: OctoConfigField<String>): String = try {
        Firebase.remoteConfig.getString(field.key)
            .takeUnless { it == DEFAULT_VALUE_FOR_STRING }
            ?: field.default
    } catch (e: IllegalStateException) {
        Napier.w(tag = "OctoConfig", message = "Firebase not initialized, using defaults")
        field.default
    }

    actual fun getBoolean(field: OctoConfigField<Boolean>): Boolean = try {
        Firebase.remoteConfig.getBoolean(field.key)
    } catch (e: IllegalStateException) {
        Napier.w(tag = "OctoConfig", message = "Firebase not initialized, using defaults")
        false
    }
}