package de.crysxd.octoapp.base.network

import android.content.Context
import android.net.wifi.WifiManager
import android.os.Looper
import com.qiniu.android.dns.DnsManager
import com.qiniu.android.dns.NetworkInfo
import com.qiniu.android.dns.local.Resolver
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.io.SettingsJson
import de.crysxd.octoapp.base.io.getSerializableOrNull
import de.crysxd.octoapp.base.io.putSerializable
import de.crysxd.octoapp.base.network.dnssd.DnsSd
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.measureTime
import de.crysxd.octoapp.sharedcommon.http.framework.UPNP_ADDRESS_PREFIX
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeoutOrNull
import kotlinx.datetime.Clock
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import java.io.IOException
import java.net.Inet4Address
import java.net.InetAddress
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.time.Duration.Companion.seconds

class CachedLocalDnsResolver(
    private val context: Context,
    private val dnsSd: DnsSd,
) : CachedDns {

    companion object {
        private val cache = mutableMapOf<String, CachedDns.Entry>()
        private val cacheEntryTtl = 300.seconds
        private const val RESOLVE_TIMEOUT = 3L
        private val resolveLock = ReentrantLock()

        private var lastPersistedHash: Int? = null
        private var lookUpCounter = 0
    }

    private val tag = "CachedLocalDnsResolver"
    private val wifi = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
    private val mutableCacheUpdateFlow = MutableSharedFlow<Unit>(replay = 1)
    private val octoPreferences by lazy { SharedBaseInjector.get().preferences }
    val cacheUpdateFlow = mutableCacheUpdateFlow.asSharedFlow()
    private val settings by lazy { SharedBaseInjector.get().settings.forNameSpace("dns-cache") }
    private val settingsKey = "cache"
    private var didLoadCache = false

    private fun persistCache() = resolveLock.withDebugLock("presist-cache") {
        try {
            if (cache.hashCode() == lastPersistedHash) {
                Napier.v(tag = tag, message = "Skip persisting cache, no changed")
            } else {
                settings.putSerializable(settingsKey, cache)
                lastPersistedHash = cache.hashCode()
                mutableCacheUpdateFlow.tryEmit(Unit)
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed to store cache", throwable = e)
        }
    }

    private fun loadCache() = resolveLock.withDebugLock("load-cache") {
        try {
            val all = settings.getSerializableOrNull<Map<String, CachedDns.Entry>>(settingsKey) ?: emptyMap()
            cache.clear()
            cache.putAll(all)
            Napier.d("Read from cache: ${all.values.joinToString { e -> "${e.hostname} -> ${e.resolvedIp}" }}")
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed to load cache", throwable = e)
        }
    }

    override fun lookup(hostname: String): List<InetAddress> {
        if (!didLoadCache) {
            loadCache()
        }

        if (hostname.startsWith(UPNP_ADDRESS_PREFIX)) {
            return localLookup(hostname)
        }

        val enforceIpv4 = octoPreferences.enforceIPv4

        return try {
            // Use default Android DNS system, let's give it a try
            InetAddress.getAllByName(hostname).toList()
        } catch (e: UnknownHostException) {
            // Up to us now....
            Napier.v("Android failed to resolve $hostname, falling back")
            localLookup(hostname)
        }.filter {
            it is Inet4Address || !enforceIpv4
        }
    }

    private fun localLookup(hostname: String) = resolveLock.withDebugLock("local-lookup") {
        // Check cache
        getFromCache(hostname)?.let {
            return@withDebugLock it.resolvedIp
        }
        Napier.v("Cache miss for $hostname")
        forceLocalLookup(hostname)
    }

    private fun forceLocalLookup(hostname: String) = resolveLock.withLock {
        // Resolve. We do this in a coroutine scope and execute the resolve async so we can limit the time effectively
        val result = when {
            hostname.startsWith(UPNP_ADDRESS_PREFIX) -> measureTime("local_upnp_dns_lookup") {
                doUpnpLookup(hostname)
            }

            hostname.endsWith(".local") || hostname.endsWith(".home") -> measureTime("local_mdns_lookup") {
                doMDnsLookup(hostname)
            }

            else -> measureTime("local_dns_lookup") {
                doDnsLookup(hostname)
            }
        }
        Napier.i(tag = tag, message = "Resolved $hostname -> $hostname")

        // Add to cache
        addCacheEntry(
            CachedDns.Entry(
                hostname = hostname,
                resolvedIpString = result.mapNotNull { it.hostAddress },
                validUntil = nextValidUntil()
            )
        )

        result
    }

    private fun doUpnpLookup(upnpHostname: String): List<InetAddress> = runBlocking {
        Napier.i(tag = tag, message = "Resolving via UPnP: $upnpHostname")
        var result: List<InetAddress>? = null

        // Resolve async
        try {
            OctoPrintUpnpDiscovery(context, "LocalDns/${lookUpCounter++}").discover(targetHost = upnpHostname) {
                if (it.upnpHostname == upnpHostname) {
                    OctoAnalytics.logEvent(OctoAnalytics.Event.UpnpDnsResolveSuccess)
                    result = listOf(it.address)
                }
            }
        } catch (e: Exception) {
            if (e is IOException && e.message?.contains("EPERM") == true) {
                Napier.w("UPnP search failed with exception because we are on mobile network (${e::class.qualifiedName}: ${e.message})")
            } else {
                Napier.e(throwable = e, tag = tag, message = "UPnP resolve failed with exception")
            }
        }

        return@runBlocking result ?: throw UnknownHostException(upnpHostname)
    }

    private fun getFromCache(hostname: String) = measureTime("cache_check") {
        cache[hostname.lowercase()]?.also {
            // We have a cache entry that's no longer valid? Try to refresh!
            if (Clock.System.now() > it.validUntil) {
                Napier.d("$hostname needs to be revalidated")
                AppScope.launch(Dispatchers.IO) {
                    try {
                        // We have a match but it's quite old. Let's validate!
                        if (it.resolvedIp.any { ip -> ip.isReachable(200) }) {
                            // IP is still valid, renew
                            Napier.d("Performed ping to ${it.resolvedIp}, $hostname is still valid")
                            addCacheEntry(it.copy(validUntil = nextValidUntil()))
                        } else {
                            // Not reachable, we need to search
                            Napier.d("Performed ping to ${it.resolvedIp}, $hostname needs to be refreshed")
                            forceLocalLookup(hostname)
                        }
                    } catch (e: Exception) {
                        Napier.e(throwable = e, tag = tag, message = "Failure while refreshing DNS entry")
                    }
                }
            }
        }
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    private fun doMDnsLookup(hostname: String): List<InetAddress> = runBlocking {
        Napier.i("Resolving via mDns: $hostname")

        // Sometimes the internal Dnssd service is not running...we can start it with this:
        context.applicationContext.getSystemService(Context.NSD_SERVICE)

        withTimeoutOrNull(TimeUnit.SECONDS.toMillis(RESOLVE_TIMEOUT)) {
            val result = MutableStateFlow<Any?>(null)
            val supervisor = SupervisorJob()
            var operation: DnsSd.Operation? = null
            val serviceData = dnsSd.createServiceData(hostname)
            if (serviceData == null) {
                Napier.w(tag = tag, message = "$dnsSd cannot resolve from hostname, unable to resolve $hostname")
                throw UnknownHostException(hostname)
            }

            try {
                // Switch to a new thread so we can kill it on timeout without having issues with blocking IO operations
                AppScope.launch(Dispatchers.IO + supervisor) {
                    operation = dnsSd.resolve(
                        serviceData,
                        failure = { result.value = it },
                        resolved = { result.value = it.host }
                    )
                }

                val address = when (val res = result.filterNotNull().first()) {
                    is Throwable -> throw res
                    is InetAddress -> res
                    else -> throw Exception("Unexpected result $res")
                }

                OctoAnalytics.logEvent(OctoAnalytics.Event.MDnsResolveSuccess)
                listOf(address)
            } finally {
                // Ensure job gets cancelled
                supervisor.cancel()
                operation?.stop()
            }
        } ?: throw UnknownHostException(hostname)
    }

    private fun doDnsLookup(hostname: String): List<InetAddress> {
        Napier.i("Resolving via local DNS: $hostname")

        // This is a manual backup DNS which should help with .home domains. Some Android devices are configured
        // to ignore the router as DNS server and directly go to Cloudflare or Google
        //
        // From the WiFi manager, get the gateway and DHCP server address, also replace last octet of own ip address with 1
        // Both usually is the router
        // For good measure, also add common router IPs in case the user has a double DHCP issue
        val dnsIps = listOf(
            wifi.dhcpInfo.gateway.asIpString(),
            wifi.dhcpInfo.serverAddress.asIpString(),
            wifi.dhcpInfo.ipAddress.asIpString().split(".").toMutableList().also { it[3] = "1" }.joinToString("."),
            "192.168.0.1",
            "192.168.1.1",
            "192.168.2.1",
        ).distinct()

        Napier.i("Using as DNS server: $dnsIps")

        // Add all DNS server from above
        val resolvers = dnsIps.map { Resolver(InetAddress.getByName(it), RESOLVE_TIMEOUT.toInt()) }.toTypedArray()

        // Resolve!
        val res = try {
            val dns = DnsManager(NetworkInfo.normal, resolvers)
            dns.queryErrorHandler = DnsManager.QueryErrorHandler { e, host -> Napier.w("Unable to resolve host $host (${e::class.java.simpleName}: ${e.message})") }
            dns.query(hostname).map { InetAddress.getByName(it) }
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Unable to resolve host $hostname", throwable = e)
            throw UnknownHostException(hostname)
        }

        if (res.isEmpty()) {
            Napier.w("No results for $hostname")
            throw UnknownHostException(hostname)
        }

        Napier.i("Resolved ${hostname}=$res")
        OctoAnalytics.logEvent(OctoAnalytics.Event.BackupDnsResolveSuccess)
        return res
    }

    private fun Int.asIpString(): String {
        var ip = this
        return (0..3).joinToString(".") {
            val octet = (ip and 0xff).toString()
            ip = ip shr 8
            octet
        }
    }

    // Add async as this function might be called from a thread a DNS lookup is waiting for,
// causing a dead lock until the DNS lookup is force-cancelled
    override fun addCacheEntry(entry: CachedDns.Entry) {
        AppScope.launch(Dispatchers.IO) {
            if (!didLoadCache) {
                loadCache()
            }

            resolveLock.withDebugLock("add-device-to-cache") {
                // Directly add all devices we found to cache
                val entryWithTtl = entry.copy(validUntil = nextValidUntil())
                Napier.v(tag = tag, message = "Add to cache: ${entryWithTtl.hostname} -> ${entryWithTtl.resolvedIp} (valid until: ${entryWithTtl.validUntil})")
                val old = cache[entryWithTtl.hostname.lowercase()] ?: entryWithTtl.copy(hostname = entryWithTtl.hostname.lowercase())
                val new = old.copy(resolvedIpString = (old.resolvedIpString + entryWithTtl.resolvedIpString).distinct())
                cache[entryWithTtl.hostname.lowercase()] = new
                persistCache()
            }
        }
    }

    private fun nextValidUntil() = Clock.System.now() + cacheEntryTtl

    fun exportCache(): ByteArray = resolveLock.withDebugLock("export-cache") {
        if (!didLoadCache) {
            loadCache()
        }

        SettingsJson.encodeToString(cache).toByteArray()
    }

    fun importCache(cache: ByteArray) = resolveLock.withDebugLock("import-cache") {
        importAll(SettingsJson.decodeFromString(String(cache)))
    }

    fun importAll(all: Map<String, CachedDns.Entry>) = resolveLock.withDebugLock("import-all") {
        cache.clear()
        cache.putAll(all)
        persistCache()
    }

    private fun <T> ReentrantLock.withDebugLock(tag: String, block: () -> T) = withLock {
        if (Looper.getMainLooper() == Looper.myLooper()) throw IllegalStateException("No main thread allowed")
        try {
            Napier.v("LOCK: $tag")
            block()
        } finally {
            Napier.v("UNLOCK: $tag")
        }
    }

    private val CachedDns.Entry.resolvedIp get() = resolvedIpString.map { InetAddress.getByName(it) }

}