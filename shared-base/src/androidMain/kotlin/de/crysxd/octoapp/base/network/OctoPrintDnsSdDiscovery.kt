package de.crysxd.octoapp.base.network

import android.content.Context
import de.crysxd.octoapp.base.network.dnssd.DnsSd
import de.crysxd.octoapp.base.utils.AppScope
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.job
import kotlinx.coroutines.launch
import java.net.InetAddress


class OctoPrintDnsSdDiscovery(
    private val context: Context,
    private val dnssd: DnsSd,
) {

    private val tag = "OctoPrintDnsSdDiscovery"

    suspend fun discover(callback: (Service) -> Unit) {

        // Sometimes the internal Dnssd service is not running...we can start it with this:
        context.applicationContext.getSystemService(Context.NSD_SERVICE)

        try {
            discoverWithMulticastLock(callback)
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Error during discovery", throwable = e)
        }
    }

    private suspend fun discoverWithMulticastLock(callback: (Service) -> Unit) {
        Napier.i(tag = tag, message = "Starting mDNS discovery")
        val service = dnssd.browse(
            regType = "_octoprint._tcp",
            serviceLost = {},
            serviceFound = { service ->
                Napier.i(tag = tag, message = "Found ${service.description}")
                AppScope.launch(Dispatchers.IO) {
                    dnssd.resolve(
                        service = service,
                        resolved = {
                            callback(
                                Service(
                                    label = it.label,
                                    webUrl = it.webUrl,
                                    port = it.port,
                                    hostname = it.hostname,
                                    host = it.host,
                                )
                            )
                        },
                        failure = {
                            Napier.e(tag = tag, message = "mDNS resolve failed (${it.message})")
                        },
                    )
                }
            },
            failure = {
                Napier.e(tag = tag, message = "mDNS browse failed (${it.message})")
            },
        )

        currentCoroutineContext().job.invokeOnCompletion {
            Napier.i(tag = tag, message = "Stopping mDNS discovery")
            service.stop()
        }

        Job().join()
    }

    data class Service(
        val label: String,
        val webUrl: String,
        val port: Int,
        val host: InetAddress,
        val hostname: String,
    )
}
