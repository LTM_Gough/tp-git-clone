package de.crysxd.octoapp.base.ext

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.io.OctoSharedPreferencesSettings

fun OctoPreferences.export() =
    requireNotNull(settings as? OctoSharedPreferencesSettings) { "Can only export from OctoSharedPreferencesSettings" }.export()

fun OctoPreferences.import(bytes: ByteArray) =
    requireNotNull(settings as? OctoSharedPreferencesSettings) { "Can only export from OctoSharedPreferencesSettings" }.import(bytes)