package de.crysxd.octoapp.base

import android.os.Bundle
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase


actual class AnalyticsAdapter actual constructor() {
    actual fun logEvent(event: String, params: Map<String, Any?>) {
        val bundle = Bundle()
        params.forEach {
            when (val v = it.value) {
                null -> return@forEach
                is String -> bundle.putString(it.key, v)
                is Int -> bundle.putInt(it.key, v)
                is Long -> bundle.putLong(it.key, v)
                is Float -> bundle.putFloat(it.key, v)
                is Boolean -> bundle.putBoolean(it.key, v)
                else -> bundle.putString(it.key, v.toString())
            }
        }

        Firebase.analytics.logEvent(event, bundle)
    }

    actual fun setUserProperty(property: String, value: String?) {
        Firebase.analytics.setUserProperty(property, value)
    }
}