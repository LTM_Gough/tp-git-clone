package de.crysxd.octoapp.base.network

import android.content.Context
import de.crysxd.octoapp.base.data.models.NetworkService
import de.crysxd.octoapp.base.network.dnssd.DnsSd
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AndroidNetworkServiceDiscovery(
    private val context: Context,
    private val dnsSd: DnsSd,
) : NetworkServiceDiscovery {

    override suspend fun discover(callback: (NetworkService) -> Unit) {
        withContext(Dispatchers.IO) {
            launch {
                OctoPrintUpnpDiscovery(context = context).discover {
                    callback(
                        NetworkService(
                            label = "OctoPrint via UPnP",
                            detailLabel = it.address.hostName,
                            webUrl = "http://${it.upnpHostname}:80/",
                            origin = NetworkService.Origin.Upnp,
                            originQuality = 10,
                        )
                    )
                }
            }

            launch {
                OctoPrintDnsSdDiscovery(context = context, dnssd = dnsSd).discover {
                    callback(
                        NetworkService(
                            label = it.label,
                            detailLabel = it.webUrl.toUrl().host,
                            webUrl = it.webUrl,
                            origin = NetworkService.Origin.DnsSd,
                            originQuality = 20,
                        )
                    )
                }
            }
        }
    }
}

