package de.crysxd.octoapp.base.network.wear

import android.content.Context
import android.content.pm.PackageManager
import com.google.android.gms.tasks.Tasks
import com.google.android.gms.wearable.CapabilityClient
import com.google.android.gms.wearable.ChannelClient
import com.google.android.gms.wearable.Wearable
import de.crysxd.octoapp.base.ext.blockingAwait
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.closeQuietly
import de.crysxd.octoapp.base.utils.requireString
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeout
import java.io.InputStream
import java.io.OutputStream
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.ServerSocket
import java.net.Socket
import java.util.UUID
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong

class WearOsSocksProxyEntryPoint(context: Context) {
    companion object {
        private const val SOCKET_ACTIVITY_THRESHOLD_SECS = 20
        private const val MAX_ACTIVE_SESSIONS = 32
    }

    private val tag = "WearOsSocksProxyEntryPoint"
    private val capabilityClient = Wearable.getCapabilityClient(context)
    private val channelClient = Wearable.getChannelClient(context)
    private val socksCapability = requireString("rpc_capability___socks_proxy").toString()
    private val socksChannelPath = requireString("rpc_channel_path___socks_proxy").toString()
    private var socksNodeIdFlow = MutableStateFlow<String?>(null)
    private val ioThreadPool = Executors.newCachedThreadPool()
    private val activeSockets = mutableListOf<Pair<Socket, AtomicLong>>()
    private val channelIdBase = UUID.randomUUID().toString().split("-").first()
    private var channelCounter = 0
    private var proxySocketAddress: InetSocketAddress? = null

    val socketAddress get() = proxySocketAddress.takeIf { socksNodeIdFlow.value != null }

    init {
        require(context.packageManager.hasSystemFeature(PackageManager.FEATURE_WATCH)) {
            "This class should only be run on Wear OS watches!"
        }

        startForever()
        searchSocksNode()
        capabilityClient.addListener({
            searchSocksNode()
        }, socksCapability)
    }

    private fun nextChannelPath() = listOf(socksChannelPath, channelIdBase, (channelCounter++).toString()).joinToString("/")

    private fun searchSocksNode() = AppScope.launch(Dispatchers.IO) {
        Napier.d(tag = tag, message = "Selecting for socks proxy node")
        socksNodeIdFlow.value = capabilityClient.getCapability(socksCapability, CapabilityClient.FILTER_REACHABLE)
            .blockingAwait()
            .nodes.firstOrNull { it.isNearby }?.id
        Napier.d(tag = tag, message = "Now using ${socksNodeIdFlow.value} as proxy exit point")
    }

    private fun startForever() {
        AppScope.launch(Dispatchers.IO) {
            while (currentCoroutineContext().isActive) {
                try {
                    runEntryPoint()
                } catch (e: Exception) {
                    Napier.e(throwable = e, tag = tag, message = "Socks entry point crashed")
                    delay(1000)
                }
            }
        }
    }

    private suspend fun runEntryPoint() {
        val localhost = InetAddress.getByName(null)
        val serverSocket = ServerSocket(0, 0, localhost)
        Napier.i(tag = tag, message = "Waiting for incoming proxy connections on ${serverSocket.localPort}")
        proxySocketAddress = InetSocketAddress(localhost, serverSocket.localPort)

        while (currentCoroutineContext().isActive) {
            val socket = serverSocket.accept()

            while (activeSockets.size > MAX_ACTIVE_SESSIONS) {
                Napier.i(tag = tag, message = "Too many active sessions, closing oldest: ")
                val oldest = activeSockets.minByOrNull { it.second.get() }
                activeSockets.remove(oldest)
                oldest?.first?.closeQuietly()
            }

            connectChannel(socket)
        }
    }

    private fun connectChannel(socket: Socket) = AppScope.launch(Dispatchers.IO) {
        var wearChannel: ChannelClient.Channel? = null
        val lastIoTime = AtomicLong(System.currentTimeMillis())
        activeSockets.add(socket to lastIoTime)

        try {
            val channel = withTimeout(10_000) {
                val nodeId = socksNodeIdFlow.filterNotNull().first()
                Napier.v(tag = tag, message = "Creating proxy channel for port ${socket.localPort} to $nodeId")
                val channel = channelClient.openChannel(nodeId, nextChannelPath()).blockingAwait()
                wearChannel = channel
                Napier.i(tag = tag, message = "[${channel.path}] Proxy session started, ${activeSockets.size} now active")
                channel
            }

            val input = ioThreadPool.submit {
                val channelOutput = Tasks.await(channelClient.getOutputStream(channel))
                Napier.v(tag = tag, message = "[${channel.path}] Copying channel input to socket output")
                socket.getInputStream().copyTo(channelOutput, lastIoTime)
            }

            val output = ioThreadPool.submit {
                val channelInput = Tasks.await(channelClient.getInputStream(channel))
                Napier.v(tag = tag, message = "[${channel.path}] Copying socket input to channel output")
                channelInput.copyTo(socket.getOutputStream(), lastIoTime)
            }

            Napier.d(tag = tag, message = "[${channel.path}] Copying input/output from socket to channel")

            // Constantly test the socket. If there is no IO for the set threshold, we kill the connection
            while (currentCoroutineContext().isActive) {
                delay(5_000)

                val msSinceLastIo = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastIoTime.get())
                if (msSinceLastIo > SOCKET_ACTIVITY_THRESHOLD_SECS) {
                    Napier.d("[${channel.path}] No activity on socket for ${SOCKET_ACTIVITY_THRESHOLD_SECS}s, killing channel")
                    socket.closeQuietly()
                    input.cancel(true)
                    output.cancel(true)

                    break
                } else {
                    Napier.v(tag = tag, message = "[${channel.path}] Still active, last IO ${msSinceLastIo}s ago")
                }
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "[${wearChannel?.path}] Failure in socks proxy channel, closing socket")
        } finally {
            socket.closeQuietly()
            wearChannel?.let { channelClient.close(it) }
            activeSockets.removeAll { it.first == socket }
            Napier.i(tag = tag, message = "[${wearChannel?.path}] Closing proxy channel, ${activeSockets.size} still active")
        }
    }

    private fun InputStream.copyTo(out: OutputStream, lastIoOperationTime: AtomicLong) {
        val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        var bytes = read(buffer)
        while (bytes >= 0) {
            out.write(buffer, 0, bytes)
            out.flush()
            lastIoOperationTime.set(System.currentTimeMillis())
            bytes = read(buffer)
        }
    }
}