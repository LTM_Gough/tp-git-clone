package de.crysxd.octoapp.base.utils.texts

import android.annotation.SuppressLint
import android.os.Build
import android.text.Html
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector

@SuppressLint("DiscouragedApi")
actual fun getString(id: String, vararg formatArgs: Any): String = SharedCommonInjector.get().platform.context.run {
    val res = resources.getIdentifier(id, "string", packageName).takeUnless { it == 0 }
    res?.let { getString(it, *formatArgs) } ?: id
}

actual fun CharSequence.parseHtml(): CharSequence = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
    Html.fromHtml(this.toString(), Html.FROM_HTML_MODE_COMPACT)
} else {
    @Suppress("DEPRECATION")
    Html.fromHtml(this.toString())
}
