package de.crysxd.octoapp.base.di

import de.crysxd.octoapp.base.network.SslKeyStoreHandler
import de.crysxd.octoapp.base.network.wear.WearOsProxySelector
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.http.config.Dns
import de.crysxd.octoapp.sharedcommon.http.config.KeyStoreProvider
import de.crysxd.octoapp.sharedcommon.http.config.ProxySelector


actual fun createKeyStoreProvider(platform: Platform): KeyStoreProvider = SslKeyStoreHandler(platform.context)

actual fun createProxySelector(
    platform: Platform,
    dns: Dns
): ProxySelector = if (platform.lowPowerDevice) {
    WearOsProxySelector(context = platform.context)
} else {
    ProxySelector.Noop
}