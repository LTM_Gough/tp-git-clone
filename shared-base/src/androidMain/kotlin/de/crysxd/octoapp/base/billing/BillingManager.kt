package de.crysxd.octoapp.base.billing

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.annotation.VisibleForTesting
import androidx.core.content.edit
import com.android.billingclient.api.AcknowledgePurchaseParams
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingClientStateListener
import com.android.billingclient.api.BillingFlowParams
import com.android.billingclient.api.BillingResult
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.PurchasesUpdatedListener
import com.android.billingclient.api.SkuDetails
import com.android.billingclient.api.SkuDetailsParams
import com.android.billingclient.api.acknowledgePurchase
import com.android.billingclient.api.queryPurchasesAsync
import com.android.billingclient.api.querySkuDetails
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.data.models.WidgetType
import de.crysxd.octoapp.base.data.repository.WidgetPreferencesRepository
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.blockingAwait
import de.crysxd.octoapp.base.ext.purchaseOffersForced
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import kotlin.time.Duration.Companion.seconds


actual object BillingManager {

    private const val KEY_PREMIUM_ACTIVE = "premium_active"

    @VisibleForTesting
    var enabledForTest: Boolean? = null

    private val context by lazy { SharedCommonInjector.get().platform.context }
    private val tag = "BillingManager"
    private val billingEventChannel = MutableStateFlow<BillingEvent?>(null)
    private val billingChannel = MutableStateFlow(BillingData(isInitialized = false))
    private val sharedPrefs by lazy { SharedCommonInjector.get().platform.context.getSharedPreferences("billing", Context.MODE_PRIVATE) }
    private val purchasesUpdateListener = PurchasesUpdatedListener { billingResult, purchases ->
        Napier.i(tag = tag, message = "On purchase updated: $billingResult $purchases")
        when (billingResult.responseCode) {
            BillingClient.BillingResponseCode.OK -> {
                OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseFlowCompleted, mapOf("sku" to purchases?.map { it.skus }?.joinToString(",")))

                // Ensure we un-hide the Gcode preview when a purchase was made
                ensureGcodeUnHidden()
                AppScope.launch {
                    purchases?.let { handlePurchases(it) }
                }
            }

            BillingClient.BillingResponseCode.USER_CANCELED -> OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseFlowCancelled)

            else -> {
                OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseFlowFailed)
                logError("Purchase flow failed", billingResult)
            }
        }
    }

    private fun ensureGcodeUnHidden() {
        try {
            SharedBaseInjector.get().widgetPreferencesRepository.apply {
                getWidgetOrder(WidgetPreferencesRepository.LIST_PRINT)?.let { prefs ->
                    val updated = prefs.copy(hidden = prefs.hidden.filter { it != WidgetType.GcodePreviewWidget })
                    setWidgetOrder(WidgetPreferencesRepository.LIST_PRINT, updated)
                }
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, throwable = e, message = "Failed to ensure Gcode unhidden")
        }
    }

    private var billingClient: BillingClient? = null

    private fun MutableStateFlow<BillingData>.update(block: (BillingData) -> BillingData) {
        value = block(value)
        Napier.v(tag = tag, message = "Updated: $value")
    }

    private fun initBilling() = AppScope.launch(Dispatchers.IO) {
        try {
            OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.BillingStatus, "unknown")
            fetchRemoteConfig()

            try {
                val isPremiumActive = sharedPrefs.getBoolean(KEY_PREMIUM_ACTIVE, false)
                Napier.i(tag = tag, message = "Restore state $isPremiumActive")
                billingChannel.update {
                    it.copy(isPremiumActive = isPremiumActive)
                }
            } catch (e: Exception) {
                Napier.i(tag = tag, throwable = e, message = "Failed to load persisted data")
            }

            Napier.i(tag = tag, message = "Initializing billing")
            billingClient?.endConnection()
            billingClient = BillingClient.newBuilder(context)
                .setListener(purchasesUpdateListener)
                .enablePendingPurchases()
                .build()

            billingClient?.startConnection(object : BillingClientStateListener {
                override fun onBillingSetupFinished(billingResult: BillingResult) {
                    if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                        Napier.i("Billing connected")
                        OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.BillingStatus, "available")

                        // Don't load SKU on watches
                        if (!context.packageManager.hasSystemFeature(PackageManager.FEATURE_WATCH)) {
                            updateSku()
                        }

                        queryPurchases()
                        billingChannel.update {
                            it.copy(isBillingAvailable = Firebase.remoteConfig.getBoolean("billing_active"))
                        }
                    } else {
                        Napier.w(tag = tag, message = "Billing not connected: ${billingResult.debugMessage}")
                        billingChannel.update {
                            it.copy(isBillingAvailable = false, isInitialized = true)
                        }
                    }
                }

                override fun onBillingServiceDisconnected() {
                    Napier.i(tag = tag, message = "Billing disconnected")
                    billingClient = null
                    billingChannel.update {
                        it.copy(isBillingAvailable = false, isInitialized = true)
                    }
                }
            })
        } catch (e: Exception) {
            OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.BillingStatus, "error")
            Napier.e(tag = tag, message = "Faield", throwable = e)
        }
    }

    private suspend fun fetchRemoteConfig() {
        try {
            withTimeout(3.seconds) {
                Napier.i(tag = tag, message = "Fetching latest remote config")
                Firebase.remoteConfig.fetchAndActivate().blockingAwait()
            }
        } catch (e: Exception) {
            // Continue with old values
        }
    }

    private fun updateSku() = AppScope.launch(Dispatchers.IO) {
        try {
            fetchRemoteConfig()
            Napier.i(tag = tag, message = "Updating SKU")
            val offers = Firebase.remoteConfig.purchaseOffersForced
            val subscriptionSkuIds = offers.subscriptionSku ?: emptyList()
            val purchaseSkuIds = offers.purchaseSku ?: emptyList()
            Napier.i(tag = tag, message = "Fetching SKU: subscriptions=$subscriptionSkuIds purchases=$purchaseSkuIds")

            val supervisor = SupervisorJob()
            val subscriptions = async(supervisor) {
                fetchSku(
                    SkuDetailsParams.newBuilder()
                        .setSkusList(subscriptionSkuIds)
                        .setType(BillingClient.SkuType.SUBS)
                        .build()
                )
            }

            val purchases = async(supervisor) {
                fetchSku(
                    SkuDetailsParams.newBuilder()
                        .setSkusList(purchaseSkuIds)
                        .setType(BillingClient.SkuType.INAPP)
                        .build()
                )
            }

            val allSku = listOf(subscriptions.await(), purchases.await()).flatten()
            Napier.i(tag = tag, message = "Updated SKU: ${allSku.map { it.sku }}")
            Napier.i(tag = tag, message = "Active offer: ${offers.activeConfig}")
            Napier.i(tag = tag, message = "Premium features: ${Firebase.remoteConfig.getString("premium_features")}")
            billingChannel.update { it.copy(allSku = allSku) }
        } catch (e: Exception) {
            Napier.e(tag = tag, throwable = e, message = "Failed")
        }
    }

    private suspend fun fetchSku(params: SkuDetailsParams): List<SkuDetails> {
        if (params.skusList.isEmpty()) {
            return emptyList()
        } else {
            Napier.i(tag = tag, message = "Fetching ${params.skusList.size} SKUs for ${params.skuType}")
        }

        val result = billingClient?.querySkuDetails(params)
        return if (result?.billingResult?.responseCode == BillingClient.BillingResponseCode.OK) {
            result.skuDetailsList ?: emptyList()
        } else {
            logError("SKU update failed for $params", result?.billingResult)
            emptyList()
        }
    }

    fun billingFlow() = billingChannel.asStateFlow()
    fun billingEventFlow() = billingEventChannel.asStateFlow().filterNotNull()

    fun purchase(activity: Activity, skuDetails: SkuDetails): Boolean {
        val flowParams = BillingFlowParams.newBuilder()
            .setSkuDetails(skuDetails)
            .build()

        val billingResult = billingClient?.launchBillingFlow(activity, flowParams)
        return if (billingResult?.responseCode == BillingClient.BillingResponseCode.OK) {
            true
        } else {
            OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseFlowFailed, mapOf("code" to billingResult?.responseCode, "sku" to skuDetails.sku))
            logError("Unable to launch billing flow", billingResult)
            false
        }
    }

    private suspend fun handlePurchases(purchases: List<Purchase>) {
        Napier.i(tag = tag, message = "Handling ${purchases.size} purchases PATCHED")
        try {
            // Collect all purchases, we cache purchases in the current session to prevent hick ups
            val allPurchases = billingChannel.value.rawPurchases.toMutableMap().apply {
                putAll(purchases.map { purchase -> purchase.orderId to purchase })
            }.values

            Napier.i(tag = tag, message = "Looking at ${allPurchases.size} purchases in total: ${allPurchases.map { it.orderId }}")

            // Check if premium is active
            val premiumActive = allPurchases.any {
                Purchase.PurchaseState.PURCHASED == it.purchaseState
            }
            val fromSubscription = allPurchases.any {
                Purchase.PurchaseState.PURCHASED == it.purchaseState && it.skus.any { sku -> sku.contains("_sub_") }
            }

            OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.PremiumUser, premiumActive.toString())
            OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.PremiumSubUser, fromSubscription.toString())

            try {
                Napier.i(tag = tag, message = "Persisting state $premiumActive")
                sharedPrefs.edit { putBoolean(KEY_PREMIUM_ACTIVE, premiumActive) }
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to persist state $premiumActive", throwable = e)
            }

            billingChannel.update {
                it.copy(
                    isInitialized = true,
                    isPremiumActive = premiumActive,
                    isPremiumFromSubscription = fromSubscription,
                    purchases = it.purchases.toMutableSet().apply {
                        addAll(purchases.map { purchase -> purchase.orderId })
                    },
                    rawPurchases = it.rawPurchases.toMutableMap().apply {
                        putAll(purchases.map { purchase -> purchase.orderId to purchase })
                    },
                )
            }

            // Activate purchases
            var purchaseEventSent = false
            purchases.forEach { purchase ->
                if (!purchase.isAcknowledged) {
                    if (!purchaseEventSent) {
                        purchaseEventSent = true
                        billingEventChannel.value = BillingEvent.PurchaseCompleted
                    }

                    val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
                        .setPurchaseToken(purchase.purchaseToken)
                    val billingResult = withContext(Dispatchers.IO) {
                        billingClient?.acknowledgePurchase(acknowledgePurchaseParams.build())
                    } ?: return Napier.w(tag = tag, message = "BillingClient was not ready, unable to handle purchases")
                    if (billingResult.responseCode != BillingClient.BillingResponseCode.OK) {
                        logError("Failed to acknowledge purchase ${purchase.orderId}", billingResult)
                    } else {
                        Napier.i(tag = tag, message = "Confirmed purchase ${purchase.orderId}")
                    }
                }
            }


        } catch (e: Exception) {
            Napier.e(throwable = e, message = "Failed")
        }
    }

    private fun logError(description: String, billingResult: BillingResult?) {
        if (billingResult == null) {
            Napier.w(tag = tag, message = "Billing result is null, indicating billing connection was paused during an active process")
            return
        }

        if (billingResult.responseCode == 2) {
            Napier.w(tag = tag, message = "No internet connection, service unavailable")
            return
        }

        if (billingResult.responseCode == 6) {
            Napier.w(tag = tag, message = "Internal error")
            return
        }

        if (billingResult.responseCode == -1) {
            Napier.w(tag = tag, message = "Service was disconnected")
            return
        }

        val playServicesAvailable = try {
            val googleApiAvailability = GoogleApiAvailability.getInstance()
            val resultCode = googleApiAvailability.isGooglePlayServicesAvailable(context)
            resultCode == ConnectionResult.SUCCESS
        } catch (e: Exception) {
            Napier.e(throwable = e, message = "Failed", tag = tag)
            null
        }

        if (playServicesAvailable != false) {
            Napier.e(
                tag = tag,
                message = "No Play Services",
                throwable = Exception("$description. responseCode=${billingResult.responseCode} message=${billingResult.debugMessage} billingResult=${billingResult.let { "non-null" }} playServicesAvailable=$playServicesAvailable")
            )
        } else {
            Napier.w("BillingManager encountered problem but Play Services are not available")
        }
    }

    private fun queryPurchases() = AppScope.launch(Dispatchers.IO) {
        suspend fun queryPurchases(@BillingClient.SkuType type: String): List<Purchase> {
            val purchaseResult = billingClient?.queryPurchasesAsync(type)
            return if (purchaseResult?.billingResult?.responseCode != BillingClient.BillingResponseCode.OK) {
                logError("Unable to query purchases", purchaseResult?.billingResult)
                emptyList()
            } else {
                purchaseResult.purchasesList
            }
        }

        try {
            if (billingClient?.isReady == true) {
                Napier.i(tag = tag, message = "Querying purchases")
                val purchases = listOf(
                    queryPurchases(BillingClient.SkuType.INAPP),
                    queryPurchases(BillingClient.SkuType.SUBS)
                ).flatten()
                Napier.i(tag = tag, message = "Found ${purchases.size} purchases")
                handlePurchases(purchases)
            } else {
                Napier.i(tag = tag, message = "Billing client not ready, skipping purchase query")
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed", throwable = e)
        }
    }

    actual fun isFeatureEnabled(feature: String): Boolean {
        val isPremiumFeature = Firebase.remoteConfig.getString("premium_features").split(",").map { it.trim() }.contains(feature)
        val hasPremium = billingChannel.value.isPremiumActive
        return enabledForTest ?: (!isPremiumFeature || hasPremium)
    }

    actual fun isFeatureEnabledFlow(feature: String) = billingFlow().map {
        isFeatureEnabled(feature)
    }

    actual fun shouldAdvertisePremium() = billingChannel.value.let {
        it.isBillingAvailable && !it.isPremiumActive && !Firebase.remoteConfig.purchaseOffersForced.activeConfig.offers.isNullOrEmpty() && it.allSku.isNotEmpty()
    }

    actual fun onResume() {
        AppScope.launch {
            Napier.i("Resuming billing")
            initBilling()
        }
    }

    actual fun onPause() {
        Napier.i("Pausing billing")
        billingClient?.endConnection()
        billingClient = null
    }

    actual fun getPurchases(): Set<String> = billingFlow().value.purchases
}