package de.crysxd.octoapp.base.network.dnssd

import android.os.Build
import com.github.druk.dnssd.BrowseListener
import com.github.druk.dnssd.DNSSDBindable
import com.github.druk.dnssd.DNSSDEmbedded
import com.github.druk.dnssd.DNSSDService
import com.github.druk.dnssd.QueryListener
import com.github.druk.dnssd.ResolveListener
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import io.github.aakira.napier.Napier
import java.net.InetAddress

class LegacyDnsSd : DnsSd {

    companion object {
        private var instanceCounter = 0
        val SharedDnsSd by lazy {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                DNSSDEmbedded(SharedCommonInjector.get().platform.context)
            } else {
                DNSSDBindable(SharedCommonInjector.get().platform.context)
            }
        }
    }

    private val tag = "LegacyDnsSd/${instanceCounter++}"

    override suspend fun browse(
        regType: String,
        serviceFound: (DnsSd.ServiceData) -> Unit,
        serviceLost: (DnsSd.ServiceData) -> Unit,
        failure: (Throwable) -> Unit
    ): DnsSd.Operation {
        val service = SharedDnsSd.browse("_octoprint._tcp", object : BrowseListener {
            override fun operationFailed(service: DNSSDService?, errorCode: Int) {
                failure(Exception("DNSSD Operation failed with code: $errorCode"))
            }

            override fun serviceFound(browser: DNSSDService?, flags: Int, ifIndex: Int, serviceName: String, regType: String, domain: String) {
                serviceFound(
                    LegacyServiceData(
                        flags = flags,
                        ifIndex = ifIndex,
                        serviceName = serviceName,
                        regType = regType,
                        domain = domain
                    )
                )
            }

            override fun serviceLost(browser: DNSSDService?, flags: Int, ifIndex: Int, serviceName: String, regType: String, domain: String) {
                serviceLost(
                    LegacyServiceData(
                        flags = flags,
                        ifIndex = ifIndex,
                        serviceName = serviceName,
                        regType = regType,
                        domain = domain
                    )
                )
            }
        })

        return object : DnsSd.Operation {
            override fun stop() = service.stop()
        }
    }

    override fun createServiceData(hostName: String) = LegacyServiceData(
        flags = 0,
        ifIndex = 0,
        serviceName = hostName,
        domain = "",
        regType = "",
    )

    override suspend fun resolve(service: DnsSd.ServiceData, resolved: (DnsSd.Service) -> Unit, failure: (Throwable) -> Unit): DnsSd.Operation {
        require(service is LegacyServiceData)

        val s = SharedDnsSd.resolve(service.flags, service.ifIndex, service.serviceName, service.regType, service.domain, object : ResolveListener {
            override fun operationFailed(service: DNSSDService?, errorCode: Int) {
                failure(Exception("DNSSD Operation failed with code: $errorCode"))
            }

            override fun serviceResolved(
                resolver: DNSSDService,
                flags: Int,
                ifIndex: Int,
                fullName: String,
                hostName: String,
                port: Int,
                txtRecord: MutableMap<String, String>
            ) {
                resolver.stop()
                queryService(
                    flags = flags,
                    ifIndex = ifIndex,
                    serviceName = service.serviceName,
                    hostName = hostName,
                    port = port,
                    txtRecord = txtRecord,
                    callback = resolved
                )
            }
        })

        return object : DnsSd.Operation {
            override fun stop() = s.stop()
        }
    }

    private fun queryService(
        flags: Int,
        ifIndex: Int,
        serviceName: String,
        hostName: String,
        port: Int,
        txtRecord: MutableMap<String, String>,
        callback: (DnsSd.Service) -> Unit
    ) {
        SharedDnsSd.queryRecord(flags, ifIndex, hostName, 1 /* IPv4 */, 1, object : QueryListener {
            override fun operationFailed(service: DNSSDService, errorCode: Int) {
                Napier.e(tag = tag, message = "mDNS query failed (errorCode=$errorCode)")
            }

            override fun queryAnswered(query: DNSSDService, flags: Int, ifIndex: Int, fullName: String, rrtype: Int, rrclass: Int, rdata: ByteArray, ttl: Int) {
                query.stop()
                val fixedHostname = hostName.removeSuffix(".")
                Napier.i(tag = tag, message = "Resolved mDNS service $fixedHostname")

                // Construct OctoPrint
                val path = txtRecord["path"] ?: "/"
                val user = txtRecord["u"]
                val password = txtRecord["p"]
                val credentials = user?.let { u ->
                    password?.let { p -> "$u:$p@" } ?: "$u@"
                } ?: ""
                val device = DnsSd.Service(
                    label = serviceName,
                    hostname = fixedHostname,
                    port = port,
                    webUrl = "http://${credentials}${fixedHostname}:${port}$path",
                    host = InetAddress.getByAddress(hostName, rdata)
                )
                SharedBaseInjector.get().dnsResolver.addCacheEntry(
                    CachedDns.Entry(
                        hostname = device.hostname,
                        resolvedIpString = listOfNotNull(device.host.hostAddress),
                    )
                )
                callback(device)
            }
        })
    }

    data class LegacyServiceData(
        val flags: Int,
        val ifIndex: Int,
        val serviceName: String,
        val regType: String,
        val domain: String
    ) : DnsSd.ServiceData {
        override val description = "$regType $serviceName $domain"
    }
}