//
//  FlowStateStruct.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

enum FlowStateStruct<T: Equatable>: Equatable {
    case loading, error(KotlinThrowable), ready(T)
}
