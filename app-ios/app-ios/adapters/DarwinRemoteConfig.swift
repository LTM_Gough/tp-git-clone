//
//  DarwinRemoteConfig.swift
//  OctoApp
//
//  Created by Christian on 26/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import FirebaseRemoteConfig

class DarwinRemoteConfigImpl : DarwinRemoteConfig {
    
    private let remoteConfig: RemoteConfig
    
    init(remoteConfig: RemoteConfig) {
        self.remoteConfig = remoteConfig
    }
    
    func getLong(key: String) -> KotlinLong? {
        if remoteConfig.keys(withPrefix: nil).contains(key) {
            let int = remoteConfig.configValue(forKey: key).numberValue.int64Value
            return KotlinLong(longLong: int)
        } else {
            return nil
        }
    }
    
    func getString(key: String) -> String? {
        if remoteConfig.keys(withPrefix: nil).contains(key) {
            return remoteConfig.configValue(forKey: key).stringValue
        } else {
            return nil
        }
    }
}

extension RemoteConfig {
    func fetchAndActivateWithLog() {
        fetch { (status, error) -> Void in
            if status == .success {
                print("[RemoteConfig] fetched!")
                self.activate()
            } else {
                print("[RemoteConfig] not fetched")
                print("[RemoteConfig] Error: \(error?.localizedDescription ?? "No error available.")")
            }
        }
    }
}
