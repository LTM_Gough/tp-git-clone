//
//  DarwinServiceDisacovery.swift
//  app-ios
//
//  Created by Christian on 12/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import Network
import OctoAppBase

class DarwinNetworkDiscovery : NetworkServiceDiscovery {
    
    init() {
        
    }
    
    
    func discover(callback: @escaping (NetworkService) -> Void) async throws {
        let parameters = NWParameters()
        let  browser = NWBrowser(for: .bonjour(type: "_octoprint._tcp", domain: nil), using: parameters)
        browser.stateUpdateHandler = { newState in
            print("browser.stateUpdateHandler \(newState)")
        }
        
        
        browser.browseResultsChangedHandler = { results, changes in
            for result in results {
                switch result.endpoint {
                case .service(let name, _, _, _):
                    print("DISCOVER: \(name)")
                    self.resolve(name: name, endpoint: result.endpoint, callback: callback)
                    
                case NWEndpoint.url:
                    print("DISCOVER: url -> \(result.endpoint)")
                    
                    
                case NWEndpoint.hostPort:
                    print("DISCOVER: hostPort -> \(result.endpoint)")
                    
                case NWEndpoint.unix:
                    print("DISCOVER: unix -> \(result.endpoint)")
                    
                default:
                    print("DISCOVER: Unknown")
                }
            }
        }
        
        browser.start(queue: .main)
    }
    
    private func resolve(name: String, endpoint: NWEndpoint, callback: @escaping (NetworkService) -> Void) {
        let connection = NWConnection(to: endpoint, using: .tcp)
        connection.stateUpdateHandler = { state in
            switch state {
            case .ready:
                if let innerEndpoint = connection.currentPath?.remoteEndpoint,
                   case .hostPort(let host, let port) = innerEndpoint {
                    switch host {
                    case .name(name, _):
                        print("RESOLVE: \(name)")
//                        self.resolve(name: service.name, endpoint: result.endpoint, callback: callback)

                    case .ipv4(let ip):
                        print("RESOLVE: url -> \(ip.rawValue.debugDescription)")


                    case .ipv6(let ip):
                        print("RESOLVE: hostPort -> \(ip.debugDescription)")

                    default:
                        print("RESOLVE: Unknown")
                    }

                    let h = host.debugDescription.components(separatedBy: "%")[0]
                    callback(
                        NetworkService(
                            label: name,
                            detailLabel: h,
                            webUrl: "http://\(h):\(port)",
                            originQuality: 100,
                            origin: NetworkService.Origin.dnssd
                        )
                    )
                }
            default:
                break
            }
        }
        connection.start(queue: .global())
        
//        let ref = DNSServiceRef()
        
//        DNSServiceResolve(ref, DNSServiceFlags(), endpoint.interface?.index, name, <#T##regtype: UnsafePointer<CChar>!##UnsafePointer<CChar>!#>, ".local", <#T##callBack: DNSServiceResolveReply!##DNSServiceResolveReply!##(DNSServiceRef?, DNSServiceFlags, UInt32, DNSServiceErrorType, UnsafePointer<CChar>?, UnsafePointer<CChar>?, UInt16, UInt16, UnsafePointer<UInt8>?, UnsafeMutableRawPointer?) -> Void#>, <#T##context: UnsafeMutableRawPointer!##UnsafeMutableRawPointer!#>)
    }
}
