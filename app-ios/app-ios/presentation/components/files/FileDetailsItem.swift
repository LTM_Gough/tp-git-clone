//
//  FileDetailsItem.swift
//  OctoApp
//
//  Created by Christian on 23/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct FileDetailsItem: View {
    var title: String
    var value: String?
    
    var body: some View {
        if let v = value {
            HStack {
                Text(title).typographyLabel()
                Spacer()
                Text(v)
                    .foregroundColor(OctoTheme.colors.darkText)
                    .multilineTextAlignment(.trailing)
                    .typographyBase()
            }
        }
    }
}
struct FileDetailsItem_Previews: PreviewProvider {
    static var previews: some View {
        FileDetailsItem(title: "Title", value: "Value")
    }
}
