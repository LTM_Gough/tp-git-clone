//
//  ThumbnailAnnouncement.swift
//  OctoApp
//
//  Created by Christian on 20/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct FilesThumbnailAnnouncement: View {
    
    var informAboutThumbnails: Bool
    var path: String
    var onDismissThumbnailInfo: () -> Void
    @Environment(\.openURL) var openUrl
    
    var body: some View {
        if(informAboutThumbnails && path == "/") {
            Section {
                LargeAnnouncement(
                    partOfList: true,
                    title: "file_manager___thumbnail_info___title"~,
                    text: "file_manager___thumbnail_info___details"~,
                    learnMoreButton: "learn_more"~,
                    onLearnMore: { openUrl( UriLibrary.shared.getPluginLibraryUri(category: "files")) },
                    onHideAnnouncement: onDismissThumbnailInfo
                )
                .listStyle(.plain)
            }
        }
    }
}

struct FilesThumbnailAnnouncement_Previews: PreviewProvider {
    static var previews: some View {
        List {
            FilesThumbnailAnnouncement(informAboutThumbnails: false, path: "/", onDismissThumbnailInfo: {})
        }
    }
}
