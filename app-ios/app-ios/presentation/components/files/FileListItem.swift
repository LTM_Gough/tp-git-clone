//
//  FileListItem.swift
//  OctoApp
//
//  Created by Christian on 20/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct FileListItem: View {
    var file: FileObjectStruct
    private var defaultIcon: String {
        if (file.isFolder) {
            return "folder.fill"
        } else if (file.typePath.isEmpty) {
            return "questionmark.app"
        } else if (file.typePath.last == "gcode") {
            return  "printer.fill"
        } else {
            return  "doc.fill"
        }
    }
    private var statusIcon: String {
        switch(file.prints?.lastPrintSuccess) {
        case false: return "x.circle.fill"
        case true: return "checkmark.circle.fill"
        default: return ""
        }
    }
    private var detail: String {
        if (file.isFolder) {
            return ""
        } else {
            var date: String? = nil
            var size: String? = nil
            
            if let d = file.date {
                date = d.formatted()
            }
            
            if let s = file.size {
                size = "(\(s.asStyleFileSize()))"
            }
            
            return [date, size].filter { $0 != nil }.map { $0!! }.joined(separator: " ")
        }
    }
    
    var body: some View {
        HStack(spacing: OctoTheme.dimens.margin12) {
            ZStack(alignment: .bottomTrailing) {
                NetworkImage(url: file.thumbnail, backupSystemName: defaultIcon)
                    .frame(width: 48, height: 48)
                    .padding(OctoTheme.dimens.margin01)
                    .foregroundColor(OctoTheme.colors.accent)
                    .surface()
                
                Image(systemName: statusIcon)
                    .font(.system(size: 16))
                    .offset(x: 2, y: 2)
                    .foregroundColor(OctoTheme.colors.lightText)
            }
            
            VStack(spacing: OctoTheme.dimens.margin01) {
                Text(file.display)
                    .typographySubtitle()
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .lineLimit(2)
                    .truncationMode(.middle)
                if (!file.isFolder) {
                    Text(detail)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .typographyLabelSmall()
                }
            }
        }
    }
}

struct FileListItem_Previews: PreviewProvider {
    static var previews: some View {
        FileListItem(file: FileObjectStruct(origin: .local, display: "Some folder", name: "Some folder", type: "folder", typePath: ["folder"], path: "/folder", isFolder: true))
            .previewDisplayName("Folder")
        FileListItem(file: FileObjectStruct(origin: .local, display: "Some file.gcode", name: "Some file", type: "file", typePath: ["file", "printable"], path: "/folder/some file.gcode", isFolder: false))
            .previewDisplayName("File")
        FileListItem(file: FileObjectStruct(origin: .local, display: "Some file.gcode", name: "Some file", type: "file", typePath: ["file", "gcode"], date: Date(timeIntervalSince1970: 3248234234), path: "/folder/some file.gcode", size: 36248234, isFolder: false))
            .previewDisplayName("Gcode")
    }
}
