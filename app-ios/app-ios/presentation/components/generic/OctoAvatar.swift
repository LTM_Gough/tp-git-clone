//
//  OctoAvatar.swift
//  OctoApp
//
//  Created by Christian on 15/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import LottieUI

struct OctoAvatar: View {
    
    var action: OctoAvatarAction
    @Namespace private var animation
    @Environment(\.colorScheme) var colorScheme
    
    var body: some View {
        LottieView(state: lottiAnimation)
            .matchedGeometryEffect(id: "avatar", in: animation)
            .id(AnimationId(action: action, colorScheme: colorScheme))
            .aspectRatio(7/3, contentMode: .fit)
    }
    
    var lottiAnimation: LUStateData {
        var suffix = ""
        if colorScheme == .dark {
            suffix = "-dark"
        }
        
        switch action {
        case .wave: return LUStateData(type: .name("octo-wave\(suffix)"), loopMode: .playOnce)
        case .swim: return LUStateData(type: .name("octo-swim\(suffix)"), loopMode: .loop)
        case .party: return LUStateData(type: .name("octo-party\(suffix)"), loopMode: .playOnce)
        case .idle: return LUStateData(type: .name("octo-blink\(suffix)"), loopMode: .loop)
        }
    }
}

private struct AnimationId : Hashable {
    let action: OctoAvatarAction
    let colorScheme: ColorScheme?
}

enum OctoAvatarAction {
    case idle, wave, swim, party
}

struct OctoAvatar_Previews: PreviewProvider {
    static var previews: some View {
        OctoAvatar(action: .wave)
            .previewDisplayName("Wave")
        
        OctoAvatar(action: .swim)
            .previewDisplayName("Swim")
        
        OctoAvatar(action: .idle)
            .previewDisplayName("Idle")
        
        OctoAvatar(action: .idle)
            .previewDisplayName("Blink")
        
        OctoAvatar(action: .idle)
            .previewDisplayName("Party")
    }
}

