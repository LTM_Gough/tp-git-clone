//
//  SafariView.swift
//  app-ios
//
//  Created by Christian on 15/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import SafariServices
import OctoAppBase

struct SafariView: UIViewControllerRepresentable {

    let url: Ktor_httpUrl
    
    init(url: String) {
        self.url = UrlExtKt.toUrl(url)
    }
    
    init(url: Ktor_httpUrl) {
        self.url = url
    }

    func makeUIViewController(context: UIViewControllerRepresentableContext<SafariView>) -> SFSafariViewController {
        return SFSafariViewController(url: URL(string: url.description())!)
    }

    func updateUIViewController(_ uiViewController: SFSafariViewController, context: UIViewControllerRepresentableContext<SafariView>) {

    }
}

struct SafariView_Previews: PreviewProvider {
    static var previews: some View {
        SafariView(url: UrlExtKt.toUrl("https://google.com"))
    }
}
