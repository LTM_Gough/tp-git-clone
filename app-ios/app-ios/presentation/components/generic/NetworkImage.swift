//
//  PicassoImage.swift
//  OctoApp
//
//  Created by Christian on 21/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

let NetworkImageLoadingMarker: String = "loading"

struct NetworkImage: View {
    var url: String?
    var backupSystemName: String = "photo"
    var backupColor: Color = OctoTheme.colors.accent
    var small = true
    
    @Environment(\.instanceId) private var instanceId
    @StateObject private var viewModel = PicassoImageViewModel()
    
    var body: some View {
        ZStack {
            switch(viewModel.state){
            case .loading: ProgressView()
                    .if(small) { $0.scaleEffect(0.66) }
                    .transition(.opacity)
                
            case .loaded(let image): Image(uiImage: image)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .transition(.opacity)
                
            case .failed: Image(systemName: backupSystemName)
                    .foregroundColor(backupColor)
                    .font(.system(size: 24))
                    .transition(.opacity)
                    .if(!small) { $0.onTapGesture { triggerLoad(url: url) } }
            }
        }
        .animation(.default, value: viewModel.state)
        .onChange(of: url, perform: { triggerLoad(url: $0) })
        .onAppear { triggerLoad(url: url) }
        .onDisappear { viewModel.cancel() }
    }
    
    func triggerLoad(url: String?) {
        viewModel.loadImage(instanceId: instanceId, url: url)
    }
}

private class PicassoImageViewModel : ObservableObject {
    @Published var state: State = .loading
    private var task: Task<Void, Never>? = nil
        
    func cancel() {
        task?.cancel()
    }
    
    func loadImage(instanceId: String, url: String?) {
        // Loading marker?
        guard url != NetworkImageLoadingMarker else {
            self.state = .loading
            return
        }
        
        // URL valid?
        guard let checkedUrl = url else {
            self.state = .failed
            return
        }
      
        // Already cached?
        task?.cancel()
        let cacheKey = "\(instanceId)/\(checkedUrl)"
        if let cached = LruCache[cacheKey] {
            LruCache[cacheKey] = CacheEntry(lastUsed: Date(), image: cached.image, cacheKey: cacheKey)
            self.state = .loaded(cached.image)
            return
        }
        
        // Load
        self.state = .loading
        task = Task.detached(priority: .medium) {
            do {
                let data = try await SimpleNetworkRequestKt.simpleNetworkRequest(instanceId: instanceId, path: checkedUrl)
                
                DispatchQueue.main.async {
                    if let image = UIImage(data: data) {
                        LruCache[cacheKey] = CacheEntry(lastUsed: Date(), image: image, cacheKey: cacheKey)
                        limitCacheSize()
                        self.state = .loaded(image)
                    } else {
                        self.state = .failed
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    self.state = .failed
                }
            }
        }
    }
}

private var LruCache = Dictionary<String, CacheEntry>()

private func limitCacheSize() {
    // Limit cache size to 25 images, delete oldest until limit is adhered to
    while (LruCache.count > 25) {
        let oldest = LruCache.values.sorted(by: { $0.lastUsed < $1.lastUsed })[0]
        LruCache.removeValue(forKey: oldest.cacheKey)
    }
}

private struct CacheEntry {
    var lastUsed: Date
    var image: UIImage
    var cacheKey: String
}

private enum State: Equatable {
    case loading, loaded(UIImage), failed
}

struct PicassoImage_Previews: PreviewProvider {
    static var previews: some View {
        NetworkImage(url: "http://google.com")
            .previewDisplayName("Failed")
        NetworkImage(url: NetworkImageLoadingMarker )
            .previewDisplayName("Loading")
    }
}
