//
//  MenuItem.swift
//  OctoApp
//
//  Created by Christian on 25/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct MenuItem: View {
    var title: String
    var iconSystemName: String?
    var foregroundColor: Color
    var backgroundColor: Color
    var showAsOutline: Bool = false
    var showAsSubMenu: Bool = false
    
    private let strokeWidth: CGFloat = 1
    
    var body: some View {
        HStack(spacing: OctoTheme.dimens.margin1) {
            if let icon = iconSystemName {
                Image(systemName: icon)
                    .font(.system(size: 20))
                    .foregroundColor(foregroundColor)
                    .padding(OctoTheme.dimens.margin01)
                    .aspectRatio(1, contentMode: .fit)
                    .frame(minWidth: 40, minHeight: 40)
                    .padding(strokeWidth)
                    .background(Circle().fill(backgroundColor))
            } else {
                Spacer()
                    .frame(width: OctoTheme.dimens.margin1)
            }
            
            Text(title)
                .foregroundColor(OctoTheme.colors.darkText)
                .typographyButton(small: true)
                .lineLimit(1)
                .padding([.top, .bottom], OctoTheme.dimens.margin1)
            
            Spacer()
            
            if(showAsSubMenu) {
                Image(systemName: "chevron.forward")
                    .font(.system(size: 20))
                    .aspectRatio(1, contentMode: .fit)
                    .foregroundColor(foregroundColor)
                    .padding(OctoTheme.dimens.margin1)
            }
        }
        .frame(maxWidth: .infinity)
        .if(showAsOutline) { $0.background(Capsule().strokeBorder(foregroundColor, lineWidth: strokeWidth)) }
        .if(!showAsOutline) { $0.background(Capsule().fill(backgroundColor)) }
    }
}

struct MenuItem_Previews: PreviewProvider {
    static var previews: some View {
        MenuItem(
            title: "Explore plugins",
            iconSystemName: nil,
            foregroundColor: OctoTheme.colors.menuStylePrinterForeground,
            backgroundColor: OctoTheme.colors.menuStylePrinterBackground
        )
        .previewDisplayName("No icon")
        
        MenuItem(
            title: "Explore plugins",
            iconSystemName: "puzzlepiece.extension.fill",
            foregroundColor: OctoTheme.colors.menuStylePrinterForeground,
            backgroundColor: OctoTheme.colors.menuStylePrinterBackground,
            showAsOutline: true
        )
        .previewDisplayName("Outline")
        
        MenuItem(
            title: "Explore plugins",
            iconSystemName: "puzzlepiece.extension.fill",
            foregroundColor: OctoTheme.colors.menuStylePrinterForeground,
            backgroundColor: OctoTheme.colors.menuStylePrinterBackground,
            showAsOutline: false
        )
        .previewDisplayName("Normal")
        
        NavigationLink(value: "") {
            MenuItem(
                title: "Explore plugins",
                iconSystemName: "puzzlepiece.extension.fill",
                foregroundColor: OctoTheme.colors.menuStylePrinterForeground,
                backgroundColor: OctoTheme.colors.menuStylePrinterBackground,
                showAsSubMenu: true
            )
        }
        .previewDisplayName("Link")
    }
}
