//
//  OctoButton.swift
//  app-ios
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct OctoButton: View {
    
    var text: LocalizedStringKey
    var type: OctoButtonType = .primary
    var small = false
    var loading = false
    var enabled = true
    var clickListener: () -> Void
    private var active: Bool { return enabled && !loading }
    
    var body: some View {
        Button(action: { if active { clickListener() } }) {
            ZStack {
                Text(text)
                    .foregroundColor(textColor(type: type, active: active))
                    .if(type != .link) { $0.typographyButton(small: small) }
                    .if(type == .link) { $0.typographyButtonLink() }
                    .shadow(color: .clear, radius: 0)
                    .opacity(loading ? 0 : 1)
                    .transition(.opacity)
                    .lineLimit(1)
                
                if(loading) {
                    ProgressView()
                        .transition(.opacity)
                        .tint(textColor(type: type, active: active))
                }
            }
        }
        .buttonStyle(OctoButtonStyle(type: type, small: small, active: active))
        .animation(.default, value: loading)
    }
}

struct OctoAsyncButton : View {
    
    @State var loading: Bool = false
    
    var text: LocalizedStringKey
    var type: OctoButtonType = .primary
    var small = false
    var enabled = true
    var clickListener: () async throws -> Void
    
    var body: some View {
        OctoButton(
            text: text,
            type: type,
            small: small,
            loading: loading,
            enabled: enabled
        ) {
            self.loading = true
            Task {
                do {
                    try await clickListener()
                } catch {
                    ExceptionReceivers.shared.dispatchException(throwable: KotlinThrowable(message: "Error in Swift: \(error)"))
                }
                
                DispatchQueue.main.async {
                    self.loading = false
                }
            }
        }
    }
}

struct OctoMenuButton: View {
    
    @State private var pressed = false
    var clickListener: () -> Void
    
    var body: some View {
        Button(action: clickListener) {
            ZStack {
                Text("X")
                    .opacity(0)
                    .typographyButton(small: true)
                
                Image(systemName: "line.3.horizontal")
                    .padding(.trailing, OctoTheme.dimens.margin1)
                    .shadow(color: .clear, radius: 0)
                    .foregroundColor(OctoTheme.colors.accent)
                    .typographyButton(small: true)
            }
        }
        .buttonStyle(OctoButtonStyle(type: .secondary, small: true, active: true))
    }
}

enum OctoButtonType : Equatable {
    case primary, primaryUnselected, secondary, link, surface
    case special(background: Color, foreground: Color, stroke: Color)
}

private func backgroundColor(type: OctoButtonType, active: Bool) -> Color {
    if (!active) {
        return OctoTheme.colors.lightGrey
    }
    
    switch(type){
    case .primary, .surface: return OctoTheme.colors.accent
    case .link, .primaryUnselected: return .clear
    case .secondary: return OctoTheme.colors.inputBackground
    case .special(background: let c, foreground: _, stroke: _): return c
    }
}

private func borderColor(type: OctoButtonType) -> Color {
    switch type {
    case .primary, .surface, .primaryUnselected, .link: return .clear
    case .secondary: return OctoTheme.colors.accent
    case .special(background: _, foreground: _, stroke: let c): return c
    }
}

private func textColor(type: OctoButtonType, active: Bool) -> Color {
    if !active {
        return OctoTheme.colors.normalText
    }
    
    switch type {
    case .primary, .surface: return OctoTheme.colors.textColoredBackground
    case .link: return OctoTheme.colors.accent
    case .secondary, .primaryUnselected: return OctoTheme.colors.accent
    case .special(background: _, foreground: let c, stroke: _): return c
    }
}

private func hasShadow(type: OctoButtonType, active: Bool, small: Bool) -> Bool {
    if !active {
        return false
    }
    
    if small {
        return false
    }
    
    switch(type){
    case .primary, .secondary, .special(_, _ ,_ ): return true
    default: return false
    }
}

private struct OctoButtonStyle: ButtonStyle {
    
    let type: OctoButtonType
    let small: Bool
    let active: Bool
    
    func makeBody(configuration: Self.Configuration) -> some View {
        let activelyPressed = configuration.isPressed && active
        
        configuration.label
            .frame(maxWidth: small && type != .surface ? nil : .infinity)
            .padding([.leading, .trailing], OctoTheme.dimens.margin2)
            .padding([.top, .bottom], small ? OctoTheme.dimens.margin1 : OctoTheme.dimens.margin2)
            .if(type == .surface) { $0.background(Rectangle().fill(activelyPressed ? OctoTheme.colors.clickIndicatorOverlay : .clear)) }
            .if(type != .surface) { $0.background(Capsule().fill(activelyPressed ? OctoTheme.colors.clickIndicatorOverlay : .clear)) }
            .if(type == .surface) { $0.background(Rectangle().fill(backgroundColor(type: type, active: active))) }
            .if(type != .surface) { $0.background(Capsule().fill(backgroundColor(type: type, active: active))) }
            .if(hasShadow(type: type, active: active, small: small)) { $0.background(Capsule().shadow(radius: activelyPressed ? 8 : 3)) }
            .if(type == .surface) { $0.background(Rectangle().stroke(borderColor(type: type), lineWidth: 2)) }
            .if(type != .surface) { $0.background(Capsule().stroke(borderColor(type: type), lineWidth: 2)) }
            .scaleEffect(activelyPressed && !small ? OctoTheme.dimens.buttonActiveScale : 1)
    }
}

struct OctoButton_Previews: PreviewProvider {
    static var previews: some View {
        VStack(spacing: 15) {
            OctoButton(text: "Primary", type: .primary) {}
            OctoButton(text: "Primary", type: .primary, enabled: false) {}
            OctoButton(text: "Primary", type: .primary, loading: true) {}
            
            OctoButton(text: "Secondary", type: .secondary) {}
            OctoButton(text: "Link", type: .link) {}
            OctoButton(text: "Surface", type: .surface) {}
            OctoButton(text: "Primary Small", type: .primary, small: true) {}
            OctoMenuButton {}
        }
    }
}

