//
//  SmallAnnouncement.swift
//  OctoApp
//
//  Created by Christian on 20/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI


struct LargeAnnouncement: View {
    var partOfList:Bool = false
    var title: String
    var text: String
    var learnMoreButton: String? = nil
    var hideButton: String = "hide"
    var onLearnMore: () -> Void = {}
    var onHideAnnouncement: () -> Void
    
    var sideMargin: CGFloat { return partOfList ? OctoTheme.dimens.margin12 : OctoTheme.dimens.margin2 }
    
    var body: some View {
        VStack(alignment: .leading, spacing: OctoTheme.dimens.margin0) {
            Text(title)
                .frame(maxWidth: .infinity, alignment: .leading)
                .typographySubtitle()
                .padding([.leading, .trailing, .top], sideMargin)

            
            Text(text)
                .foregroundColor(OctoTheme.colors.lightText)
                .frame(maxWidth: .infinity, alignment: .leading)
                .typographyBase()
                .padding([.leading, .trailing], sideMargin)
                .padding(.top, OctoTheme.dimens.margin0)
            
            HStack(spacing: -OctoTheme.dimens.margin12) {
                Spacer()
                if let lm = learnMoreButton {
                    OctoButton(
                        text: LocalizedStringKey(lm),
                        type: .link,
                        small: true,
                        clickListener: onLearnMore
                    )
                }
                OctoButton(
                    text: LocalizedStringKey(hideButton),
                    type: .link,
                    small: true,
                    clickListener: onHideAnnouncement
                )
            }
            .if(!partOfList) { $0.padding(.trailing, OctoTheme.dimens.margin01) }
            .padding(.bottom, OctoTheme.dimens.margin01)
            .padding(.top, OctoTheme.dimens.margin1)
        }
        .frame(maxWidth: .infinity)
        .if(!partOfList) { $0.surface() }
        .if(partOfList) { $0.listRowInsets(EdgeInsets()) }
        
    }
}
      

struct SmallAnnouncement: View {
    var hasBackground:Bool = true
    var text: String
    var learnMoreButton: String? = nil
    var hideButton: String
    var onLearnMore: () -> Void = {}
    var onHideAnnouncement: () -> Void
    
    var body: some View {
        HStack(spacing: OctoTheme.dimens.margin1) {
            Text(text)
                .foregroundColor(OctoTheme.colors.darkText)
                .typographyBase()
                .frame(maxWidth: .infinity, alignment: .leading)
            
            HStack(spacing: -OctoTheme.dimens.margin1) {
                if let lm = learnMoreButton {
                    OctoButton(
                        text: LocalizedStringKey(lm),
                        type: .link,
                        small: true,
                        clickListener: onLearnMore
                    )
                }
                OctoButton(
                    text: LocalizedStringKey(hideButton),
                    type: .link,
                    small: true,
                    clickListener: onHideAnnouncement
                )
            }
            .padding(.trailing, OctoTheme.dimens.margin01)
            .layoutPriority(1)
        }
        .frame(maxWidth: .infinity)
        .if(hasBackground) { $0.padding([.leading, .bottom, .top], OctoTheme.dimens.margin2).surface() }
    }
}
        


struct SmallAnnouncement_Previews: PreviewProvider {
    static var previews: some View {
        LargeAnnouncement(title: "This is an announcement", text: "This is a description", hideButton: "Hide", onHideAnnouncement: {})
            .previewDisplayName("Large")
        LargeAnnouncement(title: "This is an announcement", text: "This is a description", learnMoreButton: "Learn more", hideButton: "Hide", onHideAnnouncement: {})
            .previewDisplayName("Large + Learn more")
        SmallAnnouncement(text: "This is a description dfdfhuihf uishufsu fhs", learnMoreButton: "Learn more", hideButton: "Hide", onHideAnnouncement: {})
            .previewDisplayName("Small + Learn more")
        SmallAnnouncement(text: "This is a description dsuhu sdhuifhsuifhsui hfuis", hideButton: "Hide", onHideAnnouncement: {})
            .previewDisplayName("Small")
    }
}
