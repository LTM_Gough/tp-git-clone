//
//  ScreenError.swift
//  OctoApp
//
//  Created by Christian on 20/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct ScreenError: View {
    @Environment(\.dismiss) var dismiss
    var title: String = "error_general"~
    var description: String = ""
    var error: KotlinThrowable? = nil
    var onRetry: (() -> Void)? = nil
    
    var body: some View {
        VStack(spacing: 0) {
            Text(title)
                .multilineTextAlignment(.center)
                .typographySubtitle()
            
            if !description.isEmpty {
                Text(description)
                    .multilineTextAlignment(.center)
                    .typographyLabel()
                    .padding(.top, OctoTheme.dimens.margin01)
            }
            
            HStack(spacing: OctoTheme.dimens.margin12) {
                if let throwable = error {
                    OctoButton(text: LocalizedStringKey("show_details"), type: .secondary, small: true) {
                        ExceptionReceivers.shared.dispatchException(throwable: throwable)
                    }
                }
                
                if let retry = onRetry {
                    OctoButton(text: LocalizedStringKey("try_again"), small: true, clickListener: retry)
                } else {
                    OctoButton(text: LocalizedStringKey("done"~), small: true, clickListener: { dismiss() })
                }
            }
            .padding(.top, OctoTheme.dimens.margin2)
        }
    }
}

struct ScreenError_Previews: PreviewProvider {
    static var previews: some View {
        ScreenError(description: "Description", error: KotlinThrowable()) {}
        ScreenError(description: "Description", error: KotlinThrowable())
        ScreenError(description: "Description")
    }
}
