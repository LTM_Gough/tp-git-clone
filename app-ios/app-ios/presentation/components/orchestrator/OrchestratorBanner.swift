//
//  OrchestratorBanner.swift
//  OctoApp
//
//  Created by Christian on 20/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct OrchestratorBanner<Content: View>: View {
    
    let content: () -> Content
    
    @State private var bannerHidden: Bool = false
    @State private var bannerShrunken: Bool = false
    @EnvironmentObject private var  viewModel: OrchestratorViewModel
    
    var body: some View {
        ZStack{
            content()
        }
        .onChange(of: viewModel.banner) { newData in
            runAnimation(data: newData)
        }
        .onAppear {
            runAnimation(data: viewModel.banner)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .animation(.easeInOut, value: viewModel.banner)
        .safeAreaInset(edge: .top) {
            VStack(spacing: 0) {
                if (!bannerHidden) {
                    HStack(spacing: OctoTheme.dimens.margin1) {
                        if (viewModel.banner.connectionType == nil) {
                            ProgressView()
                                .progressViewStyle(CircularProgressViewStyle(tint: OctoTheme.colors.textColoredBackground))
                        }
                        
                        if (!bannerShrunken) {
                            if let icon = getConnectionIcon() {
                                Image(icon)
                                    .padding([.trailing], OctoTheme.dimens.margin1)
                                    .frame(width: 16, height: 16)
                            }
                            Text(getConnectionTitle())
                                .foregroundColor(OctoTheme.colors.textColoredBackground)
                                .typographyBase()
                        }
                    }
                    .transition(.opacity.combined(with: .move(edge: .top)))
                    .padding([.top, .bottom], bannerShrunken ? 0 : OctoTheme.dimens.margin12)
                    .transition(.opacity.combined(with: .move(edge: .top)))
                    .frame(maxWidth: .infinity)
                    .background(getConnectionColor())
                }

                
                if let l = viewModel.banner.instanceLabel {
                    Text(l)
                        .foregroundColor(OctoTheme.colors.textColoredBackground)
                        .typographyBase()
                        .padding([.bottom], OctoTheme.dimens.margin01)
                        .padding([.top], bannerHidden ? 0 : OctoTheme.dimens.margin01)
                        .frame(maxWidth: .infinity)
                }
            }
            .background(viewModel.banner.instanceColor ?? Color.clear)
        }
    }
    
    private func runAnimation(data: OrchestratorBannerData) {
        withAnimation {
            bannerHidden = false
            bannerShrunken = false
        }
        
        Task {
            try await Task.sleep(nanoseconds: 3_000_000_000)
            withAnimation {
                if data.connectionType == .default_ {
                    bannerHidden = true
                } else if data.connectionType != nil {
                    bannerShrunken = true
                }
            }
        }
    }
    
    private func getConnectionTitle() -> String {
        if let connectionType = viewModel.banner.connectionType {
            switch(connectionType) {
            case .octoeverywhere: return "main___banner_connected_via_octoeverywhere"~
            case .spaghettidetective: return "main___banner_connected_via_spaghetti_detective"~
            case .ngrok: return "main___banner_connected_via_ngrok"~
            case .tailscale: return "main___banner_connected_via_tailscale"~
            case .defaultcloud: return "main___banner_connected_via_alternative"~
            default: return "main___banner_connected_via_local"~
            }
        } else {
            return "main___banner_connection_lost_reconnecting"~
        }
    }
    
    private func getConnectionIcon() -> String? {
        if let connectionType = viewModel.banner.connectionType {
            switch(connectionType) {
            case .octoeverywhere: return "octoEverywhereSmall"
            case .spaghettidetective: return "obicoSmall"
            case .ngrok: return "ngrokSmall"
            case .tailscale: return "tailscaleSmall"
            case .defaultcloud: return nil
            default: return nil
            }
        } else {
            return nil
        }
    }
    
    private func getConnectionColor() -> Color {
        if let connectionType = viewModel.banner.connectionType {
            switch(connectionType) {
            case .octoeverywhere: return OctoTheme.colors.externalOctoEverywhere
            case .spaghettidetective: return OctoTheme.colors.externalObico
            case .ngrok: return OctoTheme.colors.externalNgrok
            case .tailscale: return OctoTheme.colors.externalTailscale
            case .defaultcloud: return OctoTheme.colors.blue
            default: return OctoTheme.colors.green
            }
        } else {
            return OctoTheme.colors.yellow
        }
    }
}

struct OrchestratorBannerData : Equatable {
    let connectionType: ConnectionType?
    let instanceLabel: String?
    let instanceColor: Color?
}

struct BannerContainer_Previews: PreviewProvider {
    
    static var previews: some View {
//        OrchestratorBanner(data: OrchestratorBannerData(connectionType: nil, instanceLabel: "Some Printer", instanceColor: OctoTheme.colors.defaultColorScheme)) { Spacer() }
//            .previewDisplayName("Disconnected w Label")
//
//        OrchestratorBanner(data: OrchestratorBannerData(connectionType: nil, instanceLabel: nil, instanceColor: nil)) { Spacer() }
//            .previewDisplayName("Disconnected w/o Label")
//
//        OrchestratorBanner(data: OrchestratorBannerData(connectionType: .default_, instanceLabel: "Some Printer", instanceColor: OctoTheme.colors.orangeColorScheme)) { Spacer() }
//            .previewDisplayName("Default w Label")
//
//        OrchestratorBanner(data: OrchestratorBannerData(connectionType: .default_, instanceLabel: nil, instanceColor: nil)) { Spacer() }
//            .previewDisplayName("Default w/o Label")
//
//        OrchestratorBanner(data: OrchestratorBannerData(connectionType: .octoeverywhere, instanceLabel: nil, instanceColor: nil)) { Spacer() }
//            .previewDisplayName("OE w/o Label")
//
//        OrchestratorBanner(data: OrchestratorBannerData(connectionType: .spaghettidetective, instanceLabel: nil, instanceColor: nil)) { Spacer() }
//            .previewDisplayName("Obico w/o Label")
//
//        OrchestratorBanner(data: OrchestratorBannerData(connectionType: .ngrok, instanceLabel: nil, instanceColor: nil)) { Spacer() }
//            .previewDisplayName("Ngrok w/o Label")
//
//        OrchestratorBanner(data: OrchestratorBannerData(connectionType: .tailscale, instanceLabel: nil, instanceColor: nil)) { Spacer() }
//            .previewDisplayName("Tailscale w/o Label")
//
//        OrchestratorBanner(data: OrchestratorBannerData(connectionType: .defaultcloud, instanceLabel: "Some Printer", instanceColor: OctoTheme.colors.defaultColorScheme)) { Spacer() }
//            .previewDisplayName("Cloud w Label")
//
        Color.clear
    }
}
