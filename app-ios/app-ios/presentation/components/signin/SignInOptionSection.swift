//
//  SignInOptionSection.swift
//  app-ios
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct SignInOptionSection<Content: View> : View {
    
    var text: LocalizedStringKey
    let decoration : () -> Content
    
    init(text: LocalizedStringKey, @ViewBuilder decoration: @escaping () -> Content) {
        self.decoration = decoration
        self.text = text
    }
    
    var body: some View {
        let minDimen = 24 + OctoTheme.dimens.margin1 * 2

        HStack {
            Text(text).typographySubtitle()
            Spacer()
            ZStack {
                decoration()
                    .foregroundColor(OctoTheme.colors.normalText)
            }.frame(width: minDimen, height: minDimen)
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding([.top, .leading], OctoTheme.dimens.margin2)
        
    }
}


struct SignInOptionSection_Previews: PreviewProvider {
    static var previews: some View {
        SignInOptionSection(text: "Test") {
            ProgressView()
        }.previewDisplayName("Spinner")
            .background(.red)
        
        
        SignInOptionSection(text: "Test") {
            OctoIconButton(icon: "trash", clickListener: {})
        }.previewDisplayName("Icon")
            .background(.red)
        
        SignInOptionSection(text: "Test") {
          
        }.previewDisplayName("Nothing")
            .background(.red)
    }
}
