//
//  SignInHeader.swift
//  app-ios
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct SignInHeader: View {
    var state: SignInViewModelCore.State
    private let library = PrinterFindingDescriptionLibrary()

    var body: some View {
        let title = createTitle()
        let subtitle = createSubtitle()
        
        VStack {
            ZStack {
                Image("Octopus background")
                    .resizable()
                    .scaledToFit()
                    .frame(maxWidth: .infinity)
                    .scaleEffect(2)
                    .opacity(0.75)
                OctoAvatar(action: createAvatarAction())
            }
            .padding([.bottom], -OctoTheme.dimens.margin01)
            
            if (!title.isEmpty) {
                Text(LocalizedStringKey(title))
                    .typographyTitle()
                    .multilineTextAlignment(.center)
                    .padding([.bottom], OctoTheme.dimens.margin0)
                    .contentTransition(.opacity)
            }
            
            if (!subtitle.isEmpty) {
                Text(LocalizedStringKey(subtitle))
                    .typographyBase()
                    .multilineTextAlignment(.center)
                    .contentTransition(.opacity)
            }
            
            if(hasHelp()) {
                SignInNeedHelp()
            }
            
        }
        .padding([.leading, .trailing], OctoTheme.dimens.margin2)
        .animation(.default, value: subtitle + title)
        
    }
    
    func createTitle() -> String {
        if state is SignInViewModelCore.StateSplash {
            return "sign_in___discovery___welcome_title"
        } else if state is SignInViewModelCore.StateDiscover {
            return "sign_in___discovery___options_title"
        } else if let probe = state as? SignInViewModelCore.StateProbe {
            if let finding = probe.finding {
                return library.getTitleForFinding(finding: finding)
            } else {
               return "sign_in___probe___probing_active_title"
            }
        } else if state is SignInViewModelCore.StateAccessRequest {
            return "sign_in___access___confirm_in_web_interface"
        } else if state is SignInViewModelCore.StateSuccess {
            return "sign_in___success___title"
        } else if state is SignInViewModelCore.StateAccessRequestFailed {
            return "error_general"
        } else {
            return ""
        }
    }
    
    func createSubtitle() -> String {
        if state is SignInViewModelCore.StateSplash {
            return "sign_in___discovery___welcome_subtitle_searching"
        } else if state is SignInViewModelCore.StateDiscover {
            return ""
        } else if state is SignInViewModelCore.StateProbe {
            return ""
        } else if state is SignInViewModelCore.StateAccessRequest {
            return ""
        } else if state is SignInViewModelCore.StateSuccess {
            return "sign_in___success___subtile"
        } else {
            return ""
        }
    }
    
    func createAvatarAction() -> OctoAvatarAction {
        if state is SignInViewModelCore.StateSplash {
            return OctoAvatarAction.wave
        } else if state is SignInViewModelCore.StateInitial {
            return OctoAvatarAction.idle
        } else if let probe = state as? SignInViewModelCore.StateProbe {
            return probe.finding == nil ? OctoAvatarAction.swim : OctoAvatarAction.idle
        } else if state is SignInViewModelCore.StateSuccess {
            return OctoAvatarAction.party
        } else {
            return OctoAvatarAction.idle
        }
    }
    
    func hasHelp() -> Bool {
        if state is SignInViewModelCore.StateProbe && (state as? SignInViewModelCore.StateProbe)?.finding == nil {
            return false
        }
        
        return !(state is SignInViewModelCore.StateSplash) &&
        !(state is SignInViewModelCore.StateInitial) &&
        !(state is SignInViewModelCore.StateSuccess)
    }
}

struct SignInHeader_Previews: PreviewProvider {
    static var previews: some View {
        SignInHeader(
            state: SignInViewModelCore.StateSplash(
                until: Kotlinx_datetimeInstant.companion.DISTANT_FUTURE
            )
        )
        SignInHeader(
            state: SignInViewModelCore.StateDiscover(
                services: [],
                configs: [],
                hasQuickSwitch: true
            )
        )
        SignInHeader(
            state: SignInViewModelCore.StateProbe(
                urlString: "Someurl",
                apiKey: nil,
                reusedInstanceId: nil,
                finding: nil
            )
        )
    }
}
