//
//  SignInNeedHelp.swift
//  app-ios
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct SignInNeedHelp: View {
    @Environment(\.openURL) var openURL
    
    var body: some View {
        Button(
            action: {
                if let url = URL(string: UriLibrary.shared.getSignInHelpUri().description()) {
                    openURL(url)
                }
            },
            label: {
                Text("sign_in___discovery___need_help")
                    .typographyButtonLink()
            }
        )
        .buttonStyle(NeedHelpButtonStyle())
    }
}


private struct NeedHelpButtonStyle: ButtonStyle {
    
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .padding([.top, .bottom], OctoTheme.dimens.margin1)
            .padding([.leading, .trailing], OctoTheme.dimens.margin2)
            .background(configuration.isPressed ? OctoTheme.colors.accent.opacity(0.2) : .clear)
            .clipShape(Capsule())
    }
}


struct SignInNeedHelp_Previews: PreviewProvider {
    static var previews: some View {
        SignInNeedHelp()
    }
}
