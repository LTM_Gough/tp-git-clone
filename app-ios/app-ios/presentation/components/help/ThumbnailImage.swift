//
//  ThumbnailImage.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct ThumbnailImage: View {
    var url: URL?
    var showPlay: Bool = false
    private let overscan: CGFloat = 4
    
    var body: some View {
        AsyncImage(
            url: url,
            content: { image in
                GeometryReader { geo in
                    ZStack {
                        image
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: geo.size.width, height: geo.size.height + overscan)
                            .offset(y: -(overscan/2))
                            .if(showPlay) { $0.blur(radius: 2) }
                        
                        if (showPlay) {
                            Image(systemName: "play.fill")
                                .font(.system(size: 48))
                                .foregroundColor(.white)
                        }
                    }
                }
            },
            placeholder: {
                ProgressView()
            }
        )
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .aspectRatio(16/9, contentMode: .fit)
        .surface()
    }
}

struct ThumbnailImage_Previews: PreviewProvider {
    static var previews: some View {
        ThumbnailImage(url: URL(string: "https://i.ytimg.com/vi/71nX2FNGK6Q/hqdefault.jpg"))
            .frame(width: 300)
        
        ThumbnailImage(url: URL(string: ""))
            .frame(width: 300)
    }
}
