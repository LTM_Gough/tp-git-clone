//
//  ControlsHeader.swift
//  OctoApp
//
//  Created by Christian on 19/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct ControlsHeader: View {
    var state: ControlsState
    @Namespace private var animation
    private let transition = AnyTransition.asymmetric(insertion: .opacity, removal: .scale(scale: 0.6).combined(with: .opacity))
    
    var body: some View {
        HStack(spacing: 0) {
            if case .connect = state {
                Active(title: "workspace___connect"~, position: 1)
                    .transition(transition)
                Connector()

            } else {
                Inactive()
                    .transition(transition)
                Connector()
            }
                        
            if case .prepare = state {
                Active(title: "workspace___prepare"~, position: 2)
                    .transition(transition)
                Connector()

            } else {
                Inactive()
                    .transition(transition)
                Connector()
            }
                        
            if case .print(_) = state {
                Active(title: "workspace___print"~, position: 3)
                    .transition(transition)
            } else {
                Inactive()
                    .transition(transition)
            }
        }
        .frame(height: height)
        .padding(strokeWidth)
        .animation(.spring(), value: state)
    }
}

private struct Active: View {
    var title: String
    var position: Int
    @Environment(\.instanceColor) private var color
    
    var body: some View {
        HStack(spacing: 0) {
            ZStack{
                Text("\(position)")
                    .padding(OctoTheme.dimens.margin1)
                    .foregroundColor(OctoTheme.colors.textColoredBackground)
            }
            .frame(width:height, height:height)
            .background(Circle().fill(color.base))

            Text(title)
                .padding([.leading], OctoTheme.dimens.margin01)
                .padding([.trailing], OctoTheme.dimens.margin1)
                .frame(height: height)
                .typographyLabel()
        }
        .frame(height:height)
        .background(Capsule().strokeBorder(color.light, lineWidth: strokeWidth))
    }
}

private let height: CGFloat = 30
private let strokeWidth: CGFloat = 2

private struct Inactive: View {
    
    @Environment(\.instanceColor) private var color
    
    var body: some View {
        ZStack {
            Color.clear
                .background(Circle().fill(color.base))
            
            Color.clear
                .frame(width: 6, height: 6)
                .background(Circle().fill(OctoTheme.colors.textColoredBackground))
                
        }
        .frame(width:height, height:height)
    }
}

private struct Connector: View {
    
    @Environment(\.instanceColor) private var color
    
    var body: some View {
        color.light.frame(width: 10, height: strokeWidth)
    }
}

struct ControlsHeader_Previews: PreviewProvider {
    
    @State static var interactie: ControlsState = .connect
    @State static var color = Color.pink
    static let printInfo = PrintInformation(progress: 100, pausing: false, paused: false, cancelling: false)
    
    static var previews: some View {
        StatefulPreviewWrapper(ControlsState.connect){ value in
            ControlsHeader(state: value.wrappedValue)
                .onTapGesture {
                    switch(value.wrappedValue) {
                    case .initial: value.wrappedValue = .connect
                    case .connect: value.wrappedValue = .prepare
                    case .prepare: value.wrappedValue = .print(printInfo)
                    case .print: value.wrappedValue = .connect
                    }
                }
        }
        .previewDisplayName("Animated")
        
        ControlsHeader(state: ControlsState.connect)
            .previewDisplayName("Connect")
        ControlsHeader(state: ControlsState.prepare)
            .previewDisplayName("Prepare")
        ControlsHeader(state: ControlsState.print(printInfo))
            .previewDisplayName("Print")
    }
}

