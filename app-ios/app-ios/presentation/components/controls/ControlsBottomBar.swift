//
//  ControlsMenuBar.swift
//  OctoApp
//
//  Created by Christian on 19/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct ControlsBottomBar: View {
    
    var state: ControlsState
    var onTogglePause: () -> Void = {}
    var onCancel: () -> Void = {}
    var onOpenMenu: () -> Void = {}
    var onShowFiles: () -> Void = {}
    var onShowTemperatures: () -> Void = {}
    
    var body: some View {
        HStack {
            switch(state) {
            case .prepare: PrepareControls(onShowFiles: onShowFiles, onShowTemperatures: onShowTemperatures)
            case .print(let info): PrintControls(info: info, onTogglePause: onTogglePause, onCancel:onCancel, onOpenMenu: onOpenMenu)
            default: Spacer()
            }
            
            Spacer()
            
            HStack(spacing: OctoTheme.dimens.margin12) {
                if case .print(let printInformation) = state {
                    Text(printInformation.description())
                        .typographySubtitle()
                        .transition(.opacity)
                }
                
                OctoMenuButton(clickListener: onOpenMenu)
            }
            .offset(x: OctoTheme.dimens.margin2)
            .padding([.top, .bottom], OctoTheme.dimens.margin01)
            .animation(.spring(), value: state)
        }
        .padding(.leading, OctoTheme.dimens.margin2)
        .animation(.spring(), value: state)
    }
}

private var actionTransition = AnyTransition.asymmetric(
    insertion: .move(edge: .leading),
    removal: .move(edge: .trailing)
).combined(with: .opacity)

private struct Separator: View {
    
    var body: some View {
        OctoTheme.colors.inputBackground
            .frame(width: 1, height: OctoTheme.dimens.margin2)
            
    }
}

private struct PrepareControls: View {
    
    let onShowFiles: () -> Void
    let onShowTemperatures: () -> Void
    
    var body: some View {
        HStack {
            OctoButton(text: LocalizedStringKey("start_printing"), small: true, clickListener: onShowFiles)
                .transition(actionTransition)
                .padding(.trailing, OctoTheme.dimens.margin1)
            Separator()
            OctoIconButton(icon: "flame.fill", clickListener: onShowTemperatures)
                .transition(actionTransition)
        }
    }
}

private struct PrintControls: View {
    let info: PrintInformation
    
    @State private var confirmCancelShown = false
    @State private var confirmPauseShown = false
    @State private var confirmResumeShown = false
    
    let onTogglePause: () -> Void
    let onCancel: () -> Void
    let onOpenMenu: () -> Void
    
    var body: some View {
        HStack {
            if (!info.cancelling) {
                OctoIconButton(icon: "stop.fill") { confirmCancelShown = true }
                    .transition(actionTransition)
                    .alert("cancel_print_confirmation_message"~, isPresented: $confirmCancelShown) {
                        Button("cancel_print_confirmation_action"~, role: .destructive) {
                            confirmCancelShown = false
                            onCancel()
                        }
                        Button("resume_print_confirmation_action"~, role: .cancel) {
                            confirmCancelShown = false
                        }
                    }
                
                if(!info.pausing) {
                    Separator()
                        .transition(.opacity)
                }
            }
            
            if (info.paused) {
                OctoIconButton(icon: "play.fill") { confirmResumeShown = true}
                    .transition(actionTransition)
                    .alert("resume_print_confirmation_message"~, isPresented: $confirmResumeShown) {
                        Button("resume_print_confirmation_action"~, role: .destructive) {
                            confirmResumeShown = false
                            onTogglePause()
                        }
                        Button("cancel"~, role: .cancel) {
                            confirmResumeShown = false
                        }
                    }
            }
            
            if (!info.pausing && !info.cancelling && !info.paused) {
                OctoIconButton(icon: "pause.fill") { confirmPauseShown = true }
                    .transition(actionTransition)
                    .alert("pause_print_confirmation_message"~, isPresented: $confirmPauseShown) {
                        Button("pause_print_confirmation_action"~, role: .destructive) {
                            confirmPauseShown = false
                            onTogglePause()
                        }
                        Button("cancel"~, role: .cancel) {
                            confirmPauseShown = false
                        }
                    }
            }
        }
    }
}


private extension PrintInformation {
    func description() -> String {
        if pausing {
            return "pausing"~
        } else if paused {
            return "paused"~
        } else if cancelling {
            return "cancelling"~
        } else {
            return "\(progress)%"
        }
    }
}


struct ControlsMenuBar_Previews: PreviewProvider {
    static var previews: some View {
        StatefulPreviewWrapper(ControlsState.initial) { state in
            ControlsBottomBar(state: state.wrappedValue)
                .background(.black.opacity(0.05))
                .onTapGesture {
                    switch(state.wrappedValue) {
                    case .initial: state.wrappedValue = .connect
                    case .connect: state.wrappedValue = .prepare
                    case .prepare: state.wrappedValue = .print(PrintInformation(progress: 42, pausing: false, paused: false, cancelling: false))
                    case .print(_):  state.wrappedValue = .print(PrintInformation(progress: 42, pausing: false, paused: true, cancelling: false))
                    }
                }
        }
        .previewDisplayName("Dynamic")

        ControlsBottomBar(state: .connect)
            .background(.black.opacity(0.05))
            .previewDisplayName("Connect")
        
        ControlsBottomBar(state: .prepare)
            .background(.black.opacity(0.05))
            .previewDisplayName("Prepare")
        
        ControlsBottomBar(state: .print(PrintInformation(progress: 42, pausing: false, paused: false, cancelling: false)))
            .background(.black.opacity(0.05))
            .previewDisplayName("Printing")
        
        ControlsBottomBar(state: .print(PrintInformation(progress: 42, pausing: true, paused: false, cancelling: false)))
            .background(.black.opacity(0.05))
            .previewDisplayName("Pausing")
        
        ControlsBottomBar(state: .print(PrintInformation(progress: 42, pausing: false, paused: true, cancelling: false)))
            .background(.black.opacity(0.05))
            .previewDisplayName("Paused")
        
        ControlsBottomBar(state: .print(PrintInformation(progress: 42, pausing: false, paused: false, cancelling: true)))
            .background(.black.opacity(0.05))
            .previewDisplayName("Cancelling")
    }
}
