//
//  ControlsScaffold.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct ControlsScaffold<Control: View>: View {
    
    var title: String
    var icon: String? = "slider.horizontal.3"
    var control: () -> Control
    
    var body: some View {
        VStack(spacing: 0) {
            HStack(spacing: OctoTheme.dimens.margin2) {
                Text(title)
                    .typographySectionHeader()
                    .padding([.leading], OctoTheme.dimens.margin2)
                    .padding([.top, .bottom], OctoTheme.dimens.margin1)
                
                Spacer()
                
                if let icon = icon {
                    OctoIconButton(icon: icon) {}
                        .padding([.trailing], OctoTheme.dimens.margin1)
                }
            }
            .frame(maxWidth: .infinity)
            
            control()
        }
        .padding([.leading, .trailing], OctoTheme.dimens.margin2)
        .padding([.top, .bottom], OctoTheme.dimens.margin1)
        .frame(maxWidth: .infinity)
    }
}

struct ControlsScaffold_Previews: PreviewProvider {
    static var previews: some View {
        ControlsScaffold(title: "Control title", icon: "slider.horizontal.3") {
            ZStack {
                
            }
            .frame(maxWidth: .infinity)
            .frame(height: 100)
            .background(OctoTheme.colors.inputBackground)
            .cornerRadius(OctoTheme.dimens.cornerRadius)
        }
        .previewDisplayName("Icon")
        
        ControlsScaffold(title: "Control title") {
            ZStack {
                
            }
            .frame(maxWidth: .infinity)
            .frame(height: 100)
            .background(OctoTheme.colors.inputBackground)
            .cornerRadius(OctoTheme.dimens.cornerRadius)
        }
        .previewDisplayName("No icon")
    }
}
