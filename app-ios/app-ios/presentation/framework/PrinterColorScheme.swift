//
//  OctoColorScheme.swift
//  OctoApp
//
//  Created by Christian on 19/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import SwiftUI

struct PrinterColorScheme {
    var light: Color
    var base: Color
    
    init(colorName: String?) {
        switch(colorName) {
            
        case "red": do {
            light = OctoTheme.colors.redColorSchemeLight
            base = OctoTheme.colors.redColorScheme
        }
            
        case "orange": do {
            light = OctoTheme.colors.orangeColorSchemeLight
            base = OctoTheme.colors.orangeColorScheme
        }
            
        case "yellow": do {
            light = OctoTheme.colors.yellowColorSchemeLight
            base = OctoTheme.colors.yellowColorScheme
        }
            
        case "green": do {
            light = OctoTheme.colors.greenColorScremeLight
            base = OctoTheme.colors.greenColorScreme
        }
            
        case "blue": do {
            light = OctoTheme.colors.blueColorSchemeLight
            base = OctoTheme.colors.blueColorScheme
        }
            
        case "violet": do {
            light = OctoTheme.colors.violetColorSchemeLight
            base = OctoTheme.colors.violetColorScheme
        }
            
        case "white": do {
            light = OctoTheme.colors.whiteColorSchemeLight
            base = OctoTheme.colors.whiteColorScheme
        }
            
        case "black": do {
            light = OctoTheme.colors.blackColorSchemeLight
            base = OctoTheme.colors.blackColorScheme
        }
            
        default: do {
            light = OctoTheme.colors.defaultColorSchemeLight
            base = OctoTheme.colors.defaultColorScheme
        }
            
        }
    }
}
