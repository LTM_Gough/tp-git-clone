//
//  Dimensions.swift
//  app-ios
//
//  Created by Christian on 09/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import SwiftUI

struct OctoTheme {
    static let dimens = OctoDimens()
    static let colors = OctoColors()
    static let typography = OctoTypography()
}

struct OctoDimens {
    let cornerRadius: CGFloat = 20
    let cornerRadiusSmall: CGFloat = 5
    
    let margin0: CGFloat = 2
    let margin01: CGFloat = 5
    let margin1: CGFloat = 10
    let margin12: CGFloat = 15
    let margin2: CGFloat = 20
    let margin3: CGFloat = 40
    let margin4: CGFloat = 60
    let margin5: CGFloat = 90
    let margin6: CGFloat = 130
    
    let buttonActiveScale = 1.01
}

struct OctoColors {
    let primary = Color("primary")
    let accent = Color("blue")
    
    let inputBackground = Color("inputBackground")
    let inputBackgroundAlternative = Color("inputBackgroundAlternative")
    let darkText = Color("darkText")
    let normalText = Color("normalText")
    let lightText = Color("lightText")
    let clickIndicatorOverlay = Color("clickIndicatorOverlay")
    let textColoredBackground = Color("white")
    let destructive = Color("red")
    let windowBackground = Color("windowBackground")
  
    let hot = Color("hot")
    let cold = Color("inputBackground")
    
    let white = Color("white")
    let lightGrey = Color("lightGrey")
    let darkGrey = Color("darkGrey")
    let blue = Color("blue")
    let blueTranslucent = Color("blueTranslucent")
    let green = Color("green")
    let greenTranslucent = Color("greenTranslucent")
    let yellow = Color("yellow")
    let yellowTranslucent = Color("yellowTranslucent")
    let red = Color("red")
    let orange = Color("orange")
    let redTranslucent = Color("redTranslucent")
    
    let externalOctoEverywhere = Color("externalOctoEverywhere")
    let externalOctoEverywhere2 = Color("externalOctoEverywhere2")
    let externalOctoEverywhere3 = Color("externalOctoEverywhere3")
    let externalObico = Color("externalObico")
    let externalObico2 = Color("externalObico2")
    let externalNgrok = Color("externalNgrok")
    let externalNgrok2 = Color("externalNgrok2")
    let externalTailscale = Color("externalTailscale")
    
    let defaultColorScheme = Color("defaultColorScheme")
    let defaultColorSchemeLight = Color("defaultColorSchemeLight")
    let redColorScheme = Color("redColorScheme")
    let redColorSchemeLight = Color("redColorSchemeLight")
    let orangeColorScheme = Color("orangeColorScheme")
    let orangeColorSchemeLight = Color("orangeColorSchemeLight")
    let yellowColorScheme = Color("yellowColorScheme")
    let yellowColorSchemeLight = Color("yellowColorSchemeLight")
    let greenColorScreme = Color("greenColorScreme")
    let greenColorScremeLight = Color("greenColorScremeLight")
    let blueColorScheme = Color("blueColorScheme")
    let blueColorSchemeLight = Color("blueColorSchemeLight")
    let violetColorScheme = Color("violetColorScheme")
    let violetColorSchemeLight = Color("violetColorSchemeLight")
    let whiteColorScheme = Color("whiteColorScheme")
    let whiteColorSchemeLight = Color("whiteColorSchemeLight")
    let blackColorScheme = Color("blackColorScheme")
    let blackColorSchemeLight = Color("blackColorSchemeLight")
    
    let menuStylePrinterForeground = Color("blue")
    let menuStylePrinterBackground = Color("blueTranslucent")
    let menuStyleOctoPrintForeground = Color("green")
    let menuStyleOctoPrintBackground = Color("greenTranslucent")
    let menuStyleSettingsForeground = Color("yellow")
    let menuStyleSettingsBackground = Color("yellowTranslucent")
    let menuStyleSupportForeground = Color("red")
    let menuStyleSupportBackground = Color("redTranslucent")
    let menuStyleNeutralForeground = Color("lightGrey")
    let menuStyleNeutralBackground = Color("lightGreyTranslucent")
}

struct OctoTypography {
    let base = Font.custom("Roboto-Regular", size: 16)
    let label = Font.custom("Roboto-Light", size: 14)
    let labelSmall = Font.custom("Roboto-Regular", size: 13)
    let focus = Font.custom("Roboto-Medium", size: 14)
    let input = Font.custom("Ubuntu-Light", size: 20)
    let data = Font.custom("Ubuntu-Light", size: 20)
    let dataLarge = Font.custom("Ubuntu-Light", size: 30)
    let title = Font.custom("Ubuntu-Regular", size: 26)
    let titleBig = Font.custom("Ubuntu-Regular", size: 32)
    let titleLarge = Font.custom("Ubuntu-Regular", size: 40)
    let subtitle = Font.custom("Ubuntu-Light", size: 16)
    let button = Font.custom("Ubuntu-Light", size: 22)
    let buttonSmall = Font.custom("Ubuntu-Light", size: 20)
    let buttonLink = Font.custom("Roboto-Medium", size: 16)
    let sectionHeader = Font.custom("Roboto-Medium", size: 16)
}

func colorFromAppearanceName(name: String) -> Color {
    switch name {
    case "red": return OctoTheme.colors.redColorScheme
    case "orange": return OctoTheme.colors.orangeColorScheme
    case "yellow": return OctoTheme.colors.yellowColorScheme
    case "green": return OctoTheme.colors.greenColorScreme
    case "blue": return OctoTheme.colors.blueColorScheme
    case "violet": return OctoTheme.colors.violetColorScheme
    case "white": return OctoTheme.colors.whiteColorScheme
    case "black": return OctoTheme.colors.blackColorScheme
    default: return OctoTheme.colors.defaultColorScheme
    }
}
