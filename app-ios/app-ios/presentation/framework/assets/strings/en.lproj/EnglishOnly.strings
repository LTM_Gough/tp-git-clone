/* 
  EnglishOnly.strings
  app-ios

  Created by Christian on 14/10/2022.
  Copyright © 2022 orgName. All rights reserved.
*/


/*Privacy menu*/
"privacy_menu___title" = "Privacy";
"privacy_menu___subtitle" = "OctoApp does not collect any personalized information. I do not have any data about you, such as your email address. Even if you have an active subscription, you are completely anonymous.";
"privacy_menu___bottom_text" = "<![CDATA[All data collected is in the app\'s legitimate interest. If you have any questions, please reach out to <a href=\"mailto\">hello@octoapp.eu</a><br><br><a href=\"https://octoapp-4e438.web.app/privacy\">Data privacy statement</a><br><br>OctoApp is an <a href=\"https://gitlab.com/crysxd/octoapp\">open source</a> project.]]>";
"privacy_menu___crash_reporting_title" = "Anonymous crash reporting";
"privacy_menu___crash_reporting_description" = "If enabled, OctoApp sends automated crash reports to Firebase Crashlytics, helping me make the app stable for all users and devices. These reports are anonymous and deleted after 90 days. They contain generic information about your device, such as the model or iOS version and the most recent logs collected by the app. Crash reports do not contain any sensitive data as all API keys, host names or Basic Auth credentials are scrubbed (best effort) when any error is created and again before the logs are cached.";
"privacy_menu___analytics_title" = "Anonymous usage statistics";
"privacy_menu___analytics_description" = "If enabled, OctoApp sends anonymous usage information to Firebase Analytics which will be deleted after 90 days. This information helps me in determining which features of the app are used the most, and where I should spend time on improving to benefit users. This information does not contain any data except which features of the app are used and basic information about your setup, such as which plugins are installed or the OctoPrint version. The data is not linked to any advertisement profile or anything similar. If you reinstall the app or switch your device, I do not have any means to connect your data.";

/*Discover*/
"sign_in___discovery___octoeverywhere_error_comply" = "Connect local first";
"sign_in___discovery___octoeverywhere_error_ignore" = "Connect OctoEverywhere now";
"sign_in___discovery___octoeverywhere_error_message" = "To get the most out of OctoApp and OctoEverywhere, connect your printer using the local web URL first. Once everything is set up, you can log in with your OctoEverywhere account.";
"sign_in___discovery___octoeverywhere_error_message_addition_shared_connection" = "You need to create a shared connection on the OctoEverywhere webpage, normal OctoEverywhere URLs can\'t be used.\n\nTo get the most out of OctoApp and OctoEverywhere, connect your printer using the local web URL first. Once everything is set up, you can log in with your OctoEverywhere account.";
"sign_in___discovery___ngrok_error_message" = "You can\'t use an ngrok URL to sign in as this can have unintended side effects. Please connect your OctoPrint using the local URL first, OctoApp will then configure ngrok automatically.";

/*Probe*/
"sign_in___probe_finding___title_basic_auth" = "Basic Authentication required";
"sign_in___probe_finding___explainer_basic_auth" = "Please enter your Basic Auth credentials below, not your OctoPrint user and password.";
"sign_in___probe_finding___title_dns_failure" = "Unable to resolve **%s**";
"sign_in___probe_finding___explainer_dns_failure" = "iOS is not able to resolve the IP address for **%1$s**. Check following things to resolve the issue:\n\n- Ensure OctoApp is allowed to use the local network in [iOS settings](app-settings:)\n- Ensure there is no typo\n - Make sure you are connected to the internet\n- Make sure you are on the correct WiFi network\n- Try to open [%2$s](%2$s) in your browser\n- Try using the **IP address** instead of **%1$s**";
"sign_in___probe_finding___title_local_dns_failure" = "Unable to resolve **%s**";
"sign_in___probe_finding___explainer_local_dns_failure" = "OctoApp is not able to resolve the IP address for **%1$s**. Check following things to resolve the issue:\n\n- Ensure OctoApp is allowed to use the local network in [iOS settings](app-settings:)\n- Ensure there is no typo\n- Make sure you are on the correct WiFi network\n- Try to open [%2$s](%2$s) in your browser\n- Try using the **IP address** instead of **%1$s**. You can use the [ipOnConnect plugin](https://plugins.octoprint.org/plugins/ipOnConnect/) to see your OctoPrint\'s IP address on your printer\'s display.";
"sign_in___probe_finding___title_host_unreachable" = "Unable to connect to **%s**";
"sign_in___probe_finding___explainer_host_unreachable" = "OctoApp resolved the IP of **%1$s**, but **%2$s** can\'t be reached within 2 seconds. Check following things to resolve the issue:\n\n- Make sure the machine hosting the server is turned on and connected\n- If your server is only available locally, make sure you are connected to the correct WiFi network\n- Ensure OctoApp is allowed to use the local network in [iOS settings](app-settings:)\n- Try to open [%3$s](%3$s) in your browser";
"sign_in___probe_finding___title_https_not_trusted" = "iOS does not trust **%s**";
"sign_in___probe_finding___explainer_https_not_trusted_weak_hostname_verification" = "The SSL certificate presented by your OctoPrint can not be used to establish a HTTPS connection because iOS does not trust the certificate. This is a common issue for self-signed certificates that were not installed on this phone.\n\nAdditionally, the certificate uses a deprecated format that does not comply to modern security standards. A \"subject alternative name\" (SAN) field is required from iOS 9 onwards.\n\nOctoApp can add this certificate to a trusted list and will bypass iOS’s check for this certificate.";
"sign_in___probe_finding___explainer_https_not_trusted" = "The SSL certificate presented by your OctoPrint can not be used to establish a HTTPS connection because iOS does not trust the certificate. This is a common issue for self-signed certificates that were not installed on this phone.\n\nOctoApp can add this certificate to a trusted list and will bypass iOS’s check for this certificate.";
"sign_in___probe_finding___explainer_https_not_trusted_no_cert" = "OctoApp was not able to communicate with **%s** over HTTPS. This indicates an incorrectly configured server as OctoApp also failed to extract the certificate from your server. Make sure whether you should use **HTTP instead of HTTP\_S\_!**";
"sign_in___probe_finding___title_url_syntax" = "URL syntax error";
"sign_in___probe_finding___explainer_url_syntax" = "The URL **%1$s** seems to contain a syntax error and can\'t be parsed. iOS reports following error:\n\n**%2$s**";
"sign_in___probe_finding___title_octoprint_not_found" = "OctoPrint not found";
"sign_in___probe_finding___explainer_octoprint_not_found" = "OctoApp was able to connect to **%1$s** but received a 404 response. Check following things to resolve the issue:\n\n- Make sure you use the correct port and host\n\n- Make sure that you use the correct path in the URL\n\n- Make sure you have a recent version of OctoPrint, OctoApp is not compatible with very old versions (e.g. **PrusaPrint OctoPrint**)\n\n- Try to open [%1$s](**%1$s**) in your browser";
"sign_in___probe_finding___title_port_closed" = "Connected to **%1$s** but port **%2$d** is closed";
"sign_in___probe_finding___explainer_port_closed" = "OctoApp was able to connect to **%1$s** but port **%2$d** is closed. Check following things to resolve the issue:\n\n- Make sure **%2$d** is correct. If you don\'t specify the port explicitly, OctoApp will use 80 for HTTP and 443 for HTTPS\n- Ensure OctoApp is allowed to use the local network in [iOS settings](app-settings:)\n- Try to open [%3$s](%3$s) in your browser";
"sign_in___probe_finding___title_failed_to_connect_via_http" = "Failed to connect to **%s** via HTTP";
"sign_in___probe_finding___explainer_failed_to_connect_via_http" = "OctoApp was able to communicate with **%1$s**, but when trying to establish a HTTP(S) connection an unexpected error occurred. iOS reports following issue:\n\n**%2$s**\n\nYou can **long-press \"Need help?\"** above to get support (please include logs).";
"sign_in___probe_finding___title_unexpected_issue" = "Unexpected issue";
"sign_in___probe_finding___explainer_unexpected_issue" = "OctoApp encountered an unexpected error. iOS reports following issue:\n\n**%1$s**\n\nYou can **long-press \"Need help?\"** above to get support (please include logs).";
"sign_in___probe_finding___title_might_not_be_octoprint" = "**%s** might not be an OctoPrint";
"sign_in___probe_finding___explainer_might_not_be_octoprint" = "OctoApp was able to communicate with **%1$s**, but the server seems not to be a recent version of OctoPrint.\n\nYou can continue, but other issues may arise in the following steps.";
"sign_in___probe_finding___title_websocket_upgrade_failed" = "HTTP access to **%s** works, but web socket broken";
"sign_in___probe_finding___explainer_websocket_upgrade_failed" = "OctoApp can communicate with **%1$s**, but the web socket failed to connect with response code **%2$d**. This is a very common issue with incorrectly configured reverse proxy setups. Check following things to resolve the issue:\n\n- Make sure your proxy sets the `Upgrade: WebSocket` header for `%3$s` as it is not forwarded to OctoPrint by default\n- Refer to the [configuration examples in the OctoPrint community](https://community.octoprint.org/t/reverse-proxy-configuration-examples/1107) for Nginx, HAProxy, Apache and others";

/*Webcam test*/
"help___webcam_troubleshooting___title_basic_auth" = "Basic Authentication required";
"help___webcam_troubleshooting___explainer_basic_auth" = "You need to add the Basic Authentication credentials to the webcam stream URL in your OctoPrint settings (webinterface). Use this URL and fill in your username and password:\n\n%1$s";
"help___webcam_troubleshooting___title_dns_failure" = "@string/sign_in___probe_finding___title_dns_failure";
"help___webcam_troubleshooting___explainer_dns_failure" = "@string/sign_in___probe_finding___explainer_dns_failure";
"help___webcam_troubleshooting___title_local_dns_failure" = "@string/sign_in___probe_finding___title_local_dns_failure";
"help___webcam_troubleshooting___explainer_local_dns_failure" = "@string/sign_in___probe_finding___explainer_local_dns_failure";
"help___webcam_troubleshooting___title_host_unreachable" = "@string/sign_in___probe_finding___title_host_unreachable";
"help___webcam_troubleshooting___explainer_host_unreachable" = "@string/sign_in___probe_finding___explainer_host_unreachable";
"help___webcam_troubleshooting___title_https_not_trusted" = "@string/sign_in___probe_finding___title_https_not_trusted";
"help___webcam_troubleshooting___explainer_https_not_trusted_weak_hostname_verification" = "@string/sign_in___probe_finding___explainer_https_not_trusted_weak_hostname_verification";
"help___webcam_troubleshooting___explainer_https_not_trusted" = "@string/sign_in___probe_finding___explainer_https_not_trusted";
"help___webcam_troubleshooting___title_url_syntax" = "@string/sign_in___probe_finding___title_url_syntax";
"help___webcam_troubleshooting___explainer_url_syntax" = "@string/sign_in___probe_finding___explainer_url_syntax";
"help___webcam_troubleshooting___title_webcam_not_found" = "Webcam not found";
"help___webcam_troubleshooting___explainer_webcam_not_found" = "OctoApp was able to connect to **%1$s** but received a 404 response. Check following things to resolve the issue:\n\n- Make sure you use the correct port and host\n\n- Make sure that you use the correct path in the URL. If the stream URL you entered in the OctoPrint settings starts with `/`, OctoApp will treat the stream URL as a absolute path of OctoPrint host. If the stream URL you entered starts with `http`, OctoApp will treat the stream URL as a complete URL and not modify it. In any other case, OctoApp treats the stream URL as a relative path which is appended to your OctoPrint\'s web url.\n- If you try to reach your webcam from outside your home and you configured remote access with OctoApp, be aware that you need to host the webcam on the same host as OctoPrint or you need to expose your webcam separatly to the internet and use a absolute URL as stream URL\n- Try to open [%1$s](**%1$s**) in your browser";
"help___webcam_troubleshooting___title_port_closed" = "@string/sign_in___probe_finding___title_port_closed";
"help___webcam_troubleshooting___explainer_port_closed" = "@string/sign_in___probe_finding___explainer_port_closed";
"help___webcam_troubleshooting___title_failed_to_connect_via_http" = "@string/sign_in___probe_finding___title_failed_to_connect_via_http";
"help___webcam_troubleshooting___explainer_failed_to_connect_via_http" = "@string/sign_in___probe_finding___explainer_failed_to_connect_via_http";
"help___webcam_troubleshooting___title_unexpected_issue" = "@string/sign_in___probe_finding___title_unexpected_issue";
"help___webcam_troubleshooting___explainer_unexpected_issue" = "@string/sign_in___probe_finding___explainer_unexpected_issue";
"help___webcam_troubleshooting___title_might_not_be_webcam" = "**%s** might not be an webcam";
"help___webcam_troubleshooting___explainer_might_not_be_webcam" = "OctoApp was able to communicate with **%1$s**, but the server does not seem to be a webcam as the connection was instantly closed or no image was received.\n\nIf you are sure that **%2$s** is the correct host, make sure that **%1$s** is the correct path and port. If not specified, OctoApp will use port 80 for HTTP and port 443 for HTTPS.";
"help___webcam_troubleshooting___continue" = "Continue";
"help___webcam_troubleshooting___try_again" = "Try again";
"help___webcam_troubleshooting___trust_and_try_again" = "Trust server";
"help___webcam_troubleshooting___only_mjpeg_supported" = "At the moment troubleshooting is only available for MJPEG webcams";
"help___webcam_troubleshooting___loading_title" = "Testing your webcam…";
"help___webcam_troubleshooting___help" = "Need Help?";
"help___webcam_troubleshooting___title_webcam_is_working" = "Webcam is working";
"help___webcam_troubleshooting___explainer_webcam_is_working" = "OctoApp was able to load images with %.1f FPS from your webcam:";
"help___webcam_troubleshooting___explainer_data_saver_not_working" = "OctoApp is unable to use the data saver webcam mode because of following error:";
"help___webcam_troubleshooting___explainer_data_saver_working" = "<![CDATA[OctoApp was also able to use the data saver mode with images %1$s with a resolution of <b>%2$d x %3$d px</b>:]]>";
"help___webcam_troubleshooting___explainer_data_saver_source_companion" = "<![CDATA[from the <b>OctoApp Companion plugin</b>]]>";
"help___webcam_troubleshooting___explainer_data_saver_source_snapshot" = "<![CDATA[from <a href=\"%1$s\">%1$s</a>]]>";
"help___webcam_troubleshooting___explainer_data_saver_source_mjpeg" = "<![CDATA[from single frames of <a href=\"%1$s\">%1$s</a>]]>";
/*Help*/
"help___status_x" = "Status: %s";
"help___octoprint_community" = "OctoPrint community";
"help___octoprint_discord" = "OctoPrint Discord";
"help___report_a_bug" = "I want to report a bug";
"help___ask_a_question" = "I have an other question";
"help___title" = "<![CDATA[FAQ & Help]]>";
"help___introduction_part_2" = "If you are new to OctoApp, you can check out this introduction to get started with the app’s concept!";
"help___introduction_part_1" = "Hi! I’m Chris, the developer of OctoApp! Thanks for using the app. If you don’t find what you need below, you can always reach out! 🖖";
"help___introduction_video_caption" = "1 minute introduction";
"help___faq_title" = "FAQ";
"help___bugs_title" = "Known bugs in this version";
"help___contact_title" = "Still no answers?";
"help___contact_detail_information" = "I speak English and German. My current local time is %1$s (%2$s).";

/*OctoLab*/
"lab_menu___title" = "OctoApp Lab";
"lab_menu___subtitle" = "Here you can find experimental settings and things I do not officially support but which you can use at your own will. Features might be removed without prior notice.";
"lab_menu___allow_battery_saver_title" = "Notification battery saver";
"lab_menu___changelog_title" = "Changelog";
"lab_menu___allow_battery_saver_description" = "With version 1.8, I introduced a battery saving mechanic for the print notification as it was consuming a lot of power on some devices. You can disable it to return to the old behaviour. This might resolve issues with missing filament change notifications.";
"lab_menu___allow_to_rotate_title" = "Allow OctoApp to rotate";
"lab_menu___allow_to_rotate_description" = "OctoApp can rotate into landscape mode. The UI is not optimized and rotating might cause issues on some screens. Will take affect after restart.";
"lab_menu___suppress_m115_request_title" = "Suppress M115 request";
"lab_menu___suppress_m115_request_description" = "By default OctoApp requests M115 when a printer is connected or the app is opened with a connected printer. This is used to style the Gcode preview for your printer. Suppressing the M115 will result in a generic print bed to be used.";
"lab_menu___allow_terminal_during_print_title" = "Allow terminal during print";
"lab_menu___allow_terminal_during_print_description" = "Usually OctoApp does not allow you to send Gcode commands from the terminal if a print is active. Enabling this setting bypasses the check. Be cautious.";
"lab_menu___suppress_remote_notification_init" = "Suppress remote notifications";
"lab_menu___suppress_remote_notification_init_description" = "On some devices with after-market Google Play services OctoApp can crash while setting up remote notifications. This options suppresses remote notifications from being initialized, thus preventing the crash.";
"lab_menu___debug_network_logging" = "Debug network logging";
"lab_menu___debug_network_logging_description" = "<![CDATA[
    Enabled extended logging around network requests and DNS. This will also <b>remove the obfuscation of IP addresses and hostnames and may leak credentials</b> in logs and crash reports. Only enable this when asked to, takes affect after restart.
]]>";
"lab_menu___enforce_ip_v4" = "Enforce IPv4";
"lab_menu___enforce_ip_v4_description" = "Suppress the use of IPv6 addresses. If no IPv4 for a domain can be resolved, a connection won\'t be possible. Applies immediately.";
"lab_menu___record_webcam_traffic_for_debug" = "Record webcam traffic";
"lab_menu___record_webcam_traffic_for_debug_description" = "Records 5MiB of raw network traffic for debug purposes of the MJPEG connection and opens a share dialog. This happens when the webcam is shown next or right away if already shown.";
"lab_menu___compact_layout" = "Compact layout";
"lab_menu___compact_layout_description" = "Removes the titles in the prepare and print workspaces to save space. Applies after restart.";

/*Play services*/
"announcement_play_services_error___text" = "Remote notifications disabled because Play Services crashed";
"announcement_play_services_error___learn_more" = "Learn more";

/*Remote access*/
"configure_remote_acces___manual___error_normal_octoeverywhere_url" = "You can\'t use a normal OctoEverywhere URL. Please create a shared connection URL on octoeverywhere.com or directly sign in with OctoEverywhere (preferred)";
"configure_remote_acces___manual___error_shared_octoeverywhere_url" = "It\'s better to directly sign in with OctoEverywhere than using a shared connection! You can proceed if you want.";
"configure_remote_acces___manual___error_unable_to_verify" = "Unable to verify that the remote URL points to the same instance";
"configure_remote_acces___manual___error_unable_to_connect" = "Unable to connect";
"configure_remote_acces___manual___error_invalid_url" = "Please provide a valid URL (including http:// or https://)";
"plugin_library___octoapp_tutorial" = "OctoApp Tutorial";
"plugin_library___plugin_page" = "Plugin page";
"plugin_library___title" = "Supported Plugins";
"plugin_library___description" = "OctoApp supports many OctoPrint plugins. You can use plugins to customize OctoApp and add more functionality to it.";
"configure_remote_acces___spaghetti_detective___custom_button" = "Use custom instance";
"configure_remote_acces___spaghetti_detective___custom_title" = "Use custom TSD";
"configure_remote_acces___spaghetti_detective___custom_action" = "Connect";
"configure_remote_acces___spaghetti_detective___custom_hint" = "Base URL";
"configure_remote_acces___spaghetti_detective___custom_error" = "Enter a valid HTTP(S) URL";

/*Misc*/
"print_confidence_disclaimer_octoeverywhere" = "Gadget by\nOctoEverywhere";
