//
//  HelpItemView.swift
//  OctoApp
//
//  Created by Christian on 26/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase
import MarkdownUI

struct HelpItemView: View {
    var id: String
    @StateObject private var viewModel = HelpItemViewModel()
    
    var body: some View {
        ScrollView {
            VStack {
                let (faq, knownBug) = viewModel.content(id: id)
                if let f = faq {
                    FaqView(faq: f)
                } else if let k = knownBug {
                    KnownBugView(knownBug: k)
                } else {
                    ScreenError(title: "help___content_not_available"~)
                }
            }
        }
    }
}

private struct FaqView: View {
    var faq: FaqStruct
    @Environment(\.openURL) private var openUrl
    
    var body: some View {
        
        VStack(spacing: OctoTheme.dimens.margin1) {
            if let url = URL(string: faq.youtubeUrl ?? "") {
                if let thumb = URL(string: faq.youtubeThumbnailUrl ?? "") {
                    Button(action: { openUrl(url) }) {
                        ThumbnailImage(url: thumb, showPlay: true)
                    }
                }
            }
            
            if let s = faq.content {
                Markdown(s)
                    .frame(maxWidth: .infinity, alignment: .leading)
            }
            
            Spacer()
        }
        .padding(OctoTheme.dimens.margin12)
        .navigationTitle(faq.title ?? "")
    }
}

private struct KnownBugView: View {
    var knownBug: KnownBugStruct
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin1) {
            if let s = knownBug.status {
                Text(String(format: "help___status_x"~, s))
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .typographyTitle()
            }
            
            if let s = knownBug.content {
                Markdown(s)
                    .frame(maxWidth: .infinity, alignment: .leading)
            }
            Spacer()
        }
        .padding(OctoTheme.dimens.margin12)
        .navigationTitle(knownBug.title ?? "")
    }
}

private class HelpItemViewModel : ObservableObject {
    
    private var core = HelpLauncherViewModelCore()
    
    private var cache: (FaqStruct?, KnownBugStruct?)? = nil
    func content(id: String) -> (FaqStruct?, KnownBugStruct?) {
        if let c = cache {
            return c
        }
        
        let f = core.faq.filter { $0.id == id}
        let k = core.knownBugs.filter { $0.id == id}
        
        if !f.isEmpty {
            let faq = f[0].toStruct()
            cache = (faq, nil)
            return (faq, nil)
        } else if !k.isEmpty {
            let knownBug = k[0].toStruct()
            cache = (nil, knownBug)
            return (nil, knownBug)
        } else {
            cache = (nil, nil)
            return (nil, nil)
        }
    }
}

struct HelpItemView_Previews: PreviewProvider {
    static var previews: some View {
        FaqView(faq: FaqStruct(
            id: "",
            hidden: false,
            title: "",
            content: "Some content ",
            youtubeUrl: "http://someurl.com",
            youtubeThumbnailUrl: "https://img.youtube.com/vi/71nX2FNGK6Q/hqdefault.jpg")
        )
        .padding(20)
        .previewDisplayName("FAQ")
        
        KnownBugView(knownBug: KnownBugStruct(
            id: "",
            title: "",
            status: "Investigating",
            content: "Some content")
        )
        .previewDisplayName("Known Bug")
    }
}
