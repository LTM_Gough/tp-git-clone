//
//  HelpTutorials.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct HelpTutorialsView: View {
    @StateObject private var viewModel = HelpTutorialsViewModel()
    
    var body: some View {
        VStack {
            switch viewModel.state {
            case .loading: ProgressView()
            case .error(let e): ScreenError(
                error: e,
                onRetry: { viewModel.reload() }
            )
            case .ready(let t, let s): TutorialList(
                tutorials: t,
                seenUntil: s,
                reload: { await viewModel.reloadAndWait() }
            )
            }
        }
        .navigationTitle("tutorials___header"~)
    }
}

private struct TutorialList: View {
    var tutorials: [TutorialStruct]
    var seenUntil: Date
    var reload:  () async -> Void
    
    var body: some View {
        List {
            ForEach(tutorials) { t in
                TutorialsListItem(tutorial: t, seenUntil: seenUntil)
            }
        }
        .refreshable { await reload() }
    }
}

private class HelpTutorialsViewModel : ObservableObject {
    var bag:Set<AnyCancellable> = []
    private let core = HelpTutorialViewModelCore()
    @Published var state: State = .loading
    
    init() {
        core.state.asPublisher()
            .sink(receiveValue: { (state: FlowState<HelpTutorialViewModelCore.TutorialsList>) in
                if state is FlowStateLoading {
                    self.state = .loading
                } else if let e = state as? FlowStateError {
                    self.state = .error(e.throwable)
                } else if let l = (state as? FlowStateReady)?.data {
                    self.state = .ready(l.tutorials.map { $0.toStruct() }, l.seenUntil.toDate())
                }
            })
            .store(in: &bag)
    }
    
    func reloadAndWait() async {
        try? await core.reloadAndWait()
    }
    
    func reload() {
        core.reload()
    }
}

private enum State {
    case loading, error(KotlinThrowable), ready([TutorialStruct], Date)
}

struct HelpTutorials_Previews: PreviewProvider {
    static var previews: some View {
        HelpTutorialsView()
    }
}
