//
//  SignInViewAccessRequestFailed.swift
//  app-ios
//
//  Created by Christian on 14/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import SwiftUI
import OctoAppBase

struct SignInViewAccessRequestFailed: View {
    
    var state: SignInViewModelCore.StateAccessRequestFailed
    var onRetry: () -> Void
    
    var body: some View {
        OctoButton(text: "retry_general", clickListener: onRetry)
            .padding([.top], OctoTheme.dimens.margin4)
    }
}

struct SignInViewAccessRequestFailedPreviews: PreviewProvider {
    static var previews: some View {
        ScrollView {
            let state = SignInViewModelCore.StateAccessRequestFailed(url: UrlExtKt.toUrl("http://octopi.local"), reusedInstanceId: nil)
            VStack {
                SignInHeader(state: state)
                SignInViewAccessRequestFailed(state: state) {}
            }
        }
    }
}
