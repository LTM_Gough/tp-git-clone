//
//  SwiftUIView.swift
//  app-ios
//
//  Created by Christian on 14/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase
import SafariServices

struct SignInViewAccessRequest: View {
    
    var state: SignInViewModelCore.StateAccessRequest
    var onReturnToDiscover : () -> Void
    @State private var showingSafari = false
    
    var body: some View {
        VStack {
            VStack {
                Text("sign_in___access___explainer")
                    .padding([.top, .leading,.trailing], OctoTheme.dimens.margin2)
                    .typographyBase()
                    .frame(maxWidth: .infinity)
                    .multilineTextAlignment(.center)
                
                SignInPlayer(videoName: "access_explainer", videoExtension: "mp4")
                    .surface(color: .alternative)
                    .aspectRatio(16/9, contentMode: .fit)
                    .padding(OctoTheme.dimens.margin01)
            }
            .surface()
            
            OctoButton(text: "sign_in___access___open_web") {
                showingSafari = true
            }
            .padding([.top], OctoTheme.dimens.margin2)
            .sheet(isPresented: $showingSafari) {
                SafariView(url: state.loginUrl ?? state.url)
            }
           
            OctoButton(text: "sign_in___probe___edit_information", type: .link) {
                onReturnToDiscover()
            }
            .padding([.top], OctoTheme.dimens.margin01)
        }
    }
}

struct SignInViewAccessRequestPreviews: PreviewProvider {
    static var previews: some View {
        ScrollView {
            let state = SignInViewModelCore.StateAccessRequest(url: UrlExtKt.toUrl("http://octopi.local"), reusedInstanceId: nil, loginUrl: nil)
            VStack {
                SignInHeader(state: state)
                SignInViewAccessRequest(state: state) {
                    
                }
            }
            .padding(OctoTheme.dimens.margin2)
        }
    }
}
