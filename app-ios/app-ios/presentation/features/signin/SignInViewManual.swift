//
//  SignInViewManual.swift
//  app-ios
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct SignInViewManual: View {
    @Environment(\.dismiss) var dismiss
    
    @State var input = ""
    @State private var invalidInputShown = false
    @State private var invalidOctoEverywhereUrlShown = false
    @State private var sharedOctoEeverywhereUrlShonw = false
    var onContinue: (String) -> Void
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            
            ScrollView {
                VStack {
                    Text("sign_in___discovery___connect_manually_title")
                        .typographyTitle()
                        .frame(maxWidth: .infinity)
                        .padding([.top], OctoTheme.dimens.margin3)
                        .padding([.bottom], OctoTheme.dimens.margin1)
                    
                    SignInNeedHelp()
                        .padding([.bottom], OctoTheme.dimens.margin3)
                    
                    OctoTextField(
                        placeholder: "http://my-octoprint.com",
                        labelActiv: "sign_in___discover___web_url_hint_active",
                        keyboardType: .URL,
                        input: $input,
                        autocorrectionDisabled: true,
                        autoFocus: true
                    )
                    .onSubmit {
                        submit()
                    }
                    
                    OctoButton(text: "sign_in___continue") {
                        submit()
                    }
                    .padding([.top], OctoTheme.dimens.margin2)
                }
                .alert("sign_in___discovery___error_invalid_url", isPresented: $invalidInputShown) {
                    Button("OK", role: .cancel) {
                        invalidInputShown = false
                    }
                }
                .alert("sign_in___discovery___octoeverywhere_error_message_addition_shared_connection"~, isPresented: $invalidOctoEverywhereUrlShown) {
                    Button("OK", role: .cancel) {
                        invalidOctoEverywhereUrlShown = false
                    }
                }
                .alert(isPresented: $sharedOctoEeverywhereUrlShonw) {
                    Alert(
                        title: Text("sign_in___discovery___octoeverywhere_error_message"~),
                        primaryButton: .destructive(Text("sign_in___discovery___octoeverywhere_error_ignore"~)) {
                            sharedOctoEeverywhereUrlShonw = false
                            onContinue(UrlUtilsKt.urlFromStringInput(input: input)!)
                            dismiss()
                        },
                        secondaryButton: .default(Text("sign_in___discovery___octoeverywhere_error_comply"~)) {
                            sharedOctoEeverywhereUrlShonw = false
                        }
                    )
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .padding(OctoTheme.dimens.margin2)
            }
            
            
            OctoIconButton(icon: "xmark") { dismiss() }
                .padding(OctoTheme.dimens.margin1)
            
        }
    }
    
    private func submit() {
        if let urlString = UrlUtilsKt.urlFromStringInput(input: input) {
            if let url = UrlExtKt.toUrlOrNull(urlString) {
                if url.isSharedOctoEverywhereUrl() {
                    sharedOctoEeverywhereUrlShonw = true
                } else  if url.isOctoEverywhereUrl() {
                    invalidOctoEverywhereUrlShown = true
                } else {
                    onContinue(urlString)
                    dismiss()
                }
            } else {
                invalidInputShown = true
            }
        } else {
            invalidInputShown = true
        }
    }
}

struct SignInViewManual_Previews: PreviewProvider {
    static var previews: some View {
        SignInViewManual() { input in
            
        }
    }
}
