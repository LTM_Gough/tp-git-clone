//
//  SignInViewInitial.swift
//  app-ios
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct SignInViewSplash: View {
    var state: SignInViewModelCore.StateSplash
    
    var body: some View {
        ProgressView()
    }
}

struct SignInViewInitial_Previews: PreviewProvider {
    static var previews: some View {
        let now = Date().timeIntervalSince1970 + 4
        let until = Kotlinx_datetimeInstant.companion.fromEpochSeconds(epochSeconds: Int64(now), nanosecondAdjustment: 0)
        SignInViewSplash(
            state: SignInViewModelCore.StateSplash(until: until)
        )
    }
}
