//
//  SignInVie.swift
//  app-ios
//
//  Created by Christian on 09/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct SignInView: View {
    
    var repairForInstanceId: String? = nil
    @StateObject var viewModel = SiginInViewModel()
    
    var body: some View {
        LargeScreenStage {
            GeometryReader { geometry in
                ZStack {
                    if viewModel.state is SignInViewModelCore.StateSuccess {
                        SignInViewSuccessBackground()
                    }
                    
                    ScrollView(showsIndicators: false) {
                        
                        VStack {
                            SignInHeader(
                                state: viewModel.state
                            )
                            .transition(.slide)
                            .padding([.bottom], OctoTheme.dimens.margin2)
                            
                            if let discover = viewModel.state as? SignInViewModelCore.StateSplash {
                                SignInViewSplash(state: discover)
                                    .transition(.opacity)
                            } else if let discover = viewModel.state as? SignInViewModelCore.StateDiscover {
                                SignInViewDiscover(
                                    state: discover,
                                    onContinueWithUrl: { webUrl, confirmed in
                                        if (confirmed) {
                                            viewModel.moveToState(state: SignInViewModelCore.StateAccessRequest(url: UrlExtKt.toUrl(webUrl), reusedInstanceId: nil, loginUrl: nil))
                                        } else {
                                            viewModel.moveToState(state: SignInViewModelCore.StateProbe(urlString: webUrl,  apiKey: nil, reusedInstanceId: nil, finding: nil))
                                        }
                                    },
                                    onContinuePrevious: { config in
                                        viewModel.activate(config: config)
                                    },
                                    onDeletePrevious: { configId in
                                        viewModel.delete(configId: configId)
                                    }
                                ).transition(.opacity)
                            } else if let probe = viewModel.state as? SignInViewModelCore.StateProbe {
                                SignInViewProbe(
                                    state: probe,
                                    onTryWithUrl: { webUrl in
                                        viewModel.moveToState(state: SignInViewModelCore.StateProbe(urlString: webUrl, apiKey: nil, reusedInstanceId: probe.reusedInstanceId, finding: nil))
                                    }
                                ).transition(.opacity)
                            } else if let accessRquest = viewModel.state as? SignInViewModelCore.StateAccessRequest {
                                SignInViewAccessRequest(
                                    state: accessRquest,
                                    onReturnToDiscover: { viewModel.moveToState(state: SignInViewModelCore.StateDiscover(services: [], configs: [], hasQuickSwitch: true))}
                                ).transition(.opacity)
                            } else if let accessRquestFailed = viewModel.state as? SignInViewModelCore.StateAccessRequestFailed {
                                SignInViewAccessRequestFailed(
                                    state: accessRquestFailed,
                                    onRetry: { viewModel.moveToState(state: SignInViewModelCore.StateAccessRequest(url: accessRquestFailed.url, reusedInstanceId: accessRquestFailed.reusedInstanceId, loginUrl: nil))}
                                ).transition(.opacity)
                            }
                        }
                        .padding(OctoTheme.dimens.margin2)
                        .frame(width: geometry.size.width)
                        .frame(minHeight: geometry.size.height)
                        .padding([.bottom], OctoTheme.dimens.margin2)
                        .animation(.easeInOut, value: viewModel.state)
                    }
                    
                    if let success = viewModel.state as? SignInViewModelCore.StateSuccess {
                        SignInViewSuccessForeground {
                            viewModel.activate(config: success.config)
                        }
                    }
                }
                .transition(.opacity)
                .onAppear {
                    viewModel.start(repairForInstanceId: repairForInstanceId)
                }
            }
        }
    }
}

class SiginInViewModel : ObservableObject {
    // Disallow network in splash screen so we don't have the local network pop up right away
    private let core = SignInViewModelCore(allowNetworkInSplash: false)
    var bag:Set<AnyCancellable> = []

    @Published var state: SignInViewModelCore.State = SignInViewModelCore.StateInitial.shared
    
    init() {
        core.state.asPublisher()
            .sink { result in self.state = result }
            .store(in: &bag)
    }
    
    func moveToState(state: SignInViewModelCore.State) {
        core.moveToState(state: state)
    }
    
    func activate(config: OctoPrintInstanceInformationV3) {
        core.activate(config: config)
    }
    
    func delete(configId: String) {
        core.delete(configId: configId)
    }
    
    func start(repairForInstanceId: String?) {
        core.start(repairForInstanceId: repairForInstanceId)
    }
}

struct SignInVie_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
    }
}
