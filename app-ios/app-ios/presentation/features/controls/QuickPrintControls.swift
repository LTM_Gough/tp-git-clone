//
//  QuickPrintControls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct QuickPrintControls: View {
    var body: some View {
        ControlsScaffold(title: "widget_quick_print"~, icon: nil) {
            VStack {
                ForEach(0...3, id: \.self) { _ in
                    ControlsGhost()
                        .frame(height: 45)
                        .clipShape(Capsule())
                }
            }
        }
    }
}

struct QuickPrintControls_Previews: PreviewProvider {
    static var previews: some View {
        QuickPrintControls()
    }
}
