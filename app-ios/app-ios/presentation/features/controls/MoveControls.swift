//
//  MoveControls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct MoveControls: View {
    var body: some View {
        ControlsScaffold(title: "widget_move"~, icon: nil) {
            HStack {
                ControlsGhost().frame(width: 160)
                Spacer()
                ControlsGhost().frame(width: 80)
                Spacer()
                ControlsGhost().frame(width: 80)
            }
            .frame(height: 160)
        }
    }
}

struct MoveControls_Previews: PreviewProvider {
    static var previews: some View {
        MoveControls()
    }
}
