//
//  WebcamControls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct WebcamControls: View {
    @StateObject private var viewModel = WebcamControlsViewModel()
    
    var body: some View {
        ControlsScaffold(title: "webcam"~) {
            ZStack {
                Image(uiImage: viewModel.image)
                    .resizable()
                
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                
                if(!viewModel.text.isEmpty) {
                    Text(viewModel.text)
                        .foregroundColor(OctoTheme.colors.textColoredBackground)
                        .typographySubtitle()
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                        .background(.black.opacity(0.2))
                }
            }
            .aspectRatio(16/9, contentMode: .fill)
            .background(Color.black)
            .frame(maxWidth: .infinity)
            .cornerRadius(OctoTheme.dimens.cornerRadius)
        }
    }
}

struct WebcamControls_Previews: PreviewProvider {
    static var previews: some View {
        WebcamControls()
    }
}


private class WebcamControlsViewModel : ObservableObject {
    var bag:Set<AnyCancellable> = []
    private let core = WebcamControlsViewModelCore(instanceId: nil)
    
    @Published var text: String = ""
    @Published var connection: String = "Connecting..."
    @Published var image: UIImage = UIImage()
    
    init() {
        core.state.asPublisher()
            .sink { (result: WebcamControlsViewModelCore.UiState) in
                if result is WebcamControlsViewModelCore.UiStateLoading {
                    self.text = "Loading"
                } else if let frame = result as? WebcamControlsViewModelCore.UiStateFrame {
                    self.text = ""
                    self.image = frame.frame.frame
                }  else if let error = result as?WebcamControlsViewModelCore.UiStateError {
                    self.text = "\(error.throwable.composeErrorMessage())"
                }
            }
            .store(in: &bag)
    }
}
