//
//  TemperatureControl.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Charts

struct TemperatureControlsComponent: View {
    var snapshot: TemperatureSnapshotStruct?
        
    @Environment(\.colorScheme) private var colorScheme
    @State var showInput = false
    private var detailText : String? {
        if snapshot?.canControl != true {
            return nil
        }
        
        guard let target = snapshot?.current.target else {
            return " "
        }
        
        if target <= 0 {
            return "target_off"~
        }
        
        guard let offset = snapshot?.offset else {
            return String(format: "target_x"~, Int(target).description)
        }
        
        guard offset > 0 else {
            return String(format: "target_x"~, Int(target).description)
        }

        
        return String(format: "target_x_offset_y"~, Int(target).description, Int(offset).description)
    }
    private var mainText: String {
        if let offset = snapshot?.current.actual {
            return String(format: "temperature_x"~, Int(offset).description)
        }else {
            return " "
        }
    }
    private var chartGradient : LinearGradient {
        return LinearGradient(
            gradient: Gradient (
                colors: [
                    colorScheme == .dark ? .white.opacity(0.1) : .black.opacity(0.08),
                    colorScheme == .dark ? .white.opacity(0.02) : .black.opacity(0.01),
                ]
            ),
            startPoint: .top,
            endPoint: .bottom
        )
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            ZStack {
                if let history = snapshot?.history {
                    ZStack {
                        Chart {
                            ForEach(history) { h in
                                AreaMark(
                                    x: .value("Time", h.time),
                                    yStart: .value("Temp", 0),
                                    yEnd: .value("Temp", h.temperature)
                                )
                            }
                        }
                        .animation(.easeIn, value: history.last?.time)
                        .chartXAxis(.hidden)
                        .chartYAxis(.hidden)
                        .foregroundStyle(chartGradient)
                        .chartYScale(domain: 0...(snapshot?.maxTemp ?? 100))
                    }
                }
                    
                VStack(alignment: .leading) {
                    Text(snapshot?.componentLabel ?? "")
                        .typographyLabel()
                    
                    Text(mainText)
                        .typographyDataLarge()
                        .lineLimit(1)
                    
                    if let detail = detailText {
                        Text(detail)
                            .typographyLabel()
                    }
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding([.trailing, .leading], OctoTheme.dimens.margin2)
                .padding([.top], OctoTheme.dimens.margin12)
                .padding([.bottom], OctoTheme.dimens.margin1)
            }
            
            if snapshot?.canControl == true {
                OctoButton(text: LocalizedStringKey("set"), type: .surface, small: true)
                {
                    showInput = true
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                .sheet(isPresented: $showInput) {
                    if let s = snapshot {
                        TemperatureChangeView(
                            component: s.component,
                            componentLabel: s.componentLabel,
                            currentOffset: Int(s.offset ?? 0),
                            currentTarget: Int(s.current.target ?? 0)
                        )
                    }
                }
            } else {
                Color.clear
                    .frame(height: OctoTheme.dimens.margin1)
            }
        }
        .background(tempColor())
        .surface()
        .animation(.default, value: mainText)
        .animation(.default, value: detailText)
    }
    
    func tempColor() -> Color {
        let min = Double(25)
        let max = Double(snapshot?.maxTemp ?? 100)
        let current = Double(snapshot?.current.actual ?? Float(min))
        let percent = ((current - min) / (max - min))
        
        
        return OctoTheme.colors.hot.opacity(percent * 0.4)
    }
}

extension UIColor {
    static func blend(color1: UIColor, intensity1: CGFloat = 0.5, color2: UIColor, intensity2: CGFloat = 0.5) -> UIColor {
        let total = intensity1 + intensity2
        let l1 = intensity1/total
        let l2 = intensity2/total
        guard l1 > 0 else { return color2 }
        guard l2 > 0 else { return color1 }
        var (r1, g1, b1, a1): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
        var (r2, g2, b2, a2): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)

        color1.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        color2.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)

        return UIColor(red: l1*r1 + l2*r2, green: l1*g1 + l2*g2, blue: l1*b1 + l2*b2, alpha: l1*a1 + l2*a2)
    }
}

struct TemperatureControlsComponent_Previews: PreviewProvider {
    static var previews: some View {
        TemperatureControlsComponent(
            snapshot: TemperatureSnapshotStruct(
                component: "T",
                componentLabel: "Tool 1",
                current: ComponentTemperatureStruct(actual: 204.4, target: 205),
                history: [],
                canControl: true,
                offset: nil,
                isHidden: false,
                maxTemp: 240
            )
        )
        .previewDisplayName("Hotend ON")
        
        TemperatureControlsComponent(
            snapshot: TemperatureSnapshotStruct(
                component: "T",
                componentLabel: "Tool 1",
                current: ComponentTemperatureStruct(actual: 20, target: 21),
                history: [],
                canControl: true,
                offset: nil,
                isHidden: false,
                maxTemp: 240
            )
        )
        .previewDisplayName("Hotend OFF")
        
        TemperatureControlsComponent(
            snapshot: TemperatureSnapshotStruct(
                component: "T",
                componentLabel: "Tool 1",
                current: ComponentTemperatureStruct(actual: 20, target: 21),
                history: [],
                canControl: true,
                offset: 10,
                isHidden: false,
                maxTemp: 240
            )
        )
        .previewDisplayName("Hotend ON + offset")
        
        TemperatureControlsComponent(
            snapshot: TemperatureSnapshotStruct(
                component: "A",
                componentLabel: "Ambient",
                current: ComponentTemperatureStruct(actual: 20, target: nil),
                history: [],
                canControl: false,
                offset: 10,
                isHidden: false,
                maxTemp: 240
            )
        )
        .previewDisplayName("Ambient")
    }
}
