//
//  TemperatureControlsChange.swift
//  OctoApp
//
//  Created by Christian on 29/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct TemperatureChangeView: View {
    
    @Environment(\.dismiss) private var dismiss
    var component: String
    var componentLabel: String
    var currentOffset: Int
    var currentTarget: Int
    
    var body: some View {
        NavigationStack {
            TemperatureChangeViewInner(
                component: component,
                componentLabel: componentLabel,
                currentOffset: currentOffset,
                currentTarget: currentTarget
            )
            .navigationTitle(String(format: "x_temperature"~, componentLabel))
            .toolbar {
                ToolbarItemGroup(placement: .cancellationAction) {
                    Button("cancel"~) { dismiss() }
                }
            }
        }
    }
}

private struct TemperatureChangeViewInner: View {
    
    var component: String
    var componentLabel: String
    var currentOffset: Int
    var currentTarget: Int
    
    @State var offset: String = ""
    @State var target: String = ""
    @Environment(\.dismiss) private var dismiss
    @Environment(\.instanceId) private var instanceId
    @StateObject private var viewModel = TemperatureControlsViewModel()
    
    var body: some View {
        VStack {
            OctoTextField(
                placeholder: "target_temperature"~,
                labelActiv: LocalizedStringKey(String(format: "x_temperature"~, componentLabel)),
                keyboardType: .numberPad,
                input: $target,
                autoFocus: true
            )
            OctoTextField(
                placeholder: "temperature_widget___change_offset"~,
                labelActiv: LocalizedStringKey("temperature_widget___change_offset"),
                keyboardType: .numberPad,
                input: $offset
            )
            OctoAsyncButton(
                text: LocalizedStringKey("set_temperature"),
                enabled: !offset.isEmpty && !target.isEmpty
            ) {
                try await viewModel.setTemperature(
                    instanceId: instanceId,
                    component: component,
                    target: target,
                    offset: offset
                )
                dismiss()
            }
            .padding(.top, OctoTheme.dimens.margin2)
            Spacer()
        }
        .padding(OctoTheme.dimens.margin12)
        .onAppear {
            offset = "\(currentOffset)"
            target = "\(currentTarget)"
        }
    }
}

private class TemperatureControlsViewModel : ObservableObject {
    var bag:Set<AnyCancellable> = []
    private var core = ChangeTemperatureViewModelCore()
    
    func setTemperature(
        instanceId: String,
        component: String,
        target: String,
        offset: String
    ) async throws {
        try await core.setTemperature(
            instanceId: instanceId,
            component: component,
            target: target,
            offset: offset
        )
    }
}

struct TemperatureControlsChange_Previews: PreviewProvider {
    static var previews: some View {
        TemperatureChangeView(
            component: "T",
            componentLabel: "Hotend",
            currentOffset: 10,
            currentTarget: 200
        )
    }
}
