//
//  TuneControls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct TuneControls: View {
    var body: some View {
        ZStack {
            HStack {
                Text("tune"~)
                    .typographySectionHeader()
                   
                Spacer()
                Image(systemName: "chevron.right")
                    .foregroundColor(OctoTheme.colors.accent)
            }
        }
        .padding(OctoTheme.dimens.margin2)
        .frame(maxWidth: .infinity)
        .background(OctoTheme.colors.inputBackground)
        .cornerRadius(OctoTheme.dimens.cornerRadius)
        .padding([.leading, .trailing], OctoTheme.dimens.margin2)
        .padding([.top, .bottom], OctoTheme.dimens.margin12)
    }
}

struct TuneControls_Previews: PreviewProvider {
    static var previews: some View {
        TuneControls()
    }
}
