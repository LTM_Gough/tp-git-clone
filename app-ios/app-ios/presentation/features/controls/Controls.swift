//
//  Controls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import SwiftUITrackableScrollView
import Combine
import OctoAppBase

struct Controls: View {
    
    @Environment(\.instanceId) private var instanceId
    @Environment(\.instanceLabel) private var instanceLabel
    @Environment(\.openURL) private var openUrl
    @Namespace private var animation
    @StateObject private var viewModel = ControlsViewModel()
    @State private var scrollViewContentOffset = CGFloat(0)
    @State private var topShadowRadius: CGFloat = 0
    @State private var bottomShadowRadius: CGFloat = 1
    @State private var headerAlpha: CGFloat = 1
    @State private var showMainMenu = false
    
    
    var body: some View {
        VStack(spacing: 0) {
            ZStack(alignment: .top) {
                if (controlTypes().isEmpty) {
                    ZStack {
                        ProgressView()
                    }
                    .frame(maxHeight: .infinity)
                }
                
                TrackableScrollView(.vertical, contentOffset: $scrollViewContentOffset) {
                    LazyVStack {
                        ForEach(controlTypes()) { controlType in
                            switch(controlType) {
                            case .progress:  ProgressControls().matchedGeometryEffect(id: "progress", in: animation)
                            case .temperature: TemperatureControls().matchedGeometryEffect(id: "temperature", in: animation)
                            case .webcam: WebcamControls().matchedGeometryEffect(id: "webcam", in: animation)
                            case .move: MoveControls().matchedGeometryEffect(id: "move", in: animation)
                            case .extrude: ExtrudeControls().matchedGeometryEffect(id: "extrude", in: animation)
                            case .sendGcode: SendGcodeControls().matchedGeometryEffect(id: "sendGcode", in: animation)
                            case .gcodePreview: TuneControls().matchedGeometryEffect(id: "gcodePreview", in: animation)
                            case .tune: TuneControls().matchedGeometryEffect(id: "tune", in: animation)
                            case .quickAccess: QuickAccessControls().matchedGeometryEffect(id: "quickAccess", in: animation)
                            case .quickPrint:  QuickPrintControls().matchedGeometryEffect(id: "quickPrint", in: animation)
                            }
                        }
                    }
                    .padding(.top, OctoTheme.dimens.margin6)
                    .padding(.bottom, OctoTheme.dimens.margin2)
                }
                
                ControlsHeader(state: viewModel.state)
                    .opacity(headerAlpha)
                    .padding(.top, OctoTheme.dimens.margin1)
            }
        }.safeAreaInset(edge: .top) {
            ZStack {}
                .frame(maxWidth: .infinity)
                .background(OctoTheme.colors.windowBackground)
                .shadow(radius: topShadowRadius)
        }.safeAreaInset(edge: .bottom) {
            ControlsBottomBar(
                state: viewModel.state,
                onTogglePause: { viewModel.togglePausePrint() },
                onCancel: { viewModel.cancelPrint() },
                onOpenMenu: { showMainMenu = true},
                onShowFiles: { openUrl(UriLibrary.shared.getFileManagerUri(instanceId: instanceId, path: nil, label: instanceLabel)) },
                onShowTemperatures: { ExceptionReceivers.shared.dispatchException(throwable: KotlinThrowable()) }
            )
            .background(OctoTheme.colors.windowBackground.shadow(radius: bottomShadowRadius))
            .mask(Rectangle().padding(.top, -10))
            .background(OctoTheme.colors.windowBackground)
        }.onChange(of: scrollViewContentOffset, perform: { value in
            if (scrollViewContentOffset > 10 && topShadowRadius == 0) {
                withAnimation {
                    topShadowRadius = 1
                    headerAlpha = 0
                }
            }
            
            if (scrollViewContentOffset <= 10 && topShadowRadius > 0) {
                withAnimation {
                    topShadowRadius = 0
                    headerAlpha = 1
                }
            }
        }).withInstanceId {
            viewModel.initWith(instanceId: $0)
        }.sheet(isPresented: $showMainMenu) {
            Menu().presentationDetents([.height(300)])
        }.animation(.default, value: viewModel.state)
    }
    
    private func controlTypes() -> [ControlsType] {
        switch(viewModel.state) {
        case .initial: return []
        case .connect: return [.webcam, .quickAccess]
        case .prepare: return [.temperature, .webcam, .move, .extrude, .sendGcode, .quickPrint, .quickAccess]
        case .print: return [.progress, .temperature, .webcam, .gcodePreview, .quickAccess]
        }
    }
}

struct Controls_Previews: PreviewProvider {
    static var previews: some View {
        Controls()
    }
}

private class ControlsViewModel : ObservableObject {
    var bag:Set<AnyCancellable> = []
    private var currentCore: ControlsViewModelCore? = nil
    @Published var state: ControlsState = .initial
    
    func initWith(instanceId: String) {
        let core = ControlsViewModelCore(instanceId: instanceId)
        state = .initial
        currentCore = core
        core.state.asPublisher()
            .sink { (result: ControlsViewModelCore.State) in
                if result is ControlsViewModelCore.StateConnect {
                    self.state = .connect
                } else if result is ControlsViewModelCore.StatePrepare {
                    self.state = .prepare
                } else if let s = result as? ControlsViewModelCore.StatePrint {
                    self.state = .print(PrintInformation(progress: s.progress, pausing: s.pausing, paused: s.paused, cancelling: s.cancelling))
                }
            }
            .store(in: &bag)
    }
    
    func togglePausePrint() {
        Task {
            try await currentCore?.togglePause()
        }
    }
    
    func cancelPrint() {
        Task {
            try await currentCore?.cancelPrint()
        }
    }
}

enum ControlsState: Equatable {
    case initial, connect, prepare, print(PrintInformation)
}

enum ControlsType: String {
    case progress = "progress"
    case temperature = "temperature"
    case webcam = "webcam"
    case move = "move"
    case extrude = "extrude"
    case sendGcode = "sendGcode"
    case gcodePreview = "gcodePreview"
    case tune = "tune"
    case quickAccess = "quickAccess"
    case quickPrint = "quickPrint"
}

extension ControlsType: Identifiable {
    var id: String { rawValue }
}

struct PrintInformation: Equatable {
    let progress: Int32
    let pausing: Bool
    let paused: Bool
    let cancelling: Bool
}
