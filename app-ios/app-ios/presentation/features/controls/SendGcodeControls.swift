//
//  SendGcodeControls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Flow

struct SendGcodeControls: View {
    var body: some View {
        ControlsScaffold(title: "send_gcode"~, icon: nil) {
            HStack {
                HFlow(
                    itemSpacing: OctoTheme.dimens.margin1,
                    rowSpacing: OctoTheme.dimens.margin01
                ) {
                    ForEach(0...4, id: \.self) { _ in
                        ControlsGhost()
                            .frame(width: 100, height: 50)
                            .background(OctoTheme.colors.inputBackground)
                            .clipShape(Capsule())
                    }
                }
                Spacer()
            }
        }
    }
}

struct SendGcodeControls_Previews: PreviewProvider {
    static var previews: some View {
        SendGcodeControls()
    }
}
