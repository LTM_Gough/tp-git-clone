//
//  ProgressControlThumbnail.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct ProgressControlsThumbnail: View {
    
    var settings: ProgressWidgetSettingsStruct?
    var thumbnailUrl: String?
    
    var body: some View {
        let showThumb = settings?.showThumbnail == true
        
        ZStack {
            if showThumb {
                NetworkImage(
                    url: thumbnailUrl,
                    small: true
                )
                .padding(OctoTheme.dimens.margin01)
                .ifPad { $0.frame(width: 125, height: 125) }
                .ifNotPad { $0.frame(width: 100, height: 100) }
                .surface()
            }
        }
        .padding(.trailing, showThumb ? OctoTheme.dimens.margin12 : OctoTheme.dimens.margin2)
    }
}

struct ProgressControlsThumbnail_Previews: PreviewProvider {
    static var previews: some View {
        ProgressControlsThumbnail(
            settings: ProgressWidgetSettingsStruct(
                showUsedTime: true,
                showLeftTime: true,
                showThumbnail: true,
                showPrinterMessage: true,
                showLayer: true,
                showZHeight: true,
                etaStyle: .compact,
                printNameStyle: .compact,
                fontSize: .normal
            ),
            thumbnailUrl: NetworkImageLoadingMarker
        )
    }
}
