//
//  ProgressControlsBar.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI


private let trailingMin = 70

struct ProgressControlsBar: View {
    var progress: Int
    var pausing: Bool = false
    var paused: Bool = false
    var cancelling: Bool = false
    
    private var alignment: Alignment {
        return progress >= trailingMin ? .trailing : .leading
    }
    @State private var text = ""
    @Environment(\.instanceColor) private var progressColor
    
    var body: some View {
        ProgressControlsBarLayout(progress: progress) {
            progressColor.base
            Text(text)
                .foregroundColor(alignment == .leading ? OctoTheme.colors.darkText : OctoTheme.colors.textColoredBackground)
                .typographySubtitle()
                .padding([.leading, .trailing], OctoTheme.dimens.margin01)
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: alignment)
            
        }
        .background(OctoTheme.colors.inputBackground)
        .frame(maxWidth: .infinity)
        .frame(height: 45)
        .clipShape(Capsule())
        .animation(.spring(dampingFraction: 1), value: progress)
        .animation(.spring(dampingFraction: 1), value: cancelling)
        .animation(.spring(dampingFraction: 1), value: paused)
        .animation(.spring(dampingFraction: 1), value: pausing)
        .onChange(of: progress) { c in updateText(progress: c, cancelling: cancelling, paused: paused, pausing: pausing) }
        .onChange(of: cancelling) { c in updateText(progress: progress, cancelling: c, paused: paused, pausing: pausing) }
        .onChange(of: paused) { c in updateText(progress: progress, cancelling: cancelling, paused: c, pausing: pausing) }
        .onChange(of: pausing) { c in updateText(progress: progress, cancelling: cancelling, paused: paused, pausing: c) }
        .onAppear { updateText(progress: progress, cancelling: cancelling, paused: paused, pausing: pausing) }
    }
    
    func updateText(progress: Int, cancelling: Bool, paused: Bool, pausing: Bool) {
        // Decouple the text change from the progress change
        Task {
            try? await Task.sleep(for: .milliseconds(50))
            if cancelling {
                text = "cancelling"~
            } else if pausing {
                text = "pausing"~
            } else if paused {
                text = "paused"~
            } else {
                text = String(format: "x_percent_int"~, progress < 100 ? progress.description : 100.description)
            }
        }
    }
}


private struct ProgressControlsBarLayout: Layout {
    
    var progress: Int
    
    func sizeThatFits(proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) -> CGSize {
        return proposal.replacingUnspecifiedDimensions()
    }
    
    
    func placeSubviews(in bounds: CGRect, proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) {
        var p = bounds.origin
        let progressWidth = bounds.size.width * (Double(progress) / 100.0)
        let leftProgress = progress >= trailingMin
        
        subviews[0].place(
            at: p,
            proposal: .init(width: progressWidth, height: bounds.size.height)
        )
        
        if (leftProgress) {
            subviews[1].place(
                at: p,
                proposal: .init(width: progressWidth, height: bounds.size.height)
            )
        } else {
            p.x += progressWidth
            subviews[1].place(
                at: p,
                proposal: .init(width: bounds.size.width - progressWidth, height: bounds.size.height)
            )
        }
    }
}

struct ProgressControlsBar_Previews: PreviewProvider {
    static var previews: some View {
        StatefulPreviewWrapper(1) { progress in
            ProgressControlsBar(
                progress: progress.wrappedValue,
                cancelling: progress.wrappedValue == 0
            )
                .task {
                    while progress.wrappedValue < 100 {
                        try? await Task.sleep(for: .milliseconds(300))
                        progress.wrappedValue = progress.wrappedValue + Int.random(in: 1..<10)
                    }
                    progress.wrappedValue = 0
                }
            
        }
        .previewDisplayName("Animated")
    }
}
