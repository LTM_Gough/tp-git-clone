//
//  ProgressControls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct ProgressControls: View {
    @StateObject private var viewModel = ProgressControlsViewModel()
    
    var body: some View {
        ControlsScaffold(title: "progress_widget___title"~) {
            VStack(spacing: OctoTheme.dimens.margin12) {
                ProgressControlsBar(
                    progress: Int(viewModel.state?.current?.progress?.completion ?? 0.0),
                    pausing:  viewModel.state?.current?.state.flags.pausing ?? false,
                    paused:  viewModel.state?.current?.state.flags.paused ?? false,
                    cancelling: viewModel.state?.current?.state.flags.cancelling ?? false
                )
                
                HStack(spacing: 0) {
                    ProgressControlsThumbnail(
                        settings: viewModel.state?.settings,
                        thumbnailUrl: viewModel.state?.thumbanilUrl
                    )
                    
                    ProgressControlsInformation(
                        current: viewModel.state?.current,
                        settings: viewModel.state?.settings
                    )
                    .padding(.trailing, OctoTheme.dimens.margin2)
                }
            }
        }
        .withInstanceId { viewModel.initWithInstance(instanceId: $0) }
        .animation(.default, value: viewModel.state)
    }
}



private class ProgressControlsViewModel : ObservableObject {
    var bag:Set<AnyCancellable> = []
    private var currentCore: ProgressControlsViewModelCore? = nil
    @Published var state: ProrgessState? = nil


    func initWithInstance(instanceId: String) {
        let core = ProgressControlsViewModelCore(instanceId: instanceId)
        currentCore = core
        state = nil

        core.state.asPublisher()
            .sink { (state: ProgressControlsViewModelCore.State) in
                var thumbnail: String? = nil
                
                if state.activeFile is FlowStateLoading {
                    thumbnail = NetworkImageLoadingMarker
                }
                
                if let f = (state.activeFile as? FlowStateReady<FileObject.File>)?.data {
                    thumbnail = f.thumbnail
                }

                self.state = ProrgessState(
                    current: state.current.toStruct(),
                    thumbanilUrl: thumbnail,
                    plugin: state.plugin?.toStruct(),
                    settings: state.settings.toStruct()
                )
            }
            .store(in: &bag)
    }
}


private struct ProrgessState: Equatable {
    let current: MessageCurrentStruct?
    let thumbanilUrl: String?
    let plugin: CompanionPluginMessageStruct?
    let settings: ProgressWidgetSettingsStruct?
}


struct ProgressControls_Previews: PreviewProvider {
    static var previews: some View {
        ProgressControls()
    }
}
