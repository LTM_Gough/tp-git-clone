//
//  FileManager.swift
//  OctoApp
//
//  Created by Christian on 19/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct FileManager: View {
    var path: String = "/"
    var label: String? = nil
    @Environment(\.instanceLabel) private var instanceLabel

    var body: some View {
        FileManagerView(path: path, title: path == "/" ? instanceLabel : (label ?? ""))
    }
}

class FileManagerViewModel : ObservableObject {
    
    var bag:Set<AnyCancellable> = []
    private var currentCore: FileListViewModelCore? = nil
    private var currentPath: String =  "/"
    private var pendingContinuation: CheckedContinuation<Void, Never>? = nil
    
    
    @Published var prinStartLoading: Bool = false
    @Published var printStarted: Bool = false
    @Published var canStartPrint: Bool = false
    @Published var confirmSettingsState: FileStartPrintState = .idle
    @Published var state: FileManagerState = .loading
    @Published var informAboutThumbnails = false
    @Published var title: String? = nil
    
    
    func initWith(instanceId: String, path: String, title: String) {
        let core = FileListViewModelCore(instanceId: instanceId)
        currentPath = path
        currentCore = core
        
        core.fileListState
            .asPublisher()
            .sink(receiveValue: { (state: FileListViewModelCore.State) in
                self.canStartPrint = state.canStartPrint
                self.confirmSettingsState = .idle
                
                if state.state is FileListState.Loading {
                    switch(self.state) {
                    case .loading: self.state = .loading
                    case .error(_): self.state = .loading
                    case .loaded(_): do { /* Drop loading state as refresh is shown */}
                    }
                } else if let e = state.state as? FileListState.Error {
                    self.state = .error(e.exception)
                    self.firePendingContinuation()
                } else if let f = state.state as? FileListState.Loaded {
                    self.state = .loaded(f.file.toStruct())
                    self.firePendingContinuation()
                    
                    // Update title, stick with the default for the root folder
                    self.title = f.file.path == "/" ? title : f.file.display
                }
            })
            .store(in: &bag)
        
        core.informAboutThumbnails
            .asPublisher()
            .sink(receiveValue: { (inform: KotlinBoolean) in
                self.informAboutThumbnails = inform.boolValue
            })
            .store(in: &bag)
        
        core.loadFiles(path: path, skipCache: false)
    }
    
    private func firePendingContinuation() {
        self.pendingContinuation?.resume()
        self.pendingContinuation = nil
    }
    
    func reloadAndWait() async {
        await withCheckedContinuation{ continuation in
            self.firePendingContinuation()
            self.pendingContinuation = continuation
            reload()
        }
    }
    
    func reload() {
        currentCore?.loadFiles(path: currentPath, skipCache: true)
    }
    
    func dismissThumbnailInfo() {
        currentCore?.dismissThumbnailInformation()
    }
    
    func startPrint(confirmedMaterial: Bool = false, confirmedTimelapse: Bool = false) {
        currentCore?.startPrint(confirmedMaterial: confirmedMaterial, confirmedTimelapse: confirmedTimelapse)
            .asPublisher()
            .sink(receiveValue: { (state: FileListViewModelCore.StartPrintResult) in
                self.prinStartLoading = state is FileListViewModelCore.StartPrintResultLoading
                self.printStarted = state is FileListViewModelCore.StartPrintResultStarted
                if state is FileListViewModelCore.StartPrintResultMaterialConfirmationRequired {
                    self.confirmSettingsState = .materialConfirmation(confirmedTimelapse)
                } else if state is FileListViewModelCore.StartPrintResultTimelapseConfirmationRequired {
                    self.confirmSettingsState = .timelapseConfirmation(confirmedMaterial)
                } else {
                    self.confirmSettingsState = .idle
                }
            })
            .store(in: &bag)
    }
}

enum FileStartPrintState: Equatable {
    case idle, materialConfirmation(Bool), timelapseConfirmation(Bool)
    
}

enum FileManagerState : Equatable {
    case loading, error(KotlinThrowable), loaded(FileObjectStruct)
}

struct FileManager_Previews: PreviewProvider {
    static var previews: some View {
        FileManager()
    }
}
