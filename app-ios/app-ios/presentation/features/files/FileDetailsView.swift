//
//  FileDetailsView.swift
//  OctoApp
//
//  Created by Christian on 23/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct FileDetailsView: View {
    var file: FileObjectStruct
    var loading: Bool
    var canStartPrint: Bool
    var onStartPrint: () -> Void
    var displayPath: String {
        var parts = file.path.split(separator: "/")
        parts.removeLast()
        return "/\(parts.joined(separator: "/"))"
    }
    var filamentUse: String? {
        let format = "%.02f m / %.02f cm³"
        if (file.filamentUseMm?.isEmpty == true) {
            return nil
        } else  if (file.filamentUseMm?.count == 1) {
            let f = file.filamentUseMm![0]
            return  String(format: format, f.name, f.length, f.volume)
        } else {
            return file.filamentUseMm?.map { i in
                String(format: "%s: \(format)", i.name, i.length, i.volume)
            }.joined(separator: "\n")
        }
        
    }
    var dimension: String? {
        guard let dimen = file.gcodeDimension else {
            return nil
        }
        return String(format: "%.1f × %.1f × %.1f mm", dimen.width, dimen.depth, dimen.height)
    }
    var printTime: String? {
        guard let pt = file.estimatedPrintTime else {
            return nil
        }
        
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute]
        formatter.unitsStyle = .brief
        return formatter.string(from: pt)
    }
    var lastPrint: String {
        guard let lastPrint = file.prints?.lastPrintDate else {
            return "file_manager___file_details___never"~
        }
        
        switch(file.prints?.lastPrintSuccess) {
        case false: return String(format: "file_manager___file_details___last_print_at_x_failure"~, lastPrint.formatted())
        case true: return String(format: "file_manager___file_details___last_print_at_x_success"~, lastPrint.formatted())
        default: return lastPrint.formatted()
        }
    }
    var failures: String {
        guard let failure = file.prints?.failure else {
            return "file_manager___file_details___never"~
        }
        
        return String(format: "x_times"~, failure.description)
    }
    var successes: String {
        guard let success = file.prints?.success else {
            return "file_manager___file_details___never"~
        }
        
        return String(format: "x_times"~, success.description)
    }
    
    var body: some View {
        ZStack(alignment: .bottom) {
            List {
                if (file.thumbnail != nil) {
                    Section {
                        ZStack(alignment: .center) {
                            NetworkImage(
                                url: file.thumbnail,
                                backupSystemName: "photo",
                                backupColor: OctoTheme.colors.lightText,
                                small: false
                            )
                            .frame(maxWidth: .infinity, maxHeight: .infinity)
                            .aspectRatio(1, contentMode: .fit)
                        }
                        .frame(maxWidth: .infinity, maxHeight: 300, alignment: .center)
                    }
                }
                if(printTime != nil || dimension != nil || filamentUse != nil) {
                    Section("file_manager___file_details___print_info"~) {
                        FileDetailsItem(
                            title: "file_manager___file_details___print_time"~,
                            value: printTime
                        )
                        FileDetailsItem(
                            title: "file_manager___file_details___model_size"~,
                            value: dimension
                        )
                        FileDetailsItem(
                            title: "file_manager___file_details___filament_use"~,
                            value: filamentUse
                        )
                    }
                }
                Section("file_manager___file_details___file"~) {
                    FileDetailsItem(
                        title: "location"~,
                        value: file.origin.label
                    )
                    FileDetailsItem(
                        title: "file_manager___file_details___path"~,
                        value: displayPath)
                    FileDetailsItem(
                        title: "file_manager___file_details___file_size"~,
                        value: file.size?.asStyleFileSize()
                    )
                }
                Section("file_manager___file_details___history"~) {
                    FileDetailsItem(
                        title: "file_manager___file_details___last_print"~,
                        value: lastPrint
                    )
                    FileDetailsItem(
                        title: "file_manager___file_details___completed"~,
                        value: successes
                    )
                    FileDetailsItem(
                        title: "file_manager___file_details___failures"~,
                        value: failures
                    )
                }
                
                // Padding for button
                Section {
                    Color.clear
                        .frame(height:  OctoTheme.dimens.margin4)
                        .listRowBackground(Color.clear)
                }
            }
            
            
            OctoButton(
                text: LocalizedStringKey("start_printing"),
                loading: loading,
                enabled: canStartPrint,
                clickListener: onStartPrint
            )
            .padding(OctoTheme.dimens.margin2)
        }
    }
}

struct FileDetailsView_Previews: PreviewProvider {
    static var file = FileObjectStruct(
        origin: .local,
        display: "Some file.gcode",
        name: "Some file.gcode",
        type: "gcode",
        typePath: ["machinecode", "gcode"],
        path: "/Some file.gcode",
        isFolder: false
    )
    
    static var previews: some View {
        FileDetailsView(file: file, loading: true, canStartPrint: true, onStartPrint: {})
            .previewDisplayName("Starting to print")
        
        FileDetailsView(file: file, loading: false, canStartPrint: false, onStartPrint: {})
            .previewDisplayName("Already printing")
        
        FileDetailsView(file: file, loading: false, canStartPrint: false, onStartPrint: {})
            .previewDisplayName("Can start printing")
    }
}
