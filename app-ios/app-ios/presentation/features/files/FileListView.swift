//
//  FileListView.swift
//  OctoApp
//
//  Created by Christian on 23/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct FileListView: View {
    
    var file: FileObjectStruct
    var informAboutThumbnails: Bool
    var onDismissThumbnailInfo: () -> Void
    @State private var searchTerm: String = ""
    
    var body: some View {
        ZStack {
            FileListOuter(
                file: file,
                searchTerm: $searchTerm,
                informAboutThumbnails: informAboutThumbnails,
                onDismissThumbnailInfo: onDismissThumbnailInfo
            )
        }
        .if(file.path == "/") { $0.searchable(text: $searchTerm) }
        .onChange(of: searchTerm) { st in }
    }
}

private struct FileListOuter: View {
    
    var file: FileObjectStruct
    @Binding var searchTerm: String
    var informAboutThumbnails: Bool
    var onDismissThumbnailInfo: () -> Void
    @Environment(\.isSearching) var isSearching
    
    var body: some View {
        if isSearching {
            FileSearchView(
                searchTerm: searchTerm
            )
        } else {
            FileListInner(
                file: file,
                informAboutThumbnails: informAboutThumbnails,
                onDismissThumbnailInfo: onDismissThumbnailInfo
            )
            .onChange(of: isSearching) { _ in searchTerm = "" }
        }
    }
}


private struct FileListInner: View {
    var file: FileObjectStruct
    var informAboutThumbnails: Bool
    var onDismissThumbnailInfo: () -> Void
    
    var body: some View {
        List {
            FilesThumbnailAnnouncement(
                informAboutThumbnails: informAboutThumbnails,
                path: file.path,
                onDismissThumbnailInfo: onDismissThumbnailInfo
            )
            .transition(.opacity)
            
            FileListSection(files: file.childFolders)
            FileListSection(files: file.children)
        }
        .animation(.default, value: informAboutThumbnails)
        .animation(.default, value: file)
    }
}

struct FileListSection : View {
    @Environment(\.openURL) var openUrl
    @Environment(\.instanceId) var instanceId
    var files: [FileObjectStruct]?
    
    var body: some View {
        if (files != nil && files?.isEmpty == false) {
            Section {
                ForEach(files ?? []) { f in
                    NavigationLink(
                        value: UriLibrary.shared.getFileManagerUri(instanceId: instanceId, path: f.path, label: f.display).description(),
                        label: {FileListItem(file: f)}
                    )
                }
            }
        }
    }
}

struct FileListView_Previews: PreviewProvider {
    
    static var file = FileObjectStruct(
        origin: .local,
        display: "Some folder",
        name: "Some folder",
        type: "folder",
        typePath: [""],
        path: "/Some folder",
        
        childFolders: [
            FileObjectStruct(
                origin: .local,
                display: "Some other folder",
                name: "Some other folder",
                type: "folder",
                typePath: [""],
                path: "/Some folder/Some other folder",
                isFolder: true
            ),
            FileObjectStruct(
                origin: .local,
                display: "Third folder",
                name: "Third folder",
                type: "folder",
                typePath: [""],
                path: "/Some folder/Third folder",
                isFolder: true
            )
        ],
        children: [
            FileObjectStruct(
                origin: .local,
                display: "Some file.gcode",
                name: "Some file.gcode",
                type: "gcode",
                typePath: ["machinecode", "gcode"],
                path: "/Some file.gcode",
                isFolder: false
            ),
            FileObjectStruct(
                origin: .local,
                display: "Some other file.gcode",
                name: "Some other file.gcode",
                type: "gcode",
                typePath: ["machinecode", "gcode"],
                path: "/Some other file.gcode",
                isFolder: false
            )
        ],
        isFolder: true
    )
    
    static var previews: some View {
        FileListView(file: file, informAboutThumbnails: false, onDismissThumbnailInfo: {})
            .previewDisplayName("Normal")
        
        FileListView(file: file, informAboutThumbnails: true, onDismissThumbnailInfo: {})
            .previewDisplayName("Thumbnail info")
    }
}
