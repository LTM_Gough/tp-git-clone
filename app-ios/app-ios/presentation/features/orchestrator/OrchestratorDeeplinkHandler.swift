//
//  DeeplinkHandler.swift
//  OctoApp
//
//  Created by Christian on 19/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import SwiftUI
import OctoAppBase

private func handleUrl(externalOpenUrl: OpenURLAction, url: URL, inAppOpenUrl: (String) -> Void) {
    if UriLibrary.shared.isInAppLink(urlString: url.description) {
        print("Open in app URL \(url)")
        inAppOpenUrl(url.description)
    } else {
        print("Open external URL \(url)")
        externalOpenUrl(url)
    }
}

private struct OrchestratorDeeplinkHandler: ViewModifier {
    
    @State private var urls = [String]()
    @State private var sheetShown = false
    @Environment(\.openURL) var externalOpenUrl
    
    var openUrl: OpenURLAction {
        return OpenURLAction { url in
            handleUrl(externalOpenUrl: externalOpenUrl, url: url) { urlString in
                self.urls = [urlString]
                self.sheetShown = true
            }
            
            return OpenURLAction.Result.handled
        }
    }
    
    
    func body(content: Content) -> some View {
        content
            .sheet(isPresented: $sheetShown) {
                if !urls.isEmpty {
                    DeeplinkNavigator(initialUrl: urls)
                }
            }
            .environment(\.openURL, openUrl)
            .environment(\.dismissToRoot, { sheetShown = false })
    }
}

private struct DeeplinkDoneButton: ViewModifier {
    @Environment(\.dismissToRoot) private var dismiss
    var url: String
    
    func body(content: Content) -> some View {
        content.toolbar {
            ToolbarItemGroup(placement: .confirmationAction) {
                Button(textForUrl()) { dismiss() }
            }
        }
    }
    
    func textForUrl() -> String {
        let route = urlToRoute(url, activeInstanceId: "")
        switch route {
        case .contact(_): return "cancel"~
        default: return "done"~
        }
    }
}

private struct DeeplinkNavigator : View {
    
    var initialUrl: [String]
    @State private var path = [String]()
    @Environment(\.openURL) private var externalOpenUrl
    
    private var openUrl: OpenURLAction {
        return OpenURLAction { url in
            handleUrl(externalOpenUrl: externalOpenUrl, url: url) { urlString in
                path.append(urlString)
            }
            
            return OpenURLAction.Result.handled
        }
    }
    
    var body: some View {
        NavigationStack(path: $path) {
            NavigationDestination(
                url: initialUrl[0]
            )
            .deeplinkDoneButton(initialUrl[0])
            .navigationDestination(for: String.self) { url in
                NavigationDestination(url: url)
                    .deeplinkDoneButton(url)
            }
        }
        .onAppear {
            if path.isEmpty {
                path.append(contentsOf: initialUrl.dropFirst(1))
            }
        }
        .background(OctoTheme.colors.windowBackground)
        .environment(\.openURL, openUrl)
        .errorDialogDisplay()
    }
}

private struct NavigationDestination : View {
    var url: String
    @Environment(\.instanceId) var instanceId
    
    var body: some View {
        let route = urlToRoute(url, activeInstanceId: instanceId)
        
        switch(route) {
        case .file(instanceId: let i, path: let p, label: let l): FileManager(path: p, label: l)
                .environment(\.instanceId, i)
        case .terminal:  Text("Terminal")
        case .help: HelpLauncherView()
        case .helpItem(id: let id): HelpItemView(id: id)
        case .tutorials: HelpTutorialsView()
        case .contact(bugReport: let bugReport): HelpContactView(bugReport: bugReport)
        case .configureRemote: RemoteAccessView()
        default: ScreenError(title: "help___content_not_available"~)
                .padding(OctoTheme.dimens.margin12)
        }
    }
}

private enum NavigationRoute {
    case file(instanceId: String, path: String, label: String)
    case terminal
    case help
    case tutorials
    case error
    case helpItem(id: String)
    case contact(bugReport: Bool)
    case configureRemote
}

private func urlToRoute(_ urlString: String, activeInstanceId: String) -> NavigationRoute {
    let lib = UriLibrary.shared
    let url = UrlExtKt.toUrl(urlString)
    
    switch(url.withoutQuery().description) {
    case lib.getFileManagerUri(instanceId: nil, path: nil, label: nil).withoutQuery().description: return .file(
        instanceId: url.parameters.get(name: "instanceId") ?? activeInstanceId,
        path: lib.secureDecodeParam(url: url, name: "path", defaultValue: "/"),
        label: lib.secureDecodeParam(url: url, name: "label", defaultValue: "")
    )
    case lib.getFaqUri(faqId: "id").withoutQuery().description(): return .helpItem(
        id: lib.secureDecodeParam(url: url, name: "faqId", defaultValue: "unknown2")
    )
    case lib.getContactUri(bugReport: false).withoutQuery().description(): return .contact(
        bugReport: url.parameters.get(name: "bugReport") == "true"
    )
    case lib.getTerminalUri().description(): return .terminal
    case lib.getHelpUri().description(): return .help
    case lib.getTutorialsUri().description(): return .tutorials
    case lib.getConfigureRemoteAccessUri().description(): return .configureRemote
    default: return .error
    }
}


extension View {
    func deeplinkNavigation() -> some View {
        modifier(OrchestratorDeeplinkHandler())
    }
}

private extension View {
    func deeplinkDoneButton(_ url: String) -> some View {
        modifier(DeeplinkDoneButton(url: url))
    }
}

extension OpenURLAction {
    public func callAsFunction(_ url: Ktor_httpUrl) {
        self(URL(string: url.description())!)
    }
    public func callAsFunction(_ url: String) {
        self(URL(string: url)!)
    }
}
