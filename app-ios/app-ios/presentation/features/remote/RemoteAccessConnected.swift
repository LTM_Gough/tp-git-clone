//
//  RemoteAccessConnected.swift
//  OctoApp
//
//  Created by Christian on 30/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct RemoteAccessConnected: View {
    var text: String
    
    var body: some View {
        HStack {
            Image(systemName: "checkmark.icloud.fill")
            Text(text)
        }
        .foregroundColor(.green)
        .typographySectionHeader()
        .padding(.top, OctoTheme.dimens.margin2)
    }
}

struct RemoteAccessConnected_Previews: PreviewProvider {
    static var previews: some View {
        RemoteAccessConnected(text: "configure_remote_acces___octoeverywhere___connected"~)
    }
}
