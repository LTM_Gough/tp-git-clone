//
//  RemoteAccessManual.swift
//  OctoApp
//
//  Created by Christian on 30/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct RemoteAccessManual: View {
    
    @StateObject private var viewModel = RemoteAccessManualViewModel()
    @State private var url: String = ""
    @State private var user: String = ""
    @State private var password: String = ""
    @State private var skipTest: Bool = false
    @State private var saved = false
    
    private var showInfo: Bool {
        return url.isEmpty && user.isEmpty && password.isEmpty
    }
    
    var body: some View {
        VStack {
            if showInfo {
                Text("configure_remote_acces___manual___description"~)
                    .multilineTextAlignment(.center)
                    .typographyBase()
                    .padding(.bottom, OctoTheme.dimens.margin1)
            }
            
            OctoTextField(
                placeholder: "sign_in___discover___web_url_hint"~,
                labelActiv: LocalizedStringKey("sign_in___discover___web_url_hint_active"),
                alternativeBackgroung: true,
                keyboardType: .URL,
                input: $url,
                autocorrectionDisabled: true
            )
            
            if !showInfo {
                
                OctoTextField(
                    placeholder: "configure_remote_access___manual___username_hint"~,
                    labelActiv: LocalizedStringKey("configure_remote_access___manual___username_hint_active"),
                    alternativeBackgroung: true,
                    keyboardType: .asciiCapable,
                    input: $user,
                    autocorrectionDisabled: true
                )

                OctoTextField(
                    placeholder: "configure_remote_access___manual___password_hint"~,
                    labelActiv: LocalizedStringKey("configure_remote_access___manual___password_hint_active"),
                    alternativeBackgroung: true,
                    keyboardType: .asciiCapable,
                    input: $password,
                    autocorrectionDisabled: true
                )

                Toggle(LocalizedStringKey("Skip test"), isOn: $skipTest)
                    .typographyInput()
                    .padding([.leading, .trailing], OctoTheme.dimens.margin2)
            }
            
            OctoAsyncButton(
                text: LocalizedStringKey("configure_remote_acces___manual___button"~),
                clickListener: {
                    let success = try await viewModel.setManual(
                        url: url,
                        user: user,
                        password: password,
                        skipTest: skipTest
                    )
                    
                    guard success else { return }
                    
                    DispatchQueue.main.async {
                        saved = true
                        Task {
                            try? await Task.sleep(for: .seconds(2))
                            DispatchQueue.main.async {
                                saved = false
                            }
                        }
                    }
                }
            )
            .padding([.top, .bottom], OctoTheme.dimens.margin2)
            
            if saved {
                RemoteAccessConnected(text: "configure_remote_acces___remote_access_configured"~)
            }
            
            Spacer()
        }
        .padding(OctoTheme.dimens.margin12)
        .animation(.spring(), value: showInfo)
        .animation(.spring(), value: saved)
        .withInstanceId { viewModel.initWithInstance($0) }
        .onChange(of: viewModel.url) { u in self.url = u }
        .onChange(of: viewModel.user) { u in self.user = u }
        .onChange(of: viewModel.password) { p in self.password = p }
        .submitLabel(.done)
    }
}

private class RemoteAccessManualViewModel : ObservableObject {
    private var currentCore: RemoteAccessManualViewModelCore? = nil
    var bag:Set<AnyCancellable> = []
    @Published var url: String = ""
    @Published var user: String = ""
    @Published var password: String = ""
    
    func initWithInstance(_ instanceId: String) {
        let core = RemoteAccessManualViewModelCore(instanceId: instanceId)
        currentCore = core
        
        core.connectedState.asPublisher()
            .sink { (state: RemoteAccessBaseViewModelCore.State) in
                // Implies URL is not "" and not nil
                guard state.url?.isEmpty == false else {
                    self.url = ""
                    self.user = ""
                    self.password = ""
                    return
                }
                
                Napier.i(tag: "RemoteAccessManualViewModel", message: "Manual url: \(String(describing: state.url))")
                let u = UrlExtKt.toUrl(state.url!)
                self.url = u.withoutBasicAuth().description()
                self.user = u.user ?? ""
                self.password = u.password ?? ""
            }
            .store(in: &bag)
    }
    
    func setManual(url: String, user: String, password: String, skipTest: Bool) async throws -> Bool {
        return try await currentCore?.setManual(
            url: url,
            user: user,
            password: password,
            skipTest: skipTest
        ).boolValue == true
    }
}

struct RemoteAccessManual_Previews: PreviewProvider {
    static var previews: some View {
        RemoteAccessManual()
            .surface()
    }
}
