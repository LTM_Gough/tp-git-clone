//
//  RemoteAccessView.swift
//  OctoApp
//
//  Created by Christian on 29/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct RemoteAccessView: View {
    @StateObject var viewModel = RemoteAccessViewModel()
    @State var selection : RemoteService = .none
    
    var body: some View {
        GeometryReader { geo in
            if selection != .none && !viewModel.tabs.isEmpty {
                ScrollViewReader { reader in
                    ScrollView {
                        RemoteAccessLayout(screenHeight: geo.size.height) {
                            Header()
                            Tabs(selection: $selection, tabs: viewModel.tabs)
                            Pager(selection: $selection, tabs: viewModel.tabs).id("bottom")
                        }
                    }.onChange(of: selection) { x in
                        withAnimation {
                            reader.scrollTo("bottom", anchor: .bottom)
                        }
                    }
                }
            }
        }
        .animation(.spring(), value: selection)
        .animation(.spring(), value: viewModel.tabs)
        .ignoresSafeArea(edges: .bottom)
        .navigationTitle("configure_remote_access___title"~)
        .navigationBarTitleDisplayMode(.inline)
        .background(OctoTheme.colors.inputBackground)
        .withInstanceId { viewModel.initWithInstanceId($0) }
        .onChange(of: viewModel.tabs) { t in
            // Select first tab if none selected
            if selection == .none {
                selection = t.isEmpty ? .none : t[0]
            }
        }
    }
}

private struct Header : View {
    
    @Environment(\.colorScheme) var colorScheme

    
    var body : some View {
        VStack {
            SimpleLottieView(lottieFile: colorScheme == .dark ? "octo-vacation-dark" : "octo-vacation")
                .aspectRatio(16/9, contentMode: .fit)
                .padding(.top, OctoTheme.dimens.margin2)
            
            Text(LocalizedStringKey("configure_remote_acces___description"~))
                .multilineTextAlignment(.center)
                .typographyLabel()
                .padding([.bottom], OctoTheme.dimens.margin2)
                .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        }
    }
}

private struct Tabs : View {
    
    @Binding var selection: RemoteService
    var tabs: [RemoteService]
    
    var body : some View {
        ScrollViewReader { reader in
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: OctoTheme.dimens.margin01) {
                    ForEach(tabs) { tab in
                        TabButton(selection: $selection, service: tab) { reader.scrollTo(tab, anchor: .center) }
                    }
                }
                .padding([.leading, .trailing], OctoTheme.dimens.margin12)
                .padding([.top], OctoTheme.dimens.margin12)
                .onChange(of: selection) { s in
                    Task {
                        // For some reason we need to decouple this to make it work...
                        try? await Task.sleep(for: .milliseconds(10))
                        DispatchQueue.main.async {
                            withAnimation {
                                reader.scrollTo(s, anchor: .center)
                            }
                        }
                    }
                }
                .onChange(of: tabs) { _ in
                    withAnimation {
                        reader.scrollTo(selection, anchor: .center)
                    }
                }
            }
            .background(OctoTheme.colors.windowBackground)
        }
    }
}

private struct TabButton : View{
    
    @Binding var selection: RemoteService
    var service: RemoteService
    var scrollToMe: () -> Void
    
    var body : some View {
        OctoButton(
            text: LocalizedStringKey(service.title),
            type: selection == service ? .primary : .primaryUnselected,
            small: true,
            clickListener: {
                withAnimation {
                    selection = service
                    scrollToMe()
                }
            }
        )
        .id(service)
    }
}
private struct Pager : View{
    
    @Binding var selection: RemoteService
    var tabs: [RemoteService]
    
    var body : some View {
        TabView(selection: $selection) {
            ForEach(tabs) { tab in
                Page {
                    ZStack {
                        switch(tab) {
                        case .octoEverywhere: RemoteAccessOctoEverywhere()
                        case .obico: RemoteAccessObico()
                        case .ngrok: RemoteAccessNgrok()
                        case .manual: RemoteAccessManual()
                        case .none: Color.clear
                        }
                    }
                }.tag(tab)
            }
        }
        .tabViewStyle(.page(indexDisplayMode: .never))
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(.bottom, getSafeAreaBottom())
        .background(OctoTheme.colors.windowBackground)
    }
}

private struct Page<Content: View> : View {
    let content: () -> Content
    var body : some View {
        content()
            .surface(shadow: true)
            .padding(OctoTheme.dimens.margin12)
    }
}

private struct RemoteAccessLayout: Layout {
    var screenHeight: CGFloat
    
    func sizeThatFits(proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) -> CGSize {
        var p = proposal.replacingUnspecifiedDimensions()
        let headerHeight = subviews[0].sizeThatFits(proposal).height
        let tabHeight = subviews[1].sizeThatFits(proposal).height
        let contentHeight = screenHeight - tabHeight
        p.height = headerHeight + tabHeight + contentHeight
        return p
    }
    
    func placeSubviews(in bounds: CGRect, proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) {
        var p = bounds.origin
        
        // Header
        let headerHeigh = subviews[0].sizeThatFits(proposal).height
        subviews[0].place(
            at: p,
            proposal: .init(width: bounds.size.width, height: headerHeigh)
        )
        
        // Tabs
        p.y += headerHeigh
        let tabHeight = subviews[1].sizeThatFits(proposal).height
        subviews[1].place(
            at: p,
            proposal: .init(width: bounds.size.width, height: tabHeight)
        )
        
        // Pager
        p.y += tabHeight
        let pagerHeight = screenHeight - tabHeight
        subviews[2].place(
            at: p,
            proposal: .init(width: bounds.size.width, height: pagerHeight)
        )
    }
}

private func getSafeAreaBottom() -> CGFloat{
    let keyWindow = UIApplication.shared.connectedScenes
        .filter({$0.activationState == .foregroundActive})
        .map({$0 as? UIWindowScene})
        .compactMap({$0})
        .first?.windows
        .filter({$0.isKeyWindow}).first
    
    return (keyWindow?.safeAreaInsets.bottom)!
}

enum RemoteService : Identifiable, Equatable {
    var id: String { return title }
    case octoEverywhere, obico, ngrok, manual, none
}

extension RemoteService {
    var title: String {
        switch self {
        case .manual: return "configure_remote_acces___manual___title"~
        case .ngrok: return "configure_remote_acces___ngrok___title"~
        case .octoEverywhere: return "configure_remote_acces___octoeverywhere___title"~
        case .obico: return "configure_remote_acces___spaghetti_detective___title"~
        case .none: return ""
        }
    }
}

class RemoteAccessViewModel : ObservableObject {
    var bag:Set<AnyCancellable> = []
    @Published var tabs: [RemoteService] = []
    
    func initWithInstanceId(_ instanceId: String) {
        let core = RemoteAccessViewModelCore(instanceId: instanceId)
        core.services.asPublisher()
            .sink { (state: RemoteAccessViewModelCore.State) in
                self.tabs = state.services.map { s in
                    switch s {
                    case .octoeverywhere: return .octoEverywhere
                    case .obico: return .obico
                    case .ngrok: return .ngrok
                    case .manual: return .manual
                    default: return .none
                    }
                }
            }
            .store(in: &bag)
    }
}

struct RemoteAccessView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack {
            RemoteAccessView()
        }
        .background(OctoTheme.colors.windowBackground)
        
    }
}
