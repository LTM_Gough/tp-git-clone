//
//  RemoteAccessObico.swift
//  OctoApp
//
//  Created by Christian on 30/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

private let surfaceBackground = Color.white.opacity(0.9)
private let surfaceForeground = Color.black

struct RemoteAccessObico: View {
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin2) {
            TitleBar()
            Description()
            Connection()
            Spacer()
        }
        .frame(maxWidth: .infinity)
        .background(OctoTheme.colors.externalObico2)
    }
}

private struct TitleBar : View {
    
    var body: some View {
        Image("obicoLarge")
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(OctoTheme.dimens.margin12)
    }
}

private struct Description : View {
    
    var body: some View {
        Text("configure_remote_acces___spaghetti_detective___description"~)
            .foregroundColor(.black)
            .multilineTextAlignment(.center)
            .frame(maxWidth: .infinity)
            .typographyBase()
            .padding([.leading, .trailing], OctoTheme.dimens.margin12)
    }
}

private struct Connection : View {
    
    @State private var showSignIn = false
    @StateObject private var viewModel = RemoteAccessObicoViewModel()
    @Namespace var animation
    @Environment(\.openURL) var openUrl
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin2) {
            if viewModel.connected == true {
                RemoteAccessConnected(text: "configure_remote_acces___spaghetti_detective___connected"~)
                
                OctoAsyncButton(
                    text: LocalizedStringKey("configure_remote_acces___spaghetti_detective___disconnect_button"),
                    type: .special(
                        background: OctoTheme.colors.white,
                        foreground: OctoTheme.colors.externalObico,
                        stroke: OctoTheme.colors.externalObico
                    ),
                    clickListener: {
                        try await viewModel.disconnect()
                    }
                )
                .matchedGeometryEffect(id: "button", in: animation)
                
                DataUsage(
                    dataUsage: viewModel.dataUsage,
                    retry: { viewModel.reloadDataUsage() }
                )
            } else if viewModel.connected == false {
                OctoAsyncButton(
                    text: LocalizedStringKey("configure_remote_acces___spaghetti_detective___connect_button"),
                    type: .special(
                        background: OctoTheme.colors.externalObico,
                        foreground: OctoTheme.colors.textColoredBackground,
                        stroke: .clear
                    ),
                    clickListener: {
                        await viewModel.getLoginUrl()
                        
                        DispatchQueue.main.async {
                            showSignIn = true
                        }
                    }
                )
                .matchedGeometryEffect(id: "button", in: animation)
                .sheet(isPresented: $showSignIn) {
                    if let url = viewModel.loginUrl {
                        SafariView(url: url)
                    } else {
                        ScreenError()
                    }
                }
                
                if viewModel.failure != nil {
                    RemoteAccessFailure(
                        failure: viewModel.failure,
                        background: surfaceBackground,
                        foreground: surfaceForeground,
                        highlight: OctoTheme.colors.externalObico
                    )
                } else {
                    RemoteAccessUspView(
                        foreground: surfaceForeground,
                        background: surfaceBackground,
                        usps: [
                            RemoteAccessUsp(
                                systemImageName: "checkmark.icloud.fill",
                                description: "configure_remote_acces___spaghetti_detective___usp_1"~
                            ),
                            RemoteAccessUsp(
                                systemImageName: "video.fill",
                                description: "configure_remote_acces___spaghetti_detective___usp_2"~
                            ),
                            RemoteAccessUsp(
                                systemImageName: "eye.fill",
                                description: "configure_remote_acces___spaghetti_detective___usp_3"~
                            ),
                            RemoteAccessUsp(
                                systemImageName: "lock.fill",
                                description: "configure_remote_acces___spaghetti_detective___usp_4"~
                            )
                        ]
                    )
                }
            }
        }
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        .animation(.default, value: viewModel.connected)
        .withInstanceId { viewModel.initWithInstance($0) }
        .onChange(of: viewModel.connected) { _ in
            showSignIn = false
        }
    }
}

private struct DataUsage : View {
    var dataUsage : FlowStateStruct<ObicoDataUsageStruct>
    var retry: () -> Void
    
    private var detailText: String {
        switch dataUsage {
        case .loading: return "loading"~
        case .error(_): return "configure_remote_acces___spaghetti_detective___data_usage_failed"~
        case .ready(let t): do {
            if t.hasDataCap {
                let df = DateFormatter()
                df.dateStyle = .short
                df.timeStyle = .none
                return String(
                    format: "configure_remote_acces___spaghetti_detective___data_usage_limited"~,
                    t.totalBytes.asStyleFileSize(intOnly: true),
                    t.monthlyCapBytes.asStyleFileSize(intOnly: true),
                    df.string(from: Date() + t.resetIn)
                )
            } else {
                return ""
            }
        }
        }
    }
    
    private var shouldShow : Bool {
        if case .ready(let du) = dataUsage {
            return du.hasDataCap
        } else {
            return true
        }
    }
    
    var body: some View {
        HStack() {
            if (shouldShow) {
                VStack(alignment: .leading, spacing: OctoTheme.dimens.margin01) {
                    Text("configure_remote_acces___spaghetti_detective___data_usafe"~)
                        .foregroundColor(surfaceForeground)
                        .typographySectionHeader()
                    
                    if case .ready(let du) = dataUsage {
                        ProgressView(value: Double(du.totalBytes), total: Double(du.monthlyCapBytes))
                            .tint(OctoTheme.colors.externalObico)
                    } else {
                        ProgressView(value: 0, total: 100)
                    }
                    
                    Text(detailText)
                        .foregroundColor(.black.opacity(0.75))
                        .typographyLabelSmall()
                }
                
                ZStack {
                    if dataUsage == .loading {
                        ProgressView()
                            .tint(.black)
                    } else {
                        OctoIconButton(
                            icon: "arrow.counterclockwise",
                            color: OctoTheme.colors.externalObico,
                            clickListener: retry
                        )
                    }
                }
                .frame(width: 50, height: 50)
                
            }
        }
        .padding([.top, .bottom, .leading], OctoTheme.dimens.margin12)
        .padding([.trailing], OctoTheme.dimens.margin01)
        .surface(color: .special(surfaceBackground))
        .animation(.default, value: dataUsage)
    }
}

private class RemoteAccessObicoViewModel : ObservableObject {
    private var currentCore: RemoteAccessObicoViewModelCore? = nil
    var bag:Set<AnyCancellable> = []
    var loginUrl: String? = nil
    @Published var connected: Bool? = nil
    @Published var failure: RemoteConnectionFailureStruct? = nil
    @Published var dataUsage: FlowStateStruct<ObicoDataUsageStruct> = .loading
    
    func initWithInstance(_ instanceId: String) {
        let core = RemoteAccessObicoViewModelCore(instanceId: instanceId)
        currentCore = core
        
        core.connectedState.asPublisher()
            .sink { (state: RemoteAccessBaseViewModelCore.State) in
                self.connected = state.connected
                self.failure = state.failure?.toStruct()
            }
            .store(in: &bag)
        
        core.dataUsage.asPublisher()
            .sink { (state: FlowState<SpaghettiDetectiveDataUsage>) in
                if state is FlowStateLoading {
                    self.dataUsage = .loading
                } else if let s = state as? FlowStateError {
                    self.dataUsage = .error(s.throwable)
                } else if let d = (state as? FlowStateReady)?.data {
                    self.dataUsage = .ready(d.toStruct())
                }
            }
            .store(in: &bag)
    }
    
    func disconnect() async throws {
        try await currentCore?.disconnect()
    }
    
    func reloadDataUsage() {
        Task {
            try? await currentCore?.reloadDataUsage()
        }
    }
    
    func getLoginUrl()  async {
        loginUrl = nil
        loginUrl = try? await currentCore?.getLoginUrl(baseUrl: nil)
    }
}

struct RemoteAccessObico_Previews: PreviewProvider {
    static var previews: some View {
        RemoteAccessObico()
            .previewDisplayName("General")
        
        DataUsage(
            dataUsage: .loading,
            retry: {}
        )
        .previewDisplayName("Data Usage / Loading")
        
        
        DataUsage(
            dataUsage: .error(
                KotlinException()
            ),
            retry: {}
        )
        .previewDisplayName("Data Usage / Error")
        
        DataUsage(
            dataUsage: .ready(
                ObicoDataUsageStruct(
                    monthlyCapBytes: 0,
                    resetIn: TimeInterval(123322),
                    totalBytes: 374678,
                    hasDataCap: false
                )
            ),
            retry: {}
        )
        .previewDisplayName("Data Usage / No Cap")
        
        DataUsage(
            dataUsage: .ready(
                ObicoDataUsageStruct(
                    monthlyCapBytes: 82334330,
                    resetIn: TimeInterval(123322),
                    totalBytes: 3746478,
                    hasDataCap: true
                )
            ),
            retry: {}
        )
        .previewDisplayName("Data Usage / Cap")
        
    }
}
