//
//  RemoteAccessNgrok.swift
//  OctoApp
//
//  Created by Christian on 30/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

private var surfaceBackground: Color { return OctoTheme.colors.externalNgrok2 }
private let surfaceForeground = Color.black

struct RemoteAccessNgrok: View {
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin2) {
            TitleBar()
            Description()
            Connection()
            Spacer()
        }
        .frame(maxWidth: .infinity)
        .background(.white)
    }
}

private struct TitleBar : View {
    
    var body: some View {
        HStack {
            Image("ngrokLarge")
            Spacer()
            Text("configure_remote_acces___ngrok___unofficial"~)
                .foregroundColor(.white)
                .padding([.top, .bottom], OctoTheme.dimens.margin0)
                .padding([.leading, .trailing], OctoTheme.dimens.margin1)
                .background(Capsule().fill(.black))
                .typographyBase()
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding(OctoTheme.dimens.margin12)
        .background(.white)
        .shadow(radius: 1)
    }
}

private struct Description : View {
    
    var body: some View {
        Text("configure_remote_acces___ngrok___description_1"~)
            .foregroundColor(.black)
            .multilineTextAlignment(.center)
            .frame(maxWidth: .infinity)
            .typographyBase()
            .padding([.leading, .trailing], OctoTheme.dimens.margin12)
    }
}

private struct Connection : View {
    
    @StateObject private var viewModel = RemoteAccesNgrokViewModel()
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin2) {
            if viewModel.connected {
                RemoteAccessConnected(text: "configure_remote_acces___ngrok___connected"~)
                Text(viewModel.url ?? "???")
                    .foregroundColor(.black)
                    .multilineTextAlignment(.center)
                    .frame(maxWidth: .infinity)
                    .typographySectionHeader()
                    .padding([.leading, .trailing], OctoTheme.dimens.margin12)
            } else {
                Text("configure_remote_acces___ngrok___description_2"~)
                    .foregroundColor(.black)
                    .multilineTextAlignment(.center)
                    .frame(maxWidth: .infinity)
                    .typographySectionHeader()
                    .padding([.leading, .trailing], OctoTheme.dimens.margin12)
                
                if viewModel.failure != nil {
                    RemoteAccessFailure(
                        failure: nil, //.failure,
                        background: surfaceBackground,
                        foreground: surfaceForeground,
                        highlight: OctoTheme.colors.externalOctoEverywhere3
                    )
                } else {
                    RemoteAccessUspView(
                        foreground: surfaceForeground,
                        background: surfaceBackground,
                        usps: [
                            RemoteAccessUsp(
                                systemImageName: "checkmark.icloud.fill",
                                description: "configure_remote_acces___ngrok___usp_1"~
                            ),
                            RemoteAccessUsp(
                                systemImageName: "exclamationmark.triangle",
                                description: "configure_remote_acces___ngrok___usp_2"~
                            ),
                            RemoteAccessUsp(
                                systemImageName: "wand.and.stars.inverse",
                                description: "configure_remote_acces___ngrok___usp_3"~
                            ),
                            RemoteAccessUsp(
                                systemImageName: "lock.fill",
                                description: "configure_remote_acces___ngrok___usp_4"~
                            )
                        ]
                    )
                }
            }
            Spacer()
            Text("configure_remote_acces___ngrok___disclaimer"~)
                .foregroundColor(.black.opacity(0.75))
                .multilineTextAlignment(.center)
                .typographyLabelSmall()
        }
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        .withInstanceId { viewModel.initWithInstance($0) }
    }
}

private class RemoteAccesNgrokViewModel : ObservableObject {
    private var currentCore: RemoteAccessNgrokViewModelCore? = nil
    var bag:Set<AnyCancellable> = []
    @Published var connected: Bool = false
    @Published var url: String? = nil
    @Published var failure: RemoteConnectionFailureStruct? = nil
    
    func initWithInstance(_ instanceId: String) {
        let core = RemoteAccessNgrokViewModelCore(instanceId: instanceId)
        currentCore = core
        
        core.connectedState.asPublisher()
            .sink { (state: RemoteAccessBaseViewModelCore.State) in
                self.connected = state.connected
                self.url = state.url
                self.failure = state.failure?.toStruct()
            }
            .store(in: &bag)
    }
}

struct RemoteAccessNgrok_Previews: PreviewProvider {
    static var previews: some View {
        RemoteAccessNgrok()
    }
}

