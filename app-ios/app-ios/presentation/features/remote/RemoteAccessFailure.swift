//
//  RemoteAccessFailure.swift
//  OctoApp
//
//  Created by Christian on 30/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct RemoteAccessFailure: View {
    var failure: RemoteConnectionFailureStruct?
    var background: Color
    var foreground: Color
    var highlight: Color
    
    @State private var showError = false
    @State private var showDetails = false
    
    var body: some View {
        if let f = failure {
            HStack(alignment: .center, spacing: OctoTheme.dimens.margin1) {
                Text(String(format: "configure_remote_access___failure"~, f.date.formatted()))
                    .foregroundColor(foreground)
                    .typographyBase()
                    .frame(maxWidth: .infinity)
                
                OctoIconButton(
                    icon: "info.circle.fill",
                    color: highlight,
                    clickListener: { showError = true }
                )
            }
            .padding(OctoTheme.dimens.margin1)
            .surface(color: .special(background))
            .alert(failure?.errorMessage ?? "error_general"~, isPresented: $showError) {
                if failure?.errorMessageStack != nil {
                    Button("show_details"~) { showError = false; showDetails = true }
                }
                Button("done"~) { showError = false }
            }
            .alert(failure?.errorMessage ?? "???", isPresented: $showDetails, actions: {
                Button("done"~) { showError = false }
            }, message: {
                Text(failure?.errorMessageStack ?? "???")
            })
        }
    }
}

struct RemoteAccessFailure_Previews: PreviewProvider {
    static var previews: some View {
        RemoteAccessFailure(
            failure: RemoteConnectionFailureStruct(
                errorMessage: "Something went wrong",
                errorMessageStack: "Stack",
                stackTrace: "Trace",
                remoteServiceName: "some",
                date: Date()
            ),
            background: OctoTheme.colors.inputBackground,
            foreground: OctoTheme.colors.darkText,
            highlight: OctoTheme.colors.accent
        )
        .previewDisplayName("Some")
        
        RemoteAccessFailure(
            failure: nil,
            background: OctoTheme.colors.inputBackground,
            foreground: OctoTheme.colors.darkText,
            highlight: OctoTheme.colors.accent
        )
        .previewDisplayName("None")
    }
}
