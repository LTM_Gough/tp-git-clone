package de.crysxd.octoapp.benchmark.wear

import androidx.benchmark.macro.CompilationMode
import androidx.benchmark.macro.FrameTimingMetric
import androidx.benchmark.macro.StartupMode
import androidx.benchmark.macro.StartupTimingMetric
import androidx.benchmark.macro.junit4.MacrobenchmarkRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import de.crysxd.octoapp.tests.rules.InitBenchmarkAppRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class BaselineProfileBenchmark {
    private val benchmarkRule = MacrobenchmarkRule()

    @get:Rule
    val chain = RuleChain.outerRule(benchmarkRule)
        .around(InitBenchmarkAppRule())
        .around(TestDocumentationRule(showNotification = false))

    @Test
    fun benchmarkStartupNone() = benchmarkStartup(CompilationMode.None())

    @Test
    fun benchmarkStartupPartial() = benchmarkStartup(CompilationMode.Partial())

    @Test
    fun benchmarkStartupFull() = benchmarkStartup(CompilationMode.Full())

    private fun benchmarkStartup(compilationMode: CompilationMode) = benchmarkRule.measureRepeated(
        packageName = "de.crysxd.octoapp",
        metrics = listOf(StartupTimingMetric()),
        iterations = 5,
        startupMode = StartupMode.COLD,
        compilationMode = compilationMode,
    ) {
        launchApp()
    }

    @Test
    fun benchmarkJourneyNone() = benchmarkJourney(CompilationMode.None())

    @Test
    fun benchmarkJourneyPartial() = benchmarkJourney(CompilationMode.Partial())

    @Test
    fun benchmarkJourneyFull() = benchmarkJourney(CompilationMode.Full())

    private fun benchmarkJourney(compilationMode: CompilationMode) = benchmarkRule.measureRepeated(
        packageName = "de.crysxd.octoapp",
        metrics = listOf(FrameTimingMetric()),
        iterations = 1,
        compilationMode = compilationMode,
        startupMode = StartupMode.COLD
    ) {
        launchApp()
        runBenchmark()
    }
}
