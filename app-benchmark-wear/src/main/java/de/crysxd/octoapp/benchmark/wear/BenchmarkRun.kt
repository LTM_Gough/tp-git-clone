package de.crysxd.octoapp.benchmark.wear

import androidx.benchmark.macro.MacrobenchmarkScope
import androidx.test.uiautomator.By
import androidx.test.uiautomator.Direction
import androidx.test.uiautomator.Until


fun MacrobenchmarkScope.launchApp() {
    // Wear doesn't launch the app from home??? Press recent apps to reliably launch the app.
    device.pressRecentApps()
    startActivityAndWait()
}

fun MacrobenchmarkScope.runBenchmark() = with(device) {
    wait(Until.hasObject(By.res("main-pager")), 15_000)
    findObject(By.res("main-pager")).swipe(Direction.LEFT, 1f)
    findObject(By.res("main-pager")).swipe(Direction.LEFT, 1f)
    Thread.sleep(1000)
    findObject(By.res("main-column")).scroll(Direction.DOWN, 1f)
    Thread.sleep(1000)
    findObject(By.res("main-pager")).swipe(Direction.RIGHT, 1f)
    Thread.sleep(1000)
    repeat(5) {
        findObject(By.res("main-column")).scroll(Direction.DOWN, 1f)
    }
}