#!/bin/bash
./gradlew :app-wear:assembleBenchmark :app-benchmark-wear:assembleBenchmark
adb -s emulator-5554 root
adb -s emulator-5554 install app-wear/build/outputs/apk/benchmark/app-wear-benchmark.apk
adb -s emulator-5554 install app-benchmark-wear/build/outputs/apk/benchmark/app-benchmark-wear-benchmark.apk
adb -s emulator-5554 shell am instrument -e class de.crysxd.octoapp.benchmark.wear.BaselineProfileGenerator -w de.crysxd.octoapp.benchmark.wear
adb -s emulator-5554 pull "/storage/emulated/0/Android/media/de.crysxd.octoapp.benchmark.wear/BaselineProfileGenerator_journey-baseline-prof.txt" app-wear/src/main/baseline-prof.txt