package de.crysxd.octoapp.sharedexternalapis.mjpeg

import de.crysxd.octoapp.engine.exceptions.PrinterApiException
import de.crysxd.octoapp.sharedcommon.http.DefaultHttpClient
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import io.github.aakira.napier.Napier
import io.ktor.client.request.prepareGet
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.bodyAsChannel
import io.ktor.client.statement.bodyAsText
import io.ktor.http.Url
import io.ktor.utils.io.ByteReadChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.map
import java.io.IOException
import kotlin.time.Duration.Companion.seconds
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

internal actual class MjpegSocket actual constructor(
    maxImageSize: Int?,
    private val url: Url,
    httpClientSettings: HttpClientSettings,
    private val logTag: String,
) {

    private val tempCache = ByteArray(16_384)
    private val cache = MjpegImageCache(maxImageSize = maxImageSize, logTag = logTag)
    private val httpClient = DefaultHttpClient(
        settings = httpClientSettings,
        logTag = "KTOR/$logTag",
    )

    // Some funky stuff is going on here when throwing exception inside the connect {} block. To fix this we emit exceptions
    // and then throw them below in the map block. We also need channelFlow to make this work!
    actual fun connect(): Flow<MjpegConnection3.MjpegSnapshot> = channelFlow {
        connect { boundary, inputStream ->
            try {
                cache.reset()

                while (true) {
                    val (frame, analytics) = readNextImage(cache, boundary, inputStream)
                    send(MjpegConnection3.MjpegSnapshot.Frame(frame, analytics))
                }
            } catch (e: Exception) {
                Napier.w(tag = logTag, message = "Caught exception, emitting and throwing")
                send(e)
                throw e
            }
        }
    }.map {
        when (it) {
            is MjpegConnection3.MjpegSnapshot -> it
            is Throwable -> throw  it
            else -> throw IllegalStateException("Unable to handle $it")
        }
    }

    @OptIn(ExperimentalTime::class)
    private suspend fun readNextImage(cache: MjpegImageCache, boundary: String, input: ByteReadChannel, dropCount: Int = 0): Pair<Image, MjpegConnection3.Analytics> {
        var boundaryStart: Int?
        var boundaryEnd: Int?
        var nextOffset = 0
        if (dropCount >= 5) {
            throw  IOException("Too many dropped frames")
        }
        var readTime = 0.seconds
        var searchTime = 0.seconds

        do {
            val read: Int
            readTime += measureTime { read = input.readAvailable(tempCache, 0, tempCache.size) }
            if (read < 0) throw IOException("Connection broken")
            cache.push(tempCache, read)
            val bounds: MjpegImageCache.IndexResult
            searchTime += measureTime { bounds = cache.indexOf(nextOffset, boundary) }
            boundaryStart = bounds.start
            boundaryEnd = bounds.end
            nextOffset = bounds.nextOffset
        } while (boundaryStart == null || boundaryEnd == null)

        val frame: Image?
        val decodeTime = measureTime { frame = cache.readImage(boundaryStart, boundaryEnd) }
        return frame?.let {
            it to MjpegConnection3.Analytics(
                readTime = readTime,
                searchTime = searchTime,
                decodeTime = decodeTime,
                dropCount = dropCount,
                byteCount = boundaryStart,
                fps = 0f,
            )
        } ?: readNextImage(cache, boundary, input, dropCount + 1)
    }

    private suspend fun connect(then: suspend (String, ByteReadChannel) -> Unit) {
        Napier.i(tag = logTag, message = "Connecting")
        httpClient.prepareGet(url).execute { httpResponse ->
            if (httpResponse.status.value !in 200..299) {
                throw PrinterApiException(url, httpResponse.status.value, httpResponse.bodyAsText())
            }

            Napier.i(tag = logTag, message = "Connected with ${httpResponse.status.value}, getting boundary")
            val boundary = extractBoundary(httpResponse)
            Napier.i(tag = logTag, message = "Boundary extracted, starting to load images ($boundary)")
            then(boundary, httpResponse.bodyAsChannel())
        }
    }

    private fun extractBoundary(response: HttpResponse): String {
        // Try to extract a boundary from HTTP header first.
        // If the information is not presented, throw an exception and use default value instead.
        val contentType: String = response.headers["Content-Type"] ?: throw Exception("Unable to get content type")
        val types = contentType.split(";".toRegex()).toTypedArray()
        if (types.none { it.startsWith("multipart/") }) {
            throw MjpegConnection3.NoImageResourceException(contentType)
        }

        var extractedBoundary: String? = null
        for (ct in types) {
            val trimmedCt = ct.trim { it <= ' ' }
            if (trimmedCt.startsWith("boundary=")) {
                extractedBoundary = trimmedCt.removePrefix("boundary=").removePrefix("--") // Content after 'boundary='
            }
        }
        return if (extractedBoundary == null) {
            throw Exception("Unable to find mjpeg boundary from $types")
        } else if (extractedBoundary.first() == '"' && extractedBoundary.last() == '"') {
            "--" + extractedBoundary.removePrefix("\"").removeSuffix("\"")
        } else {
            "--$extractedBoundary"
        }
    }
}