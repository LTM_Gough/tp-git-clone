package de.crysxd.octoapp.sharedexternalapis.mjpeg

import android.graphics.Bitmap

actual typealias Image = Bitmap