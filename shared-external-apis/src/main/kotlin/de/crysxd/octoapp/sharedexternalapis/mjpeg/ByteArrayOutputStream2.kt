/*
 * Copyright (c) 1994, 2013, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package de.crysxd.octoapp.sharedexternalapis.mjpeg

import java.io.IOException
import java.io.OutputStream
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.util.Arrays

/**
 * This class implements an output stream in which the data is
 * written into a byte array. The buffer automatically grows as data
 * is written to it.
 * The data can be retrieved using `toByteArray()` and
 * `toString()`.
 *
 *
 * Closing a <tt>ByteArrayOutputStream</tt> has no effect. The methods in
 * this class can be called after the stream has been closed without
 * generating an <tt>IOException</tt>.
 *
 * @author Arthur van Hoff
 * @since JDK1.0
 */
internal class ByteArrayOutputStream2 @JvmOverloads constructor(size: Int = 32) : OutputStream() {
    /**
     * The buffer where data is stored.
     */
    private var buf: ByteArray

    /**
     * The number of valid bytes in the buffer.
     */
    private var count = 0
    /**
     * Creates a new byte array output stream, with a buffer capacity of
     * the specified size, in bytes.
     *
     * @param size the initial size.
     * @throws IllegalArgumentException if size is negative.
     */
    /**
     * Creates a new byte array output stream. The buffer capacity is
     * initially 32 bytes, though its size increases if necessary.
     */
    init {
        require(size >= 0) {
            ("Negative initial size: "
                    + size)
        }
        buf = ByteArray(size)
    }

    /**
     * Increases the capacity if necessary to ensure that it can hold
     * at least the number of elements specified by the minimum
     * capacity argument.
     *
     * @param minCapacity the desired minimum capacity
     * @throws OutOfMemoryError if `minCapacity < 0`.  This is
     * interpreted as a request for the unsatisfiably large capacity
     * `(long) Integer.MAX_VALUE + (minCapacity - Integer.MAX_VALUE)`.
     */
    private fun ensureCapacity(minCapacity: Int) {
        // overflow-conscious code
        if (minCapacity - buf.size > 0) grow(minCapacity)
    }

    /**
     * Increases the capacity to ensure that it can hold at least the
     * number of elements specified by the minimum capacity argument.
     *
     * @param minCapacity the desired minimum capacity
     */
    private fun grow(minCapacity: Int) {
        // overflow-conscious code
        val oldCapacity = buf.size
        var newCapacity = oldCapacity shl 1
        if (newCapacity - minCapacity < 0) newCapacity = minCapacity
        if (newCapacity < 0) {
            if (minCapacity < 0) throw OutOfMemoryError()
            newCapacity = Int.MAX_VALUE
        }
        buf = Arrays.copyOf(buf, newCapacity)
    }

    /**
     * Writes the specified byte to this byte array output stream.
     *
     * @param b the byte to be written.
     */
    @Synchronized
    override fun write(b: Int) {
        ensureCapacity(count + 1)
        buf[count] = b.toByte()
        count += 1
    }

    /**
     * Writes `len` bytes from the specified byte array
     * starting at offset `off` to this byte array output stream.
     *
     * @param b   the data.
     * @param off the start offset in the data.
     * @param len the number of bytes to write.
     */
    @Synchronized
    override fun write(b: ByteArray, off: Int, len: Int) {
        if (off < 0 || off > b.size || len < 0 ||
            off + len - b.size > 0
        ) {
            throw IndexOutOfBoundsException()
        }
        ensureCapacity(count + len)
        System.arraycopy(b, off, buf, count, len)
        count += len
    }

    /**
     * Writes the complete contents of this byte array output stream to
     * the specified output stream argument, as if by calling the output
     * stream's write method using `out.write(buf, 0, count)`.
     *
     * @param out the output stream to which to write the data.
     * @throws IOException if an I/O error occurs.
     */
    @Synchronized
    @Throws(IOException::class)
    fun writeTo(out: OutputStream) {
        out.write(buf, 0, count)
    }

    /**
     * Resets the `count` field of this byte array output
     * stream to zero, so that all currently accumulated output in the
     * output stream is discarded. The output stream can be used again,
     * reusing the already allocated buffer space.
     *
     * @see java.io.ByteArrayInputStream.count
     */
    @Synchronized
    fun reset() {
        count = 0
    }

    /**
     * Creates a newly allocated byte array. Its size is the current
     * size of this output stream and the valid contents of the buffer
     * have been copied into it.
     *
     * @return the current contents of this output stream, as a byte array.
     * @see java.io.ByteArrayOutputStream.size
     */
    @Synchronized
    fun toByteArray(): ByteArray {
        return buf
    }

    /**
     * Returns the current size of the buffer.
     *
     * @return the value of the `count` field, which is the number
     * of valid bytes in this output stream.
     * @see java.io.ByteArrayOutputStream.count
     */
    @Synchronized
    fun size(): Int {
        return count
    }

    /**
     * Converts the buffer's contents into a string decoding bytes using the
     * platform's default character set. The length of the new <tt>String</tt>
     * is a function of the character set, and hence may not be equal to the
     * size of the buffer.
     *
     *
     *  This method always replaces malformed-input and unmappable-character
     * sequences with the default replacement string for the platform's
     * default character set. The [java.nio.charset.CharsetDecoder]
     * class should be used when more control over the decoding process is
     * required.
     *
     * @return String decoded from the buffer's contents.
     * @since JDK1.1
     */
    @Synchronized
    override fun toString(): String {
        return String(buf, 0, count)
    }

    /**
     * Converts the buffer's contents into a string by decoding the bytes using
     * the named [charset][java.nio.charset.Charset]. The length of the new
     * <tt>String</tt> is a function of the charset, and hence may not be equal
     * to the length of the byte array.
     *
     *
     *  This method always replaces malformed-input and unmappable-character
     * sequences with this charset's default replacement string. The [ ] class should be used when more control
     * over the decoding process is required.
     *
     * @param charsetName the name of a supported
     * [charset][java.nio.charset.Charset]
     * @return String decoded from the buffer's contents.
     * @throws UnsupportedEncodingException If the named charset is not supported
     * @since JDK1.1
     */
    @Synchronized
    @Throws(UnsupportedEncodingException::class)
    fun toString(charsetName: String): String {
        return String(buf, 0, count, Charset.forName(charsetName))
    }

    /**
     * Closing a <tt>ByteArrayOutputStream</tt> has no effect. The methods in
     * this class can be called after the stream has been closed without
     * generating an <tt>IOException</tt>.
     */
    @Throws(IOException::class)
    override fun close() {
    }
}