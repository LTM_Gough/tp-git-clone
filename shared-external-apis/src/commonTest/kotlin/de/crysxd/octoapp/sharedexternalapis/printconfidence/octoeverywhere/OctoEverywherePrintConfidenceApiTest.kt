package de.crysxd.octoapp.sharedexternalapis.printconfidence.octoeverywhere

import de.crysxd.octoapp.sharedexternalapis.http.GenericHttpClientFactory
import de.crysxd.octoapp.sharedexternalapis.printconfidence.PrintConfidence
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue

class OctoEverywherePrintConfidenceApiTest {

    @Test
    fun WHEN_status_is_queried_THEN_response_is_parsed() = runBlocking {
        //region GIVEN
        val appToken = "1234"
        val httpClient = GenericHttpClientFactory(
            MockEngine {
                assertEquals(
                    expected = HttpMethod.Post,
                    actual = it.method,
                    message = "Expected POST"
                )
                assertEquals(
                    expected = appToken,
                    actual = it.headers["AppToken"],
                    message = "Expected app token"
                )
                respond(
                    content = "{\n" +
                            "    \"Status\": 200,\n" +
                            "    \"Error\": \"\",\n" +
                            "    \"IsUserError\": false,\n" +
                            "    \"Result\": {\n" +
                            "        \"Status\": \"Looking Great\",\n" +
                            "        \"StatusColor\": \"g\",\n" +
                            "        \"Rating\": 10,\n" +
                            "        \"State\": 3\n" +
                            "    }\n" +
                            "}",
                    headers = headersOf(HttpHeaders.ContentType, ContentType.Application.Json.toString()),
                    status = HttpStatusCode.OK
                )
            }
        )
        val target = OctoEverywherePrintConfidenceApi(appToken, httpClient)
        //endregion
        //region WHEN
        val quality = target.checkConfidence()
        //endregion
        //region THEN
        assertEquals(
            actual = quality,
            expected = PrintConfidence(
                description = "Looking Great",
                confidence = 1f,
                origin = PrintConfidence.Origin.OctoEverywhere,
                level = PrintConfidence.Level.High
            ),
            message = "Expected quality to match"
        )
        //endregion
    }

    @Test
    fun WHEN_status_is_queried_but_disabled_THEN_response_is_parsed() = runBlocking {
        //region GIVEN
        val appToken = "1234"
        val httpClient = GenericHttpClientFactory(
            MockEngine {
                respond(
                    content = "{ \"Result\": {\"Status\": \"Some description\",\"StatusColor\": \"s\",\"Rating\": 10,\"State\": 0}}",
                    headers = headersOf(HttpHeaders.ContentType, ContentType.Application.Json.toString()),
                    status = HttpStatusCode.OK
                )
            }
        )
        val target = OctoEverywherePrintConfidenceApi(appToken, httpClient)
        //endregion
        //region WHEN
        val response = target.checkConfidence()
        //endregion
        //region THEN
        assertNull(
            actual = response,
            message = "Expected exception to match"
        )
        //endregion
    }

    @Test
    fun WHEN_status_is_queried_but_temporarily_disabled_THEN_response_is_parsed() = runBlocking {
        //region GIVEN
        val appToken = "1234"
        val httpClient = GenericHttpClientFactory(
            MockEngine {
                respond(
                    content = "{ \"Result\": {\"Status\": \"Some description\",\"StatusColor\": \"s\",\"Rating\": 10,\"State\": 1}}",
                    headers = headersOf(HttpHeaders.ContentType, ContentType.Application.Json.toString()),
                    status = HttpStatusCode.OK
                )
            }
        )
        val target = OctoEverywherePrintConfidenceApi(appToken, httpClient)
        //endregion
        //region WHEN
        val result = target.checkConfidence()
        //endregion
        //region THEN
        assertNull(
            actual = result,
            message = "Expected exception to match"
        )
        //endregion
    }

    @Test
    fun WHEN_status_is_queried_but_plugin_outdated_THEN_response_is_parsed() = runBlocking {
        //region GIVEN
        val appToken = "1234"
        val httpClient = GenericHttpClientFactory(
            MockEngine {
                respond(
                    content = "",
                    headers = headersOf(HttpHeaders.ContentType, ContentType.Application.Json.toString()),
                    status = HttpStatusCode(610, "Outdated")
                )
            }
        )
        val target = OctoEverywherePrintConfidenceApi(appToken, httpClient)
        //endregion
        //region WHEN
        val quality = target.checkConfidence()
        //endregion
        //region THEN
        assertEquals(
            actual = quality,
            expected = PrintConfidence(
                description = "OctoEverywhere plugin outdated",
                confidence = null,
                origin = PrintConfidence.Origin.OctoEverywhere,
                level = PrintConfidence.Level.Low
            ),
            message = "Expected quality to match"
        )
        //endregion
    }

    @Test
    fun WHEN_status_is_queried_but_not_beta_member_THEN_response_is_parsed() = runBlocking {
        //region GIVEN
        val appToken = "1234"
        val httpClient = GenericHttpClientFactory(
            MockEngine {
                respond(
                    content = "",
                    headers = headersOf(HttpHeaders.ContentType, ContentType.Application.Json.toString()),
                    status = HttpStatusCode(611, "Outdated")
                )
            }
        )
        val target = OctoEverywherePrintConfidenceApi(appToken, httpClient)
        //endregion
        //region WHEN
        val e = try {
            target.checkConfidence()
            null
        } catch (e: Exception) {
            e
        }
        //endregion
        //region THEN
        assertTrue(
            actual = e is OctoEverywhereDisconnectedException,
            message = "Expected exception to match"
        )
        //endregion
    }

    @Test
    fun WHEN_status_is_queried_but_app_connection_missing_THEN_response_is_parsed() = runBlocking {
        //region GIVEN
        val appToken = "1234"
        val httpClient = GenericHttpClientFactory(
            MockEngine {
                respond(
                    content = "",
                    headers = headersOf(HttpHeaders.ContentType, ContentType.Application.Json.toString()),
                    status = HttpStatusCode(603, "Outdated")
                )
            }
        )
        val target = OctoEverywherePrintConfidenceApi(appToken, httpClient)
        //endregion
        //region WHEN
        val e = try {
            target.checkConfidence()
            null
        } catch (e: Exception) {
            e
        }
        //endregion
        //region THEN
        assertTrue(
            actual = e is OctoEverywhereDisconnectedException,
            message = "Expected exception to match"
        )
        //endregion
    }

    @Test
    fun WHEN_status_is_queried_but_app_connection_invalid_THEN_response_is_parsed() = runBlocking {
        //region GIVEN
        val appToken = "1234"
        val httpClient = GenericHttpClientFactory(
            MockEngine {
                respond(
                    content = "",
                    headers = headersOf(HttpHeaders.ContentType, ContentType.Application.Json.toString()),
                    status = HttpStatusCode(604, "Outdated")
                )
            }
        )
        val target = OctoEverywherePrintConfidenceApi(appToken, httpClient)
        //endregion
        //region WHEN
        val e = try {
            target.checkConfidence()
            null
        } catch (e: Exception) {
            e
        }
        //endregion
        //region THEN
        assertTrue(
            actual = e is OctoEverywhereDisconnectedException,
            message = "Expected exception to match"
        )
        //endregion
    }

    @Test
    fun WHEN_status_is_queried_but_offline_THEN_response_is_parsed() = runBlocking {
        //region GIVEN
        val appToken = "1234"
        val httpClient = GenericHttpClientFactory(
            MockEngine {
                respond(
                    content = "",
                    headers = headersOf(HttpHeaders.ContentType, ContentType.Application.Json.toString()),
                    status = HttpStatusCode(601, "Outdated")
                )
            }
        )
        val target = OctoEverywherePrintConfidenceApi(appToken, httpClient)
        //endregion
        //region WHEN
        val quality = target.checkConfidence()
        //endregion
        //region THEN
        assertEquals(
            actual = quality,
            expected = PrintConfidence(
                description = "Can't see your print",
                confidence = null,
                origin = PrintConfidence.Origin.OctoEverywhere,
                level = PrintConfidence.Level.Low
            ),
            message = "Expected quality to match"
        )
        //endregion
    }

    @Test
    fun WHEN_status_is_queried_but_timeout_THEN_response_is_parsed() = runBlocking {
        //region GIVEN
        val appToken = "1234"
        val httpClient = GenericHttpClientFactory(
            MockEngine {
                respond(
                    content = "",
                    headers = headersOf(HttpHeaders.ContentType, ContentType.Application.Json.toString()),
                    status = HttpStatusCode(602, "Outdated")
                )
            }
        )
        val target = OctoEverywherePrintConfidenceApi(appToken, httpClient)
        //endregion
        //region WHEN
        val quality = target.checkConfidence()
        //endregion
        //region THEN
        assertEquals(
            actual = quality,
            expected = PrintConfidence(
                description = "Can't see your print",
                confidence = null,
                origin = PrintConfidence.Origin.OctoEverywhere,
                level = PrintConfidence.Level.Low
            ),
            message = "Expected quality to match"
        )
        //endregion
    }

    @Test
    fun WHEN_status_is_queried_but_error_THEN_response_is_parsed() = runBlocking {
        //region GIVEN
        val appToken = "1234"
        val httpClient = GenericHttpClientFactory(
            MockEngine {
                respond(
                    content = "",
                    headers = headersOf(HttpHeaders.ContentType, ContentType.Application.Json.toString()),
                    status = HttpStatusCode(500, "Outdated")
                )
            }
        )
        val target = OctoEverywherePrintConfidenceApi(appToken, httpClient)
        //endregion
        //region WHEN
        val result = target.checkConfidence()
        //endregion
        //region THEN
        assertNull(
            actual = result,
            message = "Expected exception to match"
        )
        //endregion
    }
}