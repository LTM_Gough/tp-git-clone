package de.crysxd.octoapp.sharedexternalapis.tutorials.model

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.days

data class Tutorial(
    val title: String,
    val description: String,
    val thumbnails: List<Thumbnail>,
    val publishedAt: Instant,
    val url: String,
) {
    fun isNew(seenUpUntil: Instant): Boolean {
        val oldestForNew = Clock.System.now() - 30.days
        return publishedAt > oldestForNew && publishedAt > seenUpUntil
    }

    data class Thumbnail(
        val url: String?,
        val width: Int,
        val height: Int,
    )
}