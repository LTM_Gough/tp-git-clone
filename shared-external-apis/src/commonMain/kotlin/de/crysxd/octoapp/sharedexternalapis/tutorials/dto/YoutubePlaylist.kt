package de.crysxd.octoapp.sharedexternalapis.tutorials.dto

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
internal data class YoutubePlaylist(
    val items: List<PlaylistItem> = emptyList()
) {
    @Serializable
    data class PlaylistItem(
        val snippet: Snippet?,
        val contentDetails: ContentDetails?,
    ) {

        @Serializable
        data class Snippet(
            val title: String?,
            val description: String?,
            val thumbnails: Map<String, Thumbnail>?,
        )

        @Serializable
        data class Thumbnail(
            val url: String?,
            val width: Int,
            val height: Int,
        )

        @Serializable
        data class ContentDetails(
            val videoId: String?,
            val videoPublishedAt: Instant?,
        )
    }
}