package de.crysxd.octoapp.sharedexternalapis.mjpeg

import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import io.ktor.http.Url
import kotlinx.coroutines.flow.Flow

internal expect class MjpegSocket(
    maxImageSize: Int?,
    url: Url,
    httpClientSettings: HttpClientSettings,
    logTag: String,
) {

    fun connect(): Flow<MjpegConnection3.MjpegSnapshot>

}