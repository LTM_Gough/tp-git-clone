package de.crysxd.octoapp.sharedexternalapis.http

import de.crysxd.octoapp.sharedcommon.http.logging.StyledLogger
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.plugins.cache.HttpCache
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logging
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json

fun GenericHttpClientFactory(engine: HttpClientEngine? = null): HttpClient {
    val intializer: HttpClientConfig<*>.() -> Unit = {
        expectSuccess = true
        install(HttpCache)
        install(ContentNegotiation) {
            json(
                Json {
                    ignoreUnknownKeys = true
                }
            )
        }
        install(Logging) {
            level = LogLevel.HEADERS
            logger = StyledLogger(logTag = "KTOR/External")
        }
    }

    return if (engine == null) {
        HttpClient(intializer)
    } else {
        HttpClient(engine, intializer)
    }
}
