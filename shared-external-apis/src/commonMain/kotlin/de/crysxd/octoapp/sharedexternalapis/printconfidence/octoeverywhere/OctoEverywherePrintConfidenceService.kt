package de.crysxd.octoapp.sharedexternalapis.printconfidence.octoeverywhere

import de.crysxd.octoapp.sharedexternalapis.printconfidence.PrintConfidenceService
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.retryWhen
import kotlin.time.Duration.Companion.seconds

internal class OctoEverywherePrintConfidenceService(
    private val api: OctoEverywherePrintConfidenceApi
) : PrintConfidenceService {

    override suspend fun checkPrintQuality() = flow {
        while (true) {
            val r = api.checkConfidence()
            Napier.v("Checking print confidence from OctoEverywhere: $r")
            emit(r)
            delay(12.seconds)
        }
    }.retryWhen { e, attempt ->
        if (e is OctoEverywhereDisconnectedException) {
            Napier.e(message = "Gadget not available, stopping")
            false
        } else {
            val delay = (5.seconds * attempt.toInt()).coerceAtMost(30.seconds)
            Napier.e(message = "Gadget error, retrying after $delay", throwable = e)
            delay(delay)
            true
        }
    }
}