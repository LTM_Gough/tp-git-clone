package de.crysxd.octoapp.sharedexternalapis.tutorials

import de.crysxd.octoapp.sharedexternalapis.http.GenericHttpClientFactory
import de.crysxd.octoapp.sharedexternalapis.tutorials.dto.YoutubePlaylist
import de.crysxd.octoapp.sharedexternalapis.tutorials.model.Tutorial
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.url
import io.ktor.http.HttpHeaders
import kotlinx.datetime.Instant

class TutorialApi(
    private val httpClient: HttpClient = GenericHttpClientFactory()
) {

    private val versionFilter = Regex("#octoapp:min:(\\d+)")

    suspend fun getTutorials(skipCache: Boolean, currentVersion: Int) = httpClient.get {
        url("https://octoapp.eu/config/tutorials.json")
        if (skipCache) {
            header(HttpHeaders.CacheControl, "max-age=0")
        } else {
            header(HttpHeaders.CacheControl, "public, max-age=1, stale-if-error")
        }
    }.body<YoutubePlaylist>().items.mapNotNull {
        Tutorial(
            title = it.snippet?.title?.removePrefix("OctoApp Tutorials:")?.trim() ?: return@mapNotNull null,
            description = it.snippet.description ?: return@mapNotNull null,
            publishedAt = it.contentDetails?.videoPublishedAt ?: Instant.DISTANT_PAST,
            thumbnails = it.snippet.thumbnails?.values?.map { t -> Tutorial.Thumbnail(url = t.url, width = t.width, height = t.height) } ?: emptyList(),
            url = it.contentDetails?.videoId?.let { vid -> "https://www.youtube.com/watch?v=${vid}&list=PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ" } ?: return@mapNotNull null
        )
    }.filter {
        versionFilter.find(it.description)?.groupValues?.getOrNull(1)?.toIntOrNull()?.let { minVersion -> currentVersion >= minVersion } ?: true
    }.sortedByDescending { it.publishedAt }
}