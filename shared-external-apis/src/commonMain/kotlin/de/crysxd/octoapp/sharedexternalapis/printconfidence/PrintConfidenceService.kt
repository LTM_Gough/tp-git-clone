package de.crysxd.octoapp.sharedexternalapis.printconfidence

import kotlinx.coroutines.flow.Flow

interface PrintConfidenceService {
    suspend fun checkPrintQuality(): Flow<PrintConfidence?>
}
