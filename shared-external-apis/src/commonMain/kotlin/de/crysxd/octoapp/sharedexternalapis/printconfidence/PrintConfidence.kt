package de.crysxd.octoapp.sharedexternalapis.printconfidence

data class PrintConfidence(
    val description: String,
    val confidence: Float?,
    val origin: Origin,
    val level: Level,
) {
    enum class Origin {
        OctoEverywhere, Obico
    }

    enum class Level {
        High, Medium, Low
    }
}