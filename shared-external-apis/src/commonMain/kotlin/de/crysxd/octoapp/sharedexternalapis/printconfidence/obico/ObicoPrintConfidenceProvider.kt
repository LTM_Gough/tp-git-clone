package de.crysxd.octoapp.sharedexternalapis.printconfidence.obico

import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder
import de.crysxd.octoapp.sharedexternalapis.printconfidence.PrintConfidenceService

class ObicoPrintConfidenceProvider {
    fun forBaseUrlAndApiKey(baseUrl: String, apiKey: String): PrintConfidenceService = ObicoPrintConfidenceService(
        api = OctoPrintEngineBuilder {
            this.apiKey = apiKey
            this.baseUrls = listOf(baseUrl)
        }.spaghettiDetectiveApi
    )
}