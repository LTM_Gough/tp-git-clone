package de.crysxd.octoapp.sharedexternalapis.printconfidence.octoeverywhere.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class OctoEverywherePrintConfidenceResponse(
    @SerialName("Result") val result: Result = Result()
) {
    @Serializable
    data class Result(
        @SerialName("Status") val status: String? = null,
        @SerialName("StatusColor") val statusColor: String? = null,
        @SerialName("Rating") val rating: Int? = null,
        @SerialName("State") val state: Int? = null,
    )
}