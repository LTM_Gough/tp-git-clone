package de.crysxd.octoapp.sharedexternalapis.mjpeg

import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedException
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.ext.format
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retryWhen
import kotlinx.datetime.Clock
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

class MjpegConnection3(
    streamUrl: Url,
    private val throwExceptions: Boolean = false,
    httpSettings: HttpClientSettings,
    name: String,
    maxSize: Int? = null
) {

    companion object {
        private var instanceCounter = 0
    }

    private val tag = "MjpegConnection3/$name/${instanceCounter++}"
    private val socket = MjpegSocket(
        maxImageSize = maxSize,
        logTag = "Cache/$tag",
        url = streamUrl,
        httpClientSettings = httpSettings,
    )

    @Suppress("BlockingMethodInNonBlockingContext")
    fun load(): Flow<MjpegSnapshot> {
        var hasBeenConnected = false
        var lastImageTime = Clock.System.now()

        val imageTimes = mutableListOf<Duration>()
        var imageCounter = 0
        var readTime = 0.seconds
        var searchTime = 0.seconds
        var decodeTime = 0.seconds

        return flow {
            emit(MjpegSnapshot.Loading)
            emitAll(socket.connect())
        }.onCompletion {
            Napier.i(tag = tag, message = "Stopped stream")
        }.onStart {
            Napier.i(tag = tag, message = "Started stream")
        }.onEach {
            hasBeenConnected = hasBeenConnected || it is MjpegSnapshot.Frame
        }.retryWhen { cause, attempt ->
            Napier.e(tag = tag, message = "Stream failed", throwable = cause)

            if (throwExceptions) {
                throw cause
            }

            // If we had been connected in the past, wait 1s and try to reconnect once
            when {
                attempt >= 2 -> {
                    Napier.i(tag = tag, message = "Reconnection attempt failed, escalating error")
                    false
                }
                hasBeenConnected -> {
                    val backoff = 2000 * (attempt + 1)
                    Napier.i(tag = tag, message = "Connection broke down, scheduling reconnect (attempt=$attempt, backoff=${backoff}ms)")
                    emit(MjpegSnapshot.Loading)
                    delay(backoff)
                    Napier.i(tag = tag, message = "Reconnecting...")
                    true
                }
                else -> {
                    Napier.i(tag = tag, message = "Connection broke down but never was connected, skipping reconnect")
                    false
                }
            }
        }.map {
            if (it is MjpegSnapshot.Frame) {
                val time = Clock.System.now() - lastImageTime

                imageCounter++
                imageTimes += time
                searchTime += it.analytics.searchTime
                readTime += it.analytics.readTime
                decodeTime += it.analytics.decodeTime

                // Image times keeps up to 30 measurements
                if (imageTimes.size > 30) {
                    imageTimes.drop(imageTimes.size - 30)
                }

                // Calc FPS based on median image time
                lastImageTime = Clock.System.now()
                val fps = 1000f / imageTimes.sorted()[imageTimes.size / 2].inWholeMilliseconds

                if (imageCounter > 100) {
                    Napier.d(tag = tag, message = "FPS: %.1f (readTime=%s searchTime=%s decodeTime=%s)".format(fps, readTime, searchTime, decodeTime))
                    imageCounter = 0
                    readTime = 0.seconds
                    searchTime = 0.seconds
                    decodeTime = 0.seconds
                }
                it.copy(analytics = it.analytics.copy(fps = fps))
            } else {
                it
            }
        }.flowOn(Dispatchers.SharedIO)
    }

    data class Analytics(
        val readTime: Duration,
        val searchTime: Duration,
        val decodeTime: Duration,
        val dropCount: Int,
        val byteCount: Int,
        val fps: Float,
    ) {
        val byteCountPerMinute = (byteCount * fps * 60).toLong()
    }

    sealed class MjpegSnapshot {
        object Loading : MjpegSnapshot()
        data class Frame constructor(
            val frame: Image,
            val analytics: Analytics
        ) : MjpegSnapshot()
    }

    class NoImageResourceException(mimeType: String) : IllegalStateException("No image resource: $mimeType"), SuppressedException
}
