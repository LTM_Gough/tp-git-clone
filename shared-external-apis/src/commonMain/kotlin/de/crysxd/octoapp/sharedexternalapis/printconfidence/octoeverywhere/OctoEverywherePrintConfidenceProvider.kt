package de.crysxd.octoapp.sharedexternalapis.printconfidence.octoeverywhere

import de.crysxd.octoapp.sharedexternalapis.printconfidence.PrintConfidenceService

class OctoEverywherePrintConfidenceProvider {
    fun forAppKey(appKey: String): PrintConfidenceService = OctoEverywherePrintConfidenceService(
        api = OctoEverywherePrintConfidenceApi(appToken = appKey)
    )
}