package de.crysxd.octoapp.sharedexternalapis.mjpeg

import kotlinx.cinterop.addressOf
import kotlinx.cinterop.convert
import kotlinx.cinterop.usePinned
import platform.Foundation.NSData
import platform.Foundation.create
import platform.UIKit.UIImage

actual class JpegDecoder actual constructor(
    usePool: Boolean,
    logTag: String,
    maxImageSize: Int?,
) {

    actual suspend fun decode(byteArray: ByteArray, length: Int): Image {
        return UIImage(data = byteArray.toNSData())
    }

    private fun ByteArray.toNSData(): NSData = usePinned { array ->
        NSData.create(bytesNoCopy = array.addressOf(0), length = this.size.convert())
    }

    actual fun reset() = Unit
}