package de.crysxd.octoapp.sharedexternalapis.mjpeg

import de.crysxd.octoapp.engine.exceptions.PrinterApiException
import de.crysxd.octoapp.sharedcommon.exceptions.BasicAuthRequiredException
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.framework.extractAndRemoveBasicAuth
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MjpegConnection3.MjpegSnapshot.Frame
import io.github.aakira.napier.Napier
import io.ktor.http.HttpHeaders
import io.ktor.http.Url
import io.ktor.utils.io.errors.IOException
import kotlinx.atomicfu.AtomicBoolean
import kotlinx.atomicfu.atomic
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onCompletion
import kotlinx.datetime.Clock
import okio.toByteString
import platform.Foundation.NSData
import platform.Foundation.NSError
import platform.Foundation.NSHTTPURLResponse
import platform.Foundation.NSLock
import platform.Foundation.NSMutableData
import platform.Foundation.NSMutableURLRequest
import platform.Foundation.NSOperationQueue
import platform.Foundation.NSURL.Companion.URLWithString
import platform.Foundation.NSURLAuthenticationChallenge
import platform.Foundation.NSURLCredential
import platform.Foundation.NSURLRequest
import platform.Foundation.NSURLResponse
import platform.Foundation.NSURLSession
import platform.Foundation.NSURLSessionAuthChallengePerformDefaultHandling
import platform.Foundation.NSURLSessionConfiguration
import platform.Foundation.NSURLSessionDataDelegateProtocol
import platform.Foundation.NSURLSessionDataTask
import platform.Foundation.NSURLSessionResponseDisposition
import platform.Foundation.NSURLSessionTask
import platform.Foundation.appendData
import platform.Foundation.setValue
import platform.UIKit.UIImage
import platform.darwin.NSObject
import kotlin.time.Duration.Companion.seconds

internal actual class MjpegSocket actual constructor(
    maxImageSize: Int?,
    private val url: Url,
    httpClientSettings: HttpClientSettings,
    private val logTag: String,
) {
    private val requestQueue = NSOperationQueue()

    actual fun connect(): Flow<MjpegConnection3.MjpegSnapshot> {
        var session: Session? = null
        Napier.i("Using url $url")
        val (plainUrl, basicAuth) = url.extractAndRemoveBasicAuth()

        return flow {
            val internalFlow = MutableStateFlow<Any>(Unit)
            val closeSignal = "close"

            session = createSession(
                onFrame = { internalFlow.value = it },
                onCompletion = { internalFlow.value = it ?: closeSignal },
            ).apply {
                Napier.i(tag = logTag, message = "Starting session, connecting to $url")
                val request = NSMutableURLRequest(URLWithString(plainUrl.toString())!!)
                if (basicAuth != null) {
                    request.setValue(basicAuth, forHTTPHeaderField = HttpHeaders.Authorization)
                }
                nsUrlSession.dataTaskWithRequest(request).resume()
                Napier.v(tag = logTag, message = "Connection active")
            }

            val mappedFlow = internalFlow.mapNotNull {
                when (it) {
                    is Frame -> it
                    is Throwable -> throw it
                    closeSignal -> throw CancellationException("Connection closed")
                    Unit -> null
                    else -> throw UnsupportedOperationException("Can't handle $it")
                }
            }

            Napier.v(tag = logTag, message = "Emitting all")
            emitAll(mappedFlow)
        }.onCompletion {
            Napier.i(tag = logTag, message = "Closing session")
            session?.close()
        }
    }

    private fun createSession(onFrame: (Frame) -> Unit, onCompletion: (Throwable?) -> Unit): Session {
        val delegate = DarwinResponseDelegate(
            logTag = logTag,
            onFrame = onFrame,
            url = url,
            onCompletion = onCompletion,
        )

        return Session(
            nsUrlSession = NSURLSession.sessionWithConfiguration(
                NSURLSessionConfiguration.defaultSessionConfiguration(),
                delegate,
                delegateQueue = requestQueue
            )
        )
    }

    private fun Session.close() {
        if (!closed.compareAndSet(expect = false, update = true)) return
        nsUrlSession.finishTasksAndInvalidate()
    }

    private data class Session(
        val nsUrlSession: NSURLSession,
        val closed: AtomicBoolean = atomic(false)
    )

    private class DarwinResponseDelegate(
        private val logTag: String,
        private val onFrame: (Frame) -> Unit,
        private val onCompletion: (Throwable?) -> Unit,
        private val url: Url,
    ) : NSObject(), NSURLSessionDataDelegateProtocol {

        private var dropCount = 0
        private var receivedData = NSMutableData()
        private var readStart = Clock.System.now()
        private val lock = NSLock()

        override fun URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveData: NSData) = lock.withLock {
            receivedData.appendData(didReceiveData)
        }

        override fun URLSession(
            session: NSURLSession,
            dataTask: NSURLSessionDataTask,
            didReceiveResponse: NSURLResponse,
            completionHandler: (NSURLSessionResponseDisposition) -> Unit
        ) = lock.withLock {
            didReceiveResponse.validate(receivedData)

            when {
                receivedData.length <= 0u -> {
                    dropCount++
                    Napier.w(tag = logTag, message = "Received empty frame")
                }

                else -> try {
                    val start = Clock.System.now()
                    val byteCount = receivedData.length
                    val image = UIImage(receivedData)
                    val end = Clock.System.now()

                    val analytics = MjpegConnection3.Analytics(
                        readTime = Clock.System.now() - readStart,
                        searchTime = 0.seconds,
                        decodeTime = end - start,
                        dropCount = 0,
                        byteCount = byteCount.toInt(),
                        fps = 0f,
                    )

                    onFrame(Frame(image, analytics))
                    dropCount = 0
                } catch (e: NullPointerException) {
                    // Ignore
                    dropCount++
                } catch (e: Exception) {
                    Napier.e(tag = logTag, message = "Error in image decode: ${e.message}", throwable = e)
                    dropCount++

                    if (dropCount > 3) {
                        onCompletion(IllegalStateException("Too many dropped frames"))
                    }
                }
            }

            receivedData.setLength(0)
            readStart = Clock.System.now()
            completionHandler(1 /* URLSession.ResponseDisposition.allow */)
        }

        private fun NSURLResponse.validate(receivedData: NSMutableData) {
            require(this is NSHTTPURLResponse)
            val authHeader = this.valueForHTTPHeaderField("WWW-Authenticate")

            when {
                statusCode == 200L -> null

                statusCode == 401L && authHeader != null -> BasicAuthRequiredException(
                    header = authHeader,
                    webUrl = url,
                )

                else -> PrinterApiException(
                    httpUrl = url,
                    responseCode = statusCode.toInt(),
                    body = receivedData.toByteString().utf8()
                )
            }?.let(onCompletion)
        }

        override fun URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError: NSError?) {
            onCompletion(didCompleteWithError?.let { IOException("NSError: ${it.description}") })
        }

        override fun URLSession(
            session: NSURLSession,
            task: NSURLSessionTask,
            willPerformHTTPRedirection: NSHTTPURLResponse,
            newRequest: NSURLRequest,
            completionHandler: (NSURLRequest?) -> Unit
        ) {
            Napier.v(tag = logTag, message = "willPerformHTTPRedirection")
            completionHandler(null)
        }

        override fun URLSession(
            session: NSURLSession,
            task: NSURLSessionTask,
            didReceiveChallenge: NSURLAuthenticationChallenge,
            completionHandler: (Long, NSURLCredential?) -> Unit
        ) {
            Napier.v(tag = logTag, message = "didReceiveChallenge")
            completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, didReceiveChallenge.proposedCredential)
        }

        private inline fun <T> NSLock.withLock(block: () -> T): T = try {
            lock()
            block()
        } finally {
            unlock()
        }
    }
}