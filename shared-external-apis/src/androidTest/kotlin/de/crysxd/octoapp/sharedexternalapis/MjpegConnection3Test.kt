package de.crysxd.octoapp.sharedexternalapis

import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.LogLevel
import de.crysxd.octoapp.sharedcommon.http.config.ProxySelector
import de.crysxd.octoapp.sharedcommon.http.config.Timeouts
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MjpegConnection3TestCore
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.net.InetAddress
import kotlin.time.Duration.Companion.seconds

@RunWith(RobolectricTestRunner::class)
@Suppress("BlockingMethodInNonBlockingContext")
class MjpegConnection3Test : MjpegConnection3TestCore() {

    override val settings = HttpClientSettings(
        logLevel = LogLevel.Verbose,
        timeouts = Timeouts(connectionTimeout = 60.seconds),
        dns = { hostname -> (InetAddress.getAllByName(hostname)?.toList()) ?: emptyList() },
        proxySelector = ProxySelector.Noop,
        keyStore = null,
    )
}