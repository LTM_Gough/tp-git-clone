package de.crysxd.octoapp.help.plugins

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.BaseViewModel
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import io.ktor.http.Url
import kotlinx.coroutines.launch
import timber.log.Timber

class PluginsLibraryViewModel(
    private val octoPrintRepository: OctoPrintRepository,
) : BaseViewModel() {

    companion object {
        private val OCTOEVERYWHERE_ORDER = (100..1000).random()
        private val SPAGHETTI_DETECTIVE_ORDER = (100..1000).random()
        private val NGROK_ORDER = (0..100).random()
        private val TAILSCALE_ORDER = (0..100).random()
    }

    private val configurationNeeded = "**Requires sign in before use.**\n\n"
    val pluginsIndex = MutableLiveData(
        PluginsIndex(
            listOf(
                PluginCategory(
                    name = "Recommended",
                    id = null,
                    plugins = listOf(
                        Plugin(
                            name = "OctoApp Companion",
                            key = OctoPlugins.OctoApp,
                            highlight = true,
                            order = 10_000,
                            description = "Install the OctoApp Companion plugin for many performance improvements and better notifications. This plugin is required for some features in the app.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/octoapp/"),
                        ),
                        Plugin(
                            name = "PSU Control",
                            key = OctoPlugins.PsuControl,
                            highlight = true,
                            order = 9_999,
                            description = "Turn your printer on and off via the webinterface and OctoApp and let your printer turn off automatically after a print is done.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/psucontrol/"),
                        ),
                        Plugin(
                            name = "OctoEverywhere",
                            highlight = true,
                            key = OctoPlugins.OctoEverywhere,
                            order = OCTOEVERYWHERE_ORDER,
                            description = "${configurationNeeded}Easy remote access to OctoPrint so you can use OctoApp from wherever you are!",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/octoeverywhere/"),
                            octoAppTutorial = Uri.parse("https://www.youtube.com/watch?v=kvSLAsBHL00"),
                            configureLink = UriLibrary.getConfigureRemoteAccessUri(),
                        ),
                        Plugin(
                            name = "Obico (The Spaghetti Detective)",
                            highlight = true,
                            order = SPAGHETTI_DETECTIVE_ORDER,
                            key = OctoPlugins.Obico,
                            description = "${configurationNeeded}Use Obico's tunnel to use OctoApp when you are not at home!",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/obico/"),
                            octoAppTutorial = Uri.parse("https://www.youtube.com/watch?v=kOfhlZgye10"),
                            configureLink = UriLibrary.getConfigureRemoteAccessUri(),
                        ),
                    ).sortedByDescending { it.order }
                ),
                PluginCategory(
                    name = "Remote access",
                    id = "remoteAccess",
                    plugins = listOf(
                        Plugin(
                            name = "ngrok",
                            key = OctoPlugins.Ngrok,
                            order = NGROK_ORDER,
                            description = "${configurationNeeded}A plugin to securely access your OctoPrint instance remotely through ngrok",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/ngrok/"),
                            octoAppTutorial = Uri.parse("https://www.youtube.com/watch?v=Rskcyzujhps"),
                            configureLink = UriLibrary.getConfigureRemoteAccessUri(),
                        ),
                        Plugin(
                            name = "OctoEverywhere",
                            highlight = true,
                            key = OctoPlugins.OctoEverywhere,
                            order = OCTOEVERYWHERE_ORDER,
                            description = "${configurationNeeded}Easy remote access to OctoPrint so you can use OctoApp from wherever you are!",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/octoeverywhere/"),
                            octoAppTutorial = Uri.parse("https://www.youtube.com/watch?v=kvSLAsBHL00"),
                            configureLink = UriLibrary.getConfigureRemoteAccessUri(),
                        ),
                        Plugin(
                            name = "Obico (The Spaghetti Detective)",
                            highlight = true,
                            order = SPAGHETTI_DETECTIVE_ORDER,
                            key = OctoPlugins.Obico,
                            description = "${configurationNeeded}Use Obico's tunnel to use OctoApp when you are not at home!",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/obico/"),
                            octoAppTutorial = Uri.parse("https://www.youtube.com/watch?v=kOfhlZgye10"),
                            configureLink = UriLibrary.getConfigureRemoteAccessUri(),
                        ),
                        Plugin(
                            name = "Tailscale",
                            key = "tailscale",
                            order = TAILSCALE_ORDER,
                            description = "Not a plugin — but a great way to easily enabled remote access",
                            octoAppTutorial = Uri.parse("https://www.youtube.com/watch?v=2Ox1JJEEYoU"),
                            configureLink = UriLibrary.getConfigureRemoteAccessUri(),
                        ),
                    ).sortedByDescending { it.order }
                ),
                PluginCategory(
                    name = "Power",
                    id = "power",
                    plugins = listOf(
                        Plugin(
                            name = "PSU Control",
                            key = OctoPlugins.PsuControl,
                            highlight = true,
                            description = "Turn your printer on and off via the webinterface and OctoApp and let your printer turn off automatically after a print is done.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/psucontrol/"),
                        ),
                        Plugin(
                            name = "GPIO Control",
                            key = OctoPlugins.GpioControl,
                            description = "Control each device connected to your Raspberry Pi from the web interface and OctoApp.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/gpiocontrol/"),
                        ),
                        Plugin(
                            name = "Tasmota",
                            key = OctoPlugins.Tasmota,
                            description = "Simple plugin to control sonoff devices that have been flashed with Tasmota.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/tasmota/"),
                        ),
                        Plugin(
                            name = "TPLink Smart Plug",
                            key = OctoPlugins.TpLinkSmartPlug,
                            description = "Plugin to control TP-Link Smartplug devices from OctoPrint web interface and OctoApp",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/tplinksmartplug/"),
                        ),
                        Plugin(
                            name = "Tuya Smart Plug",
                            key = OctoPlugins.PsuControl,
                            description = "Plugin to control Tuya based Smartplug devices from OctoPrint web interface and OctoApp",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/psucontrol/"),
                        ),
                        Plugin(
                            name = "IKEA Tradfri",
                            key = OctoPlugins.IkeaTradfri,
                            description = "Control Ikea Tradfri outlet from OctoPrint web interface and OctoApp",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/ikea_tradfri/"),
                        ),
                        Plugin(
                            name = "WS281x LED Status",
                            key = OctoPlugins.Ws281xLedStatus,
                            description = "Add some WS281x type RGB LEDs to your printer for a quick status update! OctoApp can control the torch to illuminate your print.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/ws281x_led_status/"),
                        ),
                        Plugin(
                            name = "MyStrom",
                            key = OctoPlugins.MyStromSwitch,
                            description = "Plugin to integrate myStrom Switch into your OctoPrint installation.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/mystromswitch/"),
                        ),
                        Plugin(
                            name = "OctoRelay",
                            key = OctoPlugins.OctoRelay,
                            description = "A plugin to control relays or other things on the GPIO pins of your raspberry pi. For example turn the power of printer, the light or a fan ON and OFF via the web interface.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/octorelay/"),
                        ),
                        Plugin(
                            name = "OctoLight",
                            key = OctoPlugins.OctoLight,
                            description = "A simple plugin, that add's a button to the navbar, toggling GPIO on the RPi. It can be used for turning on and off a light.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/octolight/"),
                        ),
                        Plugin(
                            name = "Wemo Switch",
                            key = OctoPlugins.WemoSwitch,
                            description = "This plugin allows for the control of Belkin Wemo devices via OctoApp, navbar buttons and gcode commands.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/wemoswitch/"),
                        ),
                        Plugin(
                            name = "USB Relay Control",
                            key = OctoPlugins.UsbRelayControl,
                            description = "Control each USB Relay connected to your Raspberry Pi from the web interface and OctoApp.",
                            pluginPage = Uri.parse("https://github.com/abudden/OctoPrint-USBRelayControl"),
                        ),
                        Plugin(
                            name = "Ophom",
                            key = OctoPlugins.Ophom,
                            description = "Switch a Philips Hue that your printer is connected to on or off. You can set an automatic switch-off based on a minimum temperature.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/ophom/"),
                        ),
                        Plugin(
                            name = "OctoHue",
                            key = OctoPlugins.OctoHue,
                            description = "Hue lighting control for Octoprint - Illuminate your printer and signal its status using Phillips Hue lights.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/octohue/"),
                        ),
                        Plugin(
                            name = "Wled",
                            key = OctoPlugins.Wled,
                            description = "This plugin allows you to configure a WLED device to connect to OctoPrint to display the status of your prints with ease!",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/wled/"),
                        ),
                        Plugin(
                            name = "Enclosure",
                            key = OctoPlugins.Enclosure,
                            description = "Control GPIO of your Raspberry Pi and add additional temperature sensors to OctoApp.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/enclosure/"),
                        ),
                        Plugin(
                            name = "Tuya Smartplug",
                            key = OctoPlugins.Tuya,
                            description = "Work based on OctoPrint-TPLinkSmartplug and python-tuya. This plugin controlls Tuya-based SmartPlugs.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/tuyasmartplug/"),
                        ),
                    )
                ),
                PluginCategory(
                    name = "Materials",
                    id = "materials",
                    plugins = listOf(
                        Plugin(
                            name = "FilamentManager",
                            key = OctoPlugins.FilamentManager,
                            description = "This OctoPrint plugin helps to manage your filament spools.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/filamentmanager/"),
                        ),
                        Plugin(
                            name = "SpoolManager",
                            key = OctoPlugins.SpoolManager,
                            description = "The OctoPrint-Plugin manages all spool informations and stores it in a database.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/SpoolManager/"),
                        ),
                    )
                ),
                PluginCategory(
                    name = "Files",
                    id = "files",
                    plugins = listOf(
                        Plugin(
                            name = "Cura Thumbnails",
                            key = OctoPlugins.UltimakerFormatPackage,
                            description = "This plugin adds support for Ultimaker Format Package (.ufp) files.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/UltimakerFormatPackage/"),
                        ),
                        Plugin(
                            name = "Slicer Thumbnails",
                            key = OctoPlugins.PrusaSlicerThumbnails,
                            description = "Extracts various slicer's embedded thumbnails from gcode files.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/prusaslicerthumbnails/"),
                        ),
                        Plugin(
                            name = "Upload Anything",
                            key = OctoPlugins.UploadAnything,
                            description = "Allows custom file types to be uploaded via the web interface and OctoApp.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/uploadanything/"),
                        ),
                    )
                ),
                PluginCategory(
                    name = "Others",
                    id = "others",
                    plugins = listOf(
                        Plugin(
                            name = "PrintTimeGenius",
                            key = OctoPlugins.PrintTimeGenius,
                            highlight = true,
                            description = "Use a gcode pre-analysis to provide better print time estimation. OctoApp can show the improved estimations.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/PrintTimeGenius/"),
                        ),
                        Plugin(
                            name = "MultiCam",
                            key = OctoPlugins.MultiCam,
                            description = "Extends the Control tab of OctoPrint and the webcam view in OctoApp, allowing the ability to switch between multiple webcam feeds.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/multicam/"),
                        ),
                        Plugin(
                            name = "Cancelobject",
                            key = OctoPlugins.CancelObject,
                            description = "Cancel single objects during a print based on gcode comment lines",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/cancelobject/"),
                        ),
                        Plugin(
                            name = "ArcWelder",
                            key = OctoPlugins.ArcWelder,
                            description = "Anti-Stutter and GCode Compression. Replaces G0/G1 with G2/G3 where possible. OctoApp can show arcs generated in the Gcode preview.",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/arc_welder/"),
                        ),
                        Plugin(
                            name = "Octolapse",
                            key = OctoPlugins.Octolapse,
                            description = "Confirm snapshot previews in OctoApp",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/octolapse/"),
                        ),
                        Plugin(
                            name = "Mmu2filamentselect",
                            key = OctoPlugins.Mmu2FilamentSelect,
                            description = "Select filament for the MMU2 in OctoApp\n**Attention: also requires the OctoApp companion plugin for reliable operation!**",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/mmu2filamentselect/"),
                        ),
                        Plugin(
                            name = "Better Grbl Support",
                            key = OctoPlugins.BetterGrblSupport,
                            description = "OctoApp hides useless controls when the plugin is installed",
                            pluginPage = Uri.parse("https://plugins.octoprint.org/plugins/bettergrblsupport/"),
                        ),
                    )
                )
            )
        )
    )

    init {
        viewModelScope.launch(coroutineExceptionHandler) {
            try {
                val installed = octoPrintRepository.getActiveInstanceSnapshot()?.availablePlugins ?: emptyMap()
                val current = pluginsIndex.value ?: return@launch
                val categories = current.categories.map { c ->
                    val plugins = c.plugins.map { p ->
                        p.copy(installed = installed.contains(p.key))
                    }
                    c.copy(plugins = plugins)
                }
                pluginsIndex.value = current.copy(categories = categories)
            } catch (e: Exception) {
                Timber.w(e)
            }
        }
    }

    data class PluginsIndex(
        val categories: List<PluginCategory>
    )

    data class PluginCategory(
        val plugins: List<Plugin>,
        val id: String?,
        val name: String,
    )

    data class Plugin(
        val key: String,
        val name: String,
        val description: String,
        val highlight: Boolean = false,
        val pluginPage: Uri? = null,
        val octoAppTutorial: Uri? = null,
        val installed: Boolean? = null,
        val order: Int = 0,
        val configureLink: Url? = null
    )
}