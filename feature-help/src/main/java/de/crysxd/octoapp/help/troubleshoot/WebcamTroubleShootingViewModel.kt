package de.crysxd.octoapp.help.troubleshoot

import androidx.lifecycle.asLiveData
import de.crysxd.baseui.BaseViewModel
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.TestFullNetworkStackUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import timber.log.Timber

@OptIn(ExperimentalCoroutinesApi::class)
class WebcamTroubleShootingViewModel(
    octoPrintRepository: OctoPrintRepository,
    octoPrintProvider: OctoPrintProvider,
    testFullNetworkStackUseCase: TestFullNetworkStackUseCase,
) : BaseViewModel() {

    companion object {
        private const val MIN_LOADING_TIME = 2000
    }

    private val retrySignalChannel = MutableStateFlow(0)
    val uiState = octoPrintProvider.octoPrintFlow()
        .combine(retrySignalChannel) { _, _ ->
            // No data returned, we only need a trigger :)
        }.flatMapLatest {
            val instance = requireNotNull(octoPrintRepository.getActiveInstanceSnapshot()) { "No OctoPrint active" }
            val activeIndex = instance.appSettings?.activeWebcamIndex ?: 0
            Timber.i("Troubleshooting webcam $activeIndex @ $instance'")
            flow {
                emit(UiState.Loading)
                val start = System.currentTimeMillis()
                val target = TestFullNetworkStackUseCase.Target.Webcam(
                    instanceId = instance.id,
                    webcamIndex = activeIndex
                )
                val finding = testFullNetworkStackUseCase.execute(target)
                val end = System.currentTimeMillis()
                val delay = MIN_LOADING_TIME - (end - start)
                if (delay > 0) delay(delay)
                emit(UiState.Finding(finding))
            }
        }.catch {
            Timber.e(it)
            if (it is TestFullNetworkStackUseCase.WebcamUnsupportedException) {
                emit(UiState.UnsupportedWebcam)
            } else {
                emit(UiState.Finding(TestFullNetworkStackUseCase.Finding.UnexpectedIssue(null, it)))
            }
        }.asLiveData()

    fun retry() = retrySignalChannel.value++

    sealed class UiState {
        object Loading : UiState()
        data class Finding(val finding: TestFullNetworkStackUseCase.Finding) : UiState()
        object UnsupportedWebcam : UiState()
    }
}