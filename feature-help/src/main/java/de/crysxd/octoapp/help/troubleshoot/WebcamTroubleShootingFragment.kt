package de.crysxd.octoapp.help.troubleshoot

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionManager
import de.crysxd.baseui.BaseFragment
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.common.LinkClickMovementMethod
import de.crysxd.baseui.common.feedback.SendFeedbackDialog
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.baseui.utils.ThemePlugin
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.composeMessageStack
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.base.usecase.TestFullNetworkStackUseCase
import de.crysxd.octoapp.base.utils.texts.WebcamFindingDescriptionLibrary
import de.crysxd.octoapp.help.R
import de.crysxd.octoapp.help.databinding.HelpWebcamTroubleshootingBinding
import de.crysxd.octoapp.help.di.injectViewModel
import io.noties.markwon.Markwon

class WebcamTroubleShootingFragment : BaseFragment() {
    override val viewModel: WebcamTroubleShootingViewModel by injectViewModel()
    private lateinit var binding: HelpWebcamTroubleshootingBinding
    private val findingDescriptionLibrary by lazy { WebcamFindingDescriptionLibrary() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        HelpWebcamTroubleshootingBinding.inflate(inflater, container, false).also {
            binding = it
        }.root

    override fun onStart() {
        super.onStart()
        requireOctoActivity().octo.isVisible = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.help.setOnClickListener {
            UriLibrary.getHelpUri().open(requireOctoActivity())
        }
        binding.help.setOnLongClickListener {
            SendFeedbackDialog().show(childFragmentManager, "webcam-feedback")
            true
        }
        showLoadingState()
        viewModel.uiState.observe(viewLifecycleOwner) {
            TransitionManager.beginDelayedTransition(binding.root)
            when (it) {
                is WebcamTroubleShootingViewModel.UiState.Finding -> showFinding(it.finding)
                WebcamTroubleShootingViewModel.UiState.Loading -> showLoadingState()
                WebcamTroubleShootingViewModel.UiState.UnsupportedWebcam -> {
                    requireOctoActivity().showDialog(
                        OctoActivity.Message.DialogMessage(
                            text = { getString(R.string.help___webcam_troubleshooting___only_mjpeg_supported) }
                        )
                    )
                    findNavController().popBackStack()
                }
            }
        }
    }

    private fun showLoadingState() {
        binding.finding.isVisible = false
        binding.loading.isVisible = true
        binding.webcamViewContainer2.isVisible = false
        binding.content2.isVisible = false
        binding.octoView.scheduleAnimation(600) {
            binding.octoView.swim()
        }
    }

    private fun showFinding(finding: TestFullNetworkStackUseCase.Finding) {
        binding.finding.isVisible = true
        binding.loading.isVisible = false
        binding.webcamViewContainer.isVisible = false
        binding.octoView.idle()

        val markwon = Markwon.builder(requireContext())
            .usePlugin(ThemePlugin(requireContext()))
            .build()

        markwon.setMarkdown(binding.content, findingDescriptionLibrary.getExplainerForFinding(finding))
        markwon.setMarkdown(binding.title, findingDescriptionLibrary.getTitleForFinding(finding))

        when (finding) {
            is TestFullNetworkStackUseCase.Finding.WebcamReady -> {
                binding.buttonContinue.text = getString(R.string.help___webcam_troubleshooting___continue)
                binding.buttonContinue.setOnClickListener { findNavController().popBackStack() }
                binding.webcamViewContainer.isVisible = true
                binding.webcamView.setImageBitmap(finding.image)
                binding.webcamViewContainer2.isVisible = true
                binding.content2.isVisible = true
                binding.content2.movementMethod = LinkClickMovementMethod(LinkClickMovementMethod.OpenWithIntentLinkClickedListener(requireOctoActivity()))

                if (finding.dataSaverImage != null) {
                    binding.webcamView2.setImageBitmap(finding.dataSaverImage)
                    binding.content2.text = getString(
                        R.string.help___webcam_troubleshooting___explainer_data_saver_working,
                        "",
//                        when (finding.dataSaverSource) {
//                            GetWebcamSnapshotUseCase.SnapshotSource.CompanionPlugin -> getString(
//                                R.string.help___webcam_troubleshooting___explainer_data_saver_source_companion
//                            )
//                            is GetWebcamSnapshotUseCase.SnapshotSource.MjpegSnapshot -> getString(
//                                R.string.help___webcam_troubleshooting___explainer_data_saver_source_mjpeg,
//                                (finding.dataSaverSource as GetWebcamSnapshotUseCase.SnapshotSource.MjpegSnapshot).url
//                            )
//                            is GetWebcamSnapshotUseCase.SnapshotSource.OctoPrintSnapshot -> getString(
//                                R.string.help___webcam_troubleshooting___explainer_data_saver_source_snapshot,
//                                (finding.dataSaverSource as GetWebcamSnapshotUseCase.SnapshotSource.OctoPrintSnapshot).url
//                            )
//                            null -> ""
//                        },
                        finding.dataSaverImage!!.width,
                        finding.dataSaverImage!!.height,
                    ).toHtml()
                } else {
                    binding.content2.text = getString(R.string.help___webcam_troubleshooting___explainer_data_saver_not_working)
                    binding.webcamError2.text = finding.dataSaverException?.composeMessageStack()
                }
            }

            is TestFullNetworkStackUseCase.Finding.HttpsNotTrusted -> {
                binding.buttonContinue.text = getString(R.string.help___webcam_troubleshooting___trust_and_try_again)
                binding.buttonContinue.setOnClickListener {
                    finding.certificate?.let { BaseInjector.get().sslKeyStoreHandler().storeCertificate(it) }
                    if (finding.weakHostnameVerificationRequired) {
                        BaseInjector.get().sslKeyStoreHandler().enforceWeakVerificationForHost(finding.webUrl)
                    }
                    viewModel.retry()
                }
            }

            else -> {
                binding.buttonContinue.text = getString(R.string.help___webcam_troubleshooting___try_again)
                binding.buttonContinue.setOnClickListener { viewModel.retry() }
            }
        }
    }
}