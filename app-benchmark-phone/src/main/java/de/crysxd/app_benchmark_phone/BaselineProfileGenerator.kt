package de.crysxd.app_benchmark_phone

import androidx.benchmark.macro.ExperimentalBaselineProfilesApi
import androidx.benchmark.macro.junit4.BaselineProfileRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.InitBenchmarkAppRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import de.crysxd.octoapp.tests.utils.VirtualPrinterUtils.connectPrinter
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.junit.runner.RunWith

@ExperimentalBaselineProfilesApi
@RunWith(AndroidJUnit4::class)
class BaselineProfileGenerator {

    private val testEnv = TestEnvironmentLibrary.Corgi
    private val baselineProfileRule = BaselineProfileRule()

    @get:Rule
    val chain = RuleChain.outerRule(baselineProfileRule)
        .around(InitBenchmarkAppRule())
        .around(IdleTestEnvironmentRule(testEnv))
        .around(TestDocumentationRule(showNotification = false))

    @Before
    fun setUp() {
        testEnv.connectPrinter()
    }

    @Test
    fun journey() = baselineProfileRule.collectBaselineProfile(packageName = "de.crysxd.octoapp") {
        // PRECONDITION: Printer needs to be connected
        launchApp(testEnv)
        runBenchmark(testEnv)
    }

    @Test
    fun startup() = baselineProfileRule.collectBaselineProfile(packageName = "de.crysxd.octoapp") {
        launchApp(testEnv)
    }
}