package de.crysxd.app_benchmark_phone

import androidx.benchmark.macro.CompilationMode
import androidx.benchmark.macro.FrameTimingMetric
import androidx.benchmark.macro.StartupMode
import androidx.benchmark.macro.StartupTimingMetric
import androidx.benchmark.macro.junit4.MacrobenchmarkRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.InitBenchmarkAppRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import de.crysxd.octoapp.tests.utils.VirtualPrinterUtils.connectPrinter
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.junit.runner.RunWith


/**
 * This is an example startup benchmark.
 *
 * It navigates to the device's home screen, and launches the default activity.
 *
 * Before running this benchmark:
 * 1) switch your app's active build variant in the Studio (affects Studio runs only)
 * 2) add `<profileable android:shell="true" />` to your app's manifest, within the `<application>` tag
 *
 * Run this benchmark from Studio to see startup measurements, and captured system traces
 * for investigating your app's performance.
 */
@RunWith(AndroidJUnit4::class)
class BaselineProfileBenchmark {

    private val testEnv = TestEnvironmentLibrary.Corgi
    private val benchmarkRule = MacrobenchmarkRule()

    @get:Rule
    val chain = RuleChain.outerRule(benchmarkRule)
        .around(InitBenchmarkAppRule())
        .around(IdleTestEnvironmentRule(testEnv))
        .around(TestDocumentationRule(showNotification = false))

    @Before
    fun setUp() {
        testEnv.connectPrinter()
    }

    @Test
    fun benchmarkStartupNone() = benchmarkStartup(CompilationMode.None())

    @Test
    fun benchmarkStartupPartial() = benchmarkStartup(CompilationMode.Partial())

    @Test
    fun benchmarkStartupFull() = benchmarkStartup(CompilationMode.Full())

    private fun benchmarkStartup(compilationMode: CompilationMode) = benchmarkRule.measureRepeated(
        packageName = "de.crysxd.octoapp",
        metrics = listOf(StartupTimingMetric()),
        iterations = 5,
        startupMode = StartupMode.COLD,
        compilationMode = compilationMode,
    ) {
        launchApp(testEnv)
    }

    @Test
    fun benchmarkJourneyNone() = benchmarkJourney(CompilationMode.None())

    @Test
    fun benchmarkJourneyPartial() = benchmarkJourney(CompilationMode.Partial())

    @Test
    fun benchmarkJourneyFull() = benchmarkJourney(CompilationMode.Full())

    private fun benchmarkJourney(compilationMode: CompilationMode) = benchmarkRule.measureRepeated(
        packageName = "de.crysxd.octoapp",
        metrics = listOf(FrameTimingMetric()),
        iterations = 1,
        compilationMode = compilationMode,
        startupMode = StartupMode.COLD
    ) {
        // PRECONDITION: Printer needs to be connected
        launchApp(testEnv)
        runBenchmark(testEnv)
    }
}