package de.crysxd.octoapp.engine.models.timelapse

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize

@CommonParcelize
data class TimelapseFile(
    val name: String,
    val bytes: Long,
    val date: Long? = null,
    val downloadPath: String? = null,
    val thumbnail: String? = null,
    val processing: Boolean = false,
    val rendering: Boolean = false,
    val recording: Boolean = false,
) : CommonParcelable