package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octohue.OctoHueCommand
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.request.post
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class OctoHueApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings): List<IPowerDevice> = if (settings.plugins.octoHue != null) {
        listOf(PowerDevice(this))
    } else {
        emptyList()
    }

    private suspend fun toggle() = baseUrlRotator.request<Unit> {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.OctoHue)
            setJsonBody(OctoHueCommand())
        }
    }

    internal data class PowerDevice(
        private val owner: OctoHueApi,
    ) : IPowerDevice {
        override val id = "octohue"
        override val pluginId = OctoPlugins.OctoHue
        override val displayName = "OctoHue Light"
        override val pluginDisplayName = "OctoHue"
        override val capabilities = listOf(IPowerDevice.Capability.Illuminate)
        override val controlMethods = listOf(IPowerDevice.ControlMethod.Toggle)
        override suspend fun turnOn() = throw UnsupportedOperationException("Not supported")
        override suspend fun turnOff() = throw UnsupportedOperationException("Not supported")
        override suspend fun isOn(): Boolean? = null
        override suspend fun toggle() = owner.toggle()
    }
}