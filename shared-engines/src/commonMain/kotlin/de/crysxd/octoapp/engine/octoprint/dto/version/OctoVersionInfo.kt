package de.crysxd.octoapp.engine.octoprint.dto.version

import kotlinx.serialization.Serializable

@Serializable
data class OctoVersionInfo(
    val api: String,
    val server: String,
    val text: String
)