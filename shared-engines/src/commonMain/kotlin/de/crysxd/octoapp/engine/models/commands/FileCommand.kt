package de.crysxd.octoapp.engine.models.commands

sealed class FileCommand {

    data class SelectFile(val print: Boolean = false) : FileCommand()

    data class MoveFile(val destination: String) : FileCommand()

    data class CopyFile(val destination: String) : FileCommand()

}