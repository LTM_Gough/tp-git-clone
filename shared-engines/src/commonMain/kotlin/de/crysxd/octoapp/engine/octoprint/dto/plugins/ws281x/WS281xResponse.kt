package de.crysxd.octoapp.engine.octoprint.dto.plugins.ws281x

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class WS281xResponse(
    @SerialName("lights_on")
    val lightsOn: Boolean = false,
    @SerialName("torch_on")
    val torchOn: Boolean = false,
)