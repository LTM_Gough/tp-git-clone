package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.commands.JobCommand
import de.crysxd.octoapp.engine.models.job.Job

interface JobApi {
    suspend fun executeJobCommand(command: JobCommand)
    suspend fun getJob(): Job
}