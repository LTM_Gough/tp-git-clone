package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.api.TimelapseApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.timelapse.TimelapseConfig
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import de.crysxd.octoapp.engine.octoprint.dto.timelapse.OctoTimelapseStatus
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.delete
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.request.post

internal class OctoTimelapseApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : TimelapseApi {


    override suspend fun updateConfig(config: TimelapseConfig) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "timelapse")
            parameter("unrendered", "true")
            setJsonBody(config.map())
        }.body<OctoTimelapseStatus>()
    }.map()

    override suspend fun getStatus() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "timelapse")
            parameter("unrendered", "true")
        }.body<OctoTimelapseStatus>()
    }.map()

    private suspend fun deleteInternal(fileName: String, unrendered: Boolean) = baseUrlRotator.request {
        httpClient.delete {
            if (unrendered) {
                urlFromPath(baseUrl = it, "api", "timelapse", "unrendered", fileName)
            } else {
                urlFromPath(baseUrl = it, "api", "timelapse", fileName)
            }
            parameter("unrendered", "true")
        }.body<OctoTimelapseStatus>()
    }.map()

    override suspend fun delete(timelapseFile: TimelapseFile) = deleteInternal(fileName = timelapseFile.name, unrendered = false)

    override suspend fun deleteUnrendered(timelapseFile: TimelapseFile) = deleteInternal(fileName = timelapseFile.name, unrendered = true)


}
