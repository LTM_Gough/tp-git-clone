package de.crysxd.octoapp.engine.octoprint.dto.plugins.enclosure

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class EnclosureOutputResponse(
    @SerialName("current_value") val currentValue: Boolean = false
)
