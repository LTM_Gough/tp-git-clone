package de.crysxd.octoapp.engine.octoprint.dto.files

import kotlinx.serialization.Serializable

@Serializable
data class OctoFileList(
    val files: List<OctoFileObject> = emptyList(),
    val free: Long? = null,
    val total: Long? = null
)