package de.crysxd.octoapp.engine.octoprint.dto.job

import kotlinx.serialization.Serializable

@Serializable
data class OctoProgressInformation(
    val completion: Float? = null,
    val filepos: Long? = null,
    val printTime: Int? = null,
    val printTimeLeft: Int? = null,
    val printTimeLeftOrigin: String? = null,
)