package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.api.UserApi
import de.crysxd.octoapp.engine.exceptions.InvalidApiKeyException
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.dto.login.OctoUser
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get

internal class OctoUserApi(
    private val httpClient: HttpClient,
    private val baseUrlRotator: BaseUrlRotator
) : UserApi {

    override suspend fun getCurrentUser() = baseUrlRotator.request {
        try {
            httpClient.get {
                urlFromPath(baseUrl = it, "api", "currentuser")
            }.body()
        } catch (e: InvalidApiKeyException) {
            // OctoPrint 1.8.4 starts to return 403 for an invalid API key compared
            // to older versions returning the guest user. This method patches the new behaviour to reflect the old.
            OctoUser(groups = listOf("guests"), name = null, permissions = emptyList())
        }
    }.map()
}