package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.usbrelaycontrol.UsbRelayControlCommand
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.http.Url
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class UsbRelayControlApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings): List<IPowerDevice> = settings.plugins.usbRelayControl?.devices?.map {
        PowerDevice(
            owner = this,
            displayName = it.name,
            index = it.index
        )
    } ?: emptyList()

    private suspend fun setOn(index: Int, on: Boolean) = baseUrlRotator.request<Unit> {
        httpClient.post {
            url(it)
            setJsonBody(UsbRelayControlCommand(id = index, command = if (on) "turnUSBRelayOn" else "turnUSBRelayOff"))
        }
    }

    private suspend fun isOn(index: Int) = baseUrlRotator.request {
        httpClient.get {
            url(baseUrl = it)
        }
    }.body<List<String>>().getOrNull(index) == "on"

    private fun HttpRequestBuilder.url(baseUrl: Url) = urlFromPath(baseUrl, "api", "plugin", OctoPlugins.UsbRelayControl)

    internal data class PowerDevice(
        private val owner: UsbRelayControlApi,
        override val displayName: String,
        private val index: Int,
    ) : IPowerDevice {
        override val id: String = index.toString()
        override val pluginId: String = OctoPlugins.UsbRelayControl
        override val pluginDisplayName = "USB Relay Control"
        override val capabilities = listOf(IPowerDevice.Capability.ControlPrinterPower, IPowerDevice.Capability.Illuminate)
        override suspend fun turnOn() = owner.setOn(index = index, on = true)
        override suspend fun turnOff() = owner.setOn(index = index, on = false)
        override suspend fun isOn() = owner.isOn(index = index)
    }
}