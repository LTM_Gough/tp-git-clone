package de.crysxd.octoapp.engine.models.connection

enum class ConnectionType {
    OctoEverywhere,
    SpaghettiDetective,
    Ngrok,
    Tailscale,
    Default,
    DefaultCloud,
}