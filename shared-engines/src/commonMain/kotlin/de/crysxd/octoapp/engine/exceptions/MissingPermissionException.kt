package de.crysxd.octoapp.engine.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

open class MissingPermissionException(webUrl: Url) : NetworkException(
    webUrl = webUrl,
    technicalMessage = "OctoPrint returned 403 for $webUrl but the API key is valid. This indicates a missing permission.",
    userFacingMessage = "OctoPrint did not allow access to a function. This is caused by the current API key used by OctoApp missing a required permission."
)