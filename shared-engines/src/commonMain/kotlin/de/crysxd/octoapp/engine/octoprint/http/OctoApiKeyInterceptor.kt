package de.crysxd.octoapp.engine.octoprint.http

import de.crysxd.octoapp.engine.octoprint.Constants.OctoPrintApiKeyHeader
import de.crysxd.octoapp.engine.octoprint.Constants.OctoPrintSuppressApiKey
import io.ktor.client.HttpClient
import io.ktor.client.plugins.HttpSend
import io.ktor.client.plugins.plugin
import io.ktor.client.request.headers

internal fun HttpClient.installApiKeyInterceptor(apiKey: String) = plugin(HttpSend).intercept { request ->

    if (!request.attributes.contains(OctoPrintSuppressApiKey)) {
        request.headers {
            append(OctoPrintApiKeyHeader, apiKey)
        }
    }

    execute(request)
}