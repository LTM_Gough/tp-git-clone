package de.crysxd.octoapp.engine.octoprint.dto.printer

import kotlinx.serialization.Serializable

@Serializable
data class OctoPrinterState(
    val state: State = State(),
    val temperature: Map<String, OctoComponentTemperature> = emptyMap()
) {

    @Serializable
    data class State(
        val text: String? = null,
        val flags: Flags = Flags()
    )

    @Serializable
    data class Flags(
        val operational: Boolean = false,
        val paused: Boolean = false,
        val printing: Boolean = false,
        val cancelling: Boolean = false,
        val pausing: Boolean = false,
        val sdReady: Boolean = false,
        val error: Boolean = false,
        val ready: Boolean = false,
        val finishing: Boolean = false,
        val resuming: Boolean = false,
        val closedOrError: Boolean = false,
    )
}