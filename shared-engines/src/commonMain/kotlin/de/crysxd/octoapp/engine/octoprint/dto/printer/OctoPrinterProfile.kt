package de.crysxd.octoapp.engine.octoprint.dto.printer

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OctoPrinterProfile(
    val id: String,
    val current: Boolean = false,
    val default: Boolean = false,
    val model: String = "Unknown",
    val name: String = "Unknown",
    val color: String = "default",
    val volume: Volume = Volume(),
    val axes: Axes = Axes(),
    val extruder: Extruder = Extruder(0.4f, 1, false),
    val heatedChamber: Boolean = false,
    val heatedBed: Boolean = true,
) {

    @Serializable
    data class Volume(
        val depth: Float = 200f,
        val width: Float = 200f,
        val height: Float = 200f,
        val origin: Origin = Origin.Center,
        val formFactor: FormFactor = FormFactor.Rectangular,
    )

    @Serializable
    data class Axes(
        val e: Axis = Axis(),
        val x: Axis = Axis(),
        val y: Axis = Axis(),
        val z: Axis = Axis(),
    )

    @Serializable
    data class Axis(
        val inverted: Boolean = false,
        val speed: Float? = null,
    )

    @Serializable
    data class Extruder(
        val nozzleDiameter: Float = 0.4f,
        val count: Int = 1,
        val sharedNozzle: Boolean = false,
        val defaultExtrusionLength: Float = 5f,
        val offsets: List<List<Float>> = listOf(listOf(0f, 0f)),
    )

    @Serializable
    enum class Origin {
        @SerialName("lowerleft")
        LowerLeft,

        @SerialName("center")
        Center
    }

    @Serializable
    enum class FormFactor {
        @SerialName("circular")
        Circular,

        @SerialName("rectangular")
        Rectangular
    }
}