package de.crysxd.octoapp.engine.octoprint.dto.connection

import kotlinx.serialization.Serializable

@Serializable
@Suppress("Unused")
internal sealed class OctoConnectionCommand(val command: String) {

    @Serializable
    internal data class Connect(
        val port: String? = null,
        val baudrate: Int? = null,
        val printerProfile: String? = null,
        val save: Boolean? = null,
        val autoConnect: Boolean? = null
    ) : OctoConnectionCommand("connect")

    @Serializable
    internal class Disconnect : OctoConnectionCommand("disconnect")

}