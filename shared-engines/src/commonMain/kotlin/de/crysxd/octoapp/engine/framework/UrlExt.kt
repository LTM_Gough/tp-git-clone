package de.crysxd.octoapp.engine.framework

import de.crysxd.octoapp.engine.models.connection.ConnectionType
import de.crysxd.octoapp.sharedcommon.http.framework.isLocalNetwork
import de.crysxd.octoapp.sharedcommon.http.framework.withoutBasicAuth
import io.ktor.http.Url

private val TailscaleIpRegex = Regex("^100.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$")

fun Url.getConnectionType() = when {
    isOctoEverywhereUrl() -> ConnectionType.OctoEverywhere
    isSpaghettiDetectiveUrl() -> ConnectionType.SpaghettiDetective
    isNgrokUrl() -> ConnectionType.Ngrok
    isTailscale() -> ConnectionType.Tailscale
    isLocalNetwork() -> ConnectionType.Default
    else -> ConnectionType.DefaultCloud
}

fun Url.isSharedOctoEverywhereUrl() = host.startsWith("shared-") && host.endsWith(".octoeverywhere.com")

fun Url.isNgrokUrl() = host.endsWith(".ngrok.io")

fun Url.isTailscale() = TailscaleIpRegex.matches(host)

fun Url.isHlsStreamUrl() = pathSegments.lastOrNull()?.let { it.endsWith(".m3u") || it.endsWith(".m3u8") } ?: false

fun Url.isOctoEverywhereUrl() = host.endsWith(".octoeverywhere.com")

fun Url.isSpaghettiDetectiveUrl() = host.endsWith("thespaghettidetective.com") || host.endsWith("obico.io")

fun Url.isBasedOn(other: Url?) = other != null &&
        withoutBasicAuth().toString().removeSuffix("/").startsWith(other.withoutBasicAuth().toString().removeSuffix("/"), ignoreCase = true)
