package de.crysxd.octoapp.engine.octoprint.dto.plugins.octoeverywhere

import kotlinx.serialization.SerialName

@kotlinx.serialization.Serializable
data class OctoEverywhereInfo(
    @SerialName("PluginVersion") val version: String,
    @SerialName("PrinterId") val printerId: String,
)