package de.crysxd.octoapp.engine.octoprint.dto.event

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoEventTransportSubscription(
    val state: State = State.NoLogs(),
    val plugins: List<String> = emptyList(),
    val events: List<String> = listOf(
        "PrinterStateChanged",
        "PrinterProfileModified",
        "Connecting",
        "Connected",
        "Disconnected",
        "UpdatedFiles",
        "PrintStarted",
        "FileSelected",
        "PrintCancelling",
        "PrintCancelled",
        "PrintPausing",
        "PrintPaused",
        "PrintFailed",
        "PrintDone",
        "PrintResumed",
        "FirmwareData",
        "SettingsUpdated",
        "MovieRendering",
        "MovieDone",
        "MovieFailed",
    ),
) {
    @Serializable
    sealed class State {
        abstract val messages: Boolean
        abstract val state: Boolean

        @Serializable
        data class AllLogs(
            override val messages: Boolean = false,
            override val state: Boolean = true,
        ) : State() {
            val logs: Boolean = true
        }

        @Serializable
        data class NoLogs(
            override val messages: Boolean = false,
            override val state: Boolean = true,
        ) : State() {
            val logs: Boolean = false
        }

        @Serializable
        data class SomeLogs(
            override val messages: Boolean = false,
            override val state: Boolean = true,
            val logs: String
        ) : State()
    }
}
