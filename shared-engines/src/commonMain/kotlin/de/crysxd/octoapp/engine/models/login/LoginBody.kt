package de.crysxd.octoapp.engine.models.login

class LoginBody(
    val user: String? = null,
    val pass: String? = null,
)
