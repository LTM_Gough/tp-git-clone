package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.job.Job
import de.crysxd.octoapp.engine.models.job.JobInformation
import de.crysxd.octoapp.engine.models.job.ProgressInformation
import de.crysxd.octoapp.engine.octoprint.dto.job.OctoJob
import de.crysxd.octoapp.engine.octoprint.dto.job.OctoJobInformation
import de.crysxd.octoapp.engine.octoprint.dto.job.OctoProgressInformation

internal fun OctoJob.map() = Job(
    info = job.map(),
    progress = progress.map(),
)

internal fun OctoProgressInformation.map() = ProgressInformation(
    completion = if (printTimeLeftOrigin == ProgressInformation.ORIGIN_GENIUS && printTime != null && printTimeLeft != null) {
        // If the time estimation comes from PrintTimeGenius, the webinterface shows the progress based of time. We need to adapt here to have the same value.
        (printTime / (printTime + printTimeLeft.toFloat())) * 100f
    } else {
        completion
    },
    printTime = printTime,
    printTimeLeft = printTimeLeft,
    filepos = filepos,
    printTimeLeftOrigin = printTimeLeftOrigin,
)

internal fun OctoJobInformation.map() = JobInformation(
    file = file?.map()
)

internal fun OctoJobInformation.File.map() = if (date != null && name != null && origin != null && size != null && path != null) {
    JobInformation.JobFile(
        date = date.toEpochMilliseconds(),
        name = name,
        origin = origin.map(),
        size = size,
        path = path,
        display = display ?: name
    )
} else {
    null
}
