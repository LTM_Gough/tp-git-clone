package de.crysxd.octoapp.engine.models.timelapse

data class TimelapseStatus(
    val config: TimelapseConfig = TimelapseConfig(),
    val enabled: Boolean = false,
    val files: List<TimelapseFile> = emptyList(),
    val unrendered: List<TimelapseFile> = emptyList(),
)