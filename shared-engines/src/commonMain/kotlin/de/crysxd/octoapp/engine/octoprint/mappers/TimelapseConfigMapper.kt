package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.timelapse.TimelapseConfig
import de.crysxd.octoapp.engine.octoprint.dto.timelapse.OctoTimelapseConfig

internal fun TimelapseConfig.map() = OctoTimelapseConfig(
    fps = fps,
    interval = interval,
    minDelay = minDelay,
    postRoll = postRoll,
    retractionZHop = retractionZHop,
    save = save,
    type = when (type) {
        TimelapseConfig.Type.Off -> OctoTimelapseConfig.Type.Off
        TimelapseConfig.Type.Timed -> OctoTimelapseConfig.Type.Timed
        TimelapseConfig.Type.ZChange -> OctoTimelapseConfig.Type.ZChange
    }
)
