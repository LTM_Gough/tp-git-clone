package de.crysxd.octoapp.engine.octoprint.dto.plugins.applicationkeys

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ApplicationKeyCheckResponse(
    @SerialName("api_key") val apiKey: String
)