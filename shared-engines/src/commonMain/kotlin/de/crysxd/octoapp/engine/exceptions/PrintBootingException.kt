package de.crysxd.octoapp.engine.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

class PrintBootingException(webUrl: Url) : NetworkException(webUrl = webUrl, userFacingMessage = "OctoPrint is still starting up")