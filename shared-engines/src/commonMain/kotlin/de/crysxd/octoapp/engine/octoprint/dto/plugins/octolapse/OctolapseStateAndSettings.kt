package de.crysxd.octoapp.engine.octoprint.dto.plugins.octolapse

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OctolapseStateAndSettings(
    @SerialName("snapshot_plan_preview") val snapshotPlanPreview: OctolapseSnapshotPlanPreview? = null
)