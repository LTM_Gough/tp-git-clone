package de.crysxd.octoapp.engine.octoprint.serializer

import de.crysxd.octoapp.engine.octoprint.dto.connection.OctoPrintConnection.State
import io.github.aakira.napier.Napier
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

internal class OctoConnectionStateSerializer : KSerializer<State> {

    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("State", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: State) = encoder.encodeString(value.toString())

    override fun deserialize(decoder: Decoder): State {
        val value = decoder.decodeString()
        return try {
            val string = "MAYBE_" + value
                .uppercase()
                .replace(" ", "_")
                .replace(":", "")

            when {
                string.contains("PRINTING") -> State.MAYBE_PRINTING
                string.startsWith("ERROR_FAILED_TO_AUTODETECT_SERIAL_PORT") -> State.MAYBE_ERROR_FAILED_TO_AUTODETECT_SERIAL_PORT
                string.startsWith("ERROR_CONNECTION_ERROR") -> State.MAYBE_CONNECTION_ERROR
                string.contains("ERROR") -> State.MAYBE_UNKNOWN_ERROR
                else -> try {
                    State.valueOf(string)
                } catch (e: Exception) {
                    Napier.d("Unable to deserialize '$value'", e)
                    State.OTHER
                }
            }
        } catch (e: Exception) {
            Napier.e("Unable to deserialize '$value'", e)
            State.OTHER
        }
    }
}