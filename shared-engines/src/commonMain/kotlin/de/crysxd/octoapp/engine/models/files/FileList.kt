package de.crysxd.octoapp.engine.models.files

data class FileList(
    val files: List<FileObject> = emptyList(),
    val free: Long? = null,
    val total: Long? = null
)