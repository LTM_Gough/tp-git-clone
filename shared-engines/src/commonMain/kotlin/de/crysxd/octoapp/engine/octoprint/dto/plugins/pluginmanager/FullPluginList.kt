package de.crysxd.octoapp.engine.octoprint.dto.plugins.pluginmanager

@kotlinx.serialization.Serializable
data class PluginList(
    val plugins: List<Plugin> = emptyList()
) {
    @kotlinx.serialization.Serializable
    data class Plugin(
        val key: String,
        val enabled: Boolean = false,
        val version: String? = null,
    )
}