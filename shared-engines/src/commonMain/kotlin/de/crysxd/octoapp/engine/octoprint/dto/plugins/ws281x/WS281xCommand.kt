package de.crysxd.octoapp.engine.octoprint.dto.plugins.ws281x

internal data class WS281xCommand(val command: String)