package de.crysxd.octoapp.engine.models.files

enum class FileOrigin {
    Local,
    SdCard
}
