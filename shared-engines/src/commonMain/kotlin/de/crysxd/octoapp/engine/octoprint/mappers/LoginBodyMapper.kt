package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.login.LoginBody
import de.crysxd.octoapp.engine.octoprint.dto.login.OctoLoginBody

internal fun LoginBody.map() = OctoLoginBody(
    passive = true,
    user = user,
    pass = pass,
    remember = false
)