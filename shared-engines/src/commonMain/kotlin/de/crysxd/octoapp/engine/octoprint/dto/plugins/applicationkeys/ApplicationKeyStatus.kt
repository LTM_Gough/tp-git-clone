package de.crysxd.octoapp.engine.octoprint.dto.plugins.applicationkeys


sealed class ApplicationKeyStatus {
    object DeniedOrTimedOut : ApplicationKeyStatus()
    object Pending : ApplicationKeyStatus()
    data class Granted(val apiKey: String) : ApplicationKeyStatus()
}