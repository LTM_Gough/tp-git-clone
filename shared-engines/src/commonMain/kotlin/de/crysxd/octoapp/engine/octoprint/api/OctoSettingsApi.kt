package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.api.SettingsApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoSettings
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get

internal class OctoSettingsApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : SettingsApi {

    override suspend fun getSettings() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "settings")
        }.body<OctoSettings>()
    }.map()
}