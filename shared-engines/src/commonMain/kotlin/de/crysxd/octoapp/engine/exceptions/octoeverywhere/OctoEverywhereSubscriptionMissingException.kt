package de.crysxd.octoapp.engine.exceptions.octoeverywhere

import de.crysxd.octoapp.engine.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.engine.exceptions.RemoteServiceConnectionBrokenException.Companion.REMOTE_SERVICE_OCTO_EVERYWHERE
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

class OctoEverywhereSubscriptionMissingException(webUrl: Url) : NetworkException(
    userFacingMessage = "<b>OctoEverywhere disabled</b><br><br>OctoEverywhere can't be used anymore as your supporter status expired. OctoEverywhere is disconnected for now, you can reconnect it at any time.",
    technicalMessage = "Missing supporter status",
    webUrl = webUrl,
    learnMoreLink = "https://octoeverywhere.com/appportal/v1/nosupporterperks?appid=octoapp"
), RemoteServiceConnectionBrokenException {
    override val remoteServiceName = REMOTE_SERVICE_OCTO_EVERYWHERE
}