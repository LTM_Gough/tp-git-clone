package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.api.ConnectionApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.commands.ConnectionCommand
import de.crysxd.octoapp.engine.octoprint.dto.connection.OctoConnectionCommand
import de.crysxd.octoapp.engine.octoprint.dto.connection.OctoConnectionState
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.post

internal class OctoConnectionApi(
    private val httpClient: HttpClient,
    private val baseUrlRotator: BaseUrlRotator
) : ConnectionApi {

    override suspend fun getConnection() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "connection")
        }.body<OctoConnectionState>()
    }.map()

    override suspend fun executeConnectionCommand(command: ConnectionCommand) = baseUrlRotator.request<Unit> {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "connection")
            when (command) {
                is ConnectionCommand.Connect -> setJsonBody(
                    OctoConnectionCommand.Connect(
                        port = command.port,
                        baudrate = command.baudrate,
                        save = command.save,
                        autoConnect = command.autoconnect,
                        printerProfile = command.printerProfile,
                    )
                )
                ConnectionCommand.Disconnect -> setJsonBody(
                    OctoConnectionCommand.Disconnect()
                )
            }
        }
    }
}