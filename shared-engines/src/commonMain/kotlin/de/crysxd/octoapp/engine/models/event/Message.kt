package de.crysxd.octoapp.engine.models.event

import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.job.JobInformation
import de.crysxd.octoapp.engine.models.job.ProgressInformation
import de.crysxd.octoapp.engine.models.printer.PrinterState


interface Message {

    data class Connected(
        val version: String? = null,
        val displayVersion: String? = null
    ) : Message

    data class Current(
        val logs: List<String> = emptyList(),
        val temps: List<HistoricTemperatureData> = emptyList(),
        val state: PrinterState.State = PrinterState.State(),
        val progress: ProgressInformation? = null,
        val job: JobInformation? = null,
        val offsets: Map<String, Float>? = null,
        val serverTime: Long,
        val isHistoryMessage: Boolean = false,
        val originHash: Int = 0,
    ) : Message


    object UnknownMessage : Message

    interface Event : Message {

        object Connecting : Event

        object UpdatedFiles : Event

        object PrintStarted : Event

        object PrintResumed : Event

        object PrintPausing : Event

        object PrintPaused : Event

        object PrintCancelling : Event

        object PrintCancelled : Event

        object PrintFailed : Event

        object PrintDone : Event

        object PrinterProfileModified : Event

        object SettingsUpdated : Event

        object MovieRendering : Event

        object MovieDone : Event

        object MovieFailed : Event

        object Disconnected : Event

        object PrinterStateChanged : Event

        data class Unknown(val type: String) : Event

        data class FirmwareData(
            val firmwareName: String?,
            val machineType: String?,
            val extruderCount: Int?
        ) : Event

        data class FileSelected(
            val origin: FileOrigin,
            val name: String,
            val path: String
        ) : Event

        data class PrinterConnected(
            val baudrate: Int?,
            val port: String?
        ) : Event

    }
}