package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.timelapse.TimelapseConfig
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import de.crysxd.octoapp.engine.models.timelapse.TimelapseStatus
import de.crysxd.octoapp.engine.octoprint.dto.timelapse.OctoTimelapseConfig
import de.crysxd.octoapp.engine.octoprint.dto.timelapse.OctoTimelapseFile
import de.crysxd.octoapp.engine.octoprint.dto.timelapse.OctoTimelapseStatus

internal fun OctoTimelapseStatus.map() = TimelapseStatus(
    config = TimelapseConfig(
        type = when (config.type) {
            OctoTimelapseConfig.Type.Off -> TimelapseConfig.Type.Off
            OctoTimelapseConfig.Type.Timed -> TimelapseConfig.Type.Timed
            OctoTimelapseConfig.Type.ZChange -> TimelapseConfig.Type.ZChange
        },
        save = config.save,
        retractionZHop = config.retractionZHop,
        postRoll = config.postRoll,
        minDelay = config.minDelay,
        interval = config.interval,
        fps = config.fps
    ),
    files = files.map { it.map() },
    unrendered = unrendered.map { it.map() },
)

private fun OctoTimelapseFile.map() = TimelapseFile(
    name = name,
    bytes = bytes,
    date = date?.toEpochMilliseconds(),
    downloadPath = url,
    thumbnail = thumbnail,
    processing = processing,
    rendering = rendering,
    recording = recording,
)