package de.crysxd.octoapp.engine.octoprint.dto.message

import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octolapse.OctolapseError
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octolapse.OctolapseSnapshotPlanPreview
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OctolapsePluginMessage(
    val type: Type? = null,
    val errors: List<OctolapseError>? = null,
    @SerialName("snapshot_plan_preview") val snapshotPlanPreview: OctolapseSnapshotPlanPreview? = null
) : OctoMessage, Message {
    @Serializable
    enum class Type {
        @SerialName("snapshot-plan-preview-complete")
        SnapshotPlanComplete,

        @SerialName("timelapse-start")
        TimelapseStart,

        @SerialName("gcode-preprocessing-start")
        GcodePreProcessingStart,

        @SerialName("gcode-preprocessing-update")
        GcodePreProcessingUpdate,
    }
}