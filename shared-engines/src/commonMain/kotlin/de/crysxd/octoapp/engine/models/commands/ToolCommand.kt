package de.crysxd.octoapp.engine.models.commands

@Suppress("Unused")
sealed class ToolCommand {
    data class SetTargetTemperature(val targets: Map<String, Float>) : ToolCommand()
    data class SetTemperatureOffset(val offsets: Map<String, Float>) : ToolCommand()
    data class ExtrudeFilament(val amount: Int) : ToolCommand()
}

