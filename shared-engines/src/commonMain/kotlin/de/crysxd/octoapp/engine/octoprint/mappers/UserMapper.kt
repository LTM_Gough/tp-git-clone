package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.login.User
import de.crysxd.octoapp.engine.octoprint.dto.login.OctoUser

internal fun OctoUser.map() = User(
    groups = groups,
    name = name ?: "guest",
    permissions = permissions,
)