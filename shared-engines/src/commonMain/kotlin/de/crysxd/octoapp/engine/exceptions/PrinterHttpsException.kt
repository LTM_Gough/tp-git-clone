package de.crysxd.octoapp.engine.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.http.config.X509Certificate
import io.ktor.http.Url

class PrinterHttpsException(
    url: Url,
    cause: Throwable,
    val certificate: X509Certificate?,
    val weakHostnameVerificationRequired: Boolean,
) : NetworkException(
    originalCause = cause,
    webUrl = url,
    userFacingMessage = "HTTPS connection to https://${url.host}:${url.port} could not be established. Make sure you installed all required certificates on your phone."
)