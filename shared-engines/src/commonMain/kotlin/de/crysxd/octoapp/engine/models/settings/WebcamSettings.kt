package de.crysxd.octoapp.engine.models.settings

import kotlinx.serialization.Serializable

@Serializable
data class WebcamSettings(
    val streamUrl: String? = null,
    val flipH: Boolean = false,
    val flipV: Boolean = false,
    val rotate90: Boolean = false,
    val webcamEnabled: Boolean = false,
    val streamRatio: String = "16:9",
    val snapshotUrl: String? = null,
)