package de.crysxd.octoapp.engine.octoprint.api.plugins.misc

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.mmu2filamentselect.Mmu2FilamentSelectChoice
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.request.post

class Mmu2FilamentSelectApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient
) {

    suspend fun selectChoice(choice: Int) = baseUrlRotator.request<Unit> {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.Mmu2FilamentSelect)
            setJsonBody(Mmu2FilamentSelectChoice(choice))
        }
    }
}