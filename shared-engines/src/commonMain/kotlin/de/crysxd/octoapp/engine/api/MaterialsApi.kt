package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.UniqueId
import de.crysxd.octoapp.engine.models.material.Material
import de.crysxd.octoapp.engine.models.settings.Settings

interface MaterialsApi {
    suspend fun isMaterialManagerAvailable(settings: Settings): Boolean
    suspend fun getMaterials(settings: Settings): List<Material>
    suspend fun activateMaterial(uniqueMaterialId: UniqueId)
}