package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.printer.ComponentTemperature
import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.engine.octoprint.dto.printer.OctoPrinterState

internal fun OctoPrinterState.map() = PrinterState(
    state = state.map(),
    temperature = temperature.mapValues { t ->
        ComponentTemperature(
            actual = t.value.actual,
            target = t.value.target
        )
    }
)

internal fun OctoPrinterState.State.map() = PrinterState.State(
    text = text,
    flags = PrinterState.Flags(
        operational = flags.operational,
        closedOrError = flags.closedOrError,
        cancelling = flags.cancelling,
        error = flags.error,
        paused = flags.paused,
        pausing = flags.pausing,
        printing = flags.printing,
        ready = flags.ready,
        sdReady = flags.sdReady,
        resuming = flags.resuming,
        finishing = flags.finishing,
    )
)