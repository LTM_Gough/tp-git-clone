package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.mystrom.MyStromCommand
import de.crysxd.octoapp.engine.octoprint.dto.plugins.ws281x.WS281xResponse
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.http.Url
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class WS281xApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings): List<IPowerDevice> = if (settings.plugins.wS281x != null) {
        listOf(
            TorchDevice(this),
            LightsDevice(this),
        )
    } else {
        emptyList()
    }

    private suspend fun executeCommand(command: String) = baseUrlRotator.request {
        httpClient.post {
            url(baseUrl = it)
            setJsonBody(MyStromCommand(command))
        }
    }

    private suspend fun turnLightsOn() {
        executeCommand("lights_on")
    }

    private suspend fun turnLightsOff() {
        executeCommand("lights_off")
    }

    private suspend fun turnTorchOn() {
        executeCommand("torch_on")
    }

    private suspend fun turnTorchOff() {
        executeCommand("torch_off")
    }

    private suspend fun isTorchOn() = getStatus().torchOn

    private suspend fun isLightsOn() = getStatus().lightsOn

    private suspend fun getStatus() = baseUrlRotator.request {
        httpClient.get {
            url(baseUrl = it)
        }
    }.body<WS281xResponse>()

    private fun HttpRequestBuilder.url(baseUrl: Url) = urlFromPath(baseUrl, "api", "plugin", OctoPlugins.Ws281xLedStatus)

    internal abstract class Device : IPowerDevice {
        override val pluginId = OctoPlugins.Ws281xLedStatus
        override val pluginDisplayName = "WS281x LED Status"
        override val capabilities = listOf(IPowerDevice.Capability.Illuminate)
    }

    internal data class TorchDevice(
        private val owner: WS281xApi,
    ) : Device() {
        override val id = "${OctoPlugins.Ws281xLedStatus}:torch"
        override val displayName = "WS281x Torch"
        override suspend fun turnOn() = owner.turnTorchOn()
        override suspend fun turnOff() = owner.turnTorchOff()
        override suspend fun isOn() = owner.isTorchOn()
    }

    internal data class LightsDevice(
        private val owner: WS281xApi,
    ) : Device() {
        override val id = "${OctoPlugins.Ws281xLedStatus}:lights"
        override val displayName = "WS281x Lights"
        override suspend fun turnOn() = owner.turnLightsOn()
        override suspend fun turnOff() = owner.turnLightsOff()
        override suspend fun isOn() = owner.isLightsOn()
    }
}