package de.crysxd.octoapp.engine.octoprint.dto.settings

import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.serializer.OctoRelaySerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class OctoSettings(
    val webcam: OctoWebcamSettings = OctoWebcamSettings(),
    val plugins: PluginSettingsGroup = PluginSettingsGroup(),
    val temperature: TemperatureSettings = TemperatureSettings(),
    val terminalFilters: List<TerminalFilter> = emptyList(),
    val appearance: Appearance = Appearance(),
) {

    @Serializable
    data class Appearance(
        val name: String? = null,
        val color: String = "default"
    )

    @Serializable
    data class TerminalFilter(
        val name: String,
        val regex: String
    )

    @Serializable
    data class TemperatureSettings(
        val profiles: List<TemperatureProfile> = emptyList()
    )

    @Serializable
    data class TemperatureProfile(
        val bed: Float?,
        val chamber: Float?,
        val extruder: Float?,
        val name: String
    )

    interface PluginSettings

    @Serializable
    data class PluginSettingsGroup(
        @SerialName(OctoPlugins.GcodeViewer) val gcodeViewer: GcodeViewerSettings? = null,
        @SerialName(OctoPlugins.OctoEverywhere) val octoEverywhere: OctoEverywhere? = null,
        @SerialName(OctoPlugins.Ngrok) val ngrok: Ngrok? = null,
        @SerialName(OctoPlugins.TheSpaghettiDetective) val spaghettiDetective: SpaghettiDetective? = null,
        @SerialName(OctoPlugins.Obico) val obico: SpaghettiDetective? = null,
        @SerialName(OctoPlugins.OctoApp) val octoAppCompanion: OctoAppCompanion? = null,
        @SerialName(OctoPlugins.MultiCam) val multiCam: MultiCam? = null,
        @SerialName(OctoPlugins.SpoolManager) val spoolManager: SpoolManager? = null,
        @SerialName(OctoPlugins.FilamentManager) val filamentManager: FilamentManager? = null,
        @SerialName(OctoPlugins.Discovery) val discovery: Discovery? = null,
        @SerialName(OctoPlugins.UploadAnything) val uploadAnything: UploadAnything? = null,
        @SerialName(OctoPlugins.Mmu2FilamentSelect) val mmu2FilamentSelect: Mmu2FilamentSelect? = null,
        @SerialName(OctoPlugins.CancelObject) val cancelObject: CancelObject? = null,
        @SerialName(OctoPlugins.PsuControl) val psuControl: PsuControl? = null,
        @SerialName(OctoPlugins.Wled) val wled: Wled? = null,
        @SerialName(OctoPlugins.OctoCam) val octoCam: OctoCam? = null,
        @SerialName(OctoPlugins.OctoLight) val octoLight: OctoLight? = null,
        @SerialName(OctoPlugins.Ophom) val ophom: Ophom? = null,
        @SerialName(OctoPlugins.OctoHue) val octoHue: OctoHue? = null,
        @SerialName(OctoPlugins.MyStromSwitch) val myStrom: MyStrom? = null,
        @SerialName(OctoPlugins.Ws281xLedStatus) val wS281x: WS281x? = null,
        @SerialName(OctoPlugins.Enclosure) val enclosure: Enclosure? = null,
        @SerialName(OctoPlugins.GpioControl) val gpioControl: GpioControl? = null,
        @Serializable(with = OctoRelaySerializer::class) @SerialName(OctoPlugins.OctoRelay) val octoRelay: OctoRelay? = null,
        @SerialName(OctoPlugins.Tasmota) val tasmota: Tasmota? = null,
        @SerialName(OctoPlugins.TpLinkSmartPlug) val tpLinkSmartPlug: TpLinkSmartPlug? = null,
        @SerialName(OctoPlugins.IkeaTradfri) val tradfri: Tradfri? = null,
        @SerialName(OctoPlugins.Tuya) val tuya: Tuya? = null,
        @SerialName(OctoPlugins.UsbRelayControl) val usbRelayControl: UsbRelayControl? = null,
        @SerialName(OctoPlugins.WemoSwitch) val wemoSwitch: WemoSwitch? = null,
    )

    @Serializable
    data class GcodeViewerSettings(
        val mobileSizeThreshold: Long = 0,
        val sizeThreshold: Long = 0
    ) : PluginSettings

    @Serializable
    data class Tradfri(
        @SerialName("selected_devices") val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {
        @Serializable
        data class Device(
            val id: String,
            val name: String,
        )
    }

    @Serializable
    data class Tuya(
        @SerialName("arrSmartplugs") val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {
        @Serializable
        data class Device(
            val label: String,
        )
    }

    @Serializable
    data class TpLinkSmartPlug(
        @SerialName("arrSmartplugs") val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {
        @Serializable
        data class Device(
            val ip: String,
            val label: String,
        )
    }

    @Serializable
    data class WemoSwitch(
        @SerialName("arrSmartplugs") val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {
        @Serializable
        data class Device(
            val ip: String,
            val label: String,
        )
    }

    @Serializable
    data class Tasmota(
        @SerialName("arrSmartplugs") val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {
        @Serializable
        data class Device(
            val ip: String,
            val idx: String,
            val label: String,
        )
    }

    @Serializable
    data class GpioControl(
        @SerialName("gpio_configurations") val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {
        @Serializable
        data class Device(
            val name: String,
        )
    }

    @Serializable
    data class Enclosure(
        @SerialName("rpi_outputs") val outputs: List<Output> = emptyList()
    ) : PluginSettings {
        @Serializable
        data class Output(
            val label: String,
            @SerialName("output_type") val type: String,
            @SerialName("index_id") val indexId: Int,
        )
    }

    @Serializable
    data class UsbRelayControl(
        @SerialName("usbrelay_configurations") val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {
        @Serializable
        data class Device(
            val name: String,
        )
    }

    @Serializable
    data class OctoRelay(
        val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {

        @Serializable
        data class Device(
            val id: String,
            val active: Boolean,
            val labelText: String,
        )
    }

    @Serializable
    class WS281x : PluginSettings

    @Serializable
    class Wled : PluginSettings

    @Serializable
    class OctoCam : PluginSettings

    @Serializable
    class OctoLight : PluginSettings

    @Serializable
    class Ophom : PluginSettings

    @Serializable
    class OctoHue : PluginSettings

    @Serializable
    class MyStrom : PluginSettings

    @Serializable
    class PsuControl : PluginSettings

    @Serializable
    class CancelObject : PluginSettings

    @Serializable
    class Ngrok(
        @SerialName("auth_name") val authName: String? = null,
        @SerialName("auth_pass") val authPassword: String? = null,
    ) : PluginSettings

    @Serializable
    class SpaghettiDetective : PluginSettings

    @Serializable
    class SpoolManager : PluginSettings

    @Serializable
    class FilamentManager : PluginSettings

    @Serializable
    data class OctoAppCompanion(
        @SerialName("encryptionKey") val encryptionKey: String?,
        @SerialName("version") val version: String?,
    ) : PluginSettings

    @Serializable
    data class MultiCam(
        @SerialName("multicam_profiles") val profiles: List<OctoWebcamSettings> = emptyList()
    ) : PluginSettings

    @Serializable
    data class Discovery(
        @SerialName("upnpUuid") val uuid: String?
    ) : PluginSettings

    @Serializable
    data class UploadAnything(
        @SerialName("allowed") val allowedExtensions: List<String> = emptyList()
    ) : PluginSettings

    @Serializable
    data class OctoEverywhere(
        @SerialName("PrinterKey") val printerKey: String? = null
    ) : PluginSettings

    @Serializable
    data class Mmu2FilamentSelect(
        val filament1: String? = null,
        val filament2: String? = null,
        val filament3: String? = null,
        val filament4: String? = null,
        val filament5: String? = null,
        val labelSource: LabelSource = LabelSource.Manual,
    ) : PluginSettings {
        @Serializable
        enum class LabelSource {
            @SerialName("manual")
            Manual,

            @SerialName("filamentManager")
            FilamentManager,

            @SerialName("spoolManager")
            SpoolManager,
        }
    }
}