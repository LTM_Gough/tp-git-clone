package de.crysxd.octoapp.engine.octoprint.dto.plugins.octolapse

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class OctolapseCommandBody(
    @SerialName("preprocessing_job_guid") val jobId: String?,
    val cancel: Boolean? = null,
)