package de.crysxd.octoapp.engine.exceptions.spaghettidetective

import de.crysxd.octoapp.engine.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.engine.exceptions.RemoteServiceConnectionBrokenException.Companion.REMOTE_SERVICE_TSD
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

class SpaghettiDetectiveTunnelNotFoundException(webUrl: Url) : NetworkException(
    userFacingMessage = "The connection with Spaghetti Detective was revoked, please connect Spaghetti Detective again.",
    technicalMessage = "Spaghetti Detective reported tunnel as deleted",
    webUrl = webUrl,
), RemoteServiceConnectionBrokenException {
    override val remoteServiceName = REMOTE_SERVICE_TSD
}