package de.crysxd.octoapp.engine.models.job

data class ProgressInformation(
    val completion: Float? = null,
    val filepos: Long? = null,
    val printTime: Int? = null,
    val printTimeLeft: Int? = null,
    val printTimeLeftOrigin: String? = null
) {

    @Suppress("unused")
    companion object {
        const val ORIGIN_LINEAR = "linear"
        const val ORIGIN_ANALYSIS = "analysis"
        const val ORIGIN_ESTIMATE = "estimate"
        const val ORIGIN_AVERAGE = "average"
        const val ORIGIN_GENIUS = "genius"
        const val ORIGIN_MIXED_ANALYSIS = "mixed-analysis"
        const val ORIGIN_MIXED_AVERAGE = "mixed-average"
    }
}