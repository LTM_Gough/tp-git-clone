package de.crysxd.octoapp.engine.models.printer

data class ComponentTemperature(
    val actual: Float? = null,
    val target: Float? = null,
)