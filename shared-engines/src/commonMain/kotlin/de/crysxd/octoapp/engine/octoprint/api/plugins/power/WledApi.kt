package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.mystrom.MyStromCommand
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.post
import io.ktor.http.Url
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class WledApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings): List<IPowerDevice> = if (settings.plugins.wled != null) {
        listOf(PowerDevice(this))
    } else {
        emptyList()
    }

    private suspend fun executeCommand(command: String) = baseUrlRotator.request {
        httpClient.post {
            url(baseUrl = it)
            setJsonBody(MyStromCommand(command))
        }
    }

    private suspend fun turnOn() {
        executeCommand("lights_on")
    }

    private suspend fun turnOff() {
        executeCommand("lights_off")
    }

    private fun HttpRequestBuilder.url(baseUrl: Url) = urlFromPath(baseUrl, "api", "plugin", OctoPlugins.Wled)

    internal data class PowerDevice(
        private val owner: WledApi,
    ) : IPowerDevice {
        override val id = "wleds"
        override val pluginId = OctoPlugins.Wled
        override val displayName = "WLEDs"
        override val pluginDisplayName = "myStrom"
        override val capabilities = listOf(IPowerDevice.Capability.Illuminate)
        override val controlMethods get() = listOf(IPowerDevice.ControlMethod.TurnOnOff)
        override suspend fun turnOn() = owner.turnOn()
        override suspend fun turnOff() = owner.turnOff()
        override suspend fun isOn() = null
    }
}