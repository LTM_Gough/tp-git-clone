package de.crysxd.octoapp.engine.octoprint.dto.event

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoEventTransportConfiguration(
    val subscription: OctoEventTransportSubscription = OctoEventTransportSubscription(),
    val throttle: OctoEventTransportThrottle = OctoEventTransportThrottle(),
)
