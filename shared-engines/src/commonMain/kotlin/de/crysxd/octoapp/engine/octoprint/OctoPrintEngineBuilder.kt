package de.crysxd.octoapp.engine.octoprint

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.octoprint.http.ExceptionInspector
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import io.ktor.http.Url

fun OctoPrintEngineBuilder(block: OctoPrintEngineBuilder.Scope.() -> Unit): OctoPrintEngine {
    val scope = object : OctoPrintEngineBuilder.Scope {
        override var baseUrls: List<String> = emptyList()
        override var apiKey: String = ""
        override var interpolateEvents: Boolean = false
        override var exceptionInspector: ExceptionInspector = ExceptionInspector {}
        override var httpClientSettings: HttpClientSettings? = null
        override var allowWebSocketTransport: Boolean = true
    }

    scope.block()

    require(scope.baseUrls.isNotEmpty()) { "At least one URL required" }
    val settings = requireNotNull(scope.httpClientSettings) { "Missing HttpClientSettings" }

    val bur = ActiveBaseUrlRotator(baseUrls = scope.baseUrls.map { Url(it) })
    return OctoPrintEngine(
        baseUrlRotator = bur,
        interpolateEvents = scope.interpolateEvents,
        allowWebSocketTransport = scope.allowWebSocketTransport,
        httpClient = createOctoPrintHttpClient(
            settings = OctoPrintEngineBuilder.OctoHttpClientSettings(
                general = settings,
                apiKey = scope.apiKey,
                exceptionInspector = scope.exceptionInspector,
                baseUrlRotator = bur,
            )
        )
    )
}

class OctoPrintEngineBuilder {
    interface Scope {
        var baseUrls: List<String>
        var apiKey: String
        var interpolateEvents: Boolean
        var exceptionInspector: ExceptionInspector
        var httpClientSettings: HttpClientSettings?
        var allowWebSocketTransport: Boolean
    }

    internal data class OctoHttpClientSettings(
        val general: HttpClientSettings = HttpClientSettings(),
        val baseUrlRotator: BaseUrlRotator,
        val apiKey: String,
        val exceptionInspector: ExceptionInspector = ExceptionInspector {},
    )
}




