@file:UseSerializers(OctoInstantSerializer::class)

package de.crysxd.octoapp.engine.octoprint.dto.event

import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileOrigin
import de.crysxd.octoapp.engine.octoprint.dto.job.OctoJobInformation
import de.crysxd.octoapp.engine.octoprint.dto.job.OctoProgressInformation
import de.crysxd.octoapp.engine.octoprint.dto.printer.OctoPrinterState
import de.crysxd.octoapp.engine.octoprint.serializer.OctoInstantSerializer
import kotlinx.datetime.Instant
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers

interface OctoMessage {

    @Serializable
    data class Connected(
        val version: String? = null,
        @SerialName("display_version") val displayVersion: String? = null
    ) : OctoMessage

    @Serializable
    data class Current(
        val logs: List<String> = emptyList(),
        val temps: List<OctoHistoricTemperatureData> = emptyList(),
        val state: OctoPrinterState.State = OctoPrinterState.State(),
        val progress: OctoProgressInformation = OctoProgressInformation(),
        val job: OctoJobInformation = OctoJobInformation(),
        val offsets: Map<String, Float>?,
        val serverTime: Instant,
        val isHistoryMessage: Boolean = false,
        val originHash: Int = 0,
    ) : OctoMessage

    @Serializable
    class ReAuthRequired : OctoMessage

    @Serializable
    class UnknownMessage : OctoMessage

    sealed class Event : OctoMessage {

        @Serializable
        object Connecting : Event()

        @Serializable
        object PrinterStateChanged : Event()

        @Serializable
        object UpdatedFiles : Event()

        @Serializable
        object PrintStarted : Event()

        @Serializable
        object PrintResumed : Event()

        @Serializable
        object PrintPausing : Event()

        @Serializable
        object PrintPaused : Event()

        @Serializable
        object PrintCancelling : Event()

        @Serializable
        object PrintCancelled : Event()

        @Serializable
        object PrintFailed : Event()

        @Serializable
        object PrintDone : Event()

        @Serializable
        object PrinterProfileModified : Event()

        @Serializable
        object SettingsUpdated : Event()

        @Serializable
        object MovieRendering : Event()

        @Serializable
        object MovieDone : Event()

        @Serializable
        object MovieFailed : Event()

        @Serializable
        object Disconnected : Event()

        @Serializable
        data class Unknown(val type: String) : Event()

        @Serializable
        data class FirmwareData(
            val firmwareName: String?,
            val machineType: String?,
            val extruderCount: Int?
        ) : Event()

        @Serializable
        data class FileSelected(
            val origin: OctoFileOrigin,
            val name: String,
            val path: String
        ) : Event()

        @Serializable
        data class PrinterConnected(
            val baudrate: Int?,
            val port: String?
        ) : Event()
    }
}