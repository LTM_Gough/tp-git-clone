package de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SpaghettiDetectiveStatus(
    @SerialName("linked_printer") val linkedPrinter: LinkedPrinter? = null
) {

    @Serializable
    data class LinkedPrinter(
        @SerialName("id") val id: String? = null,
        @SerialName("is_pro") val isPro: Boolean? = false,
        @SerialName("name") val name: String? = null,
    )
}