package de.crysxd.octoapp.engine.octoprint.dto.plugins.usbrelaycontrol

import kotlinx.serialization.Serializable

@Serializable
internal data class UsbRelayControlCommand(val command: String, val id: Int)