package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.Constants.OctoPrintApiKeyHeader
import de.crysxd.octoapp.engine.octoprint.Constants.OctoPrintSuppressApiKey
import de.crysxd.octoapp.engine.octoprint.Constants.OctoPrintTag
import de.crysxd.octoapp.engine.octoprint.dto.login.OctoUser
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.headers

internal class OctoProbeApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient
) {

    suspend fun probe() = try {
        baseUrlRotator.request {
            httpClient.get {
                headers {
                    remove(OctoPrintApiKeyHeader)
                }
                attributes.put(OctoPrintSuppressApiKey, true)
                urlFromPath(baseUrl = it, "api", "currentuser")
            }.body<OctoUser>().groups == listOf("guests")
        }
    } catch (e: Exception) {
        Napier.w(tag = OctoPrintTag, message = "Probe failed: ${e::class.qualifiedName}")
        false
    }
}
