package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.api.PrinterProfileApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.dto.printer.OctoPrinterProfileList
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get

internal class OctoPrinterProfileApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PrinterProfileApi {

    override suspend fun getPrinterProfiles() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "printerprofiles")
        }.body<OctoPrinterProfileList>()
    }.profiles.mapValues { v -> v.value.map() }
}