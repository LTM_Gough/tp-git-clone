package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.commands.ConnectionCommand
import de.crysxd.octoapp.engine.models.connection.ConnectionState

interface ConnectionApi {
    suspend fun getConnection(): ConnectionState

    suspend fun executeConnectionCommand(command: ConnectionCommand)
}