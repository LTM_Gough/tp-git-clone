package de.crysxd.octoapp.engine.models.commands

sealed class JobCommand {
    object PauseJobCommand : JobCommand()
    object ResumeJobCommand : JobCommand()
    object TogglePauseCommand : JobCommand()
    object CancelJobCommand : JobCommand()
    object StartJobCommand : JobCommand()
    object RestartJobCommand : JobCommand()
}