package de.crysxd.octoapp.engine.octoprint.dto.plugins.octocam

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoCamCommand(val command: String)

