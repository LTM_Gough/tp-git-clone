package de.crysxd.octoapp.engine.octoprint.dto.connection

import de.crysxd.octoapp.engine.octoprint.serializer.OctoConnectionStateSerializer
import kotlinx.serialization.Serializable

@Serializable
internal data class OctoPrintConnection(
    val state: State,
    val port: String? = null,
    val baudrate: Int? = null,
    val printerProfile: String
) {
    // This field is derived from a UI string and is not reliable!
    // This should only be used for UI states etc but not for important decisions
    @Serializable(with = OctoConnectionStateSerializer::class)
    internal enum class State {
        MAYBE_DETECTING_SERIAL_PORT,
        MAYBE_DETECTING_BAUDRATE,
        MAYBE_CONNECTING,
        MAYBE_OPERATIONAL,
        MAYBE_CLOSED,
        MAYBE_ERROR_FAILED_TO_AUTODETECT_SERIAL_PORT,
        MAYBE_CONNECTION_ERROR,
        MAYBE_DETECTING_SERIAL_CONNECTION,
        MAYBE_PRINTING,
        MAYBE_UNKNOWN_ERROR,
        OTHER,
    }
}