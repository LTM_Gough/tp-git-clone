package de.crysxd.octoapp.engine.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

class PrinterUnavailableException(e: Throwable? = null, webUrl: Url) : NetworkException(
    originalCause = e,
    webUrl = webUrl,
    userFacingMessage = "OctoPrint is not available, check your network connection.",
    technicalMessage = "${e?.message} $webUrl",
)