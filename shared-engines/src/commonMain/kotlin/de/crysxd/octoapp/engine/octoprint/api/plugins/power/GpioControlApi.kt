package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.gpiocontrol.GpioControlCommand
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.http.Url
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class GpioControlApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings): List<IPowerDevice> = settings.plugins.gpioControl?.devices?.map {
        PowerDevice(
            owner = this,
            index = it.index,
            displayName = it.name
        )
    } ?: emptyList()

    private suspend fun setOn(index: Int, on: Boolean) = baseUrlRotator.request<Unit> {
        httpClient.post {
            url(it)
            setJsonBody(GpioControlCommand(id = index, command = if (on) "turnGpioOn" else "turnGpioOff"))
        }
    }

    private suspend fun isOn(index: Int) = baseUrlRotator.request {
        httpClient.get {
            url(it)
        }.body<List<String>>().getOrNull(index) == "on"
    }

    private fun HttpRequestBuilder.url(baseUrl: Url) = urlFromPath(baseUrl, "api", "plugin", OctoPlugins.GpioControl)

    internal data class PowerDevice(
        private val owner: GpioControlApi,
        private val index: Int,
        override val displayName: String,
    ) : IPowerDevice {
        override val id: String = "$index"
        override val pluginId: String = OctoPlugins.GpioControl
        override val pluginDisplayName = "GPIO Control"
        override val capabilities = listOf(IPowerDevice.Capability.ControlPrinterPower, IPowerDevice.Capability.Illuminate)
        override suspend fun turnOn() = owner.setOn(index, true)
        override suspend fun turnOff() = owner.setOn(index, false)
        override suspend fun isOn() = owner.isOn(index)
    }
}