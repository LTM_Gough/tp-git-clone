package de.crysxd.octoapp.engine.octoprint.dto.plugins.companion

import kotlinx.serialization.Serializable

@Serializable
data class GetSnapshotBody(
    val size: Int,
    val webcamIndex: Int,
) {
    val command: String = "getWebcamSnapshot"
}