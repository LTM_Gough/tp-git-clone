package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.commands.BedCommand
import de.crysxd.octoapp.engine.models.commands.ChamberCommand
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.models.commands.PrintHeadCommand
import de.crysxd.octoapp.engine.models.commands.ToolCommand
import de.crysxd.octoapp.engine.models.printer.PrinterState

@Suppress("Unused")
interface PrinterApi {

    suspend fun getPrinterState(): PrinterState

    suspend fun executeChamberCommand(cmd: ChamberCommand)

    suspend fun executeToolCommand(cmd: ToolCommand)

    suspend fun executeBedCommand(cmd: BedCommand)

    suspend fun executePrintHeadCommand(cmd: PrintHeadCommand)

    suspend fun executeGcodeCommand(cmd: GcodeCommand)

}