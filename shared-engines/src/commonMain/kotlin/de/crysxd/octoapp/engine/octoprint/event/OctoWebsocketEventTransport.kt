package de.crysxd.octoapp.engine.octoprint.event

import de.crysxd.octoapp.engine.api.LoginApi
import de.crysxd.octoapp.engine.exceptions.PrinterApiException
import de.crysxd.octoapp.engine.exceptions.WebSocketUnstableException
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.getConnectionType
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.connection.ConnectionQuality
import de.crysxd.octoapp.engine.models.connection.ConnectionType
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoEventTransportAuthentication
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage
import de.crysxd.octoapp.engine.octoprint.http.OctoJson
import de.crysxd.octoapp.engine.octoprint.mappers.map
import de.crysxd.octoapp.engine.octoprint.serializer.OctoEventMessageSerializer
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedException
import de.crysxd.octoapp.sharedcommon.utils.asVersion
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.network.sockets.ConnectTimeoutException
import io.ktor.client.plugins.websocket.DefaultClientWebSocketSession
import io.ktor.client.plugins.websocket.webSocket
import io.ktor.http.URLProtocol
import io.ktor.http.Url
import io.ktor.websocket.Frame
import io.ktor.websocket.readText
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.time.Duration.Companion.seconds

internal class OctoWebsocketEventTransport(
    parentJob: Job,
    parenTag: String,
    loginApi: () -> LoginApi,
    private val probe: suspend () -> Unit,
    private val eventSink: OctoEventSource,
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
    private val onCurrentReceived: (Pair<String, OctoMessage.Current>?) -> Unit,
    private val onWebsocketBroken: suspend (OctoWebsocketEventTransport, Throwable) -> Unit,
) : OctoEventTransport(
    loginApi = loginApi,
    parentJob = parentJob,
    parenTag = parenTag,
) {

    private val noMessageTimeout = if (OctoWebsocketConfig.canUseLongNoMessageTimeout) OctoWebsocketConfig.longNoMessageTimeout else 5.seconds
    private var compatReAuthDetection = true

    private var messageCounter = 0L
    private var lastMessageReceivedAt = Instant.DISTANT_PAST
    private var currentMessageCounter = 0L
    private var reconnectCounter = 0
    private var currentBaseUrl: Url? = null
    private var websocketUrl: Url? = null
    override val name = "WS"

    override fun connect() {
        recreateScope().launch {
            // Any exception in this block is handled by the scope's exception handler
            messageCounter = 0
            currentMessageCounter = 0
            onCurrentReceived(null)

            Napier.i(tag = tag, message = "Starting connecting")
            baseUrlRotator.request { baseUrl ->
                currentBaseUrl = baseUrl
                Napier.i(tag = tag, message = "Connecting using $baseUrl")
                httpClient.webSocket(
                    request = {
                        urlFromPath(baseUrl = baseUrl, "sockjs", "websocket") {
                            protocol = if (baseUrl.protocol == URLProtocol.HTTPS) URLProtocol.WSS else URLProtocol.WS
                        }
                        websocketUrl = url.build()
                    }
                ) {
                    receiveAllMessagedAsync(connectionType = baseUrl.getConnectionType())
                    sendConfigurations()
                    ensureAuthValid()
                    sendAuthentication()
                    ensureConnectionAlive()

                    // Wait until we are closed.
                    job?.join()
                }
            }
        }.invokeOnCompletion {
            if (job?.isCancelled != true && job?.isActive == true && it == null) {
                // This happens on iOS when the baseUrl is changed. No biggy, just connect again.
                Napier.d(
                    tag = tag,
                    message = "Main loop exited but job not cancelled, connecting again after delay isCompleted=${job?.isCompleted} isCancelled=${job?.isCancelled} isActive=${job?.isActive}"
                )
                scope.launch {
                    delay(1.seconds)
                    Napier.w(tag = tag, message = "Re-connecting now")
                    connect()
                }
            }
        }
    }

    override fun handleException(e: Throwable) {
        scope.launch {
            when {
                // OkHttp throws this message when the WS does abruptly disconnect, this is a known bug in octo4a
                // if we never received a single message (lastMessageReceivedAt == distant past)
                e::class.simpleName == "EOFException" && lastMessageReceivedAt == Instant.DISTANT_PAST -> {
                    Napier.e(tag = tag, throwable = e, message = "EOF exception in WebSocket, connection not possible")
                    onWebsocketBroken(this@OctoWebsocketEventTransport, WebSocketUnstableException(e))
                }

                // When we fail to upgrade the websocket this has either one of those two Exception as a consequence
                e is PrinterApiException || e::class.qualifiedName == "java.net.ProtocolException" -> {
                    Napier.e(tag = tag, throwable = e, message = "API exception in WebSocket, upgrade failed")
                    onWebsocketBroken(this@OctoWebsocketEventTransport, WebSocketUnstableException(e))
                }

                e is ConnectTimeoutException -> currentBaseUrl?.let {
                    Napier.e(tag = tag, throwable = e, message = "Forwarding ConnectionTimeoutException to BaseUrlRotator")
                    baseUrlRotator.considerException(baseUrl = it, e = e)
                    connect()
                }

                else -> {
                    reconnectCounter++

                    if (e is WebsocketDeadException) {
                        // This is the iOS variant of the octo4a WS bug
                        if (reconnectCounter > 1) {
                            // This means the websocket is connecting but not healthy...we should fall back on an alternate transport mode
                            Napier.e(tag = tag, throwable = e, message = "Failed to establish a healthy connection after 2 successful connections, reporting broken")
                            onWebsocketBroken(this@OctoWebsocketEventTransport, WebSocketUnstableException(e))
                        } else {
                            // Reconnect immediately, we already waited for messages so no spamming
                            Napier.w(tag = tag, message = "Reconnecting after exception (${e::class.simpleName}) [1]")
                            connect()
                        }
                    } else {
                        // Report disconnected after the second attempt
                        if (reconnectCounter > 1) {
                            Napier.i(tag = tag, throwable = e, message = "Reporting disconnect")
                            eventSink.emitEvent(Event.Disconnected(e))
                        }

                        // On iOS basic auth, broken remote connection, invalid API key cause
                        // a weird exception when they occur on the websocket (not a "invalid response code"), so we need to probe
                        // manually
                        if (e::class.simpleName == "DarwinHttpRequestException") {
                            Napier.i(tag = tag, throwable = e, message = "Received error indicating invalid HTTP response code, probing...")
                            probe()
                        }

                        // Reconnect after delay to prevent spamming
                        // Needs min delay: https://github.com/square/okhttp/issues/7381
                        val delay = (1.seconds * (reconnectCounter - 1)).coerceAtMost(5.seconds).coerceAtLeast(1.seconds)
                        Napier.e(tag = tag, throwable = e, message = "Exception in WebSocket, retrying after ${delay.inWholeSeconds}s (attempt=$reconnectCounter)")
                        delay(delay)
                        Napier.w(tag = tag, message = "Reconnecting after exception (${e::class.simpleName}) [2]")
                        connect()
                    }
                }
            }
        }
    }

    private fun ensureAuthValid() = scope.launch {
        // Prior to OctoPrint 1.8.4 the only way to tell if the server declined the auth message
        // is to see if there is nothing send but the "connected" message. After 1.8.4 a "reauth" message is sent
        val minMessageCount = 3
        val time = 5.seconds
        delay(time)

        if (minMessageCount > messageCounter && compatReAuthDetection) {
            Napier.w(tag = tag, message = "Did not receive $minMessageCount messages within $time, authentication might be broken. Re-authenticating in new session.")
            reAuthenticate()
        }
    }

    private suspend fun reAuthenticate() {
        Napier.w(tag = tag, message = "Re-authenticating...")
        logOut()
        delay(1000)
        connect()
    }

    private suspend fun DefaultClientWebSocketSession.sendAuthentication() {
        val auth = logIn()
        Napier.i(tag = tag, message = "Sending auth: ${auth.split(":")[0]}:******")
        outgoing.send(Frame.Text(text = OctoJson().encodeToString(OctoEventTransportAuthentication(auth))))
    }

    private suspend fun ensureConnectionAlive() = scope.launch {
        Napier.i(tag = tag, message = "Ensuring connection stays alive (noMessageTimeout=$noMessageTimeout)")
        lastMessageReceivedAt = Clock.System.now()
        while (isActive) {
            delay(1.seconds)
            if (Clock.System.now() > lastMessageReceivedAt + noMessageTimeout) {
                throw WebsocketDeadException("No messages for $noMessageTimeout")
            }
        }
    }

    private fun DefaultClientWebSocketSession.sendConfigurations() = scope.launch {
        config.collectLatest {
            Napier.i(tag = tag, message = "Sending throttle: ${it.throttle}")
            outgoing.send(Frame.Text(Json.encodeToString(it.throttle)))
            Napier.i(tag = tag, message = "Sending subscription: ${it.subscription}")
            outgoing.send(Frame.Text(Json.encodeToString(it.subscription)))
        }
    }

    private fun DefaultClientWebSocketSession.receiveAllMessagedAsync(connectionType: ConnectionType) = scope.launch {
        while (isActive) {
            when (val frame = incoming.receive()) {
                is Frame.Binary -> Napier.v(tag = tag, message = "Received binary, ignoring")
                is Frame.Text -> {
                    val text = frame.readText()
                    Napier.v(tag = tag, message = "Received message, ${text.take(100)}")
                    receiveTextMessage(text, connectionType)
                }
                is Frame.Close -> {
                    Napier.v(tag = tag, message = "Received close")
                    disconnect()
                }
                is Frame.Ping -> Napier.v(tag = tag, message = "Received Ping")
                is Frame.Pong -> Napier.v(tag = tag, message = "Received Pong")
                else -> Napier.v(tag = tag, message = "Received unknown message: $frame")
            }

            lastMessageReceivedAt = Clock.System.now()
        }
    }

    private suspend fun receiveTextMessage(text: String, connectionType: ConnectionType) {
        handleFirstMessage(connectionType)

        try {
            val message = Json.decodeFromString(deserializer = OctoEventMessageSerializer(), string = text)

            when (message) {
                is OctoMessage.Connected -> {
                    // Prior to OctoPrint 1.8.4 the only way to tell if the server declined the auth message
                    // is to see if there is nothing send but the "connected" message. After 1.8.4 a "reauth" message is sent
                    val oldOctoPrint = message.version?.asVersion()?.let { "1.8.4".asVersion() > it } ?: true
                    compatReAuthDetection = oldOctoPrint
                    Napier.i(tag = tag, message = "Connected to OctoPrint ${message.version} (oldOctoPrint=$oldOctoPrint)")
                }

                is OctoMessage.ReAuthRequired -> {
                    // We need to reset the connection and re-authenticate. Do not emit.
                    scope.launch { reAuthenticate() }
                    return
                }

                is OctoMessage.Current -> {
                    // Forward to special handler (in addition to emitting below)
                    onCurrentReceived(text to message)
                }
            }

            if (message !is OctoMessage.UnknownMessage) {
                eventSink.emitEvent(Event.MessageReceived(message.map()))
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, throwable = e, message = "Failed to decode message: ${text.take(128)}")
        }
    }

    private suspend fun handleFirstMessage(connectionType: ConnectionType) {
        messageCounter++

        if (messageCounter == 1L) {
            Napier.i("Emitting connected via ${connectionType.name}")
            eventSink.emitEvent(Event.Connected(connectionType = connectionType, connectionQuality = ConnectionQuality.Normal))
            reconnectCounter = 0
        }
    }

    private fun WebSocketUnstableException(cause: Throwable) = WebSocketUnstableException(
        webSocketUrl = websocketUrl ?: currentBaseUrl ?: baseUrlRotator.activeUrl.value,
        webUrl = currentBaseUrl ?: baseUrlRotator.activeUrl.value,
        cause = cause,
    )

    private class WebsocketDeadException(message: String) : IllegalStateException(message), SuppressedException
}