package de.crysxd.octoapp.engine.octoprint.api.plugins.material

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.UniqueId
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.api.OctoMaterialApi
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spoolmanager.OctoSpoolManagerList
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spoolmanager.OctoSpoolManagerSelectionRequest
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.request.put
import de.crysxd.octoapp.engine.models.material.Material as IMaterial

internal class SpoolManagerApi(
    val baseUrlRotator: BaseUrlRotator,
    val httpClient: HttpClient
) : OctoMaterialApi.Delegate {

    override val providerId = OctoPlugins.SpoolManager

    override suspend fun isMaterialManagerAvailable(settings: Settings) = settings.plugins.spoolManager != null

    override suspend fun getMaterials(settings: Settings) = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "plugin", OctoPlugins.SpoolManager, "loadSpoolsByQuery")
            parameter("from", 0)
            parameter("to", 1000)
            parameter("sortColumn", "displayName")
            parameter("sortOrder", "desc")
            parameter("filterName", "hideEmptySpools,hideInactiveSpools")
        }.body<OctoSpoolManagerList>()
    }.let { response ->
        response.allSpools.filter {
            // Hide spools that
            // - Are templates
            // - Are not active
            // - Are empty
            it.databaseId != null && it.isActive != false && (it.remainingWeight ?: 1f) > 0 && it.isTemplate != true
        }.map {
            Material(
                id = UniqueId(
                    id = requireNotNull(it.databaseId) { "Database ID was validated to be not null" },
                    providerId = providerId
                ),
                displayName = it.displayName ?: it.databaseId.toString(),
                color = it.color?.normalizeColor(),
                colorName = it.colorName,
                vendor = it.vendor ?: "Unknown",
                material = it.material ?: "Unknown",
                providerDisplayName = "SpoolManager",
                activeToolIndex = response.selectedSpools.indexOfFirst { s -> s?.databaseId == it.databaseId },
                weightGrams = it.remainingWeight,
            )
        }
    }

    override suspend fun activateMaterial(uniqueMaterialId: UniqueId) = baseUrlRotator.request<Unit> {
        httpClient.put {
            urlFromPath(baseUrl = it, "plugin", "SpoolManager", "selectSpool")
            setJsonBody(OctoSpoolManagerSelectionRequest(uniqueMaterialId.id.toInt()))
        }
    }

    private fun String.normalizeColor() = "#${this.removePrefix("#").trim()}"

    @CommonParcelize
    data class Material(
        override val id: UniqueId,
        override val displayName: String,
        override val vendor: String,
        override val material: String,
        override val color: String?,
        override val colorName: String?,
        override val providerDisplayName: String,
        override val activeToolIndex: Int?,
        override val weightGrams: Float?
    ) : IMaterial {
        override fun copyWithName(displayName: String) = copy(displayName = displayName)
    }
}