package de.crysxd.octoapp.engine.octoprint.api.plugins.bundled

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.dto.plugins.pluginmanager.PluginList
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get

class PluginManagerApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) {

    suspend fun getFullPluginInfo() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "plugin", "pluginmanager", "plugins")
        }.body<PluginList>()
    }

    suspend fun getSimplePluginInfo() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "plugin", "pluginmanager", "plugins", "versions")
        }.body<Map<String, String?>>()
    }
}