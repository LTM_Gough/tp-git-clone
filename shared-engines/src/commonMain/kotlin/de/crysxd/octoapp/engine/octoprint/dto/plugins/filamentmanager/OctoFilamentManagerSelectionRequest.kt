package de.crysxd.octoapp.engine.octoprint.dto.plugins.filamentmanager

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoFilamentManagerSelectionRequest(
    val selection: Selection
) {
    @Serializable
    data class Selection(
        val tool: Int = 0,
        val spool: SpoolSelection
    )

    @Serializable
    data class SpoolSelection(
        val id: String
    )
}