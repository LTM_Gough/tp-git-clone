package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.api.SystemApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.system.SystemCommand
import de.crysxd.octoapp.engine.octoprint.dto.system.OctoSystemCommandList
import de.crysxd.octoapp.engine.octoprint.dto.system.OctoSystemInfo
import de.crysxd.octoapp.engine.octoprint.http.OctoJson
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.post
import kotlinx.serialization.decodeFromString

internal class OctoSystemApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : SystemApi {

    override suspend fun getSystemCommands() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "system", "commands")
        }.body<OctoSystemCommandList>()
    }.map()

    override suspend fun getSystemInfo() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "system", "info")
        }.body<OctoSystemInfo>()
    }.map()

    override suspend fun executeSystemCommand(command: SystemCommand) = baseUrlRotator.request<Unit> {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "system", "commands", command.source, command.action)
        }
    }
}