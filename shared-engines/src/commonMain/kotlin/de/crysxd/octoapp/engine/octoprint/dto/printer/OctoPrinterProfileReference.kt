package de.crysxd.octoapp.engine.octoprint.dto.printer

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoPrinterProfileReference(
    val id: String,
    val name: String? = null,
)