package de.crysxd.octoapp.engine.octoprint.dto.commands

import kotlinx.serialization.Serializable

@Serializable
sealed class OctoJobCommand(val command: String) {
    @Serializable
    class PauseJobCommand : OctoJobCommand("pause")

    @Serializable
    class ResumeJobCommand : OctoJobCommand("resume") {
        val action = "resume"
    }

    @Serializable
    class TogglePauseCommand : OctoJobCommand("pause") {
        val action = "toggle"
    }

    @Serializable
    class CancelJobCommand : OctoJobCommand("cancel")

    @Serializable
    class StartJobCommand : OctoJobCommand("start")

    @Serializable
    class RestartJobCommand : OctoJobCommand("restart")
}