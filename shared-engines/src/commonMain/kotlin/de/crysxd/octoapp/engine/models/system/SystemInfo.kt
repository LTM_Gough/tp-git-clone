package de.crysxd.octoapp.engine.models.system

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class SystemInfo(
    val generatedAt: Instant,
    val printerFirmware: String? = null,
    val safeMode: Boolean = false,
)