package de.crysxd.octoapp.engine.models.files

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize


sealed class FileObject : CommonParcelable {
    abstract val name: String
    abstract val display: String
    abstract val origin: FileOrigin
    abstract val path: String
    abstract val type: String
    abstract val typePath: List<String>
    abstract val size: Long?
    abstract val ref: Reference?

    val isPrintable get() = typePath.contains(FILE_TYPE_MACHINE_CODE)

    @CommonParcelize
    data class File(
        override val name: String,
        override val path: String,
        override val origin: FileOrigin,
        override val display: String = name,
        override val type: String = "unknown",
        override val typePath: List<String> = listOf("unknown"),
        override val ref: Reference? = null,
        override val size: Long? = null,
        val thumbnail: String? = null,
        val date: Long = 0,
        val hash: String? = null,
        val gcodeAnalysis: GcodeAnalysis? = null,
        val prints: PrintHistory? = null,
    ) : FileObject() {
        val extension get() = name.split(".").lastOrNull()
    }

    @CommonParcelize
    data class Folder(
        override val name: String,
        override val origin: FileOrigin,
        override val path: String,
        override val display: String = name,
        override val type: String = FILE_TYPE_FOLDER,
        override val typePath: List<String> = listOf(FILE_TYPE_FOLDER),
        override val ref: Reference? = null,
        override val size: Long? = null,
        val children: List<FileObject> = emptyList()
    ) : FileObject()

    @CommonParcelize
    data class Reference(
        val download: String?,
        val resource: String
    ) : CommonParcelable

    @CommonParcelize
    data class PrintHistory(
        val failure: Int = 0,
        val success: Int = 0,
        val last: LastPrint? = null
    ) : CommonParcelable {
        @CommonParcelize
        data class LastPrint(
            val date: Long,
            val success: Boolean
        ) : CommonParcelable
    }

    @CommonParcelize
    data class GcodeAnalysis(
        val dimensions: Dimensions? = null,
        val estimatedPrintTime: Float? = null,
        val filament: Map<String, FilamentUse> = emptyMap(),
    ) : CommonParcelable {
        @CommonParcelize
        data class Dimensions(
            val depth: Double,
            val height: Double,
            val width: Double,
        ) : CommonParcelable

        @CommonParcelize
        data class FilamentUse(
            val length: Double,
            val volume: Double,
        ) : CommonParcelable
    }

    companion object {
        const val FILE_TYPE_FOLDER = "folder"
        const val FILE_TYPE_MACHINE_CODE = "machinecode"
        const val FILE_TYPE_MACHINE_CODE_GCODE = "gcode"
    }
}