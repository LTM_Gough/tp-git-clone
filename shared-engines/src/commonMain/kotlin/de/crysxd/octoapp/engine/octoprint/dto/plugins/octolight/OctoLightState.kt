package de.crysxd.octoapp.engine.octoprint.dto.plugins.octolight

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoLightState(
    val state: Boolean = false
)
