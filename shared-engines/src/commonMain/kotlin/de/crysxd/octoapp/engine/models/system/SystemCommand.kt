package de.crysxd.octoapp.engine.models.system

import kotlinx.serialization.Serializable

@Serializable
data class SystemCommand internal constructor(
    val name: String,
    val action: String,
    val source: String,
    val confirmation: String?,
)