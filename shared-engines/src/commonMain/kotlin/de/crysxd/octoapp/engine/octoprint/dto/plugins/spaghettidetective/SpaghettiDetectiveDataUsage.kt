package de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SpaghettiDetectiveDataUsage(
    @SerialName("monthly_cap") val monthlyCapBytes: Double,
    @SerialName("reset_in_seconds") val resetInSeconds: Double,
    @SerialName("total") val totalBytes: Double
) {
    val hasDataCap get() = monthlyCapBytes > 0
}