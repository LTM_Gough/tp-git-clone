package de.crysxd.octoapp.engine.exceptions.spaghettidetective

import de.crysxd.octoapp.engine.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

class SpaghettiDetectiveTunnelUsageLimitReachedException(webUrl: Url) : NetworkException(
    userFacingMessage = "You used up the data volume of your Spaghetti Detective tunnel.\n\nSpaghetti Detective is disconnected for now, you can reconnect it again it in the „Configure Remote Access“ menu.",
    technicalMessage = "Received error code 481 from Spaghetti Detective",
    webUrl = webUrl,
), RemoteServiceConnectionBrokenException {
    override val remoteServiceName = "Spaghetti Detective"
}