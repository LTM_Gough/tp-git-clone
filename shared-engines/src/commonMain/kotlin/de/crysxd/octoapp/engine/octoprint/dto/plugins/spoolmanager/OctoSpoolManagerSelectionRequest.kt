package de.crysxd.octoapp.engine.octoprint.dto.plugins.spoolmanager

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoSpoolManagerSelectionRequest(
    val databaseId: Int,
    val toolIndex: Int = 0,
)