@file:Suppress("unused")

package de.crysxd.octoapp.engine.octoprint.dto.commands

import kotlinx.serialization.Serializable

@Serializable
sealed class OctoPrintHeadCommand {

    @Serializable
    data class JogPrintHeadCommand(val x: Float = 0f, val y: Float = 0f, val z: Float = 0f, val speed: Int? = null) : OctoPrintHeadCommand() {
        val command = "jog"
    }

    @Serializable
    data class HomePrintHeadCommand(val axes: List<String>) : OctoPrintHeadCommand() {
        val command = "home"
    }
}

