package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.version.VersionInfo

interface VersionApi {

    suspend fun getVersion(): VersionInfo

}