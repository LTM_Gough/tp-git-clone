package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.api.LoginApi
import de.crysxd.octoapp.engine.exceptions.InvalidApiKeyException
import de.crysxd.octoapp.engine.exceptions.PrinterApiException
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.login.LoginBody
import de.crysxd.octoapp.engine.octoprint.dto.login.OctoLoginResponse
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.post
import io.ktor.http.ContentType
import io.ktor.http.contentType

internal class OctoLoginApi(
    private val httpClient: HttpClient,
    private val baseUrlRotator: BaseUrlRotator
) : LoginApi {

    override suspend fun passiveLogin(body: LoginBody) = baseUrlRotator.request {
        try {
            httpClient.post {
                contentType(ContentType.Application.Json)
                urlFromPath(baseUrl = it, "api", "login")
                setJsonBody(body.map())
            }.body()
        } catch (e: PrinterApiException) {
            // Patch for 1.8.3's faulty behaviour. Invalid API key results in 400 and CSRF validation error
            if (e.body.contains("CSRF validation failed")) {
                OctoLoginResponse(session = null, groups = listOf(), name = null, admin = false)
            } else {
                throw e
            }
        } catch (e: InvalidApiKeyException) {
            // OctoPrint 1.8.4 will start returning 403 for an invalid API key. This restores the old behaviour of returning 200 and guest.
            OctoLoginResponse(session = null, groups = listOf(), name = null, admin = false)
        }
    }.map()
}
