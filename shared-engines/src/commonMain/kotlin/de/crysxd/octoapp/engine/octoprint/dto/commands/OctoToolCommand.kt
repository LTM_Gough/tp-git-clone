@file:Suppress("unused")

package de.crysxd.octoapp.engine.octoprint.dto.commands

import kotlinx.serialization.Serializable

@Serializable
sealed class OctoToolCommand {

    @Serializable
    data class SetTargetTemperature(val targets: Map<String, Float>) : OctoToolCommand() {
        val command = "target"
    }

    @Serializable
    data class SetTemperatureOffset(val offsets: Map<String, Float>) : OctoToolCommand() {
        val command = "offset"
    }

    @Serializable
    data class ExtrudeFilament(val amount: Int) : OctoToolCommand() {
        val command = "extrude"
    }
}

