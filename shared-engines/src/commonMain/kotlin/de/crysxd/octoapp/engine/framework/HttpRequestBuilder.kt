package de.crysxd.octoapp.engine.framework

import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.http.appendEncodedPathSegments
import io.ktor.http.appendPathSegments
import io.ktor.http.encodeURLPathPart
import io.ktor.http.takeFrom

fun HttpRequestBuilder.urlFromEncodedPath(
    baseUrl: Url,
    vararg pathSegments: String,
    additional: URLBuilder.() -> Unit = {}
) = url {
    takeFrom(baseUrl)
    appendEncodedPathSegments(pathSegments.toList())
    additional()
}

fun HttpRequestBuilder.urlFromPath(
    baseUrl: Url,
    vararg pathSegments: String,
    additional: URLBuilder.() -> Unit = {}
) = url {
    takeFrom(baseUrl)
    appendPathSegments(
        pathSegments.map {
            it.replace("%", "%25")
                .replace("&", "%26")
                .replace("?", "%3F")
                .encodeURLPathPart()
        }
    )
    additional()
}

fun HttpRequestBuilder.urlFromGivenValue(
    baseUrl: Url,
    givenValue: String,
    additional: URLBuilder.() -> Unit = {}
) = urlFromPath(
    baseUrl = baseUrl,
    pathSegments = givenValue.toUrl().pathSegments.map { it.encodeURLPathPart() }.toTypedArray(),
    additional = additional
)