package de.crysxd.octoapp.engine.octoprint.dto.plugins.wled

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class WledResponse(
    @SerialName("lights_on") val lightsOn: Boolean = false
)