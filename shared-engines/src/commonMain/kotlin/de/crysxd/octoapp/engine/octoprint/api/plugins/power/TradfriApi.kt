package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.exceptions.PrinterApiException
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.tradfri.TradfriCommand
import de.crysxd.octoapp.engine.octoprint.dto.plugins.tradfri.TradfriResponse
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.post
import io.ktor.http.Url
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class TradfriApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings): List<IPowerDevice> = settings.plugins.tradfri?.devices?.map {
        PowerDevice(
            owner = this,
            id = it.id,
            displayName = it.name
        )
    } ?: emptyList()

    private suspend fun sendCommand(device: PowerDevice, command: String) = try {
        baseUrlRotator.request {
            httpClient.post {
                url(it)
                setJsonBody(TradfriCommand(dev = TradfriCommand.Device(id = device.id, name = device.displayName), command = command))
            }
        }
    } catch (e: PrinterApiException) {
        if (e.responseCode == 500) {
            // That's basically a 200....
            null
        } else {
            throw e
        }
    }

    private suspend fun setOn(device: PowerDevice, on: Boolean) {
        sendCommand(device = device, command = if (on) "turnOn" else "turnOff")
    }

    private suspend fun isOn(device: PowerDevice) =
        sendCommand(device = device, command = "checkStatus")?.body<TradfriResponse>()?.state

    private fun HttpRequestBuilder.url(baseUrl: Url) = urlFromPath(baseUrl, "api", "plugin", OctoPlugins.IkeaTradfri)

    internal data class PowerDevice(
        private val owner: TradfriApi,
        override val id: String,
        override val displayName: String,
    ) : IPowerDevice {
        override val pluginId: String = OctoPlugins.IkeaTradfri
        override val pluginDisplayName = "Trådfri"
        override val capabilities = listOf(IPowerDevice.Capability.ControlPrinterPower, IPowerDevice.Capability.Illuminate)
        override suspend fun turnOn() = owner.setOn(device = this, on = true)
        override suspend fun turnOff() = owner.setOn(device = this, on = false)
        override suspend fun isOn() = owner.isOn(device = this)
    }
}