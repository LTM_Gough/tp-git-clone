package de.crysxd.octoapp.engine.models.material

import de.crysxd.octoapp.engine.models.UniqueId
import de.crysxd.octoapp.sharedcommon.CommonParcelable

interface Material : CommonParcelable {
    val id: UniqueId
    val displayName: String
    val vendor: String
    val material: String
    val color: String?
    val colorName: String?
    val providerDisplayName: String
    val activeToolIndex: Int?
    val weightGrams: Float?

    val isActivated
        get() = activeToolIndex?.takeIf { it >= 0 } != null

    fun copyWithName(displayName: String): Material
}