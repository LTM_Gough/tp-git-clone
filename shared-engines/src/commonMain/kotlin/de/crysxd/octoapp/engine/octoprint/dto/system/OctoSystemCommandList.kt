package de.crysxd.octoapp.engine.octoprint.dto.system

import kotlinx.serialization.Serializable

@Serializable
data class OctoSystemCommandList(
    val core: List<OctoSystemCommand> = emptyList(),
    val custom: List<OctoSystemCommand> = emptyList(),
    val plugin: List<OctoSystemCommand> = emptyList(),
)