package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.settings.Settings


interface SettingsApi {

    suspend fun getSettings(): Settings

}