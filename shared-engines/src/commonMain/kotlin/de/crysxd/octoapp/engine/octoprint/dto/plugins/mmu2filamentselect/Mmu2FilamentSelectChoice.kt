package de.crysxd.octoapp.engine.octoprint.dto.plugins.mmu2filamentselect

import kotlinx.serialization.Serializable

@Serializable
internal data class Mmu2FilamentSelectChoice(
    val choice: Int,
) {
    val command: String = "select"
}