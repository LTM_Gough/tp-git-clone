package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.login.User

interface UserApi {
    suspend fun getCurrentUser(): User
}