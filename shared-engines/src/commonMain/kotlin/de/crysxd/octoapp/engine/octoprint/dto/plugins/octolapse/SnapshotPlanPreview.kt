package de.crysxd.octoapp.engine.octoprint.dto.plugins.octolapse

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
@CommonParcelize
data class OctolapseSnapshotPlanPreview(
    @SerialName("preprocessing_job_guid") val jobId: String? = null,
    @SerialName("snapshot_plans") val snapshotPlans: SnapshotPlanList? = null
) : CommonParcelable {

    @Serializable
    @CommonParcelize
    data class SnapshotPlanList(
        @SerialName("snapshot_plans") val snapshotPlans: List<SnapshotPlan>? = null,
        @SerialName("printer_volume") val volume: PrinterVolume? = null,
    ) : CommonParcelable

    @Serializable
    @CommonParcelize
    data class SnapshotPlan(
        @SerialName("return_position") val returnPosition: Position? = null,
        @SerialName("initial_position") val initialPosition: Position? = null,
        @SerialName("steps") val steps: List<Step>? = null,
    ) : CommonParcelable

    @Serializable
    @CommonParcelize
    data class Position(
        val x: Float? = null,
        val y: Float? = null,
    ) : CommonParcelable

    @Serializable
    @CommonParcelize
    data class PrinterVolume(
        @SerialName("max_x") val width: Float? = null,
        @SerialName("max_y") val height: Float? = null,
        @SerialName("origin_type") val origin: Origin? = null,
    ) : CommonParcelable

    @Serializable
    enum class Origin {
        @SerialName("lowerleft")
        LowerLeft,

        @SerialName("center")
        Center
    }

    @Serializable
    @CommonParcelize
    data class Step(
        val action: String? = null,
        val x: Float? = null,
        val y: Float? = null,
    ) : CommonParcelable
}
