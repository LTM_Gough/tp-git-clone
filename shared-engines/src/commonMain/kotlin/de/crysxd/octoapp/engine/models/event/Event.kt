package de.crysxd.octoapp.engine.models.event

import de.crysxd.octoapp.engine.models.connection.ConnectionQuality
import de.crysxd.octoapp.engine.models.connection.ConnectionType

sealed class Event {
    data class Connected(val connectionType: ConnectionType, val connectionQuality: ConnectionQuality) : Event()
    data class Disconnected(val exception: Throwable? = null) : Event()
    data class MessageReceived(val message: Message, private val isSelfGenerated: Boolean = false) : Event()
}