package de.crysxd.octoapp.engine.octoprint.api.plugins.remote

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.SpaghettiCamFrame
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.SpaghettiDetectiveCommand
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.SpaghettiDetectiveDataUsage
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.SpaghettiDetectivePrediction
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.SpaghettiDetectiveStatus
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.http.appendEncodedPathSegments

class SpaghettiDetectiveApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) {

    suspend fun getPluginStatus() = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.Obico)
            setJsonBody(SpaghettiDetectiveCommand.GetPluginStatus())
        }.body<SpaghettiDetectiveStatus>()
    }

    suspend fun getSpaghettiCamFrame(webcamIndex: Int = 0) = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "_tsd_", "webcam", "$webcamIndex") {
                // Force trailing /
                appendEncodedPathSegments("")
            }
        }.body<SpaghettiCamFrame>()
    }

    suspend fun getDataUsage() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "_tsd_", "tunnelusage") {
                // Force trailing /
                appendEncodedPathSegments("")
            }
        }.body<SpaghettiDetectiveDataUsage>()
    }

    suspend fun getPrintPrediction() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "_tsd_", "prediction") {
                // Force trailing /
                appendEncodedPathSegments("")
            }
        }.body<SpaghettiDetectivePrediction>()
    }
}
