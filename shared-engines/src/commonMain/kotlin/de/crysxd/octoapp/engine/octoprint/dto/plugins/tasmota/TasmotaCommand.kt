package de.crysxd.octoapp.engine.octoprint.dto.plugins.tasmota

import kotlinx.serialization.Serializable

@Serializable
internal data class TasmotaCommand(val command: String, val ip: String, val idx: String)

