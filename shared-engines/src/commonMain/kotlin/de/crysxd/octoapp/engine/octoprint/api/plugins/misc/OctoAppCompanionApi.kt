package de.crysxd.octoapp.engine.octoprint.api.plugins.misc

import de.crysxd.octoapp.engine.exceptions.PrinterApiException
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.companion.AppRegistration
import de.crysxd.octoapp.engine.octoprint.dto.plugins.companion.GetFirmwareInfoBody
import de.crysxd.octoapp.engine.octoprint.dto.plugins.companion.GetSnapshotBody
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.post
import io.ktor.client.statement.bodyAsChannel

class OctoAppCompanionApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient
) {

    suspend fun registerApp(registration: AppRegistration) = baseUrlRotator.request<Unit> {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.OctoApp)
            setJsonBody(registration)
        }
    }

    suspend fun getFirmwareInfo(): String? = try {
        baseUrlRotator.request {
            httpClient.post {
                urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.OctoApp)
                setJsonBody(GetFirmwareInfoBody())
            }
        }.body<Map<String, String>>().map { "${it.key}: ${it.value}" }.joinToString(" ")
    } catch (e: PrinterApiException) {
        // 400 indicates that the plugin version does not yet have this command implemented
        if (e.responseCode == 400) {
            null
        } else {
            throw e
        }
    }

    suspend fun getWebcamSnapshot(maxSize: Int, webcamIndex: Int) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.OctoApp)
            setJsonBody(GetSnapshotBody(size = maxSize, webcamIndex = webcamIndex))
        }
    }.bodyAsChannel()
}