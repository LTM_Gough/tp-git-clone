package de.crysxd.octoapp.engine.octoprint.event

import de.crysxd.octoapp.engine.api.LoginApi
import de.crysxd.octoapp.engine.models.login.LoginResponse
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoEventTransportConfiguration
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.minutes

internal abstract class OctoEventTransport(
    private val loginApi: () -> LoginApi,
    private val parentJob: Job,
    private val parenTag: String,
) {
    private var connectionCounter = 0
    protected val tag get() = "$parenTag/$name/$connectionCounter"
    private var loginResponse: LoginResponse? = null
    private var loginResponseValidUntil = Instant.fromEpochSeconds(0)
    private val sessionDuration = 10.minutes
    protected val config = MutableStateFlow(OctoEventTransportConfiguration())
    protected abstract val name: String

    protected var job: Job? = null
        private set
    protected lateinit var scope: CoroutineScope
        private set

    protected fun recreateScope(): CoroutineScope {
        connectionCounter++
        Napier.i(tag = tag, message = "Recreating scope")
        job?.cancel()
        if (::scope.isInitialized) {
            scope.cancel()
        }
        val job = SupervisorJob(parentJob).also { job = it }
        scope = CoroutineScope(job + Dispatchers.SharedIO + CoroutineExceptionHandler { _, throwable -> handleException(throwable) })
        return scope
    }


    protected suspend fun logIn(): String {
        val lr = loginResponse
        return if (lr?.session != null && loginResponseValidUntil + sessionDuration > Clock.System.now()) {
            "${lr.name}:${lr.session}"
        } else {
            loginApi().passiveLogin().let {
                loginResponse = it
                loginResponseValidUntil = Clock.System.now() + sessionDuration
                "${it.name}:${it.session}"
            }
        }
    }

    protected fun logOut() {
        loginResponse = null
        loginResponseValidUntil = Instant.DISTANT_PAST
    }

    fun configure(configuration: OctoEventTransportConfiguration) {
        config.value = configuration
    }

    fun disconnect() {
        scope.cancel()
        Napier.i(tag = tag, message = "Disconnecting")
    }

    abstract fun connect()
    protected abstract fun handleException(e: Throwable)

}