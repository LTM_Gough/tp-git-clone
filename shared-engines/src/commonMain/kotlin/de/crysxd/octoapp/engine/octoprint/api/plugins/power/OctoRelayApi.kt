package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octorelay.OctoRelayCommand
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octorelay.OctoRelayResponse
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.post
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class OctoRelayApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings): List<IPowerDevice> = settings.plugins.octoRelay?.devices?.map {
        PowerDevice(
            owner = this,
            displayName = it.displayName,
            id = it.id,
        )
    } ?: emptyList()

    private suspend fun toggle(id: String) {
        request(id, "update")
    }

    private suspend fun isOn(id: String) = request(id, "getStatus").body<OctoRelayResponse>().status

    private suspend fun request(id: String, command: String) = baseUrlRotator.request { baseUrl ->
        httpClient.post {
            urlFromPath(baseUrl, "api", "plugin", OctoPlugins.OctoRelay)
            setJsonBody(OctoRelayCommand(command = command, pin = id))
        }
    }

    internal data class PowerDevice(
        private val owner: OctoRelayApi,
        override val displayName: String,
        override val id: String,
    ) : IPowerDevice {
        override val pluginId: String = OctoPlugins.OctoRelay
        override val pluginDisplayName = "OctoRelay"
        override val capabilities = listOf(IPowerDevice.Capability.ControlPrinterPower, IPowerDevice.Capability.Illuminate)
        override suspend fun turnOn() = if (isOn()) Unit else toggle()
        override suspend fun turnOff() = if (isOn()) toggle() else Unit
        override suspend fun isOn() = owner.isOn(id)
        override suspend fun toggle() = owner.toggle(id)
    }
}