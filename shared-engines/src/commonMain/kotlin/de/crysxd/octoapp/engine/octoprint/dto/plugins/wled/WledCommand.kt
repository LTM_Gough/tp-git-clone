package de.crysxd.octoapp.engine.octoprint.dto.plugins.wled

internal data class WledCommand(val command: String)