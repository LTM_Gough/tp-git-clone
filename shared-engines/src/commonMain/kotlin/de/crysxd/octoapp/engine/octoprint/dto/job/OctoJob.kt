package de.crysxd.octoapp.engine.octoprint.dto.job

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoJob(
    val progress: OctoProgressInformation = OctoProgressInformation(),
    val job: OctoJobInformation = OctoJobInformation()
)