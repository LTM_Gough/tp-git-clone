package de.crysxd.octoapp.engine.octoprint.dto.plugins.mystrom

import kotlinx.serialization.Serializable

@Serializable
internal data class MyStromCommand(val command: String)