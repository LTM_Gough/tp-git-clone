package de.crysxd.octoapp.engine.models.printer

data class PrinterProfileReference(
    val id: String,
    val name: String?,
)