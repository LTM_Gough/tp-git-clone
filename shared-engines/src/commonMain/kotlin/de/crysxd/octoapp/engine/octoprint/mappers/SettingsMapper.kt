package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoSettings

internal fun OctoSettings.map() = Settings(
    webcam = WebcamSettings(
        streamRatio = webcam.streamRatio,
        flipH = webcam.flipH,
        flipV = webcam.flipV,
        rotate90 = webcam.rotate90,
        snapshotUrl = webcam.snapshotUrl,
        streamUrl = webcam.standardStreamUrl ?: webcam.multiCamUrl,
        webcamEnabled = webcam.webcamEnabled,
    ),
    appearance = Settings.Appearance(
        name = appearance.name,
        color = appearance.color,
    ),
    temperatureProfiles = temperature.profiles.map {
        Settings.TemperatureProfile(
            name = it.name,
            bed = it.bed,
            chamber = it.chamber,
            extruder = it.extruder,
        )
    },
    terminalFilters = terminalFilters.map {
        Settings.TerminalFilter(
            name = it.name,
            regex = it.regex,
        )
    },
    plugins = Settings.PluginSettingsGroup(
        gcodeViewer = plugins.gcodeViewer?.let {
            Settings.GcodeViewer(
                mobileSizeThreshold = it.mobileSizeThreshold,
                sizeThreshold = it.sizeThreshold,
            )
        },
        octoEverywhere = plugins.octoEverywhere?.let {
            Settings.OctoEverywhere(
                printerKey = it.printerKey,
            )
        },
        cancelObject = plugins.cancelObject?.let {
            Settings.CancelObject
        },
        discovery = plugins.discovery?.let {
            Settings.Discovery(
                uuid = it.uuid,
            )
        },
        mmu2FilamentSelect = plugins.mmu2FilamentSelect?.let {
            Settings.Mmu2FilamentSelect(
                filament1 = it.filament1?.takeIf { it.isNotBlank() },
                filament2 = it.filament2?.takeIf { it.isNotBlank() },
                filament3 = it.filament3?.takeIf { it.isNotBlank() },
                filament4 = it.filament4?.takeIf { it.isNotBlank() },
                filament5 = it.filament5?.takeIf { it.isNotBlank() },
                labelSource = when (it.labelSource) {
                    OctoSettings.Mmu2FilamentSelect.LabelSource.Manual -> Settings.Mmu2FilamentSelect.LabelSource.Manual
                    OctoSettings.Mmu2FilamentSelect.LabelSource.FilamentManager -> Settings.Mmu2FilamentSelect.LabelSource.FilamentManager
                    OctoSettings.Mmu2FilamentSelect.LabelSource.SpoolManager -> Settings.Mmu2FilamentSelect.LabelSource.SpoolManager
                }
            )
        },
        multiCamSettings = plugins.multiCam?.let {
            Settings.MultiCam(
                profiles = it.profiles.map { cam ->
                    WebcamSettings(
                        streamRatio = cam.streamRatio,
                        flipH = cam.flipH,
                        flipV = cam.flipV,
                        rotate90 = cam.rotate90,
                        snapshotUrl = cam.snapshotUrl,
                        streamUrl = cam.standardStreamUrl ?: cam.multiCamUrl,
                        webcamEnabled = true,
                    )
                }
            )
        },
        ngrok = plugins.ngrok?.let {
            Settings.Ngrok(
                authName = it.authName,
                authPassword = it.authPassword,
            )
        },
        octoAppCompanion = plugins.octoAppCompanion?.let {
            Settings.OctoAppCompanion(
                version = it.version,
                encryptionKey = it.encryptionKey,
            )
        },
        spaghettiDetective = (plugins.spaghettiDetective ?: plugins.obico)?.let {
            Settings.SpaghettiDetective
        },
        uploadAnything = plugins.uploadAnything?.let {
            Settings.UploadAnything(
                allowedExtensions = it.allowedExtensions
            )
        },
        spoolManager = plugins.spoolManager?.let {
            Settings.SpoolManager
        },
        filamentManager = plugins.filamentManager?.let {
            Settings.FilamentManager
        },
        octoCam = plugins.octoCam?.let {
            Settings.OctoCam
        },
        myStrom = plugins.myStrom?.let {
            Settings.MyStrom
        },
        octoHue = plugins.octoHue?.let {
            Settings.OctoHue
        },
        octoLight = plugins.octoLight?.let {
            Settings.OctoLight
        },
        ophom = plugins.ophom?.let {
            Settings.Ophom
        },
        psuControl = plugins.psuControl?.let {
            Settings.PsuControl
        },
        wled = plugins.wled?.let {
            Settings.Wled
        },
        wS281x = plugins.wS281x?.let {
            Settings.WS281x
        },
        enclosure = plugins.enclosure?.let {
            Settings.Enclosure(
                outputs = it.outputs.map { o ->
                    Settings.Enclosure.Output(
                        indexId = o.indexId,
                        type = o.type,
                        label = o.label
                    )
                }
            )
        },
        gpioControl = plugins.gpioControl?.let {
            Settings.GpioControl(
                devices = it.devices.mapIndexed { index, device ->
                    Settings.GpioControl.Device(
                        index = index,
                        name = device.name
                    )
                }
            )
        },
        usbRelayControl = plugins.usbRelayControl?.let {
            Settings.UsbRelayControl(
                devices = it.devices.mapIndexed { index, device ->
                    Settings.UsbRelayControl.Device(
                        index = index,
                        name = device.name
                    )
                }
            )
        },
        tasmota = plugins.tasmota?.let {
            Settings.Tasmota(
                devices = it.devices.map { device ->
                    Settings.Tasmota.Device(
                        label = device.label,
                        idx = device.idx,
                        ip = device.ip
                    )
                }
            )
        },
        tpLinkSmartPlug = plugins.tpLinkSmartPlug?.let {
            Settings.TpLinkSmartPlug(
                devices = it.devices.map { device ->
                    Settings.TpLinkSmartPlug.Device(
                        label = device.label,
                        ip = device.ip
                    )
                }
            )
        },
        tradfri = plugins.tradfri?.let {
            Settings.Tradfri(
                devices = it.devices.map { device ->
                    Settings.Tradfri.Device(
                        name = device.name,
                        id = device.id
                    )
                }
            )
        },
        tuya = plugins.tuya?.let {
            Settings.Tuya(
                devices = it.devices.map { device ->
                    Settings.Tuya.Device(
                        label = device.label,
                    )
                }
            )
        },
        wemoSwitch = plugins.wemoSwitch?.let {
            Settings.WemoSwitch(
                devices = it.devices.map { device ->
                    Settings.WemoSwitch.Device(
                        label = device.label,
                        ip = device.ip,
                    )
                }
            )
        },
        octoRelay = plugins.octoRelay?.let {
            Settings.OctoRelay(
                devices = it.devices.mapNotNull { d ->
                    if (d.active) {
                        Settings.OctoRelay.Device(
                            id = d.id,
                            displayName = d.labelText
                        )
                    } else {
                        null
                    }
                }
            )
        },
    )
)