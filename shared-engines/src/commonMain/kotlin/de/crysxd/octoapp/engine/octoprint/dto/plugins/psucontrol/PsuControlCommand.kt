package de.crysxd.octoapp.engine.octoprint.dto.plugins.psucontrol

import kotlinx.serialization.Serializable

@Serializable
@Suppress("unused")
internal class PsuControlCommand(val command: String)