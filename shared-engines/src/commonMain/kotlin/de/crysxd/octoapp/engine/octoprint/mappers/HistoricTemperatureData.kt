package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.event.HistoricTemperatureData
import de.crysxd.octoapp.engine.models.printer.ComponentTemperature
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoHistoricTemperatureData
import de.crysxd.octoapp.engine.octoprint.dto.printer.OctoComponentTemperature

internal fun OctoHistoricTemperatureData.map() = HistoricTemperatureData(
    time = time.toEpochMilliseconds(),
    components = components.mapValues { it.value.map() }
)

internal fun OctoComponentTemperature.map() = ComponentTemperature(
    actual = actual,
    target = target
)