package de.crysxd.octoapp.engine.octoprint.dto.plugins.octolapse

import kotlinx.serialization.Serializable

@Serializable
data class OctolapseError(
    val name: String? = null,
    val description: String? = null,
)