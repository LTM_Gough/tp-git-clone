package de.crysxd.octoapp.engine.models.printer

typealias PrinterProfileList = Map<String, PrinterProfile>