package de.crysxd.octoapp.engine.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

open class InvalidApiKeyException(webUrl: Url) : NetworkException(
    webUrl = webUrl,
    technicalMessage = "OctoPrint reported an invalid API key when accessing $webUrl",
    userFacingMessage = "OctoPrint reported the API key as invalid"
)