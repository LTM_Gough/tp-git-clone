package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.system.SystemCommand
import de.crysxd.octoapp.engine.models.system.SystemCommandList
import de.crysxd.octoapp.engine.models.system.SystemInfo

interface SystemApi {

    suspend fun getSystemCommands(): SystemCommandList
    suspend fun getSystemInfo(): SystemInfo
    suspend fun executeSystemCommand(command: SystemCommand)

}