package de.crysxd.octoapp.engine.models.power

interface PowerDevice {
    val id: String
    val pluginId: String
    val displayName: String
    val pluginDisplayName: String
    val capabilities: List<Capability>
    val controlMethods: List<ControlMethod> get() = listOf(ControlMethod.TurnOnOff, ControlMethod.Toggle)

    suspend fun toggle() {
        val status = isOn() ?: throw IllegalStateException("Can't determine current state")
        if (status) turnOff() else turnOn()
    }

    suspend fun turnOn()
    suspend fun turnOff()
    suspend fun isOn(): Boolean?

    val uniqueId
        get() = "$pluginId:$id"

    sealed class Capability {
        object ControlPrinterPower : Capability()
        object Illuminate : Capability()
    }

    sealed class ControlMethod {
        object TurnOnOff : ControlMethod()
        object Toggle : ControlMethod()
    }
}