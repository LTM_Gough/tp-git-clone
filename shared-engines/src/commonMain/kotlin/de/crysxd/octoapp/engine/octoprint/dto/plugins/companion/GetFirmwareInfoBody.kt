package de.crysxd.octoapp.engine.octoprint.dto.plugins.companion

import kotlinx.serialization.Serializable

@Serializable
internal class GetFirmwareInfoBody {
    val command: String = "getPrinterFirmware"
}