package de.crysxd.octoapp.engine.exceptions

interface RemoteServiceConnectionBrokenException {
    val remoteServiceName: String

    companion object {
        const val REMOTE_SERVICE_TSD = "Obico"
        const val REMOTE_SERVICE_OCTO_EVERYWHERE = "OctoEverywhere"
        const val REMOTE_SERVICE_NGROK = "ngrok"
    }
}
