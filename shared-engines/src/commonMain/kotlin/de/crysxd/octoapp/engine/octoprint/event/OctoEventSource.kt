package de.crysxd.octoapp.engine.octoprint.event

import de.crysxd.octoapp.engine.EventSink
import de.crysxd.octoapp.engine.EventSource
import de.crysxd.octoapp.engine.api.LoginApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlin.time.Duration.Companion.seconds

internal class OctoEventSource(
    baseUrlRotator: BaseUrlRotator,
    interpolateEvents: Boolean,
    httpClient: HttpClient,
    loginApi: () -> LoginApi,
    probe: suspend () -> Unit,
    private val allowWebSocketTransport: Boolean,
) : EventSource, EventSink {

    private var mainTag = "Event/${eventSourceInstanceCounter++}"

    private val job = SupervisorJob()
    private val scope = CoroutineScope(job + Dispatchers.Main.immediate + CoroutineExceptionHandler { _, throwable ->
        Napier.e(tag = "$mainTag/scope", throwable = throwable, message = "Caught NON-CONTAINED exception in scope!")
    })

    private val interpolator = OctoEventInterpolator(
        interpolateEvents = interpolateEvents,
        consumer = this,
    )

    private var currentMessageCounter = 0L
    private var webSocketFailure: Throwable = IllegalStateException()
    private var webSocketBroken: Boolean = false
    private val webSocketTransport: OctoEventTransport = OctoWebsocketEventTransport(
        httpClient = httpClient,
        baseUrlRotator = baseUrlRotator,
        loginApi = loginApi,
        parentJob = job,
        parenTag = mainTag,
        eventSink = this,
        probe = probe,
        onCurrentReceived = ::onCurrentReceived,
        onWebsocketBroken = { ws, e ->
            Napier.e(tag = mainTag, message = "WebSocket transport is broken, switching to HTTP")
            ws.disconnect()
            webSocketBroken = true
            currentMessageCounter = 0L
            webSocketFailure = e
            httpTransport.connect()
        },
    )
    private val httpTransport: OctoEventTransport = OctoHttpEventTransport(
        httpClient = httpClient,
        baseUrlRotator = baseUrlRotator,
        loginApi = loginApi,
        parentJob = job,
        parenTag = mainTag,
        eventSink = this,
        onCurrentReceived = ::onCurrentReceived,
        degradationReason = { webSocketFailure },
        isBackupTransport = allowWebSocketTransport,
    )
    private val transport
        get() = when {
            webSocketBroken -> httpTransport
            allowWebSocketTransport -> webSocketTransport
            else -> httpTransport
        }
    private val eventFilters = mutableListOf<EventSource.Config>()

    internal val mutableActive = MutableStateFlow(false)
    internal val active = mutableActive.asStateFlow()

    private val mutableEventSource = MutableSharedFlow<Event>(replay = 16, extraBufferCapacity = 16)
    private val eventSource = mutableEventSource
        .onStart {
            Napier.i(tag = mainTag, message = "Connecting transport")
            transport.connect()
            currentMessageCounter = 0L
            mutableActive.value = true
        }
        .onCompletion {
            Napier.i(tag = mainTag, message = "Disconnecting transport")
            transport.disconnect()
            mutableActive.value = false

            // Reset websocket broken...we can try again with the WS next time the even stream connects
            webSocketBroken = false
        }
        .onEach { mutablePassiveEventSource.emit(it) }
        .shareIn(scope, started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 3.seconds.inWholeMilliseconds))

    private val mutablePassiveEventSource = MutableSharedFlow<Event>(replay = 16, extraBufferCapacity = 16)

    internal fun reconnect() {
        transport.disconnect()
        transport.connect()
    }

    private fun onCurrentReceived(current: Pair<String, OctoMessage.Current>?) {
        current?.let { logCurrentMessage(it.first, it.second) }
        interpolator.lastCurrentMessage = current?.second
    }

    override fun eventFlow(tag: String, config: EventSource.Config) = eventSource
        .onStart {
            Napier.d(tag = mainTag, message = "onStart for Flow (tag=$tag, eventSource=${this@OctoEventSource.toString().split("@")[1]}, filters=$config)")
            eventFilters += config
            eventFilters.buildEventTransportConfig().let {
                webSocketTransport.configure(it)
                httpTransport.configure(it)
            }
        }
        .onCompletion {
            Napier.d(tag = mainTag, message = "onCompletion for Flow (tag=$tag, eventSource=${this@OctoEventSource.toString().split("@")[1]})")
            eventFilters -= config
        }

    override fun passiveEventFlow() = mutablePassiveEventSource
        .asSharedFlow()
        .filterNotNull()

    override suspend fun injectInterpolatedEvent(event: (Message.Current?) -> Message.Event) {
        interpolator.injectEvent(event(interpolator.lastCurrentMessage?.map()))
    }

    override suspend fun injectInterpolatedPrintStart(file: FileObject.File) {
        interpolator.injectPrintStart(file)
    }

    override suspend fun injectInterpolatedTemperatureTarget(targets: Map<String, Float>) {
        interpolator.injectTemperatureChange(targets)
    }

    override suspend fun emitEvent(e: Event) {
        if (e !is Event.MessageReceived) {
            Napier.i(tag = mainTag, message = "Event: $e")
        }

        mutableEventSource.emit(e)
    }

    private fun logCurrentMessage(text: String, message: OctoMessage.Current) = if (message.isHistoryMessage || (currentMessageCounter++ % 40) != 0L) {
        //Nothing to log
    } else {
        Napier.d(tag = mainTag, message = "Current message ${currentMessageCounter - 1} received: $text -> $message")
    }
}