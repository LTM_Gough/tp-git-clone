package de.crysxd.octoapp.engine.octoprint.dto.settings

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OctoWebcamSettings(
    @SerialName("streamUrl") val standardStreamUrl: String? = null,
    @SerialName("URL") val multiCamUrl: String? = null,
    val flipH: Boolean = false,
    val flipV: Boolean = false,
    val rotate90: Boolean = false,
    val webcamEnabled: Boolean = false,
    val streamRatio: String = "16:9",
    val snapshotUrl: String? = null,
)