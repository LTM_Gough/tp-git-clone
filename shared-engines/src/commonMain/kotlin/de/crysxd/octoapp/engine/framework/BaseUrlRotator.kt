package de.crysxd.octoapp.engine.framework

import de.crysxd.octoapp.engine.exceptions.PrinterUnavailableException
import io.github.aakira.napier.Napier
import io.ktor.client.network.sockets.SocketTimeoutException
import io.ktor.client.plugins.HttpRequestTimeoutException
import io.ktor.http.Url
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.time.Duration.Companion.seconds

interface BaseUrlRotator {
    suspend fun <T> request(block: suspend (Url) -> T): T
    suspend fun considerException(baseUrl: Url, e: Throwable)
    val activeUrl: StateFlow<Url>
}

class InactiveBaseUrlRotator(private val baseUrl: Url) : BaseUrlRotator {
    override suspend fun <T> request(block: suspend (Url) -> T) = block(baseUrl)
    override suspend fun considerException(baseUrl: Url, e: Throwable) = Unit
    override val activeUrl = MutableStateFlow(baseUrl)
}

class ActiveBaseUrlRotator(private val baseUrls: List<Url>) : BaseUrlRotator {

    private val tag = "ActiveBaseUrlRotator"
    private var continuousCheckJob: Job? = null
    private var activeCollectionJob: Job? = null
    private var connectionProbe: (suspend (BaseUrlRotator) -> Boolean)? = null
    private var onConnectionChange: () -> Unit = {}
    private val job by lazy { SupervisorJob() }
    private val scope by lazy {
        CoroutineScope(job + Dispatchers.Main.immediate + CoroutineExceptionHandler { _, throwable ->
            Napier.e(tag = "BaseUrlRotator/scope", throwable = throwable, message = "Caught NON-CONTAINED exception in scope!")
        })
    }

    private val mutex = Mutex()
    private var lastException: Throwable? = null
    private val mutableActiveUrl = MutableStateFlow(baseUrls[0])
    override val activeUrl = mutableActiveUrl.asStateFlow()
    private var activeUrlIndex: Int = 0
        set(value) {
            mutableActiveUrl.value = baseUrls[value]
            field = value
        }

    init {
        require(baseUrls.isNotEmpty()) { "Need at least one base URL, none given!" }
        Napier.d(tag = tag, message = "Creating BaseUrlRotator with selection: $baseUrls")
        Napier.i(tag = tag, message = "Initial base: ${baseUrls[0]} (${baseUrls.size - 1} alternatives)")
    }

    override suspend fun <T> request(block: suspend (Url) -> T): T {
        val baseUrlsForRequest = ((activeUrlIndex until baseUrls.size) + (0 until activeUrlIndex)).map { baseUrls[it] }
        var exception: Throwable = IllegalStateException("Should not be used")

        return baseUrlsForRequest.firstNotNullOfOrNull { baseUrl ->
            try {
                block(baseUrl)
            } catch (e: CancellationException) {
                throw  e
            } catch (e: Exception) {
                if (e.canBeSolvedByBaseUrlChange()) {
                    exception = e
                    considerException(baseUrl, e)
                    null
                } else {
                    Napier.d(tag = tag, message = "Caught '${e::class.simpleName}' which can't be solved by a URL change. Escalating.")
                    throw e
                }
            }
        } ?: let {
            Napier.e("All base Urls failed. Reporting last exception")
            throw exception
        }
    }

    override suspend fun considerException(baseUrl: Url, e: Throwable) = mutex.withLock {
        if (e.canBeSolvedByBaseUrlChange()) {
            val currentBase = baseUrls[activeUrlIndex]
            if (baseUrl == currentBase && e != lastException) {
                lastException = e
                activeUrlIndex = (activeUrlIndex + 1) % baseUrls.size
                val newBase = baseUrls[activeUrlIndex]
                Napier.d(tag = tag, message = "Caught '${e::class.simpleName}', can be solved by base URL change. Re-attempting...")
                Napier.i(tag = tag, message = "Switching: $currentBase -> $newBase")
            } else {
                Napier.d(tag = tag, message = "Already switched from $baseUrl -> $currentBase")
            }
        } else {
            Napier.d(tag = tag, message = "Got '${e::class.simpleName}' reported for consideration which can't be solved by a URL change. Ignoring.")
        }
    }

    private fun Throwable.canBeSolvedByBaseUrlChange(): Boolean = if (
    // Usually it's always this one
        this is PrinterUnavailableException ||

        // CIO Engine
        this::class.qualifiedName == "java.nio.channels.UnresolvedAddressException" ||

        // OkHTTP Engine
        this::class.qualifiedName == "java.net.ConnectException" ||
        this::class.qualifiedName == "java.net.UnknownHostException" ||
        this::class.qualifiedName == "java.net.SocketException" ||

        // Darwin Engine
        this.message?.contains("NSURLErrorDomain") == true ||
        this.message?.contains("NSErrorFailingURLStringKey") == true ||

        // Generic
        this::class.qualifiedName == "io.ktor.client.network.sockets.ConnectTimeoutException" ||
        this::class.qualifiedName == "io.ktor.client.plugins.ConnectTimeoutException" ||
        this is HttpRequestTimeoutException ||
        this is SocketTimeoutException
    ) {
        true
    } else {
        cause?.canBeSolvedByBaseUrlChange() == true
    }

    suspend fun considerUpgradingConnection() {
        val probe = connectionProbe ?: return Napier.i(tag = tag, message = "Skipping upgrade connection check, no probe available")

        // Test all baseUrls but last. If a baseUrl is working with an higher index than the active one we switch to it
        // We skip the last because...no point in switching to it as it will be already active if all others are offline
        baseUrls.dropLast(1).forEachIndexed { index, baseUrl ->
            try {
                val nestedRotator = InactiveBaseUrlRotator(baseUrl)
                when {
                    !probe(nestedRotator) -> Napier.i(tag = tag, message = "$baseUrl OFFLINE")

                    activeUrlIndex != index -> {
                        Napier.i("$baseUrl ONLINE")
                        activeUrlIndex = index
                        onConnectionChange()

                        // First one passed, not interested in others
                        return
                    }

                    else -> {
                        Napier.i(tag = tag, message = "$baseUrl ONLINE (already active)")

                        // First one passed, not interested in others
                        return
                    }
                }
            } catch (e: Exception) {
                Napier.i(tag = tag, message = "$baseUrl OFFLINE (${e::class.qualifiedName})")
            }
        }
    }

    fun consumeActiveFlow(
        active: Flow<Boolean>,
        probe: suspend (BaseUrlRotator) -> Boolean,
        onChange: () -> Unit,
    ) {
        connectionProbe = probe
        onConnectionChange = onChange
        activeCollectionJob?.cancel()
        activeCollectionJob = scope.launch {
            active.collectLatest {
                if (it) {
                    continuousCheckJob?.cancel()
                    continuousCheckJob = starContinuousCheck()
                }
            }
        }
    }

    private fun starContinuousCheck() = scope.launch {
        while (isActive) {
            delay(30.seconds)
            considerUpgradingConnection()
        }
    }
}
