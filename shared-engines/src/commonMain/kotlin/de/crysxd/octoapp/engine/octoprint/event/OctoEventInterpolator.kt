package de.crysxd.octoapp.engine.octoprint.event

import de.crysxd.octoapp.engine.EventSink
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoHistoricTemperatureData
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage
import de.crysxd.octoapp.engine.octoprint.dto.job.OctoJobInformation
import de.crysxd.octoapp.engine.octoprint.dto.job.OctoProgressInformation
import de.crysxd.octoapp.engine.octoprint.dto.printer.OctoPrinterState
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.github.aakira.napier.Napier
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.seconds

internal class OctoEventInterpolator(
    private val interpolateEvents: Boolean,
    private val consumer: EventSink,
) {

    private val interpolationThreshold = 1.seconds
    var lastCurrentMessage: OctoMessage.Current? = null
    private val currentMessageInterpolations = mutableListOf<Pair<Instant, (OctoMessage.Current) -> OctoMessage.Current>>()

    suspend fun injectEvent(event: Message.Event) {
        if (interpolateEvents) {
            injectCurrentInterpolation { it.upgrade(event) ?: it }
            consumer.emitEvent(Event.MessageReceived(event, isSelfGenerated = true))
        }
    }

    private suspend fun injectCurrentInterpolation(interpolation: (OctoMessage.Current) -> OctoMessage.Current) {
        if (interpolateEvents) {
            currentMessageInterpolations.add(Clock.System.now() to interpolation)

            lastCurrentMessage = lastCurrentMessage?.withInterpolations()?.also {
                consumer.emitEvent(Event.MessageReceived(it.map(), isSelfGenerated = true))
            }
        }
    }

    suspend fun injectTemperatureChange(targets: Map<String, Float>) {
        injectCurrentInterpolation {
            val lastTemp = it.temps.lastOrNull() ?: return@injectCurrentInterpolation it

            it.copy(
                temps = it.temps + OctoHistoricTemperatureData(
                    time = Clock.System.now(),
                    components = lastTemp.components.mapValues {
                        it.value.copy(target = targets.getOrElse(it.key) { it.value.target })
                    }
                )
            )
        }
    }

    private fun OctoMessage.Current.withInterpolations(): OctoMessage.Current {
        val now = Clock.System.now()
        currentMessageInterpolations.removeAll { now > it.first + interpolationThreshold }
        var message = this
        currentMessageInterpolations.forEach {
            message = it.second(message)
        }
        return message
    }

    fun OctoMessage.Current.upgrade(event: Message.Event): OctoMessage.Current? {
        if (event is Message.Event.Unknown) return null
        Napier.w(tag = "OctoEventInterpolator", message = "Upgrading with event: ${event::class.simpleName}")

        return when (event) {
            is Message.Event.PrintStarted -> updateFlags(resetProgress = true) { copy(printing = true, paused = false, pausing = false, cancelling = false) }
            is Message.Event.PrintPaused -> updateFlags { copy(printing = true, paused = true, pausing = false, cancelling = false) }
            is Message.Event.PrintPausing -> updateFlags { copy(printing = true, paused = false, pausing = true, cancelling = false) }
            is Message.Event.PrintCancelling -> updateFlags { copy(printing = true, pausing = false, paused = false, cancelling = true) }
            is Message.Event.PrintFailed -> updateFlags { copy(printing = false, pausing = false, paused = false, cancelling = false) }
            is Message.Event.PrintDone -> updateFlags { copy(printing = false, pausing = false, paused = false, cancelling = false) }
            is Message.Event.PrintResumed -> updateFlags { copy(printing = true, pausing = false, paused = false, cancelling = false) }
            else -> null
        }
    }

    fun OctoMessage.Current.updateFlags(
        resetProgress: Boolean = false,
        block: OctoPrinterState.Flags.() -> OctoPrinterState.Flags
    ) = this.state.flags.let(block).let {
        copy(
            state = state.copy(flags = it),
            progress = progress.takeUnless { resetProgress } ?: OctoProgressInformation()
        )
    }

    suspend fun injectPrintStart(file: FileObject.File) {
        injectCurrentInterpolation {
            it.copy(
                progress = OctoProgressInformation(
                    completion = 0f,
                    filepos = 0,
                    printTime = 0,
                    printTimeLeft = 0,
                    printTimeLeftOrigin = null
                ),
                state = OctoPrinterState.State(
                    text = "Printing...",
                    flags = OctoPrinterState.Flags(
                        operational = true,
                        printing = true,
                        pausing = false,
                        paused = false,
                        cancelling = false,
                        error = false,
                        closedOrError = false,
                        ready = true,
                        sdReady = true
                    )
                ),
                job = OctoJobInformation(
                    file = OctoJobInformation.File(
                        name = file.name,
                        path = file.path,
                        origin = file.origin.map(),
                        date = Instant.fromEpochMilliseconds(file.date),
                        size = file.size,
                        display = file.display,
                    )
                )
            )
        }
    }
}