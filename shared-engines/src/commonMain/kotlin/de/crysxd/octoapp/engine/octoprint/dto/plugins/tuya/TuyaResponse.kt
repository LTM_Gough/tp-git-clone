package de.crysxd.octoapp.engine.octoprint.dto.plugins.tuya

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TuyaResponse(
    val currentState: State = State.UNKNOWN
) {
    @Serializable
    enum class State {
        @SerialName("on")
        ON,

        @SerialName("off")
        OFF,

        @SerialName("unknown")
        UNKNOWN
    }
}