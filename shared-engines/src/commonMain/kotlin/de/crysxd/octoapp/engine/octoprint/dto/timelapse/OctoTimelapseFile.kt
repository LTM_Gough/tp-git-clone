@file:UseSerializers(OctoInstantSerializer::class)

package de.crysxd.octoapp.engine.octoprint.dto.timelapse

import de.crysxd.octoapp.engine.octoprint.serializer.OctoInstantSerializer
import kotlinx.datetime.Instant
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers

@Serializable
data class OctoTimelapseFile(
    val name: String,
    val bytes: Long,
    @SerialName("timestamp") val date: Instant? = null,
    val url: String? = null,
    val thumbnail: String? = null,
    val processing: Boolean = false,
    val rendering: Boolean = false,
    val recording: Boolean = false,
)