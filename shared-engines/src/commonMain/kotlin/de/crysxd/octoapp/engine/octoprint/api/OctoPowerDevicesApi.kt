package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.currentCoroutineContext

class OctoPowerDevicesApi(
    private val delegates: List<PowerDevicesApi>
) : PowerDevicesApi {
    override suspend fun getDevices(settings: Settings) = with(CoroutineScope(currentCoroutineContext() + Dispatchers.SharedIO)) {
        delegates
            .map { async { it.getDevices(settings) } }
            .flatMap { it.await() }
    }
}