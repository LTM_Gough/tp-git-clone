package de.crysxd.octoapp.engine.octoprint

import de.crysxd.octoapp.engine.EventSource
import de.crysxd.octoapp.engine.PrinterEngine
import de.crysxd.octoapp.engine.api.ConnectionApi
import de.crysxd.octoapp.engine.api.FilesApi
import de.crysxd.octoapp.engine.api.JobApi
import de.crysxd.octoapp.engine.api.LoginApi
import de.crysxd.octoapp.engine.api.MaterialsApi
import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.api.PrinterApi
import de.crysxd.octoapp.engine.api.PrinterProfileApi
import de.crysxd.octoapp.engine.api.SettingsApi
import de.crysxd.octoapp.engine.api.SystemApi
import de.crysxd.octoapp.engine.api.TimelapseApi
import de.crysxd.octoapp.engine.api.UserApi
import de.crysxd.octoapp.engine.api.VersionApi
import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.octoprint.api.OctoConnectionApi
import de.crysxd.octoapp.engine.octoprint.api.OctoFilesApi
import de.crysxd.octoapp.engine.octoprint.api.OctoJobApi
import de.crysxd.octoapp.engine.octoprint.api.OctoLoginApi
import de.crysxd.octoapp.engine.octoprint.api.OctoMaterialApi
import de.crysxd.octoapp.engine.octoprint.api.OctoPowerDevicesApi
import de.crysxd.octoapp.engine.octoprint.api.OctoPrinterApi
import de.crysxd.octoapp.engine.octoprint.api.OctoPrinterProfileApi
import de.crysxd.octoapp.engine.octoprint.api.OctoProbeApi
import de.crysxd.octoapp.engine.octoprint.api.OctoSettingsApi
import de.crysxd.octoapp.engine.octoprint.api.OctoSystemApi
import de.crysxd.octoapp.engine.octoprint.api.OctoTimelapseApi
import de.crysxd.octoapp.engine.octoprint.api.OctoUserApi
import de.crysxd.octoapp.engine.octoprint.api.OctoVersionApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.bundled.ApplicationKeysApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.bundled.PluginManagerApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.material.FilamentManagerApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.material.SpoolManagerApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.misc.CancelObjectApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.misc.Mmu2FilamentSelectApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.misc.OctoAppCompanionApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.misc.OctolapseApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.EnclosureApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.GpioControlApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.MyStromApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.OctoCamApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.OctoHueApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.OctoLightApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.OctoRelayApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.OphomApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.PsuControlApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.TasmotaApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.TpLinkSmartPlugApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.TradfriApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.TuyaApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.UsbRelayControlApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.WS281xApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.WemoSwitchApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.power.WledApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.remote.NgrokApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.remote.OctoEverywhereApi
import de.crysxd.octoapp.engine.octoprint.api.plugins.remote.SpaghettiDetectiveApi
import de.crysxd.octoapp.engine.octoprint.event.OctoEventSource
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.http.Url

class OctoPrintEngine internal constructor(
    private val baseUrlRotator: ActiveBaseUrlRotator,
    private val httpClient: HttpClient,
    interpolateEvents: Boolean,
    allowWebSocketTransport: Boolean,
) : PrinterEngine {

    // Events
    private val tag = "OctoPrintEngine"
    override val baseUrl = baseUrlRotator.activeUrl
    private val octoPrintEventSource = OctoEventSource(
        httpClient = httpClient,
        baseUrlRotator = baseUrlRotator,
        interpolateEvents = interpolateEvents,
        loginApi = { loginApi },
        allowWebSocketTransport = allowWebSocketTransport,
        probe = { probe(baseUrlRotator) }
    )
    override val eventSource: EventSource = octoPrintEventSource

    init {
        Napier.i(tag = tag, message = "New engine for ${baseUrl.value} (this=$this)")
        baseUrlRotator.consumeActiveFlow(
            active = octoPrintEventSource.active,
            probe = ::probe,
            onChange = { octoPrintEventSource.reconnect() }
        )
    }

    override fun destroy() {
        Napier.i(tag = tag, message = "Destroying engine (this=$this)")
        httpClient.close()
    }

    private suspend fun probe(baseUrlRotator: BaseUrlRotator): Boolean = OctoProbeApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator).probe()

    // Common APIs
    override val userApi: UserApi = OctoUserApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    override val versionApi: VersionApi = OctoVersionApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    override val filesApi: FilesApi = OctoFilesApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator, eventSink = octoPrintEventSource)
    override val jobApi: JobApi = OctoJobApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator, eventSink = octoPrintEventSource)
    override val loginApi: LoginApi = OctoLoginApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    override val connectionApi: ConnectionApi = OctoConnectionApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    override val printerProfileApi: PrinterProfileApi = OctoPrinterProfileApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    override val settingsApi: SettingsApi = OctoSettingsApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    override val systemApi: SystemApi = OctoSystemApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    override val timelapseApi: TimelapseApi = OctoTimelapseApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    override val printerApi: PrinterApi = OctoPrinterApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator, eventSink = octoPrintEventSource)
    override val powerDevicesApi: PowerDevicesApi = OctoPowerDevicesApi(
        delegates = listOf(
            PsuControlApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            EnclosureApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            GpioControlApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            MyStromApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            OctoCamApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            OctoHueApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            OctoLightApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            OctoRelayApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            OphomApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            TasmotaApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            TpLinkSmartPlugApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            TradfriApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            TuyaApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            UsbRelayControlApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            WemoSwitchApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            WledApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            WS281xApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
        )
    )
    override val materialsApi: MaterialsApi = OctoMaterialApi(
        delegates = listOf(
            SpoolManagerApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator),
            FilamentManagerApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
        )
    )

    // OctoPrint specific / Plugins
    val octoAppCompanionApi = OctoAppCompanionApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    val cancelObjectApi = CancelObjectApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    val applicationKeysApi = ApplicationKeysApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    val mmu2FilamentSelectApi = Mmu2FilamentSelectApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    val ngrokApi = NgrokApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    val octoEverywhereApi = OctoEverywhereApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    val pluginManagerApi = PluginManagerApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    val spaghettiDetectiveApi = SpaghettiDetectiveApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    val octolapseApi = OctolapseApi(httpClient = httpClient, baseUrlRotator = baseUrlRotator)

    override suspend fun notifyConnectionChange() = baseUrlRotator.considerUpgradingConnection()

    override suspend fun <T> genericRequest(block: suspend (Url, HttpClient) -> T): T = baseUrlRotator.request {
        block(it, httpClient)
    }
}