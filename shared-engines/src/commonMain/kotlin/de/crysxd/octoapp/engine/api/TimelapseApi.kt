package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.timelapse.TimelapseConfig
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import de.crysxd.octoapp.engine.models.timelapse.TimelapseStatus

interface TimelapseApi {

    suspend fun updateConfig(config: TimelapseConfig): TimelapseStatus
    suspend fun getStatus(): TimelapseStatus
    suspend fun delete(timelapseFile: TimelapseFile): TimelapseStatus
    suspend fun deleteUnrendered(timelapseFile: TimelapseFile): TimelapseStatus

}
