package de.crysxd.octoapp.engine.octoprint.dto.plugins.tasmota

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
internal data class TasmotaResponse(
    val currentState: State = State.UNKNOWN
) {

    @Serializable
    enum class State {
        @SerialName("on")
        ON,

        @SerialName("off")
        OFF,

        @SerialName("unknown")
        UNKNOWN
    }
}