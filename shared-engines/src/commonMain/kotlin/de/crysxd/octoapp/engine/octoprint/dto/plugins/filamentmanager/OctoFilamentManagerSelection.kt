package de.crysxd.octoapp.engine.octoprint.dto.plugins.filamentmanager

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoFilamentManagerSelection(
    val selections: List<Selection> = emptyList()
) {
    @Serializable
    data class Selection(
        val tool: Int = 0,
        val spool: OctoFilamentManagerSpool,
    )
}