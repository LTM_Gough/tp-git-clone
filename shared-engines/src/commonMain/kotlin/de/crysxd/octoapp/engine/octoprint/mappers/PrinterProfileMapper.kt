package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.engine.octoprint.dto.printer.OctoPrinterProfile

internal fun OctoPrinterProfile.map() = PrinterProfile(
    id = id,
    current = current,
    axes = PrinterProfile.Axes(
        e = axes.e.map(),
        x = axes.x.map(),
        y = axes.y.map(),
        z = axes.z.map(),
    ),
    name = name,
    model = model,
    default = default,
    extruder = extruder.let {
        PrinterProfile.Extruder(
            nozzleDiameter = it.nozzleDiameter,
            sharedNozzle = it.sharedNozzle,
            count = it.count,
            defaultExtrusionLength = it.defaultExtrusionLength,
            offsets = it.offsets.map { o ->
                require(o.size == 2) { "Expected 2 entries, got ${o.size}" }
                o[0] to o[1]
            }
        )
    },
    heatedBed = heatedBed,
    heatedChamber = heatedChamber,
    volume = volume.let {
        PrinterProfile.Volume(
            depth = it.depth,
            height = it.height,
            width = it.width,
            origin = when (it.origin) {
                OctoPrinterProfile.Origin.LowerLeft -> PrinterProfile.Origin.LowerLeft
                OctoPrinterProfile.Origin.Center -> PrinterProfile.Origin.Center
            },
            formFactor = when (it.formFactor) {
                OctoPrinterProfile.FormFactor.Circular -> PrinterProfile.FormFactor.Circular
                OctoPrinterProfile.FormFactor.Rectangular -> PrinterProfile.FormFactor.Rectangular
            }
        )
    }
)

private fun OctoPrinterProfile.Axis.map() = PrinterProfile.Axis(inverted = inverted, speed = speed)
