package de.crysxd.octoapp.engine.octoprint.dto.plugins.ngrok

import kotlinx.serialization.Serializable

@Serializable
data class NgrokConfig(
    val tunnel: String?,
)