package de.crysxd.octoapp.engine.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

class DownloadTooLargeException(webUrl: Url) : NetworkException(
    userFacingMessage = "The server does not allow downloading this file because it is too large.",
    technicalMessage = "Received response code 413, indicating content is too large",
    webUrl = webUrl,
)