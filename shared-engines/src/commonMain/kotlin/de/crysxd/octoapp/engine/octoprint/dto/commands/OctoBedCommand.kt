@file:Suppress("unused")

package de.crysxd.octoapp.engine.octoprint.dto.commands

import kotlinx.serialization.Serializable

@Serializable
sealed class OctoBedCommand {

    @Serializable
    data class SetTargetTemperature(val target: Float) : OctoBedCommand() {
        val command = "target"
    }

    @Serializable
    data class SetTemperatureOffset(val offset: Float) : OctoBedCommand() {
        val command = "offset"
    }

}

