package de.crysxd.octoapp.engine.octoprint.serializer

import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.InternalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PolymorphicKind
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonObject

class OctoEventMessageListSerializer : KSerializer<List<OctoMessage>> {

    @OptIn(InternalSerializationApi::class, ExperimentalSerializationApi::class)
    override val descriptor: SerialDescriptor = buildSerialDescriptor("OctoMessage", PolymorphicKind.OPEN)
    private val nested = OctoEventMessageSerializer()

    override fun deserialize(decoder: Decoder): List<OctoMessage> {
        val input = decoder as JsonDecoder
        val tree = input.decodeJsonElement().jsonArray
        return tree.map {
            nested.deserialize(it.jsonObject)
        }
    }

    override fun serialize(encoder: Encoder, value: List<OctoMessage>) {
        throw UnsupportedOperationException("Not implemented")
    }
}