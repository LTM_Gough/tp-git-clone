package de.crysxd.octoapp.engine.octoprint.dto.plugins.psucontrol

import kotlinx.serialization.Serializable

@Serializable
internal data class PsuControlState(val isPSUOn: String = "false")