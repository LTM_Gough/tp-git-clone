package de.crysxd.octoapp.engine.octoprint.http

import de.crysxd.octoapp.engine.exceptions.PrinterHttpsException
import de.crysxd.octoapp.engine.exceptions.octoeverywhere.OctoEverywhereConnectionNotFoundException
import de.crysxd.octoapp.engine.framework.InactiveBaseUrlRotator
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder
import de.crysxd.octoapp.sharedcommon.exceptions.BasicAuthRequiredException
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.KeyStoreProvider
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.request.get
import io.ktor.http.HttpHeaders.WWWAuthenticate
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import org.junit.Test
import java.io.ByteArrayInputStream
import java.security.KeyStore
import java.security.cert.CertificateFactory
import java.util.Base64
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class OctoHttpClientFactory {

    private val testCertBas64 = "MIIDeTCCAmGgAwIBAgIJAJWlQWpveElqMA0GCSqGSIb3DQEBCwUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQIDApDYWxpZm9ybmlhMRYwFAYDVQQHDA1TYW4gRnJhbmNpc" +
            "2NvMQ8wDQYDVQQKDAZCYWRTU0wxFTATBgNVBAMMDCouYmFkc3NsLmNvbTAeFw0yMjEwMjcxOTA5MTBaFw0yNDEwMjYxOTA5MTBaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQIDApDYWxpZm9ybmlhMR" +
            "YwFAYDVQQHDA1TYW4gRnJhbmNpc2NvMQ8wDQYDVQQKDAZCYWRTU0wxFTATBgNVBAMMDCouYmFkc3NsLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMIE7PiM7gTCs9hQ1XBYzJM" +
            "Y61yoaEmwIrX5lZ6xKyx2PmzAS2BMTOqytMAPgLaw+XLJhgL5XEFdEyt/ccRLvOmULlA3pmccYYz2QULFRtMWhyefdOsKnRFSJiFzbIRMeVXk0WvoBj1IFVKtsyjbqv9u/2CVSndrOfEk0TG23U3A" +
            "xPxTuW1CrbV8/q71FdIzSOciccfCFHpsKOo3St/qbLVytH5aohbcabFXRNsKEqveww9HdFxBIuGa+RuT5q0iBikusbpJHAwnnqP7i/dAcgCskgjZjFeEU4EFy+b+a1SYQCeFxxC7c3DvaRhBB0VVf" +
            "PlkPz0sw6l865MaTIbRyoUCAwEAAaMyMDAwCQYDVR0TBAIwADAjBgNVHREEHDAaggwqLmJhZHNzbC5jb22CCmJhZHNzbC5jb20wDQYJKoZIhvcNAQELBQADggEBAHOCRfreG+y9q2jxDTYOYESH+S" +
            "IOPh1a4TDKfWMGQ5shdW7QqlsRHqXmENFAalhHlNqGms1n77IdkIocygP9ZNZocyGwyLCDNn7YGdxCG4AlW2LoNW7Bs86GUhUrWtahHpOnmrjQ/zzDH1T9pBxUQwA72x3SdgvfFu+WuAU2Dg/X6El" +
            "yIQ3a4/xy5NzlWsXBRvt67+uI2Fg3PGMenpWLD6SSLQzjjtEDXwv0dfBsYIt+m3QZgOHdx459vr8bpqjBz09izM2W/uYHcjixXQVpKujN9AA6a7c5hiGpfXPdhiUmaHupCkR+c/8O1yyxkBMbttbp" +
            "7VdUIXYAmB8yJuGoqe8="

    @Test
    fun WHEN_basic_auth_is_required_THEN_correct_exception_is_thrown() = runBlocking {
        //region GIVEN
        val message = "Hello World!"
        val url = Url("https://self-signed.badssl.com")
        val http = createOctoPrintHttpClient(
            settings = OctoPrintEngineBuilder.OctoHttpClientSettings(apiKey = "", baseUrlRotator = InactiveBaseUrlRotator(url)),
            engine = MockEngine {
                respond(content = "", status = HttpStatusCode.Unauthorized, headersOf(WWWAuthenticate, """spamrealm="$message"spam"""))
            }
        )
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: NetworkException) {
            e
        }
        //endregion
        //region THEN
        assertTrue(
            actual = exception is BasicAuthRequiredException,
            message = "Expected PrinterHttpsException"
        )
        assertEquals(
            expected = message,
            actual = exception.userRealm,
            message = "Expected user realm to match"
        )
        //endregion
    }

    @Test
    fun WHEN_401_from_octoeverywhere_THEN_correct_exception_is_thrown() = runBlocking {
        // Double check the correct exception here because the order of plugins matter. The basicauth plugin might handle this 401 differently if it's lower in the stack
        //region GIVEN
        val url = Url("https://shared-something.octoeverywhere.com")
        val http = createOctoPrintHttpClient(
            settings = OctoPrintEngineBuilder.OctoHttpClientSettings(apiKey = "", baseUrlRotator = InactiveBaseUrlRotator(url)),
            engine = MockEngine {
                respond(content = "", status = HttpStatusCode.Unauthorized)
            }
        )
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: NetworkException) {
            e
        }
        //endregion
        //region THEN
        assertTrue(
            actual = exception is OctoEverywhereConnectionNotFoundException,
            message = "Expected OctoEverywhereConnectionNotFoundException got ${exception?.let { it::class.simpleName }}"
        )
        //endregion
    }


    @Test
    fun WHEN_self_signed_certificate_THEN_printer_https_exception_is_thrown() = runBlocking {
        //region GIVEN
        val url = Url("https://self-signed.badssl.com")
        val http = createOctoPrintHttpClient(settings = OctoPrintEngineBuilder.OctoHttpClientSettings(apiKey = "", baseUrlRotator = InactiveBaseUrlRotator(url)))
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: NetworkException) {
            e
        }
        //endregion
        //region THEN
        assertTrue(
            actual = exception is PrinterHttpsException,
            message = "Expected PrinterHttpsException"
        )
        assertFalse(
            actual = exception.weakHostnameVerificationRequired,
            message = "Expected weak hostname verification to not be required"
        )
        assertNotNull(
            actual = exception.certificate,
            message = "Expected certificate to be given"
        )
        assertEquals(
            expected = testCertBas64,
            actual = Base64.getEncoder().encodeToString(exception.certificate!!.encoded),
            message = "Expected public key to match"
        )
        //endregion
    }

    @Test
    fun WHEN_self_signed_certificate_but_trusted_THEN_printer_https_exception_is_thrown() = runBlocking {
        //region GIVEN
        val url = Url("https://self-signed.badssl.com")
        val http = createOctoPrintHttpClient(
            settings = OctoPrintEngineBuilder.OctoHttpClientSettings(
                apiKey = "",
                baseUrlRotator = InactiveBaseUrlRotator(url),
                general = HttpClientSettings(
                    keyStore = object : KeyStoreProvider {
                        override fun getWeakVerificationForHosts() = emptyList<String>()
                        override fun loadKeyStore() = KeyStore.getInstance(KeyStore.getDefaultType()).apply {
                            load(null)
                            val bytes = Base64.getDecoder().decode(testCertBas64)
                            val cert = CertificateFactory.getInstance("X.509").generateCertificate(ByteArrayInputStream(bytes))
                            setCertificateEntry("my_cert", cert)
                        }

                    },
                )
            )
        )
        //endregion
        //region WHEN
        val res = http.get(url)
        //endregion
        //region THEN
        assertEquals(
            expected = 200,
            actual = res.status.value,
            message = "Expected status to match"
        )
        //endregion
    }
}