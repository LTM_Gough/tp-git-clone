package de.crysxd.octoapp.engine.octoprint.serializer

import de.crysxd.octoapp.engine.octoprint.dto.printer.OctoComponentTemperature
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class OctoSafeFloatSerializerTest {

    @Test
    fun WHEN_safe_float_serializer_is_used_THEN_it_never_fails() {
        assertEquals(
            expected = OctoComponentTemperature(actual = null, target = 13.37f),
            actual = Json.decodeFromString("{\"actual\":\"\",\"target\":13.37}")
        )
        assertEquals(
            expected = OctoComponentTemperature(actual = 42.12f, target = 13.37f),
            actual = Json.decodeFromString("{\"actual\":\"42.12\",\"target\":13.37}")
        )
        assertEquals(
            expected = OctoComponentTemperature(actual = 42f, target = 13.37f),
            actual = Json.decodeFromString("{\"actual\":\"42\",\"target\":13.37}")
        )
        assertEquals(
            expected = OctoComponentTemperature(actual = 42f, target = 13.37f),
            actual = Json.decodeFromString("{\"actual\":42,\"target\":13.37}")
        )
        assertEquals(
            expected = OctoComponentTemperature(actual = null, target = 13.37f),
            actual = Json.decodeFromString("{\"actual\":\"notafloat\",\"target\":13.37}")
        )
        assertEquals(
            expected = OctoComponentTemperature(actual = 42.24f, target = 13.37f),
            actual = Json.decodeFromString("{\"actual\":42.24,\"target\":13.37}")
        )
    }
}