package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.models.login.LoginBody
import de.crysxd.octoapp.engine.models.login.LoginResponse
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.OctoHttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class OctoLoginApiTest {

    @Test
    fun WHEN_a_login_request_is_made_with_invalid_key_on_OctoPrint_1_8_2_and_older_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        {
                            "_is_external_client": false,
                            "needs": {
                                "group": [
                                    "guests"
                                ]
                            }
                        }
                    """.trimIndent(),
                )
            }
        )
        val target = OctoLoginApi(http, rotator)
        //endregion
        //region WHEN
        val login = target.passiveLogin(LoginBody())
        //endregion
        //region THEN
        assertEquals(
            expected = LoginResponse(),
            actual = login,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_a_login_request_is_made_with_invalid_key_on_OctoPrint_1_8_3_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                respond(
                    status = HttpStatusCode.BadRequest,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        { "error": "CSRF validation failed" }
                    """.trimIndent(),
                )
            }
        )
        val target = OctoLoginApi(http, rotator)
        //endregion
        //region WHEN
        val login = target.passiveLogin(LoginBody())
        //endregion
        //region THEN
        assertEquals(
            expected = LoginResponse(groups = listOf()),
            actual = login,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_a_login_request_is_made_with_invalid_key_on_OctoPrint_1_8_4_and_newer_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                respond(
                    status = HttpStatusCode.Forbidden,
                    content = ""
                )
            }
        )
        val target = OctoLoginApi(http, rotator)
        //endregion
        //region WHEN
        val login = target.passiveLogin(LoginBody())
        //endregion
        //region THEN
        assertEquals(
            expected = LoginResponse(groups = listOf()),
            actual = login,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_a_login_request_is_made_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        {
                            "_is_external_client": false,
                            "_login_mechanism": "apikey",
                            "active": true,
                            "admin": true,
                            "apikey": null,
                            "groups": [
                                "users",
                                "admins"
                            ],
                            "name": "admin",
                            "needs": {
                                "group": [
                                    "admins",
                                    "users"
                                ],
                                "role": [
                                    "settings",
                                    "control",
                                    "files_delete",
                                    "files_upload",
                                    "system",
                                    "plugin_pi_support_check",
                                    "status",
                                    "plugin_announcements_manage",
                                    "timelapse_download",
                                    "plugin_backup_access",
                                    "plugin_firmware_check_display",
                                    "timelapse_delete",
                                    "plugin_softwareupdate_check",
                                    "plugin_pluginmanager_manage",
                                    "plugin_appkeys_user",
                                    "plugin_logging_manage",
                                    "slice",
                                    "timelapse_admin",
                                    "timelapse_list",
                                    "webcam",
                                    "gcodeviewer",
                                    "monitor_terminal",
                                    "plugin_action_command_notification_clear",
                                    "plugin_action_command_notification_show",
                                    "plugin_pluginmanager_install",
                                    "plugin_octoapp_admin",
                                    "settings_read",
                                    "files_download",
                                    "print",
                                    "files_list",
                                    "plugin_appkeys_admin",
                                    "plugin_announcements_read",
                                    "files_select",
                                    "plugin_softwareupdate_configure",
                                    "connection",
                                    "plugin_softwareupdate_update",
                                    "admin",
                                    "plugin_action_command_prompt_interact"
                                ]
                            },
                            "permissions": [],
                            "roles": [
                                "user",
                                "admin"
                            ],
                            "session": "C13215DA6F7A4502AFD37D99DCFC45F2",
                            "settings": {},
                            "user": true
                        }
                    """.trimIndent()
                )
            }
        )
        val target = OctoLoginApi(http, rotator)
        //endregion
        //region WHEN
        val login = target.passiveLogin(LoginBody())
        //endregion
        //region THEN
        assertEquals(
            expected = LoginResponse(groups = listOf("users", "admins"), isAdmin = true, name = "admin", session = "C13215DA6F7A4502AFD37D99DCFC45F2"),
            actual = login,
            message = "Expected response to match"
        )
        //endregion
    }
}