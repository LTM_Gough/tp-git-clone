package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.mocks.TestApiBuilder
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.mocks.assertPatch
import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoSettings
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.respondOk
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class EnclosureApiTest {

    @Test
    fun WHEN_devices_are_listed_THEN_devices_are_returned() = runBlocking {
        //region GIVEN
        val settings = OctoSettings(
            plugins = OctoSettings.PluginSettingsGroup(
                enclosure = OctoSettings.Enclosure(
                    outputs = listOf(
                        OctoSettings.Enclosure.Output(
                            type = "pwm",
                            label = "PWM",
                            indexId = 1,
                        ),
                        OctoSettings.Enclosure.Output(
                            type = "something",
                            label = "Something",
                            indexId = 2,
                        ),
                        OctoSettings.Enclosure.Output(
                            type = "regular",
                            label = "GPIO",
                            indexId = 3,
                        )
                    )
                )
            )
        )
        val target = TestApiBuilder(
            MockEngine {
                throw IllegalStateException("No request expected")
            }
        ) { rotator, httpClient ->
            EnclosureApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        //endregion
        //region WHEN
        val devices = target.getDevices(settings.map())
        //endregion
        //region THEN
        assertEquals(
            expected = 1,
            actual = devices.size,
            message = "Expected one device"
        )
        assertEquals(
            expected = EnclosureApi.PowerDevice(owner = target, indexId = 3, displayName = "GPIO"),
            actual = devices[0],
            message = "Expected device to match"
        )
        //endregion
    }

    @Test
    fun WHEN_device_is_turned_on_THEN_request_is_made() = testSet(
        request = "{\"status\":true}",
        action = { turnOn() }
    )

    @Test
    fun WHEN_device_is_turned_off_THEN_request_is_made() = testSet(
        request = "{\"status\":false}",
        action = { turnOff() }
    )

    @Test
    fun WHEN_device_status_is_checked_and_off_THEN_request_is_made() = testGet(
        response = "{\"current_value\":false}",
        expectedOn = false
    )

    @Test
    fun WHEN_device_status_is_checked_and_on_THEN_request_is_made() = testGet(
        response = "{\"current_value\":true}",
        expectedOn = true
    )

    @Test
    fun WHEN_device_status_is_checked_and_broken_THEN_request_is_made() = testGet(
        response = "{}",
        expectedOn = false
    )

    private fun testGet(response: String, expectedOn: Boolean) = runBlocking {
        //region GIVEN
        val target = TestApiBuilder(
            MockEngine {
                it.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/plugin/enclosure/outputs/3"),
                    actual = it.url,
                    message = "Expected URL to match"
                )
                respond(
                    content = response,
                    headers = headersOf("Content-Type" to listOf("application/json")),
                    status = HttpStatusCode.OK
                )
            }
        ) { rotator, httpClient ->
            EnclosureApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        val device = EnclosureApi.PowerDevice(
            owner = target,
            indexId = 3,
            displayName = "Label"
        )
        //endregion
        //region WHEN
        val isOn = device.isOn()
        //endregion
        //region THEN
        assertEquals(
            expected = expectedOn,
            actual = isOn,
            message = "Expected device to be on: $expectedOn"
        )
        //endregion
    }

    private fun testSet(request: String, action: suspend EnclosureApi.PowerDevice.() -> Unit) = runBlocking {
        //region GIVEN
        var requestMade = false
        val target = TestApiBuilder(
            MockEngine {
                it.assertPatch()
                assertEquals(
                    expected = Url("http://gstatic.com/plugin/enclosure/outputs/3"),
                    actual = it.url,
                    message = "Expected URL to match"
                )
                assertEquals(
                    expected = request,
                    actual = it.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                requestMade = true
                respondOk()
            }
        ) { rotator, httpClient ->
            EnclosureApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        val device = EnclosureApi.PowerDevice(
            owner = target,
            indexId = 3,
            displayName = "Label"
        )
        //endregion
        //region WHEN
        device.action()
        //endregion
        //region THEN
        assertTrue(
            actual = requestMade,
            message = "Expected request to be made"
        )
        //endregion
    }
}