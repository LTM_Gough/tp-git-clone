package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.mocks.TestApiBuilder
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.mocks.assertPost
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoSettings
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.respondOk
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class W281xApiTest {

    @Test
    fun WHEN_devices_are_listed_THEN_devices_are_returned() = runBlocking {
        //region GIVEN
        val settings = OctoSettings(
            plugins = OctoSettings.PluginSettingsGroup(
                wS281x = OctoSettings.WS281x()
            )
        )
        val target = TestApiBuilder(
            MockEngine {
                throw IllegalStateException("No request expected")
            }
        ) { rotator, httpClient ->
            WS281xApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        //endregion
        //region WHEN
        val devices = target.getDevices(settings.map())
        //endregion
        //region THEN
        assertEquals(
            expected = 2,
            actual = devices.size,
            message = "Expected one device"
        )
        assertEquals(
            expected = WS281xApi.TorchDevice(owner = target),
            actual = devices[0],
            message = "Expected device to match"
        )
        assertEquals(
            expected = WS281xApi.LightsDevice(owner = target),
            actual = devices[1],
            message = "Expected device to match"
        )
        //endregion
    }

    @Test
    fun WHEN_torch_is_turned_on_THEN_request_is_made() = testSet(
        request = "{\"command\":\"torch_on\"}",
        action = { turnOn() },
        device = { WS281xApi.TorchDevice(it) }
    )

    @Test
    fun WHEN_torch_is_turned_off_THEN_request_is_made() = testSet(
        request = "{\"command\":\"torch_off\"}",
        action = { turnOff() },
        device = { WS281xApi.TorchDevice(it) }
    )

    @Test
    fun WHEN_lights_is_turned_on_THEN_request_is_made() = testSet(
        request = "{\"command\":\"lights_on\"}",
        action = { turnOn() },
        device = { WS281xApi.LightsDevice(it) }
    )

    @Test
    fun WHEN_lights_is_turned_off_THEN_request_is_made() = testSet(
        request = "{\"command\":\"lights_off\"}",
        action = { turnOff() },
        device = { WS281xApi.LightsDevice(it) }
    )

    @Test
    fun WHEN_light_status_is_checked_and_off_THEN_request_is_made() = testGet(
        response = "{\"lights_on\":false}",
        expectedOn = false,
        device = { WS281xApi.LightsDevice(it) }
    )

    @Test
    fun WHEN_light_status_is_checked_and_on_THEN_request_is_made() = testGet(
        response = "{\"lights_on\":true}",
        expectedOn = true,
        device = { WS281xApi.LightsDevice(it) }
    )

    @Test
    fun WHEN_light_status_is_checked_and_broken_THEN_request_is_made() = testGet(
        response = "{}",
        expectedOn = false,
        device = { WS281xApi.LightsDevice(it) }
    )

    @Test
    fun WHEN_torch_status_is_checked_and_off_THEN_request_is_made() = testGet(
        response = "{\"torch_on\":false}",
        expectedOn = false,
        device = { WS281xApi.TorchDevice(it) }
    )

    @Test
    fun WHEN_torch_status_is_checked_and_on_THEN_request_is_made() = testGet(
        response = "{\"torch_on\":true}",
        expectedOn = true,
        device = { WS281xApi.TorchDevice(it) }
    )

    @Test
    fun WHEN_torch_status_is_checked_and_broken_THEN_request_is_made() = testGet(
        response = "{}",
        expectedOn = false,
        device = { WS281xApi.TorchDevice(it) }
    )

    private fun testGet(response: String, device: (WS281xApi) -> WS281xApi.Device, expectedOn: Boolean) = runBlocking {
        //region GIVEN
        val target = TestApiBuilder(
            MockEngine {
                it.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/ws281x_led_status"),
                    actual = it.url,
                    message = "Expected URL to match"
                )
                respond(
                    content = response,
                    headers = headersOf("Content-Type" to listOf("application/json")),
                    status = HttpStatusCode.OK
                )
            }
        ) { rotator, httpClient ->
            WS281xApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        //endregion
        //region WHEN
        val isOn = device(target).isOn()
        //endregion
        //region THEN
        assertEquals(
            expected = expectedOn,
            actual = isOn,
            message = "Expected device to be on: $expectedOn"
        )
        //endregion
    }

    private fun testSet(request: String, device: (WS281xApi) -> WS281xApi.Device, action: suspend PowerDevice.() -> Unit) = runBlocking {
        //region GIVEN
        var requestMade = false
        val target = TestApiBuilder(
            MockEngine {
                it.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/ws281x_led_status"),
                    actual = it.url,
                    message = "Expected URL to match"
                )
                assertEquals(
                    expected = request,
                    actual = it.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                requestMade = true
                respondOk()
            }
        ) { rotator, httpClient ->
            WS281xApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        //endregion
        //region WHEN
        device(target).action()
        //endregion
        //region THEN
        assertTrue(
            actual = requestMade,
            message = "Expected request to be made"
        )
        //endregion
    }
}