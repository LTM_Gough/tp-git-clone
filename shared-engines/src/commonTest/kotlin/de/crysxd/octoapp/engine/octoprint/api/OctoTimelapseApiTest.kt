package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.assertDelete
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.mocks.assertPost
import de.crysxd.octoapp.engine.models.timelapse.TimelapseConfig
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import de.crysxd.octoapp.engine.models.timelapse.TimelapseStatus
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.OctoHttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class OctoTimelapseApiTest {

    @Test
    fun WHEN_timelapse_status_is_get_and_status_is_off_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/timelapse?unrendered=true"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    //region content = ...
                    content = """
                        {
                            "config": {
                                "type": "off"
                            },
                            "enabled": false,
                            "files": [
                                {
                                    "bytes": 211072,
                                    "date": "2022-03-19 04:02",
                                    "name": "Calibration_Cube_Single_Filament_MMU-2_20220319040218.mp4",
                                    "size": "206.1KB",
                                    "timestamp": 1647662543.5880575,
                                    "url": "/downloads/timelapse/Calibration_Cube_Single_Filament_MMU-2_20220319040218.mp4"
                                },
                                {
                                    "bytes": 211308,
                                    "date": "2022-03-19 04:25",
                                    "name": "Calibration_Cube_Single_Filament_MMU-2_20220319042529.mp4",
                                    "size": "206.4KB",
                                    "timestamp": 1647663934.0517764,
                                    "url": "/downloads/timelapse/Calibration_Cube_Single_Filament_MMU-2_20220319042529.mp4"
                                }
                            ],
                            "unrendered": [
                                {
                                    "bytes": 210392,
                                    "date": "2022-03-19 04:48",
                                    "name": "Calibration_Cube_Single_Filament_MMU-2_20220319044758.mp4",
                                    "size": "205.5KB",
                                    "timestamp": 1647665283.443445,
                                    "url": "/downloads/timelapse/Calibration_Cube_Single_Filament_MMU-2_20220319044758.mp4"
                                }
                            ]
                        }
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoTimelapseApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.getStatus()
        //endregion
        //region THEN
        assertEquals(
            expected = TimelapseStatus(
                config = TimelapseConfig(type = TimelapseConfig.Type.Off),
                enabled = false,
                files = listOf(
                    TimelapseFile(
                        bytes = 211072,
                        date = 1647662543588L,
                        name = "Calibration_Cube_Single_Filament_MMU-2_20220319040218.mp4",
                        downloadPath = "/downloads/timelapse/Calibration_Cube_Single_Filament_MMU-2_20220319040218.mp4",
                    ),
                    TimelapseFile(
                        bytes = 211308,
                        date = 1647663934052L,
                        name = "Calibration_Cube_Single_Filament_MMU-2_20220319042529.mp4",
                        downloadPath = "/downloads/timelapse/Calibration_Cube_Single_Filament_MMU-2_20220319042529.mp4"
                    )
                ),
                unrendered = listOf(
                    TimelapseFile(
                        bytes = 210392,
                        date = 1647665283443L,
                        name = "Calibration_Cube_Single_Filament_MMU-2_20220319044758.mp4",
                        downloadPath = "/downloads/timelapse/Calibration_Cube_Single_Filament_MMU-2_20220319044758.mp4"
                    )
                )
            ),
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_timelapse_status_is_get_and_status_is_timed_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/timelapse?unrendered=true"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    //region content = ...
                    content = """
                     {
                        "config": {
                            "fps": 25,
                            "interval": 10,
                            "postRoll": 0,
                            "type": "timed"
                        },
                        "enabled": true
                    }
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoTimelapseApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.getStatus()
        //endregion
        //region THEN
        assertEquals(
            expected = TimelapseStatus(
                config = TimelapseConfig(
                    type = TimelapseConfig.Type.Timed,
                    fps = 25,
                    interval = 10,
                    postRoll = 0,
                ),
                enabled = false,
            ),
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_timelapse_status_is_get_and_status_is_zchange_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/timelapse?unrendered=true"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    //region content = ...
                    content = """
                     {
                           "config": {
                                "fps": 25,
                                "minDelay": 5.0,
                                "postRoll": 0,
                                "retractionZHop": 0.0,
                                "type": "zchange"
                            },
                        "enabled": true
                    }
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoTimelapseApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.getStatus()
        //endregion
        //region THEN
        assertEquals(
            expected = TimelapseStatus(
                config = TimelapseConfig(
                    type = TimelapseConfig.Type.ZChange,
                    fps = 25,
                    postRoll = 0,
                    minDelay = 5f,
                    retractionZHop = 0f,
                ),
                enabled = false,
            ),
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_timelapse_config_is_updated_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/timelapse?unrendered=true"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                assertEquals(
                    expected = "{\"type\":\"timed\",\"fps\":25,\"save\":false,\"minDelay\":5.5,\"interval\":10}",
                    actual = request.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    //region content = ...
                    content = """
                     {
                           "config": {
                                "fps": 25,
                                "minDelay": 5.0,
                                "postRoll": 0,
                                "type": "zchange"
                            },
                        "enabled": true
                    }
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoTimelapseApi(rotator, http)
        //endregion
        //region WHEN
        val login = target.updateConfig(
            TimelapseConfig(
                type = TimelapseConfig.Type.Timed,
                interval = 10,
                fps = 25,
                minDelay = 5.5f
            )
        )
        //endregion
        //region THEN
        assertEquals(
            expected = TimelapseStatus(
                config = TimelapseConfig(
                    type = TimelapseConfig.Type.ZChange,
                    fps = 25,
                    postRoll = 0,
                    minDelay = 5f,
                ),
                enabled = false,
            ),
            actual = login,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_timelapse_file_is_deleted_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertDelete()
                assertEquals(
                    expected = Url("http://gstatic.com/api/timelapse/%F0%9F%A5%B9%F0%9F%98%AD%F0%9F%98%87%F0%9F%93%B8.mp4?unrendered=true"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    //region content = ...
                    content = """
                     {
                           "config": {
                                "fps": 25,
                                "minDelay": 5.0,
                                "postRoll": 0,
                                "retractionZHop": 0.0,
                                "type": "zchange"
                            },
                        "enabled": true
                    }
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoTimelapseApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.delete(
            TimelapseFile(
                name = "🥹😭😇📸.mp4",
                bytes = 1,
            )
        )
        //endregion
        //region THEN
        assertEquals(
            expected = TimelapseStatus(
                config = TimelapseConfig(
                    type = TimelapseConfig.Type.ZChange,
                    fps = 25,
                    postRoll = 0,
                    minDelay = 5f,
                    retractionZHop = 0f,
                ),
                enabled = false,
            ),
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_unrendered_timelapse_file_is_deleted_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertDelete()
                assertEquals(
                    expected = Url("http://gstatic.com/api/timelapse/unrendered/%F0%9F%A5%B9%F0%9F%98%AD%F0%9F%98%87%F0%9F%93%B8.mp4?unrendered=true"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    //region content = ...
                    content = """
                     {
                           "config": {
                                "fps": 25,
                                "minDelay": 5.0,
                                "postRoll": 0,
                                "retractionZHop": 0.0,
                                "type": "zchange"
                            },
                        "enabled": true
                    }
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoTimelapseApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.deleteUnrendered(
            TimelapseFile(
                name = "🥹😭😇📸.mp4",
                bytes = 1,
            )
        )
        //endregion
        //region THEN
        assertEquals(
            expected = TimelapseStatus(
                config = TimelapseConfig(
                    type = TimelapseConfig.Type.ZChange,
                    fps = 25,
                    postRoll = 0,
                    minDelay = 5f,
                    retractionZHop = 0f,
                ),
                enabled = false,
            ),
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }
}