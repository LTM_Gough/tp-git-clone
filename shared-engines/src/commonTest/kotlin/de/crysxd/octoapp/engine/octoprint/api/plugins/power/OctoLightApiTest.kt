package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.mocks.TestApiBuilder
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoSettings
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.respondOk
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class OctoLightApiTest {

    @Test
    fun WHEN_devices_are_listed_THEN_devices_are_returned() = runBlocking {
        //region GIVEN
        val settings = OctoSettings(
            plugins = OctoSettings.PluginSettingsGroup(
                octoLight = OctoSettings.OctoLight()
            )
        )
        val target = TestApiBuilder(
            MockEngine {
                throw IllegalStateException("No request expected")
            }
        ) { rotator, httpClient ->
            OctoLightApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        //endregion
        //region WHEN
        val devices = target.getDevices(settings.map())
        //endregion
        //region THEN
        assertEquals(
            expected = 1,
            actual = devices.size,
            message = "Expected one device"
        )
        assertEquals(
            expected = OctoLightApi.PowerDevice(owner = target),
            actual = devices[0],
            message = "Expected device to match"
        )
        //endregion
    }

    @Test
    fun WHEN_device_is_turned_on_THEN_request_is_made() = testSet(
        actionId = "turnOn",
        action = { turnOn() }
    )

    @Test
    fun WHEN_device_is_turned_off_THEN_request_is_made() = testSet(
        actionId = "turnOff",
        action = { turnOff() }
    )

    @Test
    fun WHEN_device_is_toggled_THEN_request_is_made() = testSet(
        actionId = "toggle",
        action = { toggle() }
    )

    @Test
    fun WHEN_device_status_is_checked_and_off_THEN_request_is_made() = testGet(
        response = "{\"state\":false}",
        expectedOn = false
    )

    @Test
    fun WHEN_device_status_is_checked_and_on_THEN_request_is_made() = testGet(
        response = "{\"state\":true}",
        expectedOn = true
    )

    @Test
    fun WHEN_device_status_is_checked_and_broken_THEN_request_is_made() = testGet(
        response = "{}",
        expectedOn = false
    )

    private fun testGet(response: String, expectedOn: Boolean) = runBlocking {
        //region GIVEN
        val target = TestApiBuilder(
            MockEngine {
                it.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/octolight?action=getState"),
                    actual = it.url,
                    message = "Expected URL to match"
                )
                respond(
                    content = response,
                    headers = headersOf("Content-Type" to listOf("application/json")),
                    status = HttpStatusCode.OK
                )
            }
        ) { rotator, httpClient ->
            OctoLightApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        val device = OctoLightApi.PowerDevice(
            owner = target,
        )
        //endregion
        //region WHEN
        val isOn = device.isOn()
        //endregion
        //region THEN
        assertEquals(
            expected = expectedOn,
            actual = isOn,
            message = "Expected device to be on: $expectedOn"
        )
        //endregion
    }

    private fun testSet(actionId: String, action: suspend PowerDevice.() -> Unit) = runBlocking {
        //region GIVEN
        var requestMade = false
        val target = TestApiBuilder(
            MockEngine {
                it.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/octolight?action=$actionId"),
                    actual = it.url,
                    message = "Expected URL to match"
                )
                requestMade = true
                respondOk()
            }
        ) { rotator, httpClient ->
            OctoLightApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        val device = OctoLightApi.PowerDevice(
            owner = target,
        )
        //endregion
        //region WHEN
        device.action()
        //endregion
        //region THEN
        assertTrue(
            actual = requestMade,
            message = "Expected request to be made"
        )
        //endregion
    }
}