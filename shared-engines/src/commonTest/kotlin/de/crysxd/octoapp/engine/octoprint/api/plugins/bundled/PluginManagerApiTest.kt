package de.crysxd.octoapp.engine.octoprint.api.plugins.bundled

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.OctoHttpClientSettings
import de.crysxd.octoapp.engine.octoprint.dto.plugins.pluginmanager.PluginList
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class PluginManagerApiTest {

    @Test
    fun WHEN_plugin_versions_are_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/plugin/pluginmanager/plugins/versions"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                       {
                            "backup": null,
                            "cancelobject": "0.4.7",
                            "file_check": "2021.2.23"
                        }
                    """.trimIndent()
                )
            }
        )
        val target = PluginManagerApi(rotator, http)
        //endregion
        //region WHEN
        val versions = target.getSimplePluginInfo()
        //endregion
        //region THEN
        assertEquals(
            expected = mapOf(
                "backup" to null,
                "cancelobject" to "0.4.7",
                "file_check" to "2021.2.23",
            ),
            actual = versions,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_plugins_are_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/plugin/pluginmanager/plugins"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                       {
                            "octoprint": "1.8.3",
                            "online": true,
                            "os": "linux",
                            "pip": {
                                "additional_args": null,
                                "available": true,
                                "install_dir": "/usr/local/lib/python3.8/site-packages",
                                "python": "/usr/local/bin/python",
                                "use_user": false,
                                "version": "21.2.4",
                                "virtual_env": false
                            },
                            "plugins": [
                                {
                                    "author": "Gina Häußge",
                                    "blacklisted": false,
                                    "bundled": true,
                                    "description": "Allows your printer to trigger notifications via action commands on the connection",
                                    "disabling_discouraged": "Without this plugin your printer will no longer be able to trigger notifications in OctoPrint",
                                    "enabled": true,
                                    "forced_disabled": false,
                                    "incompatible": false,
                                    "key": "action_command_notification",
                                    "license": "AGPLv3",
                                    "managable": false,
                                    "name": "Action Command Notification Support",
                                    "notifications": null,
                                    "origin": "module",
                                    "pending_disable": false,
                                    "pending_enable": false,
                                    "pending_install": false,
                                    "pending_uninstall": false,
                                    "python": ">=3.7,<4",
                                    "safe_mode_victim": false,
                                    "url": null,
                                    "version": null
                                },
                                {
                                    "author": "Brad Hochgesang",
                                    "blacklisted": false,
                                    "bundled": false,
                                    "description": "Create stabilized timelapses of your 3d prints.  Highly customizable, loads of presets, lots of fun.",
                                    "disabling_discouraged": false,
                                    "enabled": false,
                                    "forced_disabled": true,
                                    "incompatible": false,
                                    "key": "octolapse",
                                    "license": "AGPLv3",
                                    "managable": true,
                                    "name": "Octolapse",
                                    "notifications": null,
                                    "origin": "entry_point",
                                    "pending_disable": false,
                                    "pending_enable": false,
                                    "pending_install": false,
                                    "pending_uninstall": false,
                                    "python": ">=2.7,<4",
                                    "safe_mode_victim": false,
                                    "url": "https://github.com/FormerLurker/Octolapse",
                                    "version": "0.4.1"
                                }
                            ],
                            "safe_mode": false,
                            "supported_extensions": {
                                "archive": [
                                    ".zip",
                                    ".tar.gz",
                                    ".tgz",
                                    ".tar",
                                    ".gz",
                                    ".whl"
                                ],
                                "python": [
                                    ".py"
                                ]
                            }
                        }
                    """.trimIndent()
                )
            }
        )
        val target = PluginManagerApi(rotator, http)
        //endregion
        //region WHEN
        val versions = target.getFullPluginInfo()
        //endregion
        //region THEN
        assertEquals(
            expected = PluginList(
                plugins = listOf(
                    PluginList.Plugin(key = "action_command_notification", enabled = true, version = null),
                    PluginList.Plugin(key = "octolapse", enabled = false, version = "0.4.1")
                )
            ),
            actual = versions,
            message = "Expected response to match"
        )
        //endregion
    }
}