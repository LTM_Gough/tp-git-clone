package de.crysxd.octoapp.engine.octoprint.http

import de.crysxd.octoapp.engine.exceptions.DownloadTooLargeException
import de.crysxd.octoapp.engine.exceptions.InvalidApiKeyException
import de.crysxd.octoapp.engine.exceptions.MissingPermissionException
import de.crysxd.octoapp.engine.exceptions.PrintBootingException
import de.crysxd.octoapp.engine.exceptions.PrinterApiException
import de.crysxd.octoapp.engine.exceptions.PrinterHttpsException
import de.crysxd.octoapp.engine.exceptions.PrinterNotFoundException
import de.crysxd.octoapp.engine.exceptions.PrinterNotOperationalException
import de.crysxd.octoapp.engine.exceptions.PrinterUnavailableException
import de.crysxd.octoapp.engine.exceptions.ngrok.NgrokTunnelNotFoundException
import de.crysxd.octoapp.engine.exceptions.octoeverywhere.OctoEverywhereCantReachPrinterException
import de.crysxd.octoapp.engine.exceptions.octoeverywhere.OctoEverywhereConnectionNotFoundException
import de.crysxd.octoapp.engine.exceptions.octoeverywhere.OctoEverywhereSubscriptionMissingException
import de.crysxd.octoapp.engine.exceptions.spaghettidetective.SpaghettiDetectiveCantReachPrinterException
import de.crysxd.octoapp.engine.exceptions.spaghettidetective.SpaghettiDetectiveTunnelNotFoundException
import de.crysxd.octoapp.engine.exceptions.spaghettidetective.SpaghettiDetectiveTunnelUsageLimitReachedException
import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.octoprint.api.OctoUserApi
import de.crysxd.octoapp.engine.octoprint.dto.login.OctoUser
import de.crysxd.octoapp.sharedcommon.exceptions.BasicAuthRequiredException
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.MockRequestHandleScope
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.respondOk
import io.ktor.client.request.HttpRequestData
import io.ktor.client.request.HttpResponseData
import io.ktor.client.request.get
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import io.ktor.utils.io.errors.IOException
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class OctoExceptionInterceptorTest {

    private fun createMockHttpClient(
        baseUrlForCurrentUserInterrogation: String = "http://gstatic.com",
        block: MockRequestHandleScope.(request: HttpRequestData) -> HttpResponseData,
    ) = createRealHttpClient(baseUrlForCurrentUserInterrogation, MockEngine(block))

    private fun createRealHttpClient(
        baseUrlForCurrentUserInterrogation: String = "http://gstatic.com",
        engine: HttpClientEngine
    ) = HttpClient(engine) {
        installJsonSerialization()
    }.apply {
        installOctoPrintGenerateExceptionInterceptorPlugin {
            OctoUserApi(httpClient = it, baseUrlRotator = ActiveBaseUrlRotator(listOf(Url(baseUrlForCurrentUserInterrogation))))
        }
    }

    private fun NetworkException?.assertException(url: Url, hostExcepted: Boolean = false) {
        assertNotNull(
            actual = this,
            message = "Expected exception ot be caught"
        )
        assertEquals(
            actual = webUrl,
            expected = url,
            message = "Expected URL to match"
        )
        if (hostExcepted) {
            assertTrue(
                actual = message?.contains("redacted-host-") == true,
                message = "Expected host to be redacted: `${message}`"
            )
        }
        assertFalse(
            actual = message?.contains(url.host) == true,
            message = "Expected host to not be clear text: `${message}`"
        )
        assertFalse(
            actual = technicalMessage.contains("redacted-host-"),
            message = "Expected host to not be redacted: `${technicalMessage}`"
        )
        if (hostExcepted) {
            assertTrue(
                actual = technicalMessage.contains(url.host),
                message = "Expected host to be clear text: `${technicalMessage}`"
            )
        }
    }

    @Test
    fun WHEN_a_101_is_received_THEN_result_is_returned() = runBlocking {
        //region GIVEN
        val http = createMockHttpClient {
            respond(content = "", status = HttpStatusCode.SwitchingProtocols)
        }
        val url = Url("http://gstatic.com")
        //endregion
        //region WHEN
        val response = http.get(url).call.response
        //endregion
        //region THEN
        assertEquals(
            actual = response.status.value,
            expected = 101,
            message = "Expected URL to match"
        )
        //endregion
    }

    @Test
    fun WHEN_a_2xx_is_received_THEN_result_is_returned() = runBlocking {
        //region GIVEN
        val http = createMockHttpClient {
            respondOk()
        }
        val url = Url("http://gstatic.com")
        //endregion
        //region WHEN
        val response = http.get(url).call.response
        //endregion
        //region THEN
        assertEquals(
            actual = response.status.value,
            expected = 200,
            message = "Expected URL to match"
        )
        //endregion
    }

    @Test
    fun WHEN_a_409_is_received_THEN_printer_not_operational_exception_is_thrown() = runBlocking {
        //region GIVEN
        val http = createMockHttpClient {
            respond(content = "", status = HttpStatusCode.Conflict)
        }
        val url = Url("http://gstatic.com")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: PrinterNotOperationalException) {
            e
        }
        //endregion
        //region THEN
        assertNotNull(
            actual = exception,
            message = "Expected exception ot be caught"
        )
        assertEquals(
            actual = exception.webUrl,
            expected = url,
            message = "Expected URL to match"
        )
        assertEquals(
            actual = exception.technicalMessage,
            expected = "Printer was not operational when accessing http://gstatic.com",
            message = "Expected technical message to match"
        )
        assertEquals(
            actual = exception.message,
            expected = "Printer was not operational when accessing http://redacted-host-1ea0c258",
            message = "Expected message to be redacted"
        )
        //endregion
    }

    @Test
    fun WHEN_a_401_is_received_from_obico_THEN_tunnel_not_found_exception_thrown() = runBlocking {
        //region GIVEN
        val http = createMockHttpClient {
            respond(content = "", status = HttpStatusCode.Unauthorized)
        }
        val url = Url("http://something.obico.io")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: SpaghettiDetectiveTunnelNotFoundException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        assertEquals(
            actual = exception!!.technicalMessage,
            expected = "Spaghetti Detective reported tunnel as deleted",
            message = "Expected technical message to match"
        )
        //endregion
    }

    @Test
    fun WHEN_a_401_is_received_from_octoeverywhere_THEN_tunnel_not_found_exception_thrown() = runBlocking {
        //region GIVEN
        val http = createMockHttpClient {
            respond(content = "", status = HttpStatusCode.Unauthorized)
        }
        val url = Url("http://something.octoeverywhere.com")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: OctoEverywhereConnectionNotFoundException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        assertEquals(
            actual = exception!!.technicalMessage,
            expected = "OctoEverywhere reported connection as broken (401)",
            message = "Expected technical message to match"
        )
        //endregion
    }

    @Test
    fun WHEN_a_401_is_with_www_header_THEN_basic_auth_exception_thrown() = runBlocking {
        //region GIVEN
        val message = "Credentials required"
        val http = createMockHttpClient {
            respond(content = "", status = HttpStatusCode.Unauthorized, headers = headersOf("WWW-Authenticate", """spamrealm="$message"spam"""))
        }
        val url = Url("http://gstatic.com")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: BasicAuthRequiredException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        assertEquals(
            actual = exception!!.userRealm,
            expected = message,
            message = "Expected user realm to match"
        )
        //endregion
    }

    @Test
    fun WHEN_a_401_is_with_www_header_but_no_realm_THEN_basic_auth_exception_thrown() = runBlocking {
        //region GIVEN
        val http = createMockHttpClient {
            respond(content = "", status = HttpStatusCode.Unauthorized, headers = headersOf("WWW-Authenticate", "Something random"))
        }
        val url = Url("http://gstatic.com")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: BasicAuthRequiredException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        assertEquals(
            actual = exception!!.userRealm,
            expected = "no message",
            message = "Expected user realm to match"
        )
        //endregion
    }

    @Test
    fun WHEN_a_403_is_received_THEN_user_is_requested_and_api_key_exception_thrown_if_guest() = runBlocking {
        //region GIVEN
        var userRequested = false
        val http = createMockHttpClient { request ->
            if (request.url.pathSegments == listOf("api", "currentuser")) {
                userRequested = true
                respond(
                    status = HttpStatusCode.OK,
                    content = Json.encodeToString(
                        OctoUser(
                            permissions = emptyList(),
                            groups = listOf("guests"),
                            name = null,
                        )
                    ),
                )
            } else {
                respond(content = "", HttpStatusCode.Forbidden)
            }
        }
        val url = Url("http://gstatic.com/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: InvalidApiKeyException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url, hostExcepted = true)
        assertEquals(
            actual = exception!!.message,
            expected = "OctoPrint reported an invalid API key when accessing http://redacted-host-1ea0c258/api/something",
            message = "Expected message to match"
        )
        assertTrue(
            actual = userRequested,
            message = "Expected user to be requested"
        )
        //endregion
    }

    @Test
    fun WHEN_a_403_is_received_THEN_user_is_requested_and_missing_permission_exception_thrown_if_not_guest() = runBlocking {
        //region GIVEN
        var userRequested = false
        val http = createMockHttpClient { request ->
            if (request.url.pathSegments == listOf("api", "currentuser")) {
                userRequested = true
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = Json.encodeToString(
                        OctoUser(
                            permissions = emptyList(),
                            groups = listOf("admins"),
                            name = "johndoe",
                        )
                    ),
                )
            } else {
                respond(content = "", HttpStatusCode.Forbidden)
            }
        }
        val url = Url("http://gstatic.com/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: MissingPermissionException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url, hostExcepted = true)
        assertTrue(
            actual = userRequested,
            message = "Expected user to be requested"
        )
        assertEquals(
            actual = exception!!.message,
            expected = "OctoPrint returned 403 for http://redacted-host-1ea0c258/api/something but the API key is valid. This indicates a missing permission.",
            message = "Expected message to match"
        )
        //endregion
    }

    @Test
    fun WHEN_a_403_is_received_from_user_endpoint_THEN_user_is_not_requested_and_api_key_exception_thrown() = runBlocking {
        //region GIVEN
        var requestCount = 0
        val http = createMockHttpClient {
            requestCount++
            respond(content = "", HttpStatusCode.Forbidden)
        }
        val url = Url("http://gstatic.com/something/currentuser?query=true")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: InvalidApiKeyException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        assertEquals(
            actual = requestCount,
            expected = 1,
            message = "Expected a single request to be requested"
        )
        //endregion
    }

    @Test
    fun WHEN_a_403_is_received_from_obico_user_endpoint_THEN_tunnel_not_found_exception_is_thrown() = runBlocking {
        //region GIVEN
        var userRequested = false
        val http = createMockHttpClient(baseUrlForCurrentUserInterrogation = "http://something.obico.io") { request ->
            if (request.url.pathSegments == listOf("api", "currentuser")) {
                userRequested = true
                respond(content = "", HttpStatusCode.Unauthorized)
            } else {
                respond(content = "", HttpStatusCode.Forbidden)
            }
        }
        val url = Url("http://something.obico.io/something/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: SpaghettiDetectiveTunnelNotFoundException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        assertTrue(
            actual = userRequested,
            message = "Expected user to be requested"
        )
        //endregion
    }

    @Test
    fun WHEN_a_403_is_received_but_current_user_interrogation_fails_THEN_api_key_exception_thrown() = runBlocking {
        //region GIVEN
        var userRequested = false
        val http = createMockHttpClient { request ->
            if (request.url.pathSegments == listOf("api", "currentuser")) {
                userRequested = true
                throw IOException("Oh no!")
            } else {
                respond(content = "", HttpStatusCode.Forbidden)
            }
        }
        val url = Url("http://gstatic.com/something/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: InvalidApiKeyException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url, hostExcepted = true)
        assertEquals(
            actual = exception!!.message,
            expected = "OctoPrint reported an invalid API key when accessing http://redacted-host-1ea0c258/something/api/something",
            message = "Expected message to match"
        )
        assertTrue(
            actual = userRequested,
            message = "Expected user to be requested"
        )
        //endregion
    }

    @Test
    fun WHEN_a_404_is_received_THEN_printer_not_found_exception_thrown() = runBlocking {
        //region GIVEN
        val body = "test"
        val http = createMockHttpClient {
            respond(content = body, status = HttpStatusCode.NotFound)
        }
        val url = Url("http://gstatic.com/something/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: PrinterNotFoundException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url, hostExcepted = true)
        assertEquals(
            actual = exception!!.message,
            expected = "Received unexpected response code 404 from http://redacted-host-1ea0c258/something/api/something (body=$body)",
            message = "Expected message to match"
        )
        //endregion
    }

    @Test
    fun WHEN_a_404_is_received_from_ngrok_with_error_in_body_THEN_printer_not_found_exception_thrown() = runBlocking {
        //region GIVEN
        val body = "ERR_NGROK_3200"
        val http = createMockHttpClient {
            respond(content = body, status = HttpStatusCode.NotFound)
        }
        val url = Url("http://something.ngrok.io/something/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: NgrokTunnelNotFoundException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        //endregion
    }

    @Test
    fun WHEN_a_404_is_received_from_ngrok_THEN_printer_not_found_exception_thrown() = runBlocking {
        //region GIVEN
        val body = "test"
        val http = createMockHttpClient {
            respond(content = body, status = HttpStatusCode.NotFound)
        }
        val url = Url("http://somehting.ngrok.io/something/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: PrinterNotFoundException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        assertEquals(
            actual = exception!!.message,
            expected = "Received unexpected response code 404 from http://redacted-host-5f8851cb/something/api/something (body=$body)",
            message = "Expected message to match"
        )
        //endregion
    }

    @Test
    fun WHEN_a_413_is_received_from_ngrok_THEN_printer_not_found_exception_thrown() = runBlocking {
        //region GIVEN
        val body = "test"
        val http = createMockHttpClient {
            respond(content = body, status = HttpStatusCode.PayloadTooLarge)
        }
        val url = Url("http://somehting.ngrok.io/something/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: DownloadTooLargeException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        //endregion
    }

    @Test
    fun WHEN_a_601_is_received_THEN_printer_not_reachable_exception_thrown() = runBlocking {
        //region GIVEN
        val body = "test"
        val http = createMockHttpClient {
            respond(content = body, status = HttpStatusCode(601, "Printer not reachable"))
        }
        val url = Url("http://somehting.octoeverywhere.com/something/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: OctoEverywhereCantReachPrinterException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        //endregion
    }

    @Test
    fun WHEN_a_603_is_received_THEN_printer_not_reachable_exception_thrown() = runBlocking {
        //region GIVEN
        val body = "test"
        val http = createMockHttpClient {
            respond(content = body, status = HttpStatusCode(603, "Connection gone"))
        }
        val url = Url("http://somehting.octoeverywhere.com/something/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: OctoEverywhereConnectionNotFoundException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        //endregion
    }

    @Test
    fun WHEN_a_604_is_received_THEN_connection_not_found_exception_thrown() = runBlocking {
        //region GIVEN
        val body = "test"
        val http = createMockHttpClient {
            respond(content = body, status = HttpStatusCode(604, "Connection gone"))
        }
        val url = Url("http://somehting.octoeverywhere.com/something/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: OctoEverywhereConnectionNotFoundException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        //endregion
    }

    @Test
    fun WHEN_a_605_is_received_THEN_connection_not_found_exception_thrown() = runBlocking {
        //region GIVEN
        val body = "test"
        val http = createMockHttpClient {
            respond(content = body, status = HttpStatusCode(605, "Connection gone"))
        }
        val url = Url("http://somehting.octoeverywhere.com/something/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: OctoEverywhereSubscriptionMissingException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        //endregion
    }

    @Test
    fun WHEN_a_606_is_received_THEN_connection_not_found_exception_thrown() = runBlocking {
        //region GIVEN
        val body = "test"
        val http = createMockHttpClient {
            respond(content = body, status = HttpStatusCode(606, "Connection gone"))
        }
        val url = Url("http://somehting.octoeverywhere.com/something/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: OctoEverywhereConnectionNotFoundException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        //endregion
    }

    @Test
    fun WHEN_a_607_is_received_THEN_download_too_large_exception_thrown() = runBlocking {
        //region GIVEN
        val body = "test"
        val http = createMockHttpClient {
            respond(content = body, status = HttpStatusCode(607, "Connection gone"))
        }
        val url = Url("http://somehting.octoeverywhere.com/something/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: DownloadTooLargeException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        //endregion
    }

    @Test
    fun WHEN_a_481_is_received_THEN_download_too_large_exception_thrown() = runBlocking {
        //region GIVEN
        val body = "test"
        val http = createMockHttpClient {
            respond(content = body, status = HttpStatusCode(481, "Usage Limit Reached"))
        }
        val url = Url("http://somehting.obico.io/something/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: SpaghettiDetectiveTunnelUsageLimitReachedException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        //endregion
    }

    @Test
    fun WHEN_a_482_is_received_THEN_download_too_large_exception_thrown() = runBlocking {
        //region GIVEN
        val body = "test"
        val http = createMockHttpClient {
            respond(content = body, status = HttpStatusCode(482, "Printer Unreachable"))
        }
        val url = Url("http://somehting.octoeverywhere.com/something/api/something")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: SpaghettiDetectiveCantReachPrinterException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        //endregion
    }

    @Test
    fun WHEN_a_5xx_is_received_THEN_booting_exception_is_thrown() = runBlocking {
        //region GIVEN
        val http = createMockHttpClient {
            respond(content = "", status = HttpStatusCode.ServiceUnavailable)
        }
        val url = Url("http://gstatic.com")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: PrintBootingException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url)
        assertEquals(
            actual = exception!!.technicalMessage,
            expected = "OctoPrint is still starting up",
            message = "Expected message to match"
        )
        //endregion
    }

    @Test
    fun WHEN_a_random_status_is_received_THEN_unexpected_exception_is_thrown() = runBlocking {
        //region GIVEN
        val body = "Something"
        val http = createMockHttpClient {
            respond(content = body, status = HttpStatusCode.PreconditionFailed)
        }
        val url = Url("http://gstatic.com")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: PrinterApiException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url, hostExcepted = true)
        assertEquals(
            actual = exception!!.responseCode,
            expected = 412,
            message = "Expected code to match"
        )
        assertEquals(
            actual = exception.message,
            expected = "Received unexpected response code 412 from http://redacted-host-1ea0c258 (body=$body)",
            message = "Expected message to match"
        )
        assertEquals(
            actual = exception.technicalMessage,
            expected = "Received unexpected response code 412 from http://gstatic.com (body=$body)",
            message = "Expected message to match"
        )
        //endregion
    }

    @Test
    fun WHEN_host_unknown_THEN_printer_unavailable_exception_is_thrown() = runBlocking {
        //region GIVEN
        val http = HttpClient().apply {
            installOctoPrintGenerateExceptionInterceptorPlugin { throw NotImplementedError() }
        }
        val url = Url("http://thishostdoesnotexist-jsfhiudsfushiidsfdusifhuisdhfusdfhsdif.com")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: PrinterUnavailableException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url, hostExcepted = true)
        //endregion
    }

    @Test
    fun WHEN_port_closed_THEN_printer_unavailable_exception_is_thrown() = runBlocking {
        //region GIVEN
        val http = HttpClient().apply {
            installOctoPrintGenerateExceptionInterceptorPlugin { throw NotImplementedError() }
        }
        val url = Url("http://localhost:9999")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: PrinterUnavailableException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url, hostExcepted = true)
        //endregion
    }

    @Test
    fun WHEN_self_signed_certificate_THEN_printer_https_exception_is_thrown() = runBlocking {
        //region GIVEN
        val http = HttpClient().apply {
            installOctoPrintGenerateExceptionInterceptorPlugin { throw NotImplementedError() }
        }
        val url = Url("https://self-signed.badssl.com")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: PrinterHttpsException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url, hostExcepted = true)
        //endregion
    }

    @Test
    fun WHEN_expired_certificate_THEN_printer_https_exception_is_thrown() = runBlocking {
        //region GIVEN
        val http = HttpClient().apply {
            installOctoPrintGenerateExceptionInterceptorPlugin { throw NotImplementedError() }
        }
        val url = Url("https://expired.badssl.com")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: PrinterHttpsException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url, hostExcepted = true)
        //endregion
    }

    @Test
    fun WHEN_wrong_host_THEN_printer_https_exception_is_thrown() = runBlocking {
        //region GIVEN
        val http = HttpClient().apply {
            installOctoPrintGenerateExceptionInterceptorPlugin { throw NotImplementedError() }
        }
        val url = Url("https://wrong.host.badssl.com")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: PrinterHttpsException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url, hostExcepted = true)
        //endregion
    }

    @Test
    fun WHEN_no_subject_THEN_printer_https_exception_is_thrown() = runBlocking {
        //region GIVEN
        val http = HttpClient().apply {
            installOctoPrintGenerateExceptionInterceptorPlugin { throw NotImplementedError() }
        }
        val url = Url("https://no-subject.badssl.com")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: PrinterHttpsException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url, hostExcepted = true)
        //endregion
    }

    @Test
    fun WHEN_untrusted_root_THEN_printer_https_exception_is_thrown() = runBlocking {
        //region GIVEN
        val http = HttpClient().apply {
            installOctoPrintGenerateExceptionInterceptorPlugin { throw NotImplementedError() }
        }
        val url = Url("https://untrusted-root.badssl.com")
        //endregion
        //region WHEN
        val exception = try {
            http.get(url)
            null
        } catch (e: PrinterHttpsException) {
            e
        }
        //endregion
        //region THEN
        exception.assertException(url, hostExcepted = true)
        //endregion
    }

}