package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.MockEventSink
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.mocks.assertPost
import de.crysxd.octoapp.engine.models.commands.JobCommand
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.job.Job
import de.crysxd.octoapp.engine.models.job.JobInformation
import de.crysxd.octoapp.engine.models.job.ProgressInformation
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.OctoHttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class OctoJobApiTest {

    @Test
    fun WHEN_job_is_loaded_but_idle_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/job"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                      {
                        "job": {
                            "averagePrintTime": null,
                            "estimatedPrintTime": null,
                            "filament": null,
                            "file": {
                                "date": null,
                                "display": null,
                                "name": null,
                                "origin": null,
                                "path": null,
                                "size": null
                            },
                            "lastPrintTime": null,
                            "user": null
                        },
                        "progress": {
                            "completion": null,
                            "filepos": null,
                            "printTime": null,
                            "printTimeLeft": null,
                            "printTimeLeftOrigin": null
                        },
                        "state": "Operational"
                    }
                    """.trimIndent(),
                )
            }
        )
        val target = OctoJobApi(rotator, http, eventSink = MockEventSink)
        //endregion
        //region WHEN
        val response = target.getJob()
        //endregion
        //region THEN
        assertEquals(
            expected = Job(
                progress = ProgressInformation(),
                info = JobInformation()
            ),
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_job_is_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/job"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        {
                            "job": {
                                "averagePrintTime": 167.01728628699857,
                                "estimatedPrintTime": 1808.3587931181726,
                                "filament": {
                                    "tool0": {
                                        "length": 2996.298679999905,
                                        "volume": 7.206942908302754
                                    }
                                },
                                "file": {
                                    "date": 1663854589,
                                    "display": "Peniscoin_ergin_0.2mm_PLA_MK3S_44m.gcode",
                                    "name": "Peniscoin_ergin_0.2mm_PLA_MK3S_44m.gcode",
                                    "origin": "local",
                                    "path": "Peniscoin_ergin_0.2mm_PLA_MK3S_44m.gcode",
                                    "size": 1870735
                                },
                                "lastPrintTime": 167.01728628699857,
                                "user": "beagle"
                            },
                            "progress": {
                                "completion": 1.3415582645323898,
                                "filepos": 25097,
                                "printTime": 0,
                                "printTimeLeft": 166,
                                "printTimeLeftOrigin": "average"
                            },
                            "state": "Operational"
                        }
                    """.trimIndent(),
                )
            }
        )
        val target = OctoJobApi(rotator, http, eventSink = MockEventSink)
        //endregion
        //region WHEN
        val response = target.getJob()
        //endregion
        //region THEN
        assertEquals(
            expected = Job(
                info = JobInformation(
                    file = JobInformation.JobFile(
                        date = 1663854589000,
                        display = "Peniscoin_ergin_0.2mm_PLA_MK3S_44m.gcode",
                        name = "Peniscoin_ergin_0.2mm_PLA_MK3S_44m.gcode",
                        path = "Peniscoin_ergin_0.2mm_PLA_MK3S_44m.gcode",
                        size = 1870735,
                        origin = FileOrigin.Local,
                    )
                ),
                progress = ProgressInformation(
                    completion = 1.3415582f,
                    filepos = 25097,
                    printTime = 0,
                    printTimeLeft = 166,
                    printTimeLeftOrigin = "average"
                )
            ),
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }

    private fun runJobCommandTest(command: JobCommand, expectedBody: String) = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/job"),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                assertEquals(
                    expected = expectedBody,
                    actual = request.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = "",
                )
            }
        )
        val target = OctoJobApi(rotator, http, eventSink = MockEventSink)
        //endregion
        //region WHEN
        target.executeJobCommand(command)
        //endregion
        //region THEN
        //endregion
    }

    @Test
    fun WHEN_print_is_cancelled_THEN_the_response_is_returned() = runJobCommandTest(
        command = JobCommand.CancelJobCommand,
        expectedBody = "{\"command\":\"cancel\"}"
    )

    @Test
    fun WHEN_print_is_paused_THEN_the_response_is_returned() = runJobCommandTest(
        command = JobCommand.PauseJobCommand,
        expectedBody = "{\"command\":\"pause\"}"
    )

    @Test
    fun WHEN_print_is_resumed_THEN_the_response_is_returned() = runJobCommandTest(
        command = JobCommand.ResumeJobCommand,
        expectedBody = "{\"command\":\"resume\",\"action\":\"resume\"}"
    )

    @Test
    fun WHEN_print_is_toggle_paused_THEN_the_response_is_returned() = runJobCommandTest(
        command = JobCommand.TogglePauseCommand,
        expectedBody = "{\"command\":\"pause\",\"action\":\"toggle\"}"
    )

    @Test
    fun WHEN_print_is_started_THEN_the_response_is_returned() = runJobCommandTest(
        command = JobCommand.StartJobCommand,
        expectedBody = "{\"command\":\"start\"}"
    )

    @Test
    fun WHEN_print_is_restarted_THEN_the_response_is_returned() = runJobCommandTest(
        command = JobCommand.RestartJobCommand,
        expectedBody = "{\"command\":\"restart\"}"
    )
}