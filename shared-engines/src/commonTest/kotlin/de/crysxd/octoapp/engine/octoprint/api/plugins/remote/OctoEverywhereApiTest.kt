package de.crysxd.octoapp.engine.octoprint.api.plugins.remote

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.OctoHttpClientSettings
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octoeverywhere.OctoEverywhereInfo
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class OctoEverywhereApiTest {

    @Test
    fun WHEN_octoeverywhere_info_is_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/octoeverywhere"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                       {
                            "PluginVersion": "1.10.5",
                            "PrinterId": "347R9DNGCJZ5IS455618ZZZY4QBYC0"
                        }
                    """.trimIndent()
                )
            }
        )
        val target = OctoEverywhereApi(rotator, http)
        //endregion
        //region WHEN
        val tunnel = target.getInfo()
        //endregion
        //region THEN
        assertEquals(
            expected = OctoEverywhereInfo(
                version = "1.10.5",
                printerId = "347R9DNGCJZ5IS455618ZZZY4QBYC0"
            ),
            actual = tunnel,
            message = "Expected response to match"
        )
        //endregion
    }
}