package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.models.commands.ConnectionCommand
import de.crysxd.octoapp.engine.models.connection.Connection
import de.crysxd.octoapp.engine.models.connection.ConnectionState
import de.crysxd.octoapp.engine.models.printer.PrinterProfileReference
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.OctoHttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class OctoConnectionApiTest {

    @Test
    fun WHEN_connection_state_is_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                assertEquals(
                    expected = Url("${rotator.activeUrl.value}/api/connection"),
                    actual = request.url,
                    message = "Expected URL to be correct"
                )

                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        {
                            "current": {
                                "baudrate": 115200,
                                "port": "VIRTUAL",
                                "printerProfile": "_default",
                                "state": "Operational"
                            },
                            "options": {
                                "baudratePreference": null,
                                "baudrates": [
                                    250000,
                                    230400,
                                    115200,
                                    57600,
                                    38400,
                                    19200,
                                    9600
                                ],
                                "portPreference": null,
                                "ports": [
                                    "VIRTUAL"
                                ],
                                "printerProfilePreference": "_default",
                                "printerProfiles": [
                                    {
                                        "id": "_default",
                                        "name": "Default"
                                    }
                                ]
                            }
                        }
                    """.trimIndent(),
                )
            }
        )
        val target = OctoConnectionApi(http, rotator)
        //endregion
        //region WHEN
        val connection = target.getConnection()
        //endregion
        //region THEN
        assertEquals(
            expected = ConnectionState(
                current = Connection(
                    state = Connection.State.MAYBE_OPERATIONAL,
                    port = "VIRTUAL",
                    baudrate = 115200,
                    printerProfile = "_default"
                ),
                options = ConnectionState.Options(
                    ports = listOf("VIRTUAL"),
                    autoConnect = false,
                    printerProfilePreference = "_default",
                    baudratePreference = null,
                    portPreference = null,
                    printerProfiles = listOf(
                        PrinterProfileReference(
                            id = "_default",
                            name = "Default",
                        ),
                    ),
                    baudrates = listOf(250000, 230400, 115200, 57600, 38400, 19200, 9600)
                )
            ),
            actual = connection,
            message = "Expected response to match"
        )

        //endregion
    }

    @Test
    fun WHEN_connect_is_started_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                assertEquals(
                    expected = Url("${rotator.activeUrl.value}/api/connection"),
                    actual = request.url,
                    message = "Expected URL to be correct"
                )
                assertEquals(
                    expected = "{\"command\":\"connect\"}",
                    actual = request.body.toByteReadPacket().readText(),
                    message = "Expected body to be correct"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = "{}",
                )
            }
        )
        val target = OctoConnectionApi(http, rotator)
        //endregion
        //region WHEN
        target.executeConnectionCommand(ConnectionCommand.Connect())
        //endregion
        //region THEN
        //endregion
    }

    @Test
    fun WHEN_disconnect_is_started_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                assertEquals(
                    expected = Url("${rotator.activeUrl.value}/api/connection"),
                    actual = request.url,
                    message = "Expected URL to be correct"
                )
                assertEquals(
                    expected = "{\"command\":\"disconnect\"}",
                    actual = request.body.toByteReadPacket().readText(),
                    message = "Expected body to be correct"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = "{}",
                )
            }
        )
        val target = OctoConnectionApi(http, rotator)
        //endregion
        //region WHEN
        target.executeConnectionCommand(ConnectionCommand.Disconnect)
        //endregion
        //region THEN
        //endregion
    }
}