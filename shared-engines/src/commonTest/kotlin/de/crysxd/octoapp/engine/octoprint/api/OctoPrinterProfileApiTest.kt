package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.OctoHttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.HttpStatusCode
import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.http.appendEncodedPathSegments
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals


internal class OctoPrinterProfileApiTest {

    @Test
    fun WHEN_the_profiles_are_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).appendEncodedPathSegments("api", "printerprofiles").build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    //region content = ...
                    content = """
                       {
                            "profiles": {
                                "_default": {
                                    "axes": {
                                        "e": {
                                            "inverted": false,
                                            "speed": 300
                                        },
                                        "x": {
                                            "inverted": false,
                                            "speed": 6000
                                        },
                                        "y": {
                                            "inverted": false,
                                            "speed": 8000
                                        },
                                        "z": {
                                            "inverted": false,
                                            "speed": 200
                                        }
                                    },
                                    "color": "default",
                                    "current": true,
                                    "default": true,
                                    "extruder": {
                                        "count": 1,
                                        "defaultExtrusionLength": 5,
                                        "nozzleDiameter": 0.5,
                                        "offsets": [
                                            [
                                                0.0,
                                                0.0
                                            ]
                                        ],
                                        "sharedNozzle": false
                                    },
                                    "heatedBed": true,
                                    "heatedChamber": false,
                                    "id": "_default",
                                    "model": "Ender 3 Pro",
                                    "name": "Charlotte",
                                    "resource": "http://charlotte.local/api/printerprofiles/_default",
                                    "volume": {
                                        "custom_box": false,
                                        "depth": 235.0,
                                        "formFactor": "rectangular",
                                        "height": 220.0,
                                        "origin": "lowerleft",
                                        "width": 235.0
                                    }
                                }
                            }
                        }
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoPrinterProfileApi(rotator, http)
        //endregion
        //region WHEN
        val user = target.getPrinterProfiles()
        //endregion
        //region THEN
        assertEquals(
            expected = mutableMapOf(
                "_default" to PrinterProfile(
                    axes = PrinterProfile.Axes(
                        e = PrinterProfile.Axis(inverted = false, speed = 300f),
                        x = PrinterProfile.Axis(inverted = false, speed = 6000f),
                        y = PrinterProfile.Axis(inverted = false, speed = 8000f),
                        z = PrinterProfile.Axis(inverted = false, speed = 200f),
                    ),
                    color = "default",
                    current = true,
                    default = true,
                    extruder = PrinterProfile.Extruder(
                        nozzleDiameter = 0.5f,
                        sharedNozzle = false,
                        count = 1,
                        offsets = listOf(0f to 0f),
                        defaultExtrusionLength = 5f
                    ),
                    heatedBed = true,
                    heatedChamber = false,
                    id = "_default",
                    model = "Ender 3 Pro",
                    name = "Charlotte",
                    volume = PrinterProfile.Volume(
                        width = 235f,
                        depth = 235f,
                        height = 220f,
                        origin = PrinterProfile.Origin.LowerLeft,
                        formFactor = PrinterProfile.FormFactor.Rectangular,
                    )
                )
            ),
            actual = user,
            message = "Expected response to match"
        )
        //endregion
    }
}
