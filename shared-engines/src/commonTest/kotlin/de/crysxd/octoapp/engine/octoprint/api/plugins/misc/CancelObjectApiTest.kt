package de.crysxd.octoapp.engine.octoprint.api.plugins.misc

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.mocks.assertPost
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.OctoHttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class CancelObjectApiTest {

    @Test
    fun WHEN_initial_message_is_triggered_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/cancelobject"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(status = HttpStatusCode.NoContent, content = "")
            }
        )
        val target = CancelObjectApi(rotator, http)
        //endregion
        //region WHEN
        target.triggerInitialMessage()
        //endregion
        //region THEN
        //endregion
    }

    @Test
    fun WHEN_object_is_cancelled_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/cancelobject"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                assertEquals(
                    expected = "{\"cancelled\":3,\"command\":\"cancel\"}",
                    actual = request.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                respond(status = HttpStatusCode.NoContent, content = "")
            }
        )
        val target = CancelObjectApi(rotator, http)
        //endregion
        //region WHEN
        target.cancelObject(objectId = 3)
        //endregion
        //region THEN
        //endregion
    }
}