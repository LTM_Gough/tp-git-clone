package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.mocks.TestApiBuilder
import de.crysxd.octoapp.engine.mocks.assertPost
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoSettings
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respondOk
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.Url
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class WledApiTest {

    @Test
    fun WHEN_devices_are_listed_THEN_devices_are_returned() = runBlocking {
        //region GIVEN
        val settings = OctoSettings(
            plugins = OctoSettings.PluginSettingsGroup(
                wled = OctoSettings.Wled()
            )
        )
        val target = TestApiBuilder(
            MockEngine {
                throw IllegalStateException("No request expected")
            }
        ) { rotator, httpClient ->
            WledApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        //endregion
        //region WHEN
        val devices = target.getDevices(settings.map())
        //endregion
        //region THEN
        assertEquals(
            expected = 1,
            actual = devices.size,
            message = "Expected one device"
        )
        assertEquals(
            expected = WledApi.PowerDevice(owner = target),
            actual = devices[0],
            message = "Expected device to match"
        )
        //endregion
    }

    @Test
    fun WHEN_device_is_turned_on_THEN_request_is_made() = testSet(
        request = "{\"command\":\"lights_on\"}",
        action = { turnOn() }
    )

    @Test
    fun WHEN_device_is_turned_off_THEN_request_is_made() = testSet(
        request = "{\"command\":\"lights_off\"}",
        action = { turnOff() }
    )

    private fun testSet(request: String, action: suspend PowerDevice.() -> Unit) = runBlocking {
        //region GIVEN
        var requestMade = false
        val target = TestApiBuilder(
            MockEngine {
                it.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/wled"),
                    actual = it.url,
                    message = "Expected URL to match"
                )
                assertEquals(
                    expected = request,
                    actual = it.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                requestMade = true
                respondOk()
            }
        ) { rotator, httpClient ->
            WledApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        val device = WledApi.PowerDevice(
            owner = target,
        )
        //endregion
        //region WHEN
        device.action()
        //endregion
        //region THEN
        assertTrue(
            actual = requestMade,
            message = "Expected request to be made"
        )
        //endregion
    }
}