package de.crysxd.octoapp.engine.octoprint.api.plugins.bundled

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.mocks.assertPost
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.OctoHttpClientSettings
import de.crysxd.octoapp.engine.octoprint.dto.plugins.applicationkeys.ApplicationKeyResponse
import de.crysxd.octoapp.engine.octoprint.dto.plugins.applicationkeys.ApplicationKeyStatus
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ApplicationKeysApiTest {

    @Test
    fun WHEN_application_keys_is_probed_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/plugin/appkeys/probe"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(status = HttpStatusCode.NoContent, content = "")
            }
        )
        val target = ApplicationKeysApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.probe()
        //endregion
        //region THEN
        assertEquals(
            expected = true,
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_application_keys_is_probed_but_missing_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/plugin/appkeys/probe"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(status = HttpStatusCode.NotFound, content = "")
            }
        )
        val target = ApplicationKeysApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.probe()
        //endregion
        //region THEN
        assertEquals(
            expected = false,
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_application_keys_is_requested_on_1_8_and_newer_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/plugin/appkeys/request"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                assertEquals(
                    expected = "{\"app\":\"OctoApp\"}",
                    actual = request.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                    {
                        "app_token": "7A28145A47014763B795AD830FEC9C71",
                        "auth_dialog": "http://doggy-daycare:5001/plugin/appkeys/auth/7A28145A47014763B795AD830FEC9C71"
                    }
                """.trimIndent()
                )
            }
        )
        val target = ApplicationKeysApi(rotator, http)
        //endregion
        //region WHEN
        val appToken = target.request("OctoApp")
        //endregion
        //region THEN
        assertEquals(
            expected = ApplicationKeyResponse(
                appToken = "7A28145A47014763B795AD830FEC9C71",
                authUrl = "http://doggy-daycare:5001/plugin/appkeys/auth/7A28145A47014763B795AD830FEC9C71"
            ),
            actual = appToken,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_application_keys_is_requested_on_1_7_and_olderTHEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/plugin/appkeys/request"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                assertEquals(
                    expected = "{\"app\":\"OctoApp\"}",
                    actual = request.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                    {
                        "app_token": "7A28145A47014763B795AD830FEC9C71"
                    }
                """.trimIndent()
                )
            }
        )
        val target = ApplicationKeysApi(rotator, http)
        //endregion
        //region WHEN
        val appToken = target.request("OctoApp")
        //endregion
        //region THEN
        assertEquals(
            expected = ApplicationKeyResponse(
                appToken = "7A28145A47014763B795AD830FEC9C71",
                authUrl = null,
            ),
            actual = appToken,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_application_keys_status_is_checked_and_granted_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/plugin/appkeys/request/1234"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                    {
                        "api_key": "7A28145A47014763B795AD830FEC9C71"
                    }
                """.trimIndent()
                )
            }
        )
        val target = ApplicationKeysApi(rotator, http)
        //endregion
        //region WHEN
        val status = target.checkStatus("1234")
        //endregion
        //region THEN
        assertTrue(
            actual = status is ApplicationKeyStatus.Granted,
            message = "Expected status to match"
        )
        assertEquals(
            expected = "7A28145A47014763B795AD830FEC9C71",
            actual = status.apiKey,
            message = "Expected api key to match"
        )
        //endregion
    }

    @Test
    fun WHEN_application_keys_status_is_checked_and_denied_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/plugin/appkeys/request/1234"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(
                    status = HttpStatusCode.NotFound,
                    headers = headersOf("Content-Type", "application/json"),
                    content = ""
                )
            }
        )
        val target = ApplicationKeysApi(rotator, http)
        //endregion
        //region WHEN
        val status = target.checkStatus("1234")
        //endregion
        //region THEN
        assertTrue(
            actual = status is ApplicationKeyStatus.DeniedOrTimedOut,
            message = "Expected status to match"
        )
        //endregion
    }

    @Test
    fun WHEN_application_keys_status_is_checked_and_pending_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/plugin/appkeys/request/1234"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(
                    status = HttpStatusCode.Accepted,
                    content = ""
                )
            }
        )
        val target = ApplicationKeysApi(rotator, http)
        //endregion
        //region WHEN
        val status = target.checkStatus("1234")
        //endregion
        //region THEN
        assertEquals(
            expected = ApplicationKeyStatus.Pending::class,
            actual = status::class,
            message = "Expected status to match"
        )
        //endregion
    }
}