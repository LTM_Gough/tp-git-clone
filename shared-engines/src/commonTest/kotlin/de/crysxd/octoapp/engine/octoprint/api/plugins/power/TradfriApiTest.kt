package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.mocks.TestApiBuilder
import de.crysxd.octoapp.engine.mocks.assertPost
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoSettings
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.respondOk
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TradfriApiTest {

    @Test
    fun WHEN_devices_are_listed_THEN_devices_are_returned() = runBlocking {
        //region GIVEN
        val settings = OctoSettings(
            plugins = OctoSettings.PluginSettingsGroup(
                tradfri = OctoSettings.Tradfri(
                    devices = listOf(
                        OctoSettings.Tradfri.Device(
                            name = "2",
                            id = "12",
                        ),
                        OctoSettings.Tradfri.Device(
                            name = "3",
                            id = "21",
                        )
                    )
                )
            )
        )
        val target = TestApiBuilder(
            MockEngine {
                throw IllegalStateException("No request expected")
            }
        ) { rotator, httpClient ->
            TradfriApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        //endregion
        //region WHEN
        val devices = target.getDevices(settings.map())
        //endregion
        //region THEN
        assertEquals(
            expected = 2,
            actual = devices.size,
            message = "Expected one device"
        )
        assertEquals(
            expected = TradfriApi.PowerDevice(id = "12", displayName = "2", owner = target),
            actual = devices[0],
            message = "Expected device to match"
        )
        assertEquals(
            expected = TradfriApi.PowerDevice(id = "21", displayName = "3", owner = target),
            actual = devices[1],
            message = "Expected device to match"
        )
        //endregion
    }

    @Test
    fun WHEN_device_is_turned_on_THEN_request_is_made() = testSet(
        request = "{\"command\":\"turnOn\",\"dev\":{\"id\":\"ip\",\"name\":\"Label\"}}",
        action = { turnOn() }
    )

    @Test
    fun WHEN_device_is_turned_off_THEN_request_is_made() = testSet(
        request = "{\"command\":\"turnOff\",\"dev\":{\"id\":\"ip\",\"name\":\"Label\"}}",
        action = { turnOff() }
    )

    @Test
    fun WHEN_device_status_is_checked_and_off_THEN_request_is_made() = testGet(
        response = "{\"state\":false}",
        expectedOn = false
    )

    @Test
    fun WHEN_device_status_is_checked_and_on_THEN_request_is_made() = testGet(
        response = "{\"state\":true}",
        expectedOn = true
    )

    @Test
    fun WHEN_device_status_is_checked_and_broken_THEN_request_is_made() = testGet(
        response = "{}",
        expectedOn = false
    )

    private fun testGet(response: String, expectedOn: Boolean) = runBlocking {
        //region GIVEN
        val target = TestApiBuilder(
            MockEngine {
                it.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/ikea_tradfri"),
                    actual = it.url,
                    message = "Expected URL to match"
                )
                assertEquals(
                    expected = "{\"command\":\"checkStatus\",\"dev\":{\"id\":\"ip\",\"name\":\"Label\"}}",
                    actual = it.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                respond(
                    content = response,
                    headers = headersOf("Content-Type" to listOf("application/json")),
                    status = HttpStatusCode.OK
                )
            }
        ) { rotator, httpClient ->
            TradfriApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        val device = TradfriApi.PowerDevice(
            owner = target,
            id = "ip",
            displayName = "Label",
        )
        //endregion
        //region WHEN
        val isOn = device.isOn()
        //endregion
        //region THEN
        assertEquals(
            expected = expectedOn,
            actual = isOn,
            message = "Expected device to be on: $expectedOn"
        )
        //endregion
    }

    private fun testSet(request: String, action: suspend PowerDevice.() -> Unit) = runBlocking {
        //region GIVEN
        var requestMade = false
        val target = TestApiBuilder(
            MockEngine {
                it.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/ikea_tradfri"),
                    actual = it.url,
                    message = "Expected URL to match"
                )
                assertEquals(
                    expected = request,
                    actual = it.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                requestMade = true
                respondOk()
            }
        ) { rotator, httpClient ->
            TradfriApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        val device = TradfriApi.PowerDevice(
            owner = target,
            id = "ip",
            displayName = "Label",
        )
        //endregion
        //region WHEN
        device.action()
        //endregion
        //region THEN
        assertTrue(
            actual = requestMade,
            message = "Expected request to be made"
        )
        //endregion
    }
}