package de.crysxd.octoapp.engine.octoprint.api.plugins.remote

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.mocks.assertPost
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.OctoHttpClientSettings
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.SpaghettiCamFrame
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.SpaghettiDetectiveDataUsage
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.SpaghettiDetectivePrediction
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.SpaghettiDetectiveStatus
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class SpaghettiDetectiveApiTest {

    @Test
    fun WHEN_data_usage_is_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/_tsd_/tunnelusage/"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                         {"total": 232, "monthly_cap": 524288000.0, "reset_in_seconds": 863999.998973}
                    """.trimIndent()
                )
            }
        )
        val target = SpaghettiDetectiveApi(rotator, http)
        //endregion
        //region WHEN
        val tunnel = target.getDataUsage()
        //endregion
        //region THEN
        assertEquals(
            expected = SpaghettiDetectiveDataUsage(
                monthlyCapBytes = 524288000.0,
                resetInSeconds = 863999.998973,
                totalBytes = 232.0,
            ),
            actual = tunnel,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_prediction_is_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/_tsd_/prediction/"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        {
                            "normalized_p": 0.2
                        }
                    """.trimIndent()
                )
            }
        )
        val target = SpaghettiDetectiveApi(rotator, http)
        //endregion
        //region WHEN
        val tunnel = target.getPrintPrediction()
        //endregion
        //region THEN
        assertEquals(
            expected = SpaghettiDetectivePrediction(
                normalized = 0.2f,
            ),
            actual = tunnel,
            message = "Expected response to match"
        )
        //endregion
    }


    @Test
    fun WHEN_webcam_frame_is_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/_tsd_/webcam/3/"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        {
                            "snapshot": "something"
                        }
                    """.trimIndent()
                )
            }
        )
        val target = SpaghettiDetectiveApi(rotator, http)
        //endregion
        //region WHEN
        val tunnel = target.getSpaghettiCamFrame(webcamIndex = 3)
        //endregion
        //region THEN
        assertEquals(
            expected = SpaghettiCamFrame(
                snapshot = "something"
            ),
            actual = tunnel,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_plugin_status_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/obico"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                assertEquals(
                    expected = "{\"command\":\"get_plugin_status\"}",
                    actual = request.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        {
                            "linked_printer": {
                                "is_pro": false,
                                "name": "test",
                                "id": "id"
                            }
                        }
                    """.trimIndent()
                )
            }
        )
        val target = SpaghettiDetectiveApi(rotator, http)
        //endregion
        //region WHEN
        val tunnel = target.getPluginStatus()
        //endregion
        //region THEN
        assertEquals(
            expected = SpaghettiDetectiveStatus(
                linkedPrinter = SpaghettiDetectiveStatus.LinkedPrinter(
                    isPro = false,
                    name = "test",
                    id = "id"
                )
            ),
            actual = tunnel,
            message = "Expected response to match"
        )
        //endregion
    }
}