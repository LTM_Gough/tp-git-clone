package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.models.login.User
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.OctoHttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals


internal class OctoPrintUserApiTest {

    @Test
    fun WHEN_the_user_is_requested_with_invalid_key_on_OctoPrint_1_8_3_and_older_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        {
                            "groups": [
                                "guests"
                            ],
                            "name": null,
                            "permissions": []
                        }
                    """.trimIndent(),
                )
            }
        )
        val target = OctoUserApi(http, rotator)
        //endregion
        //region WHEN
        val user = target.getCurrentUser()
        //endregion
        //region THEN
        assertEquals(
            expected = User(name = "guest", groups = listOf("guests")),
            actual = user,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_the_user_is_requested_with_invalid_key_on_OctoPrint_1_8_4_and_newer_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                respond(
                    status = HttpStatusCode.Forbidden,
                    content = ""
                )
            }
        )
        val target = OctoUserApi(http, rotator)
        //endregion
        //region WHEN
        val user = target.getCurrentUser()
        //endregion
        //region THEN
        assertEquals(
            expected = User(name = "guest", groups = listOf("guests")),
            actual = user,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_the_user_is_requested_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        {
                            "groups": [
                                "users",
                                "admins"
                            ],
                            "name": "admin",
                            "permissions": [
                                "ADMIN",
                                "STATUS",
                                "CONNECTION",
                                "WEBCAM",
                                "SYSTEM"
                            ]
                        }
                    """.trimIndent()
                )
            }
        )
        val target = OctoUserApi(http, rotator)
        //endregion
        //region WHEN
        val user = target.getCurrentUser()
        //endregion
        //region THEN
        assertEquals(
            expected = User(
                name = "admin",
                groups = listOf("users", "admins"),
                permissions = listOf(
                    "ADMIN",
                    "STATUS",
                    "CONNECTION",
                    "WEBCAM",
                    "SYSTEM",
                )
            ),
            actual = user,
            message = "Expected response to match"
        )
        //endregion
    }
}