package de.crysxd.octoapp.engine.octoprint.event

import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

internal actual object OctoWebsocketConfig {
    actual val canUseLongNoMessageTimeout = true
    actual val longNoMessageTimeout: Duration = 30.seconds
}