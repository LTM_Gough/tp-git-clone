package de.crysxd.octoapp.sharedcommon.di

import de.crysxd.octoapp.sharedcommon.Platform
import org.koin.dsl.module

actual open class PlatformModule : BaseModule {
    actual fun providePlatform() = Platform()

    override val koinModule = module {
        single { providePlatform() }
    }
}