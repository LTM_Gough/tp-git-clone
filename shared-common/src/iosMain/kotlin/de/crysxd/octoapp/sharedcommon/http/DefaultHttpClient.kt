package de.crysxd.octoapp.sharedcommon.http

import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.framework.SoftTimeouts
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.darwin.Darwin

internal actual fun SpecificDefaultHttpClient(settings: HttpClientSettings, config: HttpClientConfig<*>.() -> Unit) = HttpClient(Darwin) {
    install(SoftTimeouts.Plugin) {
        timeouts = settings.timeouts
    }

    config()
}