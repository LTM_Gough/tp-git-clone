package de.crysxd.octoapp.sharedcommon.ext

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

actual val Dispatchers.SharedIO: CoroutineDispatcher get() = Dispatchers.IO