package de.crysxd.octoapp.sharedcommon.di

import android.app.Application
import de.crysxd.octoapp.sharedcommon.Platform
import org.koin.dsl.module

actual open class PlatformModule(private val app: Application) : BaseModule {
    actual open fun providePlatform() = Platform(app)

    override val koinModule = module {
        single { providePlatform() }
    }
}