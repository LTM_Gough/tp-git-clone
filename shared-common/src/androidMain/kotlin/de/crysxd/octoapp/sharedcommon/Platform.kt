package de.crysxd.octoapp.sharedcommon

import android.app.Application
import android.content.pm.PackageManager
import android.content.res.Resources
import android.os.Build

actual open class Platform(val context: Application) {

    private val packageInfo by lazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            context.packageManager.getPackageInfo(context.packageName, PackageManager.PackageInfoFlags.of(0))
        } else {
            @Suppress("DEPRECATION")
            context.packageManager.getPackageInfo(context.packageName, 0)
        }
    }

    actual val appVersion by lazy {
        packageInfo.versionName
    }

    actual val appBuild by lazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            packageInfo.longVersionCode.toInt()
        } else {
            @Suppress("DEPRECATION")
            packageInfo.versionCode
        }
    }

    actual val platformVersion = Build.VERSION.SDK_INT.toString()

    actual open val platformName = "Android"

    actual val appId = context.packageName

    actual val cacheDirectory: String by lazy {
        (context.externalCacheDir ?: context.cacheDir).path
    }

    actual val lowPowerDevice by lazy {
        context.packageManager.hasSystemFeature(PackageManager.FEATURE_WATCH)
    }

    actual val debugBuild = BuildConfig.DEBUG

    actual val deviceName = "${Build.BRAND.replaceFirstChar { it.uppercase() }} ${Build.MODEL.replaceFirstChar { it.uppercase() }}"

    actual val deviceModel: String = Build.MODEL

    actual val deviceLanguage: String
        get() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Resources.getSystem().configuration.locales[0]
        } else {
            Resources.getSystem().configuration.locale
        }.language
}