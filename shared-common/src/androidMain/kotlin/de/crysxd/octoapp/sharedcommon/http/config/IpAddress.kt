package de.crysxd.octoapp.sharedcommon.http.config

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import okhttp3.internal.closeQuietly
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.Socket
import kotlin.time.Duration

actual typealias IpAddress = InetAddress

actual fun ipAddressFor(ip: String) = InetAddress.getByName(ip)

actual fun IpAddress.hostNameOrIp(): String = hostAddress ?: hostName

actual suspend fun IpAddress.assertPortOpen(port: Int, timeout: Duration) = withContext(Dispatchers.IO) {
    val socket = Socket()
    try {
        val addr = InetSocketAddress(this@assertPortOpen, port)
        socket.soTimeout = timeout.inWholeMilliseconds.toInt()
        socket.connect(addr, timeout.inWholeMilliseconds.toInt())
    } finally {
        socket.closeQuietly()
    }
}

actual suspend fun IpAddress.assertReachable(timeout: Duration) {
    withTimeout(timeout) {
        withContext(Dispatchers.IO) {
            if (!isReachable(timeout.inWholeMilliseconds.toInt())) {
                throw Exception("Not reachable")
            }
        }
    }
}