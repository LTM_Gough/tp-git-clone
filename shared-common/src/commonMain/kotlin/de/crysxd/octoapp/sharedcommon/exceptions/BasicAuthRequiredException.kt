package de.crysxd.octoapp.sharedcommon.exceptions

import io.ktor.http.Url

class BasicAuthRequiredException(header: String, val userRealm: String = userRealmFromHeader(header), webUrl: Url) : NetworkException(
    technicalMessage = "The server responded with 401, requesting authentication (\"$header\")",
    userFacingMessage = "The server requires authentication. Enter your username and password or check that they are correct.<br><br><small>Server says: $userRealm</small>",
    webUrl = webUrl,
) {
    companion object {
        private fun userRealmFromHeader(header: String?) = header?.let {
            Regex(".*realm=\"([^\"]*)\".*").matchEntire(header)?.groupValues?.getOrNull(1) ?: "no message"
        } ?: "no message"
    }
}
