package de.crysxd.octoapp.sharedcommon.di

import de.crysxd.octoapp.sharedcommon.Platform

expect open class PlatformModule : BaseModule {
    fun providePlatform(): Platform
}


