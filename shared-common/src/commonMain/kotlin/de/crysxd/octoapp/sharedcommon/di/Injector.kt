package de.crysxd.octoapp.sharedcommon.di

import org.koin.core.KoinApplication
import org.koin.core.component.KoinComponent
import org.koin.core.module.Module
import org.koin.dsl.koinApplication

abstract class Injector<T : BaseComponent> {
    private var instance: T? = null

    fun setComponent(t: T) {
        instance = t
    }

    fun get() = requireNotNull(instance) { "Injector is not initialized, use init() first" }

    fun getOrNull() = instance

}

interface BaseModule {
    val koinModule: Module
}

abstract class BaseComponent(
    private val dependencies: List<BaseComponent> = emptyList(),
    private val modules: List<BaseModule>,
) : KoinComponent {
    private val koin: KoinApplication = koinApplication {
        allowOverride(false)

        val allModules = modules + dependencies.flatMap { it.modules }
        val allKoinModules = allModules.map { it.koinModule }
        modules(allKoinModules)

        createEagerInstances()
    }

    override fun getKoin() = koin.koin

}
