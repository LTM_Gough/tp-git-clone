package de.crysxd.octoapp.sharedcommon.http.config

import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

expect class IpAddress

expect fun ipAddressFor(ip: String): IpAddress

expect fun IpAddress.hostNameOrIp(): String

expect suspend fun IpAddress.assertPortOpen(port: Int, timeout: Duration = 3.seconds)

expect suspend fun IpAddress.assertReachable(timeout: Duration = 3.seconds)