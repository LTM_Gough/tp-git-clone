package de.crysxd.octoapp.sharedcommon.exceptions

import de.crysxd.octoapp.sharedcommon.http.framework.redactLoggingString
import io.ktor.http.Url

open class NetworkException(
    open val userFacingMessage: String?,
    val webUrl: Url,
    val originalCause: Throwable? = null,
    val technicalMessage: String = userFacingMessage ?: originalCause?.message ?: "No message",
    val learnMoreLink: String? = null,
) : Exception(
    webUrl.redactLoggingString(technicalMessage),
    originalCause?.let { ProxyException.create(it, webUrl) }
)