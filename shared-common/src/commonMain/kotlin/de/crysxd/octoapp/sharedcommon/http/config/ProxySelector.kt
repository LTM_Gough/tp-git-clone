package de.crysxd.octoapp.sharedcommon.http.config

import io.ktor.http.Url

interface ProxySelector {
    sealed class Proxy {
        object NoProxy : Proxy()
        data class SocksProxy(val host: String, val port: Int) : Proxy()
    }

    fun selectProxy(url: Url): List<Proxy>
    fun connectFailed(url: Url, exception: Throwable)

    companion object {
        val Noop = object : ProxySelector {
            override fun selectProxy(url: Url) = listOf(Proxy.NoProxy)
            override fun connectFailed(url: Url, exception: Throwable) = Unit
        }
    }
}