package de.crysxd.octoapp.sharedcommon.exceptions

import io.ktor.http.Url


class IllegalBasicAuthConfigurationException(url: Url) : NetworkException(
    webUrl = url,
    technicalMessage = "Illegal basic auth setup in $url",
    userFacingMessage = "<b>$url</b>\n\ncontains a illegal Basic Auth setup. Please make sure to follow the scheme \n\nhttp(s)://<b>username:password</b>@host"
)