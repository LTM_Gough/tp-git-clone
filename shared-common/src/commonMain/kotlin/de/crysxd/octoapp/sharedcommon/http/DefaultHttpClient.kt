package de.crysxd.octoapp.sharedcommon.http

import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.LogLevel
import de.crysxd.octoapp.sharedcommon.http.config.Timeouts
import de.crysxd.octoapp.sharedcommon.http.framework.installBasicAuthInterceptorPlugin
import de.crysxd.octoapp.sharedcommon.http.logging.StyledLogger
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.plugins.HttpTimeout
import io.ktor.client.plugins.UserAgent
import io.ktor.client.plugins.compression.ContentEncoding
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.setBody
import io.ktor.http.ContentType
import io.ktor.http.contentType

fun DefaultHttpClient(settings: HttpClientSettings, logTag: String, engine: HttpClientEngine? = null, config: HttpClientConfig<*>.() -> Unit = {}): HttpClient {
    val sharedConfig: HttpClientConfig<*>.() -> Unit = {
        expectSuccess = false
        followRedirects = true

        installLogging(settings.logLevel, logTag)
        installUserAgent()
        installContentEncoding()
        installTimeouts(settings.timeouts)
        config()
    }

    return if (engine != null) {
        HttpClient(engine, sharedConfig)
    } else {
        SpecificDefaultHttpClient(settings, sharedConfig)
    }.apply {
        installBasicAuthInterceptorPlugin()
    }
}


inline fun <reified T> HttpRequestBuilder.setJsonBody(body: T) {
    setBody(body)
    contentType(ContentType.Application.Json)
}

internal fun HttpClientConfig<*>.installTimeouts(timeouts: Timeouts) {
    install(HttpTimeout) {
        connectTimeoutMillis = timeouts.connectionTimeout.inWholeMilliseconds
        socketTimeoutMillis = timeouts.connectionTimeout.inWholeMilliseconds
    }
}

internal fun HttpClientConfig<*>.installContentEncoding() {
    install(ContentEncoding) {
        deflate(0.9f)
        gzip(1.0f)
    }
}

internal fun HttpClientConfig<*>.installLogging(logLevel: LogLevel, logTag: String) {
    install(Logging) {
        level = logLevel.ktorLevel
        logger = StyledLogger(logTag)
    }
}

internal fun HttpClientConfig<*>.installUserAgent() {
    SharedCommonInjector.getOrNull()?.platform?.let { platform ->
        install(UserAgent) {
            agent = "OctoApp ${platform.appVersion} (${platform.platformName} ${platform.platformVersion})"
        }
    }
}

internal expect fun SpecificDefaultHttpClient(settings: HttpClientSettings, config: HttpClientConfig<*>.() -> Unit): HttpClient
