package de.crysxd.octoapp.sharedcommon.http.framework

import de.crysxd.octoapp.sharedcommon.exceptions.IllegalBasicAuthConfigurationException
import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.http.appendEncodedPathSegments
import io.ktor.http.appendPathSegments
import io.ktor.http.set
import io.ktor.util.encodeBase64
import io.ktor.utils.io.charsets.Charsets
import io.ktor.utils.io.core.String
import io.ktor.utils.io.core.toByteArray
import kotlin.math.absoluteValue

const val UPNP_ADDRESS_PREFIX = "octoprint-via-upnp---"
private val IpClassARegex = Regex("^192\\.168\\.\\d+\\.\\d+$")
private val IpClassBRegex = Regex("((^172\\.1[6-9]\\.)|(^172\\.2[0-9]\\.)|(^172\\.3[0-1]\\.))\\d+\\.\\d+$")
private val IpClassCRegex = Regex("^10\\.\\d+\\.\\d+\\.\\d+$")

fun Url.withoutBasicAuth() = withBasicAuth(null, null)


fun Url.withoutQuery() = URLBuilder(this).apply {
    set {
        parameters.clear()
    }
}.build()


fun Url.withBasicAuth(user: String?, password: String?) = URLBuilder(this).apply {
    set {
        this.user = user?.takeIf { it.isNotBlank() }
        this.password = password?.takeIf { it.isNotBlank() }
    }
}.build()

fun Url.withHost(host: String) = URLBuilder(this).apply {
    set {
        this.host = host
    }
}.build()

fun Url.forLogging() = URLBuilder(this).apply {
    set {
        user = if (user.isNullOrEmpty()) null else "basicAuthUser"
        password = if (password.isNullOrEmpty()) null else "basicAuthPassword"
        host = redactedHost

    }
}.build()

fun Url.redactLoggingString(log: String) = log.replaceIfNotEmpty(host, redactedHost)
    .replaceIfNotEmpty(password, "basicAuthPassword")
    .replaceIfNotEmpty(user, "basicAuthUser")
    .replaceIfNotEmpty(encodedPassword, "basicAuthPassword")
    .replaceIfNotEmpty(encodedUser, "basicAuthUser")

val Url.redactedHost
    get() = when {
        isLocalNetwork() -> host

        // OctoEverywhere and ngrok
        host.endsWith(".octoeverywhere.com") -> "redacted-${hashedHost}.octoeverywhere.com"
        host.endsWith(".ngrok.com") -> "redacted-${hashedHost}.ngrok.com"
        host.endsWith(".tunnels.app.thespaghettidetective.com") -> "redacted-${hashedHost}.tunnels.app.thespaghettidetective.com"
        host.endsWith(".tunnels.app.obico.io") -> "redacted-${hashedHost}.tunnels.app.obico.io"

        // All other cases. Redact.
        else -> "redacted-host-${hashedHost}"
    }

// Host is local IP address (class A, B and C) or has a specific host suffix/prefix
fun Url.isLocalNetwork() = IpClassARegex.containsMatchIn(host) ||
        IpClassBRegex.containsMatchIn(host) ||
        IpClassCRegex.containsMatchIn(host) ||
        host.endsWith(".local") ||
        host.endsWith(".home") ||
        host.startsWith(UPNP_ADDRESS_PREFIX)

private
val Url.hashedHost
    get() =
        host.hashCode().absoluteValue.toString(radix = 16)

fun Url.extractAndRemoveBasicAuth(): Pair<Url, String?> {
    val header = if (!user.isNullOrEmpty()) {
        try {
            val usernameAndPassword = listOfNotNull(user, password).joinToString(":")
            val encoded = String(usernameAndPassword.toByteArray(charset = Charsets.ISO_8859_1)).encodeBase64()
            "Basic $encoded"
        } catch (e: Exception) {
            throw IllegalBasicAuthConfigurationException(this)
        }
    } else {
        null
    }

    return withoutBasicAuth() to header
}

private fun String.replaceIfNotEmpty(needle: String?, replacement: String) = if (needle.isNullOrEmpty()) {
    this
} else {
    replace(needle, replacement)
}

fun Url.resolve(path: String, alreadyEncoded: Boolean = false): Url {
    val sections = path.split("?")
    val pathSections = sections[0].split("/")
    val querySection = sections.getOrNull(1)

    return URLBuilder(this).apply {
        set {
            if (alreadyEncoded) {
                appendEncodedPathSegments(pathSections)
            } else {
                appendPathSegments(pathSections)
            }

            querySection?.split("&")
                ?.map { it.split("=") }
                ?.forEach {
                    parameters.appendMissing(it.first(), it.drop(1))
                }
        }
    }.build()
}

fun String.toUrl() = Url(this)

fun String.toUrlOrNull() = try {
    Url(this)
} catch (e: Exception) {
    null
}
