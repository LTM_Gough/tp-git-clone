package de.crysxd.octoapp.sharedcommon.utils

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

fun Instant.isInFuture(): Boolean = this > Clock.System.now()
fun Instant.isInPast(): Boolean = Clock.System.now() > this