package de.crysxd.octoapp.sharedcommon.http.config

expect class KeyStore

interface KeyStoreProvider {
    fun loadKeyStore(): KeyStore?
    fun getWeakVerificationForHosts(): List<String>
}