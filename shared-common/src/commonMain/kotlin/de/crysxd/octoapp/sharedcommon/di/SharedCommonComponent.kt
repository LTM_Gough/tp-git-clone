package de.crysxd.octoapp.sharedcommon.di

import de.crysxd.octoapp.sharedcommon.Platform
import kotlinx.serialization.json.Json
import org.koin.core.component.inject


class SharedCommonComponent(
    platformModule: PlatformModule,
    serializationModule: SerializationModule = SerializationModule(),
) : BaseComponent(
    modules = listOf(
        platformModule,
        serializationModule
    ),
) {
    val platform: Platform by inject()
    val json: Json by inject()
}
