package de.crysxd.octoapp.sharedcommon.http.config

import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock
import kotlin.test.Test
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue
import kotlin.time.Duration.Companion.seconds

class IpAddressTest {

    @Test
    fun WHEN_a_unreachable_ip_is_pinged_THEN_exception_is_thrown() = runBlocking {
        //region GIVEN
        val ip = ipAddressFor("190.168.1.105")
        val timeout = 3.seconds
        //endregion
        //region WHEN
        val start = Clock.System.now()
        val e = try {
            ip.assertReachable(timeout = timeout)
            null
        } catch (e: Exception) {
            e
        }
        val end = Clock.System.now()
        //endregion
        //region THEN
        assertNotNull(
            actual = e,
            message = "Expected exception"
        )
        assertTrue(
            actual = (end - start) < (timeout * 1.1),
            message = "Expected timeout to be honored. timeout=$timeout actual${end - start}"
        )
        e.printStackTrace()
        //endregion
    }

    @Test
    fun WHEN_a_reachable_ip_is_pinged_THEN_test_passes() = runBlocking {
        //region GIVEN
        val ip = ipAddressFor("127.0.0.1")
        val timeout = 1.seconds
        //endregion
        //region WHEN
        val start = Clock.System.now()
        val e = try {
            ip.assertReachable(timeout = timeout)
            null
        } catch (e: Exception) {
            e
        }
        val end = Clock.System.now()
        //endregion
        //region THEN
        assertNull(
            actual = e,
            message = "Expected exception"
        )
        assertTrue(
            actual = (end - start) < timeout,
            message = "Expected timeout to be honored. timeout=$timeout actual${end - start}"
        )
        //endregion
    }

    @Test
    fun WHEN_a_open_port_is_tested_THEN_test_passes() = runBlocking {
        //region GIVEN
        val ip = ipAddressFor("gstatic.com")
        val timeout = 1.seconds
        //endregion
        //region WHEN
        val start = Clock.System.now()
        val e = try {
            ip.assertPortOpen(port = 80, timeout = timeout)
            null
        } catch (e: Exception) {
            e
        }
        val end = Clock.System.now()
        //endregion
        //region THEN
        assertNull(
            actual = e,
            message = "Expected no exception"
        )
        assertTrue(
            actual = (end - start) < (timeout * 1.1),
            message = "Expected timeout to be honored. timeout=$timeout actual${end - start}"
        )
        //endregion
    }

    @Test
    fun WHEN_a_closed_port_is_tested_THEN_test_passes() = runBlocking {
        //region GIVEN
        val ip = ipAddressFor("gstatic.com")
        val timeout = 3.seconds
        //endregion
        //region WHEN
        val start = Clock.System.now()
        val e = try {
            ip.assertPortOpen(port = 5000, timeout = timeout)
            null
        } catch (e: Exception) {
            e
        }
        val end = Clock.System.now()
        //endregion
        //region THEN
        assertNotNull(
            actual = e,
            message = "Expected an exception"
        )
        assertTrue(
            actual = (end - start) < (timeout * 1.1),
            message = "Expected timeout to be honored. timeout=$timeout actual${end - start}"
        )
        e.printStackTrace()
        //endregion
    }
}