package de.crysxd.octoapp.sharedcommon.http.framework

import io.ktor.client.HttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respondOk
import io.ktor.client.request.get
import io.ktor.http.Url
import io.ktor.http.takeFrom
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class BasicAuthInterceptorPluginTest {

    @Test
    fun WHEN_a_url_with_credentials_is_requested_THEN_auth_header_is_set() = runBlocking {
        //region GIVEN
        val http = HttpClient(MockEngine {
            assertEquals(
                expected = "Basic dXNlcjpwYXNzd29yZA==",
                actual = it.headers["Authorization"],
                message = "Expected basic auth to be given"
            )
            assertEquals(
                expected = Url("http://host.com"),
                actual = it.url,
                message = "Expected basic auth to be removed from URL"
            )
            respondOk()
        })
        http.installBasicAuthInterceptorPlugin()
        //endregion
        //region WHEN
        val response = http.get {
            url {
                takeFrom(Url("http://user:password@host.com"))
            }
        }
        //endregion
        //region THEN
        assertEquals(
            expected = 200,
            actual = response.status.value
        )
        //endregion
    }

    @Test
    fun WHEN_a_url_without_credentials_is_requested_THEN_nothing_is_done() = runBlocking {
        //region GIVEN
        val http = HttpClient(MockEngine {
            assertEquals(
                expected = null,
                actual = it.headers["Authorization"],
                message = "Expected basic auth to be missing"
            )
            respondOk()
        })
        http.installBasicAuthInterceptorPlugin()
        //endregion
        //region WHEN
        val response = http.get {
            url {
                takeFrom(Url("http://host.com"))
            }
        }
        //endregion
        //region THEN
        assertEquals(
            expected = 200,
            actual = response.status.value
        )
        //endregion
    }
}