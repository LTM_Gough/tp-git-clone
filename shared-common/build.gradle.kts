plugins {
    kotlin("multiplatform")
    id("com.android.library")
    id("kotlin-parcelize")
    kotlin("plugin.serialization") version "1.7.0"
}

kotlin {
    android()

    listOf(
        iosX64(),
        iosArm64(),
        iosSimulatorArm64()
    ).forEach {
        it.binaries.framework {
            baseName = "shared-common"
        }
    }

    sourceSets {
        val ktorVersion = project.rootProject.ext.get("ktor_version") as String

        val commonMain by getting {
            val ktorVersion = project.rootProject.ext.get("ktor_version") as String

            dependencies {
                api("io.ktor:ktor-client-core:$ktorVersion")
                api("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.0")
                api("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
                api("org.jetbrains.kotlinx:kotlinx-datetime:0.4.0")
                api("io.github.aakira:napier:2.6.1")
                api("com.benasher44:uuid:0.5.0")
                api("io.insert-koin:koin-core:3.2.2")
                api("com.squareup.okio:okio:3.2.0")

                api("io.ktor:ktor-client-core:$ktorVersion")
                api("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
                api("io.ktor:ktor-client-content-negotiation:$ktorVersion")
                api("io.ktor:ktor-client-logging:$ktorVersion")
                api("io.ktor:ktor-client-auth:$ktorVersion")
                api("io.ktor:ktor-client-encoding:$ktorVersion")
                api("io.ktor:ktor-client-websockets:$ktorVersion")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
                api("io.ktor:ktor-client-mock:$ktorVersion")
            }
        }
        val androidMain by getting {
            dependencies {
                api("androidx.core:core-ktx:1.9.0")
                api("io.ktor:ktor-client-okhttp:$ktorVersion")
                api("com.squareup.okhttp3:logging-interceptor:4.9.2")
            }
        }
        val androidTest by getting
        val iosX64Main by getting
        val iosArm64Main by getting
        val iosSimulatorArm64Main by getting
        val iosMain by creating {
            dependsOn(commonMain)
            iosX64Main.dependsOn(this)
            iosArm64Main.dependsOn(this)
            iosSimulatorArm64Main.dependsOn(this)

            dependencies {
                api("io.ktor:ktor-client-darwin:$ktorVersion")
            }
        }
        val iosX64Test by getting
        val iosArm64Test by getting
        val iosSimulatorArm64Test by getting
        val iosTest by creating {
            dependsOn(commonTest)
            iosX64Test.dependsOn(this)
            iosArm64Test.dependsOn(this)
            iosSimulatorArm64Test.dependsOn(this)
        }
    }
}

android {
    namespace = "de.crysxd.octoapp.sharedcommon"
    compileSdk = project.rootProject.ext.get("target_sdk") as Int
    defaultConfig {
        minSdk = project.rootProject.ext.get("min_sdk") as Int
        targetSdk = project.rootProject.ext.get("target_sdk") as Int
    }
}