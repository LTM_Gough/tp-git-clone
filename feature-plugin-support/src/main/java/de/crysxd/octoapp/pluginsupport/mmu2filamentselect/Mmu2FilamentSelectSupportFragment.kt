package de.crysxd.octoapp.pluginsupport.mmu2filamentselect

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import de.crysxd.baseui.menu.base.MenuBottomSheetFragment
import de.crysxd.octoapp.pluginsupport.di.injectViewModel
import timber.log.Timber

class Mmu2FilamentSelectSupportFragment : Fragment() {

    private val viewModel by injectViewModel<Mmu2FilamentSelectSupportViewModel>()
    private val bottomSheetTag = "mmu2-filament-select-menu"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenCreated {
            viewModel.events.collect {
                Timber.i("Mmu2FilamentSelect event: $it")
                val currentFragment = childFragmentManager.findFragmentByTag(bottomSheetTag) as? MenuBottomSheetFragment

                when (it) {
                    Mmu2FilamentSelectSupportViewModel.Event.CloseMenu -> currentFragment?.dismiss()

                    is Mmu2FilamentSelectSupportViewModel.Event.ShowMenu -> if (currentFragment == null && lifecycle.currentState > Lifecycle.State.CREATED) {
                        MenuBottomSheetFragment.createForMenu(Mmu2FilamentSelectMenu(it.labelSource)).show(childFragmentManager, bottomSheetTag)
                    }
                }
            }
        }.invokeOnCompletion {
            Timber.i("Mmu2FilamentSelect event completed")
        }
    }
}