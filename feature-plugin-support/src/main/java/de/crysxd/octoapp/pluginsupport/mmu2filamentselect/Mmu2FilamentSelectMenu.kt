package de.crysxd.octoapp.pluginsupport.mmu2filamentselect

import android.content.Context
import android.graphics.Color
import de.crysxd.baseui.R
import de.crysxd.baseui.menu.base.Menu
import de.crysxd.baseui.menu.base.MenuHost
import de.crysxd.baseui.menu.base.MenuItem
import de.crysxd.baseui.menu.base.MenuItemStyle
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.SelectMmu2FilamentUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import kotlinx.coroutines.launch
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import timber.log.Timber

@Parcelize
class Mmu2FilamentSelectMenu(val source: Settings.Mmu2FilamentSelect.LabelSource) : Menu {

    @IgnoredOnParcel
    private var choiceMade = false

    override suspend fun getMenuItem(): List<MenuItem> {
        val settings = BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()?.settings?.plugins?.mmu2FilamentSelect
        return when (settings?.labelSource ?: Settings.Mmu2FilamentSelect.LabelSource.Manual) {
            Settings.Mmu2FilamentSelect.LabelSource.Manual -> manualMaterialSelection(settings)
            Settings.Mmu2FilamentSelect.LabelSource.FilamentManager -> filamentManagerSelection()
            Settings.Mmu2FilamentSelect.LabelSource.SpoolManager -> spoolManagerSelection()
        }
    }

    private suspend fun makeChoice(choice: SelectMmu2FilamentUseCase.Params) {
        choiceMade = true
        BaseInjector.get().selectMmu2FilamentUseCase().execute(choice)
    }

    override fun onDestroy() {
        super.onDestroy()

        // If no choice was made, we attempt to cancel.
        // This means if the user made a choice somewhere else this call will fail
        if (!choiceMade) {
            AppScope.launch {
                try {
                    Timber.i("No choice made, cancelling...")
                    makeChoice(SelectMmu2FilamentUseCase.Params.Cancel)
                } catch (e: Exception) {
                    Timber.w("Failed to cancel MMU2 selection: ${e::class.simpleName}: ${e.message}")
                }
            }
        }
    }

    override suspend fun getTitle(context: Context) = context.getString(R.string.material_menu___title_select_material)
    override suspend fun getSubtitle(context: Context) = context.getString(R.string.material_menu___subtitle_select_material)
    private fun manualMaterialSelection(settings: Settings.Mmu2FilamentSelect?) = listOf(
        SelectMaterialMenuItem(choice = SelectMmu2FilamentUseCase.Params.Tool0, displayName = settings?.filament1, menu = this),
        SelectMaterialMenuItem(choice = SelectMmu2FilamentUseCase.Params.Tool1, displayName = settings?.filament2, menu = this),
        SelectMaterialMenuItem(choice = SelectMmu2FilamentUseCase.Params.Tool2, displayName = settings?.filament3, menu = this),
        SelectMaterialMenuItem(choice = SelectMmu2FilamentUseCase.Params.Tool3, displayName = settings?.filament4, menu = this),
        SelectMaterialMenuItem(choice = SelectMmu2FilamentUseCase.Params.Tool4, displayName = settings?.filament5, menu = this),
        CancelMenuItem(this),
    )

    private suspend fun materialPluginSelection(pluginId: String): List<MenuItem> {
        val materials = BaseInjector.get().getMaterialsUseCase().execute(Unit).filter { it.id.providerId.equals(pluginId, ignoreCase = true) }
        val tools = (0..4).map { toolIndex ->
            toolIndex to materials.firstOrNull { it.activeToolIndex == toolIndex }
        }.map {
            val m = it.second
            SelectMaterialMenuItem(
                choice = SelectMmu2FilamentUseCase.Params.forToolIndex(it.first),
                displayName = m?.displayName,
                weightGrams = m?.weightGrams,
                colorHex = m?.color,
                menu = this
            )
        }

        return listOf(
            tools,
            listOf(CancelMenuItem(this)),
        ).flatten()
    }

    private suspend fun filamentManagerSelection() =
        materialPluginSelection(pluginId = OctoPlugins.FilamentManager)

    private suspend fun spoolManagerSelection() =
        materialPluginSelection(pluginId = OctoPlugins.SpoolManager)

    private class SelectMaterialMenuItem(
        private val menu: Mmu2FilamentSelectMenu,
        private val choice: SelectMmu2FilamentUseCase.Params,
        private val displayName: String?,
        private val weightGrams: Float? = null,
        private val colorHex: String? = null,
    ) : MenuItem {
        override val itemId = "mmuMaterials/${choice.choice}"
        override var groupId = ""
        override val order = choice.choice
        override val style = MenuItemStyle.Printer
        override val icon = R.drawable.ic_round_layers_24
        override fun getTitle(context: Context) = choice.toolIndexLabel.toString() + (displayName?.takeIf { it.isNotBlank() }?.let { ": $it" } ?: "")
        override fun getRightDetail(context: Context) = weightGrams?.let { context.getString(R.string.x_grams, it.toInt()) }
        override fun getIconColorOverwrite(context: Context) = try {
            colorHex?.let { Color.parseColor(it) }
        } catch (e: Exception) {
            Timber.e(e)
            null
        }

        override suspend fun onClicked(host: MenuHost?) {
            menu.makeChoice(choice)
        }
    }

    private class CancelMenuItem(
        private val menu: Mmu2FilamentSelectMenu,
    ) : MenuItem {
        private val choice = SelectMmu2FilamentUseCase.Params.Cancel
        override val itemId = "mmuMaterials/${choice.choice}"
        override var groupId = "cancel"
        override val order = choice.choice
        override val style = MenuItemStyle.Printer
        override val icon = R.drawable.ic_round_stop_24
        override fun getTitle(context: Context) = context.getString(R.string.cancel_print)

        override suspend fun onClicked(host: MenuHost?) {
            menu.makeChoice(choice)
        }
    }
}