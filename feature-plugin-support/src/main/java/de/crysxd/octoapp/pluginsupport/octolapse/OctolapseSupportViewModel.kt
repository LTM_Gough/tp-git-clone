package de.crysxd.octoapp.pluginsupport.octolapse

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.OctoActivity
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.message.OctolapsePluginMessage
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octolapse.OctolapseSnapshotPlanPreview
import de.crysxd.octoapp.pluginsupport.R
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import timber.log.Timber
import kotlin.time.Duration.Companion.seconds

class OctolapseSupportViewModel(
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPreferences: OctoPreferences,
) : ViewModel() {

    private val mutableEvents = MutableSharedFlow<Event>(1)
    val events = mutableEvents.map { it }

    init {
        observeConnection()
        observeEvents()
    }

    private fun observeEvents() = viewModelScope.launch {
        octoPrintProvider.passiveCachedMessageFlow("octolapse-messages", OctolapsePluginMessage::class)
            .filterNotNull()
            .onEach {
                it.errors?.firstOrNull()?.let { error ->
                    mutableEvents.tryEmit(
                        Event.ShowMessage(
                            OctoActivity.Message.DialogMessage(
                                title = { getString(R.string.octolapse___error___title) },
                                text = { listOf(error.name?.let { "<b>$it</b>" }, error.description).joinToString("<br><br>").toHtml() }
                            )
                        )
                    )
                }

                if (it.type == OctolapsePluginMessage.Type.GcodePreProcessingStart) {
                    mutableEvents.tryEmit(Event.OpenBottomSheet(null))
                }
            }.saveCollect()
    }

    private fun observeConnection() = viewModelScope.launch {
        octoPrintProvider.passiveConnectionEventFlow("octolapse-conneciton").onEach {
            try {
                when {
                    !octoPreferences.activePluginIntegrations -> Timber.i("Connected but skipping Octolapse state check, no active plugin integrations allowed")

                    octoPrintRepository.getActiveInstanceSnapshot()?.hasPlugin(OctoPlugins.Octolapse) != true -> Timber.i("Octolapse not installed")

                    else -> {
                        // Delay slightly so other more important requests go first
                        val delay = 5.seconds
                        Timber.i("Connected, checking current Octolapse state after $delay")
                        delay(delay)
                        octoPrintProvider.octoPrint().asOctoPrint().octolapseApi.getSettingsAndState().snapshotPlanPreview?.let {
                            mutableEvents.tryEmit(Event.OpenBottomSheet(it))
                        }
                    }
                }
            } catch (e: Exception) {
                // Nice try, we don't care. This is considered optional
                Timber.e(e, "Octolapse  state check failed")
            }
        }.saveCollect()
    }

    private suspend fun Flow<*>.saveCollect() = retry {
        delay(1000)
        Timber.e(it)
        true
    }.collect()

    sealed class Event {
        var handled = false

        data class ShowMessage(val message: OctoActivity.Message.DialogMessage) : Event()
        data class OpenBottomSheet(val plan: OctolapseSnapshotPlanPreview?) : Event()
    }
}