package de.crysxd.octoapp.pluginsupport.mmu2filamentselect

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.models.settings.Settings.Mmu2FilamentSelect.LabelSource
import de.crysxd.octoapp.engine.models.settings.Settings.Mmu2FilamentSelect.LabelSource.Manual
import de.crysxd.octoapp.engine.octoprint.dto.message.CompanionPluginMessage
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn
import timber.log.Timber

class Mmu2FilamentSelectSupportViewModel(
    octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
) : ViewModel() {

    val events = octoPrintProvider.passiveCachedMessageFlow("mmu2-filament-select-support", CompanionPluginMessage::class)
        .map { it?.mmuSelectionActive == true }
        .distinctUntilChanged()
        .map {
            Timber.i("MMU change: $it")
            val source = octoPrintRepository.getActiveInstanceSnapshot()?.settings?.plugins?.mmu2FilamentSelect?.labelSource ?: Manual
            when (it) {
                true -> Event.ShowMenu(source)
                false -> Event.CloseMenu
            }
        }.retry {
            Timber.e(it)
            delay(1000)
            true
        }.shareIn(viewModelScope, started = SharingStarted.Lazily, replay = 1)

    sealed class Event {
        data class ShowMenu(val labelSource: LabelSource) : Event()
        object CloseMenu : Event()
    }
}