package de.crysxd.octoapp.pluginsupport.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.di.ViewModelFactory
import de.crysxd.octoapp.base.di.ViewModelKey
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.GenerateRenderStyleUseCase
import de.crysxd.octoapp.base.usecase.UpdateNgrokTunnelUseCase
import de.crysxd.octoapp.pluginsupport.mmu2filamentselect.Mmu2FilamentSelectSupportViewModel
import de.crysxd.octoapp.pluginsupport.ngrok.NgrokSupportViewModel
import de.crysxd.octoapp.pluginsupport.octolapse.OctolapseBottomSheetViewModel
import de.crysxd.octoapp.pluginsupport.octolapse.OctolapseSupportViewModel
import javax.inject.Provider

@Module
open class ViewModelModule {

    @Provides
    fun bindViewModelFactory(creators: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>): ViewModelProvider.Factory =
        ViewModelFactory(creators)

    @Provides
    @IntoMap
    @ViewModelKey(OctolapseBottomSheetViewModel::class)
    open fun provideOctolapseBottomSheetViewModel(
        octoPrintProvider: OctoPrintProvider,
        generateRenderStyleUseCase: GenerateRenderStyleUseCase,
    ): ViewModel = OctolapseBottomSheetViewModel(
        octoPrintProvider = octoPrintProvider,
        generateRenderStyleUseCase = generateRenderStyleUseCase,
    )

    @Provides
    @IntoMap
    @ViewModelKey(OctolapseSupportViewModel::class)
    open fun provideOctolapseSupportViewModel(
        octoPrintProvider: OctoPrintProvider,
        octoPrintRepository: OctoPrintRepository,
        octoPreferences: OctoPreferences,
    ): ViewModel = OctolapseSupportViewModel(
        octoPrintProvider = octoPrintProvider,
        octoPrintRepository = octoPrintRepository,
        octoPreferences = octoPreferences,
    )

    @Provides
    @IntoMap
    @ViewModelKey(NgrokSupportViewModel::class)
    open fun provideNgrokSupportViewModel(
        octoPrintProvider: OctoPrintProvider,
        octoPrintRepository: OctoPrintRepository,
        updateNgrokTunnelUseCase: UpdateNgrokTunnelUseCase,
    ): ViewModel = NgrokSupportViewModel(
        octoPrintProvider = octoPrintProvider,
        octoPrintRepository = octoPrintRepository,
        updateNgrokTunnelUseCase = updateNgrokTunnelUseCase,
    )

    @Provides
    @IntoMap
    @ViewModelKey(Mmu2FilamentSelectSupportViewModel::class)
    open fun provideMmu2FilamentSelectSupportViewModel(
        octoPrintProvider: OctoPrintProvider,
        octoPrintRepository: OctoPrintRepository,
    ): ViewModel = Mmu2FilamentSelectSupportViewModel(
        octoPrintProvider = octoPrintProvider,
        octoPrintRepository = octoPrintRepository,
    )
}