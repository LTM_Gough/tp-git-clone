package de.crysxd.octoapp.pluginsupport.octolapse

import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.style.ImageSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.transition.TransitionManager
import de.crysxd.baseui.BaseBottomSheetDialogFragment
import de.crysxd.baseui.utils.InstantAutoTransition
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octolapse.OctolapseSnapshotPlanPreview
import de.crysxd.octoapp.pluginsupport.R
import de.crysxd.octoapp.pluginsupport.databinding.OctolapseBottomSheetBinding
import de.crysxd.octoapp.pluginsupport.di.injectViewModel

class OctolapseBottomSheetFragment : BaseBottomSheetDialogFragment() {

    companion object {
        private const val ARG_SNAPSHOT_PLAN = "snapshot_plan"
        fun create(snapshotPlan: OctolapseSnapshotPlanPreview?) = OctolapseBottomSheetFragment().also {
            it.arguments = bundleOf(ARG_SNAPSHOT_PLAN to snapshotPlan)
        }
    }

    override val viewModel by injectViewModel<OctolapseBottomSheetViewModel>()
    private lateinit var binding: OctolapseBottomSheetBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        OctolapseBottomSheetBinding.inflate(inflater, container, false).also {
            binding = it
        }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (arguments?.getParcelable<OctolapseSnapshotPlanPreview>(ARG_SNAPSHOT_PLAN))?.let {
            viewModel.consumePlan(it)
        }

        binding.buttonAccept.setOnClickListener { viewModel.accept() }
        binding.buttonCancel.setOnClickListener { dismiss() }

        showViews(true)
    }

    override fun onStart() {
        super.onStart()
        lifecycleScope.launchWhenStarted {
            viewModel.events.collect {
                TransitionManager.beginDelayedTransition(requireView().rootView as ViewGroup, InstantAutoTransition(fadeText = true))

                when (it) {
                    OctolapseState.Close -> dismiss()
                    is OctolapseState.Loading -> showViews(true, it.title)
                    is OctolapseState.SnapshotPlan -> showPreview(it)
                }

                forceResizeBottomSheet()
            }
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        viewModel.onDismiss()
    }

    private fun showViews(loading: Boolean, @StringRes loadingTitle: Int? = 0) {
        binding.progress.isVisible = loading
        binding.previewLayout.isVisible = !loading
        binding.previewSeekbar.isVisible = !loading
        binding.buttonAccept.isVisible = !loading
        binding.buttonCancel.isVisible = !loading
        binding.title.text = if (loading) {
            loadingTitle?.takeIf { it != 0 }?.let { getString(it) }
        } else {
            SpannableStringBuilder().also {
                it.append(getString(R.string.octolapse___snapshot_plan___title_ready))
                it.append("   ")
                it.setSpan(
                    ImageSpan(
                        requireContext(),
                        R.drawable.ic_beta,
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) ImageSpan.ALIGN_CENTER else ImageSpan.ALIGN_BASELINE
                    ), it.length - 1, it.length,
                    0
                )
            }
        }
        binding.title.isVisible = binding.title.text.isNotBlank()
    }

    private fun showPreview(snapshotPlan: OctolapseState.SnapshotPlan) {
        showViews(false, 0)
        snapshotPlan.layers ?: return
        binding.previewSeekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit
            override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                snapshotPlan.layers.getOrNull(progress)?.let(::showLayer)
            }
        })
        binding.previewSeekbar.max = snapshotPlan.layers.size - 1
        binding.previewSeekbar.progress = 0
        snapshotPlan.layers.firstOrNull()?.let {
            showLayer(it)
        }
    }

    private fun showLayer(layer: OctolapseState.Layer) {
        binding.preview.renderParams = layer.renderParams
        binding.layer.text = getString(R.string.x_of_y, layer.layerNumber + 1, layer.layerCount)
        binding.snapshotPosition.text = getString(R.string.octolapse___snapshot_plan___snapshot_at_x_y, layer.snapshotPosition?.x, layer.snapshotPosition?.y)
    }
}