package de.crysxd.octoapp.pluginsupport.ngrok

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import de.crysxd.octoapp.pluginsupport.di.injectViewModel
import kotlinx.coroutines.flow.collect
import timber.log.Timber

class NgrokSupportFragment : Fragment() {

    private val viewModel by injectViewModel<NgrokSupportViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenCreated {
            Timber.i("Starting ngrok support")
            viewModel.events.collect()
        }
    }
}