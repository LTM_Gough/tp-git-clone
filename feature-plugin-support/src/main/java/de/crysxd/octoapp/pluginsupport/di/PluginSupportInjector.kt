package de.crysxd.octoapp.pluginsupport.di

import de.crysxd.octoapp.base.di.BaseComponent

object PluginSupportInjector {

    private lateinit var instance: PluginSupportComponent

    fun init(baseComponent: BaseComponent) {
        instance = DaggerPluginSupportComponent.builder()
            .baseComponent(baseComponent)
            .build()
    }

    fun get(): PluginSupportComponent = instance

}