package de.crysxd.octoapp.pluginsupport.di

import androidx.lifecycle.ViewModelProvider
import dagger.Component
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.di.BaseComponent
import de.crysxd.octoapp.base.network.OctoPrintProvider

@PluginSupportScope
@Component(
    modules = [
        ViewModelModule::class,
    ],
    dependencies = [
        BaseComponent::class
    ]
)
interface PluginSupportComponent {

    // ViewModelModule
    fun viewModelFactory(): ViewModelProvider.Factory

    // OctoprintModule
    fun octoprintProvider(): OctoPrintProvider
    fun octoprintRepository(): OctoPrintRepository
}